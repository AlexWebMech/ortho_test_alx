<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty('title', 'Авторизация | Интернет-магазин ORTHOBOOM');
$APPLICATION->SetPageProperty('description', 'На данной странице можно авторизоваться на сайте интернет-магазина ORTHOBOOM.');

global $USER;
if ($USER->IsAuthorized())  {
    LocalRedirect("/personal/");
}elseif(defined('REGISTRATION_SMS_ENABLE') && REGISTRATION_SMS_ENABLE) {
    ?>
    <script>
      $(document).ready(function(){
        $('#authOpen2').trigger('click');
      });
    </script>
    <?php
}else {
    ?>
    <div class="cabinet">
        <div class="container">
            
            <?$APPLICATION->IncludeComponent("bitrix:system.auth.form","auth",Array(
                    "REGISTER_URL" => "/register/",
                    "FORGOT_PASSWORD_URL" => "",
                    "PROFILE_URL" => "/personal/",
                    "SHOW_ERRORS" => "Y"
                )
            );?>

        </div>
    </div>
    <?php
}
?>





<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>