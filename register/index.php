<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$forgot = htmlspecialcharsbx($_GET["forgot_password"]);
$change = htmlspecialcharsbx($_GET["change_password"]);
$forgot_type = htmlspecialcharsbx($_GET["forgot_type"]);

if ($forgot == "yes") {
	$APPLICATION->SetPageProperty('title', 'Восстановление пароля');
	$APPLICATION->SetPageProperty('description', 'Восстановление пароля');
} else if ($change == "yes") {
	$APPLICATION->SetPageProperty('title', 'Смена пароля');
	$APPLICATION->SetPageProperty('description', 'Смена пароля');
} else {
	$APPLICATION->SetPageProperty('title', 'Регистрация');
	$APPLICATION->SetPageProperty('description', 'Регистрация');
}?>

<div class="cabinet">
	<div class="container">	
		<div class="navibar"><a href="/catalog/">Продолжить выбор товаров</a></div>
		<?
		if ($forgot == "yes"):?>
			<?if ($forgot_type == "f"):?>
				<?$APPLICATION->IncludeComponent("at:system.auth.forgotpasswd", "",
					Array(
						"SMS_PATTERN" => "Ваш новый пароль от личного кабинета Orthoboom #NEW_PASSWORD# Рекомендуем смените его сразу же после авторизации на сайте."
					)
				);?>
			<?elseif ($forgot_type == "u"):?>
				<?$APPLICATION->IncludeComponent("at:system.auth.forgotpasswd", "ur",
					Array(
						"SMS_PATTERN" => "Ваш новый пароль от личного кабинета Orthoboom #NEW_PASSWORD# Рекомендуем смените его сразу же после авторизации на сайте."
					)
				);?>
			<?else:?>
				<h1 class="center">Восстановление пароля</h1>
				<div class="order-done center">
					<p>Укажите в качестве кого Вы регистрировались на нашем сайте</p>
				</div>
				<div class="registration blue-buttons">
					<div class="registration__choose">
						<a href="<?=$APPLICATION->GetCurPageParam("forgot_type=f", array("forgot_type"))?>" style="color:#fff; text-decoration:none;">Вы частный покупатель?</a>
						<a href="<?=$APPLICATION->GetCurPageParam("forgot_type=u", array("forgot_type"))?>" style="color:#fff; text-decoration:none;">Вы юридическое лицо?</a>
					</div>			
				</div>
			<?endif;?>
		<?elseif($change == "yes"):?>
			<?$APPLICATION->IncludeComponent("bitrix:system.auth.changepasswd", "",
				Array()
			);?>
		<?else:?>
			<h1 class="center">Регистрация</h1>
			<div class="order-done center">
				<p>Вы можете зарегистрироваться как частный покупатель <br>или юридическое лицо</p>
			</div>
			<div class="registration blue-buttons">
				<div class="registration__choose">
					<a href="/register/f/" style="color:#fff; text-decoration:none;">Зарегистрироваться как частный покупатель</a>
					<a href="/register/u/" style="color:#fff; text-decoration:none;">Зарегистрироваться как юридическое лицо</a>
				</div>			
			</div>
		<?endif;?>
	</div>
</div>	

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>