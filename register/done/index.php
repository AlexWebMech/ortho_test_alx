<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty('title', 'Регистрация завершена');
$APPLICATION->SetPageProperty('description', 'Регистрация завершена');
?> 
<?
global $USER;
if ($USER->IsAuthorized()) {
	LocalRedirect("/personal/");
}
?>


<div class="cabinet">
	<div class="container">

		<?$APPLICATION->IncludeComponent("bitrix:system.auth.form","auth-after-register",Array(
		     "REGISTER_URL" => "/register/",
		     "FORGOT_PASSWORD_URL" => "",
		     "PROFILE_URL" => "/personal/",
		     "SHOW_ERRORS" => "Y"
		     )
		);?>
						
	</div>
</div>	

<?
// $arSelect = Array("PROPERTY_MANAGER");
// $arFilter = Array("IBLOCK_ID"=>50, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_INN"=>1111111111);
// $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
// if ($ob = $res->GetNextElement())
// {
// 	$arFields = $ob->GetFields();
// 	$rsUser = CUser::GetByID($arFields["PROPERTY_MANAGER_VALUE"]);
// 	$arUser = $rsUser->Fetch();
// 	echo "<pre>"; print_r($arUser); echo "</pre>";
// }
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>