<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty('title', 'Регистрация завершена');
$APPLICATION->SetPageProperty('description', 'Регистрация завершена');
?> 



<div class="cabinet">
	<div class="container">

		<div class="navibar"><a href="/catalog/">Продолжить выбор товаров</a></div>
		<h1 class="center">Регистрация завершена!</h1>
		<div class="order-done center">
			<p>Благодарим Вас за регистрацию на нашем сайте. <br><big>После проверки, Ваш аккаунт будет активирован</big> <br>и Вы сможете войти на сайт используя email и пароль.</p>
		</div>
		

	
	</div>
</div>	

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>