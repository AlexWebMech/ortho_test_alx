<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty('title', 'Регистрация розничного покупателя');
$APPLICATION->SetPageProperty('description', 'Регистрация розничного покупателя');
?> 



<div class="cabinet">
	<div class="container">

		<?$APPLICATION->IncludeComponent("bitrix:main.register","fiz",Array(
		        "USER_PROPERTY_NAME" => "", 
		        "SEF_MODE" => "Y", 
		        "SHOW_FIELDS" => Array(
		        	"NAME",
		        	"LAST_NAME",
		        	"PERSONAL_PHONE",
		        	"PERSONAL_CITY",
		        	"PERSONAL_STREET"
		        ), 
		        "REQUIRED_FIELDS" => Array(
		        	"NAME",
		        	"LAST_NAME",
		        	"PERSONAL_PHONE",
		        	"PERSONAL_CITY",
		        	"PERSONAL_STREET"
		        ), 
		        "AUTH" => "N", 
		        "USE_BACKURL" => "Y", 
		        "SUCCESS_PAGE" => "/register/done/", 
		        "SET_TITLE" => "Y", 
		        "USER_PROPERTY" => Array(), 
		        "SEF_FOLDER" => "/", 
		        "VARIABLE_ALIASES" => Array(),
		        "AJAX_MODE" => "Y"
		    )
		);?> 
						
	</div>
</div>	

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>