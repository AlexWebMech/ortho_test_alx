<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty('title', 'Регистрация новой организации');
$APPLICATION->SetPageProperty('description', 'Регистрация новой организации');
?> 



<div class="cabinet">
	<div class="container">

		<?$APPLICATION->IncludeComponent("bitrix:main.register","ur",Array(
		        "USER_PROPERTY_NAME" => "", 
		        "SEF_MODE" => "Y", 
		        "SHOW_FIELDS" => Array(
		        	"NAME",
		        	"LAST_NAME",
		        	"PERSONAL_PHONE",
		        	"PERSONAL_CITY"
		        ), 
		        "REQUIRED_FIELDS" => Array(
		        	"NAME",
		        	"LAST_NAME",
		        	"PERSONAL_PHONE",
		        	"PERSONAL_CITY"
		        ), 
		        "AUTH" => "N", 
		        "USE_BACKURL" => "Y", 
		        "SUCCESS_PAGE" => "/register/done_u/?tpl=2_0", 
		        "SET_TITLE" => "Y", 
		        "USER_PROPERTY" => Array(), 
		        "SEF_FOLDER" => "/", 
		        "VARIABLE_ALIASES" => Array(),
		        "AJAX_MODE" => "Y"
		    )
		);?> 
						
	</div>
</div>	

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>