<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Чат");
if (!$USER->IsAuthorized()) {
	LocalRedirect("/auth/");
}

if (getPersonType() != "U")
	//LocalRedirect("/personal/");
?>

<div class="cabinet">
	<div class="container">	
		<div class="navibar"><a href="/catalog/">Продолжить выбор товаров</a></div>
		<h1 class="center">Чат</h1><br>  
		<?require_once($_SERVER["DOCUMENT_ROOT"] . "/personal/sidebar.inc.php");?>
		<div class="right-block">
			<div class="right-wrap">
		<!--
			<div class="feedback">
				<h5 class="feedback__title">Напишите свой вопрос</h5>
				<form action="" method="post" class="form user-profile">
					<div class="form__col">
						<div>
							<textarea placeholder="Текст" name="TEXT"></textarea>
						</div>
					</div>
					<div class="form__col">
						<div class="form__row">
							<input type="text" name="NAME" value="" placeholder="Ваше имя" />
						</div>
						<div class="form__row">
							<input type="text" name="PHONE" value="" placeholder="Ваш телефон" />
						</div>
					</div>



				</form>

				<div class="form-bottom">	
					<div class="cancell-button"><a href="#">Отменить</a></div> 
					<div class="login-button"><input type="submit" name="save" class="btn-submit" value="Отправить"></div> 	
				</div>
			</div>
			-->
<?$APPLICATION->IncludeComponent(
	"ak:private.messages", 
	"private_messages_orthoboom", 
	array(
		"COMPONENT_TEMPLATE" => "private_messages_orthoboom",
		"IBLOCK_TYPE" => "private_messages_orthoboom",
		"IBLOCK_ID" => "55"
	),
	false
);?> 		</div>		
		</div>
	</div>
</div>	

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>