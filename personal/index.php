<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Персональный раздел");

if (!$USER->IsAuthorized()) {
	LocalRedirect("/auth/");
}
?><?
require_once($_SERVER["DOCUMENT_ROOT"] . "/personal/main.inc.php");
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>