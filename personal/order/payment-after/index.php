<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оплата заказа");
?>
<div class="container">
    <div>
        <?
	$service = \Bitrix\Sale\PaySystem\Manager::getObjectById(4);
	if ($service) {
		$order = \Bitrix\Sale\Order::load($_GET['ORDER_NUM']);
		if ($order)	{
			$paymentCollection = $order->getPaymentCollection();
        foreach ($paymentCollection as $payment) {
        if (!$payment->isInner()) {
        $context = \Bitrix\Main\Application::getInstance()->getContext();
        $initResult = $service->initiatePay($payment, $context->getRequest(), \Bitrix\Sale\PaySystem\BaseServiceHandler::STRING);
        $buffered_output = $initResult->getTemplate();
        echo $buffered_output;
        break;
        }
        }
        }
        } else {
        echo '<span style="color:red;">'.GetMessage("SOA_TEMPL_ORDER_PS_ERROR").'</span>';
        }
        ?>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>