<?
define("STOP_STATISTICS", true);

include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (!isset($_SESSION["BASKET_GRID"])) {
	LocalRedirect("/personal/cart/");
	die();
}
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8" />
        <title>Печать</title>
    </head>
    <body>

		<?
			ob_start();
		?>
		<table style="width:735px; margin: 0 auto;" class="cart" cellpadding="10" cellspacing="0">
			<thead>
				<tr>
					<td colspan="2" style="width:200px; padding-left:30px;">
						 Товар
					</td>
					<td>
						 Цена
					</td>
					<td>
						 Количество
					</td>
					<td>
						 Стоимость
					</td>
				</tr>
			</thead>
			<tbody>
				<?foreach ($_SESSION["BASKET_GRID"]["ROWS"] as $k => $arData):?>
					<tr>
						<td style="padding-left:30px; width: 80px">
							<?
							if (strlen($arData["data"]["PREVIEW_PICTURE_SRC"]) > 0):
								$url = $arData["data"]["PREVIEW_PICTURE_SRC"];
							elseif (strlen($arData["data"]["DETAIL_PICTURE_SRC"]) > 0):
								$url = $arData["data"]["DETAIL_PICTURE_SRC"];
							else:
								//$url = $templateFolder."/images/no_photo.png";
							endif;?>

							
				 			<a href="<?=$arData["data"]["DETAIL_PAGE_URL"]?>" target="_blank"><img src="<?=$url?>"></a>
						</td>
						<td>
				 			<a href="<?=$arData["data"]["DETAIL_PAGE_URL"]?>" target="_blank"><?=$arData["data"]["NAME"]?></a>
							<p>
								 Артикул: <?=$arData["data"]["ARTICUL"]?>
							</p>
						</td>
						<td style="color: #F54E86">
							<?=$arData["data"]["PRICE_FORMATED"]?>
						</td>
						<td style="text-align: center;">
							<?=($arData["data"]["QUANTITY"])?> шт.
						</td>
						<td style="padding-right:30px; width:80px; color: #F54E86">
							<?=$arData["data"]["SUM"]?>
						</td>
					</tr>
				<?endforeach;?>
				<tr>
					<td colspan="5" style="text-align:right; padding-right:50px; font-size:20px; background: #fff; height:30px;">
						 Сумма заказа: <span style="color: #F54E86; padding-left:30px;"><?=$_SESSION["ORDER_TOTAL_PRICE_FORMATED"]?></span>
					</td>
				</tr>
			</tbody>
		</table>
		<?
		$order_basket_table = ob_get_contents();
		ob_end_clean();
		?>
		
		<?
			ob_start();
		?>
		<table style="width:735px; margin: 0 auto;" cellpadding="5" cellspacing="0">
			<tbody>
				<?foreach ($_SESSION["ORDER_DATA"] as $arData):?>
					<tr style="background: #F4FAEE; ">
						<td style="height:40px; width:300px; padding-left: 30px;">
							<?=$arData["NAME"]?>
						</td>
						<td>
							<?=$arData["VALUE"]?>
						</td>
					</tr>
				<?endforeach;?>
			</tbody>
		</table>
		<?
		$order_info_table = ob_get_contents();
		ob_end_clean();
		?>

		<?
			echo $order_basket_table;
			echo $order_info_table;
		?>
    </body>
</html>