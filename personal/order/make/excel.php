<?
// https://gist.github.com/samatsav/6637984

define("STOP_STATISTICS", true);

include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if (!isset($_SESSION["BASKET_GRID"])) {
	LocalRedirect("/personal/cart/");
	die();
}

include("excel.class.php");

$filename = 'order.xls'; // The file name you want any resulting file to be called.

#create an instance of the class
$xls = new ExportXLS($filename);


#lets set some headers for top of the spreadsheet
#

$header = "Заказ"; // single first col text
$xls->addHeader($header);

#add blank line
$header = null;
$xls->addHeader($header);

$header[] = "Товары";
$header[] = "Цена";
$header[] = "Количество";
$header[] = "Сумма";

$xls->addHeader($header);

foreach ($_SESSION["BASKET_GRID"]["ROWS"] as $k => $arData):
	$row = array();
	$row[] = $arData["data"]["NAME"] . " [Артикул: " . $arData["data"]["ARTICUL"] . "]             ";
	$row[] = $arData["data"]["PRICE_FORMATED"];
	$row[] = str_replace("&nbsp;", " ", $arData["columns"]["QUANTITY"]);
	$row[] = $arData["data"]["SUM"];
	$xls->addRow($row);
endforeach;

#add blank line
$row = null;
$xls->addRow($row);

$row = array();
$row[] = "Итого: " . $_SESSION["ORDER_TOTAL_PRICE_FORMATED"];
$xls->addRow($row);

#add blank line
$row = null;
$xls->addRow($row);

$row = "Данные о покупателе";
$xls->addRow($row);

foreach ($_SESSION["ORDER_DATA"] as $arData):
	$row = array();
	$row[] = $arData["NAME"];
	$row[] = $arData["VALUE"];
	$xls->addRow($row);
endforeach;


$xls->sendFile();
?>

