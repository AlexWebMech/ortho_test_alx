<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заказы");

if (!$USER->IsAuthorized()) {
	LocalRedirect("/auth/");
}
?><div class="cabinet">
	<div class="container">	
		<div class="navibar"><a href="/catalog/">Продолжить выбор товаров</a></div>
		<h1 class="center">История заказов</h1><br>  
		<?require_once($_SERVER["DOCUMENT_ROOT"] . "/personal/sidebar.inc.php");?>
		<div class="right-block">

	<div class="user-history">	
		<div class="order-search">
<p>Уточнить информацию по Вашему заказу можно у Вашего персонального менеджера: Мазанова Любовь, тел. +79101050145 (пн-пт с 08:00 до 17:00), e-mail: <a href="mailto:mazanova.ll@julianna.ru">mazanova.ll@julianna.ru</a>.</p>		<br>					
			<form action="">
				<input type="text" name="ORDER_ID" placeholder="Номер заказа"><input type="submit" value="Найти">
			</form>
		</div>	

			<?$APPLICATION->IncludeComponent(
				"bitrix:sale.personal.order", 
				"order-list", 
				array(
					"SEF_MODE" => "N",
					"SEF_FOLDER" => "/personal/order/",
					"ORDERS_PER_PAGE" => "10",
					"PATH_TO_PAYMENT" => "/personal/order/payment/",
					"PATH_TO_BASKET" => "/personal/cart/",
					"SET_TITLE" => "Y",
					"SAVE_IN_SESSION" => "N",
					"NAV_TEMPLATE" => "arrows",
					"SHOW_ACCOUNT_NUMBER" => "Y",
					"COMPONENT_TEMPLATE" => "list",
					"PROP_1" => array(
					),
					"PROP_2" => array(
					),
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"CACHE_TYPE" => "N",
					"CACHE_TIME" => "3600",
					"CACHE_GROUPS" => "Y",
					"CUSTOM_SELECT_PROPS" => array(
					),
					"HISTORIC_STATUSES" => array(
						0 => "F",
					),
					"STATUS_COLOR_DO" => "gray",
					"STATUS_COLOR_F" => "gray",
					"STATUS_COLOR_GO" => "gray",
					"STATUS_COLOR_N" => "green",
					"STATUS_COLOR_PSEUDO_CANCELLED" => "red",
					"COMPOSITE_FRAME_MODE" => "A",
					"COMPOSITE_FRAME_TYPE" => "AUTO"
				),
				false
			);?>
</div>
		</div>
	</div>
</div>	

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>