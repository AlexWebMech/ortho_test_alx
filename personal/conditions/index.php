<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Персональные условия");
if (!$USER->IsAuthorized()) {
	LocalRedirect("/auth/");
}

$currentUserType = getPersonType();
$arPrice = getPriceType();
?>

<div class="cabinet">
	<div class="container">	
		<div class="navibar"><a href="/catalog/">Продолжить выбор товаров</a></div>
		<h1 class="center">Персональные условия</h1><br>  
		<?require_once($_SERVER["DOCUMENT_ROOT"] . "/personal/sidebar.inc.php");?>
		<div class="right-block">
			<div class="right-wrap">
			
			<?if ($currentUserType == "U"):?>

			<?
				global $arFilterAction;
				$arFilterAction = array("PROPERTY_USERS" => $USER->GetID());
			?>

				<?$APPLICATION->IncludeComponent(
					"bitrix:news.list", 
					"exclusive-slider", 
					array(
						"ACTIVE_DATE_FORMAT" => "d.m.Y",
						"ADD_SECTIONS_CHAIN" => "N",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "Y",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"CHECK_DATES" => "Y",
						"COMPOSITE_FRAME_MODE" => "A",
						"COMPOSITE_FRAME_TYPE" => "AUTO",
						"DETAIL_URL" => "",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"DISPLAY_DATE" => "Y",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "Y",
						"DISPLAY_PREVIEW_TEXT" => "Y",
						"DISPLAY_TOP_PAGER" => "N",
						"FIELD_CODE" => array(
							0 => "",
							1 => "",
						),
						"FILTER_NAME" => "arFilterAction",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"IBLOCK_ID" => "52",
						"IBLOCK_TYPE" => "work_ob",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
						"INCLUDE_SUBSECTIONS" => "Y",
						"MESSAGE_404" => "",
						"NEWS_COUNT" => "20",
						"PAGER_BASE_LINK_ENABLE" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => ".default",
						"PAGER_TITLE" => "",
						"PARENT_SECTION" => "",
						"PARENT_SECTION_CODE" => "",
						"PREVIEW_TRUNCATE_LEN" => "",
						"PROPERTY_CODE" => array(
							0 => "",
							1 => "",
						),
						"SET_BROWSER_TITLE" => "N",
						"SET_LAST_MODIFIED" => "N",
						"SET_META_DESCRIPTION" => "N",
						"SET_META_KEYWORDS" => "N",
						"SET_STATUS_404" => "N",
						"SET_TITLE" => "N",
						"SHOW_404" => "N",
						"SORT_BY1" => "ACTIVE_FROM",
						"SORT_BY2" => "SORT",
						"SORT_ORDER1" => "DESC",
						"SORT_ORDER2" => "ASC",
						"COMPONENT_TEMPLATE" => "exclusive-slider"
					),
					false
				);?>
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/conditions_before.php"), false);?>	
			<?endif;?>



			<?
				global $arrFilter;
				//$arrFilter['OFFERS']['CATALOG_AVAILABLE'] = true; 
				//$arrFilter["PROPERTY_TOP"] = 692915;
				/*
					Серебро - 693417
					Золото - 693418
					Платина - 693419
				*/
				$arPersonalAction = array();
				$arSelect = Array("ID", "NAME", "PROPERTY_PRODUCTS");
				$arFilter = Array("IBLOCK_ID"=>58, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y"/*, "PROPERTY_STATUS" => 693417*/);

				$arGroups = CUser::GetUserGroup($USER->GetID());
				if (in_array(12, $arGroups)) {
					$arFilter["PROPERTY_STATUS"] = 693417;
				} else if (in_array(13, $arGroups)) {
					$arFilter["PROPERTY_STATUS"] = 693418;
				} else if (in_array(14, $arGroups)) {
					$arFilter["PROPERTY_STATUS"] = 693419;
				}

				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
				while($ob = $res->GetNextElement())
				{
					$arFields = $ob->GetFields();
					$arPersonalAction[] = $arFields["PROPERTY_PRODUCTS_VALUE"];
				}

				$arrFilter["ID"] = $arPersonalAction;
			?>
			<?if (count($arrFilter["ID"]) > 0 && isset($arFilter["PROPERTY_STATUS"])):?>
			<!--<h2>Ваша эксклюзивная скидка по категориям товаров</h2>-->
			<?/*$APPLICATION->IncludeComponent(
				"bitrix:catalog.section", 
				"catalog-exclusive", 
				array(
					"IBLOCK_TYPE" => "1c_catalog",
					"IBLOCK_ID" => "94",
					"SECTION_ID" => $_REQUEST["SECTION_ID"],
					"SECTION_CODE" => "",
					"SECTION_USER_FIELDS" => array(
						0 => "",
						1 => "",
					),
					"ELEMENT_SORT_FIELD" => "{$sortBy}",
					"ELEMENT_SORT_ORDER" => "{$sortOrder}",
					"ELEMENT_SORT_FIELD2" => "id",
					"ELEMENT_SORT_ORDER2" => "asc",
					"FILTER_NAME" => "arrFilter",
					"INCLUDE_SUBSECTIONS" => "Y",
					"SHOW_ALL_WO_SECTION" => "Y",
					"PAGE_ELEMENT_COUNT" => "10",
					"LINE_ELEMENT_COUNT" => "4",
					"PROPERTY_CODE" => array(
						0 => "CML2_ARTICLE",
						1 => "CML2_MANUFACTURER",
						2 => "CML2_ATTRIBUTES",
						3 => "ARTICUL",
						4 => "",
					),
					"OFFERS_LIMIT" => "0",
					"TEMPLATE_THEME" => "",
					"ADD_PICT_PROP" => "-",
					"LABEL_PROP" => "-",
					"MESS_BTN_BUY" => "Купить",
					"MESS_BTN_ADD_TO_BASKET" => "В корзину",
					"MESS_BTN_SUBSCRIBE" => "Подписаться",
					"MESS_BTN_DETAIL" => "Подробнее",
					"MESS_NOT_AVAILABLE" => "Нет в наличии",
					"SECTION_URL" => "",
					"DETAIL_URL" => "/catalog2/#CODE#/",
					"SECTION_ID_VARIABLE" => "SECTION_ID",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_GROUPS" => "N",
					"SET_META_KEYWORDS" => "N",
					"META_KEYWORDS" => "",
					"SET_META_DESCRIPTION" => "Y",
					"META_DESCRIPTION" => "",
					"BROWSER_TITLE" => "-",
					"ADD_SECTIONS_CHAIN" => "N",
					"DISPLAY_COMPARE" => "N",
					"SET_TITLE" => "Y",
					"SET_STATUS_404" => "N",
					"CACHE_FILTER" => "N",
					"PRICE_CODE" => $arPrice,
					"USE_PRICE_COUNT" => "N",
					"SHOW_PRICE_COUNT" => "1",
					"PRICE_VAT_INCLUDE" => "Y",
					"BASKET_URL" => "/personal/cart/",
					"ACTION_VARIABLE" => "action",
					"PRODUCT_ID_VARIABLE" => "id",
					"USE_PRODUCT_QUANTITY" => "Y",
					"ADD_PROPERTIES_TO_BASKET" => "Y",
					"PRODUCT_PROPS_VARIABLE" => "prop",
					"PARTIAL_PRODUCT_PROPERTIES" => "Y",
					"PRODUCT_PROPERTIES" => array(
					),
					"PAGER_TEMPLATE" => "modern",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Товары",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"PRODUCT_QUANTITY_VARIABLE" => "quantity",
					"COMPONENT_TEMPLATE" => "catalog-wishlist",
					"HIDE_NOT_AVAILABLE" => "N",
					"OFFERS_FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"OFFERS_PROPERTY_CODE" => array(
						0 => "MORE_PHOTO",
						1 => "OBUV_MATERIAL_VERKHA",
						2 => "OBUV_PODKLADKA",
						3 => "OBUV_RAZMER",
						4 => "TSVETOVAYA_GAMMA",
						5 => "",
					),
					"OFFERS_SORT_FIELD" => "sort",
					"OFFERS_SORT_ORDER" => "asc",
					"OFFERS_SORT_FIELD2" => "id",
					"OFFERS_SORT_ORDER2" => "desc",
					"BACKGROUND_IMAGE" => "-",
					"PRODUCT_DISPLAY_MODE" => "Y",
					"PRODUCT_SUBSCRIPTION" => "N",
					"SHOW_DISCOUNT_PERCENT" => "N",
					"SHOW_OLD_PRICE" => "N",
					"SEF_MODE" => "N",
					"SET_BROWSER_TITLE" => "Y",
					"SET_LAST_MODIFIED" => "N",
					"USE_MAIN_ELEMENT_SECTION" => "N",
					"CONVERT_CURRENCY" => "N",
					"OFFERS_CART_PROPERTIES" => array(
					),
					"PAGER_BASE_LINK_ENABLE" => "N",
					"SHOW_404" => "N",
					"MESSAGE_404" => "",
					"DISABLE_INIT_JS_IN_COMPONENT" => "N",
					"OFFER_ADD_PICT_PROP" => "-",
					"OFFER_TREE_PROPS" => array(
						0 => "MORE_PHOTO",
						1 => "OBUV_MATERIAL_VERKHA",
						2 => "OBUV_PODKLADKA",
						3 => "OBUV_RAZMER",
						4 => "TSVETOVAYA_GAMMA",
						5 => "",
					),
					"SHOW_CLOSE_POPUP" => "Y",
					"MESS_BTN_COMPARE" => "Сравнить",
					"COMPOSITE_FRAME_MODE" => "A",
					"COMPOSITE_FRAME_TYPE" => "AUTO",
					"ADD_TO_BASKET_ACTION" => "ADD"
				),
				false
			);*/?>


							<?$APPLICATION->IncludeComponent(
				"bitrix:catalog.section", 
				"catalog-exclusive", 
				array(
					"IBLOCK_TYPE" => "1c_catalog",
					"IBLOCK_ID" => "94",
					"SECTION_ID" => $_REQUEST["SECTION_ID"],
					"SECTION_CODE" => "",
					"SECTION_USER_FIELDS" => array(
						0 => "",
						1 => "",
					),
					"ELEMENT_SORT_FIELD" => "sort",
					"ELEMENT_SORT_ORDER" => "asc",
					"ELEMENT_SORT_FIELD2" => "id",
					"ELEMENT_SORT_ORDER2" => "asc",
					"FILTER_NAME" => "arrFilter",
					"INCLUDE_SUBSECTIONS" => "Y",
					"SHOW_ALL_WO_SECTION" => "Y",
					"PAGE_ELEMENT_COUNT" => "20",
					"LINE_ELEMENT_COUNT" => "4",
					"PROPERTY_CODE" => array(
						0 => "CML2_ARTICLE",
						1 => "CML2_MANUFACTURER",
						2 => "CML2_ATTRIBUTES",
						3 => "ARTICUL",
						4 => "",
					),
					"OFFERS_LIMIT" => "0",
					"TEMPLATE_THEME" => "",
					"ADD_PICT_PROP" => "-",
					"LABEL_PROP" => "-",
					"MESS_BTN_BUY" => "Купить",
					"MESS_BTN_ADD_TO_BASKET" => "В корзину",
					"MESS_BTN_SUBSCRIBE" => "Подписаться",
					"MESS_BTN_DETAIL" => "Подробнее",
					"MESS_NOT_AVAILABLE" => "Нет в наличии",
					"SECTION_URL" => "",
					"DETAIL_URL" => "/catalog2/#CODE#/",
					"SECTION_ID_VARIABLE" => "SECTION_ID",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_GROUPS" => "N",
					"SET_META_KEYWORDS" => "N",
					"META_KEYWORDS" => "",
					"SET_META_DESCRIPTION" => "Y",
					"META_DESCRIPTION" => "",
					"BROWSER_TITLE" => "-",
					"ADD_SECTIONS_CHAIN" => "N",
					"DISPLAY_COMPARE" => "N",
					"SET_TITLE" => "Y",
					"SET_STATUS_404" => "N",
					"CACHE_FILTER" => "N",
					"PRICE_CODE" => $arPrice,
					"USE_PRICE_COUNT" => "N",
					"SHOW_PRICE_COUNT" => "1",
					"PRICE_VAT_INCLUDE" => "Y",
					"BASKET_URL" => "/personal/cart/",
					"ACTION_VARIABLE" => "action",
					"PRODUCT_ID_VARIABLE" => "id",
					"USE_PRODUCT_QUANTITY" => "Y",
					"ADD_PROPERTIES_TO_BASKET" => "Y",
					"PRODUCT_PROPS_VARIABLE" => "prop",
					"PARTIAL_PRODUCT_PROPERTIES" => "Y",
					"PRODUCT_PROPERTIES" => array(),
					"PAGER_TEMPLATE" => "modern",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Товары",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"PRODUCT_QUANTITY_VARIABLE" => "quantity",
					"COMPONENT_TEMPLATE" => "catalog-wishlist",
					"HIDE_NOT_AVAILABLE" => "N",
					"OFFERS_FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"OFFERS_PROPERTY_CODE" => array(
						0 => "MORE_PHOTO",
						1 => "OBUV_MATERIAL_VERKHA",
						2 => "OBUV_RAZMER",
						3 => "TSVETOVAYA_GAMMA",
						4 => "OBUV_PODKLADKA",
						5 => "",
					),
					"OFFERS_SORT_FIELD" => "sort",
					"OFFERS_SORT_ORDER" => "asc",
					"OFFERS_SORT_FIELD2" => "id",
					"OFFERS_SORT_ORDER2" => "desc",
					"BACKGROUND_IMAGE" => "-",
					"PRODUCT_DISPLAY_MODE" => "Y",
					"PRODUCT_SUBSCRIPTION" => "N",
					"SHOW_DISCOUNT_PERCENT" => "N",
					"SHOW_OLD_PRICE" => "N",
					"SEF_MODE" => "N",
					"SET_BROWSER_TITLE" => "Y",
					"SET_LAST_MODIFIED" => "N",
					"USE_MAIN_ELEMENT_SECTION" => "N",
					"CONVERT_CURRENCY" => "N",
					"OFFERS_CART_PROPERTIES" => array(
						0 => "OBUV_RAZMER",
					),
					"PAGER_BASE_LINK_ENABLE" => "N",
					"SHOW_404" => "N",
					"MESSAGE_404" => "",
					"DISABLE_INIT_JS_IN_COMPONENT" => "N",
					"OFFER_ADD_PICT_PROP" => "-",
					"OFFER_TREE_PROPS" => array(
						0 => "MORE_PHOTO",
						1 => "OBUV_MATERIAL_VERKHA_1",
						2 => "OBUV_PODKLADKA",
						3 => "OBUV_RAZMER",
						4 => "TSVETOVAYA_GAMMA",
						5 => "",
					),
					"SHOW_CLOSE_POPUP" => "Y",
					"MESS_BTN_COMPARE" => "Сравнить",
					"COMPOSITE_FRAME_MODE" => "A",
					"COMPOSITE_FRAME_TYPE" => "AUTO",
					"ADD_TO_BASKET_ACTION" => "ADD"
				),
				false
			);?>
			<?endif;?>

			<div class="h2-line"><span><a href="/personal/new/">Новинки</a></span></div>
			<?
				global $arrFilter;
				$arrFilter = array("PROPERTY_NEW" => 692912);

				if (getPersonType() != "U") {
					$arrFilter["!=PROPERTY_NE_POKAZYVAT_DLYA_FL_SAYT_ORTOBUM"] = 513072;
				}
				
			?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:catalog.section", 
				"catalog-slider", 
				array(
					"IBLOCK_TYPE" => "1c_catalog",
					"IBLOCK_ID" => "94",
					"SECTION_ID" => $_REQUEST["SECTION_ID"],
					"SECTION_CODE" => "",
					"SECTION_USER_FIELDS" => array(
						0 => "",
						1 => "",
					),
					"ELEMENT_SORT_FIELD" => "sort",
					"ELEMENT_SORT_ORDER" => "asc",
					"ELEMENT_SORT_FIELD2" => "id",
					"ELEMENT_SORT_ORDER2" => "asc",
					"FILTER_NAME" => "arrFilter",
					"INCLUDE_SUBSECTIONS" => "Y",
					"SHOW_ALL_WO_SECTION" => "Y",
					"PAGE_ELEMENT_COUNT" => "20",
					"LINE_ELEMENT_COUNT" => "4",
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "ARTICUL",
						2 => "",
					),
					"OFFERS_LIMIT" => "0",
					"TEMPLATE_THEME" => "",
					"ADD_PICT_PROP" => "-",
					"LABEL_PROP" => "-",
					"MESS_BTN_BUY" => "Купить",
					"MESS_BTN_ADD_TO_BASKET" => "В корзину",
					"MESS_BTN_SUBSCRIBE" => "Подписаться",
					"MESS_BTN_DETAIL" => "Подробнее",
					"MESS_NOT_AVAILABLE" => "Нет в наличии",
					"SECTION_URL" => "",
					"DETAIL_URL" => "/catalog2/#CODE#/",
					"SECTION_ID_VARIABLE" => "SECTION_ID",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_GROUPS" => "N",
					"SET_META_KEYWORDS" => "N",
					"META_KEYWORDS" => "",
					"SET_META_DESCRIPTION" => "Y",
					"META_DESCRIPTION" => "",
					"BROWSER_TITLE" => "-",
					"ADD_SECTIONS_CHAIN" => "N",
					"DISPLAY_COMPARE" => "N",
					"SET_TITLE" => "Y",
					"SET_STATUS_404" => "N",
					"CACHE_FILTER" => "N",
					"PRICE_CODE" => $arPrice,
					"USE_PRICE_COUNT" => "N",
					"SHOW_PRICE_COUNT" => "1",
					"PRICE_VAT_INCLUDE" => "Y",
					"BASKET_URL" => "/personal/cart/",
					"ACTION_VARIABLE" => "action",
					"PRODUCT_ID_VARIABLE" => "id",
					"USE_PRODUCT_QUANTITY" => "N",
					"ADD_PROPERTIES_TO_BASKET" => "Y",
					"PRODUCT_PROPS_VARIABLE" => "prop",
					"PARTIAL_PRODUCT_PROPERTIES" => "Y",
					"PRODUCT_PROPERTIES" => array(
					),
					"PAGER_TEMPLATE" => "modern",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Товары",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"PRODUCT_QUANTITY_VARIABLE" => "quantity",
					"COMPONENT_TEMPLATE" => "catalog-grid",
					"HIDE_NOT_AVAILABLE" => "N",
					"OFFERS_FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"OFFERS_PROPERTY_CODE" => array(
						0 => "KRASKA_TSVET",
						1 => "OBUV_RAZMER",
						2 => "CML2_BAR_CODE",
						3 => "SIZE",
						4 => "",
					),
					"OFFERS_SORT_FIELD" => "sort",
					"OFFERS_SORT_ORDER" => "asc",
					"OFFERS_SORT_FIELD2" => "id",
					"OFFERS_SORT_ORDER2" => "desc",
					"BACKGROUND_IMAGE" => "-",
					"PRODUCT_DISPLAY_MODE" => "Y",
					"PRODUCT_SUBSCRIPTION" => "N",
					"SHOW_DISCOUNT_PERCENT" => "N",
					"SHOW_OLD_PRICE" => "N",
					"SEF_MODE" => "N",
					"SET_BROWSER_TITLE" => "Y",
					"SET_LAST_MODIFIED" => "N",
					"USE_MAIN_ELEMENT_SECTION" => "N",
					"CONVERT_CURRENCY" => "N",
					"OFFERS_CART_PROPERTIES" => array(
					),
					"PAGER_BASE_LINK_ENABLE" => "N",
					"SHOW_404" => "N",
					"MESSAGE_404" => "",
					"DISABLE_INIT_JS_IN_COMPONENT" => "N",
					"OFFER_ADD_PICT_PROP" => "-",
					"OFFER_TREE_PROPS" => array(
						0 => "KRASKA_TSVET",
						1 => "OBUV_RAZMER",
					),
					"SHOW_CLOSE_POPUP" => "Y",
					"MESS_BTN_COMPARE" => "Сравнить",
					"COMPOSITE_FRAME_MODE" => "A",
					"COMPOSITE_FRAME_TYPE" => "AUTO",
					"ADD_TO_BASKET_ACTION" => "ADD"
				),
				false
			);?>
			
				
			<div class="clearfix"></div>
			
			<h2 class="h2-line"><span><a href="/personal/recommended/">Рекомендуемые товары</a></span></h2> 
			<?
				global $arrFilter;
				$arrFilter = array("PROPERTY_RECOMMENDED" => 692914);

				if (getPersonType() != "U") {
					$arrFilter["!=PROPERTY_NE_POKAZYVAT_DLYA_FL_SAYT_ORTOBUM"] = 513072;
				}
			?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:catalog.section", 
				"catalog-slider", 
				array(
					"IBLOCK_TYPE" => "1c_catalog",
					"IBLOCK_ID" => "94",
					"SECTION_ID" => $_REQUEST["SECTION_ID"],
					"SECTION_CODE" => "",
					"SECTION_USER_FIELDS" => array(
						0 => "",
						1 => "",
					),
					"ELEMENT_SORT_FIELD" => "sort",
					"ELEMENT_SORT_ORDER" => "asc",
					"ELEMENT_SORT_FIELD2" => "id",
					"ELEMENT_SORT_ORDER2" => "asc",
					"FILTER_NAME" => "arrFilter",
					"INCLUDE_SUBSECTIONS" => "Y",
					"SHOW_ALL_WO_SECTION" => "Y",
					"PAGE_ELEMENT_COUNT" => "20",
					"LINE_ELEMENT_COUNT" => "4",
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "ARTICUL",
						2 => "",
					),
					"OFFERS_LIMIT" => "0",
					"TEMPLATE_THEME" => "",
					"ADD_PICT_PROP" => "-",
					"LABEL_PROP" => "-",
					"MESS_BTN_BUY" => "Купить",
					"MESS_BTN_ADD_TO_BASKET" => "В корзину",
					"MESS_BTN_SUBSCRIBE" => "Подписаться",
					"MESS_BTN_DETAIL" => "Подробнее",
					"MESS_NOT_AVAILABLE" => "Нет в наличии",
					"SECTION_URL" => "",
					"DETAIL_URL" => "/catalog2/#CODE#/",
					"SECTION_ID_VARIABLE" => "SECTION_ID",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_GROUPS" => "N",
					"SET_META_KEYWORDS" => "N",
					"META_KEYWORDS" => "",
					"SET_META_DESCRIPTION" => "Y",
					"META_DESCRIPTION" => "",
					"BROWSER_TITLE" => "-",
					"ADD_SECTIONS_CHAIN" => "N",
					"DISPLAY_COMPARE" => "N",
					"SET_TITLE" => "Y",
					"SET_STATUS_404" => "N",
					"CACHE_FILTER" => "N",
					"PRICE_CODE" => $arPrice,
					"USE_PRICE_COUNT" => "N",
					"SHOW_PRICE_COUNT" => "1",
					"PRICE_VAT_INCLUDE" => "Y",
					"BASKET_URL" => "/personal/cart/",
					"ACTION_VARIABLE" => "action",
					"PRODUCT_ID_VARIABLE" => "id",
					"USE_PRODUCT_QUANTITY" => "N",
					"ADD_PROPERTIES_TO_BASKET" => "Y",
					"PRODUCT_PROPS_VARIABLE" => "prop",
					"PARTIAL_PRODUCT_PROPERTIES" => "Y",
					"PRODUCT_PROPERTIES" => array(
					),
					"PAGER_TEMPLATE" => "modern",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Товары",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"PRODUCT_QUANTITY_VARIABLE" => "quantity",
					"COMPONENT_TEMPLATE" => "catalog-grid",
					"HIDE_NOT_AVAILABLE" => "N",
					"OFFERS_FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"OFFERS_PROPERTY_CODE" => array(
						0 => "KRASKA_TSVET",
						1 => "OBUV_RAZMER",
						2 => "CML2_BAR_CODE",
						3 => "SIZE",
						4 => "",
					),
					"OFFERS_SORT_FIELD" => "sort",
					"OFFERS_SORT_ORDER" => "asc",
					"OFFERS_SORT_FIELD2" => "id",
					"OFFERS_SORT_ORDER2" => "desc",
					"BACKGROUND_IMAGE" => "-",
					"PRODUCT_DISPLAY_MODE" => "Y",
					"PRODUCT_SUBSCRIPTION" => "N",
					"SHOW_DISCOUNT_PERCENT" => "N",
					"SHOW_OLD_PRICE" => "N",
					"SEF_MODE" => "N",
					"SET_BROWSER_TITLE" => "Y",
					"SET_LAST_MODIFIED" => "N",
					"USE_MAIN_ELEMENT_SECTION" => "N",
					"CONVERT_CURRENCY" => "N",
					"OFFERS_CART_PROPERTIES" => array(
					),
					"PAGER_BASE_LINK_ENABLE" => "N",
					"SHOW_404" => "N",
					"MESSAGE_404" => "",
					"DISABLE_INIT_JS_IN_COMPONENT" => "N",
					"OFFER_ADD_PICT_PROP" => "-",
					"OFFER_TREE_PROPS" => array(
						0 => "KRASKA_TSVET",
						1 => "OBUV_RAZMER",
					),
					"SHOW_CLOSE_POPUP" => "Y",
					"MESS_BTN_COMPARE" => "Сравнить",
					"COMPOSITE_FRAME_MODE" => "A",
					"COMPOSITE_FRAME_TYPE" => "AUTO",
					"ADD_TO_BASKET_ACTION" => "ADD"
				),
				false
			);?>							
			<div class="clearfix"></div><br>
			</div>
		</div>
	</div>
</div>	

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>