<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $user_leval;
global $USER;
$user_leval = array();

require("level.data.php");

if (!isset($currentUser) || !is_array($currentUser)) {
	$rsUser = CUser::GetByID($USER->GetID());
	$currentUser = $rsUser->Fetch();
	$currentUser["UF_SUMMA"] = intval($currentUser["UF_SUMMA"]);
}

$user_leval["NAME"] = "";
$user_leval["ID"] = 0;
if($currentUser["UF_SUMMA"] >= $user_leval["SILVER"][0] && $currentUser["UF_SUMMA"] <= $user_leval["SILVER"][1]):
	$user_leval["NAME"] = "Серебро";
	$user_leval["ID"] = 1;
elseif($currentUser["UF_SUMMA"] >= $user_leval["GOLD"][0] && $currentUser["UF_SUMMA"] <= $user_leval["GOLD"][1]):
	$user_leval["NAME"] = "Золото";
	$user_leval["ID"] = 2;
elseif($currentUser["UF_SUMMA"] >= $user_leval["PLATINUM"][0] && $currentUser["UF_SUMMA"] <= $user_leval["PLATINUM"][1]):
	$user_leval["NAME"] = "Платина";
	$user_leval["ID"] = 3;
endif;

?>