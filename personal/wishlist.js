var WishList = (function() {

	var self;

	return {
		init: function() {
			self = this;
		},
		delete: function(productId, elem) {
            if (!$(".bx_catalog_list_home")[0] && !hasClass(document.getElementById('id' + productId), 'active')) {
                self.add(productId);
                return;
            }
			$.ajax({
	            url: "/personal/ajax.php",
	            type: "POST",
	            dataType: "json",
	            data: "act=delete&product_id=" + productId,
	            success: function(res){
	            	if (res.delete == "Y") {
	            		$(elem).parents("tr").remove();
	            	}
	            	if (!$(".bx_catalog_list_home")[0]) {
                        self.showSuccessDelete();
                        document.getElementById('id' + productId).classList.remove('active');
					}
	            	var rows = $(".bx_catalog_list_home tr");
	            	if ($(".bx_catalog_list_home")[0] && rows.length == 0) {
	            		location.reload();
	            	}
	            },
                error: function(err){
                    console.log(err);
                }
	        });
		},
		deleteAll: function(productId, elem) {
			$.ajax({
	            url: "/personal/ajax.php",
	            type: "POST",
	            dataType: "json",
	            data: "act=delete_all",
	            success: function(res){
	            	if (res.deleteAll == "Y") {
	            		location.reload();
	            	}
	            },
	            error: function(err){
	                alert("Error");
	            }
	        });
		},
		add: function(productId) {
			if (hasClass(document.getElementById('id' + productId), 'active')) {
                self.delete(productId);
				return;
			}
			$.ajax({
	            url: "/personal/ajax.php",
	            type: "POST",
	            dataType: "json",
	            data: "act=add&product_id=" + productId,
	            success: function(res){
	            	if (res.add == "Y") {
                        document.getElementById('id' + productId).classList.add('active');
	            		self.showSuccess();
                    }
	            },
	            error: function(err){
                    console.log(err);
	            }
	        });
		},
		showSuccess: function() {
			$.fancybox({ 
				padding:0,
				wrapCSS: 'wl-popup',
				
				helpers: {
					overlay: {
						locked: false
					}
				},
				content: '<div class="wishlist-popup">Товар успешно добавлен в список желаний</div>',
				type: "html"
			});
		},
		showSuccessDelete: function() {
			$.fancybox({
				padding:0,
				wrapCSS: 'wl-popup',

				helpers: {
					overlay: {
						locked: false
					}
				},
				content: '<div class="wishlist-popup">Товар успешно удален из список желаний</div>',
				type: "html"
			});
		},
		showUnregister: function() {
			$.fancybox({ 
				padding:0,
				wrapCSS: 'wl-popup',
				
				helpers: {
					overlay: {
						locked: false
					}
				},
				content: '<div class="wishlist-popup"><p>В список желаний могут добавлять <br>только зарегистрированные пользователи</p><p><a href="/register/?tpl=2_0">Зарегистрироваться сейчас</a></p></div>',
				type: "html"
			});
		}
	}

})();
function hasClass(element, className) {
    return (' ' + element.className + ' ').indexOf(' ' + className+ ' ') > -1;
}
// jQuery(function() {
// 	WishList.init();
// });