<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Данные клиента");
$templateName = (getPersonType() == "U") ? "profile_u" : "profile_f";
?>

    <div class="cabinet">
        <div class="container">
            <div class="navibar"><a href="/catalog/">Продолжить выбор товаров</a></div>
            <h1 class="center">Данные клиента</h1><br>
            <?require_once($_SERVER["DOCUMENT_ROOT"] . "/personal/sidebar.inc.php");?>
            <div class="right-block">
                <?php
                if(defined('REGISTRATION_SMS_ENABLE') && REGISTRATION_SMS_ENABLE) {
                // todo Нельзя редактировать Телефон,логин
                    ?>
                <div class="modal-input-wrapper flex-container-w100 flex-side">
                    Раздел в разработке
                </div>
                    
                    <?php
                }else {
                    $APPLICATION->IncludeComponent(
                        "bitrix:main.profile",
                        $templateName,
                        array(
                            "SET_TITLE" => "Y",
                            "COMPONENT_TEMPLATE" => "profile_f",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "undefined",
                            "USER_PROPERTY" => array(
                                0 => "UF_NOTIFY_ACTION",
                            ),
                            "SEND_INFO" => "N",
                            "CHECK_RIGHTS" => "N",
                            "USER_PROPERTY_NAME" => "",
                            "COMPOSITE_FRAME_MODE" => "A",
                            "COMPOSITE_FRAME_TYPE" => "AUTO"
                        ),
                        false
                    );
                }
                ?>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>