<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Документооборот");
if (!$USER->IsAuthorized()) {
	LocalRedirect("/auth/");
}
if (getPersonType() != "U")
	LocalRedirect("/personal/");
?>

<div class="cabinet">
	<div class="container">	
		<div class="navibar"><a href="/catalog/">Продолжить выбор товаров</a></div>
		<h1 class="center">Документооборот</h1><br>  
		<?require_once($_SERVER["DOCUMENT_ROOT"] . "/personal/sidebar.inc.php");?>
		<div class="right-block">
				<div class="doc-search">
						<form action="" method="get" style="display: inline-block; margin: 0;">
							<input type="text" placeholder="Счет-фактура" name="name" value="<?=htmlspecialcharsbx($_REQUEST["name"])?>">
							<input type="submit" value="Найти">
						</form>
						<form class="add-doc" action="" method="POST" enctype="multipart/form-data">Добавить документ<input id="doc-field" type="file" name="doc" value="Найти"><input style="display: none;" type="submit"></form>
				</div>
				<?

					if (isset($_FILES["doc"]) && $_FILES["doc"]["error"] == 0) {
						$el = new CIBlockElement;

						$PROP = array();
						$PROP["USER"] = $USER->GetID();
						$PROP["FILE_SIZE"] = $_FILES["doc"]["size"];
						$PROP["FILE"] = $_FILES["doc"];

						$arLoadProductArray = Array(
							"MODIFIED_BY"    => $USER->GetID(), 
							"IBLOCK_SECTION_ID" => false,
							"IBLOCK_ID"      => 51,
							"DATE_ACTIVE_FROM" => ConvertTimeStamp(time(), "FULL"),
							"PROPERTY_VALUES"=> $PROP,
							"NAME"           => $_FILES["doc"]["name"],
							"ACTIVE"         => "Y"
						);

						$PRODUCT_ID = $el->Add($arLoadProductArray);
						if ($PRODUCT_ID > 0) {

							$rsUser = CUser::GetByID($USER->GetID());
							$arUser = $rsUser->Fetch();

							if (!empty($arUser["UF_MANAGER"])) {

								$arSelect = Array("PROPERTY_FILE");
								$arFilter = Array("IBLOCK_ID"=>51, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID"=>$PRODUCT_ID);
								$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
								if ($ob = $res->GetNextElement())
								{
									$arFields = $ob->GetFields();

									$rsManager = CUser::GetByID($arUser["UF_MANAGER"]);
									$arManager = $rsManager->Fetch();

									$arEventFields = array(
									    "EMAIL" => $arManager["EMAIL"],
									    "NAME" => $arUser["NAME"] . " " . $arUser["LAST_NAME"]
									);
									CEvent::Send("FILE_TO_MANAGER", "s1", $arEventFields, "Y", "", array($arFields["PROPERTY_FILE_VALUE"]));
								}


							}



							LocalRedirect($APPLICATION->GetCurPageParam("", array()));
							die();
						}
					}

					$remove_file = intval($_REQUEST["remove_file"]);
					if ($remove_file > 0) {
						CIBlockElement::Delete($remove_file);

						LocalRedirect($APPLICATION->GetCurPageParam("", array("remove_file")));
						die();
					}

					global $arFilterDoc;
					$arFilterDoc = array("PROPERTY_USER" => $USER->GetID());

					if (isset($_REQUEST["name"]) && $_REQUEST["name"] != "") {
						$arFilterDoc["NAME"] = "%" . htmlspecialcharsbx($_REQUEST["name"]) . "%";
					}

					$sortBy = htmlspecialcharsbx($_REQUEST["by"]);
					$sortOrder = htmlspecialcharsbx($_REQUEST["order"]);

					if ($sortBy == "")
						$sortBy = "ACTIVE_FROM";

					if ($sortOrder == "")
						$sortOrder = "DESC";
				?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:news.list",
					"docs_list",
					Array(
						"ACTIVE_DATE_FORMAT" => "d.m.Y",
						"ADD_SECTIONS_CHAIN" => "N",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "Y",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"CHECK_DATES" => "Y",
						"COMPOSITE_FRAME_MODE" => "A",
						"COMPOSITE_FRAME_TYPE" => "AUTO",
						"DETAIL_URL" => "",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"DISPLAY_DATE" => "Y",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "N",
						"DISPLAY_PREVIEW_TEXT" => "N",
						"DISPLAY_TOP_PAGER" => "N",
						"FIELD_CODE" => array("", ""),
						"FILTER_NAME" => "arFilterDoc",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"IBLOCK_ID" => "51",
						"IBLOCK_TYPE" => "work_ob",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
						"INCLUDE_SUBSECTIONS" => "N",
						"MESSAGE_404" => "",
						"NEWS_COUNT" => "20",
						"PAGER_BASE_LINK_ENABLE" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => ".default",
						"PAGER_TITLE" => "Документооборот",
						"PARENT_SECTION" => "",
						"PARENT_SECTION_CODE" => "",
						"PREVIEW_TRUNCATE_LEN" => "",
						"PROPERTY_CODE" => array("USER", "FILE"),
						"SET_BROWSER_TITLE" => "N",
						"SET_LAST_MODIFIED" => "N",
						"SET_META_DESCRIPTION" => "N",
						"SET_META_KEYWORDS" => "N",
						"SET_STATUS_404" => "N",
						"SET_TITLE" => "N",
						"SHOW_404" => "N",
						"SORT_BY1" => "{$sortBy}",
						"SORT_BY2" => "SORT",
						"SORT_ORDER1" => "{$sortOrder}",
						"SORT_ORDER2" => "ASC"
					)
				);?>
		</div>
	</div>
</div>	

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>