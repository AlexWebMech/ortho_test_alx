<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
$APPLICATION->SetPageProperty('title', 'Корзина | Интернет-магазин ORTHOBOOM');
$APPLICATION->SetPageProperty('description', 'На данной странице интернет-магазина ORTHOBOOM можно посмотреть все товары в корзине.');

CModule::IncludeModule('catalog');
$hideCoupon = 'Y';
if (isset($_COOKIE['fromlanding'])) {
    $diff = time() - intval($_COOKIE['fromlanding']);
    if ($diff <= 259200) {
        $hideCoupon = 'N'; // 72 часа
        echo '<div class="coupon-message">Введите купон <span class="coupon-code">SL-JXMHI-0QR4Y2G</span> в поле ниже, чтобы получить скидку 25% на ортопедическую обувь!</div>';
    } else {
        CCatalogDiscountCoupon::ClearCoupon();
    }
} else {
    CCatalogDiscountCoupon::ClearCoupon();
}

?>

<?$APPLICATION->IncludeComponent(
    "bitrix:sale.basket.basket",
    "basket_ita_new",
    array(
        "ACTION_VARIABLE" => "action",
        "AUTO_CALCULATION" => "Y",
        "TEMPLATE_THEME" => "blue",
        "COLUMNS_LIST" => array(
            0 => "NAME",
            1 => "DISCOUNT",
            2 => "WEIGHT",
            3 => "DELETE",
            4 => "DELAY",
            5 => "TYPE",
            6 => "PRICE",
            7 => "QUANTITY",
        ),
        "COMPONENT_TEMPLATE" => "basket_new_template",
        "GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
        "GIFTS_CONVERT_CURRENCY" => "Y",
        "GIFTS_HIDE_BLOCK_TITLE" => "N",
        "GIFTS_HIDE_NOT_AVAILABLE" => "N",
        "GIFTS_MESS_BTN_BUY" => "Выбрать",
        "GIFTS_MESS_BTN_DETAIL" => "Подробнее",
        "GIFTS_PAGE_ELEMENT_COUNT" => "4",
        "GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
        "GIFTS_PRODUCT_QUANTITY_VARIABLE" => "",
        "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
        "GIFTS_SHOW_IMAGE" => "Y",
        "GIFTS_SHOW_NAME" => "Y",
        "GIFTS_SHOW_OLD_PRICE" => "Y",
        "GIFTS_TEXT_LABEL_GIFT" => "Подарок",
        "GIFTS_PLACE" => "BOTTOM",
        "HIDE_COUPON" => "N",
        "OFFERS_PROPS" => array(
        ),
        "PATH_TO_ORDER" => "/personal/order/make/",
        "PRICE_VAT_SHOW_VALUE" => "N",
        "QUANTITY_FLOAT" => "N",
        "SET_TITLE" => "Y",
        "USE_GIFTS" => "Y",
        "USE_PREPAYMENT" => "N",
        "DEFERRED_REFRESH" => "N",
        "USE_DYNAMIC_SCROLL" => "Y",
        "SHOW_FILTER" => "N",
        "SHOW_RESTORE" => "Y",
        "COLUMNS_LIST_EXT" => array(
            0 => "PREVIEW_PICTURE",
            1 => "DISCOUNT",
            2 => "DELETE",
            3 => "DELAY",
            4 => "SUM",
            5 => "PROPERTY_OBUV_RAZMER",
        ),
        "COLUMNS_LIST_MOBILE" => array(
            0 => "PREVIEW_PICTURE",
            1 => "DISCOUNT",
            2 => "DELETE",
            3 => "DELAY",
            4 => "SUM",
        ),
        "TOTAL_BLOCK_DISPLAY" => array(
            0 => "bottom",
        ),
        "DISPLAY_MODE" => "extended",
        "PRICE_DISPLAY_MODE" => "Y",
        "SHOW_DISCOUNT_PERCENT" => "Y",
        "DISCOUNT_PERCENT_POSITION" => "bottom-right",
        "PRODUCT_BLOCKS_ORDER" => "props,sku,columns",
        "USE_PRICE_ANIMATION" => "Y",
        "LABEL_PROP" => array(
        ),
        "CORRECT_RATIO" => "Y",
        "COMPATIBLE_MODE" => "Y",
        "EMPTY_BASKET_HINT_PATH" => "/catalog2/",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "ADDITIONAL_PICT_PROP_5" => "-",
        "ADDITIONAL_PICT_PROP_11" => "-",
        "ADDITIONAL_PICT_PROP_94" => "-",
        "ADDITIONAL_PICT_PROP_95" => "-",
        "BASKET_IMAGES_SCALING" => "adaptive",
        "USE_ENHANCED_ECOMMERCE" => "N"
    ),
    false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>