<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>

<div class="sidebar">
	<?
		$menuType = "personal_f";
		$personType = getPersonType(); 
		if ($personType == "U")
			$menuType = "personal_u";
	?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:menu", 
		"personal_menu", 
		array(
			"ROOT_MENU_TYPE" => "{$menuType}",
			"MAX_LEVEL" => "1",
			"CHILD_MENU_TYPE" => "top",
			"USE_EXT" => "Y",
			"DELAY" => "N",
			"ALLOW_MULTI_SELECT" => "N",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => array(
			),
			"COMPONENT_TEMPLATE" => "personal_menu",
			"COMPOSITE_FRAME_MODE" => "A",
			"COMPOSITE_FRAME_TYPE" => "AUTO"
		),
		false
	);?>	
<?if ($personType == "U"):?>
<script>
$('.sidebar ul.active li').last().css('height', '70px');
</script>
<?endif;?>
	<?
	global $USER;
	$userIsManager = in_array(7, $USER->GetUserGroupArray()); // 7 - менеджеры юрлиц
	if (getPersonType() == "U" || $userIsManager):
		if ($userIsManager) { 
			$title = 'Чат<br> с клиентами';
			$active = '';
			$statusDiv = '';
		} else {
			$title = 'Чат<br> с персональным<br> менеджером';
			if (CUser::IsOnLine(GetUserManager($USER->GetID()))) {
				$active = 'active';
				$status = 'В сети';
			} else {
				$active = '';
				$status = 'Не в сети';
			}
			$statusDiv = '<div class="online-status">
							<ul><li>' . $status . '</li></ul>
						  </div>';
		}
		
	?>
		<a href="/personal/chat/">
			<div class="chat <?echo $active?>">
				<div class="chat-img">
					<img src="<?=SITE_TEMPLATE_PATH?>/images/consultant.png">
					<?echo $statusDiv?>							
				</div>
				<div class="chat-title">
					<?echo $title?>		
				</div>
			</div>
		</a>
	<?endif;?>
</div>