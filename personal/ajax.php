<?
define("STOP_STATISTICS", true);

include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

// if (!$USER->IsAuthorized())
// 	die();



$act = htmlspecialcharsbx($_REQUEST["act"]);

$wishlist = array();
$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();
if (count($arUser["UF_WISHLIST"]) > 0) {
	$wishlist = $arUser["UF_WISHLIST"];
}

function wishUpdate($wishlist) {
	CModule::IncludeModule("iblock");
	global $USER;
	$usr = new CUser;
	$fields = Array( 
		"UF_WISHLIST" => $wishlist, 
		"STOP_CHECK" => true
	); 
	return $usr->Update($USER->GetID(), $fields);
}

function wishDelete($product_id, $wishlist) {
	$key = array_search($product_id, $wishlist);
	if ($key !== false) {
		unset($wishlist[$key]);
		sort($wishlist);
	}

	$upd = (wishUpdate($wishlist)) ? "Y" : "N";
	echo '{"delete":"'.$upd.'"}';
}


function wishDeleteAll() {
	$upd = (wishUpdate(array())) ? "Y" : "N";
	echo '{"deleteAll":"'.$upd.'"}';
}

function wishAdd($product_id, $wishlist) {
	if (!in_array($product_id, $wishlist)) {
	    $wishlist[] = $product_id;
	}

	$upd = (wishUpdate($wishlist)) ? "Y" : "N";
	echo '{"add":"'.$upd.'"}';
}

switch($act) {
	case "delete":
		$product_id = intval($_REQUEST["product_id"]);
		wishDelete($product_id, $wishlist);
	break;

	case "add":
		$product_id = intval($_REQUEST["product_id"]);
		wishAdd($product_id, $wishlist);
	break;

	case "delete_all":
		wishDeleteAll();
	break;
}
?>