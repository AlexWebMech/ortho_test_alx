<br><h4>Delivery for wholesale buyers</h4>
<ul style="text-align:justify;">
	<li>Following registration of the order through the site, an operator will contact you regarding a more precise date, time and place for delivery.</li>
	<li>The cost of delivery depends upon the size of the order.</li>
	<li>The delivery period depends on the location of the customer and the choice of transport company.</li>
</ul>