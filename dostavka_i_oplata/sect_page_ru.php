<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle("Оплата и доставка");
$APPLICATION->SetPageProperty('title', 'Оплата и доставка - ORTHOBOOM');
$APPLICATION->SetPageProperty('description', 'Оплата и доставка. «ORTHOBOOM» - правильная обувь');
?> 
		<div class="content">
			<div class="container">
				<h1 class="center">Оплата и доставка</h1>
				<h2 class="center">Оплата</h2>
				<div class="span6 oplata1">
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "oplata1_ru",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>	
				</div>
				<div class="span6 oplata2">
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "oplata2_ru",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>	
				</div>
				<div class="clearfix"></div>
				<h2 class="center" style="margin: 0 auto;">Доставка</h2>
				<div class="clearfix"></div>
				<div class="span6 dostavka2">
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "oplata4_ru",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>						
				</div>
				<div class="span6 dostavka1">
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "oplata3_ru",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>	
					
				</div>
				<div class="span6 oplata2">
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "oplata6_ru",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>	
				</div>
				<div class="span6 dostavka3">
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "oplata7_ru",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>	
				</div>
				<div class="clearfix"></div>			<br /><br />
<div style="font-size:10px;">
<p style="font-weight:bold">ОПИСАНИЕ ПРОЦЕССА ПЕРЕДАЧИ ДАННЫХ</p>
<p>Для оплаты (ввода реквизитов Вашей карты) Вы будете перенаправлены на платежный шлюз ПАО СБЕРБАНК. Соединение с платежным шлюзом и передача информации осуществляется в защищенном режиме с использованием протокола шифрования SSL. В случае если Ваш банк поддерживает технологию безопасного проведения интернет-платежей Verified By Visa или MasterCard SecureCode для проведения платежа также может потребоваться ввод специального пароля.</p>
<p>Настоящий сайт поддерживает 256-битное шифрование. Конфиденциальность сообщаемой персональной информации обеспечивается ПАО СБЕРБАНК. Введенная информация не будет предоставлена третьим лицам за исключением случаев, предусмотренных законодательством РФ. Проведение платежей по банковским картам осуществляется в строгом соответствии с требованиями платежных систем МИР, Visa Int. и MasterCard Europe Sprl.</p>
</div>
			</div>
		</div>
