<h4>Collection</h4>
<p style="text-align:justify;">
</p>
<ul>
	<li>Hand-picked goods can be collected, when available, from any Julianna orthopaedic centre that is convenient to you.</li>
	<li>Following registration of the order through our website, an operator will contact you regarding a more precise date, time and place for delivery.</li>
	<li>Payment upon receipt.</li>
	<li>Reservation period is <b>1-2 working days</b>. Following the order will be cancelled.</li>
</ul>
<p>
</p>