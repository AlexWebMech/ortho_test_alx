<h4>Courier delivery</h4>
<p style="text-align:justify;">
</p>
<ul>
	<li>Courier delivery is available throughout Nizhny Novgorod.</li>
	<li>Following registration of the order through the site, an operator will contact you regarding a more precise date, time and place for delivery.</li>
	<li>Delivery is free for orders amounting to more than <b>2000 roubles</b>.</li>
	<li>Delivery period is <b>1-2 working days</b>.</li>
	<li>If you refuse the order at the time of its receipt, the costs for transport and courier delivery must be paid at a rate of <b>200 roubles</b>.</li>
</ul>
 <br>
<p>
</p>