<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Информация об условиях доставки и оплаты в интернет-магазине ORTHOBOOM.");
$APPLICATION->SetPageProperty("title", "Доставка и оплата | Интернет-магазин ORTHOBOOM");
$APPLICATION->SetTitle("Доставка и оплата");

$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "page_".GetLang(),
		"EDIT_TEMPLATE" => ""
	)
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>