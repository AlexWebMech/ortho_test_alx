<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Здоровье мальчиков");
?><p>
	 Сила – одно из важных физических качеств мужчины. Сильный мужчина ассоциируется с успешностью, здоровьем и уверенностью. От правильного развития опорно-двигательного аппарата зависит, насколько сильным будет будущий защитник.
</p>
<p>
	 На что обратить внимание?
</p>
<p>
	 В первую очередь, нужно позаботиться о правильном развитии стопы и позвоночника.
</p>
<p>
	 Известно, что с тяжелой степенью плоскостопия мальчиков не берут в армию. Неправильное развитие стопы дает дополнительную нагрузку на мышцы и суставы ног, позвоночник. Молодой человек быстрее устает, становится менее выносливым, не может поднимать тяжести и носить обычную обувь. Появляются боли в спине, ногах, часто болит голова. Это способствует развитию сколиоза, остеохондроза, появлению лишнего веса. У ребенка может появится косолапая, переваливающаяся походка. Искривление позвоночника может привести и к неправильной работе внутренних органов. Уж какая тут сила и мужественность?
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>