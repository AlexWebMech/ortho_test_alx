<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle("Payment and delivery");
$APPLICATION->SetPageProperty('title', 'Payment and delivery - ORTHOBOOM');
$APPLICATION->SetPageProperty('description', 'Payment and delivery. �ORTHOBOOM� - real shoes');
?> 
		<div class="content">
			<div class="container">
				<h1 class="center">Payment and delivery</h1>
				<h2 class="center">Payment</h2>
				<div class="span6 oplata1">
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "oplata1_en",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>	
				</div>
				<div class="span6 oplata2">
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "oplata2_en",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>	
				</div>
				<div class="clearfix"></div>
				<h2 class="center">Delivery</h2>
				<div class="clearfix"></div>
				<div class="span6 dostavka2">
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "oplata4_en",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>						
				</div>
				<div class="span6 dostavka1">
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "oplata3_en",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>	
					
				</div>
				<div class="span12 dostavka3">
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "oplata5_en",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>	
					
				</div>
				<div class="clearfix"></div>			<br /><br />
			</div>
		</div>
		
