<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как подобрать размер");
$APPLICATION->SetPageProperty('title', 'Как подобрать размер | Интернет-магазин ORTHOBOOM');
$APPLICATION->SetPageProperty('description', 'На данной странице интернет-магазина ORTHOBOOM представлена информация о том, как правильно подобрать размер обуви.');

?>

    <div class="content">
        <h1 class="center">Как подобрать размер</h1>
        <?
        $APPLICATION->IncludeFile(
            SITE_DIR."include/".LANGUAGE_ID."/catalog_element/razmer_inc.php",
            array(),
            array(
                "MODE"      => "html",
                "NAME"      => "Редактирование включаемой области раздела",
                "TEMPLATE"  => ""
            )
        );
        ?>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>