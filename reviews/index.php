<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отзывы");
$APPLICATION->SetPageProperty('title', 'Отзывы | Интернет-магазин ORTHOBOOM');
$APPLICATION->SetPageProperty('description', 'В данном разделе можно оставить отзыв об обуви ORTHOBOOM и работе интернет-магазина.
');

?> 
	<div class="content" style="background: #ddedca; margin-bottom: -20px">
		<div class="container">
			<h1 class="center">Отзывы</h1>
			<div class="span8">	
				<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"reviews", 
	array(
		"COMPONENT_TEMPLATE" => "reviews",
		"IBLOCK_TYPE" => "1",
		"IBLOCK_ID" => "9",
		"NEWS_COUNT" => "5",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "DATE_ACTIVE_FROM",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "EMAIL",
			1 => "REVIEW",
			2 => "NAME",
			3 => "PHONE",
			4 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => "modern",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SET_LAST_MODIFIED" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"STRICT_SECTION_CHECK" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
<br><br>
			</div>
			<div class="span4">	
<?$APPLICATION->IncludeComponent(
	"altasib:feedback.form", 
	"theme1", 
	array(
		"IBLOCK_TYPE" => "1",
		"IBLOCK_ID" => "9",
		"FORM_ID" => "12",
		"EVENT_TYPE" => "ALX_FEEDBACK_FORM",
		"PROPERTY_FIELDS" => array(
			0 => "EMAIL",
			1 => "REVIEW",
			2 => "NAME",
			3 => "PHONE",
		),
		"PROPERTY_FIELDS_REQUIRED" => array(
			0 => "EMAIL",
			1 => "REVIEW",
			2 => "PHONE",
		),
		"NAME_ELEMENT" => "NAME",
		"BBC_MAIL" => "feedback@julianna.ru",
		"MESSAGE_OK" => "Отзыв отправлен!",
		"CHECK_ERROR" => "Y",
		"ACTIVE_ELEMENT" => "N",
		"USE_CAPTCHA" => "Y",
		"SEND_MAIL" => "N",
		"HIDE_FORM" => "Y",
		"USERMAIL_FROM" => "N",
		"SHOW_MESSAGE_LINK" => "Y",
		"REWIND_FORM" => "N",
		"WIDTH_FORM" => "100%",
		"SIZE_NAME" => "14px",
		"COLOR_NAME" => "#000000",
		"SIZE_HINT" => "10px",
		"COLOR_HINT" => "#000000",
		"SIZE_INPUT" => "14px",
		"COLOR_INPUT" => "#727272",
		"BACKCOLOR_ERROR" => "#ffffff",
		"COLOR_ERROR_TITLE" => "#A90000",
		"COLOR_ERROR" => "#8E8E8E",
		"IMG_ERROR" => "/upload/altasib.feedback.gif",
		"BORDER_RADIUS" => "3px",
		"COLOR_MESS_OK" => "#963258",
		"IMG_OK" => "/upload/altasib.feedback.ok.gif",
		"CATEGORY_SELECT_NAME" => "Выберите категорию",
		"SECTION_MAIL_ALL" => "rusak.ai@julianna.ru",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"ALX_CHECK_NAME_LINK" => "N",
		"CAPTCHA_TYPE" => "default",
		"JQUERY_EN" => "N",
		"SECTION_MAIL9" => "",
		"SECTION_MAIL11" => "",
		"SECTION_MAIL10" => "",
		"SECTION_MAIL7" => "",
		"SECTION_MAIL8" => "",
		"COMPONENT_TEMPLATE" => "theme1",
		"RECAPTCHA_THEME" => "light",
		"RECAPTCHA_TYPE" => "image",
		"PROPS_AUTOCOMPLETE_NAME" => array(
		),
		"PROPS_AUTOCOMPLETE_EMAIL" => array(
			0 => "EMAIL",
		),
		"PROPS_AUTOCOMPLETE_PERSONAL_PHONE" => array(
			0 => "PHONE",
		),
		"MASKED_INPUT_PHONE" => array(
			0 => "PHONE",
		),
		"LOCAL_REDIRECT_ENABLE" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"ADD_LEAD" => "N",
		"NOT_CAPTCHA_AUTH" => "Y",
		"FB_TEXT_SOURCE" => "PREVIEW_TEXT",
		"FB_TEXT_NAME" => "",
		"ALX_LINK_POPUP" => "N",
		"SECTION_FIELDS_ENABLE" => "N",
		"PROPS_AUTOCOMPLETE_VETO" => "N",
		"INPUT_APPEARENCE" => array(
			0 => "DEFAULT",
		),
		"CHECKBOX_TYPE" => "CHECKBOX",
		"COLOR_SCHEME" => "BRIGHT",
		"COLOR_THEME" => "",
		"SHOW_LINK_TO_SEND_MORE" => "Y",
		"LINK_SEND_MORE_TEXT" => "Отправить ещё одно сообщение",
		"SEND_IMMEDIATE" => "Y",
		"ADD_HREF_LINK" => "Y",
		"COLOR_OTHER" => "#009688",
		"AGREEMENT" => "N",
		"CHANGE_CAPTCHA" => "Y",
		"LOCAL_REDIRECT_URL" => "/reviews/"
	),
	false
);?>
</div>	
			<div class="clearfix">	</div>	
		</div>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>