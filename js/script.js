$(document).ready(function() {

    if($(".fancybox").length){
        $(".fancybox").fancybox({
            padding:0,
            wrapCSS: 'fanc_cont',

            helpers: {
                overlay: {
                    locked: false // отключаем блокировку overlay
                }
            }
        });
    }
				
				
				var arr = window.location.href.split('program_redirect=');
				if (arr.length == 1) {
					$('#shop_search_form select option').each(function(){
						if (this.text == $('span .notetext').text()) {
							if (this.selected != true)
								window.location.href = 'https://orthoboom.ru/contact/?arrFilter_pf%5BCITY%5D%5B%5D=' + this.value + '&set_filter=%CF%EE%EA%E0%E7%E0%F2%FC&set_filter=Y&program_redirect=true'
						}
					});
				}
			
				$('#modal-mini .city span').click(function(){
					$('header .city span').text($(this).text());
					$('#modal-mini .city span').removeClass('currentCity');
					$(this).addClass('currentCity');
					$currentCity = $('.currentCity').text();
					document.cookie='city='+$currentCity; 
					$('#modal-mini').removeClass('in').hide();
					$('.modal-backdrop').remove();
					$('#shop_search_form select option').each(function(){
						if (this.text == $('header .city span').text())
							window.location.href = 'https://orthoboom.ru/contact/?arrFilter_pf%5BCITY%5D%5B%5D=' + this.value + '&set_filter=%CF%EE%EA%E0%E7%E0%F2%FC&set_filter=Y&program_redirect=true'
					});
				});


    if($(".bx_bigimages a").length){
        $(".bx_bigimages a").lightBox();
    }

				/*-----Clickable Table on page Сотрудники------*/
				jQuery( function($) {
					$('tbody tr[data-href]').addClass('clickable').click( function() {
							window.location = $(this).attr('data-href');
					});
				});
				/*-----------End Clickable Table---------------*/
				
				//$('optionsDivVisible.drop-undefined').css('left', '125' + 'px');

    if($(".diop_address").length){
        $('.diop_address').matchHeight();
    }
							 
			});

			$('.catalog .span3').hover(function(){
					$(this).find('img').css({'opacity':'0.3','transition':'0.1s'});
					$href = $(this).find('a').attr('href');
					// $(this).click(function(){ 
					// 	window.location.href=$href;
					// });
					$(this).append('<img src="/images/plus.png" class="plus">');
					$(this).find('.item-name a').css({'color':'#F86B73','borderColor':'#F86B73'});
				},
				function(){
					$(this).find('img').css({'opacity':'1'});	
					$('.plus').remove();
					$(this).find('.item-name a').css({'color':'#0C6695','borderColor':'#BBCBE8'});
			});

				$('#vopros').click(function(){
					if ($('.form1').attr('style') != 'display: block;') {
						$('.form2').slideToggle(500);
					}
					else {
						$('.form1').hide();
						$('.form2').fadeIn(500);						
					}
					$('#vopros').addClass('active');
					$('#zapis').removeClass('active');
				});
				$('#zapis').click(function(){
					if ($('.form2').attr('style') != 'display: block;') {
						$('.form1').slideToggle(500);
					}
					else {
						$('.form2').hide();
						$('.form1').fadeIn(500);						
					}
					$('#zapis').addClass('active');
					$('#vopros').removeClass('active');
				});	
	
				$nameoftovar = $('.item_info_section dl dd:nth-child(2)').text();
				$('#ARTICUL_FID51').val($nameoftovar);


/* non-xhr loadings */
function showWait(node, msg)
{
    node = BX(node) || document.body || document.documentElement;
    msg = msg || BX.message('JS_CORE_LOADING');

    var container_id = node.id || Math.random();

    var width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth;

    var height = window.innerHeight
        || document.documentElement.clientHeight
        || document.body.clientHeight;

    var obMsg = node.bxmsg = document.body.appendChild(BX.create('DIV', {
        props: {
            id: 'wait_' + container_id
        },
        style: {
            background: 'url("/images/loading2.gif") no-repeat scroll center center #999999',
            color: 'black',
            fontFamily: 'Verdana,Arial,sans-serif',
            fontSize: '11px',
            height: height + 'px',
            width:  width+ 'px',
            margin: 'auto',
            opacity: '.5',
            width: '100%',
            position: 'fixed',
            zIndex:'10000',
            textAlign:'center'
        },
        // text: msg
    }));

    // setTimeout(BX.delegate(_adjustWait, node), 10);

    return container_id;
};

function closeWait(container_id)
{
    var node =  document.body || document.documentElement;
    var obMsg = BX('wait_' + container_id);

    if (obMsg && obMsg.parentNode)
    {
        obMsg.parentNode.removeChild(obMsg);
        if (node) node.bxmsg = null;
        BX.cleanNode(obMsg, true);
    }
};




	