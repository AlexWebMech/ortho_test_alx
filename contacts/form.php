<?
define("LOCAL_TEMPLATE_NO_TITLE", "N");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>

<?
$SAVE_MASK1 = Array(
	"NAME" => date("Y-m-d H:i:s ") . (($_POST["PROPERTY_NAME"]) ? " - " . $_POST["PROPERTY_NAME"] : "") . " - Обратный звонок",
	"PROPERTY_USER_ID" => $USER->GetID(),
);
?>

<?
$APPLICATION->IncludeComponent("local:iblock.element.save", "feedback", Array(
	"IBLOCK_TYPE" => "info",
	"IBLOCK_ID" => 109,
	"ITEM_ID" => "",
	"ITEM_SAVE" => $_POST,
	"PROPERTIES_FORMAT" => "SHORT",
	"DISPLAY_FORM" => "Y",
	"ITEM_SAVE_MASK" => $SAVE_MASK1,
	
	"CHECK_KEYWORD_FLAG" => "Y",
	"CHECK_KEYWORD_REQUEST" => $_REQUEST["form_check"],
	"CHECK_KEYWORD" => "ok",
	
	"ERROR_MESSAGES" => $ERRORS,
	
	"SUCCESS_MESSAGE" => "Ваш вопрос успешно отправлен. <br/>Мы ответим вам в ближайшее время.",
	//"SUCCESS_URL" => "/form/success.php",
	
	"EMAIL_EVENT_FLAG" => "Y",
	"EMAIL_EVENT_TYPE" => "MESSAGE",
	"EMAIL_EVENT_TITLE" => "Заказ обратного звонка",
	"EMAIL_EVENT_EMAIL_TO" => "zakaz@orthoboom.ru",
	//"EMAIL_EVENT_EMAIL_FROM" => "",
	//"EMAIL_EVENT_EMAIL_TO" => "",
	
	"WRAP_ID" => "feedback-form",
));
?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>