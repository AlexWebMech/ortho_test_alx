<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

?><?

if ($_REQUEST["ELEMENT_ID"])
{
	$APPLICATION->IncludeComponent("local:wishlist.update", "json", Array(
		"COMMAND" => $_REQUEST["COMMAND"] ? $_REQUEST["COMMAND"] : "TOGGLE",
		"ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],
	));
}
?>
