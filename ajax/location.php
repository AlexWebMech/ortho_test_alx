<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Local\Location::init();
if ($_REQUEST["action"] == "set_location")
{
	if ($_REQUEST["LOCATION_ID"])
		\Local\Location::setLocationId($_REQUEST["LOCATION_ID"]);
}
elseif ($_REQUEST["action"] == "get_modal")
{
	?>

	<div class="location-modal ui-modal ui-modal--normal hide">
		<div class="ui-modal-overlay ui-overlay"></div>
		<div class="ui-modal-inner ui-inner">
			<div class="ui-modal-header">
				<div class="ui-modal-title"><img src="<?= SITE_TEMPLATE_PATH ?>/img/site-location-modal-header-icon.png" width="154" height="35"></div>
				<span class="ui-modal-close"><span class="ui-modal-close-icon ui-icon"></span></span>
			</div>
			<div class="ui-modal-content ui-modal-main">
					
				<div class="location-modal-title">
				<? foreach(\Local\Location::getLocationPath() as $iDepthLevel => $arSubLocation) : 
					if ($iDepthLevel == 2)
						continue; ?>
					<?= $arSubLocation["TITLE"]; ?>, 
				<? endforeach; ?>
				<?= \Local\Location::getLocationTitle(); ?>
				</div>
				
				<form class="location-form">
					
					<? $APPLICATION->IncludeComponent(
					"bitrix:sale.location.selector.search",
					"",
					Array(
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"CODE" => "",
						//"ID" => \Local\Location::getLocationId(),
						"FILTER_BY_SITE" => "N",
						"INITIALIZE_BY_GLOBAL_EVENT" => "",
						"INPUT_NAME" => "LOCATION_ID",
						"JS_CALLBACK" => "localLocationSet",
						"JS_CONTROL_GLOBAL_ID" => "",
						"PROVIDE_LINK_BY" => "id",
						"SHOW_DEFAULT_LOCATIONS" => "N",
						"SUPPRESS_ERRORS" => "N",
					), true);?>		
					
					<?
					$arResult["LOCATIONS"] = [];
					$obRes = \Bitrix\Sale\Location\DefaultSiteTable::getList([
						"filter" => ["SITE_ID" => SITE_ID],
						"order" => ["SORT" => "ASC"],
					]);
					while($a = $obRes->fetch())
						$arResult["LOCATIONS"][$a["LOCATION_CODE"]] = [];

					//$arResult["LOCATIONS"] = [];
					$obRes = \Bitrix\Sale\Location\LocationTable::getList([
						"filter" => ["CODE" => array_keys($arResult["LOCATIONS"])],
						"select" => ["ID", "CODE", "NAME_NAME" => "NAME.NAME"],
						"limit" => 15,
						"cache" => ["ttl" => 3600],
					]);
					while($a = $obRes->fetch())
						$arResult["LOCATIONS"][$a["CODE"]] = $a;
					
					
					?>
					
					<div class="location-modal-list">
						<div class="location-modal-items">
							<? foreach($arResult["LOCATIONS"] as $arLocation) : ?>
								<div class="location-modal-item">
									<a href="#" data-id="<?= $arLocation["ID"] ?>"><?= $arLocation["NAME_NAME"] ?></a>
								</div>
							<? endforeach; ?>
						</div>
					</div>
					
					<script type="text/javascript">
					function localLocationSet()
					{
						$.ajax({
							url: "/ajax/location.php?action=set_location",
							method: "post",
							data: $(".location-form").serialize(),
							success: function (html) {
								$(".location-modal").uniModal("hide");
								//window.location.reload(); //window.location.href = window.location.href;

								uniAjaxLib.ajax({
									url: "/",
									wrapTarget: ".header-location",
									cut: true,
								});
								setTimeout(function () {
									$(".location-modal").remove();
								}, 2000);
							},
						});
					}
					$(".location-modal-item a").on("click", function () {
						$.ajax({
							url: "/ajax/location.php?action=set_location",
							data: "LOCATION_ID=" + $(this).data("id"),
							method: "post",
							success: function (html) {
								$(".location-modal").uniModal("hide");
								//window.location.reload(); //window.location.href = window.location.href;
								
								uniAjaxLib.ajax({
									url: "/",
									wrapTarget: ".header-location",
									cut: true,
								});
								setTimeout(function () {
									$(".location-modal").remove();
								}, 2000);								
							},						
						});
						return false;
					});
					</script>
				
				</form>

			</div>
		</div>
		
	</div>	
	
	<script>
	$(".location-modal").uniModal({});
	$(".location-modal").uniModal("show");
	</script>	
	<?
}

?>