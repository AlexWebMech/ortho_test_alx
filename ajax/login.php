<?require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");?>
<?
if(!check_bitrix_sessid()){
    die();
}
use Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();

$login = $request->getPost("USER_LOGIN");
$password = $request->getPost("USER_PASSWORD");
global $USER;

$result = $USER->Login($login, $password, "Y");
echo \Bitrix\Main\Web\Json::encode($result);
