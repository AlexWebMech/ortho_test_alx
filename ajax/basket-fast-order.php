<?
//bitrix uses
use Bitrix\Main,
    Bitrix\Main\Localization\Loc as Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale\PersonType,
    Bitrix\Sale,
    Bitrix\Sale\Order,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Main\Context;

//increase productivity
define("STOP_STATISTICS", true);
define("NO_AGENT_CHECK", true);

//load bitrix core
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

//langs
\Bitrix\Main\Localization\Loc::loadMessages(dirname(__FILE__)."/ajax.php");

//application
$application = \Bitrix\Main\Application::getInstance();

//context
$context = $application->getContext();

//get request
$request = $context->getRequest();

//get request vars
$actionType = $request->getPost("actionType");
$siteId = $request->getPost("siteId");

//get basket vars
$personTypeId = $request->getPost("personTypeId");
$paysystemId = $request->getPost("paysystemId");
$deliveryId = $request->getPost("deliveryId");
$locationId = $request->getPost("locationId");
$extraServices = $request->getPost("extraServices");

//check siteId from get request
if(empty($siteId)){
    $siteId = $request->getQuery("siteId");
}

//check actionType from get request
if(empty($actionType)){
    $actionType = $request->getQuery("actionType");
}

//check request act
if(!empty($actionType)){

    //get fast order component
    if($actionType == "getFastBasketWindow"){

        //get masked params
        $maskedUse = $request->getQuery("maskedUse");
        $maskedFormat = $request->getQuery("maskedFormat");
        $buttonFromOrderPage = $request->getQuery("buttonFromOrderPage");

        //push component html
        $APPLICATION->IncludeComponent(
            "ita:basket.fast.order",
            ".default",
            array(
                "SITE_ID" => empty($siteId) ? $siteId : $context->getSite(),
                "USE_MASKED" => !empty($maskedUse) ? $maskedUse : "N",
                "MASKED_FORMAT" => !empty($maskedFormat) ? $maskedFormat : "",
                "BUTTON_FROM_ORDER_PAGE" => !empty($buttonFromOrderPage) ? $buttonFromOrderPage : "",
            ),
            false,
            Array(
                //hide hermitage actions
                "HIDE_ICONS" => "Y"
            )
        );

    }

}

//gifts include
function getGiftsComponent($arParams, $appliedDiscount = array(), $fullDiscount = array()){

    //globals
    global $APPLICATION;

    //vars
    $componentHTML = "";

    //start buffering
    ob_start();

    //push component
    $APPLICATION->IncludeComponent("bitrix:sale.gift.basket", ".default", array(
        "HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
        "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
        "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
        "PRODUCT_PRICE_CODE" => $arParams["PRODUCT_PRICE_CODE"],
        "APPLIED_DISCOUNT_LIST" => $appliedDiscount,
        "FULL_DISCOUNT_LIST" => $fullDiscount,
        "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
        "HIDE_MEASURES" => $arParams["HIDE_MEASURES"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "CURRENCY_ID" => $arParams["CURRENCY_ID"]
    ),
        false
    );

    //save buffer
    $componentHTML = ob_get_contents();

    //clean buffer
    ob_end_clean();

    return $componentHTML;

}
?>