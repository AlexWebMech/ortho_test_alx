<?
define('BX_SESSION_ID_CHANGE', false);
define('BX_SKIP_POST_UNQUOTE', true);
define('NO_AGENT_CHECK', true);
define("STATISTIC_SKIP_ACTIVITY_CHECK", true);

//    file_put_contents('/home/bitrix/www/tools/info.log', print_r(date("Y-m-d H:i:s"),true), FILE_APPEND | LOCK_EX);
//    file_put_contents('/home/bitrix/www/tools/info.log', print_r("\r\n",true), FILE_APPEND | LOCK_EX);
//    file_put_contents('/home/bitrix/www/tools/info.log', print_r("1c_exchange_1/1c_exchange.php",true), FILE_APPEND | LOCK_EX);
//    file_put_contents('/home/bitrix/www/tools/info.log', print_r("\r\n",true), FILE_APPEND | LOCK_EX);
//
//if(isset($_REQUEST)){
//    file_put_contents('/home/bitrix/www/tools/info.log', print_r($_REQUEST,true), FILE_APPEND | LOCK_EX);
//}
//if(isset($_SERVER['REQUEST_URI'])){
//
//    file_put_contents('/home/bitrix/www/tools/info.log', print_r($_SERVER['REQUEST_URI'],true), FILE_APPEND | LOCK_EX);
//    file_put_contents('/home/bitrix/www/tools/info.log', print_r("\r\n",true), FILE_APPEND | LOCK_EX);
//    file_put_contents('/home/bitrix/www/tools/info.log', print_r($_FILES,true), FILE_APPEND | LOCK_EX);
//    file_put_contents('/home/bitrix/www/tools/info.log', print_r("\r\n",true), FILE_APPEND | LOCK_EX);
//
////    file_put_contents('D:\OpenServer\domains\orthoboom.loc\www\tools\info.log', print_r(date("Y-m-d H:i:s"),true), FILE_APPEND | LOCK_EX);
////    file_put_contents('D:\OpenServer\domains\orthoboom.loc\www\tools\info.log', print_r("\r\n",true), FILE_APPEND | LOCK_EX);
////    file_put_contents('D:\OpenServer\domains\orthoboom.loc\www\tools\info.log', print_r($_SERVER['REQUEST_URI'],true), FILE_APPEND | LOCK_EX);
////    file_put_contents('D:\OpenServer\domains\orthoboom.loc\www\tools\info.log', print_r("\r\n",true), FILE_APPEND | LOCK_EX);
//
//    if( $_FILES['file']['name'] != "" )
//    {
//        copy ( $_FILES['file']['tmp_name'], "/home/bitrix/www/tools/" . $_FILES['file']['name'] );
////        copy ( $_FILES['file']['tmp_name'], 'D:\OpenServer\domains\orthoboom.loc\www\tools' ."\/" . $_FILES['file']['name'] );
//    }
//}

if (isset($_REQUEST["type"]) && $_REQUEST["type"] == "crm")
{
    define("ADMIN_SECTION", true);
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if($type=="catalog")
{
    // папка для хранения файлов импорта
    $_SESSION["BX_CML2_IMPORT"]["TEMP_DIR"] = $_SERVER["DOCUMENT_ROOT"]."/".COption::GetOptionString("main", "upload_dir", "upload")."/1c_catalog_1/";

    // принудительный склад - если указан, то все остатки не по складам идут в этот склад
    $_SESSION["BX_CML2_IMPORT_CUSTOM"]["FORCED_WAREHOUSE"] = "1c_catalog_1";

    // внешний код базовой цены - если он и код ниже указаны, то при импорте цен базовая цена при её отсутствии заполняется из второй цены
    $_SESSION["BX_CML2_IMPORT_CUSTOM"]["BASE_PRICE"] = "40ff571d-9ecb-11e8-93f3-d850e6e3b156";
    // вторая цена - заполняет базовую в её отстутствие
    $_SESSION["BX_CML2_IMPORT_CUSTOM"]["SECONDARY_PRICE"] = "eb101f8b-3121-11e9-80cf-d850e6e3b156";

    $APPLICATION->IncludeComponent("o2k:catalog.import.1c", "", Array(
            "IBLOCK_TYPE" => COption::GetOptionString("catalog", "1C_IBLOCK_TYPE", "-"),
            "SITE_LIST" => array(COption::GetOptionString("catalog", "1C_SITE_LIST", "-")),
            "INTERVAL" => COption::GetOptionString("catalog", "1C_INTERVAL", "-"),
            "GROUP_PERMISSIONS" => explode(",", COption::GetOptionString("catalog", "1C_GROUP_PERMISSIONS", "1")),
            "GENERATE_PREVIEW" => COption::GetOptionString("catalog", "1C_GENERATE_PREVIEW", "Y"),
            "PREVIEW_WIDTH" => COption::GetOptionString("catalog", "1C_PREVIEW_WIDTH", "100"),
            "PREVIEW_HEIGHT" => COption::GetOptionString("catalog", "1C_PREVIEW_HEIGHT", "100"),
            "DETAIL_RESIZE" => COption::GetOptionString("catalog", "1C_DETAIL_RESIZE", "Y"),
            "DETAIL_WIDTH" => COption::GetOptionString("catalog", "1C_DETAIL_WIDTH", "300"),
            "DETAIL_HEIGHT" => COption::GetOptionString("catalog", "1C_DETAIL_HEIGHT", "300"),
            "ELEMENT_ACTION" => COption::GetOptionString("catalog", "1C_ELEMENT_ACTION", "D"),
            "SECTION_ACTION" => COption::GetOptionString("catalog", "1C_SECTION_ACTION", "D"),
            "FILE_SIZE_LIMIT" => COption::GetOptionString("catalog", "1C_FILE_SIZE_LIMIT", 200*1024),
            "USE_CRC" => COption::GetOptionString("catalog", "1C_USE_CRC", "Y"),
            "USE_ZIP" => COption::GetOptionString("catalog", "1C_USE_ZIP", "Y"),
            "USE_OFFERS" => COption::GetOptionString("catalog", "1C_USE_OFFERS", "N"),
            "FORCE_OFFERS" => COption::GetOptionString("catalog", "1C_FORCE_OFFERS", "N"),
            "USE_IBLOCK_TYPE_ID" => COption::GetOptionString("catalog", "1C_USE_IBLOCK_TYPE_ID", "N"),
            "USE_IBLOCK_PICTURE_SETTINGS" => COption::GetOptionString("catalog", "1C_USE_IBLOCK_PICTURE_SETTINGS", "N"),
            "TRANSLIT_ON_ADD" => COption::GetOptionString("catalog", "1C_TRANSLIT_ON_ADD", "Y"),
            "TRANSLIT_ON_UPDATE" => COption::GetOptionString("catalog", "1C_TRANSLIT_ON_UPDATE", "Y"),
            "TRANSLIT_REPLACE_CHAR" => COption::GetOptionString("catalog", "1C_TRANSLIT_REPLACE_CHAR", "_"),
            "SKIP_ROOT_SECTION" => COption::GetOptionString("catalog", "1C_SKIP_ROOT_SECTION", "N"),
            "DISABLE_CHANGE_PRICE_NAME" => COption::GetOptionString("catalog", "1C_DISABLE_CHANGE_PRICE_NAME"),

            'USE_TEMP_DIR' => 'Y'
        )
    );
}
else
{
    $APPLICATION->RestartBuffer();
    echo "failure\n";
    echo "Unknown command type.";
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>