<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle("Главная");
$APPLICATION->SetPageProperty('title', 'Купить ортопедическую обувь для детей «ORTHOBOOM» в интернет-магазине с доставкой в России');
$APPLICATION->SetPageProperty('description', 'Основные преимущества обуви «ORTHOBOOM». «ORTHOBOOM» - правильная обувь');
?><script type="text/javascript">
			$(document).ready(function() {

				$("#owl-demo").owlCarousel({
					navigation : true,
					slideSpeed : 1000,
					paginationSpeed : 1200,
					singleItem:true,
					navigationText: ["",""],
					autoPlay: 4000,
					rewindSpeed: 0

				});   
			}); 
</script>
<div class="slider">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"owl-carousel-hidden-div",
	Array(
		"ACTIVE_DATE_FORMAT" => "",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("",""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "1",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("NAME_ENG","TEXT_ENG",""),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
</div>
<div class="parts">
	<h1 class="center">Интернет-магазин детской ортопедической обуви</h1>
 <br>
	<div class="h1">
		 Ортопедическая обувь ORTHOBOOM - это:
	</div>
 <a href="info/konstruktsia-obuvi/" class="container" style="display: block;">
	<!--
				<p id="part1">Extended or butterfly-shaped heel caps</p>
				<p id="part2">Soft collars</p>				
				<p id="part3">Hook-and-loop straps and laces</p>
				<p id="part4">Removable shell-shaped insoles</p>
--> </a>
</div>
<div class="about">
	<div class="container">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"benefit-list",
	Array(
		"ACTIVE_DATE_FORMAT" => "",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("",""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "1",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("NAME_ENG","TEXT_ENG",""),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ID",
		"SORT_BY2" => "",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => ""
	)
);?>
	</div>
</div>
 <br>
 <br>
<div style="text-align:center;">
	 <iframe width="720" height="576" src="https://www.youtube.com/embed/aYayWmBhWAU" frameborder="0" allowfullscreen></iframe> <br>
	<h2 style="text-align: center;"><a href="/upload/ngma.pdf" style="color: #0e6899">Заключение о клинических испытаниях обуви ТМ "ORTHOBOOM"</a></h2>
</div>
<div class="profit">
	<div class="container">
		<div class="h1">
			 Почему вообще полезна наша обувь
		</div>
		<div class="span2">
 <img src="/images/foots.png" alt="">
		</div>
		<div class="span1">
		</div>
		<div class="span9 green">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "polza_ru",
		"EDIT_TEMPLATE" => ""
	)
);?>
		</div>
	</div>
</div>
<div class="forma">
	<div class="container">
		<div class="callback">
			<h2 class="center">Оставить заявку на покупку обуви</h2>
			 <?$APPLICATION->IncludeComponent(
	"altasib:feedback.form",
	"template_common",
	Array(
		"ACTIVE_ELEMENT" => "Y",
		"ADD_HREF_LINK" => "Y",
		"AGREEMENT" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"ALX_CHECK_NAME_LINK" => "N",
		"ALX_LINK_POPUP" => "N",
		"BACKCOLOR_ERROR" => "#ffffff",
		"BBC_MAIL" => "feedback@julianna.ru",
		"BORDER_RADIUS" => "3px",
		"CAPTCHA_TYPE" => "recaptcha",
		"CATEGORY_SELECT_NAME" => "Выберите категорию",
		"CHECKBOX_TYPE" => "CHECKBOX",
		"CHECK_ERROR" => "Y",
		"COLOR_ERROR" => "#8E8E8E",
		"COLOR_ERROR_TITLE" => "#A90000",
		"COLOR_HINT" => "#000000",
		"COLOR_INPUT" => "#727272",
		"COLOR_MESS_OK" => "#963258",
		"COLOR_NAME" => "#000000",
		"COLOR_SCHEME" => "BRIGHT",
		"COLOR_THEME" => "",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"EVENT_TYPE" => "ALX_FEEDBACK_FORM",
		"FB_TEXT_NAME" => "",
		"FB_TEXT_SOURCE" => "PREVIEW_TEXT",
		"FORM_ID" => "1",
		"HIDE_FORM" => "Y",
		"IBLOCK_ID" => "8",
		"IBLOCK_TYPE" => "feedback_structured",
		"IMG_ERROR" => "/upload/altasib.feedback.gif",
		"IMG_OK" => "/upload/altasib.feedback.ok.gif",
		"INPUT_APPEARENCE" => array("FORM_INPUTS_LINE"),
		"JQUERY_EN" => "jquery",
		"LINK_SEND_MORE_TEXT" => "Отправить ещё одно сообщение",
		"LOCAL_REDIRECT_ENABLE" => "N",
		"MASKED_INPUT_PHONE" => array(),
		"MESSAGE_OK" => "Сообщение отправлено!",
		"NAME_ELEMENT" => "ALX_DATE",
		"NOT_CAPTCHA_AUTH" => "Y",
		"PROPERTY_FIELDS" => array("FIO","TEL","EMAIL","FEEDBACK_TEXT"),
		"PROPERTY_FIELDS_REQUIRED" => array(),
		"PROPS_AUTOCOMPLETE_EMAIL" => array("EMAIL"),
		"PROPS_AUTOCOMPLETE_NAME" => array("FIO"),
		"PROPS_AUTOCOMPLETE_PERSONAL_PHONE" => array(),
		"PROPS_AUTOCOMPLETE_VETO" => "N",
		"RECAPTCHA_THEME" => "light",
		"RECAPTCHA_TYPE" => "image",
		"REWIND_FORM" => "N",
		"SECTION_FIELDS_ENABLE" => "N",
		"SECTION_MAIL10" => "",
		"SECTION_MAIL11" => "",
		"SECTION_MAIL7" => "",
		"SECTION_MAIL8" => "",
		"SECTION_MAIL9" => "",
		"SECTION_MAIL_ALL" => "",
		"SEND_IMMEDIATE" => "Y",
		"SEND_MAIL" => "N",
		"SHOW_LINK_TO_SEND_MORE" => "Y",
		"SHOW_MESSAGE_LINK" => "Y",
		"SIZE_HINT" => "10px",
		"SIZE_INPUT" => "12px",
		"SIZE_NAME" => "12px",
		"USERMAIL_FROM" => "N",
		"USE_CAPTCHA" => "Y",
		"WIDTH_FORM" => "50%"
	)
);?>
		</div>
	</div>
</div>
 <br>