<?
use Bitrix\Sale\Cashbox\CheckManager;
use Bitrix\Sale\Order;

AddEventHandler("main", "OnBeforeUserUpdate", "OnBeforeUserUpdateHandler");
AddEventHandler("main", "OnAfterUserUpdate", "OnAfterUserUpdateHandler");

function getPersonType($user_id = 0) {
    global $USER;

    $user_id = intval($user_id);
    if ($user_id == 0)
        $user_id = $USER->GetID();

    $arGroups = CUser::GetUserGroup($user_id);
    if (in_array(5, $arGroups)) {
        return "F"; // Физик
    } else if (in_array(6, $arGroups)) {
        return "U"; // Юрик
    }

    return false; // Не физик и не юрик
}

function OnAfterUserUpdateHandler(&$arFields) {

    if(defined('REGISTRATION_SMS_ENABLE') && REGISTRATION_SMS_ENABLE && !empty($arFields['TYPE_UPDATE'])) {
        return;
    }

    if(SITE_ID != 's1') return true;
    require($_SERVER["DOCUMENT_ROOT"] . "/personal/level.data.php");

    $currentUser["UF_SUMMA"] = intval($arFields["UF_SUMMA"]);
    $g = 0;
    if($currentUser["UF_SUMMA"] >= $user_leval["SILVER"][0] && $currentUser["UF_SUMMA"] <= $user_leval["SILVER"][1]):
        $g = 12;
    elseif($currentUser["UF_SUMMA"] >= $user_leval["GOLD"][0] && $currentUser["UF_SUMMA"] <= $user_leval["GOLD"][1]):
        $g = 13;
    elseif($currentUser["UF_SUMMA"] >= $user_leval["PLATINUM"][0] && $currentUser["UF_SUMMA"] <= $user_leval["PLATINUM"][1]):
        $g = 14;
    endif;

    $arGroups = CUser::GetUserGroup($arFields["ID"]);
    $arGroupsNew = array();

    if (in_array(6, $arGroups)) {
        $arGroupsSearch = array(12, 13, 14);
        foreach($arGroups as $group) {
            if (!in_array($group, $arGroupsSearch)) {
                $arGroupsNew[] = $group;
            }
        }

        if ($g > 0)
            $arGroupsNew[] = $g;

        CUser::SetUserGroup($arFields["ID"], $arGroupsNew);
    }

}

function OnBeforeUserUpdateHandler(&$arFields) {
    if(defined('REGISTRATION_SMS_ENABLE') && REGISTRATION_SMS_ENABLE && !empty($arFields['TYPE_UPDATE'])) {
        return;
    }
    if ($arFields["LID"] == "s1") {
        $rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), array("ID" => $arFields["ID"]));
        if ($resUsers = $rsUsers->GetNext()) {
            if ($resUsers["ACTIVE"] == "N" && $arFields["ACTIVE"] == "Y") {
                $arEventFields = array(
                    "NAME"  => $arFields["NAME"] . " " . $arFields["LAST_NAME"],
                    "EMAIL" => $arFields["EMAIL"]
                );
                CEvent::Send("NEW_ORGANIZATION_ACTIVE", "s1", $arEventFields);
            }
        }
    }
}

AddEventHandler("iblock", "OnAfterIBlockElementAdd", "setMinMaxSize");
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "setMinMaxSize");

function setMinMaxSize(&$arFields) {
    if ($arFields["IBLOCK_ID"] == 57 && CModule::IncludeModule("iblock")) {

        $cmlLink = false;
        foreach ($arFields["PROPERTY_VALUES"][1428] as $arLink) {
            $cmlLink = $arLink["VALUE"];
            break;
        }

        if ($cmlLink !== false) {

            $arSize = array();

            $arSelect = Array("PROPERTY_OBUV_RAZMER", "ID");
            $arFilter = Array("IBLOCK_ID"=>$arFields["IBLOCK_ID"], "PROPERTY_CML2_LINK"=>$cmlLink);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            while($ob = $res->GetNextElement())
            {
                $arRes = $ob->GetFields();
                $arSize[] = $arRes["PROPERTY_OBUV_RAZMER_VALUE"];
            }

            sort($arSize);

            if (isset($arSize[0]))
                CIBlockElement::SetPropertyValueCode($cmlLink, "MIN_SIZE", $arSize[0]);

            if (isset($arSize[count($arSize)-1]))
                CIBlockElement::SetPropertyValueCode($cmlLink, "MAX_SIZE", $arSize[count($arSize)-1]);
        }
    }
}


function custom_mail($to,$subject,$body,$headers) {
    $f=fopen($_SERVER["DOCUMENT_ROOT"]."/maillog.txt", "a+");
    fwrite($f, print_r(array('TO' => $to, 'SUBJECT' => $subject, 'BODY' => $body, 'HEADERS' => $headers),1)."\n========\n");
    fclose($f);
    return mail($to,$subject,$body,$headers);
}

//custom_mail("a.paukov@at-nn.ru", "Тест", "Проверяем...",   "From: a.paukov@at-nn.ru \r\n");

// check print
AddEventHandler("sale", "OnOrderUpdate", "OnOrderUpdate");
function OnOrderUpdate(&$arFields) {
    if (!($arOrder = CSaleOrder::GetByID($arFields))) {
        echo "Заказ с ID " . $arFields . " не найден";
    } else {
        if($arOrder["STATUS_ID"] === "F" && $arOrder["PAYED"] === "Y") {
            $order = Order::load($arOrder["ID"]);
            if ($order) {
                $paymentCollection = $order->getPaymentCollection();
                $shipmentCollection = $order->getShipmentCollection();
                $printedChecks = $order->getPrintedChecks();
                $checkCollectionItems = array();
                foreach ($paymentCollection as $payment) {
                    $checkCollectionItems[] = $payment;
                }
                foreach ($shipmentCollection as $shipment) {
                    $checkCollectionItems[] = $shipment;
                }
                if(!($printedChecks && !empty($printedChecks))) {
                    CheckManager::addByType($checkCollectionItems, 'sell');
                }
            }
        }
    }
}

// write utm and ga cid to order
AddEventHandler("sale", "OnOrderSave", "OnOrderSave");
function OnOrderSave(&$arFields) {
    if (!($arOrder = CSaleOrder::GetByID($arFields))) {
        echo "Заказ с ID " . $arFields . " не найден";
    } else {
        $order = Order::load($arOrder["ID"]);
        if ($order) {

            // utm sbjs
            //$sbjsCookieData = explode('|||', $_COOKIE['sbjs_current']);
            $mgo_sb_current = urldecode($_COOKIE['mgo_sb_current']);
            $sbjsCookieData = explode('|*|', $mgo_sb_current);
            $sbjsData = array();
            $sbjsStr = array();
            foreach ($sbjsCookieData as $row) {
                $sbjsData[preg_replace('/=.*/', '', $row)] = preg_replace('/.*=/', '', $row);
            }
            foreach ($sbjsData as $key => $value) {
                $sbjsStr[] = $key . '=' . $value;
            }

            // ga cid
            function gaParseCookie($ga_cookie = NULL) {
                $cookie_ga = isset($_COOKIE['_ga']) ? $_COOKIE['_ga'] : $ga_cookie;
                if (isset($cookie_ga)) {
                    list($version,$domainDepth, $cid1, $cid2) = explode('.', $cookie_ga);
                    $contents = array('version' => $version, 'domainDepth' => $domainDepth, 'cid' => $cid1.'.'.$cid2);
                    $cid = $contents['cid'];
                }
                else $cid = '';
                return $cid;
            }

            // order coupon
            $couponList = [];
            $orderCouponList = \Bitrix\Sale\Internals\OrderCouponsTable::getList(array(
                'select' => array('COUPON'),
                'filter' => array('=ORDER_ID' => $arOrder["ID"])
            ));
            while ($coupon = $orderCouponList->fetch()) {
                $couponList[] = $coupon["COUPON"];
            }

            // get order products
            $orderProducts = [];
            $dbBasketItems = CSaleBasket::GetList(array(), array("ORDER_ID" => $arOrder["ID"]), false, false, array());
            while ($arItems = $dbBasketItems->Fetch()) {
                $orderProducts[] = $arItems["PRODUCT_ID"];
            }

            // get product discount list
            $orderDiscounts = [];
            if(count($orderProducts) > 0) {
                foreach($orderProducts as $product) {
                    $arDiscounts = CCatalogDiscount::GetDiscountByProduct(
                        $product,
                        [1, 2, 3, 4],
                        "N",
                        2,
                        SITE_ID
                    );
                    if(count($arDiscounts) > 0) {
                        foreach($arDiscounts as $discount) {
                            $orderDiscounts[] = $discount["NAME"];
                        }
                    }
                }
            }

            $propertyCollection = $order->getPropertyCollection();
            $arFullCollection = $propertyCollection->getArray();

            foreach ($arFullCollection["properties"] as $arProperty) {
                if($arProperty["CODE"] == "UTM") {
                    $propertyUtmValue = $propertyCollection->getItemByOrderPropertyId(intval($arProperty["ID"]));
                    $propertyUtmValue->setField('VALUE', implode("|", $sbjsStr));
                }

                if($arProperty["CODE"] == "GA_CID") {
                    $propertyGaCidValue = $propertyCollection->getItemByOrderPropertyId(intval($arProperty["ID"]));
                    $propertyGaCidValue->setField('VALUE', gaParseCookie());
                }

                if($arProperty["CODE"] == "DISCOUNTS") {
                    $propertyGaCidValue = $propertyCollection->getItemByOrderPropertyId(intval($arProperty["ID"]));
                    $propertyGaCidValue->setField('VALUE', implode("|", $orderDiscounts));
                }

                if($arProperty["CODE"] == "COUPON") {
                    $propertyGaCidValue = $propertyCollection->getItemByOrderPropertyId(intval($arProperty["ID"]));
                    $propertyGaCidValue->setField('VALUE', implode("|", $couponList));
                }
            }

            $order->doFinalAction(true);
            $order->save();
        }
    }
}


AddEventHandler("main", "OnBeforeProlog", "MyOnBeforePrologHandler", 50);
function MyOnBeforePrologHandler()
{
    global $USER;
    $USER->GetUserGroupArray();
}

function gFeedGenerate() {

    $userAdminGroup = array(1, 2, 3, 4);

    $productsCount = 0;
    $categoriesCount = 0;

    $fp = @fopen($_SERVER["DOCUMENT_ROOT"] . "/google_price/google.xml", "w");

    @fwrite($fp, "<" . "?xml version=\"1.0\" encoding=\"utf-8\"?" . ">\n");
    @fwrite($fp, "<rss version=\"2.0\" xmlns:g=\"http://base.google.com/ns/1.0\">\n");

    @fwrite($fp, "<channel>\n");

    @fwrite($fp, "<title>Orthoboom.ru</title>\n");
    @fwrite($fp, "<link>https://orthoboom.ru/</link>\n");
    @fwrite($fp, "<description>Интернет-магазин ортопедической обуви для детей и взрослых с доставкой по всей России. Собственное производство и гарантия от производителя 45 дней.</description>\n");


// получаем все активные товары
    $arSelect = Array(
        "ID",
        "NAME",
        "IBLOCK_SECTION_ID",
        "DETAIL_TEXT",
        "CATALOG_GROUP_91",
        "DETAIL_PICTURE",
        "CML2_AVAILABLE",
        "PROPERTY_TORGOVAYA_MARKA1",
        "PROPERTY_PRODUCT_COLOR_DESCRIPTION",
        "DETAIL_PAGE_URL",
        "PROPERTY_CML2_ARTICLE",
        "PROPERTY_OBUV_MATERIALVERKHA",
        "PROPERTY_OBUV_PODKLADKA",
        "PROPERTY_MATERIAL_PODOSHVY",
        "PROPERTY_MATERIAL_STELKI",
        "PROPERTY_VID_ZASTEZHKI",
        "PROPERTY_POL",
        "PROPERTY_OBUV_SEZONNOST",
        "PROPERTY_OBUV_VID",
        "PROPERTY_BRAND_COUNTRY",
        "PROPERTY_COUNTRY_OF_ORIGIN",
        "PROPERTY_TEMPERATURE_MODE",
        "PROPERTY_WATER_RESISTANCE",
        "PROPERTY_COMPLETENESS_OF_SHOES",
        "PROPERTY_STYLISM",
    );
    $arFilter = Array("IBLOCK_ID" => 94, "SECTION_ACTIVE" => "Y", "INCLUDE_SUBSECTIONS" => "Y", "CATALOG_AVAILABLE" => "Y", "ACTIVE" => "Y", "CATALOG_GROUP_91" <> "");
    $db_list = CIBlockElement::GetList(
        Array(),
        $arFilter,
        false,
        false,
        $arSelect
    );

// получаем все активные торговые предложения для каждого товара
    while ($currentProduct = $db_list->GetNext()) {
        $currentProductOffers = CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID' => '95', 'PROPERTY_CML2_LINK' => $currentProduct["ID"], "ACTIVE" => "Y", "CATALOG_AVAILABLE" => "Y", "CATALOG_GROUP_91" <> ""),
            false,
            false,
            array("ID", "NAME", "IBLOCK_SECTION_ID", "PREVIEW_TEXT", "DETAIL_PICTURE", "PROPERTY_OBUV_RAZMER", "DETAIL_PAGE_URL", "PROPERTY_CML2_BAR_CODE",)
        );

        while ($offerItem = $currentProductOffers->GetNext())
        {
            // формируем цены торгового предложения
            $arPriceList = CCatalogProduct::GetOptimalPrice($offerItem['ID'], 1, $userAdminGroup, 'N', array(), 's1');
            if (!$arPriceList || count($arPriceList) <= 0)
            {
                if ($nearestQuantity = CCatalogProduct::GetNearestQuantityPrice($offerItem['ID'], 1, $userAdminGroup))
                {
                    $quantity = $nearestQuantity;
                    $arPriceList = CCatalogProduct::GetOptimalPrice($offerItem['ID'], $quantity, $userAdminGroup, 'N', array(), 's1');
                }
            }
            $offerItemBasePrice = $arPriceList['RESULT_PRICE']['BASE_PRICE'];
            $offerItemDiscountPrice = $arPriceList['RESULT_PRICE']['DISCOUNT_PRICE'];

            if (!empty($offerItemBasePrice)) {

                @fwrite($fp, "<item>\n");
                @fwrite($fp, "<g:item_group_id>" . $currentProduct['ID'] . "</g:item_group_id>\n");
                @fwrite($fp, "<g:id>" . $offerItem['ID'] . "</g:id>\n");
                @fwrite($fp, "<g:title>" . $currentProduct['NAME'] . "</g:title>\n");
                @fwrite($fp, "<g:description>" . str_replace("&nbsp;", " ", strip_tags($currentProduct["DETAIL_TEXT"])) . "</g:description>\n");
                @fwrite($fp, "<g:link>https://orthoboom.ru" . $offerItem['DETAIL_PAGE_URL'] . "</g:link>\n");

                if($offerItemBasePrice != $offerItemDiscountPrice) {
                    @fwrite($fp, "<g:price>" . $offerItemBasePrice . " RUB</g:price>\n");
                    @fwrite($fp, "<g:sale_price>" . $offerItemDiscountPrice . " RUB</g:sale_price>\n");
                } else {
                    @fwrite($fp, "<g:price>" . $offerItemBasePrice . " RUB</g:price>\n");
                }

                @fwrite($fp, "<g:google_product_category>187</g:google_product_category>\n");

                if($currentProduct['PROPERTY_TORGOVAYA_MARKA1_VALUE']) {
                    @fwrite($fp, "<g:brand>" . $currentProduct['PROPERTY_TORGOVAYA_MARKA1_VALUE'] . "</g:brand>\n");
                }

                if($currentProduct['PROPERTY_CML2_ARTICLE_VALUE']) {
                    @fwrite($fp, "<g:mpn>" . $currentProduct['PROPERTY_CML2_ARTICLE_VALUE'] . "</g:mpn>\n");
                }

                if($offerItem['PROPERTY_CML2_BAR_CODE_VALUE']) {
                    @fwrite($fp, "<g:gtin>" . $offerItem['PROPERTY_CML2_BAR_CODE_VALUE'] . "</g:gtin>\n");
                }

                if ($currentProduct['CATALOG_AVAILABLE'] == "N") {
                    @fwrite($fp, "<g:availability>out of stock</g:availability>\n");
                } else {
                    @fwrite($fp, "<g:availability>in stock</g:availability>\n");
                }

                $currentProductPicture = CFile::GetByID(IntVal($currentProduct['DETAIL_PICTURE']));
                if ($ar_file = $currentProductPicture->Fetch()) {
                    $strFile = "/" . (COption::GetOptionString("main", "upload_dir", "upload")) . "/" . $ar_file["SUBDIR"] . "/" . $ar_file["FILE_NAME"];
                    $strFile = str_replace("//", "/", $strFile);
                    @fwrite($fp, "<g:image_link>https://orthoboom.ru" . implode("/", array_map("rawurlencode", explode("/", $strFile))) . "</g:image_link>\n");
                }

                if($offerItem['PROPERTY_OBUV_RAZMER_VALUE']) {
                    @fwrite($fp, "<g:size>" . str_replace("р.", "", $offerItem['PROPERTY_OBUV_RAZMER_VALUE']) . "</g:size>\n");
                    @fwrite($fp, "<g:size_system>EU</g:size_system>\n");
                }

                if($currentProduct['PROPERTY_POL_VALUE']) {
                    switch ($currentProduct['PROPERTY_POL_VALUE']) {
                        case 'девочка':
                            $googleGender = 'женский';
                            break;
                        case 'мальчик':
                            $googleGender = 'мужской';
                            break;
                        default:
                            $googleGender = $currentProduct['PROPERTY_POL_VALUE'];
                    }
                    @fwrite($fp, "<g:gender>" . $googleGender . "</g:gender>\n");
                }

                if($currentProduct['PROPERTY_OBUV_MATERIALVERKHA_VALUE']) {
                    @fwrite($fp, "<g:material>" . $currentProduct['PROPERTY_OBUV_MATERIALVERKHA_VALUE'] . "</g:material>\n");
                }

                @fwrite($fp, "</item>\n");
            }

            $productsCount = $productsCount + 1;

        }
    }

    @fwrite($fp, "</channel>\n");
    @fwrite($fp, "</rss>\n");

    @fclose($fp);

    //echo "Файл <b>google.xml</b> успешно создан<br>";
    //echo "Выгружено <b>$tov</b> товара(ов)";

    return "gFeedGenerate();";
}

function yaFeedGenerate() {

    $testalx = 1;

    $userAdminGroup = array(1, 2, 3, 4);

    $productsCount = 0;
    $categoriesCount = 0;

    $fp = @fopen($_SERVER["DOCUMENT_ROOT"] . "/yandex_price/yandex.yml", "w");

    @fwrite($fp, "<" . "?xml version=\"1.0\" encoding=\"utf-8\"?" . ">\n");
    @fwrite($fp, "<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">\n");
    @fwrite($fp, "<yml_catalog date=\"" . Date("Y-m-d H:i") . "\">\n");

    @fwrite($fp, "<shop>\n");

    @fwrite($fp, "<name>Orthoboom.ru</name>\n");
    @fwrite($fp, "<company>Ортопедическая обувь для детей и взрослых в интернет-магазине ORTHOBOOM</company>\n");
    @fwrite($fp, "<url>https://orthoboom.ru/</url>\n");

    @fwrite($fp, "<currencies>\n");
    @fwrite($fp, "<currency id='RUR' rate='1'/>\n");
    @fwrite($fp, "</currencies>\n");

    // получаем активные разделы каталога
    @fwrite($fp, "<categories>\n");
    $arSort = Array("SORT" => "ASC");


    /*
    //исходный вариант (start)
    $arFilter = Array('IBLOCK_ID' => '94', 'GLOBAL_ACTIVE' => 'Y');
    CModule::IncludeModule('iblock');
    $db_list = CIBlockSection::GetList($arSort, $arFilter, true);
    //исходный вариант (end)
    */

    //новый вариант (start)
    //нужен только раздел "Ортопедическая обувь"

    $SECTION_ID = '1414';

    $db_list = CIBlockSection::GetList(
        array(),
        array('ID' => $SECTION_ID, 'IBLOCK_ID' => '94', 'GLOBAL_ACTIVE' => 'Y')
    );
    $ar = $db_list->GetNext();
    $db_list = CIBlockSection::GetList(
        array('LEFT_MARGIN' => 'ASC'),
        array(
            'IBLOCK_ID' => '94',
            'GLOBAL_ACTIVE' => 'Y',
            '>LEFT_MARGIN' => $ar['LEFT_MARGIN'],
            '<RIGHT_MARGIN' => $ar['RIGHT_MARGIN']
        )
    );

    $arFSection = array();

    //новый вариант (end)

    while ($categories = $db_list->GetNext()) {
        $arFSection[] = $categories['ID'];

        if ($categories['IBLOCK_SECTION_ID'] <> "") {
            @fwrite($fp, "<category id='" . $categories['ID'] . "' parentId='" . $categories['IBLOCK_SECTION_ID'] . "'>" . $categories['NAME'] . "</category>\n");
            $categoriesCount = $categoriesCount + 1;
        } else {
            @fwrite($fp, "<category id='" . $categories['ID'] . "'>" . $categories['NAME'] . "</category>\n");
            $categoriesCount = $categoriesCount + 1;
        }
    }
    @fwrite($fp, "</categories>\n");

    @fwrite($fp, "<offers>\n");
    // получаем все активные товары
    $arSelect = Array(
        "ID",
        "NAME",
        "IBLOCK_SECTION_ID",
        "DETAIL_TEXT",
        "CATALOG_GROUP_91",
        "DETAIL_PICTURE",
        "CML2_AVAILABLE",
        "PROPERTY_TORGOVAYA_MARKA1",
        "PROPERTY_PRODUCT_COLOR_DESCRIPTION",
        "DETAIL_PAGE_URL",
        "PROPERTY_CML2_ARTICLE",
        "PROPERTY_OBUV_MATERIALVERKHA",
        "PROPERTY_OBUV_PODKLADKA",
        "PROPERTY_MATERIAL_PODOSHVY",
        "PROPERTY_MATERIAL_STELKI",
        "PROPERTY_VID_ZASTEZHKI",
        "PROPERTY_POL",
        "PROPERTY_OBUV_SEZONNOST",
        "PROPERTY_OBUV_VID",
        "PROPERTY_RYNOCHNYY_VID_OBUVI",
        "PROPERTY_BRAND_COUNTRY",
        "PROPERTY_COUNTRY_OF_ORIGIN",
        "PROPERTY_TEMPERATURE_MODE",
        "PROPERTY_WATER_RESISTANCE",
        "PROPERTY_COMPLETENESS_OF_SHOES",
        "PROPERTY_STYLISM",
        "PROPERTY_VOZRAST",
        "PROPERTY_TSVET",
        "PROPERTY_TSVET_YANDEKS_MARKET_ODNOZNACHNOE",
        "PROPERTY_VESNA",
        "PROPERTY_OSEN",
    );



    //исходный вариант (start)
    $arFilter = Array("IBLOCK_ID" => 94, 'SECTION_ID'=> $SECTION_ID, "SECTION_ACTIVE" => "Y", "INCLUDE_SUBSECTIONS" => "Y", "CATALOG_AVAILABLE" => "Y", "ACTIVE" => "Y", "CATALOG_GROUP_91" <> "");
    $db_list = CIBlockElement::GetList(
        Array(),
        $arFilter,
        false,
        false,
        $arSelect
    );
    //исходный вариант (end)

    //print_r($arFilter);
    //return;



    $i = 1;
    //echo $db_list->SelectedRowsCount();
    //return false;

    // получаем все активные торговые предложения для каждого товара
    while ($currentProduct = $db_list->GetNext()) {

        $dbElemSec = CIBlockElement::GetElementGroups($currentProduct['ID'], true, array('ID', 'DEPTH_LEVEL'));

        while ($arElemSections = $dbElemSec->Fetch()) {
            if(in_array($arElemSections['ID'], $arFSection)){
                $currentProduct['SECTIONS'][] = $arElemSections['ID'];
                $currentProduct['DEPTH_LEVEL'][] = $arElemSections['DEPTH_LEVEL'];
            }
        }

        $maxDepthKey = 0;
        $maxDepthVal = 0;
        foreach ($currentProduct['DEPTH_LEVEL'] AS $key=>$val){
            if($val > $maxDepthVal){
                $maxDepthKey = $key;
                $maxDepthVal = $val;
            }
        }

        //print_r($currentProduct['SECTIONS']);
        //print_r($currentProduct['DEPTH_LEVEL']);

        //echo "<br>maxDepthKey = ", $maxDepthKey;
        //echo "<br>maxDepthVal = ", $maxDepthVal;

        $currentProduct['ACTUAL_PARENT_ID'] = $currentProduct['SECTIONS'][$maxDepthKey];

        /*
        print_r($currentProduct);
        exit;
        */



        $testalx = 1;

        $currentProductOffers = CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID' => '95', 'PROPERTY_CML2_LINK' => $currentProduct["ID"], "ACTIVE" => "Y", "CATALOG_AVAILABLE" => "Y", "CATALOG_GROUP_91" <> ""),
            false,
            false,
            array("ID", "NAME", "IBLOCK_SECTION_ID", "PREVIEW_TEXT", "DETAIL_PICTURE", "PROPERTY_OBUV_RAZMER", "DETAIL_PAGE_URL", "PROPERTY_CML2_BAR_CODE", "ACTUAL_PARENT_ID")
        );

        $testalx = 1;

        //echo "<br>", $currentProductOffers->SelectedRowsCount();

        while ($offerItem = $currentProductOffers->GetNext())
        {
            $testalx = 1;

            // формируем цены торгового предложения
            $arPriceList = CCatalogProduct::GetOptimalPrice($offerItem['ID'], 1, $userAdminGroup, 'N', array(), 's1');
            if (!$arPriceList || count($arPriceList) <= 0)
            {
                if ($nearestQuantity = CCatalogProduct::GetNearestQuantityPrice($offerItem['ID'], 1, $userAdminGroup))
                {
                    $quantity = $nearestQuantity;
                    $arPriceList = CCatalogProduct::GetOptimalPrice($offerItem['ID'], $quantity, $userAdminGroup, 'N', array(), 's1');
                }
            }
            $offerItemBasePrice = $arPriceList['RESULT_PRICE']['BASE_PRICE'];
            $offerItemDiscountPrice = $arPriceList['RESULT_PRICE']['DISCOUNT_PRICE'];

            if (!empty($offerItemBasePrice)) {
                if($offerItemBasePrice != $offerItemDiscountPrice) {
                    $productPrice = $offerItemDiscountPrice;
                } else {
                    $productPrice = $offerItemBasePrice;
                }

                if($productPrice >= 3000) {

                    @fwrite($fp, "<offer id='" . $offerItem['ID'] . "' available=\"true\" group_id=\"" . $i . "\" >\n");
                    @fwrite($fp, "<name>" . $currentProduct['NAME'] . "</name>\n");

                    //@fwrite($fp, "<url>https://orthoboom.ru" . $offerItem['DETAIL_PAGE_URL'] . "</url>\n");
                    @fwrite($fp, "<url>https://orthoboom.ru" . $offerItem['DETAIL_PAGE_URL'] . "?prod=" . $offerItem['ID'] . "&amp;utm_source=market&amp;utm_term=" . $offerItem['ID'] . "</url>\n");

                    if ($offerItemBasePrice != $offerItemDiscountPrice) {
                        @fwrite($fp, "<price>" . $offerItemDiscountPrice . "</price>\n");
                        @fwrite($fp, "<oldprice>" . $offerItemBasePrice . "</oldprice>\n");
                    } else {
                        @fwrite($fp, "<price>" . $offerItemBasePrice . "</price>\n");
                    }

                    @fwrite($fp, "<currencyId>RUR</currencyId>\n");

                    //@fwrite($fp, "<categoryId>" . $currentProduct['IBLOCK_SECTION_ID'] . "</categoryId>\n");
                    @fwrite($fp, "<categoryId>" . $currentProduct['ACTUAL_PARENT_ID'] . "</categoryId>\n");

                    @fwrite($fp, "<delivery>true</delivery>\n");
                    @fwrite($fp, "<pickup>true</pickup>\n");
                    @fwrite($fp, "<manufacturer_warranty>true</manufacturer_warranty>\n");

                    if ($currentProduct['PROPERTY_TORGOVAYA_MARKA1_VALUE']) {
                        @fwrite($fp, "<vendor>" . $currentProduct['PROPERTY_TORGOVAYA_MARKA1_VALUE'] . "</vendor>\n");
                    }
                    if ($currentProduct['PROPERTY_CML2_ARTICLE_VALUE']) {
                        @fwrite($fp, "<vendorCode>" . $currentProduct['PROPERTY_CML2_ARTICLE_VALUE'] . "</vendorCode>\n");
                    }

                    if ($currentProduct['PROPERTY_COUNTRY_OF_ORIGIN_VALUE']) {
                        @fwrite($fp, "<country_of_origin>" . $currentProduct['PROPERTY_COUNTRY_OF_ORIGIN_VALUE'] . "</country_of_origin>\n");
                    } else {
                        @fwrite($fp, "<country_of_origin>Россия</country_of_origin>\n");
                    }

                    if ($offerItem['PROPERTY_CML2_BAR_CODE_VALUE']) {
                        @fwrite($fp, "<barcode>" . $offerItem['PROPERTY_CML2_BAR_CODE_VALUE'] . "</barcode>\n");
                    }

                    @fwrite($fp, "<description><![CDATA[ 
                        " . $currentProduct["DETAIL_TEXT"] . "
                        ]]></description>\n");

                    $currentProductPicture = CFile::GetByID(IntVal($currentProduct['DETAIL_PICTURE']));
                    if ($ar_file = $currentProductPicture->Fetch()) {
                        $strFile = "/" . (COption::GetOptionString("main", "upload_dir", "upload")) . "/" . $ar_file["SUBDIR"] . "/" . $ar_file["FILE_NAME"];
                        $strFile = str_replace("//", "/", $strFile);
                        @fwrite($fp, "<picture>https://orthoboom.ru" . implode("/", array_map("rawurlencode", explode("/", $strFile))) . "</picture>\n");
                    }

                    @fwrite($fp, "<param name=\"Размер\" unit=\"RU\" >" . str_replace("р.", "", $offerItem['PROPERTY_OBUV_RAZMER_VALUE']) . "</param>\n");

                    if ($currentProduct['PROPERTY_BRAND_COUNTRY_VALUE']) {
                        @fwrite($fp, "<param name=\"Страна бренда\">" . $currentProduct['PROPERTY_BRAND_COUNTRY_VALUE'] . "</param>\n");
                    } else {
                        @fwrite($fp, "<param name=\"Страна бренда\">Германия</param>\n");
                    }
                    if ($currentProduct['PROPERTY_PRODUCT_COLOR_DESCRIPTION_VALUE']) {
                        @fwrite($fp, "<param name=\"Цвет\">" . $currentProduct['PROPERTY_PRODUCT_COLOR_DESCRIPTION_VALUE'] . "</param>\n");
                    }
                    if ($currentProduct['PROPERTY_OBUV_MATERIALVERKHA_VALUE']) {
                        @fwrite($fp, "<param name=\"Материал верха\">" . $currentProduct['PROPERTY_OBUV_MATERIALVERKHA_VALUE'] . "</param>\n");
                    }
                    if ($currentProduct['PROPERTY_OBUV_PODKLADKA_VALUE']) {
                        @fwrite($fp, "<param name=\"Материал подклада\">" . $currentProduct['PROPERTY_OBUV_PODKLADKA_VALUE'] . "</param>\n");
                    }
                    if ($currentProduct['PROPERTY_MATERIAL_PODOSHVY_VALUE']) {
                        @fwrite($fp, "<param name=\"Материал подошвы\">" . $currentProduct['PROPERTY_MATERIAL_PODOSHVY_VALUE'] . "</param>\n");
                    }
                    if ($currentProduct['PROPERTY_MATERIAL_STELKI_VALUE']) {
                        @fwrite($fp, "<param name=\"Материал стельки\">" . $currentProduct['PROPERTY_MATERIAL_STELKI_VALUE'] . "</param>\n");
                    }
                    if ($currentProduct['PROPERTY_VID_ZASTEZHKI_VALUE']) {
                        @fwrite($fp, "<param name=\"Вид застёжки\">" . $currentProduct['PROPERTY_VID_ZASTEZHKI_VALUE'] . "</param>\n");
                    }
                    /*
                    if($currentProduct['PROPERTY_TSVET_VALUE']) {
                        foreach ($currentProduct['PROPERTY_TSVET_VALUE'] AS $itemColor){
                            @fwrite($fp, "<param name=\"Цвет\">" . $itemColor . "</param>\n");
                        }
                    }
                    */
                    /*
                    if ($currentProduct['PROPERTY_TSVET_VALUE']) {
                        if (is_array($currentProduct['PROPERTY_TSVET_VALUE']) && !empty($currentProduct['PROPERTY_TSVET_VALUE'])) {
                            @fwrite($fp, "<param name=\"Цвет\">" . current($currentProduct['PROPERTY_TSVET_VALUE']) . "</param>\n");
                        } else {
                            @fwrite($fp, "<param name=\"Цвет\">" . $currentProduct['PROPERTY_TSVET_VALUE'] . "</param>\n");
                        }
                    }
                    */
                    if ($currentProduct['PROPERTY_TSVET_YANDEKS_MARKET_ODNOZNACHNOE_VALUE']) {
                        @fwrite($fp, "<param name=\"Цвет\">" . $currentProduct['PROPERTY_TSVET_YANDEKS_MARKET_ODNOZNACHNOE_VALUE'] . "</param>\n");
                    }

                    if ($currentProduct['PROPERTY_POL_VALUE']) {
                        @fwrite($fp, "<param name=\"Пол\">" . $currentProduct['PROPERTY_POL_VALUE'] . "</param>\n");
                    }
                    if ($currentProduct['PROPERTY_VOZRAST_VALUE']) {
                        @fwrite($fp, "<param name=\"Возраст\">" . $currentProduct['PROPERTY_VOZRAST_VALUE'] . "</param>\n");
                    }

                    /*
                    if($currentProduct['PROPERTY_OBUV_SEZONNOST_VALUE']) {
                        @fwrite($fp, "<param name=\"Сезон\">" . $currentProduct['PROPERTY_OBUV_SEZONNOST_VALUE'] . "</param>\n");
                    }
                    */

                    if($currentProduct['PROPERTY_OBUV_SEZONNOST_VALUE']) {
                        if($currentProduct['PROPERTY_OBUV_SEZONNOST_VALUE'] == 'Весна - Осень'){
                            if($currentProduct['PROPERTY_OSEN_VALUE'] && $currentProduct['PROPERTY_OSEN_VALUE'] == 'Да') {
                                @fwrite($fp, "<param name=\"Сезон\">осень</param>\n");
                            }
                            if($currentProduct['PROPERTY_VESNA_VALUE'] && $currentProduct['PROPERTY_VESNA_VALUE'] == 'Да') {
                                @fwrite($fp, "<param name=\"Сезон\">весна</param>\n");
                            }
                        }else{
                            @fwrite($fp, "<param name=\"Сезон\">" . $currentProduct['PROPERTY_OBUV_SEZONNOST_VALUE'] . "</param>\n");
                        }
                    }
                    $testalx = 1;
                    /*
                    if ($currentProduct['PROPERTY_OBUV_SEZONNOST_VALUE']) {
                        if (is_array($currentProduct['PROPERTY_OBUV_SEZONNOST_VALUE'])) {
                            foreach ($currentProduct['PROPERTY_OBUV_SEZONNOST_VALUE'] AS $itemSeason) {
                                @fwrite($fp, "<param name=\"Сезон\">" . $itemSeason . "</param>\n");
                            }
                        } else {
                            @fwrite($fp, "<param name=\"Сезон\">" . $currentProduct['PROPERTY_OBUV_SEZONNOST_VALUE'] . "</param>\n");
                        }
                    }
                    */
                    /*
                    if($currentProduct['PROPERTY_OSEN_VALUE'] && $currentProduct['PROPERTY_OSEN_VALUE'] == 'Да') {
                        @fwrite($fp, "<param name=\"Сезон\">осень</param>\n");
                    }
                    if($currentProduct['PROPERTY_VESNA_VALUE'] && $currentProduct['PROPERTY_VESNA_VALUE'] == 'Да') {
                        @fwrite($fp, "<param name=\"Сезон\">весна</param>\n");
                    }
                    */

                    if ($currentProduct['PROPERTY_RYNOCHNYY_VID_OBUVI_VALUE']) {
                        $res = CIBlockElement::GetProperty(94, $currentProduct["ID"], array("sort" => "asc"), Array("CODE" => "RYNOCHNYY_VID_OBUVI"));
                        while ($ob = $res->GetNext()) {
                            @fwrite($fp, "<param name=\"Тип\">" . $ob['VALUE_ENUM'] . "</param>\n");
                        }
                    }
                    /*
                    if($currentProduct['PROPERTY_OBUV_VID_VALUE']) {
                        @fwrite($fp, "<param name=\"Тип\">" . $currentProduct['PROPERTY_OBUV_VID_VALUE'] . "</param>\n");
                    }
                    */
                    if ($currentProduct['PROPERTY_TEMPERATURE_MODE_VALUE']) {
                        @fwrite($fp, "<param name=\"Температурный режим\">" . $currentProduct['PROPERTY_TEMPERATURE_MODE_VALUE'] . "</param>\n");
                    }
                    if ($currentProduct['PROPERTY_WATER_RESISTANCE_VALUE']) {
                        @fwrite($fp, "<param name=\"Водостойкость\">" . $currentProduct['PROPERTY_WATER_RESISTANCE_VALUE'] . "</param>\n");
                    }
                    if ($currentProduct['PROPERTY_COMPLETENESS_OF_SHOES_VALUE']) {
                        @fwrite($fp, "<param name=\"Полнота обуви\">" . $currentProduct['PROPERTY_COMPLETENESS_OF_SHOES_VALUE'] . "</param>\n");
                    } else {
                        @fwrite($fp, "<param name=\"Полнота обуви\">4</param>\n");
                    }
                    if ($currentProduct['PROPERTY_STYLISM_VALUE']) {
                        @fwrite($fp, "<param name=\"Стилистика\">" . $currentProduct['PROPERTY_STYLISM_VALUE'] . "</param>\n");
                    }

                    @fwrite($fp, "</offer>\n");

                }
            }

            $productsCount = $productsCount + 1;

        }

        $i++;
    }

    @fwrite($fp, "</offers>\n");
    @fwrite($fp, "</shop>\n");
    @fwrite($fp, "</yml_catalog>\n");

    @fclose($fp);

    //echo " Файл <b>yandex.yml</b> успешно создан<br>";
    //echo " Выгружено <b>$categoriesCount</b> категорий и <b>$productsCount</b> товара(ов)";

    return "yaFeedGenerate();";
}

if(isset($_COOKIE['sbjs_current'])) {
    unset($_COOKIE['sbjs_current']);
    setcookie('sbjs_current', '', time() - 3600, '/');
}
if(isset($_COOKIE['sbjs_current_add'])) {
    unset($_COOKIE['sbjs_current_add']);
    setcookie('sbjs_current_add', '', time() - 3600, '/');
}
if(isset($_COOKIE['sbjs_first'])) {
    unset($_COOKIE['sbjs_first']);
    setcookie('sbjs_first', '', time() - 3600, '/');
}
if(isset($_COOKIE['sbjs_first_add'])) {
    unset($_COOKIE['sbjs_first_add']);
    setcookie('sbjs_first_add', '', time() - 3600, '/');
}
if(isset($_COOKIE['sbjs_migrations'])) {
    unset($_COOKIE['sbjs_migrations']);
    setcookie('sbjs_migrations', '', time() - 3600, '/');
}
if(isset($_COOKIE['sbjs_session'])) {
    unset($_COOKIE['sbjs_session']);
    setcookie('sbjs_session', '', time() - 3600, '/');
}
if(isset($_COOKIE['sbjs_udata'])) {
    unset($_COOKIE['sbjs_udata']);
    setcookie('sbjs_udata', '', time() - 3600, '/');
}