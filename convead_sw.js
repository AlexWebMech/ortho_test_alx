/*
*
*  Convead Service Worker
*
*/

/* eslint-env browser, serviceworker, es6 */

'use strict';
self.addEventListener('push', function(event) {
  const data = JSON.parse(event.data.text());

  const title = data.message.title;
  var options = {
    body: data.message.text,
    icon: data.message.icon,
    badge: data.message.badge
  };

  if (data.message.image) {
    options.image = data.message.image;
  }

  self.link = data.message.link


  if (data.message.created_at) {
    const date = new Date(data.message.created_at)
    date.setDate(date.getDate() + 1)
    
    if (date >= new Date) {
      const notificationPromise = self.registration.showNotification(title, options);
      event.waitUntil(notificationPromise);
      
    }
  }

});

self.addEventListener('notificationclick', function(event) {
  console.log('[Service Worker] Notification click Received.');

  event.notification.close();

  event.waitUntil(
    clients.openWindow(self.link)
  );
});
