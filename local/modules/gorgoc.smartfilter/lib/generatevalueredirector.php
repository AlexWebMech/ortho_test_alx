<?php


namespace Gorgoc\Smartfilter;

\Bitrix\Main\Loader::includeModule('iblock');


class GenerateValueRedirector {
    
    /**
     * Таблица значений св-в тип список
     * @var string
     */
    protected $tablePropEnum = 'b_iblock_property_enum';
    
    /**
     * Таблица сво-в
     * @var string
     */
    protected $tableProperty = 'b_iblock_property';
    
    /**
     * Таблица элементов и привязанных к ним св-в
     * @var string
     */
    protected $tableElementProperty = 'b_iblock_element_property';
    
    /**
     * Инфоблок
     */
    protected $iblockId = 94;
    
    public function isFilter():bool {
        $context = \Bitrix\Main\Application::getInstance()->getContext();
        $request = $context->getRequest();
        $uri = $request->getRequestUri();
        
        if (stripos($uri, '/filter/') === false) {
            return false;
        }
        
        if (stripos($uri, '/apply/') === false) {
            return false;
        }

//        if(preg_match('/[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12}/',$uri)===1){
//            return true;
//        }
        
        return true;
    }
    
    
    /**
     * Вернёт новый URI
     * @param string $uri
     * @return string
     */
    public function getSeoUrl(): string {
        
        $context = \Bitrix\Main\Application::getInstance()->getContext();
        $request = $context->getRequest();
        $uri = $request->getRequestUri();
        
        foreach ($this->getHashTable() as $key => $item) {
            $uri = str_replace($item['OLD_URI'], $item['NEW_URI'], $uri);
        }
        
        return $uri;
    }
    
    /**
     * Хэш таблица уникальных UUID
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function getHashTable(): array {
        $result = [];
        $dbQuery = RedirectorTable::getList(
            [
                'select' => ['CODE_LOWER', 'VALUE_TRANSLIT', 'XML_ID','VALUE_ID'],
                'cache' => ['ttl' => 43200],
            ]
        );
        
        if(!method_exists($dbQuery,'fetchAll')) {
            return [];
        }
        
        $items = $dbQuery->fetchAll();
        
        foreach ($items as $tmp) {
            //$key ="{$tmp['CODE_LOWER']}-is-{$tmp['XML_ID']}";
            $key='is'.$tmp['VALUE_ID'];
            $tmp['OLD_URI']="{$tmp['CODE_LOWER']}-is-{$tmp['XML_ID']}";
            $tmp['NEW_URI']="{$tmp['CODE_LOWER']}-is-{$tmp['VALUE_TRANSLIT']}";
            $result[$key] = $tmp;
        }
        
        foreach ($items as $tmp) {
            //$key ="-or-{$tmp['XML_ID']}";
            $key='or'.$tmp['VALUE_ID'];
            $tmp['OLD_URI']="or-{$tmp['XML_ID']}";
            $tmp['NEW_URI']="or-{$tmp['VALUE_TRANSLIT']}";
            $result[$key] = $tmp;
        }
        
        return $result;
    }
    
    
    /**
     * Заполняет таблицу свойствами и значениями для работы редиректора
     */
    public function generate() {
        foreach ($this->getProperty() as $item) {
            $this->setPropertyEnumValue($item);
        }
    }
    
    /**
     * Запускаемый агент для генерации данных в таблицу
     * @return string
     */
    public static function generateAgent() {
        $class=new self();
        $class->generate();
        return __METHOD__.'();';
    }
    
    /**
     * Информация о свойствах
     * @return array
     */
    protected function getProperty(): array {
        global $DB;
        
        $query = <<<EOT
                SELECT
                prop.CODE,
                prop.NAME,
                prop.XML_ID,
                prop.IBLOCK_ID,
                prop.ID
                FROM {$this->tableProperty} AS prop
                WHERE
                prop.IBLOCK_ID={$this->iblockId}
                AND
                prop.PROPERTY_TYPE='L'
EOT;
        
        $dbQuery = $DB->query($query);
        
        $result = [];
        
        while ($tmp = $dbQuery->fetch()) {
            $result[] = $tmp;
        }
        
        return $result;
        
    }
    
    /**
     * @param int $propertyId
     * @return array
     */
    protected function setPropertyEnumValue(array $property): bool {
        global $DB;
        
        $query = <<<EOT
                    SELECT
                    enum_value.ID,
                    enum_value.PROPERTY_ID,
                    enum_value.VALUE,
                    enum_value.XML_ID
                    FROM b_iblock_property_enum AS enum_value
                    WHERE
                    CHAR_LENGTH(enum_value.XML_ID)>1
                    AND
                    enum_value.PROPERTY_ID={$property['ID']}
EOT;
        $dbQuery = $DB->query($query);
        
        
        while ($tmp = $dbQuery->fetch()) {
            
            $field = [
                'CODE' => $property['CODE'],
                'CODE_LOWER' => ToLower($property['CODE']),
                'VALUE' => $tmp['VALUE'],
                'VALUE_TRANSLIT' => \CUtil::translit(toLower($tmp['VALUE']), "ru"),
                'XML_ID' => $tmp['XML_ID'],
                'VALUE_ID' => $tmp['ID'],
                'PROPERTY_XML_ID' => $property['XML_ID'],
                'PROPERTY_NAME' => $property['NAME'],
                'PROPERTY_ID' => $property['ID']
            ];
            
            $check = RedirectorTable::getList(
                [
                    'select' => ['ID'],
                    'filter' => ['=PROPERTY_XML_ID' => $property['XML_ID'], '=XML_ID' => $tmp['XML_ID']],
                    'limit' => 1
                ]
            )->fetch();
            
            if (!empty($check['ID'])) {
                RedirectorTable::update($check['ID'], $field);
            } else {
                RedirectorTable::add($field);
            }
            
        }
        
        return true;
        
    }
    
}