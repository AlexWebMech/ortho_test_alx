<?php


namespace Gorgoc\Smartfilter;

use Bitrix\Main\Entity;


class RedirectorTable extends Entity\DataManager {
    
    public static function getFilePath() {
        return __FILE__;
    }
    
    public static function getTableName() {
        return 'gorgoc_smartfilter_redirector';
    }
    
    public static function getMap() {
        return [
            new Entity\IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                ]
            ),
            new Entity\StringField(
                'CODE',
                [
                    'required' => false
                ]
            ),
            
            new Entity\StringField(
                'CODE_LOWER',
                [
                    'required' => false
                ]
            ),
            new Entity\StringField(
                'VALUE',
                [
                    'required' => false
                ]
            ),
            new Entity\StringField(
                'VALUE_TRANSLIT',
                [
                    'required' => false
                ]
            ),
            new Entity\StringField(
                'XML_ID',
                [
                    'required' => false
                ]
            ),
            new Entity\IntegerField(
                'VALUE_ID',
                [
                    'required' => false
                ]
            ),
            new Entity\StringField(
                'PROPERTY_XML_ID',
                [
                    'required' => false
                ]
            ),
            new Entity\StringField(
                'PROPERTY_NAME',
                [
                    'required' => false
                ]
            ),
            new Entity\IntegerField(
                'PROPERTY_ID',
                [
                    'required' => false
                ]
            ),
            new Entity\TextField(
                'URL_OLD',
                [
                    'required' => false
                ]
            ),
            new Entity\TextField(
                'URL_NEW',
                [
                    'required' => false
                ]
            ),
            new Entity\DatetimeField(
                'DATE_UPDATE',
                [
                    'required' => false
                ]
            )
        ];
    }
    
}