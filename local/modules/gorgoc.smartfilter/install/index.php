<?php

IncludeModuleLangFile(__FILE__);

class gorgoc_smartfilter extends CModule {
    
    
    public $MODULE_ID = "gorgoc.smartfilter";
    
    public $MODULE_VERSION;
    
    public $MODULE_VERSION_DATE;
    
    public $MODULE_NAME;
    
    public $MODULE_DESCRIPTION;
    
    /**
     * Агенты создаваемые и удаляемые модулем
     *
     */
    protected $agentsModule = [];
    
    function __construct() {
        
        $arModuleVersion = [];
        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");
        
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->PARTNER_NAME = GetMessage("GORGOC_SMARTFILTER_COMPANY_NAME");
        $this->PARTNER_URI = GetMessage("GORGOC_SMARTFILTER_COMPANY_URI");
        $this->MODULE_NAME = GetMessage("GORGOC_SMARTFILTER_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("GORGOC_SMARTFILTER_MODULE_DESC");
        //Агенты
        $this->agentsModule[]=['MODULE' => $this->MODULE_ID, 'AGENT' => '\Gorgoc\Smartfilter\GenerateValueRedirector::generateAgent();'];
        return true;
    }
    
    function DoInstall() {
        global $USER;
        if ($USER->IsAdmin()) {
            RegisterModule($this->MODULE_ID);
            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();
            $this->createAgents();
        }
        return true;
    }
    
    function InstallDB() {
        global $DB;
        
        if ($this->issetTable('gorgoc_smartfilter_redirector')) {
            return true;
        }
        $query = <<<EOT
                CREATE TABLE `gorgoc_smartfilter_redirector` (
                    `ID` INT(11) NOT NULL AUTO_INCREMENT,
                    `CODE` VARCHAR(300) NULL DEFAULT '0',
                    `CODE_LOWER` VARCHAR(300) NULL DEFAULT '0',
                    `VALUE` VARCHAR(300) NULL DEFAULT '0',
                    `VALUE_TRANSLIT` VARCHAR(300) NULL DEFAULT '0',
                    `XML_ID` VARCHAR(300) NULL DEFAULT '0',
                    `VALUE_ID` INT(11) NULL DEFAULT '0',
                    `PROPERTY_XML_ID` VARCHAR(300) NULL DEFAULT '0',
                    `PROPERTY_NAME` VARCHAR(300) NULL DEFAULT '0',
                    `PROPERTY_ID` INT(11) NULL DEFAULT '0',
                    `URL_OLD` TEXT NULL,
                    `URL_NEW` TEXT NULL,
                    `DATE_UPDATE` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`ID`),
                    UNIQUE INDEX `XML_ID` (`XML_ID`),
                    INDEX `CODE_LOWER` (`CODE_LOWER`),
                    INDEX `VALUE_TRANSLIT` (`VALUE_TRANSLIT`)
                )
                COLLATE='cp1251_general_ci'
                ENGINE=InnoDB
                AUTO_INCREMENT=1;

EOT;
        $DB->query($query);
        
        return true;
    }
    
    public function issetTable($tableName) {
        global $DB;
        $query=<<<EOT
            select ID from {$tableName} where 0
EOT;
        $resQuery = $DB->query($query,true,true);
        if ($resQuery) {
            return true;
        } else {
            return false;
        }
    }
    
    function InstallEvents() {
        return true;
    }
    
    function InstallFiles() {
        
        return true;
    }
    
    function createAgents() {
        if (empty($this->agentsModule)) {
            return true;
        }
        foreach ($this->agentsModule as $agent) {
            $curTime = \Bitrix\Main\Type\DateTime::createFromTimestamp(time());
            $periodAgent = 'N';
            $intervalAgent = '43200';
            $datecheckAgent = $curTime;
            $activeAgent = 'Y';
            $next_exAgent = $curTime; //Дата первого запуска агента
            $sortAgent = 40;
            \CAgent::AddAgent(
                $agent['AGENT'],
                $agent['MODULE'],
                $periodAgent,
                $intervalAgent,
                $datecheckAgent,
                $activeAgent,
                $next_exAgent,
                $sortAgent
            );
        }
        return true;
    }
    
    function DoUninstall() {
        global $USER;
        if ($USER->IsAdmin()) {
            $this->UnInstallDB();
            $this->UnInstallEvents();
            $this->UnInstallFiles();
            $this->deleteAgents();
            UnRegisterModule($this->MODULE_ID);
        }
        return true;
    }
    
    function UnInstallDB() {
        
        return true;
    }
    
    function UnInstallEvents() {
        return true;
    }
    
    function UnInstallFiles() {
        
        return true;
    }
    
    function deleteAgents() {
        return true;
    }
    
    public function clearTable($tableName) {
        return true;
    }
    
}

?>