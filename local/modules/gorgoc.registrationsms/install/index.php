<?php

IncludeModuleLangFile(__FILE__);

class gorgoc_registrationsms extends CModule
{
    
    
    public $MODULE_ID = "gorgoc.registrationsms";
    
    public $MODULE_VERSION;
    
    public $MODULE_VERSION_DATE;
    
    public $MODULE_NAME;
    
    public $MODULE_DESCRIPTION;
    
    /**
     * Агенты создаваемые и удаляемые модулем
     *
     */
    protected $agentsModule = [];
    
    function __construct()
    {
        
        $arModuleVersion = [];
        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");
        
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->PARTNER_NAME = GetMessage("GORGOC_REGISTRATIONSMS_COMPANY_NAME");
        $this->PARTNER_URI = GetMessage("GORGOC_REGISTRATIONSMS_COMPANY_URI");
        $this->MODULE_NAME = GetMessage("GORGOC_REGISTRATIONSMS_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage(
            "GORGOC_REGISTRATIONSMS_MODULE_DESC"
        );
        return true;
    }
    
    function DoInstall()
    {
        global $USER;
        if ($USER->IsAdmin()) {
            RegisterModule($this->MODULE_ID);
            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();
            $this->createAgents();
        }
        return true;
    }
    
    function InstallDB()
    {
        
        return true;
    }
    
    function InstallEvents()
    {
        return true;
    }
    
    function InstallFiles()
    {
        
        return true;
    }
    
    function createAgents()
    {
        if (empty($this->agentsModule)) {
            return true;
        }
        foreach ($this->agentsModule as $agent) {
            $curTime = \Bitrix\Main\Type\DateTime::createFromTimestamp(time());
            $periodAgent = 'N';
            $intervalAgent = '43200';
            $datecheckAgent = $curTime;
            $activeAgent = 'Y';
            $next_exAgent = $curTime; //Дата первого запуска агента
            $sortAgent = 40;
            \CAgent::AddAgent(
                $agent['AGENT'],
                $agent['MODULE'],
                $periodAgent,
                $intervalAgent,
                $datecheckAgent,
                $activeAgent,
                $next_exAgent,
                $sortAgent
            );
        }
        return true;
    }
    
    public function issetTable($tableName)
    {
        global $DB;
        $query = <<<EOT
            select ID from {$tableName} where 0
EOT;
        $resQuery = $DB->query($query, true, true);
        if ($resQuery) {
            return true;
        } else {
            return false;
        }
    }
    
    function DoUninstall()
    {
        global $USER;
        if ($USER->IsAdmin()) {
            $this->UnInstallDB();
            $this->UnInstallEvents();
            $this->UnInstallFiles();
            $this->deleteAgents();
            UnRegisterModule($this->MODULE_ID);
        }
        return true;
    }
    
    function UnInstallDB()
    {
        
        return true;
    }
    
    function UnInstallEvents()
    {
        return true;
    }
    
    function UnInstallFiles()
    {
        
        return true;
    }
    
    function deleteAgents()
    {
        return true;
    }
    
    public function clearTable($tableName)
    {
        return true;
    }
    
}

