<?php


namespace Gorgoc\Registrationsms;



/**
 * Сливает временную учёку с основной
 * Class Mergingaccounts
 *
 * @package Gorgoc\Registrationsms
 */
class MergingAccounts
{
    
    /**
     * Вешается на событие OnAfterUserAuthorize
     * Выполняет объединение временных учётных записпей с основной
     *
     * @param $arUser
     *
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\NotImplementedException*@throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\LoaderException
     */
    public function OnAfterUserAuthorizeHandler($arUser) {
        \Bitrix\Main\Loader::includeModule("mlife.smsservices");
        \Bitrix\Main\Loader::includeModule("sale");
        
        $phone = Helpers::isMobilPhone11($arUser['user_fields']["LOGIN"]);
        
        if(empty($phone)) {
            return;
        }
        
        $obSmsServ = new \CMlifeSmsServices();
        $phoneCheck = $obSmsServ->checkPhoneNumber($arUser['user_fields']["LOGIN"]);
        
        if(!$phoneCheck['check']) {
            return;
        }
        
        // $phone = Helpers::getNumber($phoneCheck['phone']);
        $filterGroup = [
            "ACTIVE"         => "Y",
            'PERSONAL_PHONE' => $phone,
        ];
        
        $arUsersOld = [];
        
        $rsUsers = \CUser::GetList($by = "id", $order = "desc", $filterGroup, array());
        while ($arUSR = $rsUsers->Fetch()) {
            $ar["USR"]["USR"][] = $arUSR;
            if (strpos($arUSR["LOGIN"], "order") !== false) {
                $arUsersOld[] = $arUSR["ID"];
            }
        }
        $ar["USR"]["_OLD"] = $arUsersOld;
        
        
        
        foreach ($arUsersOld as $usrId) {
            $arFilter = ["USER_ID" => $usrId];
            $db_sales = \CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter);
            while ($ar_sales = $db_sales->Fetch()) {
                
                $order = \Bitrix\Sale\Order::load($ar_sales["ID"]);
                
                $basket = $order->getBasket();
                
                if (empty($basket->getQuantityList())) {//Заказ без товаров
                    $res = $order->deleteNoDemand($ar_sales["ID"]); //Удаляем заказ без товаров
                    unset($order);
                    continue;
                }
                
                \CSaleOrder::Update(
                    $ar_sales["ID"],
                    ["USER_ID" => $arUser['user_fields']["ID"]]
                );
            }
            \CUser::Delete($usrId);
        }
        
        
    }
}