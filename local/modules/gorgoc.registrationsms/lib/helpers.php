<?php

namespace Gorgoc\Registrationsms;

class Helpers
{
    
    /**
     * Вернёт телефон или false если он не 11-ь знаков
     *
     * @param string $phone мобильный телефон или строка его содержащий
     *
     * @return boolean|int Номер телефона или false
     */
    public static function isMobilPhone11($phone)
    {
        $value = self::getNumber($phone);
        if (empty($value)) {
            return false;
        }
        if (strlen($value) !== 11) {
            return false;
        }
        
        return $value;
    }
    
    public static function getNumber($value)
    {
        if(empty($value)) {
            return null;
        }
        return preg_replace("/[^0-9]/", '', $value);
    }
    
    /**
     * Вернёт первый возможный телефон из строки
     * 10-и значный дополнитя до 11-и
     * @param $phone
     *
     * @return bool|int|string
     */
    public static function getFirstPhoneFromString($phone)
    {
        if( self::isMobilPhone11($phone)) {
            return self::isMobilPhone11($phone);
        }
        if( self::isMobilPhone10($phone)) {
            return "7".self::isMobilPhone10($phone);
        }
        $phone = trim($phone);
        $phone = str_replace('  ',' ',$phone);
        $phones = [];
        if(stripos($phone,',')!==false) {
            $phones += explode(',',$phone);
        }
        if(stripos($phone,' ')!==false) {
            $phones += explode(' ',$phone);
        }
        if(stripos($phone,';')!==false) {
            $phones += explode(';',$phone);
        }

        foreach ($phones as $item) {
            if( self::isMobilPhone11($item)) {
                return self::isMobilPhone11($item);
            }elseif(self::isMobilPhone10($item)){
                return "7".self::isMobilPhone10($item);
            }
        }
        return '';
    }
    
    /**
     * Вернёт телефон или false если он не 10-ь знаков
     *
     * @param string $phone мобильный телефон или строка его содержащий
     *
     * @return boolean|int Номер телефона или false
     */
    public static function isMobilPhone10($phone)
    {
        $value = self::getNumber($phone);
        if (empty($value)) {
            return false;
        }
        if (strlen($value) !== 10) {
            return false;
        }
        
        return $value;
    }
    
    /**
     * Вернёт текущий домен с http или https
     *
     * @return string
     */
    public static function getRootUrl()
    {
        $root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://'
            . $_SERVER['HTTP_HOST'];
        return $root;
    }
    
    /**
     * Выводит отформатированный вид сотового номера
     *
     * @param string $phone номер телефона 10 или 11 знаков
     * @param boolean $trim если больше 11, возьмёт до 11
     *
     * @return string +x(xxx) xxx-xx-xx или (xxx) xxx-xx-xx
     */
    public static function format_phone($phone = '', $trim = true)
    {
        if (empty($phone)) {
            return '';
        }
        
        $phone = self::getNumber($phone);
        
        if ($trim == true && strlen($phone) > 11) {
            $phone = substr($phone, 0, 11);
        }
        
        
        if (strlen($phone) == 7) {
            return preg_replace(
                "/([0-9a-zA-Z]{3})([0-9a-zA-Z]{4})/", "$1-$2", $phone
            );
        } elseif (strlen($phone) == 10) {
            return preg_replace(
                "/([0-9a-zA-Z]{3})([0-9a-zA-Z]{3})([0-9a-zA-Z]{2})([0-9a-zA-Z]{2})/",
                "($1) $2-$3-$4", $phone
            );
        } elseif (strlen($phone) == 11) {
            return preg_replace(
                "/([0-9a-zA-Z]{1})([0-9a-zA-Z]{3})([0-9a-zA-Z]{3})([0-9a-zA-Z]{2})([0-9a-zA-Z]{2})/",
                "+$1($2) $3-$4-$5", $phone
            );
        }
        
        return $phone;
    }
    
    
}
