<?php

namespace Gorgoc\Registrationsms;

use Bitrix\Main\Entity;


class FormsTable extends Entity\DataManager
{
    
    const TYPE_REGISTRATION = 'registration';
    const TYPE_AUTHORIZATION = 'authorization';
    
    const ACTIVE_ON = 1;
    const ACTIVE_OFF = 0;
    
    public static function getFilePath()
    {
        return __FILE__;
    }
    
    public static function getTableName()
    {
        return 'gorgoc_registrationsms_forms';
    }
    
    public static function getMap()
    {
        return [
            'ID'          =>
                [
                    'data_type' => 'integer',
                    'primary'   => true,
                ],
            'ACTIVE'      =>
                [
                    'data_type' => 'integer',
                ],
            'CODE'        =>
                [
                    'data_type' => 'integer', //Код смс
                ],
            'PHONE' =>
                [
                    'data_type' => 'string',
                ],
            'USER_ID' =>
                [
                    'data_type' => 'integer',
                ],
            'SSID' =>
                [
                    'data_type' => 'string',
                ],
            'TYPE'        =>
                [
                    'data_type' => 'string',
                ],
            'FORM'     =>
                [
                    'data_type' => 'text',
                ],
            'DATE_INSERT' =>
                [//Время записи
                 'data_type' => 'datetime',
                 'title'     => 'Дата и время отправки',
                ],
        ];
    }
    
    public static function onBeforeAdd(\Bitrix\Main\Entity\Event $event)
    {
        
        $result = new \Bitrix\Main\Entity\EventResult();
        
        $arFields = $event->getParameter("fields");
        
        $arFields['DATE_INSERT'] = new \Bitrix\Main\Type\DateTime();
        
        $result->modifyFields($arFields);
        
        return $result;
    }
    
    public static function onBeforeUpdate(\Bitrix\Main\Entity\Event $event)
    {
        
        $result = new \Bitrix\Main\Entity\EventResult();
        
        $arFields = $event->getParameter("fields");
        
        
        $result->modifyFields($arFields);
        
        return $result;
    }
    
}
