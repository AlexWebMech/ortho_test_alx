<?php


namespace Gorgoc\Registrationsms;


class Service
{
    
    public function __construct()
    {
    }
    
    /**
     *
     * Обновляет учётки битрикс согласно данных таблицы
     */
    public function updateBitrixAccount()
    {
      $dbQuery = \Gorgoc\Registrationsms\OldAccountsTable::getList([]);
      
      while ($account = $dbQuery->fetch()) {
         $user = new \CUser();
         $result = $user->Update($account['USER_ID'],['LOGIN' =>$account['LOGIN'],'PERSONAL_PHONE' => $account['PHONE'], "TYPE_UPDATE"=>"SMS"]);//TYPE_UPDATE - хак для обхода события
         if(!$result) {
             echo $user->LAST_ERROR.'<br>';
         }
         // break;
      }
    }
    
    /**
     *
     * Операция обратная updateBitrixAccount
     */
    public function backUpdateBitrixAccount()
    {
        $dbQuery = \Gorgoc\Registrationsms\OldAccountsTable::getList([]);
        
        while ($account = $dbQuery->fetch()) {
            $user = new \CUser();
            $result = $user->Update($account['USER_ID'],['LOGIN' =>$account['OLD_LOGIN'],'PERSONAL_PHONE' =>$account['OLD_PHONE'],"TYPE_UPDATE"=>"SMS"]);
             if(!$result) {
                 echo $user->LAST_ERROR.'<br>';
             }
        }
    }
    
    /**
     * Только запись в таблицу
     * Сохранит в таблицу Учётки кандидаты на изменение логина и телефона
     */
    public function keepRecordsTable()
    {
        $fields = $this->getAllUserRecordsToChange();
        foreach ($fields as $item) {
            $check = \Gorgoc\Registrationsms\OldAccountsTable::getList([
                'filter'=>['=USER_ID'=>$item['USER_ID']],
                'limit'=>1
            ])->fetch();
            if(!$check){
                \Gorgoc\Registrationsms\OldAccountsTable::add($item);
            }
        }
    }
    
    protected function getAllUserRecordsToChange()
    {
        $result = [];
        
        $filter = ["GROUPS_ID" => [18], '!PERSONAL_PHONE' => false];
        
        $dbObject = \CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
        $i=0;
        $j=0;
        while ($tmp = $dbObject->fetch()) {
            $phone = (string)Helpers::getNumber(($tmp['PERSONAL_PHONE']??'1'));
            if (strlen($phone)===10) {
                $phone = "7".$phone;
            }elseif (strlen($phone)===11) {
                $phone = "7".substr($phone,1);
            }else {
                continue;
            }
            $result[] = [
                'USER_ID' => $tmp['ID'],
                'LOGIN' =>$phone,
                'OLD_LOGIN' =>$tmp['LOGIN'],
                'PHONE' => $phone,
                'OLD_PHONE' =>$tmp['PERSONAL_PHONE']??null,
                'EMAIL'=>$tmp['EMAIL'],
                'NAME'=>$tmp['NAME'],
                'PROFILE' => \Bitrix\Main\Web\Json::encode($tmp)
            ];
            
        }
        
        return $result;
        
        
    }
    
}