<?php


namespace Gorgoc\Registrationsms;


class OrderEvents
{
    /** @var array Свойства с которыми работает класс */
    protected $orderProperty = [
    
    ];
    

    public function OnBeforeUserAdd(&$arParams)
    {
        
        $registration = new \Gorgoc\Registrationsms\Registration($arParams);
        $result = $registration->OnBeforeUserAdd();
        
        $arParams = array_merge($arParams,$result);
        $arParams['PERSONAL_PHONE'] = $registration->getPhone();
        return $arParams;
        
//
    }
    /**
     * Вешается на события оформления заказ (заказ уже оформлен, нужно получить поля)
     * Main\EventManager::getInstance()->addEventHandler(
    'sale',
    'OnSaleOrderBeforeSaved',
    'saleOrderBeforeSaved'
    );
     */
    public function saleOrderBeforeSaved(\Bitrix\Main\Event $event)
    {
        
        /** @var \Bitrix\Sale\Order $order */
        $order = $event->getParameter("ENTITY");
        
        if((int)$order->getId()<1){
            return new \Bitrix\Main\EventResult(1);
        }
        
        /** @var \Bitrix\Sale\PropertyValueCollection $propertyCollection */
        $propertyCollection = $order->getPropertyCollection();
        
        $propsData = $propertyCollection->getArray();
        
        if(empty($propsData)) {
            return new \Bitrix\Main\EventResult(1);
        }

        $profile=[];
        $profile['ORDER_ID'] = $order->getId();
        foreach ($propsData['properties'] as $item) {
            if($item['PERSON_TYPE_ID']!='1') {
                continue;
            }
            if($item['ID']=='1') {//имя
                $profile['NAME'] = $item['VALUE'][0]??null;
            }
            if($item['ID']=='2') {//фамилия
                $profile['LAST_NAME'] = $item['VALUE'][0]??null;
            }
            if($item['ID']=='3') {//Телефон
                $profile['PERSONAL_PHONE'] = Helpers::getNumber($item['VALUE'][0]??'');
            }
            if($item['ID']=='4') {//email
                $profile['EMAIL'] = $item['VALUE'][0]??null;
            }
        }
    
        // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/new_order_event1.txt',var_export($profile,true));
    
        $registration = new \Gorgoc\Registrationsms\Registration($profile);
        $registration->init();
    
        if($registration->getUserId()) {
            \Bitrix\Main\Loader::includeModule("sale");
            
            //\CSaleOrder::Update($order->getId(),array("USER_ID" => $registration->getUserId()));
            $tableOrder = \Bitrix\Sale\Internals\OrderTable::getTableName();
            global $DB;
            $DB->query("UPDATE {$tableOrder} SET USER_ID='{$registration->getUserId()}' WHERE ID='{$order->getId()}'");
        }

        return new \Bitrix\Main\EventResult(1);
        
        
    }
    
}