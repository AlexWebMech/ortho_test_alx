<?php

namespace Gorgoc\Registrationsms;

/**
 * Создаёт пользователя или временный профиль пользователя
 *
 * Class Registration
 *
 * @package Gorgoc\Registrationsms
 */
class Registration
{
    
    /**
     * @var array Группа пользователей по умолчанию
     */
    
    protected $user_group = [18];
    
    
    protected $profile = [];
    /**
     * ИД пользователя после создания или его нахождения в битрикс
     * @var int
     */
    protected $user_id = null;
    
    /**
     * Имя пользователя
     * @var string
     */
    protected $user_name = null;
    
    /** @var string */
    protected $user_phone = null;
    
    /**
     * Сгенерированый пароль нового пользователя
     * @var string
     */
    protected $user_pswd = null;
    
    /**
     * Новый пользователь?
     * @var bool
     */
    protected $user_isnew = false;
    
    /**
     * true - пользователь был создан, null - найден
     * @var bool
     */
    protected $user_create = null;
    
    public function __construct(array $profile)
    {
        $defaultProfile = [
            'LOGIN' => '',
            'NAME' =>  '',
            'LAST_NAME' =>  '',
            'EMAIL' =>  '',
            'GROUP_ID' =>  '',
            'ACTIVE' => 'Y',
            'LID' => '',
            'PERSONAL_PHONE' => '',
            'PERSONAL_ZIP' => '',
            'PERSONAL_STREET' =>  '',
        ];
        
        if(!empty($defaultProfile['GROUP_ID'])) {
            $this->user_group = $defaultProfile['GROUP_ID'];
        }
        
        $profile = array_merge($defaultProfile,$profile);
        
        $profile['PERSONAL_PHONE'] = Helpers::getFirstPhoneFromString($profile['PERSONAL_PHONE']);
        
        $this->profile = $profile;
    }
    public function getPhone()
    {
        return $this->profile['PERSONAL_PHONE'];
    }
    
    public function getUserId()
    {
        return $this->user_id;
    }
    
    public function getIsNewUser()
    {
        return (bool)$this->user_create;
    }
    
    /**
     * Обновляет пользователя
     */
    public function afterRegisterUserBitrixOrder()
    {
        global $USER;
        
        $arFilter = ["=PERSONAL_PHONE" => $this->profile['PERSONAL_PHONE']];
        $res = \Bitrix\Main\UserTable::getList(
            [
                "select" => ["ID", "NAME", "LOGIN", "PERSONAL_PHONE"],
                "filter" => $arFilter,
                'limit'  => 3,
            ]
        );
    
        $tmp = [];
        $users = [];
        while ($tmp = $res->fetch()) {//Пользователь есть
            $users[] = $tmp;
        }
        $password = rand(100000, 9999999);
        if(count($users)===1 && $users['ID']==$USER->GetId()) {
            
            $user = new \CUser();
            
            $result = $user->Update($tmp['ID'],[
                    "PASSWORD"         => $password,
                    "CONFIRM_PASSWORD" => $password,
                    "TYPE_UPDATE"=>"SMS"]
            );
    
            $mess = 'Ваш пароль для входа на сайт: '.$password;
    
            if (\CModule::IncludeModule("mlife.smsservices") && $result) {
                $obSmsServ = new \CMlifeSmsServices();
                $arSend = $obSmsServ->sendSms($this->profile['PERSONAL_PHONE'], $mess, 0);
            }
            
        }
        
    }
    
    /**
     * Модифицирует пользователя при создании
     * @return array
     */
    public function OnBeforeUserAdd()
    {
        $arFilter = ["=PERSONAL_PHONE" => $this->profile['PERSONAL_PHONE']];
    
        $res = \Bitrix\Main\UserTable::getList(
            [
                "select" => ["ID", "NAME", "LOGIN", "PERSONAL_PHONE"],
                "filter" => $arFilter,
                'limit'  => 1,
            ]
        );

        $arTmp = [];
        if ($arTmp = $res->fetch()) {//Пользователь есть
            $this->user_isnew = false;
        } else {//Пользователя нет
            $this->user_isnew = true;
        }
    
        if ($this->user_isnew) {
            $user_login = $this->profile['PERSONAL_PHONE'];
        } else {
            $user_login = 'order_' . mt_rand(1, 9) . time();
        }
    
        if (!empty($this->profile['EMAIL'])) {
            $email = $this->profile['EMAIL'];
        } else {
            $email = $user_login . '@noemail.gav';
        }
    
        $password = rand(100000, 9999999);
    
        $this->user_pswd = $password;

        $arFields = [
            "NAME"             => $this->profile['NAME'],
            "LAST_NAME"        => $this->profile['LAST_NAME'],
            "EMAIL"            => $email,
            "LOGIN"            => $user_login,
            "LID"              => $this->profile['LID'],
            "ADMIN_NOTES"      => "Регистрация - процедура заказа",
            "PERSONAL_PHONE"   => $this->profile['PERSONAL_PHONE'],
            "ACTIVE"           => "Y",
            "GROUP_ID"         => $this->user_group,
            "PASSWORD"         => $password,
            "CONFIRM_PASSWORD" => $password,
            "TYPE_UPDATE" => "SMS"
        ];
        
        return $arFields;
        
    }
    
    public function init()
    {
        global $USER;
        
        try {
            
            // $this->renameAccountUser();
            
            $this->checkUser();
            
            
            if (!$USER->IsAuthorized()) {
                
                $this->createUser();
                
                if ($this->user_create) {
                    $USER->Authorize($this->user_id);
                }
            }
        }catch (\Exception $e) {
            return;
        }
        return;
    }

//    protected function renameAccountUser()
//    {
//        $arFilter = ["=EMAIL" => $this->profile['EMAIL']];
//
//        $res = \Bitrix\Main\UserTable::getList(
//            [
//                "select" => ["ID", "NAME", "LOGIN", "PERSONAL_PHONE"],
//                "filter" => $arFilter,
//                'limit'  => 1,
//            ]
//        );
//
//        if($tmp = $res->fetch()) {
//            $user = new \CUser();
//            $result = $user->Update($tmp['ID'],['LOGIN' =>$tmp['PERSONAL_PHONE'],"TYPE_UPDATE"=>"SMS"]);
//            if(!$result) {
//                echo $user->LAST_ERROR.'<br>';
//            }
//        }
//    }
    
    /**
     * Проверяем есть ли пользователь в системе
     *
     * @global type $USER
     * @Если пользователя нет заполняет $this->user_isnew=true
     */
    protected function checkUser()
    {
        global $USER;
        
        
        
        if ($USER->IsAuthorized()) {
//Пользователь авторизован
            $rsUser = \CUser::GetByID($USER->GetID());
            $arUser = $rsUser->Fetch();
            $this->user_id = $arUser['ID'];
            $this->user_name = $arUser['NAME'];
            $this->user_phone = $arUser['PERSONAL_PHONE'];
            
            
            $this->user_isnew = false;
        } else {
            $arFilter = ["=PERSONAL_PHONE" => $this->profile['PERSONAL_PHONE']];
            
            $res = \Bitrix\Main\UserTable::getList(
                [
                    "select" => ["ID", "NAME", "LOGIN", "PERSONAL_PHONE"],
                    "filter" => $arFilter,
                    'limit'  => 1,
                ]
            );
            
            
            $arTmp = [];
            if ($arTmp = $res->fetch()) {//Пользователь есть
                $this->user_isnew = false;
            } else {//Пользователя нет
                $this->user_isnew = true;
            }
            
            
        }
    }
    
    /**
     * Перед вызовом нужно вызвать checkUser
     * Заводит пользователя в системе
     *
     * @throws \Exception
     */
    protected function createUser()
    {
        
        if ($this->user_isnew) {
            $user_login = $this->profile['PERSONAL_PHONE'];
        } else {
            $user_login = 'order_' . mt_rand(1, 9) . time();
        }
        
        if (!empty($this->profile['EMAIL'])) {
            $email = $this->profile['EMAIL'];
        } else {
            $email = $user_login . '@noemail.gav';
        }
        
        $password = rand(100000, 9999999);
        
        $this->user_pswd = $password;
        
        $user = new \CUser;
        
        $arFields = [
            "NAME"             => $this->profile['NAME'],
            "LAST_NAME"        => $this->profile['LAST_NAME'],
            "EMAIL"            => $email,
            "LOGIN"            => $user_login,
            "LID"              => $this->profile['LID']??'s1',
            "ADMIN_NOTES"      => "Регистрация - процедура заказа",
            "PERSONAL_PHONE"   => $this->profile['PERSONAL_PHONE'],
            "ACTIVE"           => "Y",
            "GROUP_ID"         => $this->user_group,
            "PASSWORD"         => $password,
            "CONFIRM_PASSWORD" => $password,
            'PERSONAL_ZIP' => $this->profile['PERSONAL_ZIP']??'',
            'PERSONAL_STREET' =>  $this->profile['PERSONAL_STREET']??'',
            "TYPE_UPDATE" => "SMS"
        ];
        
        $create_id = $user->Add($arFields);
        
        if (intval($create_id) > 0) {
            $this->user_id = $create_id;
            $this->user_name = $this->profile['NAME'];
            $this->user_phone = $this->profile['PERSONAL_PHONE'];
            $this->user_create = true;
        } else {
            
            throw new \Exception(
                'Ошибка создания пользователя: ' . $user->LAST_ERROR
            );
        }
    }
    
}