<?php

namespace Gorgoc\Registrationsms;

use Bitrix\Main\Entity;


class OldAccountsTable extends Entity\DataManager
{
    
    public static function getFilePath()
    {
        return __FILE__;
    }
    
    public static function getTableName()
    {
        return 'gorgoc_registrationsms_old_accounts';
    }
    
    public static function getMap()
    {
        return [
            new Entity\IntegerField(
                'ID',
                [
                    'primary'      => true,
                    'autocomplete' => true,
                ]
            ),
            new Entity\IntegerField(
                'USER_ID',
                [
                    'required' => false
                ]
            ),
            
            new Entity\StringField(
                'LOGIN',
                [
                    'required' => false
                ]
            ),
            new Entity\StringField(
                'OLD_LOGIN',
                [
                    'required' => false
                ]
            ),
            new Entity\StringField(
                'PHONE',
                [
                    'required' => false
                ]
            ),
            new Entity\StringField(
                'OLD_PHONE',
                [
                    'required' => false
                ]
            ),
            new Entity\StringField(
                'EMAIL',
                [
                    'required' => false
                ]
            ),
            new Entity\StringField(
                'NAME',
                [
                    'required' => false
                ]
            ),
            new Entity\TextField(
                'PROFILE',
                [
                    'required' => false
                ]
            ),
            new Entity\DatetimeField(
                'DATE_UPDATE',
                [
                    'required' => false
                ]
            )
        ];
    }
    
    
}
