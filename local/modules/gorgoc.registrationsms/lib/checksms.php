<?php

namespace Gorgoc\Registrationsms;

use Bitrix\Main\Entity;


class ChecksmsTable extends Entity\DataManager
{
    
    public static function getFilePath()
    {
        return __FILE__;
    }
    
    public static function getTableName()
    {
        return 'gorgoc_registrationsms_checksms';
    }
    
    public static function getMap()
    {
        return [
            'ID'          =>
                [
                    'data_type' => 'integer',
                    'primary'   => true,
                ],
            'ACTIVE'      =>
                [
                    'data_type' => 'integer',
                ],
            'PHONE'       =>
                [
                    'data_type' => 'integer', //Телефон
                ],
            'CODE'        =>
                [
                    'data_type' => 'integer', //Код смс
                ],
            'TYPE'        =>
                [
                    'data_type' => 'string', //Тип записи (произвольно)
                ],
            'MESSAGE'     =>
                [
                    'data_type' => 'text', //Сообщение - заметка (для себя)
                ],
            'DATE_INSERT' =>
                [//Время записи
                 'data_type' => 'datetime',
                 'title'     => 'Дата и время отправки',
                ],
        ];
    }
    
    public static function onBeforeAdd(\Bitrix\Main\Entity\Event $event)
    {
        
        $result = new \Bitrix\Main\Entity\EventResult();
        
        $arFields = $event->getParameter("fields");
        
        $arFields['DATE_INSERT'] = new \Bitrix\Main\Type\DateTime();
        
        $result->modifyFields($arFields);
        
        return $result;
    }
    
    public static function onBeforeUpdate(\Bitrix\Main\Entity\Event $event)
    {
        
        $result = new \Bitrix\Main\Entity\EventResult();
        
        $arFields = $event->getParameter("fields");
        
        
        $result->modifyFields($arFields);
        
        return $result;
    }
    
}
