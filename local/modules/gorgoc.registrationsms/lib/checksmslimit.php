<?php

namespace Gorgoc\Registrationsms;

\Bitrix\Main\Loader::includeModule("gorgoc.registrationsms");

class CheckSmsLimit {
    
    /**
     * Проверка разрешена ли операция за отведённый промежуток времени
     * True - действие разрешеноа,false - нет
     *
     * @param string $phone телефон
     * @param string $type  тип
     * @param int    $limit количество секунд прошедших от последнего добавления значения в таблицу
     *
     * @return boolean
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectException
     */
    public static function isAdd($phone, $type, $limit = 60): bool {

        $date = new \Bitrix\Main\Type\DateTime();

        $date->add("-{$limit} second");

        $arResult = \Gorgoc\Registrationsms\ChecksmsTable::getList([
                    'select' => ['*'],
                    'filter' => ['=PHONE' => $phone, '=TYPE' => $type, '>DATE_INSERT' => $date],
                    'limit' => 1,
                    'order' => ['ID' => 'DESC'],
                    'cache' => [//кэш
                        'ttl' => ($limit + 1),
                    ]
                ])->fetch();


        if (empty($arResult)) {
            return true;
        }

        return false;
    }

}
