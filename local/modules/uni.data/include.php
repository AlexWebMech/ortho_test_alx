<?
require_once(__DIR__ . "/lib/data.php");

$arFiles = Array(
	"/lib/tools/functions.php",
	"/lib/tools/vard.php",
	"/lib/tools/arr.php",
	"/lib/tools/bitrixpicture.php",
	"/lib/tools/flowvar.php",
	"/lib/tools/orm.php",
	"/lib/tools/vars.php",

	"/lib/spaces/bitrix/d7.php",
	"/lib/spaces/bitrix/main/spaces.php",
	// "/lib/spaces/bitrix/iblock/spaces.php",
	//"/lib/spaces/bitrix/catalog/spaces.php",	
	"/lib/spaces/bitrix/sale/spaces.php",
	"/lib/spaces/bitrix/currency/spaces.php",
	
	"/lib/spaces/uni/arr/arr.php",
	"/lib/spaces/uni/json/json.php",
	"/lib/spaces/uni/csv/csv.php",
	"/lib/spaces/uni/xml/xml.php",
	
	"/lib/fields/fields.php",
	"/lib/fields/main.php",
	
	"/lib/modifiers/modifiers.php",
	"/lib/validators/validators.php",
	
	"/lib/templates/template.php",
	
	"/lib/bitrix_components/iblock.php",
);

foreach($arFiles as $sFile)
{
	$sFilePath = __DIR__ . $sFile;
	if (file_exists($sFilePath))
		require_once($sFilePath);
}

?>