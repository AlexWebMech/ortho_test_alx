<?
namespace Uni\Data\Tools;

class BitrixPicture
{
	function ProccessItems (&$arItems, $arParams, $arPrefix = Array("PICTURE", "PREVIEW_PICTURE", "DETAIL_PICTURE"))
	{
		foreach($arItems as &$arItem)
			self::ProccessItem($arItem, $arParams, $arPrefix);
		unset($arItem);
	}
	
	function ProccessItem(&$arItem, $arParams, $arPrefix = Array("PICTURE", "PREVIEW_PICTURE", "DETAIL_PICTURE"))
	{
		foreach($arPrefix as $sPrefix)
		{
			if ($arParams[$sPrefix . "_CREATE"])
			{
				if (! $arItem[$sPrefix] && $arItem[$arParams[$sPrefix . "_CREATE"]])
					$arItem[$sPrefix] = $arItem[$arParams[$sPrefix . "_CREATE"]];	
			}
		}
			
		foreach($arPrefix as $sPrefix)
		{		
			
			if ((($arParams[$sPrefix . "_WIDTH"] > 0 || $arParams[$sPrefix . "_HEIGHT"] > 0) && $arParams[$sPrefix . "_PROCCESS"] != "N") || $arParams[$sPrefix . "_PROCCESS"] == "Y")
			{	
		
				//Resize Picture
				$arItem[$sPrefix] = self::ProccessPicture($arItem[$sPrefix], $arParams, $sPrefix . "_");
				//--
				
				//Scales
				if (is_array($arParams[$sPrefix . "_SCALE"]))
					foreach($arParams[$sPrefix . "_SCALE"] as $fScale)
						$arItem[$sPrefix . "@" . $fScale] = self::ProccessPicture($arItem[$sPrefix], $arParams, $sPrefix . "_", $fScale);
				//--
			}
		}
	}
	
	function ProccessPicture($varPicture, $arParams, $sPrefix = "", $fScale = 1)
	{
		$arSourcePicture = (is_array($varPicture) && $varPicture["ID"] > 0) ? $varPicture : \CFile::GetFileArray($varPicture);
		if (! ($arSourcePicture["ID"] > 0))
			return false;
		
		$iSetWidth = intval($arParams[$sPrefix . "WIDTH"] * $fScale);
		$iSetHeight = intval($arParams[$sPrefix . "HEIGHT"] * $fScale);
		$sResizeType = isset($arParams[$sPrefix . "RESIZE_TYPE"]) ? $arParams[$sPrefix . "RESIZE_TYPE"] : BX_RESIZE_IMAGE_PROPORTIONAL;
		
		if (! $iSetWidth)
			$iSetWidth = 10000;
		if (! $iSetHeight)
			$iSetHeight = 10000;			
		
		$arResizeParams = Array("width" => $iSetWidth, "height" => $iSetHeight);
		
	
		
		$arPictureFilters = (FALSE) ? Array(   Array("name" => "sharpen", "precision" => 35)   ) : NULL;
		$iPictureQuality = (TRUE) ? 95 : NULL;
		
		$arResultPicture = array_change_key_case(\CFile::ResizeImageGet($arSourcePicture, $arResizeParams, $sResizeType, true, $arPictureFilters, NULL, $iPictureQuality), CASE_UPPER);
		if ((is_string($arSourcePicture["DESCRIPTION"]) || is_numeric($arSourcePicture["DESCRIPTION"])) && $arSourcePicture["DESCRIPTION"] !== "")
			$arResultPicture["DESCRIPTION"] = $arSourcePicture["DESCRIPTION"];
		//vard($arResultPicture);
		return $arResultPicture;
	}
	
	public function getPicturesSmart($arPictures, $arParams, $arPrefix = Array("PICTURE", "PREVIEW_PICTURE", "DETAIL_PICTURE"))
	{
		$arReturnInfo = [];
		foreach($arPictures as $sPictureKey => $varPicture)
			if ($varPicture)
				$arReturnInfo[$sPictureKey] = self::getPictureSmart($varPicture, $arParams, $sPrefix);
		return $arReturnInfo;
	}
	
	public function getPictureSmart($varPicture, $arParams, $arPrefix = Array("PICTURE", "PREVIEW_PICTURE", "DETAIL_PICTURE"))
	{
		if (! is_array($varPicture) && is_numeric($varPicture))
			$varPicture = \CFile::GetFileArray($varPicture);
		if (! $varPicture)
			return false;

		$arReturnInfo = ["PICTURE" => $varPicture];
		foreach($arPrefix as $sPrefix)
			if ((($arParams[$sPrefix . "_WIDTH"] > 0 || $arParams[$sPrefix . "_HEIGHT"] > 0) && $arParams[$sPrefix . "_PROCCESS"] != "N") || $arParams[$sPrefix . "_PROCCESS"] == "Y")
				$arReturnInfo[$sPrefix] = self::proccessPicture($varPicture, $arParams, $sPrefix);
			
		return $arReturnInfo;
	}
}














?>