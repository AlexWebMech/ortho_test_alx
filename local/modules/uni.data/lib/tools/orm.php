<?
namespace Uni\Data\Tools;

class Orm
{
	public $arOrder; 
	
	function orderArray(&$arr, $arOrder)
	{
		$GLOBALS["TMP_ORDER_SEQUENCE"] = $arOrder;
		uasort($arr, Array("\\Uni\\Data\\Tools\Orm", "OrderArrayDo"));
		unset($GLOBALS["TMP_ORDER_SEQUENCE"]);
	}
	
	function OrderArrayDo($arItem1, $arItem2)
	{
		$arOrderSequence = Array();
		foreach($GLOBALS["TMP_ORDER_SEQUENCE"] as $field => $by) //Array(field1 => asc, field2 => desc) 
			$arOrderSequence[$field] = ($by != "desc" && $by != "DESC") ? true : false;
		
		foreach($arOrderSequence as $order => $bOrderAsc) :
			if ($arItem1[$order] > $arItem2[$order])
				return ($bOrderAsc) ? true : false; //return (true && $bOrderAsc);
			elseif ($arItem1[$order] < $arItem2[$order])	
				return ($bOrderAsc) ? false : true; //return (false || ! $bOrderAsc);
		endforeach;
		return 0;	
	}
}