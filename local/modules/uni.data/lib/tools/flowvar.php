<?
namespace Uni\Data\Tools;

class FlowVar
{
	public function &get($arItem, $varPath, $varDefaultValue = NULL)
	{
		$arPath = (is_array($varPath)) ? $varPath : Array($varPath);
		if ((string) $varPath[0] === "")
			return $varDefaultValue;
		
		for($i = 0; $i < count($arPath); $i++)
		{	
			$arSetPath = $arPath;
			unset($arSetPath[0]);
			$arSetPath = array_values($arSetPath);	
			
			if (! count($arSetPath))
				return $arItem[$arPath[$i]];
			else
				return self::get($arItem[$arPath[$i]], $arSetPath, $varDefaultValue);
		}	
 	} 

	public function set(&$arItem, $varPath, $varSetValue)
	{
		$arPath = (is_array($varPath)) ? $varPath : Array($varPath);
		
		$arSetPath = $arPath;
		unset($arSetPath[0]);
		$arSetPath = array_values($arSetPath);
		
		if (count($arPath))
			if (! is_null($arPath[0]))
				$arItem[$arPath[0]] = self::set($arItem[$arPath[0]], $arSetPath, $varSetValue);
			else
				$arItem[] = $varSetValue;
		else
			$arItem = $varSetValue;
		
		return $arItem;
	}
}


?>