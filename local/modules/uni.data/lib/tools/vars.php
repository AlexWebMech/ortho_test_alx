<?
namespace Uni\Data\Tools;

class Vars
{
	public function filterEmptyValues($varValue)
	{
		if (! is_array($varValue))
			if (check_string($varValue))
				return $varValue;
			else
				return null;
		else
			foreach($varValue as $sSuvbValue => $varSubValue)
				if (is_array($varSubValue))
					$varSubValue = self::filterEmptyValues($varSubValue);
				elseif (self::filterEmptyValues($varSubValue) === null)
					unset($varValue[$sSuvbValue]);
		return $varValue;
	}
}



?>