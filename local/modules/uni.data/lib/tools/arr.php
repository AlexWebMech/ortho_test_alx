<?
namespace Uni\Data\Tools;

class Arr
{
	public function insertBefore(array &$arr, $sBeforeKey, $sSetKey, $varSetValue)
	{
		$a = false;
		$i = 0;
		foreach($arr as $sKey => $varValue)
		{
			if ($sKey == $sBeforeKey)
			{
				$a = $i;
				break;
			}
			$i++;
		}
		if ($a === false)
			return;
		
		$arr = array_slice($arr, 0, $a, true) + Array($sSetKey => $varSetValue) + array_slice($arr, $a, count($arr) - $a, true);
	}
	
	public function insertAfter(array &$arr, $sBeforeKey, $sSetKey, $varSetValue)
	{
		$a = false;
		$i = 0;
		foreach($arr as $sKey => $varValue)
		{
			$i++;
			if ($sKey == $sBeforeKey)
			{
				$a = $i;
				break;
			}
		}
		if ($a === false)
			return;
		
		$arr = array_slice($arr, 0, $a, true) + Array($sSetKey => $varSetValue) + array_slice($arr, $a, count($arr) - $a, true);
	}	
	
	public function mergeRecursive($arr1, $arr2)
	{
		$arr = array_merge($arr1, $arr2);

		foreach($arr2 as $sKey => $varValue)
		{
			if (is_array($arr1[$sKey]) && is_array($arr2[$sKey]))
				if (count($arr1[$sKey]) && count($arr2[$sKey]))
					$arr[$sKey] = self::mergeRecursive($arr1[$sKey], $arr2[$sKey]);
		}
		
		return $arr;
	}
}
?>