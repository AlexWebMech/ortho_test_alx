<?
	function vard ($val = NULL, $message = "", $skip_check_admin = false) //Debug Variable
	{
		if ($skip_check_admin !== true)
		{
			global $USER;
			if (! $USER || ! $USER->IsAdmin())
				return false;
			if (! $USER || ($USER->GetId() == 4)) // && ! $skip_check_admin
				return false;
		}
		
		echo '<div>';
			
		if ($message)
			echo '<b>' . $message . ' : </b>';
			
			if (is_array ($val) || $val instanceof ArrayAccess)
			{
			echo '<table border="1">' ;
				foreach ($val as $key => $value)
				{
				echo "<tr><td style='padding:0 8px' valign='top'>$key</td><td style='padding:2px 8px'>" ;
				vard($value, '', true) ; echo '</td></tr>';
				}
			echo '</table>';
			}
			elseif(is_object ($val))
			{
				echo "[ Object ]";
			}
			else
			{
				if ($val !== true && $val !== false)
					echo $val;
				elseif ($val === true)
					echo 'true';
				elseif ($val === false)
					echo 'false';
			}
		
		if ($message)
			echo '';	
		
		echo '</div>';
			
		if (substr($message, 0, 3) == 'die')
		{
			throw new \Bitrix\Main\SystemException("Error");
			die($message);
		}
		##throw new \Bitrix\Main\SystemException("Error");
	}
	
		
	function vardfile($val, $comment = false, $file = '/upload/log.txt')
	{
		$sComment = date('Y-m-d H:i:s') . ((strlen($comment)) ?  " - " . $comment : "") . " - ";
		//if (is_object($GLOBALS["USER"]) && $GLOBALS["USER"]->IsAuthorized())
		//	$comment = "" . $comment; 
		
		if ($val === true || $val === false)
			$sVar = ($val) ? 'true' : 'false';
		elseif (! is_array($val))
			$sVar = $val;
		else
			$sVar = print_r($val, true);
		$sVar .= "\n";
		
		$fs = fopen($_SERVER["DOCUMENT_ROOT"] . $file, "a");
		
		if (! $fs)
			return false;
		fwrite($fs, $sComment . $sVar);
		fclose($fs);
	}		
	
?>