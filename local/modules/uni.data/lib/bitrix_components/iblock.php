<?
namespace Uni\Data\BitrixComponents;

class IblockBase
{
	function elementResultModifier($arElement, $arParams, $arResult = null)
	{
		//Pictures
		/*
		if ($arElement["PREVIEW_PICTURE"]["SRC"])
		{
			$arElement["PREVIEW_PICTURE--INFO"]["PICTURE"] = $arElement["PREVIEW_PICTURE"];
			$arElement["PREVIEW_PICTURE"] = $arElement["PREVIEW_PICTURE"]["ID"];
		}
		if ($arElement["DETAIL_PICTURE"]["SRC"])
		{
			$arElement["DETAIL_PICTURE--INFO"]["PICTURE"] = $arElement["DETAIL_PICTURE"];
			$arElement["DETAIL_PICTURE"] = $arElement["DETAIL_PICTURE"]["ID"];
		}
		if ($arElement["PROPERTIES"]["MORE_PHOTO"]["VALUE"])
		{
			$arPhotoItems = [];
			foreach($arElement["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $iMorePhoto => $iFileID)
				$arPhotoItems[] = ["PREVIEW_PICTURE" => $iFileID, "DETAIL_PICTURE" => $iFileID];
			\Uni\Data\Tools\BitrixPicture::proccessItems($arPhotoItems, $arParams);
			$arElement["PROPERTY_MORE_PHOTO--INFO"] = $arPhotoItems;
		}
		*/

		if (! $arElement["PICTURE"] && $arParams["UI_PICTURE_FIELD_CODE"] && $varSetPicture = $arElement[$arParams["UI_PICTURE_FIELD_CODE"]])
			$arElement["PICTURE"] = $varSetPicture;
		if ($arElement["PICTURE"])
			$arElement["PICTURE--INFO"] = \Uni\Data\Tools\BitrixPicture::getPictureSmart($arElement["PICTURE"], $arParams);

		if (! is_array($arElement["PICTURES"]))
			$arElement["PICTURES"] = [];
			
		if ($arParams["UI_PICTURES_ADD_FIELD_CODE"] && $varSetPicture = $arElement[$arParams["UI_PICTURES_ADD_FIELD_CODE"]])
			$arElement["PICTURES"][] = $varSetPicture;
		if ($arElement["PROPERTIES"]["MORE_PHOTO"]["VALUE"])
		{		
			foreach($arElement["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $iMorePhoto => $iFileID)
				$arElement["PICTURES"][] = $iFileID;
		}
		if ($arElement["PICTURES"])
			$arElement["PICTURES--INFO"] = \Uni\Data\Tools\BitrixPicture::getPicturesSmart($arElement["PICTURES"], $arParams);

		if ($arElement["PREVIEW_PICTURE"]["SRC"])
		{
			$arElement["PREVIEW_PICTURE"] = $arElement["PREVIEW_PICTURE"]["ID"];
		}
		if ($arElement["DETAIL_PICTURE"]["SRC"])
		{
			$arElement["DETAIL_PICTURE"] = $arElement["DETAIL_PICTURE"]["ID"];
		}	
		//--
		
		
		
		
		
		
		//Prices
		if (! $arItem["MIN_PRICE"] && $arItem["PROPERTIES"]["MIN_PRICE"]["VALUE"] > 0)
			$arItem["MIN_PRICE"] = ["PRICE" => $arItem["PROPERTIES"]["MIN_PRICE"]["VALUE"], "CURRENCY" => $arParams["BASE_CURRENCY"]];
		elseif (! $arItem["MIN_PRICE"] && $arItem["PROPERTIES"]["PRICE"]["VALUE"] > 0)
			$arItem["MIN_PRICE"] = ["PRICE" => $arItem["PROPERTIES"]["PRICE"]["VALUE"], "CURRENCY" => $arParams["BASE_CURRENCY"]];
		if (! $arItem["BASE_PRICE"] && $arItem["PROPERTIES"]["OLD_PRICE"]["VALUE"] > 0)
			$arItem["BASE_PRICE"] = ["PRICE" => $arItem["PROPERTIES"]["OLD_PRICE"]["VALUE"], "CURRENCY" => $arParams["BASE_CURRENCY"]];
		
		
		
		/* if (\Bitrix\Main\Loader::includeModule("currency"))
		{
			$arCurrencyCodes = [];
			foreach($arResult["PRICES"] as $arPrice)
				if (! in_array($arPrice["CURRENCY"], $arCurrencyCodes))
					$arCurrencyCodes[] = $arPrice["CURRENCY"];
			
			$obRes = \Bitrix\Currency\CurrencyTable::getList(["filter" => ["CURRENCY" => $arCurrencyCodes, "BASE" => "Y", "LOGIC" => "OR"]]);
			while($arItem = $obRes->Fetch())
				$arResult["CURRENCIES"][$arItem["CURRENCY"]] = $arItem;
		}
		debug($arResult["CURRENCIES"]);
		*/
		
		
		foreach($arElement["PRICES"] as $sPriceCode => $arPrice)
		{
			$arPrice["PRICE"] = $arPrice["VALUE"];
			$arPrice["PRICE--HTML"] = \CCurrencyLang::CurrencyFormat($arPrice["PRICE"], $arPrice["CURRENCY"], false);
			$arPrice["CURRENCY--HTML"] = trim(str_replace("0", "", \CCurrencyLang::CurrencyFormat(0, $arPrice["CURRENCY"], true)));
			$arElement["PRICES"][$sPriceCode] = $arPrice;
		}
		
		$arBasePriceTypeCodes = [];
		foreach($arResult["PRICES"] as $sPriceTypeCode => $arPriceType)
			if ($arPriceType["BASE"] == "Y")
				$arBasePriceTypeCodes[] = $sPriceTypeCode;
				
				
				
				
				

		##ToDo: переводить в базовую валюту
		
		if (! $arElement["MIN_PRICE"] || ! $arElement["BASE_PRICE"])
		{
			foreach($arElement["PRICES"] as $sPriceType => $arPrice)
			{
				if ($arPrice["VALUE"] == 0)
					continue;
				if (! $arElement["MIN_PRICE"] || $arPrice["VALUE"] < $arElement["MIN_PRICE"])
					$arElement["MIN_PRICE"] = $arPrice;
				if (! $arElement["BASE_PRICE"] || (in_array($sPriceType, $arBasePriceTypeCodes) && $arPrice["VALUE"] < $arElement["BASE_PRICE"]))
					$arElement["BASE_PRICE"] = $arPrice;	
			}				
		}			
		
		if (! $arElement["MIN_PRICE"] || ! $arElement["BASE_PRICE"])
		{
			foreach($arElement["OFFERS"] as $arOffer)
			{
				if ($arOffer["ACTIVE"] == "N")
					continue;
				foreach($arOffer["PRICES"] as $sPriceType => $arPrice)
				{
					if ($arPrice["VALUE"] == 0)
						continue;
					if (! $arElement["MIN_PRICE"] || $arPrice["VALUE"] < $arElement["MIN_PRICE"])
						$arElement["MIN_PRICE"] = $arPrice;
					if (! $arElement["BASE_PRICE"] || (in_array($sPriceType, $arBasePriceTypeCodes) && $arPrice["VALUE"] < $arElement["BASE_PRICE"]))
						$arElement["BASE_PRICE"] = $arPrice;
					
					
				}
			}
		}
		
		//debug($arElement["BASE_PRICE"]);
		//debug($arElement["MIN_PRICE"]);
		
		foreach(["MIN_PRICE", "BASE_PRICE"] as $sSpacialPriceCode)
		{
			if ($arPrice = $arElement[$sSpacialPriceCode])
			{
				$arPrice["PRICE"] = $arPrice["VALUE"];
				$arPrice["DISCOUNT_PRICE"] = $arPrice["DISCOUNT_VALUE"];
				$arPrice["PRICE--HTML"] = \CCurrencyLang::CurrencyFormat($arPrice["PRICE"], $arPrice["CURRENCY"], false);
				$arPrice["DISCOUNT_PRICE--HTML"] = \CCurrencyLang::CurrencyFormat($arPrice["DISCOUNT_PRICE"], $arPrice["CURRENCY"], false);
				$arPrice["CURRENCY--HTML"] = trim(str_replace("0", "", \CCurrencyLang::CurrencyFormat(0, $arPrice["CURRENCY"], true)));
				$arElement[$sSpacialPriceCode] = $arPrice;		
			}
		}
		//--

		
		if (! $arElement["MIN_ITEM_PRICE"])
		{
			foreach($arElement["OFFERS"] as $arOffer)
			{
				if ($arOffer["ACTIVE"] == "N" || $arOffer["CAN_BUY"] === false)
					continue;				

				if ($arOffer["ITEM_PRICES"][0])
					if ($arElement["MIN_ITEM_PRICE"] === null || $arOffer["ITEM_PRICES"][0]["PRICE"] < $arElement["MIN_ITEM_PRICE"]["PRICE"])
						$arElement["MIN_ITEM_PRICE"] = $arOffer["ITEM_PRICES"][0];
				
				
			}
			
			//if ($arElement["ID"] == 130716)
			//	vard($arElement["MIN_ITEM_PRICE"]);
		}

			
		//Properties
		foreach($arElement["PROPERTIES"] as $sPropertyCode => $arProperty)
		{
			$arElement["PROPERTY_" . $sPropertyCode] = $arProperty["VALUE_ENUM_ID"] !== NULL ? $arProperty["VALUE_ENUM_ID"] : $arProperty["VALUE"];
			$arElement["PROPERTY_" . $sPropertyCode . "--HTML"] = $arElement["DISPLAY_PROPERTIES"][$sPropertyCode] !== NULL ? $arElement["DISPLAY_PROPERTIES"][$sPropertyCode]["DISPLAY_VALUE"] : (is_array($arProperty["VALUE"]) ? implode(",", $arProperty["VALUE"]) : $arProperty["VALUE"]);
		}
		
		foreach($arElement["PROPERTIES"] as $sPropertyCode => $arProperty)
		{
			if ($varEnumID = $arProperty["VALUE_ENUM_ID"])
			{
				$arProperty["VALUE_HTML"] = $arProperty["VALUE"];
				$arProperty["VALUE"] = $varEnumID;
			}
			
			$arElement["PROPERTIES"][$sPropertyCode] = array_intersect_key($arProperty, ["VALUE" => NULL, "VALUE_ENUM_ID" => NULL, "DESCRIPTION" => null]);
		}
		//--
		
		//Offers
		if ($arElement["OFFERS"])
		{
			$arSetOffers = [];
			foreach($arElement["OFFERS"] as $arOffer)
				$arSetOffers[$arOffer["ID"]] = self::elementResultModifier($arOffer, $arParams, $arResult);
			$arElement["OFFERS"] = $arSetOffers;
		}
		//--
		
		
		return $arElement;
	}
	
	function sectionResultModifier($arSection, $arParams, $arResult = null)
	{
		//Pictures
		if ($arSection["PICTURE"]["SRC"])
		{
			$arSection["PICTURE--INFO"]["PICTURE"] = $arSection["PICTURE"];
			$arSection["PICTURE"] = $arSection["PICTURE"]["ID"];
		}
		if ($arSection["DETAIL_PICTURE"]["SRC"])
		{
			$arSection["DETAIL_PICTURE--INFO"]["PICTURE"] = $arSection["DETAIL_PICTURE"];
			$arSection["DETAIL_PICTURE"] = $arSection["DETAIL_PICTURE"]["ID"];
		}
		
		return $arSection;
	}
	
}

class IblockList extends IblockBase
{
	function resultModifier($var)
	{
		$obBxComponent = $var->__component;
		$arParams = &$obBxComponent->arParams;
		$arResult = &$obBxComponent->arResult;
		
		\Uni\Data\Tools\BitrixPicture::proccessItem($arResult["SECTION"], $arParams);
		\Uni\Data\Tools\BitrixPicture::proccessItems($arResult["SECTIONS"], $arParams);
		//\Uni\Data\Tools\BitrixPicture::proccessItems($arResult["ITEMS"], $arParams);
		
		//Items
		if ($arResult["ITEMS"])
		{
			//Copy properties
			$arResult["PROPERTIES"] = [];
			foreach($arResult["ITEMS"] as $arItem)
			{
				if (! $arResult["PROPERTIES"])
					if ($arItem["PROPERTIES"])
						$arResult["PROPERTIES"] = array_diff_key($arItem["PROPERTIES"], ["VALUE" => NULL, "VALUE_ENUM_ID" => NULL, "DESCRIPTION" => null]);
				
				foreach($arItem["OFFERS"] as $arOffer)
				{
					if (! $arResult["OFFER_PROPERTIES"])
						if ($arOffer["PROPERTIES"])
						{
							$arResult["OFFER_PROPERTIES"] = array_diff_key($arOffer["PROPERTIES"], ["VALUE" => NULL, "VALUE_ENUM_ID" => NULL, "DESCRIPTION" => null]);
							if ($arResult["PROPERTIES"])
								break 2;
						}
				}
			}
			//--

			$arResultElements = [];
			foreach($arResult["ITEMS"] as $arElement)
			{
				if (! $arParams["UI_PICTURE_CREATE"])
					if ($arElement["PREVIEW_PICTURE"])
						$arElement["PICTURE"] = $arElement["PREVIEW_PICTURE"];
					elseif ($arElement["DETAIL_PICTURE"])
						$arElement["PICTURE"] = $arElement["DETAIL_PICTURE"];				
				
				$arResultElements[$arElement["ID"]] = self::elementResultModifier($arElement, $arParams, $arResult);
			}
			
			unset($arResult["ITEMS"]);
			$arResult["ELEMENTS"] = $arResultElements;			
		}
		//--

		//Sections
		if ($arResult["SECTIONS"])
		{
			$arResultSections = [];
			foreach($arResult["SECTIONS"] as $arSection)
			{
				$arResultSections[$arSection["ID"]] = self::sectionResultModifier($arSection, $arParams, $arResult);
			}	
			$arResult["SECTIONS"] = $arResultSections;			
		}
		//--
		

	}
}

class IblockDetail extends IblockBase
{
	function resultModifier($var)
	{
		$obBxComponent = $var->__component;
		$arParams = &$obBxComponent->arParams;
		$arResult = &$obBxComponent->arResult;

		//\Uni\Data\Tools\BitrixPicture::proccessItem($arResult, $arParams);
	
		if (! $arParams["UI_PICTURE_CREATE"])
			$arResult["PICTURE"] = $arResult["DETAIL_PICTURE"];
		//$arResult["PICTURE--INFO"] = \Uni\Data\Tools\BitrixPicture::getPictureSmart($arResult["PICTURE"], $arParams);

		$arResult["ELEMENT"] = self::elementResultModifier($arResult, $arParams, $arResult);

		//Copy properties
		foreach($arItem["OFFERS"] as $arOffer)
		{
			if ($arOffer["PROPERTIES"])
			{
				$arResult["OFFER_PROPERTIES"] = array_diff_key($arOffer["PROPERTIES"], ["VALUE" => NULL, "VALUE_ENUM_ID" => NULL, "DESCRIPTION" => null]);
				break;
			}
		}
		//--
	}
}
?>