<?
require_once(__DIR__ . "/lib/data.php");

$arFiles = Array(
	"/lib/tools/functions.php",
	"/lib/tools/vard.php",
	"/lib/tools/arr.php",
	"/lib/tools/bitrixpicture.php",
	"/lib/tools/flowvar.php",
	"/lib/tools/orm.php",
	"/lib/tools/vars.php",
);

foreach($arFiles as $sFile)
{
	$sFilePath = __DIR__ . $sFile;
	if (file_exists($sFilePath))
		require_once($sFilePath);
}

?>