<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
if ($_REQUEST["uni_ajax_main"])
	return;
require("init.php");


$lang =  GetLang(); // defined in init.php
require_once('lang_' . $lang . '.php');
	
$curPage = $APPLICATION->GetCurPage(true);


ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1); 
error_reporting(E_ALL & ~ E_WARNING & ~ E_NOTICE & ~ E_DEPRECATED);

/*
// ????? ??? ????? ????
include_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/classes/general/captcha.php");
$cpt = new CCaptcha();
$captchaPass = COption::GetOptionString("main", "captcha_password", "");
if(strlen($captchaPass) <= 0) {
    $captchaPass = randString(10);
    COption::SetOptionString("main", "captcha_password", $captchaPass);
}
$cpt->SetCodeCrypt($captchaPass);
*/
define(SITE_TEMPLATE_PATH2, "/local/templates/new_ortoboom");
?><!DOCTYPE html>
<html>
<!--[if lte IE 9]> <html class="ie8"> <![endif]-->

	<head>
		<title><?$APPLICATION->ShowTitle();?></title>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-PNKTQ7Z');</script>
		<!-- End Google Tag Manager -->
		<!--<meta name="description" content="Интернет-магазин детской ортопедической обуви ORTHOBOOM с доставкой по всей России. Собственное производство и гарантия от производителя 45 дней." />-->
		<meta name="google-site-verification" content="Y5qrPJSwzEB1WN3uj5J1n9SuVegkF42IwSQC1xGrCZo" />
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
		
		



		<? $APPLICATION->ShowHead();?>
		
		
		
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.jpg" /> 
		<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700&amp;subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>	
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>

		<? if (1 || strpos($APPLICATION->GetCurPage(), "/catalog/2") !== 0) : ?>
		
		<? if (1) : ?>
		
		<? else : ?>
<!--		<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>-->
            <script async src="/local/templates/new_ortoboom/external/jquery/jquery.js"></script>
		<? endif; ?>
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		
		<? endif; ?>
		
		
		<?/*if($curPage != '/opt/index.php'  ) :*/?>		
		<!--<link rel="stylesheet" href="/css/select-and-radio.css">	-->
		<?/*endif;*/?>
		<link rel="stylesheet" href="/css/jquery.lightbox-0.5.css">
		<!--<link rel="stylesheet" href="//cdn.callbackhunter.com/widget2/tracker.css"> -->
		
		<link href="/css/cloud-zoom.css" rel="stylesheet" type="text/css" />

		<!--
     	<script src="/js/bootstrap.js" type="text/javascript"></script>	
		-->
		
		<script type="text/javascript" src="/js/cloud-zoom.1.0.2.js"></script>	

        <?/*
		<!-- fancybox -->
<!--		<script src="-->//=SITE_TEMPLATE_PATH<!--/js/fancy/jquery.fancybox.js"></script>-->
<!--		<script src="-->//=SITE_TEMPLATE_PATH<!--/js/fancy/helpers/jquery.fancybox-media.js"></script>-->
<!--		<link href="-->//=SITE_TEMPLATE_PATH<!--/js/fancy/jquery.fancybox.css" rel="stylesheet" />-->
		<!-- fancybox -->
*/?>


		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.matchHeight-min.js"></script> 

		<style type="text/css">	
			#owl-demo .item img{
				display: block;
				width: 100%;
				height: auto;
			}
		</style>

        <link href="/css/owl.carousel.css" rel="stylesheet">
        <link href="/css/owl.theme.css" rel="stylesheet">
        <script src="/js/owl.carousel.js"></script>
<?/*
<!--        <script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/select.js"></script>
        <script src="/js/select-and-radio.js" type="text/javascript"></script>-->
*/?>

    <!--    <script src="/js/jquery.inputmask.min.js" type="text/javascript"></script>
        <script src="/js/inputmask.min.js" type="text/javascript"></script>-->
        <script src="<?=SITE_TEMPLATE_PATH2?>/js/jquery.maskedinput.min.js"></script>
        <script src="/js/jquery.lightbox-0.5.js" type="text/javascript"></script>
<!--		<script type="text/javascript" src="/js/script.js"></script>-->
		<?/*?><script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/script.js"></script><?*/?>
		<?//$APPLICATION->ShowHead();?>

		<!--<link rel="stylesheet" href="<?/*=SITE_TEMPLATE_PATH*/?>/css/select.css">-->


		<?
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.scrollbar.js");
//		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.maskedinput.min.js");
		if (strpos($curPage, '/personal/') !== false)
			$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/script2.0.js");
		?>

		<?
//		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/bootstrap.css");
		//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/select-and-radio.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery.lightbox-0.5.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery.scrollbar.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/styles.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/cabinet.css");
		?>
        <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH2."/external/bootstrap/css/bootstrap.min.css");?>

        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH2?>/external/rateYo/rateYo.min.css">
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH2?>/fonts/icomoon/style.css">
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH2?>/external/slick/slick.css">
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH2?>/external/jquery.fullpage/jquery.fullpage.css">
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH2?>/external/animate/animate.css">
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH2?>/external/nouislider/nouislider.css">
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH2?>/external/swipebox-master/swipebox.css">
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH2?>/external/bootstrap-select/css/bootstrap-select.min.css"/>
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH2?>/external/fontawesome/fontawesome.css"/>
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH2?>/external/jquery-bar-rating/dist/themes/fontawesome-stars-o.css"/>
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH2?>/css/style.css">
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH2?>/css/custom.css">
        <!-- fancybox -->
        <link href="<?=SITE_TEMPLATE_PATH2?>/js/fancy/jquery.fancybox.css" rel="stylesheet" />
        <!-- fancybox -->
        <!--[if IE]>
        <link rel="icon" href="<?=SITE_TEMPLATE_PATH2?>/images/favicon/favicon.ico"><![endif]-->
        <!-- favicon-->
        <link rel="icon" href="<?=SITE_TEMPLATE_PATH2?>/images/favicon/favicon.ico">
        <!--link(rel="icon", href="./images/favicon/favicon.png")-->
        <!--link(rel="apple-touch-icon-precomposed", href="./images/favicon/apple-touch-icon-precomposed.png")-->
        <!--link(rel="apple-touch-icon-precomposed", sizes="72x72",  href="./images/favicon/apple-touch-icon-72x72-precomposed.png")-->
        <!--link(rel="apple-touch-icon-precomposed", sizes="114x114",  href="./images/favicon/apple-touch-icon-114x114-precomposed.png")-->
        <!--link(rel="apple-touch-icon-precomposed", sizes="144x144",  href="./images/favicon/apple-touch-icon-144x144-precomposed.png")-->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script><![endif]-->
        <!-- fonts-->
        <link href="https://fonts.googleapis.com/css?family=Istok+Web:400,700&amp;amp;subset=cyrillic" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Pacifico&amp;amp;subset=cyrillic" rel="stylesheet">
        <!-- jquery-->
       <!-- <script src="<?=SITE_TEMPLATE_PATH2?>/external/jquery/jquery.js"></script> -->
        <script src="https://www.google.com/recaptcha/api.js"></script>

        <?/*retail rocket*/?>
		<?/*
        <script type="text/javascript">
            var rrPartnerId = "5d936bc797a52819b40f3d36";
            var rrApi = {};
            var rrApiOnReady = rrApiOnReady || [];
            rrApi.addToBasket = rrApi.order = rrApi.categoryView = rrApi.view =
                rrApi.recomMouseDown = rrApi.recomAddToCart = function() {};
            (function(d) {
                var ref = d.getElementsByTagName('script')[0];
                var apiJs, apiJsId = 'rrApi-jssdk';
                if (d.getElementById(apiJsId)) return;
                apiJs = d.createElement('script');
                apiJs.id = apiJsId;
                apiJs.async = true;
                apiJs.src = "//cdn.retailrocket.ru/content/javascript/tracking.js";
                ref.parentNode.insertBefore(apiJs, ref);
            }(document));
        </script>
		*/?>
		
		

	<? if (strpos($APPLICATION->GetCurPage(), "/catalog/2") === 0) : ///////////////////////////////////////////////////////////////////
	define(SITE_TEMPLATE_PATH_OLD, "/local/templates/new_ortoboom"); ?>
		
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-PNKTQ7Z');</script>
	<!-- End Google Tag Manager -->
	<meta name="google-site-verification" content="Y5qrPJSwzEB1WN3uj5J1n9SuVegkF42IwSQC1xGrCZo" />
	<meta name="yandex-verification" content="a02db5d319195e2a" />
   
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH_OLD."/external/bootstrap/css/bootstrap.min.css");?>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH_OLD?>/external/bootstrap/css/bootstrap-reboot.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH_OLD?>/external/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH_OLD?>/external/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH_OLD?>/external/rateYo/rateYo.min.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH_OLD?>/fonts/icomoon/style.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH_OLD?>/external/slick/slick.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH_OLD?>/external/jquery.fullpage/jquery.fullpage.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH_OLD?>/external/animate/animate.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH_OLD?>/external/nouislider/nouislider.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH_OLD?>/external/swipebox-master/swipebox.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH_OLD?>/external/bootstrap-select/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH_OLD?>/external/fontawesome/fontawesome.css"/>
    <!--<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH_OLD?>/component/at/sale.order.ajax/style.css"/>-->
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH_OLD?>/external/jquery-bar-rating/dist/themes/fontawesome-stars-o.css"/>
    <!-- fancybox -->
    <link href="<?=SITE_TEMPLATE_PATH_OLD?>/js/fancy/jquery.fancybox.css" rel="stylesheet" />
    <!-- fancybox -->

    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH_OLD?>/css/style.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH_OLD?>/css/custom.css">
    <!--[if IE]>
    <link rel="icon" href="<?=SITE_TEMPLATE_PATH_OLD?>/images/favicon/favicon.ico"><![endif]-->
    <!-- favicon-->
    <link rel="icon" href="<?=SITE_TEMPLATE_PATH_OLD?>/images/favicon/favicon.ico">
    <!--link(rel="icon", href="./images/favicon/favicon.png")-->
    <!--link(rel="apple-touch-icon-precomposed", href="./images/favicon/apple-touch-icon-precomposed.png")-->
    <!--link(rel="apple-touch-icon-precomposed", sizes="72x72",  href="./images/favicon/apple-touch-icon-72x72-precomposed.png")-->
    <!--link(rel="apple-touch-icon-precomposed", sizes="114x114",  href="./images/favicon/apple-touch-icon-114x114-precomposed.png")-->
    <!--link(rel="apple-touch-icon-precomposed", sizes="144x144",  href="./images/favicon/apple-touch-icon-144x144-precomposed.png")-->
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script><![endif]-->
	
    <!-- fonts-->
    <link href="https://fonts.googleapis.com/css?family=Istok+Web:400,700&amp;amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico&amp;amp;subset=cyrillic" rel="stylesheet">
	
	<?/*
    <!-- jquery-->
    <script src="<?=SITE_TEMPLATE_PATH_OLD?>/external/jquery/jquery.js"></script>
	*/?>
	
    <!-- fancybox -->
    <script src="<?=SITE_TEMPLATE_PATH_OLD?>/js/fancy/jquery.fancybox.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH_OLD?>/js/fancy/helpers/jquery.fancybox-media.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH_OLD?>/js/jquery.maskedinput.min.js"></script>
    <!-- fancybox -->
    <script src="https://www.google.com/recaptcha/api.js"></script>

    <?/*retail rocket*/?>
	<?/*
    <script type="text/javascript">
        var rrPartnerId = "5d936bc797a52819b40f3d36";
        var rrApi = {};
        var rrApiOnReady = rrApiOnReady || [];
        rrApi.addToBasket = rrApi.order = rrApi.categoryView = rrApi.view =
            rrApi.recomMouseDown = rrApi.recomAddToCart = function() {};
        (function(d) {
            var ref = d.getElementsByTagName('script')[0];
            var apiJs, apiJsId = 'rrApi-jssdk';
            if (d.getElementById(apiJsId)) return;
            apiJs = d.createElement('script');
            apiJs.id = apiJsId;
            apiJs.async = true;
            apiJs.src = "//cdn.retailrocket.ru/content/javascript/tracking.js";
            ref.parentNode.insertBefore(apiJs, ref);
        }(document));
    </script>
*/?>

	<? endif; /////////////////////////////////////////////////////////////////// ?>





    <script charset="UTF-8" src="//web.webpushs.com/js/push/0500767f46640a8429966aad842e55ff_1.js" async></script>



    <meta name="google-site-verification" content="V_V6QgqwNBYoSS44POxtadmkemUTV4w2xRqJCgbTfQw" />

        <meta name="yandex-verification" content="f99bbc65bcb6c44a" />
        <meta name="yandex-verification" content="cf46132209f8a7bf" />
        <meta name="yandex-verification" content="cb9819fa1931bd40" />

        <meta name="yandex-verification" content="ca7964c45ded71fd" />
        <meta name="yandex-verification" content="80581883abb23ca7" />
        <meta name="yandex-verification" content="e9f998a55fa45bc8" />


</head>
	<body>
	<div id="body-div">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PNKTQ7Z" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->	

		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
    <?//include("include/city.php");?>
 
	
	
	
	
	
<div class="site-callback-modal ui-modal ui-modal--normal hide">
	<div class="ui-modal-overlay ui-overlay"></div>
	<div class="ui-modal-inner ui-inner">
		<div class="ui-modal-header">
			<div class="ui-modal-title">Заказать обратный звонок</div>
			<span class="ui-modal-close"><span class="ui-modal-close-icon ui-icon"></span></span>
		</div>
		<div class="ui-modal-content">
		
			<? $APPLICATION->IncludeComponent("local:iblock.element.save", "feedback", Array(
	"IBLOCK_TYPE" => "info",
	"IBLOCK_ID" => 109,
	"ITEM_ID" => "",
	///// "ITEM_SAVE" => $_POST,
	"PROPERTIES_FORMAT" => "SHORT",
	"DISPLAY_FORM" => "Y",
	/////"ITEM_SAVE_MASK" => $SAVE_MASK,
	
	"CHECK_KEYWORD_FLAG" => "Y",
	"CHECK_KEYWORD_REQUEST" => $_REQUEST["IS_SAVE"],
	//"CHECK_KEYWORD" => "-",
	
	"ERROR_MESSAGES" => $ERRORS,
	
	"SUCCESS_MESSAGE" => "Ваш вопрос успешно отправлен. <br/>Мы ответим вам в ближайшее время.",
	//"SUCCESS_URL" => "/form/success.php",
	
	"EMAIL_EVENT_FLAG" => "Y",
	"EMAIL_EVENT_TYPE" => "MESSAGE",
	"EMAIL_EVENT_TITLE" => "Заказ обратного звонка",
	"EMAIL_EVENT_EMAIL_TO" => "",
	//"EMAIL_EVENT_EMAIL_FROM" => "",
	//"EMAIL_EVENT_EMAIL_TO" => "",
	
	"WRAP_ID" => "feedback-form-header",
));
?>
		</div>
		<div class="ui-modal-footer">
			
		</div>
	</div>
</div>	

<script>
$(document).ready(function() {
	$(".site-callback-modal").uniModal({
		
	});
});
</script>	
	
	
	
	
	
<header>
	<div class="header-top">
		<div class="header-top-container ui-container container">
			<div class="header-top-row">
				
				<? \Local\Location::init(); ?>
				<div class="header-location">
					<span class="header-location-text" onclick="localLocation.show(); return false;"><?= ($sLocationTitle = \Local\Location::getLocationTitle()) ? $sLocationTitle : "Выбрать город" ?></span>
					
					<? if (\Local\Location::isDetectedNow() && \Local\Location::getLocationId())
					{
					?> 
					<div class="header-location-popup ui-popup">
							<div class="ui-popup-popup-main ui-popup-main">
								Правильно ли мы определили?
								Ваш регион - <b><?= \Local\Location::getLocationTitle(); ?></b>
							</div>
							
							<div class="header-location-popup-footer ui-popup-footer">
								<span class="header-location-button header-location-yes ui-popup-js-close" onclick="$('.header-location-popup').fadeOut(600); return false;">Да</span>
								<span class="header-location-button header-location-no" onclick="localLocation.show(); $('.header-location-popup').fadeOut(600); return false;">Нет</span>
							</div>
					</div>
					<?
					}
					?>

					<div style="display: none">
					<? $APPLICATION->IncludeComponent(
					"bitrix:sale.location.selector.search",
					"",
					Array(
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"CODE" => "",
						//"ID" => \Local\Location::getLocationId(),
						"FILTER_BY_SITE" => "N",
						"INITIALIZE_BY_GLOBAL_EVENT" => "",
						"INPUT_NAME" => "LOCATION_ID",
						"JS_CALLBACK" => "localLocationSet",
						"JS_CONTROL_GLOBAL_ID" => "",
						"PROVIDE_LINK_BY" => "code",
						"SHOW_DEFAULT_LOCATIONS" => "N",
						"SUPPRESS_ERRORS" => "N",
					), true);?>		
					</div>
					
				</div>
				

				
				
				
				
				
				
				
				
				<div class="header-top-menu">
									<? $APPLICATION->IncludeComponent(
										"bitrix:menu",
										"ui-menu",
										Array(
											"ROOT_MENU_TYPE" => "top1",
											"CHILD_MENU_TYPE" => "",
											"MAX_LEVEL" => "1",
											"USE_EXT" => "N",
											"ALLOW_MULTI_SELECT" => "N",
											"DELAY" => "N",
											"MENU_CACHE_GET_VARS" => array(""),
											"MENU_CACHE_TIME" => "36000",
											"MENU_CACHE_TYPE" => "A",
											"MENU_CACHE_USE_GROUPS" => "N",
										)
									); ?>

				</div>
				
				<div class="ui-flex-grow"></div>
				
				<div class="header-top-user">
					
					<? if (! $USER->IsAuthorized()) : ?>
                    <?php if(defined('REGISTRATION_SMS_ENABLE') && REGISTRATION_SMS_ENABLE) { ?>
                    <?php
                    $APPLICATION->IncludeComponent(
                        "gorgoc:authorization.ozon", "loginlink", []
                    );
                    ?>
                    <?php }else { ?>
						<a href="/auth/"><span class="header-login-icon ui-icon"></span>Войти</a>
                    <?php  } ?>
					<? else : ?>
						<a href="/personal/"><span class="header-login-icon ui-icon"></span>Личный кабинет</a>
					<? endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="header-top-2"></div>
	
	<div class="header-main-container ui-container container">
		<div class="header-main">
		<div class="header-main-row">
			<div class="header-menu-col">
				<span class="header-menu-icon ui-icon">
					<span class="header-menu-icon-line-1 header-menu-icon-line"></span>
					<span class="header-menu-icon-line-2 header-menu-icon-line"></span>
					<span class="header-menu-icon-line-3 header-menu-icon-line"></span>
				</span>
				
				<div class="header-mob-menu" style="height: 0">
					
									<? $APPLICATION->IncludeComponent(
                                        "bitrix:menu",
										"ui-menu-mobile",
										Array(
                                            "ROOT_MENU_TYPE" => "mob1",
                                            "CHILD_MENU_TYPE" => "mob1-sub",
                                            "MAX_LEVEL" => "3",
                                            "USE_EXT" => "Y",
                                            "ALLOW_MULTI_SELECT" => "N",
                                            "DELAY" => "N",
                                            "MENU_CACHE_GET_VARS" => array(""),
                                            "MENU_CACHE_TIME" => "36000",
                                            "MENU_CACHE_TYPE" => "A",
                                            "MENU_CACHE_USE_GROUPS" => "N",
                                        )
									); ?>
									
					
					<div class="ui-container container">
					<div class="mob-po-bottom-text">
					
					<div class="mob-po-bottom-phone">
					<span class="header-phone-icon ui-icon"></span>
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_TEMPLATE_PATH . "/include/mob-header-phone.php",
							"EDIT_TEMPLATE" => "",
							"AREA_FILE_SUFFIX" => "",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
						)
					);?>
					</div>
					
					<div class="mob-po-bottom-additional">
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_TEMPLATE_PATH . "/include/mob-header-additional.php",
							"EDIT_TEMPLATE" => "",
							"AREA_FILE_SUFFIX" => "",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
						)
					);?>					
					</div>
					
					</div>
					</div>
					
				</div>
				
			</div>
			
			<div class="header-phone-col">
				<div class="header-phone">
					
					<span class="header-phone-text"><span class="header-phone-icon ui-icon"></span><?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_TEMPLATE_PATH . "/include/header-phone.php",
							"EDIT_TEMPLATE" => "",
							"AREA_FILE_SUFFIX" => "",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
						)
					);?></span>
					
					<div class="header-phone-additional">
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_TEMPLATE_PATH . "/include/header-phone-additional.php",
							"EDIT_TEMPLATE" => "",
							"AREA_FILE_SUFFIX" => "",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
						)
					);?>
					</div>
				</div>			
			</div>
	
			<div class="header-callback-col">
				<div class="header-callback">
					<a class="header-callback-link js-callback" href="/contacts/" onclick="$('.site-callback-modal').uniModal('show'); return false;">Заказать звонок</a>
				</div>
			</div>
			
			<div class="header-logo-col">
				<a class="header-logo" href="/">
					<img src="<?= SITE_TEMPLATE_PATH ?>/img/header-logo.png">
				</a>
                <form class="header-search" action="/catalog/">
                    <input type="text" placeholder="Поиск" name="q">
                    <input class="ui-icon" type="submit" value="">
                </form>
			</div>
			
			<div class="header-search-col">
				<form class="header-search" action="/catalog/">
					<input type="text" placeholder="Поиск" name="q" />
					<input class="ui-icon" type="submit" value="" />
				</form>
			</div>
			
			<div class="header-cart-col js-cart">
			
				<div class="header-cart-col-flex">
				
					<a class="header-fav" href="/personal/fav/">
						<span class="header-fav-icon ui-icon"><span class="header-fav-count js-fav-count">0</span></span>
					</a>			
					<a class="header-cart" href="/personal/cart/">
						<span class="header-cart-icon ui-icon"><span class="header-cart-count js-cart-count">0</span></span>
					</a>
					
					<a class="header-cart-text ui-box" href="/personal/cart/">
						<div class="header-cart-title">Корзина</div>
						<div class="header-cart-sum js-cart-sum">0 руб.</div>
					</a>				
				
				</div>
				
			</div>
		</div>		
		</div>		
	</div>

	<div class="header-main-menu-wrap">	
		<div class="ui-container container">
			<div class="header-main-menu">

                <?/*
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "ui-menu",
                    Array(
                        "ROOT_MENU_TYPE" => "top2",
                        "CHILD_MENU_TYPE" => "",
                        "MAX_LEVEL" => "1",
                        "USE_EXT" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "DELAY" => "N",
                        "MENU_CACHE_GET_VARS" => array(""),
                        "MENU_CACHE_TIME" => "36000",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_USE_GROUPS" => "N",
                    )
                ); ?>
                */?>

                <?$APPLICATION->IncludeComponent(
                    "bitrix:catalog.section.list",
                    "menu_new",
                    Array(
                        "VIEW_MODE" => "TEXT",
                        "SHOW_PARENT_NAME" => "Y",
                        "IBLOCK_TYPE" => "",
                        "IBLOCK_ID" => "114",
                        "SECTION_ID" => "",
                        "SECTION_CODE" => "",
                        "SECTION_URL" => "",
                        "COUNT_ELEMENTS" => "Y",
                        "TOP_DEPTH" => "3",
                        "SECTION_FIELDS" => "",
                        "SECTION_USER_FIELDS" => "",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_NOTES" => "",
                        "CACHE_GROUPS" => "Y"
                    )
                );?>
				
			</div>
		</div>
	</div>
	
</header>

<main>
	
	<? if (LOCAL_TEMPLATE_WIDE != "Y") : ?>
		<div class="ui-container container">
	<? endif; ?>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "address": {
    "@type": "PostalAddress",
    "addressLocality": "Москва, Россия",
    "streetAddress": "ул. Кировоградская, д.13А"
   },
  "email": "zakaz@orthoboom.ru",
  "name": "ORTHOBOOM",
  "telephone": "+7 (499) 430-42-38",
  "logo": "/local/templates/orthoboom/img/header-logo.png",
  "url": "/local/templates/orthoboom/img/header-logo.png"
}
</script>

<?if($APPLICATION->GetCurDir() == '/') : ?>
<div style="display: none;" itemscope itemtype="http://schema.org/Product">
      <div itemprop="name">Купить ортопедическую обувь для детей и взрослых в интернет-магазине ORTHOBOOM</div>  
      <div itemprop="aggregateRating" itemscope itemtype="https://schema.org/AggregateRating">
            <meta itemprop="ratingValue" content="4.9">
            <meta itemprop="ratingCount" content="3144">
       </div>
    </div>
<? endif; ?>	
<?if($APPLICATION->GetCurDir() != '/') : ?>
    <div class="breadcrumbs test">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="breadcrumbs-wrapper">
                        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs", array(
                            "PATH" => "",
                            "SITE_ID" => "s1",
                            "START_FROM" => "0",
                        ),
                            false
                        );?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>

	<? if (LOCAL_TEMPLATE_NO_TITLE != "Y") : ?>	
		<h1 class="ui-h1"><? $APPLICATION->ShowTitle(false); ?></h1>
	<? endif; ?>
	
	
		