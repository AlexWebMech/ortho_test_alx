<?
define(SITE_TEMPLATE_PATH3, "/local/templates/orthoboom");

\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH3 . "/css/template.css");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH3 . "/css/index.css");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH3 . "/css/catalog.css");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH3 . "/template_styles.css");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH3 . "/styles.css");

\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH3 . "/patch.css");

//jQuery
\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH3 . "/js/jquery-3.5.1.min.js");

//Swiper
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH3 . "/lib/swiper/swiper-bundle.css");
\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH3 . "/lib/swiper/swiper-bundle.js");

//\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH3 . "/uni-lib/uni.js");
\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH3 . "/uni-lib/base.js");
\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH3 . "/uni-lib/ajax.js");
//\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH3 . "/uni-lib/catalog.js");
\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH3 . "/uni-lib/filter.js");
\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH3 . "/js/local.js");
\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH3 . "/js/catalog.js");
\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH3 . "/js/fancy/jquery.fancybox.js");

/*
//Dev
\Bitrix\Main\Page\Asset::getInstance()->addCss("/local/templates/.default/template_styles.css");
\Bitrix\Main\Page\Asset::getInstance()->addCss("/local/templates/.default/styles.css");
\Bitrix\Main\Page\Asset::getInstance()->addCss("/local/templates/.default/css/catalog-default.css");
\Bitrix\Main\Page\Asset::getInstance()->addCss("/local/templates/.default/css/uni-ui.css");

//Js modules
\Bitrix\Main\Page\Asset::getInstance()->addJs("/local/modules/uni.data/js/base.js");
\Bitrix\Main\Page\Asset::getInstance()->addJs("/local/modules/uni.data/js/ajax.js");
\Bitrix\Main\Page\Asset::getInstance()->addJs("/local/modules/uni.data/js/filter.js");
\Bitrix\Main\Page\Asset::getInstance()->addJs("/local/modules/uni.data/js/catalog.js");
*/

if (! defined("LOCAL_TEMPLATE_NO_TITLE") && $APPLICATION->GetCurPage() != "/")
	define("LOCAL_TEMPLATE_NO_TITLE", "Y");
?>