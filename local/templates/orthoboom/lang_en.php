<?
define(YOUR_CITY, 'Your city');
define(SELECT_CITY, 'select');
define(CALL_ME, 'Call me back');
define(SITE_SEARCH, 'Search');
define(SEARCH_RESULT, 'Search results');

define(FORM_NAME_LABEL, 'Your name');
define(FORM_PHONE_LABEL, 'Telephone number');
define(FORM_EMAIL_LABEL, 'Email');
define(FORM_APTMNT_LABEL, 'Appointment date');
define(FORM_COMPANY_LABEL, 'Company');

define(FORM_ARTICLE_LABEL, 'Article');
define(FORM_COLOR_LABEL, 'Colour');
define(FORM_AMOUNT_LABEL, 'Amount');
define(FORM_SIZE_LABEL, 'Size');
define(FORM_DEST_LABEL, 'Delivery address');

define(FORM_MESSAGE_LABEL, 'Message');
define(FORM_SEND_BUTTON, 'Send');
define(FORM_MESSAGE_OK, 'Message sent!');
define(FORM_CATEGORY_SELECT_NAME, 'Select category');
define(FORM_GUARANTEE_TEXT, 'We guarantee not to disclose your personal information to third parties.');

define(FORM_ERROR_ALL_EMPTY, 'All fields are blank.');
define(FORM_ERROR_PHONE_EMPTY, 'Please specify your phone.');
define(FORM_ERROR_WRONG_CAPTCHA, 'Security code is incorrect.');

define(ITN_YOU_CAN, 'You can');
define(ITN_ASK, 'Ask a question');
define(ITN_OR, 'or');
define(ITN_APPLY, 'Register for an appointment');

define(CATALOG_SMART_FILTER_DROPDOWN_SELECT, '--- Select ---');
define(CATALOG_BACK_TO_CATALOG_LINK, 'Back to catalogue');
define(CATALOG_GO_CATALOG_LINK, 'Go to catalogue');
define(CATALOG_ORDER_LINK, '&nbsp;&nbsp;&nbsp;Order this pair');
define(CATALOG_DESCR, 'Description');


define(PAGENAV_CAPTION, 'Pages:');
define(PAGENAV_PREV, 'Prev.');
define(PAGENAV_NEXT, 'Next');
define(PAGENAV_ALL, 'All');
define(PAGENAV_PAGINAL, 'Paginal');

define(FILTER_SELECTED, 'Selected: ');

?>