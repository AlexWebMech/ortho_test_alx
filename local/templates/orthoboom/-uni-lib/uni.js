var uniLib = {};

uniLib.initComponentForNode = function (node, componentType, params, var2, var3, var4, var5)
{
	if (typeof params == "string") {
		uniLib.commandForNode(node, componentType, params, var2, var3, var4, var5);
		return;
	}
	
	for (var k in uniCore)
		if (node.hasOwnProperty(k) == false)
			node[k] = uniCore[k];	
		
	
	for (var k in window[componentType])
		if (node.hasOwnProperty(k) == false)
			node[k] = window[componentType][k];		
		
	node.each(function() {
		node.initComponent($(this), componentType, params);
	});
	return node;
}


uniLib.commandForNode = function (node, componentType, command, var1, var2, var3, var4)
{
	node.each(function() {
		node = $(this);
		while(true) 
		{
			if (node.data(componentType))
			{
				var uniObject = node.data(componentType);
				uniObject[command](var1, var2, var3, var4);
				break;
			}
			if (node.prop('tagName').toLowerCase() == 'body')
				break;
			node = node.parent();
		}
	});
}



var uniCore = {}

uniCore.initComponent = function (node, componentType, params)
{
	this.componentType = componentType;
	this.params = {};
	this.nodes = {};
	this.events = {};
	this.result = {};
	
	//if (typeof(this.defaultParams) == "object")
	//	params = $.extend(this.defaultParams, params);
	
	if (node instanceof jQuery) {
		this.nodes.wrap = node;
		node.data(componentType, this);
		if (typeof(node.data("uni-params")) != "undefined")
			if (typeof(node.data("uni-params")) == "object")
				params = $.extend(node.data("uni-params"), params);
			else
				uniConsoleLib.log("Error parsing JSON from data-uni-params");
			
			//params = $.extend(JSON.parse(node.data("uni-params"), params));
	}
	
	this.params = params || {};
	
	if (typeof this.init == "function")
		this.init();
}

uniCore.getParam = function(key, value)
{
	return this.params[key];
}
uniCore.setParam = function(key, value)
{
	if (key != null && value != null) 
		this.params[key] = value;
}

uniCore.bind = function (nodes, evn, command, data)
{
	nodes.filter(":not(.binded)").on(evn, {uniComponent: this}, function (e) {
		if (typeof(e.data.uniComponent[command]) != "undefined") {
			var settings = {e: e, node: $(this)};
			if (data)
				settings = $.extend(param, data);
			return e.data.uniComponent[command](settings);
		}
	});
	setTimeout(function () {
		nodes.addClass("binded");
	}, 0);
}

uniCore.on = function (eventCode, handler) {
	if (typeof this.events[eventCode] == "undefined")
		this.events[eventCode] = [];
	this.events[eventCode].push({handler: handler});
}

uniCore.callFunctionForNode = function (node, func, param1, delay)
{
	//if (! node || ! func)
	//	return;
	if (! node)
		node = $("<div/>");
	
	if (parseInt(delay) > 0) {
		setTimeout(function (uniObject) {
			uniObject.callFunctionForNode(node, func, param1);		
		}, delay, this, node, func, param1);
		return;
	}
	
	this[func]({node: node});
	
	
		
	
}







var uniCoreLib = {};

uniCoreLib.init = function () {
	uniCoreLib.init();
}
uniCoreLib.initNode = function (node) {
	if (! node)
		node = $("body");
	
	
}











var uniConsoleLib = {};

uniConsoleLib.log = function (message) {
	console.log(message);
	return false;
}	

uniEffectLib = {};

uniEffectLib.show = function (node, params) 
{
	
}

uniEffectLib.hide = function (node, params) 
{
	
}

uniEffectLib.fadeIn = function (node, params)
{
	params = params || {};
	params.node = node;
	
	var showDuration = params.showDuration || params.animationDuration || 350;
		
		
		
	params.node.css("transition", "all " + (showDuration / 1000) + "s ease-in");
	
	setTimeout(function () {
		params.node.css("visibility", "visible");
		params.node.css("opacity", "1");	
		
		params.node.removeClass("hide");
		params.node.addClass("show");
		params.node.addClass("showing");		
		
		setTimeout(function () {
			params.node.removeClass("showing");
		}, showDuration, params);
	}, 0, params);
}

uniEffectLib.fadeOut = function (node, params)
{
	params = params || {};
	params.node = node;
	
	var hideDuration = params.showDuration || params.animationDuration || 350;
	params.node.css("transition", "all " + (hideDuration / 1000) + "s ease-in");
	
	setTimeout(function (params) {
		params.node.css("opacity", "0");
		
		if (typeof(params.toggledNode) == "object")
			setTimeout(function () {
				params.toggledNode.addClass("hide");
				params.toggledNode.removeClass("hiding");
				params.toggledNode.removeClass("show");
			}, hideDuration, params);
	}, 0, params);	
	
	
}

uniEffectLib.slideDown = function (node, params)
{
	params = params || {};
	params.node = node;
	
	if (typeof(params.toggledNode) == "object") {
		params.toggledNode.removeClass("hide");
		params.toggledNode.addClass("show");
	}
	
	params.node.css("transition", "none 0s ease");
	params.node.css("height", "auto");
	var setHeight = params.node.outerHeight();
	params.node.css("height", "0px");
	
	setTimeout(function (params) {
		var showDuration = params.showDuration || params.animationDuration || 350;
		params.node.css("transition", "all " + (showDuration / 1000) + "s ease-in");
		params.node.css("height", setHeight + "px");
	}, 0, params);
}

uniEffectLib.slideUp = function (node, params)
{
	params = params || {};
	params.node = node;
	
	if (typeof(params.toggledNode) == "object") {
		params.toggledNode.addClass("hiding");
	}
	var hideDuration = params.hideDuration || params.animationDuration || 350;
	
	params.node.css("height", params.node.outerHeight() + "px");
	params.node.css("transition", "all " + (hideDuration / 1000) + "s ease-in");
	
	setTimeout(function (params) {
		params.node.css("height", "0px");
		
		if (typeof(params.toggledNode) == "object")
			setTimeout(function () {
				params.toggledNode.addClass("hide");
				params.toggledNode.removeClass("hiding");
				params.toggledNode.removeClass("show");
			}, hideDuration, params);
	}, 0, params);
}








var uniToolsLib = {};

uniToolsLib.scrollTo = function () 
{
	
}

uniToolsLib.currentPopup = false;
uniToolsLib.setCurrentPopup = function (node)
{
	uniToolsLib.currentPopup = node;
}
uniToolsLib.clearCurrentPopup = function (node)
{
	uniToolsLib.currentPopup = false;
}


$(document).ready(function() {
	$("body").on("click", function (e) {
		if (typeof(uniToolsLib.currentPopup) == "object") {
			if (jQuery(e.target).closest(uniToolsLib.currentPopup).length) {
				
			} else {
				uniToolsLib.currentPopup.uniPopup("hide");
			}
		}
	});
});


var uniPopup = {};

(function ($)
{
	$.fn.uniPopup = function(params, var2, var3, var4, var5)
	{
		return uniLib.initComponentForNode(this, "uniPopup", params, var2, var3, var4, var5);	
	}
}(jQuery));

uniPopup.init = function (wrap)
{
	var wrap = this.nodes.wrap;
	
	this.nodes.closes = wrap.find(".ui-popup-close, .js-popup-close");
	this.bind(this.nodes.closes, "click", "hide");
	
	this.show();
}

uniPopup.show = function ()
{
	if (false)
		uniToolsLib.setCurrentPopup(this.nodes.wrap);
	
	uniEffectLib.fadeIn(this.nodes.wrap, this.params);
}

uniPopup.hide = function ()
{
	uniEffectLib.fadeOut(this.nodes.wrap, this.params);
}





var uniTabs = {};

(function ($)
{
	$.fn.uniTabs = function(params, var2, var3, var4, var5)
	{
		return uniLib.initComponentForNode(this, "uniTabs", params, var2, var3, var4, var5);	
	}
}(jQuery));

uniTabs.init = function (wrap)
{
	var wrap = this.nodes.wrap;
		
	if (! this.params.animationDuration)
		this.params.animationDuration = 180;
	this.result.currentTabIndex = false;
	
	if (this.params.tabsTarget)
		this.nodes.tabs = wrap.find(this.params.tabsTarget);
	else
		this.nodes.tabs = wrap.find("*[role=tab]");
	this.bind(this.nodes.tabs, "click", "setTab");
	
	if (this.params.contentsTarget)
		this.nodes.contents = wrap.find(this.params.contentsTarget);
	else
		this.nodes.contents = wrap.find("*[role=tabpanel]");
	
	if (! this.nodes.contents.length)
		return;
	
	for (var i = 0; i < this.nodes.contents.length; i++) {
		var tabNode = this.nodes.tabs.eq(i);
		if (tabNode.prop("href")) {
			var contentNode = $.find(tabNode.prop("href"));
		} else if (tabNode.data("tab-code")) {
			var contentNode = this.nodes.contents.find("*[tab-code=\"" + tabNode.data("tab-code") + "\"]");
		}else {
			var contentNode = this.nodes.contents.eq(i);
		}
		if (contentNode) {
			tabNode.data("content-node", contentNode);
		}
	}
	
	var activeTabIndex = 0;
	for (var i = 0; i < this.nodes.contents.length; i++) {
		var contentNode = this.nodes.contents.eq(i);
		if (i != activeTabIndex)
			contentNode.addClass("hide");
		else
			contentNode.addClass("show");
	}
	
	this.setTab({index: 0, hideDuration: 0, showDuration: 0});
}

uniTabs.setTab = function (settings) {
	if (settings.node) {
		var tabNode = settings.node;
	} else if (typeof(settings.index) != "undefined") {
		var tabNode = this.nodes.tabs.eq(settings.index);
	} else if (typeof(settings.code) != "undefined") {
		var tabNode = this.nodes.tabs.find("*[tab-code=\"" + settings.code + "\"]");
	}
	
	if (! tabNode)
		return;
	if (tabNode.prop("disabled"))
		return;
	
	var contentNode = tabNode.data("content-node");
	if (! contentNode)
		return;
	
	if (this.result.currentTabIndex !== false) {
		this.result.oldTabIndex = this.result.currentTabIndex;
		this.nodes.oldTab = this.nodes.currentTab;
		this.nodes.oldContent = this.nodes.currentContent;
	}
	
	this.result.currentTabIndex = this.nodes.tabs.index(tabNode);
	this.nodes.currentTab = tabNode;
	this.nodes.currentContent = contentNode;

	this.setTabEffect(settings);
}

uniTabs.setTabEffect = function (settings) 
{
	this.nodes.tabs.removeClass("active");
	this.nodes.currentTab.addClass("active");
	
	if (this.nodes.oldContent)
	{
		var hideDuration = (typeof(settings.hideDuration) != "undefined") ? settings.hideDuration : this.params.hideDuration || this.params.animationDuration || 0;
		
		this.nodes.oldContent.css("transistion", "all " + hideDuration + "ms");
		this.nodes.oldContent.css("opacity", 0);
		
		if (parseInt(hideDuration) > 0)
			setTimeout(function (uniObject, settings) {
				uniObject.hideTabEffect(settings);
			}, parseInt(hideDuration), this, settings);
		else
			this.hideTabEffect(settings);
	} else {
		this.showTabEffect(settings);
	}
}

uniTabs.hideTabEffect = function (settings) 
{
	this.nodes.oldContent.css("visibility", "hidden");
	this.nodes.oldContent.css("position", "absolute");
	
	var showDuration = (typeof(settings.showDuration) != "undefined") ? settings.showDuration : this.params.showDuration || this.params.animationDuration || 0;
	
	this.showTabEffect(settings);	
}

uniTabs.showTabEffect = function (settings) 
{
	var showDuration = (typeof(settings.showDuration) != "undefined") ? settings.showDuration : this.params.showDuration || this.params.animationDuration || 0;
	
	this.nodes.currentContent.css("transistion", "all " + showDuration + "ms");
	
	this.nodes.currentContent.css("position", "relative");
	this.nodes.currentContent.css("visibility", "visible");
	this.nodes.currentContent.css("opacity", 1);	
}

///// MODAL /////

var uniModal = {};

(function ($)
{
	$.fn.uniModal = function(params, var2, var3, var4, var5)
	{
		return uniLib.initComponentForNode(this, "uniModal", params, var2, var3, var4, var5);	
	}
}(jQuery));

uniModal.init = function (wrap)
{
	var wrap = this.nodes.wrap;
	
	this.nodes.overlay = wrap.find(".ui-overlay");
	this.bind(this.nodes.overlay, "click", "hide");
	
	this.nodes.closes = wrap.find(".ui-modal-close");
	this.bind(this.nodes.closes, "click", "hide");
}

uniModal.show = function ()
{
	this.nodes.wrap.removeClass("hide");
	this.nodes.wrap.addClass("show");	
	this.hideEffect();
}

uniModal.hide = function ()
{
	this.nodes.wrap.removeClass("show");
	this.nodes.wrap.addClass("hide");	
	this.showEffect();
}


uniModal.showEffect = function () {

}

uniModal.hideEffect = function () {

}









	/*
	var hideDuration = typeof(settings.hideDuration) != "undefined" ? settings.hideDuration : this.params.hideDuration;
	var showDuration = 0;
	
	if (this.result.activeContentNode) {
		this.callFunctionForNode(this.result.activeContentNode, "hideContentStart", null);
		this.callFunctionForNode(this.result.activeContentNode, "hideContentEnd", null, hideDuration);
	}	
	
	if (contentNode) {
		this.callFunctionForNode(contentNode, "showContentStart", null, hideDuration);
		this.callFunctionForNode(contentNode, "showContentEnd", null, hideDuration + showDuration);
	}
	
uniTabs.showContentStart = function (settings) 
{
	settings.node.css("position", "relative");
	settings.node.css("visibility", "visible");
	settings.node.css("opacity", 1);
}	

uniTabs.showContentEnd = function (settings) 
{
	
}	

uniTabs.hideContentStart = function (settings) 
{
	settings.node.css("opacity", 0);
}	

uniTabs.hideContentEnd = function (settings) 
{
	settings.node.css("visibility", "hidden");
	settings.node.css("position", "absolute");
}		
	
	*/
	//this.nodes.contents.hide();
	//contentNode.show();

/*
uniTabs.hideContentStart = function (settings)
{
	settings.node.addClass("hiding");
}

uniTabs.hideContentEnd = function (settings)
{
	settings.node.removeClass("hiding");
	settings.node.addClass("hide");
	
	settings.node.removeClass("show");
	//settings.node.css("display", "none");
	this.result.activeContentNode = false;
}

uniTabs.showContentStart = function (settings)
{	
	this.result.activeContentNode = settings.node;
	//settings.node.css("display", "block");
	
	settings.node.removeClass("hide");
	settings.node.addClass("show");
	settings.node.addClass("showing");
	
	
	
}

	this.nodes.tabs.removeClass("active");
	settings.node.addClass("active");
	
	var hideDuration = (typeof(settings.hideDuration) != "undefined") ? settings.hideDuration : this.params.hideDuration || this.params.animationDuration || 0;
	
	hideStart();
	setTimeout(function (uniObject) {
		uniObject.hideEnd();
		uniObject.showFinish();
	}, hideDuration, this);

uniTabs.showFinish = function (settings)
{
	var showDuration = (typeof(settings.showDuration) != "undefined") ? settings.showDuration : this.params.showDuration || this.params.animationDuration || 0;
	
	showStart();
	setTimeout(function (uniObject) {
		uniObject.showEnd();
	}, showDuration, this);
}

uniTabs.showStart = function (settings) 
{
	settings.node.css("position", "relative");
	settings.node.css("visibility", "visible");
	settings.node.css("opacity", 1);
}	

uniTabs.showtEnd = function (settings) 
{
	
}	

uniTabs.hideStart = function (settings) 
{
	settings.node.css("opacity", 0);
}	

uniTabs.hideEnd = function (settings) 
{

}	





uniTabs.showContentEnd = function (settings)
{
	settings.node.removeClass("showing");
}
*/


uniTabs.showComplete = function (settings)
{
	
}









var uniFormLib = {};

uniFormLib.validateForm = function (formTag)
{
	var errors = [];
	var formNode = $(formTag);
	var inputNodes = formNode.find("input, select, textarea");
	
	for (var i = 0; i < inputNodes.length; i++) {
		var inputNode = inputNodes.eq(i);
		var errorText = "";
		
		if (typeof(inputNode.data("required")) != "undefined") {
			if (inputNode.prop("type").toLowerCase() == "radio" || inputNode.prop("type").toLowerCase() == "checkbox") {
				if (((typeof(inputNode.prop("name")) == "undefined" || ! inputNode.prop("name").length) && ! inputNode.prop("checked")) || inputNode.prop("name") && ! formNode.find("input[name=\"" + inputNode.prop("name")  + "\"]:checked").length)
				{
					errors.push(uniFormLib.getInputError(inputNode, "required"));
				}
			} else {
				if (typeof(inputNode.prop("value")) == "undefined" || ! inputNode.prop("value").length)
				{
					errors.push(uniFormLib.getInputError(inputNode, "required"));
				}
			}
		}
		
		if (errorText.length)
			errors.push(errorText);
	}
	
	if (errors.length)
	{
		alert(errors[0]);
		return false;
	}	
	return true;
}


uniFormLib.getInputError = function (inputNode, errorType)
{
	var inputTitle = inputNode.data("title");
	if (typeof(inputTitle) == "undefined") {
		
	}
	
	if (typeof(inputTitle) != "undefined")
		var inputTitlePlacer = "\"" + inputTitle + "\" ";
	else 
		var inputTitlePlacer = "";
	
	var errorText = "";
	if (typeof(inputNode.data(errorType + "-error-message")) != "undefined") {
		errorText = inputNode.data(errorType + "-error-message");
	} else if (typeof(inputNode.data("error-message")) != "undefined") {
		errorText = inputNode.data("error-message");
	} else {
		errorText = "Обязательное поле " + inputTitlePlacer + "не заполнено";
	}
	return errorText;
	
}

uniFormLib.clearInputError = function (inputNode)
{
	
}



uniFormLib.submitForm = function (formTag)
{
	if (uniFormLib.validateForm(formTag))
	{
		var formNode = $(formTag);
		
		if (typeof(formNode.data("uni-form-check")) != "undefined")
		{
			if (formNode.find("input[name=form_check]").length)
				formNode.find("input[name=form_check]").prop("value", "ok");
			else	
				formNode.append('<input type="hidden" name="form_check" value="ok" />');
		}		
		
		if (typeof(formNode.data("uni-ajax-wrap")) != "undefined")
		{
			//uniAjaxLib.ajax({
			uniAjaxLib.ajaxForNode(formNode, {
				url: typeof(formNode.prop("action") != "undefined") ? formNode.prop("action") : window.location.href,
				wrapNode: formNode,
				formNode: formNode,
				//method: typeof(formNode.prop("method") != "undefined") && formNode.prop("method").toLowerCase() == "post" ? "post" : "get",
				cut: true,
			});

			return false;
		}

		if (typeof(formNode.data("uni-ajax-form")) != "undefined")
		{
			
		}			
		
		return true;
	}
	
	return false;
}




var uniAjaxLib = {};
uniAjaxLib.params = {};
uniAjaxLib.profiles = {};
uniAjaxLib.isBusy = false;
uniAjaxLib.queue = [];

uniAjaxLib.defaultAjaxParams = {
	url: "",
	method: "get",
	data: "",
	timeout: 5000,
}

uniAjaxLib.errorText = "Произошла ошибка при загрузке данных.";
uniAjaxLib.errorUniModalParams = {
	contentHtml: uniAjaxLib.errorText,
};



uniAjaxLib.init = function () 
{
	$("body").on("click", "*[data-uni-ajax-link]", function () {
		uniAjaxLib.ajaxForNode($(this).parents("*[data-uni-ajax-wrap]").eq(0));
		return false;
	});
	
	$("body").on("click", "*[data-uni-ajax-action]", function () {
		uniAjaxLib.ajaxForNode($(this));
		return false;
	});	
}

uniAjaxLib.ajaxForNode = function (node, exparams) 
{
	var params = {};
	
	if (node.data("uni-ajax-profile")) {
		var params = $.extend(uniAjaxLib.getProfile(node.data("uni-ajax-profile")));
		
	}
	if (typeof(node.data("uni-ajax-params")) == "object") {
		params = $.extend(params, node.data("uni-ajax-params"));
	}
	var data = node.data();
	if (data) {
		for (var k in data) {
			if (k.substr(0, 12) == "uniAjaxParam" && k != "uniAjaxParams")
				params[k.substr(12)] = (data[k] != "") ? data[k] : true; //##buggy
		}
	}
	
	params = $.extend(params, exparams);
		
	if (! $.isEmptyObject(params)) {
		
		//params.cut = true;
		
		//if (node.data("uni-ajax-wrap") || node.data("uni-ajax-form"))
		//	params["wrapNode"] = node; 
		uniAjaxLib.ajax(params);
	}
}

uniAjaxLib.addProfile = function (profileCode, params)
{
	uniAjaxLib.profiles[profileCode] = params;
}

uniAjaxLib.getProfile = function (profileCode) 
{
	if (typeof(uniAjaxLib.profiles[profileCode]) != "undefined")
		return uniAjaxLib.profiles[profileCode];
	return {};
}

uniAjaxLib.ajaxProfile = function (profileCode) 
{
	uniAjaxLib.ajax(uniAjaxLib.getProfile(profileCode));
}


uniAjaxLib.ajax = function (params)
{
	
	if ((typeof(params) != "object" && typeof(params) != "function") || $.isEmptyObject(params))
		return uniConsoleLib.log("Empty ajax params");
	
	if (uniAjaxLib.isBusy) {
		if (uniAjaxLib.queue.length) {
			/*console.log(params);
			
			//var paramsAsObj = JSON.stringify(params);
			for (var i in uniAjaxLib.queue) {
				if (JSON.stringify(uniAjaxLib.queue.i) === paramsAsObj)
					alert('n');
			} */
		}

		uniAjaxLib.queue.push(params);
		return false;
		
	}
	
	if (typeof(params.ajaxParams) != "object")
		params.ajaxParams = {}; //uniAjaxLib.defaultAjaxParams || 

	var ajaxParamCodes = ["url", "method", "data"];
	for (var i in ajaxParamCodes) {
		var k = ajaxParamCodes[i];
		if (typeof(params[k]) != "undefined")
			params.ajaxParams[k] = params[k];
	}

	if (typeof(params.ajaxParams["data"]) != "string")
		params.ajaxParams.data = "";

	if (typeof(params.ajaxParams["url"]) != "string")
		params.ajaxParams.url = window.location.href;

	
	
	if (! params.wrapNode && params.wrapTarget)
		params.wrapNode = $(params.wrapTarget);
	if (! params.wrapNode.length)
		return uniConsoleLib.log("Ajax wrap not found");
	
	if (typeof(params.formNode) == "undefined" && params.formTarget)
		params.formNode = $(params.formTarget);
	if (typeof(params.formNode) == "undefined" && params.wrapNode.is("form"))
		params.formNode = params.wrapNode;
	
	if (params.formNode) {
		if (! params.ajaxParams.method && params.formNode.eq(0).prop("method"))
			params.ajaxParams.method = params.formNode.eq(0).prop("method");
		params.ajaxParams.data += (params.ajaxParams.data.length ? "&" : "") + params.formNode.serialize();
	}
	
	
	
	
	
	if (params.setUrl) {
		
	} else if (params.updateUrl) {
		
	}
	
	if (typeof(params.additionalData) != "undefined")
		params.ajaxParams.data += (params.ajaxParams.data.length ? "&" : "") + params.additionalData;
	
	if (! params.commands) {
		var cutTarget = "";
		if (params.cut) {
			
			if (0) {
				
				//cutTarget777 = params.cutTarget777;
			} else {
				
				if (typeof(params.wrapNode.prop("id")) != "undefined" && params.wrapNode.prop("id").length)
					cutTarget = "#" + params.wrapNode.prop("id");
				else if (typeof(params.wrapNode.prop("class")) != "undefined")
					cutTarget += "." + params.wrapNode.prop("class").split(" ").join(".");
			}
		}
		params.commands = [{cutTarget: cutTarget, command: "html", targetNode: params.wrapNode}];

	}


		

	
	//if (typeof(params.additionalData) != "undefined")
		
		//params.ajaxParams.data += ((params.ajaxParams.data.length) ? "&": "") + params.additionalData;
	
	//params.ajaxParams.data += ((params.ajaxParams.data.length) ? "&" : "") + "ajax=y";
	
	
	params.ajaxParams.params = $.extend(params);
	params.ajaxParams.error = uniAjaxLib.onError;
	params.ajaxParams.success = uniAjaxLib.onResponse;
	
	
	
	//alert(params.ajaxParams.data.length);
	//alert(params.ajaxParams.data.length);
		
	uniAjaxLib.isBusy = true;
	
	if (typeof(params.wrapNode) == "object")
		params.wrapNode.addClass("ui-ajax-wrap-loading");

	$.ajax(params.ajaxParams);
}

uniAjaxLib.onResponse = function (ajaxResponse)
{
	var params = this.params;
	var ajaxNode = null;
	var html = null;
	
		
	//console.log(ajaxResponse);
	//console.log(params.commands);
	
	console.log(params.commands);
	
	var successCount = 0;
	for (var k in params.commands) {
		//var command = $params.commands[k];
		var command = Object.assign({}, params.commands[k]);
		console.log(command);
		
		if (! command.targetNode && command.target)
			command.targetNode = params.wrapNode.find(command.target);

		if (! command.targetNode.length) {
			console.log('Command target not found in source: ' + command.target);
			continue;
		}
		
		/* if (command.cut && ! command.cutTarget777) {
			command.cutTarget777 = "";
			if (typeof(params.wrapNode.prop("id")) != "undefined" && params.wrapNode.prop("id").length)
				command.cutTarget777 = "#" + params.wrapNode.prop("id");
			else if (typeof(params.wrapNode.prop("class")) != "undefined")
				command.cutTarget777 += "." + params.wrapNode.prop("class").split(" ").join(".");		
		}
		*/
		
		
		
		
		//if (command.cut && command.target)
		//	command.cutTarget = command.target;
		
		
		
		if (command.cutTarget) {
			if (ajaxNode === null)
				ajaxNode = $("<div/>").html(ajaxResponse);
			

			var ajaxCutNode = ajaxNode.find(command.cutTarget);
			//alert(ajaxNode.find("main form").length);
			
			if (! ajaxCutNode.length) {
				console.log('Comnmad target not found in response: ' + command.cutTarget);
				continue;
			}
			html = ajaxCutNode.html();
			
		} else {
			html = ajaxResponse;
		}
		
		
		if (! command.command || command.command == "html")
			command.targetNode.html(html);
		else if (command.command == "append")
			command.targetNode.append(html);
		else if (command.command == "prepend")
			command.targetNode.prepend(html);
		else {
			uniConsoleLib.log("Unknown ajax command");
			continue;
		}
		
		successCount++;
	}
	
	if (typeof(params.success1) == "function") {
		params.success1(params);
	}
	
	
	if (successCount > 0) {
		if (typeof(params.success) == "function") {
			params.success(this);
		}
	} else {
		//uniConsoleLib.log("Ajax error: no commands");
		//uniAjaxLib.onError(params);
	}
	
	uniAjaxLib.finishAjax(params);
}

uniAjaxLib.onError = function (params)
{
	params = this.params || params || {};
	//var params = uniAjaxLib.params;
	
	uniConsoleLib.log(params);
	
	if (false && typeof(uniModal) != "undefined" && uniAjaxLib.errorUniModalParams) {
		//$.uniModal(uniAjaxLib.errorUniModalParams);
	} else {
		alert(uniAjaxLib.errorText);
	}
	

	
	uniAjaxLib.isBusy = false;
	return false;
}

uniAjaxLib.finishAjax = function (params)
{
	uniAjaxLib.isBusy = false;
	
	if (typeof(params.wrapNode) == "object")
		params.wrapNode.removeClass("ui-ajax-wrap-loading");
	
	if (uniAjaxLib.queue.length) {
		var params = uniAjaxLib.queue[0];
		delete uniAjaxLib.queue.shift();
		uniAjaxLib.ajax(params);
	}
}


uniAjaxLib.initNode = function (node)
{
	node.find("form[data-uni-ajax-wrap], form[data-uni-form]").on("submit", function () {return uniFormLib.submitForm(this);});
	
}

$(document).ready(function() {
	uniAjaxLib.init();
	uniAjaxLib.initNode($("body"));
});









var uniNumberField = {};

(function ($)
{
	$.fn.uniNumberField = function(params, var2, var3, var4, var5)
	{
		return uniLib.initComponentForNode(this, "uniNumberField", params, var2, var3, var4, var5);	
	}
}(jQuery));

uniNumberField.init = function(wrap)
{
	this.params = $.extend({
		step: 1,
	}, this.params);
	
	var wrap = this.nodes.wrap;
	
	this.nodes.input = wrap.find("input[type=text]");
	this.bind(this.nodes.input, "input paste", "onInputChange");

	this.nodes.minus = wrap.find("*[role=minus], .ui-number-field-minus");
	this.bind(this.nodes.minus, "click", "minus");
	
	this.nodes.minus = wrap.find("*[role=plus], .ui-number-field-plus");
	this.bind(this.nodes.minus, "click", "plus");
	
	this.format();
}

uniNumberField.minus = function(settings)
{
	var step = (settings && typeof(settings.step) != "undefined") ? settings.step : this.params.step;
	this.changeValue(-step);
};

uniNumberField.plus = function(settings)
{
	var step = (settings && typeof(settings.step) != "undefined") ? settings.step : this.params.step;
	this.changeValue(step);
};

uniNumberField.changeValue = function(addValue)
{
	var input = this.nodes.input;
	
	if (input.prop("disabled"))
		return false;
	
	var value = parseFloat(this.nodes.input.val());
	if (isNaN(value))
		return false;

	if (typeof(this.params.minValue) != "undefined")
		if (value + addValue < this.params.minValue)
			return false;
		
	if (typeof(this.params.maxValue) != "undefined")
		if (value + addValue > this.params.maxValue)
			return false;
	
	input.prop("value", value + addValue);
	input.change();
};

uniNumberField.format = function () 
{
	var input = this.nodes.input;
	var value = input.prop("value");
	value = value.replace(",", ".");
	value = value.replace(/[^-0-9.$]/gim,'');
	input.prop("value", value);
}

uniNumberField.onInputChange = function()
{
	this.format();
}
































