var uniFilter = {};

/*
(function ($)
{
	$.fn.uniFilter = function(params, var2, var3, var4, var5)
	{
		return uniLib.initComponentForNode(this, "uniFilter", params, var2, var3, var4, var5);	
	}
}(jQuery));

*/

uniCoreLib.addComponent("uniFilter");

uniFilter.defaultParams = {
	animationFieldDuration: 180,
};

uniFilter.init = function ()
{	
	var wrapNode = this.nodes.wrap;
	
	if (typeof(this.params.uiMainClass) == "undefined")
		this.params.uiMainClass = "catalog";
	
	this.nodes.frm = this.nodes.wrap.is("form") ? this.nodes.wrap : this.nodes.wrap.find("form").eq(0);
	
	this.nodes.fields = wrapNode.find(".ui-filter--field");
	
	this.initFields();
		
	this.bind(wrapNode.find(".ui-filter--toggle-action"), "click", "toggleFilter");
	
	if (this.params.collapseFieldsDisallow !== true) {
		this.bind(wrapNode.find(".ui-filter--hide-all-fields, .ui-filter--js-hide-all-fields"), "click", "hideAllFields");
		this.bind(wrapNode.find(".ui-filter--show-all-fields, .ui-filter--js-show-all-fields"), "click", "showAllFields");
	}
	this.bind(wrapNode.find(".ui-filter--clear"), "click", "clearFilter");
	this.bind(wrapNode.find(".ui-filter--submit"), "click", "refreshItems");
	
	this.nodes.result = wrapNode.find(".ui-filter--result");
	this.bind(wrapNode.find(".ui-filter--result-close, .ui-filter--result-js-close"), "click", "hideResult");
	
	//this.bind(wrapNode.find(".ui-filter--result-link"), "click", "refreshItems");
	//alert('Y');
	this.bind(wrapNode.find(".ui-filter--result-link"), "click", "refreshAll");
	
	this.bind($(".ui-filter--tags-outer .ui-filter--tag-clear"), "click", "clearFieldAndResfresh");
	
	this.initLocalStorage();
}

uniFilter.initFields = function ()
{
	var wrapNode = this.nodes.wrap.eq(0);
	
	for (var i = 0; i < this.nodes.fields.length; i++) {
		var fieldNode = this.nodes.fields.eq(i);
		this.changeField({node: fieldNode});
	}

	this.nodes.inputs = wrapNode.find("input, select, textarea");
	this.bind(this.nodes.inputs, "change", "changeField");
	
	if (this.params.instantRefreshItems)
		this.bind(this.nodes.inputs.filter("input[type=radio], input[type=checkbox]"), "change", "refreshAll");
	else
		this.bind(this.nodes.inputs.filter("input[type=radio], input[type=checkbox]"), "change", "refreshResult");	
	
	if (this.params.collapseFieldsDisallow !== true)
		this.bind(wrapNode.find(".ui-filter--js-field-toggle-field"), "click", "toggleField");
	
	this.bind(wrapNode.find(".ui-filter--field-js-clear-field, .ui-filter--field-clear-field"), "click", "clearField");
	
	
	
}


uniFilter.getFieldNode = function (var1) {
	var fieldNode = false;
	if (typeof(var1) == "string") {
		fieldNode = this.nodes.fields.filter("[data-field-code=\"" + var1 + "\"]");
	} else if (typeof(var1) == "object") {
		if (typeof(var1.node) == "object") { //## is jquery object
			if (var1.node.data("target-field-code"))
				fieldNode = this.nodes.fields.filter("[data-field-code=\"" + var1.node.data("target-field-code") + "\"]").eq(0);
			else if (var1.node.is(".ui-filter--field"))
				fieldNode = var1.node;
			else
				fieldNode = var1.node.parents(".ui-filter--field").eq(0);
		}
	}
	if (fieldNode && fieldNode.length)
		return fieldNode;
	else
		return false;
}

uniFilter.toggleField = function (settings)
{
	var fieldNode = this.getFieldNode(settings);
	if (! fieldNode)
		return;
	if (fieldNode.hasClass("hiding") || fieldNode.hasClass("showing"))
		return;	
	
	if (fieldNode.is(".hide")) {
		this.showField(settings);
	} else {
		this.hideField(settings);
	}
}

uniFilter.showField = function (settings)
{
	var fieldNode = this.getFieldNode(settings);
	if (! fieldNode)
		return;
	if (! fieldNode.hasClass("hide"))
		return;		
	if (fieldNode.hasClass("showing"))
		return;	
	if (true) {
		var mainFieldNode = fieldNode.find(".ui-filter--field-main-col");
		var showDuration = (typeof(settings.showDuration) != "undefined") ? settings.showDuration : this.params.showFieldDuration || this.params.animationFieldDuration || 0;
		uniEffectLib.slideDown(mainFieldNode, {toggledNode: fieldNode, duration: showDuration});
	}
	
	if (typeof(this.result.storage) == "object") {
		var fieldCode = fieldNode.data("field-code");
		if (fieldCode) {
			if (typeof(this.result.storage.fields.fieldCode) != "object" || this.result.storage.fields.fieldCode === null)
				this.result.storage.fields[fieldCode] = {};
			this.result.storage.fields[fieldCode]["show"] = "show";
			this.saveLocalStorage();
		}
	}	
}

uniFilter.hideField = function (settings)
{
	var fieldNode = this.getFieldNode(settings);
	if (! fieldNode)
		return;
	if (fieldNode.hasClass("hide"))
		return;		
	if (fieldNode.hasClass("hiding"))
		return;
	if (true) {
		var mainFieldNode = fieldNode.find(".ui-filter--field-main-col");
		var hideDuration = (typeof(settings.hideDuration) != "undefined") ? settings.hideDuration : this.params.hideFieldDuration || this.params.animationFieldDuration || 0;
		uniEffectLib.slideUp(mainFieldNode, {toggledNode: fieldNode, duration: hideDuration});
	}
	
	if (typeof(this.result.storage) == "object") {
		var fieldCode = fieldNode.data("field-code");
		if (fieldCode) {
			if (typeof(this.result.storage.fields.fieldCode) != "object" || this.result.storage.fields.fieldCode === null)
				this.result.storage.fields[fieldCode] = {};
			this.result.storage.fields[fieldCode]["show"] = "hide";
			this.saveLocalStorage();
		}
	}
}

uniFilter.showFieldInstantly = function (settings)
{
	settings["showDuration"] = 0;
	this.showField(settings);
}

uniFilter.hideFieldInstantly = function (settings)
{
	settings["hideDuration"] = 0;
	this.hideField(settings);
}



uniFilter.hideAllFields = function (settings)
{
	for (var i = 0; i < this.nodes.fields.length; i++) {
		var fieldNode = this.nodes.fields.eq(i);
		this.hideFieldInstantly({node: fieldNode});
	}
}

uniFilter.showAllFields = function (settings)
{
	for (var i = 0; i < this.nodes.fields.length; i++) {
		var fieldNode = this.nodes.fields.eq(i);
		this.showFieldInstantly({node: fieldNode});
	}
}

uniFilter.changeField = function (settings)
{
	var fieldNode = this.getFieldNode(settings);
	if (! fieldNode)
		return;
	
	var inputNodes = fieldNode.find("input, select, textarea");
	if (inputNodes.is("input[type=radio]:checked, input[type=checkbox]:checked, input:not([type=hidden]):not([type=radio]):not([type=checkbox]):not([type=button]):not([type=submit])[value]")) {
		fieldNode.addClass("ui-filter-field-state-set");
	} else {
		fieldNode.removeClass("ui-filter-field-state-set");
	}
}

uniFilter.clearField = function (settings)
{
	var fieldNode = this.getFieldNode(settings);
	if (! fieldNode)
		return;
	var inputNodes = fieldNode.find("input, select, textarea");
	for (var i = 0; i < inputNodes.length; i++) {
		var inputNode = inputNodes.eq(i);
		if (inputNode.prop("type").toLowerCase() == "checkbox" || inputNode.prop("type").toLowerCase() == "radio") {
			inputNode.prop("checked", false);
		} else if (inputNode.prop("type").toLowerCase() != "hidden" && inputNode.prop("type").toLowerCase() != "button" && inputNode.prop("type").toLowerCase() != "submit") {
			inputNode.prop("value", "");
		}
	}
	fieldNode.removeClass("ui-filter-field-state-set");
	return false;
}

uniFilter.clearAllFields = function (settings)
{
	for (var i = 0; i < this.nodes.fields.length; i++) {
		var fieldNode = this.nodes.fields.eq(i);
		this.clearField({node: fieldNode});
	}
}

uniFilter.clearFieldAndResfresh = function (settings)
{
	this.clearField(settings);
	this.refreshAll();
}

uniFilter.clearFilter = function (settings)
{
	this.clearAllFields();
	this.refreshFilter();
}

uniFilter.toggleFilter = function ()
{
	var wrapNode = this.nodes.wrap.eq(0);
	var mainNode = wrapNode.find(".ui-filter--block");
	
	if (! mainNode.is(":visible")) {
		mainNode.slideDown(250);
		wrapNode.addClass("show");
	} else {
		mainNode.slideUp(250);
		wrapNode.addClass("hide");
	}
}





uniFilter.refresh = function (settings)
{
	this.refreshFilter();
}

uniFilter.submitForm = function (settings)
{
	this.nodes.frm.submit();
}

uniFilter.updateUrl = function ()
{
	if (typeof(this.params.resultUrl) != "undefined") {
		history.pushState(null, null, this.params.resultUrl);
		//alert(this.params.resultUrl);
	} else {
		var getQuery = this.nodes.frm.serialize();
		history.pushState(null, null, "?" + getQuery);
	}
}

uniFilter.refreshAll = function ()
{
	this.hideResult();
	this.updateUrl();
	
	var ajaxAllProfile = uniAjaxLib.getProfile(this.params.uiMainClass + "--all");
	if (ajaxAllProfile) {
		
		uniAjaxLib.ajax(ajaxAllProfile);
	}
	else {
		this.refreshItems();
		this.refreshFilter();
	}
}

uniFilter.refreshFilter = function ()
{
	var ajaxFilterProfile = uniAjaxLib.getProfile(this.params.uiMainClass + "--filter");
	if (ajaxFilterProfile) {
		ajaxFilterProfile["uniObject"] = this;
		//ajaxFilterProfile["success"] = function (params) {params.uniObject.init();};
		uniAjaxLib.ajax(ajaxFilterProfile);
	} else
		this.submitForm();
}

uniFilter.refreshItems = function ()
{
	this.hideResult();
	this.updateUrl();

	var ajaxItemsProfile = uniAjaxLib.getProfile(this.params.uiMainClass + "--items");
	if (ajaxItemsProfile) {
		//ajaxItemsProfile.scrollTo = false;
	
		uniAjaxLib.ajax(ajaxItemsProfile)
	} else
		this.submitForm();
	
	return false;
}

uniFilter.refreshResult = function (settings)
{

	this.hideResult();
	
	if (typeof(settings.node) == "object") {
		this.result.currentNodeTarget = "";
		//if (settings.node.prop("class"))
		//	this.result.currentNodeTarget += "." + settings.node.prop("class").split(" ").join(".");
		if (settings.node.prop("name").length)
			this.result.currentNodeTarget += "[name=\"" + settings.node.prop("name") + "\"]";
		if (settings.node.prop("type") == "checkbox" || settings.node.prop("type") == "radio" || settings.node.prop("type") == "hidden")
			if (settings.node.prop("value").length)
				this.result.currentNodeTarget += "[value=\"" + settings.node.prop("value") + "\"]";
		
		
	}
	
	var ajaxResultProfile = uniAjaxLib.getProfile(this.params.uiMainClass + "--filter-result");
	
	if (ajaxResultProfile)
	{
		
		//ajaxResultProfile["data"] = "uni_ajax_filter_show_result=Y";
		ajaxResultProfile["data"] = "uni_ajax_block=" + this.params.uiMainClass + "--filter";
		ajaxResultProfile["uniObject"] = this;
		//ajaxResultProfile["success1"] = function (params) {params.uniObject.init(); params.uniObject.posResult();};
		ajaxResultProfile["success1"] = function (params) {params.uniObject.posResult();};
		uniAjaxLib.ajax(ajaxResultProfile);
	}
	else
		this.refreshFilter();
	
	
	
	/*
	this.refreshResultHideEffect();
	
	var commands = [
		{target: ".ui-filter--result", cutTarget: ".ui-filter--result", command: "html"},
	];
	for (var i = 0; i < this.nodes.fields; i++) {
		var fieldNode = this.nodes.fields.eq(i);
		commands.push({target: fieldNode.prop("class").split(" ").join("."), cutTarget: fieldNode.prop("class").split(" ").join("."), command: "html"});
	}
	
		
	uniAjaxLib.ajaxProfile(this.params.uiMainClass + "-result");	
		
		
	uniAjaxLib.ajax({
		uniObject: this,
		wrapNode: this.nodes.wrap,
		formNode: this.nodes.wrap.is("form") ? this.nodes.wrap : this.nodes.wrap.parents("form").eq(0),
		cut: true,
		commands: commands,
		success1: function (params) {
			params.uniObject.init();
			params.uniObject.showResult();
		},
	});
	*/
}

uniFilter.posResult = function (settings)
{

	//this.result.currentNodeTarget.css("top", "0");
	//this.result.currentNodeTarget.uniPopup();
	//this.result.currentNodeTarget.uniPopup("show");

	//alert(this.result.currentNodeTarget);
	
	if (this.result.currentNodeTarget) {
		var currentNode = this.nodes.wrap.find(this.result.currentNodeTarget);
	
		if (currentNode.length) {
			if (! currentNode.is(":visible"))
				currentNode = currentNode.parent();
			this.nodes.result.css("transition", "top 0s ease 0s");
			setTimeout(function (uniObject) {
				uniObject.nodes.result.css("top", (currentNode.position()["top"] + (currentNode.outerHeight() / 2)) + "px");
				setTimeout(function (uniObject) {
					uniObject.showResult();
				}, 16, uniObject);
			}, 16, this);
		}
	}


	
	//this.nodes.result.uniPopup("show");
	


}

uniFilter.showResult = function (settings)
{
	
	//this.nodes.result.uniPopup(this.params.resultPopup || {}).uniPopup("show");
	this.nodes.result.fadeIn(350);
}
uniFilter.hideResult = function (settings)
{
	//this.nodes.result.uniPopup(this.params.resultPopup || {}).uniPopup("hide");
	this.nodes.result.fadeOut(350);
}


//FIlTER - SETTINGS

uniFilter.initLocalStorage = function ()
{
	var settingStr = localStorage.getItem("uni__" + this.params.uiMainClass + "__filter");
	if (settingStr)
		this.result.storage = JSON.parse(settingStr);
	if (typeof(this.result.storage) != "object" || this.result.storage === null)
		this.result.storage = {};
	if (typeof(this.result.storage.fields) != "object")

	for (fieldCode in this.result.storage.fields) {
		var fieldSettings = this.result.storage.fields[fieldCode];
		if (! this.getFieldNode(fieldCode))
			continue;
		if (this.params.collapseFieldsDisallow !== true) {
			if (fieldSettings["show"] == "show") {
				this.showFieldInstantly(fieldCode);
			} else if (fieldSettings["show"] == "hide") {
				this.hideFieldInstantly(fieldCode);
			}
		}
	}
}
uniFilter.saveLocalStorage = function ()
{
	if (typeof(this.result.storage) == "object")
		localStorage.setItem("uni__" + this.params.uiMainClass + "__filter", JSON.stringify(this.result.storage));
}
