var uniFilter = {};

(function ($)
{
	$.fn.uniFilter = function(params, var2, var3, var4, var5)
	{
		return uniLib.initComponentForNode(this, "uniFilter", params, var2, var3, var4, var5);	
	}
}(jQuery));

uniFilter.defaultParams = {
	animationFieldDuration: 180,
};

uniFilter.init = function ()
{
	var wrapNode = this.nodes.wrap;
	
	this.nodes.fields = wrapNode.find(".ui-filter-field");
	
	for (var i = 0; i < this.nodes.fields.length; i++) {
		var fieldNode = this.nodes.fields.eq(i);
		this.changeField({node: fieldNode});
	}
	
	this.nodes.inputs = wrapNode.find("input, select, textarea");
	this.bind(this.nodes.inputs, "change", "changeField");
	
	if (this.params.instantRefresh)
		this.bind(this.nodes.inputs.filter("input[type=radio], input[type=checkbox]"), "change", "refreshAll");
	else
		this.bind(this.nodes.inputs.filter("input[type=radio], input[type=checkbox]"), "change", "refreshResult");
	
	
	
	this.bind(wrapNode.find(".ui-filter-field-js-clear-field, .ui-filter-field-clear-field"), "click", "clearField");
	this.bind(wrapNode.find(".ui-filter-field-js-toggle-field"), "click", "toggleField");
	
	this.bind(wrapNode.find(".ui-filter-result-link"), "click", "refreshAll");
	
	this.nodes.result = wrapNode.find(".ui-filter-result");
	this.bind(wrapNode.find(".ui-filter-result-close"), "click", "hideResult");
	
	
	
}

uniFilter.getFieldNode = function (var1) {
	var fieldNode = false;
	if (typeof(var1) == "string") {
		fieldNode = this.nodes.fields.filter("data-field-code=[\"" + var1 + "\"]");
	} else if (typeof(var1) == "object") {
		if (typeof(var1.node) == "object") { //## is jquery object
			if (var1.node.is(".ui-filter-field"))
				fieldNode = var1.node;
			else
				fieldNode = var1.node.parents(".ui-filter-field").eq(0);
		}
	}
	if (fieldNode && fieldNode.length)
		return fieldNode;
	else
		return false;
}

uniFilter.toggleField = function (settings)
{
	var fieldNode = this.getFieldNode(settings);
	if (! fieldNode)
		return;
	if (fieldNode.hasClass("hiding") || fieldNode.hasClass("showing"))
		return;	
	
	if (fieldNode.is(".hide")) {
		this.showField(settings);
	} else {
		this.hideField(settings);
	}
}

uniFilter.showField = function (settings)
{
	var fieldNode = this.getFieldNode(settings);
	if (! fieldNode)
		return;
	if (fieldNode.hasClass("showing"))
		return;	
	if (true) {
		var mainFieldNode = fieldNode.find(".ui-filter-field-main-col");
		var showDuration = (typeof(settings.showDuration) != "undefined") ? settings.showDuration : this.params.showFieldDuration || this.params.animationFieldDuration || 0;
		uniEffectLib.slideDown(mainFieldNode, {toggledNode: fieldNode, duration: showDuration});
	}
}

uniFilter.hideField = function (settings)
{
	var fieldNode = this.getFieldNode(settings);
	if (! fieldNode)
		return;
	if (fieldNode.hasClass("hiding"))
		return;
	if (true) {
		var mainFieldNode = fieldNode.find(".ui-filter-field-main-col");
		var hideDuration = (typeof(settings.hideDuration) != "undefined") ? settings.hideDuration : this.params.hideFieldDuration || this.params.animationFieldDuration || 0;
		uniEffectLib.slideUp(mainFieldNode, {toggledNode: fieldNode, duration: hideDuration});
	}
}

uniFilter.hideAllFields = function (settings)
{
	for (var i = 0; i < this.nodes.fields.length; i++) {
		var fieldNode = this.nodes.fields.eq(i);
		this.hideField({node: fieldNode, hideFieldDuration: 0});
	}
}

uniFilter.showAllFields = function (settings)
{
	for (var i = 0; i < this.nodes.fields.length; i++) {
		var fieldNode = this.nodes.fields.eq(i);
		this.showField({node: fieldNode, hideFieldDuration: 0});
	}
}

uniFilter.changeField = function (settings)
{
	var fieldNode = this.getFieldNode(settings);
	if (! fieldNode)
		return;
	
	var inputNodes = fieldNode.find("input, select, textarea");
	if (inputNodes.is("input[type=radio]:checked, input[type=checkbox]:checked, input:not([type=hidden]):not([type=radio]):not([type=checkbox]):not([type=button]):not([type=submit])[value]")) {
		fieldNode.addClass("ui-filter-field-state-set");
	} else {
		fieldNode.removeClass("ui-filter-field-state-set");
	}
}


uniFilter.clearField = function (settings)
{
	var fieldNode = this.getFieldNode(settings);
	if (! fieldNode)
		return;
	var inputNodes = fieldNode.find("input, select, textarea");
	for (var i = 0; i < inputNodes.length; i++) {
		var inputNode = inputNodes.eq(i);
		if (inputNode.prop("type").toLowerCase() == "checkbox" || inputNode.prop("type").toLowerCase() == "radio") {
			inputNode.prop("checked", false);
		} else if (inputNode.prop("type").toLowerCase() != "hidden" && inputNode.prop("type").toLowerCase() != "button" && inputNode.prop("type").toLowerCase() != "submit") {
			inputNode.prop("value", false);
		}
	}
	fieldNode.removeClass("ui-filter-field-state-set");
	return false;
}

uniFilter.clearAllFields = function (settings)
{
	for (var i = 0; i < this.nodes.fields.length; i++) {
		var fieldNode = this.nodes.fields.eq(i);
		this.clearField({node: fieldNode});
	}
}	

uniFilter.refreshResult = function (settings)
{
	if (typeof(settings.node) == "object") {
		this.result.currentNodeTarget = "";
		if (settings.node.prop("class"))
			this.result.currentNodeTarget += "." + settings.node.prop("class").split(" ").join(".");
		if (settings.node.prop("name").length)
			this.result.currentNodeTarget += "[name=\"" + settings.node.prop("name") + "\"]";
		if (settings.node.prop("value").length)
			this.result.currentNodeTarget += "[value=\"" + settings.node.prop("value") + "\"]";
	}
	
	this.refreshResultHideEffect();
	
	var commands = [
		{target: ".ui-filter-result", cutTarget: ".ui-filter-result", command: "html"},
	];
	for (var i = 0; i < this.nodes.fields; i++) {
		var fieldNode = this.nodes.fields.eq(i);
		commands.push({target: fieldNode.prop("class").split(" ").join("."), cutTarget: fieldNode.prop("class").split(" ").join("."), command: "html"});
	}
		
	uniAjaxLib.ajax({
		uniObject: this,
		wrapNode: this.nodes.wrap,
		formNode: this.nodes.wrap.is("form") ? this.nodes.wrap : this.nodes.wrap.parents("form").eq(0),
		cut: true,
		commands: commands,
		success1: function (params) {
			params.uniObject.init();
			params.uniObject.showResult();
		},
	});
}

uniFilter.showResult = function (settings)
{
	
	if (this.result.currentNodeTarget) {
		var currentNode = this.nodes.wrap.find(this.result.currentNodeTarget);
		if (currentNode.length) {
			if (! currentNode.is(":visible"))
				currentNode = currentNode.parent();
			
			this.nodes.result.css("transition", "top 0s ease 0s");
			
			setTimeout(function (uniObject) {
				uniObject.nodes.result.css("top", (currentNode.position()["top"] + (currentNode.outerHeight() / 2)) + "px");
				setTimeout(function (uniObject) {
					uniObject.showResultEffect();
				}, 16, uniObject);
			}, 16, this);
			
			
			
		}
	}
}

uniFilter.hideResult = function (settings)
{
	var hideDuration = (typeof(settings.hideDuration) != "undefined") ? settings.hideDuration : this.params.hideResultDuration || this.params.animationResultDuration || 350;
	uniEffectLib.fadeOut(this.nodes.result, {duration: hideDuration});
}

uniFilter.showResultEffect = function ()
{
	var showDuration = 350;
	uniEffectLib.fadeIn(this.nodes.result, {duration: showDuration});	
}






uniFilter.refreshResultHideEffect = function ()
{
	uniEffectLib.fadeOut(this.nodes.result);
}





uniFilter.refreshAll = function (settings)
{
	
	var ajaxWrapNode = this.nodes.wrap.parents("*[data-uni-ajax-wrap]").eq(0);
	if (! ajaxWrapNode.length)
		return;
	uniAjaxLib.ajaxForNode(ajaxWrapNode, {
		formNode: ajaxWrapNode.is("form") ? ajaxWrapNode : (this.nodes.wrap.is("form") ? this.nodes.wrap : this.nodes.wrap.parents("form").eq(0)),
		cut: true,
		updateUrl: true,
	});
}

















var uniList = {};

(function ($)
{
	$.fn.uniList = function(params, var2, var3, var4, var5)
	{
		return uniLib.initComponentForNode(this, "uniList", params, var2, var3, var4, var5);	
	}
}(jQuery));

uniList.init = function ()
{
	var wrapNode = this.nodes.wrap;

	if (typeof(this.params.filterNode) == "object")
		this.nodes.filter = this.params.filterNode;
	else if (typeof(this.params.filterOuterEl) == "string" && this.params.itemsClass.length)
		this.nodes.filter = $(this.params.filterOuterEl);
	else
		this.nodes.filter = {};
	
}

uniList.showPage = function (settings)
{
	
}

uniList.showMore = function (settings)
{
	if (typeof(this.params.itemsClass) != "string" || ! this.params.itemsClass.length) {
		console.log("this.params.itemsClass required");
		return;
	}
	
	uniAjaxLib.ajax({
		wrapNode: this.nodes.wrap,
		formNode: this.nodes.filter,
		cut: true,
		commands: [
			{command: "append", targetNode: this.nodes.wrap.find(this.params.itemsClass)},
			{command: "html", targetNode: this.nodes.wrap.find(".ui-pagination")},
		],
	});
}