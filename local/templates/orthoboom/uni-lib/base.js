

var uniCoreLib = {};

uniCoreLib.init = function ()
{
	/*
	$("body").on("click", "[data-uni-ajax-link], [data-uni-ajax-action]", function () {
		uniAjaxLib.ajax({node: $(this)});
		return false;
	});
	*/
	
	$("body").on("click", "[data-target]", function () {
		uniCoreLib.smartAction({node: $(this), target: $(this).data("target"), targetToggle: $(this).data("toggle")});
	});
	
	uniCoreLib.initNode($("body"));
}

$(document).ready(function() {
	uniCoreLib.init();
});

uniCoreLib.initNode = function (node)
{
	var uniObjectNodes = node.find("[data-uni-component]");
	for (var i = 0; i < uniObjectNodes.length; i++) {
		$[node.data("uni-component")]();
	}
}

uniCoreLib.addComponent = function (componentType)
{
	(function ($)
	{
		$.fn[componentType] = function(params)
		{
			
			if (typeof(params) == "string")
			{
				
				//Call component function
				var agrs = arguments;
				//$(this).each(function () {
					var node = $(this);
					while(true) 
					{
						if (node.data(componentType))
						{
							var uniObject = node.data(componentType);
							return uniObject[params](agrs[1], agrs[2], agrs[3], agrs[4]);
							break;
						}
						if (node.prop('tagName').toLowerCase() == 'body')
							break;
						node = node.parent();
					}
				//});
				
			}
			else
			{
				//If component exists
				if (typeof($(this).data(componentType)) == "object") {
					return $(this).data(componentType);
				}
				
				//Init component
				var node = $(this);
				
				for (var k in uniComponentCore)
					if (node.hasOwnProperty(k) == false)
						node[k] = uniComponentCore[k];	
					
				for (var k in window[componentType])
					if (node.hasOwnProperty(k) == false)
						node[k] = window[componentType][k];		
					
				node.each(function() {
					//node.addClass(componentType + "-binded");

					node.initComponent($(node), componentType, params);
				});	
				
				return node;
			}	
		}
	}(jQuery))
}

uniCoreLib.smartAction = function (params)
{
	var targetNode = params.targetNode || false;
	typeof(! targetNode && typeof(params.target) != "undefined")
		targetNode = $(params.target);

	if (! targetNode || ! targetNode.length)
		return false;

	if (targetNode.length) {
		if (targetNode.is(".ui-modal, [role=dialog]")) {
			//if (! uniLib.isComponent(targetNode, "uniModal"))
			//	targetNode.uniModal({});
			targetNode.uniModal("show");
			return;
		} else if (targetNode.is(".ui-popup, [role=popup]")) {
			//if (! uniLib.isComponent(targetNode, "uniPopup"))
			//	targetNode.uniPopup({});
		
			//alert('Y');
			targetNode.uniPopup("toggle");
			return;
		}
	}
}




var uniComponentCore = {}

uniComponentCore.initComponent = function (node, componentType, params)
{
	this.componentType = componentType;
	this.params = {};
	this.nodes = {};
	this.events = {};
	this.result = {};
	
	//if (typeof(this.defaultParams) == "object")
	//	params = uniAjaxLib.extend(this.defaultParams, params);
	
	if (node instanceof jQuery) {
		this.nodes.wrap = node;
		node.data(componentType, this);
		if (typeof(node.data("uni-params")) != "undefined")
			if (typeof(node.data("uni-params")) == "object")
				params = uniAjaxLib.extend(node.data("uni-params"), params);
			else
				console.log("Error parsing JSON from data-uni-params for " + componentType);
	}
	
	//node.data(this.componentType, this);
	
	if (params)
		this.params = params;
	
	if (typeof this.init == "function")
		this.init();
}

uniComponentCore.getParam = function(key)
{
	if (typeof(this.params[key]) == "undefined")
		return null;
	return this.params[key];
}
uniComponentCore.setParam = function(key, value)
{
	if (key != null && value != null) 
		this.params[key] = value;
}

uniComponentCore.setParams = function(params)
{
	this.params = uniAjaxLib.extend(this.params, params);
}

uniComponentCore.bind = function (nodes, evn, command, data)
{
	//console.log(nodes.filter("." + this.componentType ).length);
	nodes.filter(":not(." + this.componentType + "-binded)").on(evn, {uniComponent: this}, function (e) {
		if (typeof(e.data.uniComponent[command]) != "undefined") {
			var settings = {e: e, node: $(this)};
			if (data)
				settings = uniAjaxLib.extend(param, data);
			return e.data.uniComponent[command](settings);
		}
	});
	setTimeout(function (uniObject) {
		nodes.addClass(uniObject.componentType + "-binded");
	}, 0, this);
}



uniComponentCore.on = function (eventCode, handler) {
	if (typeof(this.events[eventCode]) == "undefined")
		this.events[eventCode] = [];
	this.events[eventCode].push({handler: handler});
}

uniComponentCore.callEvents = function (eventCode)
{
	var events = [];
	
	if (typeof(this.params[eventCode]) != "undefined")
		events.push(this.params[eventCode]);

	for (var i = 0; i < events.length; i++) {
		var eventResult = events[i]();
	}
	
	
	/*
	if (typeof(this.events[eventCode]) != "undefined" && this.events[eventCode].length)
		
	
	for (var i = 0; i < this.events[eventCode].length; i++) {
		if (typeof(this.events[eventCode][i]) == "function")
			this.events[eventCode][i](this);
	}
	*/
}



















uniEffectLib = {};

uniEffectLib.show = function (params) 
{
	if (typeof(params) != "object")
		return;
	
	if (! params.targetNode && typeof(params.target) != "undefined")
		params.targetNode = $(params.target);
	if (! params.targetNode && typeof(params.wrapNode) == "object")
		params.targetNode = params.wrapNode;
	if (! params.targetNode || ! params.targetNode.length)
		return;
	
	var targetNode = params.targetNode;
	
	if (targetNode.hasClass("showing"))
		return;
	if (targetNode.hasClass("hiding")) {
		if (targetNode.data("uni-show-timer"))
			return;
		var showTimer = setTimeout(function (params) {
			uniEffectLib.show(params);
		}, 250, params);
		targetNode.data("uni-show-timer", showTimer);
		return;
	}
	
	if (typeof(params.showEffects) == "undefined")
		params.showEffects = [];	
	if (params.effect == "fade")
		params.showEffects.push({function: uniEffectLib.fadeInEffect});	
	
	params.targetNode.css("transition", "none");
	
	setTimeout(function (params) {
		
		params.showDuration = params.showDuration || params.animationDuration || 350;
		params.showEasing = params.hideEasing || params.animationEasing || "linear"; //"ease-in"
		params.targetNode.css("transition", "all " + (params.showDuration / 1000) + "s " + params.showEasing);
		
		setTimeout(function () {
			params.targetNode.removeClass("hide");
			params.targetNode.addClass("show");
			params.targetNode.addClass("showing");	
			
			if (typeof(params.showEffects) != "undefined")
				for (var i = 0; i < params.showEffects.length; i++) {
					if (typeof(params.showEffects[i]["function"]) == "function")
						params.showEffects[i]["function"](uniAjaxLib.extend(params, params.showEffects[i]));
				}			
			
			//params.targetNode.css("opacity", 1);
			
			setTimeout(function () {
				params.targetNode.removeClass("showing");
			}, params.showDuration, params);
		}, 0, params);
		
	}, 0, params);	
}

uniEffectLib.hide = function (params) 
{
	if (typeof(params) != "object")
		return;
	
	if (typeof(params.targetNode) == "undefined" && typeof(params.target) != "undefined")
		params.targetNode = $(params.target);
	if (typeof(params.targetNode) == "undefined" && typeof(params.wrapNode) == "object")
		params.targetNode = params.wrapNode;
	if (typeof(params.targetNode) == "undefined" || ! params.targetNode.length)
		return;

	if (params.targetNode.hasClass("hiding"))
		return;	
	if (params.targetNode.hasClass("showing")) {
		if (params.targetNode.data("uni-hide-timer"))
			return;
		var hideTimer = setTimeout(function (params) {
			uniEffectLib.show(params);
		}, 250, params);
		params.targetNode.data("uni-hide-timer", hideTimer);
		return;
	}
	
	if (typeof(params.hideEffects) == "undefined")
		params.hideEffects = [];	
	if (params.effect == "fade")
		params.hideEffects.push({function: uniEffectLib.fadeOutEffect});
	
	params.targetNode.css("transition", "none");
	
	setTimeout(function (params) {
		
		params.hideDuration = params.hideDuration || params.animationDuration || 350;
		params.hideEasing = params.hideEasing || params.animationEasing || "linear"; //"ease-in"
		params.targetNode.css("transition", "all " + (params.hideDuration / 1000) + "s " + params.hideEasing);
		
		setTimeout(function (params) {
			params.targetNode.addClass("hide");
			params.targetNode.addClass("hiding");
			params.targetNode.removeClass("show");
			
			if (typeof(params.hideEffects) != "undefined")
				for (var i = 0; i < params.hideEffects.length; i++) {
					if (typeof(params.hideEffects[i]["function"]) == "function")
						params.hideEffects[i]["function"](uniAjaxLib.extend(params, params.hideEffects[i]));
				}
					
				
			
			//params.targetNode.css("opacity", 0);
			
			setTimeout(function () {
				params.targetNode.removeClass("hiding");
				
			}, params.hideDuration, params);
			
		}, 0, params);	

	}, 0, params);			
}

uniEffectLib.fadeInEffect = function (params)
{
	params.targetNode.css("opacity", 1);
}

uniEffectLib.fadeOutEffect = function (params)
{
	params.targetNode.css("opacity", 0);
}









uniEffectLib.fadeIn = function (node, params)
{
	params = params || {};
	params.node = node;
	
	var showDuration = params.showDuration || params.animationDuration || 350;
		
		
		
	params.node.css("transition", "all " + (showDuration / 1000) + "s ease-in");
	
	setTimeout(function () {
		params.node.css("visibility", "visible");
		params.node.css("opacity", "1");	
		
		params.node.removeClass("hide");
		params.node.addClass("show");
		params.node.addClass("showing");		
		
		setTimeout(function () {
			params.node.removeClass("showing");
		}, showDuration, params);
	}, 0, params);
}

uniEffectLib.fadeOut = function (node, params)
{
	params = params || {};
	params.node = node;
	
	var hideDuration = params.showDuration || params.animationDuration || 350;
	params.node.css("transition", "all " + (hideDuration / 1000) + "s ease-in");
	
	setTimeout(function (params) {
		params.node.css("opacity", "0");
		
		if (typeof(params.toggledNode) == "object")
			setTimeout(function () {
				params.toggledNode.addClass("hide");
				params.toggledNode.removeClass("hiding");
				params.toggledNode.removeClass("show");
			}, hideDuration, params);
	}, 0, params);	
	
	
}

uniEffectLib.slideDown = function (node, params)
{
	params = params || {};
	params.node = node;
	
	if (typeof(params.toggledNode) == "object") {
		params.toggledNode.removeClass("hide");
		params.toggledNode.addClass("show");
	}
	
	params.node.css("transition", "none 0s ease");
	params.node.css("height", "auto");
	var setHeight = params.node.outerHeight();
	params.node.css("height", "0px");
	
	setTimeout(function (params) {
		var showDuration = params.showDuration || params.animationDuration || 350;
		params.node.css("transition", "all " + (showDuration / 1000) + "s ease-in");
		params.node.css("height", setHeight + "px");
	}, 0, params);
}

uniEffectLib.slideUp = function (node, params)
{
	params = params || {};
	params.node = node;
	
	if (typeof(params.toggledNode) == "object") {
		params.toggledNode.addClass("hiding");
	}
	var hideDuration = params.hideDuration || params.animationDuration || 350;
	
	params.node.css("height", params.node.outerHeight() + "px");
	params.node.css("transition", "all " + (hideDuration / 1000) + "s ease-in");
	
	setTimeout(function (params) {
		params.node.css("height", "0px");
		
		if (typeof(params.toggledNode) == "object")
			setTimeout(function () {
				params.toggledNode.addClass("hide");
				params.toggledNode.removeClass("hiding");
				params.toggledNode.removeClass("show");
			}, hideDuration, params);
	}, 0, params);
}








var uniToolsLib = {};

uniToolsLib.scrollTo = function (params) 
{
	
	if (params.targetNode)
		var targetNode = params.targetNode;
	else if (params.target)
		var targetNode = $(params.target);
	if (typeof(targetNode) == "undefined" || ! targetNode.length)
		return false;
	
	var offsetY = targetNode.offset().top;

	if (! isNaN(params.moveY))
		offsetY += params.moveY;
		
    $([document.documentElement, document.body]).animate({scrollTop: offsetY}, params.duration || 350);
}

uniToolsLib.currentPopup = false;
uniToolsLib.setCurrentPopup = function (node)
{
	uniToolsLib.currentPopup = node;
}
uniToolsLib.clearCurrentPopup = function (node)
{
	uniToolsLib.currentPopup = false;
}


$(document).ready(function() {
	$("body").on("click", function (e) {
		if (typeof(uniToolsLib.currentPopup) == "object") {
			if (jQuery(e.target).closest(uniToolsLib.currentPopup).length) {
				
			} else {
				uniToolsLib.currentPopup.uniPopup("hide");
			}
		}
	});
});


var uniPopup = {};

uniCoreLib.addComponent("uniPopup");
/* (function ($)
{
	$.fn.uniPopup = function(params, var2, var3, var4, var5)
	{
		return uniLib.initComponentForNode(this, "uniPopup", params, var2, var3, var4, var5);	
	}
}(jQuery));
*/

uniPopup.init = function (wrap)
{
	var wrap = this.nodes.wrap;

	this.nodes.closes = wrap.find(".ui-popup-close, .js-popup-close");
	this.bind(this.nodes.closes, "click", "hide");
}

uniPopup.show = function ()
{
	uniEffectLib.show(uniAjaxLib.extend(this.params, {targetNode: this.nodes.wrap}));
	//if (false)
	//	uniToolsLib.setCurrentPopup(this.nodes.wrap);
	
	//uniEffectLib.fadeIn(this.nodes.wrap, this.params);
}

uniPopup.hide = function ()
{
	uniEffectLib.hide(uniAjaxLib.extend(this.params, {targetNode: this.nodes.wrap}));
	//uniEffectLib.fadeOut(this.nodes.wrap, this.params);
}

uniPopup.toggle = function ()
{
	
	if (this.nodes.wrap.hasClass("hide"))
		this.show();
	else
		this.hide();
}





var uniTabs = {};

uniCoreLib.addComponent("uniTabs");

/*
(function ($)
{
	$.fn.uniTabs = function(params, var2, var3, var4, var5)
	{
		return uniLib.initComponentForNode(this, "uniTabs", params, var2, var3, var4, var5);	
	}
}(jQuery));*/

uniTabs.init = function (wrap)
{
	var wrap = this.nodes.wrap;
		
	if (! this.params.animationDuration)
		this.params.animationDuration = 180;
	this.result.currentTabIndex = false;
	
	if (this.params.tabsTarget)
		this.nodes.tabs = wrap.find(this.params.tabsTarget);
	else
		this.nodes.tabs = wrap.find("*[role=tab]");
	this.bind(this.nodes.tabs, "click", "setTab");
	
	if (this.params.contentsTarget)
		this.nodes.contents = wrap.find(this.params.contentsTarget);
	else
		this.nodes.contents = wrap.find("*[role=tabpanel]");
	
	if (! this.nodes.contents.length)
		return;
	
	for (var i = 0; i < this.nodes.contents.length; i++) {
		var tabNode = this.nodes.tabs.eq(i);
		if (tabNode.prop("href")) {
			var contentNode = $.find(tabNode.prop("href"));
		} else if (tabNode.data("tab-code")) {
			var contentNode = this.nodes.contents.find("*[tab-code=\"" + tabNode.data("tab-code") + "\"]");
		}else {
			var contentNode = this.nodes.contents.eq(i);
		}
		if (contentNode) {
			tabNode.data("content-node", contentNode);
		}
	}
	
	var activeTabIndex = 0;
	for (var i = 0; i < this.nodes.contents.length; i++) {
		var contentNode = this.nodes.contents.eq(i);
		if (i != activeTabIndex)
			contentNode.addClass("hide");
		else
			contentNode.addClass("show");
	}
	
	this.setTab({index: 0, hideDuration: 0, showDuration: 0});
}

uniTabs.setTab = function (settings) {
	if (settings.node) {
		var tabNode = settings.node;
	} else if (typeof(settings.index) != "undefined") {
		var tabNode = this.nodes.tabs.eq(settings.index);
	} else if (typeof(settings.code) != "undefined") {
		var tabNode = this.nodes.tabs.find("*[tab-code=\"" + settings.code + "\"]");
	}
	
	if (! tabNode)
		return;
	if (tabNode.prop("disabled"))
		return;
	
	var contentNode = tabNode.data("content-node");
	if (! contentNode)
		return;
	
	if (this.result.currentTabIndex !== false) {
		this.result.oldTabIndex = this.result.currentTabIndex;
		this.nodes.oldTab = this.nodes.currentTab;
		this.nodes.oldContent = this.nodes.currentContent;
	}
	
	this.result.currentTabIndex = this.nodes.tabs.index(tabNode);
	this.nodes.currentTab = tabNode;
	this.nodes.currentContent = contentNode;

	this.setTabEffect(settings);
}

uniTabs.setTabEffect = function (settings) 
{
	this.nodes.tabs.removeClass("active");
	this.nodes.currentTab.addClass("active");
	
	if (this.nodes.oldContent)
	{
		var hideDuration = (typeof(settings.hideDuration) != "undefined") ? settings.hideDuration : this.params.hideDuration || this.params.animationDuration || 0;
		
		this.nodes.oldContent.css("transistion", "all " + hideDuration + "ms");
		this.nodes.oldContent.css("opacity", 0);
		
		if (parseInt(hideDuration) > 0)
			setTimeout(function (uniObject, settings) {
				uniObject.hideTabEffect(settings);
			}, parseInt(hideDuration), this, settings);
		else
			this.hideTabEffect(settings);
	} else {
		this.showTabEffect(settings);
	}
}

uniTabs.hideTabEffect = function (settings) 
{
	this.nodes.oldContent.css("visibility", "hidden");
	this.nodes.oldContent.css("position", "absolute");
	
	var showDuration = (typeof(settings.showDuration) != "undefined") ? settings.showDuration : this.params.showDuration || this.params.animationDuration || 0;
	
	this.showTabEffect(settings);	
}

uniTabs.showTabEffect = function (settings) 
{
	var showDuration = (typeof(settings.showDuration) != "undefined") ? settings.showDuration : this.params.showDuration || this.params.animationDuration || 0;
	
	this.nodes.currentContent.css("transistion", "all " + showDuration + "ms");
	
	this.nodes.currentContent.css("position", "relative");
	this.nodes.currentContent.css("visibility", "visible");
	this.nodes.currentContent.css("opacity", 1);	
}

///// MODAL /////

var uniModal = {};

uniCoreLib.addComponent("uniModal");

/*
(function ($)
{
	$.fn.uniModal = function(params, var2, var3, var4, var5)
	{
		return uniLib.initComponentForNode(this, "uniModal", params, var2, var3, var4, var5);	
	}
}(jQuery));
*/


uniModal.init = function (wrap)
{
	var wrap = this.nodes.wrap;
	

	this.nodes.overlay = wrap.find(".ui-overlay");
	this.bind(this.nodes.overlay, "click", "hide");
	
	this.nodes.closes = wrap.find(".ui-modal-close");
	this.bind(this.nodes.closes, "click", "hide");
}

uniModal.show = function ()
{
	this.nodes.wrap.removeClass("hide");
	this.nodes.wrap.addClass("show");	
	this.hideEffect();
}

uniModal.hide = function ()
{
	this.nodes.wrap.removeClass("show");
	this.nodes.wrap.addClass("hide");	
	this.showEffect();
}


uniModal.showEffect = function () {

}

uniModal.hideEffect = function () {

}









	/*
	var hideDuration = typeof(settings.hideDuration) != "undefined" ? settings.hideDuration : this.params.hideDuration;
	var showDuration = 0;
	
	if (this.result.activeContentNode) {
		this.callFunctionForNode(this.result.activeContentNode, "hideContentStart", null);
		this.callFunctionForNode(this.result.activeContentNode, "hideContentEnd", null, hideDuration);
	}	
	
	if (contentNode) {
		this.callFunctionForNode(contentNode, "showContentStart", null, hideDuration);
		this.callFunctionForNode(contentNode, "showContentEnd", null, hideDuration + showDuration);
	}
	
uniTabs.showContentStart = function (settings) 
{
	settings.node.css("position", "relative");
	settings.node.css("visibility", "visible");
	settings.node.css("opacity", 1);
}	

uniTabs.showContentEnd = function (settings) 
{
	
}	

uniTabs.hideContentStart = function (settings) 
{
	settings.node.css("opacity", 0);
}	

uniTabs.hideContentEnd = function (settings) 
{
	settings.node.css("visibility", "hidden");
	settings.node.css("position", "absolute");
}		
	
	*/
	//this.nodes.contents.hide();
	//contentNode.show();

/*
uniTabs.hideContentStart = function (settings)
{
	settings.node.addClass("hiding");
}

uniTabs.hideContentEnd = function (settings)
{
	settings.node.removeClass("hiding");
	settings.node.addClass("hide");
	
	settings.node.removeClass("show");
	//settings.node.css("display", "none");
	this.result.activeContentNode = false;
}

uniTabs.showContentStart = function (settings)
{	
	this.result.activeContentNode = settings.node;
	//settings.node.css("display", "block");
	
	settings.node.removeClass("hide");
	settings.node.addClass("show");
	settings.node.addClass("showing");
	
	
	
}

	this.nodes.tabs.removeClass("active");
	settings.node.addClass("active");
	
	var hideDuration = (typeof(settings.hideDuration) != "undefined") ? settings.hideDuration : this.params.hideDuration || this.params.animationDuration || 0;
	
	hideStart();
	setTimeout(function (uniObject) {
		uniObject.hideEnd();
		uniObject.showFinish();
	}, hideDuration, this);

uniTabs.showFinish = function (settings)
{
	var showDuration = (typeof(settings.showDuration) != "undefined") ? settings.showDuration : this.params.showDuration || this.params.animationDuration || 0;
	
	showStart();
	setTimeout(function (uniObject) {
		uniObject.showEnd();
	}, showDuration, this);
}

uniTabs.showStart = function (settings) 
{
	settings.node.css("position", "relative");
	settings.node.css("visibility", "visible");
	settings.node.css("opacity", 1);
}	

uniTabs.showtEnd = function (settings) 
{
	
}	

uniTabs.hideStart = function (settings) 
{
	settings.node.css("opacity", 0);
}	

uniTabs.hideEnd = function (settings) 
{

}	





uniTabs.showContentEnd = function (settings)
{
	settings.node.removeClass("showing");
}
*/


uniTabs.showComplete = function (settings)
{
	
}









var uniFormLib = {};

uniFormLib.validateForm = function (formTag)
{
	var errors = [];
	var formNode = $(formTag);
	var inputNodes = formNode.find("input, select, textarea");
	
	for (var i = 0; i < inputNodes.length; i++) {
		var inputNode = inputNodes.eq(i);
		var errorText = "";
		
		if (typeof(inputNode.data("required")) != "undefined") {
			if (inputNode.prop("type").toLowerCase() == "radio" || inputNode.prop("type").toLowerCase() == "checkbox") {
				if (((typeof(inputNode.prop("name")) == "undefined" || ! inputNode.prop("name").length) && ! inputNode.prop("checked")) || inputNode.prop("name") && ! formNode.find("input[name=\"" + inputNode.prop("name")  + "\"]:checked").length)
				{
					errors.push(uniFormLib.getInputError(inputNode, "required"));
				}
			} else {
				if (typeof(inputNode.prop("value")) == "undefined" || ! inputNode.prop("value").length)
				{
					errors.push(uniFormLib.getInputError(inputNode, "required"));
				}
			}
		}
		
		if (errorText.length)
			errors.push(errorText);
	}
	
	if (errors.length)
	{
		alert(errors[0]);
		return false;
	}	
	return true;
}


uniFormLib.getInputError = function (inputNode, errorType)
{
	var inputTitle = inputNode.data("title");
	if (typeof(inputTitle) == "undefined") {
		
	}
	
	if (typeof(inputTitle) != "undefined")
		var inputTitlePlacer = "\"" + inputTitle + "\" ";
	else 
		var inputTitlePlacer = "";
	
	var errorText = "";
	if (typeof(inputNode.data(errorType + "-error-message")) != "undefined") {
		errorText = inputNode.data(errorType + "-error-message");
	} else if (typeof(inputNode.data("error-message")) != "undefined") {
		errorText = inputNode.data("error-message");
	} else {
		errorText = "Обязательное поле " + inputTitlePlacer + "не заполнено";
	}
	return errorText;
	
}

uniFormLib.clearInputError = function (inputNode)
{
	
}



uniFormLib.submitForm = function (formTag)
{
	if (uniFormLib.validateForm(formTag))
	{
		var formNode = $(formTag);
		
		if (typeof(formNode.data("uni-form-check")) != "undefined")
		{
			if (formNode.find("input[name=form_check]").length)
				formNode.find("input[name=form_check]").prop("value", "ok");
			else	
				formNode.append('<input type="hidden" name="form_check" value="ok" />');
		}		
		
		if (typeof(formNode.data("uni-ajax-wrap")) != "undefined")
		{
			//uniAjaxLib.ajax({
			uniAjaxLib.ajaxForNode(formNode, {
				url: typeof(formNode.prop("action") != "undefined") ? formNode.prop("action") : window.location.href,
				wrapNode: formNode,
				formNode: formNode,
				//method: typeof(formNode.prop("method") != "undefined") && formNode.prop("method").toLowerCase() == "post" ? "post" : "get",
				cut: true,
			});

			return false;
		}

		if (typeof(formNode.data("uni-ajax-form")) != "undefined")
		{
			
		}			
		
		return true;
	}
	
	return false;
}














var uniNumberField = {};

/* (function ($)
{
	$.fn.uniNumberField = function(params, var2, var3, var4, var5)
	{
		return uniLib.initComponentForNode(this, "uniNumberField", params, var2, var3, var4, var5);	
	}
}(jQuery));
*/

uniCoreLib.addComponent("uniNumberField");

uniNumberField.init = function(wrap)
{
	this.params = uniAjaxLib.extend({
		step: 1,
	}, this.params);
	
	var wrap = this.nodes.wrap;
	
	this.nodes.input = wrap.find("input[type=text]");
	this.bind(this.nodes.input, "input paste", "onInputChange");

	this.nodes.minus = wrap.find("*[role=minus], .ui-number-field-minus");
	this.bind(this.nodes.minus, "click", "minus");
	
	this.nodes.minus = wrap.find("*[role=plus], .ui-number-field-plus");
	this.bind(this.nodes.minus, "click", "plus");
	
	this.format();
}

uniNumberField.minus = function(settings)
{
	var step = (settings && typeof(settings.step) != "undefined") ? settings.step : this.params.step;
	this.changeValue(-step);
};

uniNumberField.plus = function(settings)
{
	var step = (settings && typeof(settings.step) != "undefined") ? settings.step : this.params.step;
	this.changeValue(step);
};

uniNumberField.changeValue = function(addValue)
{
	var input = this.nodes.input;
	
	if (input.prop("disabled"))
		return false;
	
	var value = parseFloat(this.nodes.input.val());
	if (isNaN(value))
		return false;

	if (typeof(this.params.minValue) != "undefined")
		if (value + addValue < this.params.minValue)
			return false;
		
	if (typeof(this.params.maxValue) != "undefined")
		if (value + addValue > this.params.maxValue)
			return false;
	
	input.prop("value", value + addValue);
	input.change();
};

uniNumberField.format = function () 
{
	var input = this.nodes.input;
	var value = input.prop("value");
	value = value.replace(",", ".");
	value = value.replace(/[^-0-9.$]/gim,'');
	input.prop("value", value);
}

uniNumberField.onInputChange = function()
{
	this.format();
}

























alertJson = function (obj) {
	var result = JSON.stringify(obj);
	if (typeof(result) != "undefined") // result.length
		alert(result);
	else
		alert(obj);
}

