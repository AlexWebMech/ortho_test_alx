var uniAjaxLib = {};
uniAjaxLib.params = {};
uniAjaxLib.profiles = {};
uniAjaxLib.isBusy = false;
uniAjaxLib.queue = [];

uniAjaxLib.defaultAjaxParams = {
	url: "",
	method: "get",
	data: "",
	//timeout: 15000,
}

uniAjaxLib.errorText = "Произошла ошибка при загрузке данных.";
uniAjaxLib.errorUniModalParams = {
	contentHtml: uniAjaxLib.errorText,
};



uniAjaxLib.init = function () 
{
	$("body").on("click", "*[data-uni-ajax-link]", function () {
		//uniAjaxLib.ajaxForNode($(this).parents("*[data-uni-ajax-wrap]").eq(0));
		uniAjaxLib.ajax({node: $(this)});
		return false;
	});
	
	$("body").on("click", "*[data-uni-ajax-action]", function () {
		//uniAjaxLib.ajaxForNode($(this));

		uniAjaxLib.ajax({node: $(this)});
		return false;
	});	
}


uniAjaxLib.extend = function (){
    for(var i=1; i<arguments.length; i++)
        for(var key in arguments[i])
            if(arguments[i].hasOwnProperty(key))
                arguments[0][key] = arguments[i][key];
    return arguments[0];
}
/*
uniAjaxLib.ajaxForNode = function (node, exparams) 
{
	var params = {};
	
	if (node.data("uni-ajax-profile")) {
		var params = uniAjaxLib.extend(uniAjaxLib.getProfile(node.data("uni-ajax-profile")));
	}
	if (typeof(node.data("uni-ajax-params")) == "object") {
		params = uniAjaxLib.extend(params, node.data("uni-ajax-params"));
	}
	var data = node.data();
	if (data) {
		for (var k in data) {
			if (k.substr(0, 12) == "uniAjaxParam" && k != "uniAjaxParams")
				params[k.substr(12)] = (data[k] != "") ? data[k] : true; //##buggy
		}
	}
	
	params = uniAjaxLib.extend(params, exparams);
		
	if (! $.isEmptyObject(params)) {
		
		//params.cut = true;
		
		//if (node.data("uni-ajax-wrap") || node.data("uni-ajax-form"))
		//	params["wrapNode"] = node; 
		uniAjaxLib.ajax(params);
	}
}
*/

uniAjaxLib.addProfile = function (profileCode, params)
{
	uniAjaxLib.profiles[profileCode] = params;
}

uniAjaxLib.getProfile = function (profileCode) 
{
	if (typeof(uniAjaxLib.profiles[profileCode]) != "undefined") 
		return Object.assign({}, uniAjaxLib.profiles[profileCode]);
	return false;
}

uniAjaxLib.ajax = function (params)
{
	if ((typeof(params) != "object") || $.isEmptyObject(params))
		return console.log("Empty ajax params");
	
	//console.log('!!!'); console.log(params);
	
	
	if (typeof(params.profile) != "undefined") {
		var profileVars = uniAjaxLib.getProfile(params.profile);
		console.log(profileVars);
		if (profileVars)
			params = uniAjaxLib.extend(params, profileVars);
	}

	if (uniAjaxLib.isBusy) {
		if (uniAjaxLib.queue.length) {
			/*console.log(params);
			
			//var paramsAsObj = JSON.stringify(params);
			for (var i in uniAjaxLib.queue) {
				if (JSON.stringify(uniAjaxLib.queue.i) === paramsAsObj)
					alert('n');
			} */
		}

		uniAjaxLib.queue.push(params);
		return false;
	}
	
	//Node
	if (params.node) {
		var node = params.node;
		if (node.data("uni-ajax-profile"))
			var params = uniAjaxLib.extend(uniAjaxLib.getProfile(node.data("uni-ajax-profile")), params);
		if (typeof(node.data("uni-ajax-params")) == "object")
			params = uniAjaxLib.extend(node.data("uni-ajax-params"), params);
		
		if (typeof(params.url) == "undefined")
			if (params.node.prop("data-href"))
				params.url = params.node.prop("href");
			else if (params.node.prop("href"))
				params.url = params.node.prop("href");
			
		if (! params.wrapNode && ! params.wrapTarget) {
			var wrapNode = node;
			while (wrapNode && wrapNode.hasClass("uni-ajax-wrap"))
				wrapNode = node.parent();
			if (wrapNode)
				params.wrapNode = wrapNode;
			if (params.wrapNode) {
				if (node.data("uni-ajax-profile"))
					params = uniAjaxLib.extend(uniAjaxLib.getProfile(node.data("uni-ajax-profile")), params);
				if (typeof(node.data("uni-ajax-params")) == "object")
					params = uniAjaxLib.extend(params, node.data("uni-ajax-params"), params);
			}
		}
	}
	
	//Init params
	if (typeof(params.ajaxParams) != "object")
		params.ajaxParams = Object.assign({}, uniAjaxLib.defaultAjaxParams || {});


	

	params.ajaxParams.url = params.url || window.location.href;
	params.ajaxParams.method = params.method || "get";
	params.ajaxParams.data = params.data || "";

	//Wrap and form
	if (! params.wrapNode && params.wrapTarget)
		params.wrapNode = $(params.wrapTarget);
	if (! params.wrapNode.length) {
		return console.log("Ajax wrap not found"); // uniConsoleLib.log("Ajax wrap not found");
		return console.log(param); // uniConsoleLib.log("Ajax wrap not found");
	}
	
	if (typeof(params.formNode) == "undefined" && params.formTarget)
		params.formNode = $(params.formTarget);
	if (typeof(params.formNode) == "undefined" && params.wrapNode.is("form"))
		params.formNode = params.wrapNode;
	
	//Data
	if (params.formNode) {
		if (! params.ajaxParams.method && params.formNode.eq(0).prop("method"))
			params.ajaxParams.method = params.formNode.eq(0).prop("method");
		params.ajaxParams.data += (params.ajaxParams.data.length ? "&" : "") + params.formNode.serialize();
	}
	if (typeof(params.dataEx) != "undefined")
		params.ajaxParams.data += (params.ajaxParams.data.length ? "&" : "") + params.dataEx;	

	//Commands
	if (typeof(params.commands) == "undefined")
		params.commands = [{targetNode: params.wrapNode, cut: true, command: "html"}];
	
	//Set Url
	if (params.updateUrl)
	{
		var setUrl = params.url;
		history.pushState(null, null, setUrl);
	}
	params.ajaxParams.data += (params.ajaxParams.data.length ? "&" : "") + "uni_ajax=Y";
		
	
	
	//
	/* if (! params.commands) {
		var cutTarget = "";
		if (params.cut) {
			
			if (0) {
				
				//cutTarget777 = params.cutTarget777;
			} else {
				
				if (typeof(params.wrapNode.prop("id")) != "undefined" && params.wrapNode.prop("id").length)
					cutTarget = "#" + params.wrapNode.prop("id");
				else if (typeof(params.wrapNode.prop("class")) != "undefined")
					cutTarget += "." + params.wrapNode.prop("class").split(" ").join(".");
			}
		}
		params.commands = [{cutTarget: cutTarget, command: "html", targetNode: params.wrapNode}];

	}
	*/

		
		

	
	//if (typeof(params.additionalData) != "undefined")
		
		//params.ajaxParams.data += ((params.ajaxParams.data.length) ? "&": "") + params.additionalData;
	
	//params.ajaxParams.data += ((params.ajaxParams.data.length) ? "&" : "") + "ajax=y";
	
	
	//Final
	//params.ajaxParams.params = uniAjaxLib.extend(params);
		
	//params.ajaxParams.params = uniAjaxLib.extend(params);
	
	params.ajaxParams.error = uniAjaxLib.onError;
	params.ajaxParams.success = uniAjaxLib.onResponse;
	
	//Submit
	uniAjaxLib.isBusy = true;
	
	if (typeof(params.wrapNode) == "object")
		params.wrapNode.addClass("ui-ajax-wrap-loading");
	
	
	//params.ajaxParams.params = $.extend(params);
	//params.ajaxParams.params = $.extend(Object.assign({}, params));
	
	console.log("[Uni] Submit ajax query"); console.log(params);
	//alert('w'); return false;
	//$.ajax(params.ajaxParams);
	
	uniAjaxLib.params = params;
	$.ajax(Object.assign({}, params.ajaxParams));
}

uniAjaxLib.onResponse = function (ajaxResponse)
{
	params = Object.assign({}, uniAjaxLib.params);

	//var params = this.params;
	//console.log("[Uni] Submit ajax query GET"); console.log(params);
	var ajaxNode = null;
	var html = null;
	
	for (var k in params.commands) {
		var command = Object.assign({}, params.commands[k]);
		
		if (typeof(command.cutTarget) == "undefined")
			command.cutTarget = "";
		if (! command.targetNode && command.target)
		{
			command.targetNode = params.wrapNode.find(command.target);
			if (! command.targetNode.length)
				command.targetNode = params.wrapNode.filter(command.target);
			
			if (command.cut)
				command.cutTarget = command.target;
		}
		
		//console.log(command);
		if (typeof(command.targetNode) == "undefined" || ! command.targetNode.length) {
			console.log('[Uni] Command target not found in source: ' + command.target);
			continue;
		}
		if (command.cut && ! command.cutTarget && command.targetNode)
			if (typeof(command.targetNode.prop("id")) != "undefined" && command.targetNode.prop("id").length)
				command.cutTarget = "#" + command.targetNode.prop("id");
			else if (typeof(command.targetNode.prop("class")) != "undefined")
				command.cutTarget += "." + command.targetNode.prop("class").split(" ").join(".").replace(".ui-ajax-wrap-loading", "");				
		
		if (command.cutTarget) {
			if (ajaxNode === null)
				ajaxNode = $("<div/>").html(ajaxResponse);
			
			var ajaxCutNode = ajaxNode.find(command.cutTarget);
			
			if (! ajaxCutNode.length) {
				console.log('[Uni] Comnmad target not found in response: ' + command.cutTarget);
				continue;
			}
			html = ajaxCutNode.html();
		} else {
			html = ajaxResponse;
		}

		if (! command.command || command.command == "html")
			command.targetNode.html(html);
		else if (command.command == "append")
			command.targetNode.append(html);
		else if (command.command == "prepend")
			command.targetNode.prepend(html);
		else {
			console.log("[Uni] Unknown ajax command"); //uniConsoleLib.log("Unknown ajax command");
			console.log(command);
			continue;
		}
	}
	
	//alert(ajaxNode.find("script.uni-ajax-script").html());
	
	////DELETE IT!/////
	if (ajaxNode)
		ajaxNode.find("script.uni-ajax-script").each(function () {
			
			$("body").append("<script>" + $(this).html() + "</script>");
		});
		
	//alert(ajaxNode.find("script.uni-ajax-script").length);
	
	if (typeof(params.success1) == "function") {
		
		params.success1(params);
	}
	if (typeof(params.success) == "function") {
		params.success(params);
	}	
	
	if (typeof(params.scrollTo) != "undefined")
		uniToolsLib.scrollTo(params.scrollTo);
	
	uniAjaxLib.finishAjax(params);
}

uniAjaxLib.onError = function (params)
{
	params = this.params || params || {};
	//var params = uniAjaxLib.params;
	
	console.log("[Uni] Ajax error");
	console.log(params);
	
	if (false && typeof(uniModal) != "undefined" && uniAjaxLib.errorUniModalParams) {
		//$.uniModal(uniAjaxLib.errorUniModalParams);
	} else {
		alert(uniAjaxLib.errorText);
	}
	
	//uniAjaxLib.finishAjax();
	
	uniAjaxLib.isBusy = false;
	return false;
}

uniAjaxLib.finishAjax = function (params)
{
	uniAjaxLib.isBusy = false;
	
	if (typeof(params.wrapNode) == "object")
		params.wrapNode.removeClass("ui-ajax-wrap-loading");
	
	if (uniAjaxLib.queue.length) {
		var params = uniAjaxLib.queue[0];
		delete uniAjaxLib.queue.shift();
		uniAjaxLib.ajax(params);
	}
}


uniAjaxLib.initNode = function (node)
{
	node.find("form[data-uni-ajax-wrap], form[data-uni-form]").on("submit", function () {return uniFormLib.submitForm(this);});
}

$(document).ready(function() {
	uniAjaxLib.init();
	uniAjaxLib.initNode($("body"));
});