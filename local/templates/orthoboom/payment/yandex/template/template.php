<?
use Bitrix\Main\Localization\Loc;
\Bitrix\Main\Page\Asset::getInstance()->addCss("/bitrix/themes/.default/sale.css");
Loc::loadMessages(__FILE__);

$sum = roundEx($params['PAYMENT_SHOULD_PAY'], 2);
$sum = number_format($sum, 2, '.', '');
$shopID = htmlspecialcharsbx($params['YANDEX_SHOP_ID']);
$scid = htmlspecialcharsbx($params['YANDEX_SCID']);
$customerNumber = htmlspecialcharsbx($params['PAYMENT_BUYER_ID']);
$orderNumber = htmlspecialcharsbx($params['PAYMENT_ID']);
$paymentType = htmlspecialcharsbx($params['PS_MODE']);
$BX_PAYSYSTEM_CODE = $params['BX_PAYSYSTEM_CODE'];

$contact = "--";
$res = CSaleOrderPropsValue::GetOrderProps($orderNumber);
while ($row = $res->fetch()) {
	if ($row['IS_EMAIL']=='Y' && check_email($row['VALUE'])) $contact = $row['VALUE'];
}
if ($order = CSaleOrder::getById($orderNumber)) {
	if ($user = CUser::GetByID($order['USER_ID'])->fetch()) $contact = $user['EMAIL'];
}

$receipt = array('customerContact' => $contact, 'taxSystem' => 2, 'items' => array());
$arOrder = CSaleOrder::GetByID($orderNumber);
$dbBasket = CSaleBasket::GetList(array("NAME" => "ASC"), array("ORDER_ID" => $orderNumber));
while ($arBasket = $dbBasket->Fetch()) {
	$receipt['items'][] = array(
		'quantity' => $arBasket['QUANTITY'],
		'text' =>  substr($arBasket['NAME'], 0, 128),
		'tax' => 1,
		'price' => array(
			'amount' => number_format($arBasket['PRICE'], 2, '.', ''),
			'currency' => 'RUB'
		),
	);
}
if ($arOrder['PRICE_DELIVERY'] > 0) {
	$receipt['items'][] = array(
		'quantity' => 1,
		'text' => 'Доставка',
		'tax' => 1,
		'price' => array(
			'amount' => number_format($arOrder['PRICE_DELIVERY'], 2, '.', ''),
			'currency' => 'RUB'
		),
	);
}
$eValue = \Bitrix\Main\Web\Json::encode($receipt, $options = null);
$eValue = preg_replace_callback('/\\\\u(\w{4})/', function ($matches) {
	return html_entity_decode('&#x' . $matches[1] . ';', ENT_COMPAT, 'Windows-1251');
}, $eValue);
?>

<div class="sale-paysystem-wrapper">
	<span class="tablebodytext">
		<?=Loc::getMessage('SALE_HANDLERS_PAY_SYSTEM_YANDEX_DESCRIPTION')." ".SaleFormatCurrency($params['PAYMENT_SHOULD_PAY'], $payment->getField('CURRENCY'));?>
	</span>
	<form name="ShopForm" action="<?=$params['URL'];?>" method="post">
		<input name="ShopID" value="<?=$shopID?>" type="hidden">
		<input name="scid" value="<?=$scid?>" type="hidden">
		<input name="customerNumber" value="<?=$customerNumber?>" type="hidden">
		<input name="orderNumber" value="<?=$orderNumber?>" type="hidden">
		<input name="Sum" value="<?=$sum?>" type="hidden">
		<input name="paymentType" value="<?=$paymentType?>" type="hidden">
		<input name="cms_name" value="1C-Bitrix" type="hidden">
		<input name="BX_HANDLER" value="YANDEX" type="hidden">
		<input name="BX_PAYSYSTEM_CODE" value="<?=$BX_PAYSYSTEM_CODE?>" type="hidden">
		<input name="ym_merchant_receipt" value='<?=$eValue?>' type="hidden">
		<div class="sale-paysystem-yandex-button-container">
			<span class="sale-paysystem-yandex-button">
				<input class="sale-paysystem-yandex-button-item" name="BuyButton" value="<?=Loc::getMessage('SALE_HANDLERS_PAY_SYSTEM_YANDEX_BUTTON_PAID')?>" type="submit">
			</span><!--sale-paysystem-yandex-button-->
			<span class="sale-paysystem-yandex-button-descrition"><?=Loc::getMessage('SALE_HANDLERS_PAY_SYSTEM_YANDEX_REDIRECT_MESS');?></span><!--sale-paysystem-yandex-button-descrition-->
		</div><!--sale-paysystem-yandex-button-container-->

		<p>
			<span class="tablebodytext sale-paysystem-description"><?=Loc::getMessage('SALE_HANDLERS_PAY_SYSTEM_YANDEX_WARNING_RETURN');?></span>
		</p>
	</form>
</div><!--sale-paysystem-wrapper-->