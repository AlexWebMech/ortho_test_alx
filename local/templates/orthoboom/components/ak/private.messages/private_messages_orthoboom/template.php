<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $USER;
$USER_ID = $USER->GetID();
$USER_IS_MANAGER = in_array(7, $USER->GetUserGroupArray());
?>
						<a name="chat_anchor"></a>
						<?if ($USER_IS_MANAGER):?>
							<?if (!$arResult['SIDE2']):?>
							<h2>Ваши клиенты</h2>
							<?endif;?>
						<a href="#" id="show_list">&#9650;</a>
						<div class="clearfix"></div>
						<ul class="chat-user-list">
						<?
						foreach($arResult['USER_LIST'] as $userId) :
							$arUser = CUser::GetByID($userId)->Fetch();
							$userId == $_REQUEST['user_id_to'] ? $active = 'active' : $active = '';
						?>
							<li class="<?echo $active?>">
								<a href="/personal/chat/?user_id_to=<? echo $userId?>"><? echo $arUser['LAST_NAME']?> <? echo $arUser['NAME']?></a>
								<?if (in_array($userId, $arResult['USER_LIST_NEW_MSG'])):?>
								<sup style="color: #fff; background: #DD3942; border-radius: 7px; padding: 1px 5px; font-weight: normal; font-size: 12px;">new</sup>
								<?endif;?>
							</li>
	
						<?endforeach;?>
						</ul>
						<?endif;?>
						<div class="clearfix"></div>
						<?if ($arResult['SIDE2']):?>
						<div class="tab-content">
							<div class="tab-pane  active" id="tab_chat_with_<?echo $arResult['SIDE2']?>">
								<div class="chat-area">
									<div class="scrollbar-inner">
										
										<?
										foreach($arResult['MESSAGE_LIST'] as $msg):
											if (strpos($msg['FILE']['CONTENT_TYPE'], 'image/') !== false) {
												$src = $msg['FILE']['SRC'];
												$style = 'max-width:350px; max-height:350px;';
											} else {
												$src = '/bitrix/templates/' . SITE_TEMPLATE_ID . '/components/ak/private.messages/private_messages_orthoboom/file.png';
												$style = 'height:50px;';
											}
										?>
										<?if($msg['CREATED_BY'] == $USER_ID): /* сообщение создано первой стороной */?>
										<div class="question">
											<?echo $msg['PREVIEW_TEXT']?>
											<?if ($msg['FILE']):?>
											<div style="margin-top:5px;">
												<a href="<?echo $msg['FILE']['SRC']?>" target="_blank"><img src="<?=$src?>" style="<?=$style?>" title="Нажмите, чтобы загрузить файл" /></a>
											</div>
											<?endif;?>
											<p class="message-date"><?echo $msg['DATE']?> <?echo $msg['TIME']?></p> 
										</div>
										<?else: /* сообщение создано второй стороной */?>
										<div class="answer">
											<div class="consultant-img">
												<img src="/bitrix/templates/<?=SITE_TEMPLATE_ID?>/components/ak/private.messages/private_messages_orthoboom/round.png">
												<?
												if (CUser::IsOnLine($msg['CREATED_BY'])) {
													$active = 'active';
													$status = 'В сети';
												} else {
													$active = '';
													$status = 'Не в сети';
												}
												?>
												<div class="online-status <?echo $active?>">
													<ul>
														<li><?echo $status?></li>
													</ul>
												</div>
											</div>
											<div class="answer-block">
												<?$arUser = CUser::GetByID($msg['CREATED_BY'])->Fetch();?>
												<p class="answer-name"><?echo $arUser['NAME'] . ' ' . $arUser['LAST_NAME']?></p>
												<div class="answer-text">
													<?echo $msg['PREVIEW_TEXT']?>
													<?if ($msg['FILE']):?>
													<div style="margin-top:5px;">
														<a href="<?echo $msg['FILE']['SRC']?>" target="_blank"><img src="<?=$src?>" style="<?=$style?>" title="Нажмите, чтобы загрузить файл" /></a>
													</div>
													<?endif;?>
													<p class="message-date"><?echo $msg['DATE']?> <?echo $msg['TIME']?></p>														
												</div>
											</div>
										</div>
										<?endif;?>
										<?endforeach;?>
										<div class="clearfix"></div>
									</div>
								</div>	
									<div class="message-title">Ваше сообщение:</div>
									<form id="form_chat_with_<?echo $arResult['SIDE2']?>" method="post" action="#chat_anchor" enctype="multipart/form-data">
									<input type="hidden" name="user_id_to" value="<?echo $arResult['SIDE2']?>">
									<div class="add-message">
										<div class="input-file"><input type="file" name="userfile"></div>									
										<textarea class="scrollbar-inner" name="message"></textarea>
									</div>
									<div class="attachment" style="display: none">
										<span class="filename"></span>
										<span class="remove-file" style="font-weight: bold; cursor: pointer;">&times;</span>
									</div>
									<div class="clearfix"></div>
									<div class="green-buttons">
										<a href="#" onclick="PrintChat();return false;">Распечатать переписку</a> 
									</div>									
									<div class="blue-buttons send-message">
										<a href="#" onclick="ValidateSend();return false;">Отправить</a> 
									</div>
									</form>
									<div class="clearfix"></div>
							</div>
							<!--
							<div class="tab-pane" id="two">
									<div class="chat-area">
									
									</div>
									<div class="message-title">Ваше сообщение:</div>
									<div class="add-message">
										<form>
											<div class="input-file"><input type="file"></div>									
											<textarea class="scrollbar-inner"></textarea>
										</form>
									</div>
									<div class="clearfix"></div>
									<div class="green-buttons">
										<a href="">Распечатать переписку</a> 
									</div>									
									<div class="blue-buttons send-message">
										<a href="">Отправить</a> 
									</div>
									<div class="clearfix"></div>																
							</div>							
							-->
						</div>
						<?endif;?>
<script>
	var frm = $('#form_chat_with_<?echo $arResult['SIDE2']?>');
	var tarea = frm.find('textarea');
	var attachdiv = frm.find('.attachment');
	var tab = $('#tab_chat_with_<?echo $arResult['SIDE2']?>'); // когда будет не одна вкладка

	$(document).ready(function(){
		tab.find('.scrollbar-inner').scrollTop(9999);
		
		frm.find('input[type=file]').on('change', function() {
			if ($(this)[0].files[0] == undefined) attachdiv.hide();
			
			var size = $(this)[0].files[0].size;
			if (size > 5242880) {
				alert('Файл должен быть не более 5 Мб.');
				$(this).wrap('<form>').closest('form').get(0).reset();
				$(this).unwrap();
				attachdiv.hide();
			} else {
				var sizeKB = ($(this)[0].files[0].size / 1024).toFixed(2);
				attachdiv.find('.filename').html('<b>Файл:</b> ' + $(this)[0].files[0].name + ' (' + sizeKB + 'KB)');
				attachdiv.show();
			}
		});
		
		frm.find('.attachment .remove-file').on('click', function() {
			var inputFile = frm.find('input[type=file]');
			var attachdiv = frm.find('.attachment');
			inputFile.wrap('<form>').closest('form').get(0).reset();
			inputFile.unwrap();
			attachdiv.find('.filename').html('');
			attachdiv.hide();
		});
		
		$('#show_list').on('click', function() {
			var chatUL = $('.chat-user-list');
			chatUL.slideToggle(200, function() {
				if (chatUL.css('display') == 'none') $('#show_list').html('&#9660;'); else $('#show_list').html('&#9650;');
				set_cookie('userlist', chatUL.css('display'), 31536000000);
			});
			return false;
		});
		
		<?if ($arResult['SIDE2']):?>
		if (get_cookie('userlist') == 'none') $('#show_list').click();
		<?endif;?>

	});
	
	function ValidateSend() { 
		if(tarea.val().trim() != '' || attachdiv.is(':visible')) {
			frm.find('.send-message a').hide();
			frm.submit();
		}
	}
	
	function PrintChat() { 
		var w = window.open('', '', 'width=800,height=600,scrollbars=yes,location=no,directories=no,status=no');
		var t = $('.tab-content').clone();
		t.find('.consultant-img').remove();
		t.find('.message-title').remove();
		t.find('form').remove();
		//t.find('.answer-name').css('font-weight', 'bold');
		t.find('.message-date').css('font-size', '10px');
		w.document.write('<!DOCTYPE html>\n\
							<head>\n\
							<title>Диалог с менеджером</title>\n\
							</head>\n\
							<body>\n');
		w.document.write(t.html());
		w.document.write('<body>\n\
						</html>\n');
		w.print();
	} 
</script>