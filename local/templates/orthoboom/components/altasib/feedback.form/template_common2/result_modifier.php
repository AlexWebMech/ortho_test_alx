<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach($arResult['FIELDS'] as &$f) { //print_r($f);
	if (strpos($f['CODE'], 'FIO_FID') !== false) $f['NAME'] = FORM_NAME_LABEL . ':';
	if (strpos($f['CODE'], 'TEL_FID') !== false) $f['NAME'] = FORM_PHONE_LABEL . ':';
	if (strpos($f['CODE'], 'EMAIL_FID') !== false) $f['NAME'] = FORM_EMAIL_LABEL . ':';
	if (strpos($f['CODE'], 'TIME_FID') !== false) $f['NAME'] = FORM_APTMNT_LABEL . ':';
	
}
unset($f);

//print_r($arResult['FORM_ERRORS']);
if (isset($arResult['FORM_ERRORS'])) {
	if (array_key_exists('EMPTY_FIELD', $arResult['FORM_ERRORS'])) {
		if (isset($arResult['FORM_ERRORS']['EMPTY_FIELD']['ALL_EMPTY'])) $arResult['FORM_ERRORS']['EMPTY_FIELD']['ALL_EMPTY'] = FORM_ERROR_ALL_EMPTY;
		if (isset($arResult['FORM_ERRORS']['EMPTY_FIELD']['TEL_FID11'])) $arResult['FORM_ERRORS']['EMPTY_FIELD']['TEL_FID11'] = FORM_ERROR_PHONE_EMPTY;
	}
	if (array_key_exists('CAPTCHA_WORD', $arResult['FORM_ERRORS'])) {
		if (isset($arResult['FORM_ERRORS']['CAPTCHA_WORD']['ALX_CP_WRONG_CAPTCHA'])) $arResult['FORM_ERRORS']['CAPTCHA_WORD']['ALX_CP_WRONG_CAPTCHA'] = FORM_ERROR_WRONG_CAPTCHA;
	}
}

?>