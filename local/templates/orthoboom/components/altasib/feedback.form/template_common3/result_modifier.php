<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach($arResult['FIELDS'] as &$f) { //print_r($f);
	if (strpos($f['CODE'], 'FIO_FID5') !== false) $f['NAME'] = FORM_NAME_LABEL . ':';
	if (strpos($f['CODE'], 'TEL_FID5') !== false) $f['NAME'] = FORM_PHONE_LABEL . ':';
	if (strpos($f['CODE'], 'ADDRESS_FID5') !== false) $f['NAME'] = FORM_DEST_LABEL . ':';
	if (strpos($f['CODE'], 'ARTICUL_FID5') !== false) $f['NAME'] = FORM_ARTICLE_LABEL . ':';
	if (strpos($f['CODE'], 'COLOR_FID5') !== false) $f['NAME'] = FORM_COLOR_LABEL . ':';
	if (strpos($f['CODE'], 'COUNT_FID5') !== false) $f['NAME'] = FORM_AMOUNT_LABEL . ':';
	if (strpos($f['CODE'], 'SIZE_FID5') !== false) $f['NAME'] = FORM_SIZE_LABEL . ':';
}
unset($f);

if (isset($arResult['FORM_ERRORS'])) {
	if (array_key_exists('EMPTY_FIELD', $arResult['FORM_ERRORS'])) {
		if (isset($arResult['FORM_ERRORS']['EMPTY_FIELD']['ALL_EMPTY'])) $arResult['FORM_ERRORS']['EMPTY_FIELD']['ALL_EMPTY'] = FORM_ERROR_ALL_EMPTY;
		if (isset($arResult['FORM_ERRORS']['EMPTY_FIELD']['TEL_FID5'])) $arResult['FORM_ERRORS']['EMPTY_FIELD']['TEL_FID5'] = FORM_ERROR_PHONE_EMPTY;
	}
	if (array_key_exists('CAPTCHA_WORD', $arResult['FORM_ERRORS'])) {
		if (isset($arResult['FORM_ERRORS']['CAPTCHA_WORD']['ALX_CP_WRONG_CAPTCHA'])) $arResult['FORM_ERRORS']['CAPTCHA_WORD']['ALX_CP_WRONG_CAPTCHA'] = FORM_ERROR_WRONG_CAPTCHA;
	}
}
//print_r($arResult['FORM_ERRORS']);

?>