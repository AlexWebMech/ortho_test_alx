<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/props_format.php");
?>
    <div style="display: none;">
        <h4><?= GetMessage("SOA_TEMPL_PROP_INFO") ?></h4>
        <?
        $bHideProps = true;

        if (is_array($arResult["ORDER_PROP"]["USER_PROFILES"]) && !empty($arResult["ORDER_PROP"]["USER_PROFILES"])):
            if ($arParams["ALLOW_NEW_PROFILE"] == "Y"):
                ?>
                <div class="bx_block r1x3">
                    <?=GetMessage("SOA_TEMPL_PROP_CHOOSE")?>
                </div>
                <div class="bx_block r3x1">
                    <select name="PROFILE_ID" id="ID_PROFILE_ID" onChange="SetContact(this.value)">
                        <option value="0"><?= GetMessage("SOA_TEMPL_PROP_NEW_PROFILE") ?></option>
                        <?
                        foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles) {
                            ?>
                            <option value="<?= $arUserProfiles["ID"] ?>"<? if ($arUserProfiles["CHECKED"] == "Y") echo " selected"; ?>><?= $arUserProfiles["NAME"] ?></option>
                            <?
                        }
                        ?>
                    </select>
                    <div style="clear: both;"></div>
                </div>
            <?
            else:
                ?>
                <div class="bx_block r1x3">
                    <?=GetMessage("SOA_TEMPL_EXISTING_PROFILE")?>
                </div>
                <div class="bx_block r3x1">
                    <?
                    if (count($arResult["ORDER_PROP"]["USER_PROFILES"]) == 1) {
                        foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles) {
                            echo "<strong>" . $arUserProfiles["NAME"] . "</strong>";
                            ?>
                            <input type="hidden" name="PROFILE_ID" id="ID_PROFILE_ID"
                                   value="<?= $arUserProfiles["ID"] ?>"/>
                            <?
                        }
                    } else {
                        ?>
                        <select name="PROFILE_ID" id="ID_PROFILE_ID" onChange="SetContact(this.value)">
                            <?
                            foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles) {
                                ?>
                                <option value="<?= $arUserProfiles["ID"] ?>"<? if ($arUserProfiles["CHECKED"] == "Y") echo " selected"; ?>><?= $arUserProfiles["NAME"] ?></option>
                                <?
                            }
                            ?>
                        </select>
                        <?
                    }
                    ?>
                    <div style="clear: both;"></div>
                </div>
            <?
            endif;
        else:
            $bHideProps = false;
        endif;
        ?>
    </div>


    <div>
        <div id="sale_order_props">
            <?
            if (isset($arResult["ORDER_PROP"]["USER_PROPS_N"][8])) {
                foreach ($arResult["ORDER_PROP"]["USER_PROPS_N"][8]["VARIANTS"] as $arVariant) {
                    if ($arVariant["VALUE"] != "d1" && $arVariant["SELECTED"] == "Y") {
                        $arResult["ORDER_PROP"]["USER_PROPS_Y"][9]["REQUIRED"] = "Y";
                        $arResult["ORDER_PROP"]["USER_PROPS_Y"][9]["~REQUIRED"] = "Y";
                        $arResult["ORDER_PROP"]["USER_PROPS_Y"][9]["REQUIED_FORMATED"] = "Y";
                        break;
                    }
                }
            }


            $arUserProps = array_merge($arResult["ORDER_PROP"]["USER_PROPS_N"], $arResult["ORDER_PROP"]["USER_PROPS_Y"]);
            $arUserPropsN = $arUserPropsY = array();

            if ($USER->IsAuthorized()) {
                $rsUser = CUser::GetByID($USER->GetID());
                $arUser = $rsUser->Fetch();
            }

            $_SESSION["ORDER_DATA"] = array();

            foreach ($arUserProps as $arProp) {
                if ($arProp["REQUIRED"] == "Y" && ($arProp["TYPE"] == "TEXT" || $arProp["TYPE"] == "TEXTAREA")) {
                    $_SESSION["ORDER_DATA"][] = array(
                        "NAME" => $arProp["NAME"],
                        "VALUE" => $arProp["VALUE"]
                    );
                }
            }

            if (getPersonType() == "U") {

                $arUserPropsZ = array();

                foreach ($arUserProps as $kUP => $arUP) {

                    if ($USER->IsAuthorized()) {
                        if ($arUP["ID"] == 15 && empty($arUP["VALUE"])) { // Наименование фирмы
                            $arUP["VALUE"] = $arResult["ORGANIZATION_DATA"]["NAME"];
                        } elseif ($arUP["ID"] == 16 && empty($arUP["VALUE"])) { // Юр адрес
                            $arUP["VALUE"] = $arResult["ORGANIZATION_DATA"]["PROPERTY_LEGAL_ADDRESS_VALUE"];
                        } elseif ($arUP["ID"] == 17 && empty($arUP["VALUE"])) { // ОГРН
                            $arUP["VALUE"] = $arResult["ORGANIZATION_DATA"]["PROPERTY_OGRN_VALUE"];
                        } elseif ($arUP["ID"] == 18 && empty($arUP["VALUE"])) { // ИНН
                            $arUP["VALUE"] = $arResult["ORGANIZATION_DATA"]["PROPERTY_INN_VALUE"];
                        } elseif ($arUP["ID"] == 19 && empty($arUP["VALUE"])) { // КПП
                            $arUP["VALUE"] = $arResult["ORGANIZATION_DATA"]["PROPERTY_KPP_VALUE"];
                        } elseif ($arUP["ID"] == 20 && empty($arUP["VALUE"])) { // Наименование банка
                            $arUP["VALUE"] = $arResult["ORGANIZATION_DATA"]["PROPERTY_BANK_NAME_VALUE"];
                        } elseif ($arUP["ID"] == 21 && empty($arUP["VALUE"])) { // Расч. счет
                            $arUP["VALUE"] = $arResult["ORGANIZATION_DATA"]["PROPERTY_RASCH_SCHET_VALUE"];
                        } elseif ($arUP["ID"] == 22 && empty($arUP["VALUE"])) { // Адрес банка
                            $arUP["VALUE"] = $arResult["ORGANIZATION_DATA"]["PROPERTY_BANK_ADDRESS_VALUE"];
                        } elseif ($arUP["ID"] == 23 && empty($arUP["VALUE"])) { // БИК банка
                            $arUP["VALUE"] = $arResult["ORGANIZATION_DATA"]["PROPERTY_BANK_BIK_VALUE"];
                        } elseif ($arUP["ID"] == 24 && empty($arUP["VALUE"])) { // Корр счет банка
                            $arUP["VALUE"] = $arResult["ORGANIZATION_DATA"]["PROPERTY_KORR_SCHET_VALUE"];
                        } elseif ($arUP["ID"] == 12 && empty($arUP["VALUE"])) { // Корр счет банка
                            $arUP["VALUE"] = $arResult["ORGANIZATION_DATA"]["NAME"];
                        } elseif ($arUP["ID"] == 27 && empty($arUP["VALUE"])) { // Город
                            $arUP["VALUE"] = $arResult["ORGANIZATION_DATA"]["PROPERTY_CITY_VALUE"];
                        } elseif ($arUP["ID"] == 28 && empty($arUP["VALUE"])) { // Факт. адрес
                            $arUP["VALUE"] = $arResult["ORGANIZATION_DATA"]["PROPERTY_ACTUAL_ADDRESS_VALUE"];
                        }
                    }


                    if ($arUP["PROPS_GROUP_ID"] == 3) {
                        $arUserPropsN[$arUP["ID"]] = $arUP;
                    } else if ($arUP["PROPS_GROUP_ID"] == 5) {
                        $arUserPropsZ[$arUP["ID"]] = $arUP;
                    } else {
                        $arUserPropsY[$arUP["ID"]] = $arUP;
                    }
                } ?>

                <h2>
                    <?= GetMessage("SOA_TEMPL_BUYER_ORG_INFO") ?>
                </h2>
                <?
                PrintPropsForm($arUserPropsN, $arParams["TEMPLATE_LOCATION"]);
                ?>
                <h2>
                    <?= GetMessage("SOA_TEMPL_DELIVERY_INFO") ?>
                </h2>
                <div class="organization-info">
                    <?
                    PrintPropsForm($arUserPropsZ, $arParams["TEMPLATE_LOCATION"]); ?></div>
                <h2>
                    <?= GetMessage("PAY_SYSTEM") ?>
                </h2>
                <?
                PrintPropsForm($arUserPropsY, $arParams["TEMPLATE_LOCATION"]);

            } else {

                foreach ($arUserProps as $kUP => $arUP) {

                    if ($USER->IsAuthorized() && $_SERVER["REQUEST_METHOD"] != "POST") {
                        if ($arUP["ID"] == 9 && empty($arUP["VALUE"])) { // Адрес доставки
                            $arUP["VALUE"] = $arUser["PERSONAL_STREET"];
                        } elseif ($arUP["ID"] == 6 && empty($arUP["VALUE"])) { // Город
                            $arUP["VALUE"] = $arUser["PERSONAL_CITY"];
                        } elseif ($arUP["ID"] == 5 && empty($arUP["VALUE"])) { // Индекс
                            $arUP["VALUE"] = $arUser["PERSONAL_ZIP"];
                        }
                    }

                    if ($arUP["PROPS_GROUP_ID"] == 1) {
                        $arUserPropsN[$arUP["ID"]] = $arUP;
                    } else {
                        $arUserPropsY[$arUP["ID"]] = $arUP;
                    }
                }


                ?>
                <h2>
                    <?= GetMessage("SOA_TEMPL_BUYER_INFO") ?>
                </h2>
                <?
                PrintPropsForm($arUserPropsN, $arParams["TEMPLATE_LOCATION"]);
                ?>
                <h2>
                    <?= GetMessage("SOA_TEMPL_DELIVERY_INFO") ?>
                </h2>
                <?
                PrintPropsForm($arUserPropsY, $arParams["TEMPLATE_LOCATION"]);

            } ?>


        </div>
    </div>


    <script type="text/javascript">
        function fGetBuyerProps(el) {
            var show = '<?=GetMessageJS('SOA_TEMPL_BUYER_SHOW')?>';
            var hide = '<?=GetMessageJS('SOA_TEMPL_BUYER_HIDE')?>';
            var status = BX('sale_order_props').style.display;
            var startVal = 0;
            var startHeight = 0;
            var endVal = 0;
            var endHeight = 0;
            var pFormCont = BX('sale_order_props');
            pFormCont.style.display = "block";
            pFormCont.style.overflow = "hidden";
            pFormCont.style.height = 0;
            var display = "";

            if (status == 'none') {
                el.text = '<?=GetMessageJS('SOA_TEMPL_BUYER_HIDE');?>';

                startVal = 0;
                startHeight = 0;
                endVal = 100;
                endHeight = pFormCont.scrollHeight;
                display = 'block';
                BX('showProps').value = "Y";
                el.innerHTML = hide;
            } else {
                el.text = '<?=GetMessageJS('SOA_TEMPL_BUYER_SHOW');?>';

                startVal = 100;
                startHeight = pFormCont.scrollHeight;
                endVal = 0;
                endHeight = 0;
                display = 'none';
                BX('showProps').value = "N";
                pFormCont.style.height = startHeight + 'px';
                el.innerHTML = show;
            }

            (new BX.easing({
                duration: 700,
                start: {opacity: startVal, height: startHeight},
                finish: {opacity: endVal, height: endHeight},
                transition: BX.easing.makeEaseOut(BX.easing.transitions.quart),
                step: function (state) {
                    pFormCont.style.height = state.height + "px";
                    pFormCont.style.opacity = state.opacity / 100;
                },
                complete: function () {
                    BX('sale_order_props').style.display = display;
                    BX('sale_order_props').style.height = '';

                    pFormCont.style.overflow = "visible";
                }
            })).animate();
        }
    </script>

<? if (!CSaleLocation::isLocationProEnabled()): ?>
    <div style="display:none;">

        <?$APPLICATION->IncludeComponent(
        "bitrix:sale.ajax.locations",
        $arParams["TEMPLATE_LOCATION"],
        array(
        "AJAX_CALL" => "N",
        "COUNTRY_INPUT_NAME" => "COUNTRY_tmp",
        "REGION_INPUT_NAME" => "REGION_tmp",
        "CITY_INPUT_NAME" => "tmp",
        "CITY_OUT_LOCATION" => "Y",
        "LOCATION_VALUE" => "",
        "ONCITYCHANGE" => "submitForm()",
        ),
        null,
        array('HIDE_ICONS' => 'Y')
        );?>

    </div>
<? endif ?>