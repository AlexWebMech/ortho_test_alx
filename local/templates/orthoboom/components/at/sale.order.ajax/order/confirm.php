<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult["ORDER"]))
{


	?>


<?
if($_SESSION['GA_ON'] == true): // метка в сессии, добавляем данные в dataLayer google tag manager
?>
	<script>
	window.dataLayer = window.dataLayer || [];
	dataLayer.push({
	 'ecommerce': {
	   'currencyCode': 'RUB',
	   'purchase': {
		 'actionField': {
		   'id': '<?=$arResult['ORDER']['ID']?>',
		   'affiliation': 'ORTHOBOOM',
		 },
		 'products': [
			<?
			//$arItems = array();
			//$arIds = array();
			$basItems = CSaleBasket::GetList(array(), array('ORDER_ID' => $arResult["ORDER"]['ID'])); // достаем информацию о товарах в корзине
			while($basItem = $basItems->Fetch()):?>
			{
			   'name': '<?=str_replace("'", '"', $basItem['NAME'])?>',
			   'id': '<?=$basItem['PRODUCT_ID']?>',
			   'price': <?=$basItem['PRICE']?>,
			   'quantity': <?=$basItem['QUANTITY']?>
			},
			<?endwhile;?>
		  ]
	   }
	 },
	 'event': 'gtm-ee-event',
	 'gtm-ee-event-category': 'Enhanced Ecommerce',
	 'gtm-ee-event-action': 'Purchase',
	 'gtm-ee-event-non-interaction': 'False',
	});
	</script>
<?endif;
unset($_SESSION['GA_ON']); // удаляем метку разрешения отсылки транзакции, чтобы не было дублей
?>



	<?if (getPersonType() == "U"):
		$arOrderProps = array();
		$userOrder = "";
		$dbOrderProps = CSaleOrderPropsValue::GetList(
	        array("SORT" => "ASC"),
	        array("ORDER_ID" => $arResult["ORDER"]["ACCOUNT_NUMBER"])
	    );
	    while ($arOrderProp = $dbOrderProps->GetNext()):
	        $arOrderProps[] = $arOrderProp;
	    	if ($arOrderProp["~ORDER_PROPS_ID"] == 10 && $arOrderProp["ORDER_PROPS_ID"] == 10) {
	    		$userOrder = $arOrderProp["VALUE"];
	    	}
	    	//echo '<pre>'; print_r($arOrderProp); echo '</pre>';
	    endwhile;
		?>
		<div style="border: 1px solid #fff; background: #fff; width:735px; margin: 0 auto;">

			<table style="width:735px; margin: 0 auto;" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td style="padding-right:30px; color: #344772; font-size:26px; line-height:1.5; padding:20px 0 20px 30px; ">
						<?if (!empty($userOrder)):?>
							Здравствуйте, <?=$userOrder?>! <br>
						<?endif;?>
						Ваш заказ №<?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?> принят в обработку!</td>
					</tr>
					<tr>
						<td colspan="2" style="background: #DDEDC9; padding:20px 30px 15px;">
							<p>В ближайшее время с Вами свяжется наш менеджер.</p>
							<p>Узнать о состоянии заказа Вы можете:</p>
							<p>— по ссылке <a style="color: #424242; text-decoration: underline" target="_blank" href="http://<?=$_SERVER["SERVER_NAME"]?>/personal/order/?ID=<?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?>">http://<?=$_SERVER["SERVER_NAME"]?>/personal/order/?ID=<?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?></a></p>
							<p>— по телефону +7 831 456-67-89</p><br>
							<p style="color: #344772; font-size:20px;">Выбранные товары:</p>
						</td>
					</tr>
			</table>


			<table style="width:735px; margin: 0 auto;"  class="cart"  border="0" cellpadding="10" cellspacing="0">
						<thead>
							<tr>
								<td colspan="2" style="width:200px; padding-left:30px;">Товар</td>
								<td>Цена</td>
								<td>Количество</td>
								<td>Стоимость</td>
							</tr>
						</thead>
						<tbody>
							<?
							$dbBasket = CSaleBasket::GetList(Array("ID"=>"ASC"), Array("ORDER_ID"=>$arResult["ORDER"]["ACCOUNT_NUMBER"]));
							while ($arItems = $dbBasket->Fetch()):?>

								<?
								$catItemId = null;
								$catItemUrl = "";
								$catItemPicture = "";
								$arSelect = Array("PROPERTY_CML2_LINK", "DETAIL_PICTURE");
								$arFilter = Array("IBLOCK_ID"=>57, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID"=>$arItems["PRODUCT_ID"]);
								$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
								if ($ob = $res->GetNextElement())
								{
									$arFields = $ob->GetFields();
									$catItemId = $arFields["PROPERTY_CML2_LINK_VALUE"];
								}

								if ($catItemId !== null) {
									$arSelect = Array("DETAIL_PICTURE", "DETAIL_PAGE_URL");
									$arFilter = Array("IBLOCK_ID"=>56, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID"=>$catItemId);
									$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
									if ($ob = $res->GetNextElement())
									{
										$arFields = $ob->GetFields();
										$catItemUrl = $arFields["DETAIL_PAGE_URL"];

										if (!empty($arFields["DETAIL_PICTURE"])) {
											$file = CFile::ResizeImageGet($arFields["DETAIL_PICTURE"], array('width'=>80, 'height'=>80), BX_RESIZE_IMAGE_EXACT, true);
											$catItemPicture = $file["src"];
										}
									}
								}

								?>
								<tr>
									<td style="padding-left:30px; width: 80px"><a href="<?=$catItemUrl?>" target="_blank">
										<?if (!empty($catItemPicture)):?>
											<img src="<?=$catItemPicture?>" alt="">
										<?endif;?>
									</a></td>
									<td><a style="color: #236FA0; text-decoration: underline;" href="<?=$catItemUrl?>" target="_blank"><?=$arItems["NAME"]?></a></td>
									<td style="color: #F54E86; white-space: nowrap;"><?=number_format($arItems["PRICE"], 0, ',', ' ');?> &#8381;</td>
									<td style="text-align: center;">
										<?=$arItems["QUANTITY"]?> шт.
									</td>
									<td style="padding-right:30px; width:80px; color: #F54E86; white-space: nowrap;"><?=number_format($arItems["PRICE"] * $arItems["QUANTITY"], 0, ',', ' ');?> &#8381;</td>

								</tr>
							<?endwhile;?>
							<tr>
								<td colspan="5" style="text-align:right; padding-right:50px; font-size:20px; background: #fff; height:30px;">
									Сумма заказа: <span style="color: #F54E86; padding-left:30px;"><?=number_format($arResult["ORDER"]["PRICE"], 0, ',', ' ');?>  &#8381;</span>
								</td>
							</tr>
						</tbody>
			</table>

			<p style="color: #344772; font-size:20px; background: #DDEDC9; padding:30px; margin:0;"><br>Информация о вашем заказе:</p>

			<table style="width:735px; margin: 0 auto;"  border="0" cellpadding="5" cellspacing="0">
				<tbody>
					<?foreach($arOrderProps as $k => $arOrderProp):?>
						<?
							// if ($arOrderProp["VALUE"] == "")
							// 	continue;

							if ($arOrderProp["ORDER_PROPS_ID"] == 25) {
								if ($arVal = CSaleOrderPropsVariant::GetByValue($arOrderProp["ORDER_PROPS_ID"], $arOrderProp["VALUE"]) ) {
									$arOrderProp["VALUE"] = $arVal["NAME"];
								}
							}
						?>
						<tr<?=(($k+1) % 2 != 0) ? ' style="background: #F4FAEE;"' : '';?>>
							<td style="height:40px; width:300px; padding-left: 30px; text-transform: uppercase;"><?=$arOrderProp["NAME"]?></td><td><?=$arOrderProp["VALUE"]?></td>
						</tr>
					<?endforeach;?>
				</tbody>
			</table>

			<table style="background: #DDEDC9;  margin:0; width:735px; ">
					<tr>
						<td style="padding:30px 20px 30px 30px; width:390px;">Если у вас есть вопросы или замечания по заказу, <br>то вы можете написать менеджеру:</td>
						<td><a href="/personal/chat/" target="_blank" style="background: #426EC7; padding: 10px 20px; border-radius:5px; font-size:15px; color: #fff; text-decoration: none">ЗАДАТЬ ВОПРОС МЕНЕДЖЕРУ</a></td>
					</tr>
			</table>

		</div>

		<?
		if (!empty($arResult["PAY_SYSTEM"]))
		{
			?>


			<table class="sale_order_full_table" style="margin: 0 auto;">
				<tr>
					<td class="ps_logo" style="text-align: center;">
						<div class="order-done center">
							<p><b><?=GetMessage("SOA_TEMPL_PAY")?>:</b>
							<?= $arResult["PAY_SYSTEM"]["NAME"] ?></p>
						</div>
						<?/*<?=CFile::ShowImage($arResult["PAY_SYSTEM"]["LOGOTIP"], 100, 100, "border=0", "", false);?>*/?>

					</td>
				</tr>
				<?
				if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
				{
					?>
					<tr>
						<td>
							<?
							$service = \Bitrix\Sale\PaySystem\Manager::getObjectById($arResult["ORDER"]['PAY_SYSTEM_ID']);

							if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
							{
								?>
								<script language="JavaScript">
									window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>&PAYMENT_ID=<?=$arResult['ORDER']["PAYMENT_ID"]?>');
								</script>
								<?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&PAYMENT_ID=".$arResult['ORDER']["PAYMENT_ID"]))?>
								<?
								if (CSalePdf::isPdfAvailable() && $service->isAffordPdf())
								{
									?><br />
									<?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&PAYMENT_ID=".$arResult['ORDER']["PAYMENT_ID"]."&pdf=1&DOWNLOAD=Y")) ?>
									<?
								}
							}
							else
							{
								if ($service)
								{
									/** @var \Bitrix\Sale\Order $order */
									$order = \Bitrix\Sale\Order::load($arResult["ORDER_ID"]);

									/** @var \Bitrix\Sale\PaymentCollection $paymentCollection */
									$paymentCollection = $order->getPaymentCollection();

									/** @var \Bitrix\Sale\Payment $payment */
									foreach ($paymentCollection as $payment)
									{
										if (!$payment->isInner())
										{
											$context = \Bitrix\Main\Application::getInstance()->getContext();
											$service->initiatePay($payment, $context->getRequest());
											break;
										}
									}
								}
								else
								{
									echo '<span style="color:red;">'.GetMessage("SOA_TEMPL_ORDER_PS_ERROR").'</span>';
								}
							}
							?>
						</td>
					</tr>
					<?
				}
				?>
			</table>
			<?
		}
		?>
	<?else:?>
		<div class="order-done center">
			<p>Ваш заказ <span class="order-num">№<?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?></span> принят! Менеджер свяжется с Вами в ближайшее время!</p>
			<a href="/catalog/">Продолжить выбор</a>
		</div>
		<div class="ticket">
			<div class="done">
				Заказ принят!
				<div class="order-num">№<?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?></div>
			</div>

			<div class="blue-button"><a href="/personal/order/make/print.php">Распечатать информацию о заказе</a></div>
		</div>


		<?/*<b><?=GetMessage("SOA_TEMPL_ORDER_COMPLETE")?></b><br /><br />
		<table class="sale_order_full_table">
			<tr>
				<td>
					<?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => $arResult["ORDER"]["ACCOUNT_NUMBER"]))?>
					<br /><br />
					<?= GetMessage("SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"])) ?>
				</td>
			</tr>
		</table>*/?>
		<?
		if (!empty($arResult["PAY_SYSTEM"]))
		{
			?>


			<table class="sale_order_full_table" style="margin: 0 auto;">
				<tr>
					<td class="ps_logo" style="text-align: center;">
						<div class="order-done center">
							<p><b><?=GetMessage("SOA_TEMPL_PAY")?>:</b>
							<?= $arResult["PAY_SYSTEM"]["NAME"] ?></p>
						</div>
						<?/*<?=CFile::ShowImage($arResult["PAY_SYSTEM"]["LOGOTIP"], 100, 100, "border=0", "", false);?>*/?>

					</td>
				</tr>
				<?
				if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
				{
					?>
					<tr>
						<td>
							<?
							$service = \Bitrix\Sale\PaySystem\Manager::getObjectById($arResult["ORDER"]['PAY_SYSTEM_ID']);

							if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
							{
								?>
								<script language="JavaScript">
									window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>&PAYMENT_ID=<?=$arResult['ORDER']["PAYMENT_ID"]?>');
								</script>
								<?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&PAYMENT_ID=".$arResult['ORDER']["PAYMENT_ID"]))?>
								<?
								if (CSalePdf::isPdfAvailable() && $service->isAffordPdf())
								{
									?><br />
									<?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&PAYMENT_ID=".$arResult['ORDER']["PAYMENT_ID"]."&pdf=1&DOWNLOAD=Y")) ?>
									<?
								}
							}
							else
							{
								if ($service)
								{
									/** @var \Bitrix\Sale\Order $order */
									$order = \Bitrix\Sale\Order::load($arResult["ORDER_ID"]);

									/** @var \Bitrix\Sale\PaymentCollection $paymentCollection */
									$paymentCollection = $order->getPaymentCollection();

									/** @var \Bitrix\Sale\Payment $payment */
									foreach ($paymentCollection as $payment)
									{
										if (!$payment->isInner())
										{
											$context = \Bitrix\Main\Application::getInstance()->getContext();
											$service->initiatePay($payment, $context->getRequest());
											break;
										}
									}
								}
								else
								{
									echo '<span style="color:red;">'.GetMessage("SOA_TEMPL_ORDER_PS_ERROR").'</span>';
								}
							}
							?>
						</td>
					</tr>
					<?
				}
				?>
			</table>
			<?
		}
	endif;
}
else
{
	?>
	<b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]))?>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>
	<?
}

$order = \Bitrix\Sale\Order::load($arResult["ORDER_ID"]);
$basket = $order->getBasket();
$itemsObj = array();
foreach ($basket as $key => $basketItem) {
    $product_id = $basketItem->getProductId();
    $elem = CCatalogSku::GetProductInfo($product_id);
    $itemsObj[$key]['id'] = $elem['ID'];
    $itemsObj[$key]['qnt'] = $basketItem->getQuantity();
    $itemsObj[$key]['price'] = $basketItem->getPrice();
}
$itemsObj = json_encode($itemsObj);
?>
<script>
	$(function(){
		$(".sale-paysystem-yandex-button-descrition").text('Вы будете перенаправлены на страницу оплаты');
	});
</script>

<?/*retail rocket*/?>
<script type="text/javascript">
    (window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function() {
        try {
            rrApi.order({
                transaction: "<?=$arResult["ORDER_ID"]?>",
                items: <?=$itemsObj?>
        });
        } catch(e) {}
    })
</script>
