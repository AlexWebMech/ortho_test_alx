<?
$MESS['T_IMG_WIDTH'] = "Ширина картинки товара";
$MESS['T_IMG_HEIGHT'] = "Высота картинки товара";
$MESS['T_PAYMENT_SERVICES_NAMES'] = "Отображать названия платежных систем";
$MESS['T_ALLOW_NEW_PROFILE'] = "Разрешить множество профилей покупателей";
$MESS['T_SHOW_STORES_IMAGES'] = "Показывать изображения складов в окне выбора пункта выдачи";
$MESS["IBLOCK_STR"] = "Инфоблок каталога";
$MESS["PROPERTY_CODE"] = "Список свойств выводимых в описании";
$MESS["PROPERTY_PHOTOS"] = "Свойство с доп. фото";
$MESS["PROPERTY_ARTICUL"] = "Свойство артикула";
?>