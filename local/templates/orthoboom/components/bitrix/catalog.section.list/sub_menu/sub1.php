<ul class="dropdown flex-container-w100 menu">
    <?foreach ($arItems["SECTIONS"] as $subItems):?>
        <div class="dropdown-column flex-4">
            <p class="dropdown-title">
                <?=$subItems["NAME"]?>
                <svg class="header-svg transition" xmlns="http://www.w3.org/2000/svg" width="12px" height="18px">
                    <path fill-rule="evenodd"  stroke="rgb(89, 120, 141)" stroke-width="2px"
                          stroke-linecap="butt" stroke-linejoin="miter" fill="none"
                          d="M2.566,1.175 L9.625,8.226 L2.566,15.278 "/>
                </svg>
            </p>
            <?if ($subItems["ELEMENT"]):?>
                <ul>
                    <?foreach ($subItems["ELEMENT"] as $subItems2):?>
                        <?if (!$subItems2["PROPERTY_LIST_VALUE"]):?>
                            <li class="dropdown-item">
                                <a class="dropdown-link" href="<?=$subItems2["CODE"]?>">
                                    <?=$subItems2["NAME"]?>
                                </a>
                            </li>
                        <?else:?>
                            <?$arSubIt[] =  $subItems2;?>
                        <?endif;?>
                    <?endforeach;?>
                    <?if ($arSubIt):?>
                        <div class="dropdown-action">
                            <?foreach ($arSubIt as $sub):?>
                                <li class="dropdown-item">
                                    <a class="dropdown-link <?if ($sub["PROPERTY_LIST_VALUE"] == "sale"):?>
                                    dropdown-btn transition<?endif;?>"
                                       href="<?=$sub["CODE"]?>">
                                        <?=$sub["NAME"]?>
                                    </a>
                                </li>
                            <?endforeach;?>
                        </div>
                    <?endif;?>
                </ul>
                <?unset($arSubIt);?>
            <?endif;?>
        </div>
    <?endforeach;?>
</ul>
