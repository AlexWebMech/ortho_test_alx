<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$sectionsTree = CIBlockSection::GetTreeList(
    $arFilter=Array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'ACTIVE' => 'Y'),
    $arSelect=Array("UF_*")
);

while($section = $sectionsTree->GetNext()) {
    $arResult["SECTIONS_N"][] = $section;

    if($section["UF_SHOW_ON_MOBILE"] == "1") {
        $arResult["SHOW_ON_MOBILE"][] = $section;
    }
}

