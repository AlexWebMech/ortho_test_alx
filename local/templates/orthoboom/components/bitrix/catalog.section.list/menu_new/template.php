<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>





<nav class="menu-items level-1 menu-items--only-desktop"><?

    foreach($arResult["SECTIONS_N"] as $i => $arMenuItem)
    {
        if ($arMenuItem["DEPTH_LEVEL"] < $arResult["SECTIONS_N"][$i - 1]["DEPTH_LEVEL"] && $arMenuItem["DEPTH_LEVEL"] == 1) {
            echo str_repeat("</div></div>"
                /*добавить товар*/
                . "</div></span>", $arResult["SECTIONS_N"][$i - 1]["DEPTH_LEVEL"] - $arMenuItem["DEPTH_LEVEL"]);
        } else {
            echo str_repeat("</div></div></div></span>", $arResult["SECTIONS_N"][$i - 1]["DEPTH_LEVEL"] - $arMenuItem["DEPTH_LEVEL"]);
        }

        $sMenuItemCode = preg_replace('/[^A-Za-z0-9]/u', '_', $arMenuItem["CODE"]);
        $sMenuItemCode = trim($sMenuItemCode, "_");

        ?><span js-category-id="<?=$arMenuItem["ID"]?>" class="menu-item menu-item-code-<?= $sMenuItemCode ?> level-<?= $arMenuItem["DEPTH_LEVEL"] ?><? if ($arMenuItem["SELECTED"] == "Y") : ?> active<? endif; ?><? if ($arMenuItem["IS_PARENT"]) : ?> parent<? endif; ?>" data-level="<?= $arMenuItem["DEPTH_LEVEL"]  ?>"><a class="menu-item-link" href="<?= $arMenuItem["CODE"] ?>"><span class="text"><?= $arMenuItem["NAME"] ?></span></a><?

        if ($arMenuItem["DEPTH_LEVEL"] < $arResult["SECTIONS_N"][$i + 1]["DEPTH_LEVEL"]) :
            ?><div class="menu-sub menu-sub-code-<?= $sMenuItemCode ?> level-<?= $arMenuItem["DEPTH_LEVEL"] + 1 ?>" data-level="<?= $arMenuItem["DEPTH_LEVEL"] + 1 ?>"><div class="body"><? if ($arMenuItem["DEPTH_LEVEL"] + 1 == 2) : ?><? endif; ?><div class="menu-items level-<?= $arMenuItem["DEPTH_LEVEL"] + 1 ?>"><?
        else :
            ?></span><?
        endif;

        ?><?
    }

    ?></nav>


<? if(count($arResult["SHOW_ON_MOBILE"]) > 0): ?>
    <nav class="menu-items level-1 menu-items--only-mobile">
        <?
        foreach($arResult["SHOW_ON_MOBILE"] as $i => $arMenuItem)
        {
            $sMenuItemCode = preg_replace('/[^A-Za-z0-9]/u', '_', $arMenuItem["CODE"]);
            $sMenuItemCode = trim($sMenuItemCode, "_");

            ?>
            <span class="menu-item level-1">
                <a class="menu-item-link" href="<?= $arMenuItem["CODE"] ?>">
                    <span class="text"><?= $arMenuItem["NAME"] ?></span>
                </a>
            </span><?
        }
        ?>
    </nav>
<? endif; ?>