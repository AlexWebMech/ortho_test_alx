<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<div class="bx-auth">
<?
ShowMessage($arParams["~AUTH_RESULT"]);
?>
<?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && is_array($arParams["AUTH_RESULT"]) &&  $arParams["AUTH_RESULT"]["TYPE"] === "OK"):?>
<p><?echo GetMessage("AUTH_EMAIL_SENT")?></p>
<?else:?>

<?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):?>
	<p><?echo GetMessage("AUTH_EMAIL_WILL_BE_SENT")?></p>
<?endif?>
<noindex>
<form method="post" class="modal-body" id="registerForm" action="<?=$arResult["AUTH_URL"]?>" name="bform" enctype="multipart/form-data">
<?
if (strlen($arResult["BACKURL"]) > 0)
{
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
}
?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="REGISTRATION" />
    <div class="row">
        <div class="col-12">
            <div class="modal-input-wrapper flex-container-w100 flex-side">
                <label><?=GetMessage("AUTH_NAME")?> <sup>*</sup></label>
                <input type="text" name="USER_NAME" maxlength="50" value="<?=$arResult["USER_NAME"]?>"  />
            </div>
            <div class="modal-input-wrapper flex-container-w100 flex-side">
                <label><?=GetMessage("AUTH_LAST_NAME")?></label>
                <input type="text" name="USER_LAST_NAME" maxlength="50" value="<?=$arResult["USER_LAST_NAME"]?>"  />
            </div>
            <div class="modal-input-wrapper flex-container-w100 flex-side">
                <label><?=GetMessage("AUTH_EMAIL")?> <sup>*</sup></label>
                <input type="email" name="USER_EMAIL" maxlength="255" value="<?=$arResult["USER_EMAIL"]?>"  />
            </div>
            <div class="modal-input-wrapper flex-container-w100 flex-side">
                <label><?=GetMessage("AUTH_LOGIN_MIN")?></label>
                <input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>"  />
            </div>
            <div class="modal-input-wrapper flex-container-w100 flex-side">
                <label><?=GetMessage("AUTH_PASSWORD_REQ")?> <sup>*</sup></label>
                <input type="password" name="USER_PASSWORD" maxlength="50" value="<?=$arResult["USER_PASSWORD"]?>"
                       autocomplete="off" />
            </div>
            <div class="modal-input-wrapper flex-container-w100 flex-side">
                <label><?=GetMessage("AUTH_CONFIRM")?> <sup>*</sup></label>
                <input type="password" name="USER_CONFIRM_PASSWORD" maxlength="50"
                       value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>"  autocomplete="off" />
            </div>
            <? include $_SERVER["DOCUMENT_ROOT"] . "/include/agreement.php";?>
            <div class="modal-input-wrapper flex-container-w100 flex-center">
                <?// ******************** /User properties ***************************************************

                /* CAPTCHA */
                if ($arResult["USE_CAPTCHA"] == "Y")
                {
                    ?>
                    <tr>
                        <td></td>
                        <td>
                            <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                        </td>
                    </tr>
                    <tr>
                        <td><input type="text" name="captcha_word" maxlength="50" value="" /></td>
                    </tr>
                    <?
                }
                /* CAPTCHA */
                ?>
            </div>
            <div class="modal-input-wrapper flex-container-w100 flex-center">
                <input class="mybtn transition yourCity-btn" type="submit" name="Register"
                       value="<?=GetMessage("AUTH_REGISTER")?>">
            </div>
        </div>
    </div>
</form>
</noindex>
<script type="text/javascript">
document.bform.USER_NAME.focus();
</script>

<?endif?>
</div>