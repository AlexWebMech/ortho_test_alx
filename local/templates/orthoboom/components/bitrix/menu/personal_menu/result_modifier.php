<?
global $APPLICATION;
$dir = $APPLICATION->GetCurDir();
foreach($arResult as $key => $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;

	if($arItem["SELECTED"]):
		if ($dir != $arItem["LINK"]) {
			unset($arResult[$key]["SELECTED"]);
		}
	endif;
	
endforeach?>