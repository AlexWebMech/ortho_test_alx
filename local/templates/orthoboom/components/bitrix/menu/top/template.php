<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div class="header-bottom flex-container-w100">
    <ul class="header__flex-menu flex-container-w100 header-menu">
    <?
    foreach($arResult as $arItem):
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
            continue;
    ?>
        <?if($arItem["SELECTED"]):?>
            <li class="header__flex-item flex-container flex-center">
                <a class="header__flex-link <?=$arItem["PARAMS"]["CLASS"]?>" href="<?=$arItem["LINK"]?>">
                    <?=$arItem["TEXT"]?>
                </a>
            </li>
        <?else:?>
            <li class="header__flex-item flex-container flex-center">
                <a class="header__flex-link <?=$arItem["PARAMS"]["CLASS"]?>" href="<?=$arItem["LINK"]?>">
                    <?=$arItem["TEXT"]?>
                </a>
            </li>
        <?endif?>

    <?endforeach?>
		<li class="header__flex-item flex-container flex-center only-mob"><
            a class="transition header__flex-link offline-icon" href="#">Оффлайн-сеть</a>
        </li>
		<?if (!$USER->IsAuthorized()):?>
			<li class="header__flex-item flex-container flex-center only-mob">
                <a class="transition header__flex-link" href="/auth/">Войти</a></li>
		<?else:?>
			<li class="header__flex-item flex-container flex-center only-mob">
                <a class="transition header__flex-link" href="/personal/">Личный кабинет</a>
            </li>
		<?endif;?>
    </ul>
</div>
<?endif?>