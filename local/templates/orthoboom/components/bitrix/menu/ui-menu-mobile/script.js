$(function () {
    var menuMobile = $('.header-mob-menu'),
        menuParentItem = menuMobile.find('.menu-item.parent'),
        menuSubmenu = menuMobile.find('.menu-sub'),
        submenuOpenControl = menuMobile.find('[data-submenu-open]'),
        submenuCloseControl = menuMobile.find('[data-submenu-close]'),
        activeClass = 'active';

    $(".header-menu-icon").on("click", function () {
        if (! $(".header-mob-menu").outerHeight()) {
            var offset = $(".header-main-container").height() - 6;
            $(".header-mob-menu").css("height", "calc(100vh - " + offset + "px)");
            $("body").addClass("mobile-menu-opened");
        } else {
            menuSubmenu.removeClass(activeClass);
            $(".header-mob-menu").css("height", "0");
            $("body").removeClass("mobile-menu-opened");
        }
    });

    $(submenuOpenControl).click(function(){
        showSubmenu($(this));
    });

    $(submenuCloseControl).click(function(){
        closeSubmenu($(this));
    });

    function showSubmenu(control) {
        var item = control.closest('.menu-item.parent'),
            itemLevel = item.data('level'),
            itemNextLevel = itemLevel + 1,
            itemSubmenu = item.find('.menu-sub').filter('[data-level=' + itemNextLevel + ']');

        if(!itemSubmenu.hasClass(activeClass)) {
            itemSubmenu.addClass(activeClass);
        }
    }

    function closeSubmenu(control) {
        var itemParentSubmenu = control.closest('.menu-sub');

        console.log(itemParentSubmenu);

        if(itemParentSubmenu.hasClass(activeClass)) {
            itemParentSubmenu.removeClass(activeClass);
        }
    }
});