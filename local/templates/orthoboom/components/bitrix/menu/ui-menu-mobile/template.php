<?

//debug($arResult);

?><nav class="menu-items level-1"><?

foreach($arResult as $i => $arMenuItem)
{
	if ($arMenuItem["DEPTH_LEVEL"] < $arResult[$i - 1]["DEPTH_LEVEL"] && $arMenuItem["DEPTH_LEVEL"] == 1) {
        echo str_repeat("</div></div></div></span>", $arResult[$i - 1]["DEPTH_LEVEL"] - $arMenuItem["DEPTH_LEVEL"]);
    } else {
        echo str_repeat("</div></div></div></span>", $arResult[$i - 1]["DEPTH_LEVEL"] - $arMenuItem["DEPTH_LEVEL"]);
    }
	
	$sMenuItemCode = preg_replace('/[^A-Za-z0-9]/u', '_', $arMenuItem["LINK"]);
	$sMenuItemCode = trim($sMenuItemCode, "_");
	
	?><span class="menu-item level-<?= $arMenuItem["DEPTH_LEVEL"] ?><? if ($arMenuItem["SELECTED"] == "Y") : ?> active<? endif; ?><? if ($arMenuItem["IS_PARENT"]) : ?> parent<? endif; ?>" data-level="<?= $arMenuItem["DEPTH_LEVEL"]  ?>">
        <? if($arMenuItem["IS_PARENT"]): ?>
            <span class="menu-item-link" data-submenu-open>
                <span class="text"><?= $arMenuItem["TEXT"] ?></span>
                <span class="icon-angle-right"></span>
            </span>
        <? else: ?>
            <a class="menu-item-link" href="<?= $arMenuItem["LINK"] ?>">
                <span class="text"><?= $arMenuItem["TEXT"] ?></span>
            </a>
        <? endif; ?>

    <?
	
	if ($arMenuItem["IS_PARENT"]) :
		?><div class="menu-sub level-<?= $arMenuItem["DEPTH_LEVEL"] + 1 ?>" data-level="<?= $arMenuItem["DEPTH_LEVEL"] + 1 ?>">
            <div class="body">
                <div class="menu-items level-<?= $arMenuItem["DEPTH_LEVEL"] + 1 ?>">
                <div class="menu-sub__back" data-submenu-close>
                    <span class="menu-sub__back-icon icon-angle-left"></span>
                    <?= $arMenuItem["TEXT"] ?>
                </div><?
	else : 
		?></span><?
	endif;	

	//if ($i < count($arResult))
	// echo $arParams["SEPARATOR"];

	?><?
}

?></nav><?

