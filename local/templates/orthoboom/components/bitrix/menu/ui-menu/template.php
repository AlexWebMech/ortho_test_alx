<?

//debug($arResult);

?><nav class="menu-items level-1"><?

foreach($arResult as $i => $arMenuItem)
{
	if ($arMenuItem["DEPTH_LEVEL"] < $arResult[$i - 1]["DEPTH_LEVEL"])
		echo str_repeat("</div></div></div></span>", $arResult[$i - 1]["DEPTH_LEVEL"] - $arMenuItem["DEPTH_LEVEL"]);
	
	$sMenuItemCode = preg_replace('/[^A-Za-z0-9]/u', '_', $arMenuItem["LINK"]);
	$sMenuItemCode = trim($sMenuItemCode, "_");
	
	?><span class="menu-item menu-item-code-<?= $sMenuItemCode ?> level-<?= $arMenuItem["DEPTH_LEVEL"] ?><? if ($arMenuItem["SELECTED"] == "Y") : ?> active<? endif; ?><? if ($arMenuItem["IS_PARENT"]) : ?> parent<? endif; ?>" data-level="<?= $arMenuItem["DEPTH_LEVEL"]  ?>"><a class="menu-item-link" href="<?= $arMenuItem["LINK"] ?>"><span class="text"><?= $arMenuItem["TEXT"] ?></span></a><?
	
	if ($arMenuItem["IS_PARENT"]) :
		?><div class="menu-sub menu-sub-code-<?= $sMenuItemCode ?> level-<?= $arMenuItem["DEPTH_LEVEL"] + 1 ?>" data-level="<?= $arMenuItem["DEPTH_LEVEL"] + 1 ?>"><div class="body"><? if ($arMenuItem["DEPTH_LEVEL"] + 1 == 2) : ?><span class="triangle-top"></span><? endif; ?><div class="menu-items level-<?= $arMenuItem["DEPTH_LEVEL"] + 1 ?>"><?
	else : 
		?></span><?
	endif;	

	//if ($i < count($arResult))
	// echo $arParams["SEPARATOR"];

	?><?
}

?></nav><?

