<? if(! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();



\Bitrix\Main\Loader::includeSharewareModule("uni.data");
\Uni\Data\BitrixComponents\IblockList::resultModifier($this);


foreach($arResult["ELEMENTS"] as &$arElement)
{
	if ($arElement["PROPERTY_FIKSIROVANNAYA_TSENA_ZNACHENIE"]) //$arElement["PROPERTY_FIKSIROVANNAYA_TSENA_INTERNET_MAGAZIN"] && 
		$arElement["MIN_PRICE"] = ["DISCOUNT_PRICE" => $arElement["PROPERTY_FIKSIROVANNAYA_TSENA_ZNACHENIE"], "PRICE" => $arElement["PROPERTY_FIKSIROVANNAYA_TSENA_ZNACHENIE"], "CURRENCY" => "RUB"];
	//if ($arElement["PROPERTY_FIKSIROVANNAYA_TSENA_ZNACHENIE"])
	//	vard($arElement["MIN_PRICE"]);
	//vard($arElement, 'die');

	if ($arElement["PROPERTY_CML2_ARTICLE"])
		if (mb_strpos($arElement["NAME"], $arElement["PROPERTY_CML2_ARTICLE"]) !== false)
			$arElement["NAME"] = str_replace($arElement["PROPERTY_CML2_ARTICLE"], '<span style="white-space: nowrap">' . $arElement["PROPERTY_CML2_ARTICLE"] . '</span>', $arElement["NAME"]);
		

	$arElement["OFFERS"] = array_reverse($arElement["OFFERS"]);
	foreach($arElement["OFFERS"] as &$arOffer)
	{
		if (mb_substr($arOffer["PROPERTY_OBUV_RAZMER--HTML"], 0, 2) == "р.")
			$arOffer["PROPERTY_OBUV_RAZMER--HTML"] = mb_substr($arOffer["PROPERTY_OBUV_RAZMER--HTML"], 2);
	}
	unset($arOffer);
}
unset($arElement);


?>