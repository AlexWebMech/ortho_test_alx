<? if(! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

$this->setFrameMode(true);

//vard($arResult, '', true);

if (! $arParams["UI_MAIN_CLASS"])
	$arParams["UI_MAIN_CLASS"] = "catalog";
if (! $arParams["UI_MAIN_LIST_CLASS"])
	$arParams["UI_MAIN_LIST_CLASS"] = $arParams["UI_MAIN_CLASS"] . "-list";

if (! $arParams["UI_LIST_WRAP_CLASS"])
	if ($arParams["UI_WRAP_CLASS"])
		$arParams["UI_LIST_WRAP_CLASS"] = $arParams["UI_WRAP_CLASS"];
	else
		$arParams["UI_LIST_WRAP_CLASS"] = "ui-list--box";	

if (! $arParams["UI_MAIN_PAGINATION_CLASS"])
	$arParams["UI_MAIN_PAGINATION_CLASS"] = $arParams["UI_MAIN_CLASS"] . "--pagination";
if (! $arParams["UI_WRAP_CLASS"])
	$arParams["UI_WRAP_CLASS"] = "catalog-list__orthoboom";
	
if ($arParams["UNI_AJAX_MODE"] == "Y")
	$arParams["UI_WRAP_CLASS"] .= " uni-ajax-wrap";

if (! $arParams["UI_FLEX_SHOW"])
	$arParams["UI_FLEX_SHOW"] = "Y";
if (! $arParams["UI_TITLE_SHOW"])
	$arParams["UI_TITLE_SHOW"] = "Y";
if (! $arParams["UI_PICTURE_SHOW"])
	$arParams["UI_PICTURE_SHOW"] = "Y";
if (! $arParams["UI_PRICE_SHOW"])
	$arParams["UI_PRICE_SHOW"] = "Y";
if (! $arParams["UI_PROPERTIES_SHOW"])
	$arParams["UI_PROPERTIES_SHOW"] = "Y";

//vard($arResult);
//vard(current($arResult["ELEMENTS"]));
?>

<? if (! $arResult["ELEMENTS"])
{
	?><?
}
else
{
	?> 
	

	
	<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?> <?= $arParams["UI_LIST_WRAP_CLASS"] ?>">
		<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--block">
		
	<div style="height: 0">
		<span class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--fav-button ui-icon"></span>
		<span class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--fav-button-active ui-icon"></span>
	</div>		
		
			<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--items"><?
			foreach($arResult["ELEMENTS"] as $arItem)
			{
				?> 
          <?if (in_array($arItem['ID'], $arResult["WISHLIST"])):?>
              <?$wishlist = "delete";?>
          <?else:?>
              <?$wishlist = "add";?>
          <?endif;?>
				<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--item uprod_list_item" data-iblock-element-id="<?= htmlspecialchars($arItem["ID"]) ?>" data-item-id="<?= htmlspecialchars($arItem["ID"]) ?>">
				
					<? if ($arParams["UI_INNER_SHOW"] == "Y") : //Inner ?>
					<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--inner ui-inner"> 
					<? endif; ?>
					
					<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--marks">		
					<? if ($arItem["PROPERTY_AKTSIYA_INTERNET_MAGAZIN"]) : ?>
						<span class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--mark <?= $arParams["UI_MAIN_LIST_CLASS"] ?>--mark-sale">Акция</span>
					<? endif; ?>
					<? if ($arItem["PROPERTY_KHIT_PRODAZH_INTERNET_MAGAZIN"]) : ?>
						<span class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--mark <?= $arParams["UI_MAIN_LIST_CLASS"] ?>--mark-hit">Хит</span>
					<? endif; ?>				
					<? if ($arItem["PROPERTY_NOVINKA_INTERNET_MAGAZIN"]) : ?>
						<span class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--mark <?= $arParams["UI_MAIN_LIST_CLASS"] ?>--mark-novelty">Новинка</span>
					<? endif; ?>				
					
					</div>
	
					<? //Picture thumbnails ?>
					<? if ($arParams["UI_THUMBNAILS_SHOW"] == "Y") : ?>
					<? if (is_array($arItem["PICTURES--INFO"]) && count($arItem["PICTURES--INFO"])) : ?>
						<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--thumbnails-wrap">
							<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--thumbnails">
								<? $iThumbnail = 0; foreach($arItem["PICTURES--INFO"] as $arPictureInfo) : $iThumbnail++; ?>
									<? if ($arPicture = $arPictureInfo["PREVIEW_PICTURE"] ? $arPictureInfo["PREVIEW_PICTURE"] : $arPictureInfo["PICTURE"]) : ?>
										<span class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--thumbnail">
											<img src="<?= $arPicture["SRC"] ?>" width="<?= $arPicture["WIDTH"] ?>" height="<?= $arPicture["HEIGHT"] ?>" alt="">
										</span>
									<? endif; ?>
									<? if ($iThumbnail >= 4)
										break; ?>
								<? endforeach; ?>
							</div>
						</div>
					<? else : ?>
						<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--thumbnails-wrap ui-empty"></div>
					<? endif; ?>
					<? endif; ?>
					<? //-- ?>				
					
					<? if ($arParams["UI_FLEX_SHOW"] == "Y") : //Flex ?>
					<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--flex">
						<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--picture-col ui-picture-col">
					<? endif; ?>
				
							<? //Picture ?>
							<? if ($arParams["UI_PICTURE_SHOW"] == "Y") : ?>
								<? if ($arPicture = $arItem["PICTURE--INFO"]["PREVIEW_PICTURE"] ? $arItem["PICTURE--INFO"]["PREVIEW_PICTURE"] : $arItem["PICTURE--INFO"]["PICTURE"]) : ?>
									<span class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--picture"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
										<img src="<?= $arPicture["SRC"] ?>" width="<?= $arPicture["WIDTH"] ?>" height="<?= $arPicture["HEIGHT"] ?>" alt="">
									</a></span>
								<? else : ?>
									<span class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--picture ui-empty"></span>
								<? endif; ?>
							<? endif; ?>
							<? //-- ?>
							
							<? //Pictures ?>
							<? if ($arParams["UI_PICTURES_SHOW"] == "Y") : ?>
							<? if (is_array($arItem["PICTURES--INFO"]) && count($arItem["PICTURES--INFO"])) : ?>
								<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--pictures-wrap">
									<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--pictures">
										<? $iThumbnail = 0; foreach($arItem["PICTURES--INFO"] as $arPictureInfo) : $iThumbnail++; ?>
											<? if ($arPicture = $arPictureInfo["PREVIEW_PICTURE"] ? $arPictureInfo["PREVIEW_PICTURE"] : $arPictureInfo["PICTURE"]) : ?>
												<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--picture"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
													<img src="<?= $arPicture["SRC"] ?>" width="<?= $arPicture["WIDTH"] ?>" height="<?= $arPicture["HEIGHT"] ?>" alt="">
												</a></div>
											<? endif; ?>
											<? if ($iThumbnail >= 4)
												break; ?>
										<? endforeach; ?>
									</div>
								</div>
							<? else : ?>
								<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--pictures-wrap ui-empty"></div>
							<? endif; ?>	
							<? endif; ?>
							
					<? if ($arParams["UI_FLEX_SHOW"] == "Y") : //Flex ?>		
						</div>
						<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--main-col ui-main-col">
					<? endif; ?>
          
              <?if ($arItem["PROPERTIES"]["CML2_ARTICLE"]) : ?>
                <div class="goods-article some-text text-center mb-2">артикул:&#160;<?= $arItem["PROPERTIES"]["CML2_ARTICLE"]["VALUE"] ?></div>
					    <?endif;?>

							<? //Title ?>
							<? if ($arParams["UI_TITLE_SHOW"] == "Y") : ?>
								<div class="goods-title small-title text-center mb-3 masonry-grid__link">
									<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
								</div>
							<? endif; ?>	
							<? //-- ?>
							
							<? //Text ?>
							<? if ($arParams["UI_TEXT_SHOW"] == "Y") : ?>
								<? if ($arItem["PREVIEW_TEXT"]) : ?>
									<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--text"><?= $arItem["PREVIEW_TEXT"] ?></div>
								<? endif; ?>
							<? endif; ?>
							
							<? //Properties ?>
							<? if ($arParams["UI_PROPERTIES_SHOW"] == "Y") : ?>
								
								<? 
								//$arDisplayPropCodes = (is_array($arParams["PROPERTY_CODE"]) && count($arParams["PROPERTY_CODE"])) ? $arParams["PROPERTY_CODE"] : array_keys($arItem["DISPLAY_PROPERTIES"]);
								
								//Calc properties
								$arDisplayPropCodes = array_keys($arItem["DISPLAY_PROPERTIES"]);
								//--
								?> 
								<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--properties">
								<? foreach($arDisplayPropCodes as $sPropertyCode) : 
									if ($arResult["PROPERTIES"][$sPropertyCode]["SHOW"] == "N")
										continue; ?>
									<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--property">
										<span class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--property-title"><?= $arProperty["NAME"] ?>:</span> 
										<span class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--property-value"><?= $arItem["PROPERTY_" . $sPropertyCode . "--HTML"] ? $arItem["PROPERTY_" . $sPropertyCode . "--HTML"] : $arItem["PROPERTY_" . $sPropertyCode] ?></span>
									</div>						
								<? endforeach; ?>
								</div>
							<? endif; ?>						
							<? //-- ?>
	
							<? //Price ?>
							<? /* if ($arParams["UI_PRICE_SHOW"] != "N") : ?>
								<? if ($arItem["MIN_PRICE"]["PRICE"] > 0) : ?>
									<span class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--price">
										<span class="ui-price" data-currency-code="<?= htmlspecialchars($arItem["MIN_PRICE"]["CURRENCY"]) ?>">
											<? if ($arItem["BASE_PRICE"]["PRICE"] > $arItem["MIN_PRICE"]["DISCOUNT_PRICE"]) : ?>
											<span class="ui-price--old-value"><?= $arItem["BASE_PRICE"]["PRICE--HTML"] !== NULL ? $arItem["BASE_PRICE"]["PRICE--HTML"] : CCurrencyLang::CurrencyFormat($arItem["BASE_PRICE"]["PRICE"], $arItem["BASE_PRICE"]["CURRENCY"], false) ?></span>
											<? endif; ?>
											<span class="ui-price--value"><?= $arItem["MIN_PRICE"]["DISCOUNT_PRICE--HTML"] !== NULL ? $arItem["MIN_PRICE"]["DISCOUNT_PRICE--HTML"] : CCurrencyLang::CurrencyFormat($arItem["MIN_PRICE"]["DISCOUNT_PRICE"], $arItem["MIN_PRICE"]["CURRENCY"], false); ?></span>
											<span class="ui-price--currency"><?= $arItem["MIN_PRICE"]["CURRENCY#HTML"] !== NULL ? $arItem["MIN_PRICE"]["CURRENCY#HTML"] : str_replace("0", "", CCurrencyLang::CurrencyFormat("0", $arItem["MIN_PRICE"]["CURRENCY"], true)); ?></span>
										</span>
									</span>
								<? endif; ?>
							<? endif; ?>
							<? //-- */ ?>

              <form class="goods-item-buy-form">
                <input type="hidden" name="product_name" value='<?=$arItem["NAME"];?>'>
                <input type="hidden" name="fixed_price" value='<?=$arItem["MIN_PRICE"]["DISCOUNT_PRICE"] ? $arItem["MIN_PRICE"]["DISCOUNT_PRICE"] : $arItem["BASE_PRICE"]["PRICE"];?>'>
                <div class="goods-buy-area">
                  <div class="goods-price" data-currency-code="<?= htmlspecialchars($arItem["MIN_PRICE"]["CURRENCY"]) ?>">

                      <? if ($arItem["BASE_PRICE"]["PRICE"] > $arItem["MIN_PRICE"]["DISCOUNT_PRICE"]) : ?>
											<div class="goods-old-price"><?= $arItem["BASE_PRICE"]["PRICE--HTML"] !== NULL ? $arItem["BASE_PRICE"]["PRICE--HTML"] : CCurrencyLang::CurrencyFormat($arItem["BASE_PRICE"]["PRICE"], $arItem["BASE_PRICE"]["CURRENCY"], false) ?> <span class="ui-price--currency"><?= $arItem["MIN_PRICE"]["CURRENCY#HTML"] !== NULL ? $arItem["MIN_PRICE"]["CURRENCY#HTML"] : str_replace("0", "", CCurrencyLang::CurrencyFormat("0", $arItem["MIN_PRICE"]["CURRENCY"], true)); ?></span></div>
											<? endif; ?>

                      <? if ($arItem["MIN_PRICE"]["PRICE"] > 0) : ?>
											<div class="goods-new-price"><?= $arItem["MIN_PRICE"]["DISCOUNT_PRICE--HTML"] !== NULL ? $arItem["MIN_PRICE"]["DISCOUNT_PRICE--HTML"] : CCurrencyLang::CurrencyFormat($arItem["MIN_PRICE"]["DISCOUNT_PRICE"], $arItem["MIN_PRICE"]["CURRENCY"], false); ?> <span class="ui-price--currency"><?= $arItem["MIN_PRICE"]["CURRENCY--HTML"] !== NULL ? $arItem["MIN_PRICE"]["CURRENCY--HTML"] : str_replace("0", "", CCurrencyLang::CurrencyFormat("0", $arItem["MIN_PRICE"]["CURRENCY"], true)); ?></span></div>
											<? endif; ?>

                  </div>
                  <div class="goods-buttons">
                    <div class="goods-item-buy">
                      <button type="button" class="add2cart-btn">В корзину</button>
                    </div>
                    <div class="goods-item-wish">
                      <button type="button" id="id<?=$arItem["ID"]?>" data-id="<?=$arItem["ID"]?>" class="add2wish-btn" onclick="ym(25218083, 'reachGoal', 'favorite_add');return false;"></button>
                    </div>
                  </div>
                  <? if ($arParams["UI_OFFERS_SHOW"] == "Y") : ?>
                    <div class="goods-infoblock">
                      <div class="goods-sizes">
                        <? if (count($arItem["PROPERTIES"]["ITEM_SIZES"] > 0)): ?>
                            <div class="goods-sizes__title">Выберите размер</div>
                            <div class="goods-sizes__body">
                              <?$cnt = 1;?>
                              <? foreach($arItem["OFFERS"] as $arOffer) : ?>
                              <?$randId = mt_rand();
                                if (! $arOffer["CAN_BUY"])
                                  continue;
                                $sOffetTitle = false;
                                foreach($arParams["UI_OFFERS_TITLE_PROPERTY_CODES"] as $sOfferPropertyCode)
                                  if (strlen($sOffetTitle = $arOffer["PROPERTY_" . $sOfferPropertyCode . "--HTML"]))
                                    break;
                                if ($sOffetTitle !== false) : ?>
                                      <div class="goods-sizes__item">
                                        <input
                                        class="goods-size-item-input"
                                        type="radio"
                                        id="item-size-<?=$randId?>"
                                        name="id"
                                        value="<?= $arOffer["ID"] ?>"
                                        data-id="<?= $arOffer["ID"] ?>"
                                        data-name="bootSize"
                                        <?=$cnt == 1 ? ' checked ' : '';?>
                                        >
                                        <label class="goods-size-item-label" for="item-size-<?=$randId?>">р.<?= $sOffetTitle ?></label>
                                      </div>
                                <? endif; ?>
                                <?$cnt++;?>
                              <? endforeach; ?>
                            </div>
                        <? endif; ?>
                      </div>
                      <div class="goods-info-text">Срок доставки от 2 до 7 дней</div>
                    </div>
                  <? endif; ?>
                </div>
              </form>
							
							<? //Offers ?>
							
							
							<? //if ($arParams["UI_OFFERS_SHOW"] == "Y") : ?>
							
							<?/*
							<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--sku">
								<? if (count($arItem["OFFERS"])) : ?>
								<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--sku-block">
									<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--sku-title">
										Размеры в наличии
									</div>
									<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--offers">
									<? foreach($arItem["OFFERS"] as $arOffer) : 
										if (! $arOffer["CAN_BUY"])
											continue;
										$sOffetTitle = false;
										foreach($arParams["UI_OFFERS_TITLE_PROPERTY_CODES"] as $sOfferPropertyCode)
											if (strlen($sOffetTitle = $arOffer["PROPERTY_" . $sOfferPropertyCode . "--HTML"]))
												break;
										if ($sOffetTitle !== false) : ?>
											<span class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--offer">
												<?= $sOffetTitle ?>
											</span>
										<? endif; ?>
									<? endforeach; ?>
									</div>
								</div>	
								<? endif; ?>
							
							</div>
							*/ ?>
							
							
							<? //endif; ?>
	
							<? //Buy ?>
							<? // if ($arParams["UI_BUY_SHOW"] != "N") : ?>
								
								<? ////////////////////////////// ?>
									
								<?/*	
										<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--sku-modal ui-modal ui-modal--normal hide">
											<div class="ui-modal-overlay ui-overlay"></div>
											<div class="ui-modal-inner ui-inner">
												<div class="ui-modal-header">
													<div class="ui-modal-title">Добавление в корзину</div>
													<span class="ui-modal-close"><span class="ui-modal-close-icon ui-icon"></span></span>
												</div>
												<div class="ui-modal-content ui-modal-main">
														
													<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--sku-modal--title">
														Выбор размера
													</div>
														
													<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--sku-modal--offers">
														<? foreach($arItem["OFFERS"] as $arOffer) : 	
															if (! $arOffer["CAN_BUY"])
																continue;
															$sOffetTitle = false;
															foreach($arParams["UI_OFFERS_TITLE_PROPERTY_CODES"] as $sOfferPropertyCode)
																if (strlen($sOffetTitle = $arOffer["PROPERTY_" . $sOfferPropertyCode . "--HTML"]))
																	break;
															if ($sOffetTitle !== false) : ?>
																		<label class="ui-checkbox ui-checkbox--box">
																			<input type="radio" name="id" value="<?= $arOffer["ID"] ?>">
																			<span class="ui-box">
																				<span class="ui-checkbox--icon ui-icon"></span>
																				<span class="ui-text"><?= $sOffetTitle ?></span>
																			</span>
																		</label>
															<? endif; ?>
														<? endforeach; ?>
													</div>
													
													<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--sku-modal--buy--sku-modal--buy">
														<span class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--sku-modal--buy-button ui-button <?= $arParams["UI_MAIN_CLASS"] ?>--item-buy-action" >В корзину</span>
													</div>	
													
													
									
												</div>
											</div>
											
										</div>	
									*/?>
									
									<? ////////////////////////////// ?>						
								
								
							<?// endif; ?>
							<? //-- ?>
							
							<? //Detail ?>
							<? if ($arParams["UI_DETAIL_SHOW"] != "N") : ?>
								<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--detail">
									
								</div>
							<? endif; ?>
							<? //-- ?>		
							
					<? if ($arParams["UI_FLEX_SHOW"] == "Y") : //Flex ?>		
						</div>
					</div>	
					<? endif; ?>
					
		
					
					<? if (0 && $arParams["UI_BG_LINK_SHOW"] != "N") : ?>
					<a class="catalog--link" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"></a>
					<? endif; ?>
					
					<? if ($arParams["UI_INNER_SHOW"] == "Y") : //Inner ?>
					</div>
					<? endif; ?>					
					
				</div>
				<?
			}
			for ($i = 1; $i <= 8; $i++)
			{
				?><div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--item ui-flex-empty"></div><?
			}
			?></div>

		<script type="text/javascript" class="uni-ajax-script">
		$(".catalog-list--item:not(.inited)").each(function () {
			
			$(this).find(".<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--buy-button").on("click", function () { 
				var itemId = $(this).parents(".catalog-list--item").eq(0).data("iblock-element-id");

				localCatalog.addToCart(itemId) 
			});
			
			$(this).find(".catalog-list--fav-button").on("click", function () {
				var itemId = $(this).parents(".catalog-list--item").eq(0).data("iblock-element-id");
				localCatalog.toggleFav(itemId);
			});
			
			$(this).addClass("inited");
		});
		</script>	
		
		</div>		
		<?
		
		if ($arParams["UI_NAV_SHOW"] == "Y" || $arParams["DISPLAY_BOTTOM_PAGER"] == "Y") 
		{ ?> 			
			<div class="<?= $arParams["UI_MAIN_PAGINATION_CLASS"] ?>">
				<? $APPLICATION->IncludeComponent("uni:pagination", "", Array(
					"UI_MAIN_CLASS" => $arParams["UI_MAIN_CLASS"],
					"UI_WRAP_CLASS" => "ui-pagination__orthoboom",
					
					"NAV_OBJECT" => $arResult["NAV_RESULT"],
					"CLEAR_VARS" => ["uni_ajax", "SECTION_CODE_PATH", "SECTION_ID", "SECTION_CODE", "ELEMENT_ID", "ELEMENT_CODE"],
					
					"ITEM_AJAX_PROFILE" => $arParams["UI_MAIN_CLASS"] . "--items",
					"MORE_AJAX_PROFILE" => $arParams["UI_MAIN_CLASS"] . "--more",
					
					//"UI_LIST_WRAP_CLASS" => $arParams["UI_MAIN_CLASS"] . "-list",
					//"UI_ITEMS_CLASS" => $arParams["UI_MAIN_CLASS"] . "-items",
					//"UI_ITEM_CLASS" => $arParams["UI_MAIN_CLASS"] . "--item",	
				
					"UI_FIRST_PAGE_SHOW" => "N",
					"UI_FIRST_NUM_PAGE_SHOW" => "N",
					"UI_PREV_PAGE_SHOW" => "Y",
					"UI_PREV_PAGE_TITLE" => "Предыдущая", 
					"UI_NEXT_PAGE_SHOW" => "Y",
					"UI_NEXT_PAGE_TITLE" => "Следующая", 
					"UI_HALF_PAGE_SHOW" => "N",
					"UI_LAST_NUM_PAGE_SHOW" => "N",
					"UI_LAST_PAGE_SHOW" => "N",
					
					"PAGE_LIMIT" => 6,
					
					"UI_MORE_TITLE" => "Показать еще",
					
					
				)); ?>
			</div>
			
			<? if (1) //
			{
				$arResult["UNI_AJAX"]["PROFILES"]["ITEMS"]["wrapTarget"] = "." . $arParams["UI_MAIN_LIST_CLASS"];
				
				$arResult["UNI_AJAX"]["PROFILES"]["ITEMS"]["cut"] = true;
				$arResult["UNI_AJAX"]["PROFILES"]["ITEMS"]["updateUrl"] = true;
				if (! isset($arResult["UNI_AJAX"]["PROFILES"]["ITEMS"]["scrollTo"]))
					$arResult["UNI_AJAX"]["PROFILES"]["ITEMS"]["scrollTo"]["target"] = "main"; //"." . $arParams["UI_MAIN_LIST_CLASS"];
				
				$arResult["UNI_AJAX"]["PROFILES"]["MORE"]["wrapTarget"] = "." . $arParams["UI_MAIN_LIST_CLASS"];
				$arResult["UNI_AJAX"]["PROFILES"]["MORE"]["updateUrl"] = true;
				$arResult["UNI_AJAX"]["PROFILES"]["MORE"]["commands"] = [
					["target" => "." . $arParams["UI_MAIN_LIST_CLASS"] . "--items", "command" => "append", "cut" => true],
					["target" => "." . $arParams["UI_MAIN_PAGINATION_CLASS"], "cut" => true],				
					["target" => "script", "command" => "append", "cut" => true],
				];
				?>
				<script type="text/javascript">
				if (typeof(uniAjaxLib) != "undefined") {
					uniAjaxLib.addProfile("<?= $arParams["UI_MAIN_CLASS"] ?>--items", <?= \Bitrix\Main\Web\Json::encode($arResult["UNI_AJAX"]["PROFILES"]["ITEMS"], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK) ?>);
					uniAjaxLib.addProfile("<?= $arParams["UI_MAIN_CLASS"] ?>--more", <?= \Bitrix\Main\Web\Json::encode($arResult["UNI_AJAX"]["PROFILES"]["MORE"], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK) ?>);
				}
				</script>
			<? 
			} 
			?>			
		<? 
		} ?> 
		
		<script  class="uni-ajax-script" type="text/javascript">
		/*
		$(".<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--item").on("mouseenter", function () {
			//if (! $(this).css("height"))
				$(this).css("height", $(this).outerHeight() + "px");
		});
		*/
		
		$(".<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--item").each(function () {
			$(this).css("height", $(this).outerHeight() + "px");
		});
		</script>		
				
		<? //Swiper (thumbnails)
		if ($arParams["UI_THUMBNAILS_SWIPER_SHOW"] == "Y" && $arItem["PICTURES"])
		{
			$arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["thumbnailSwiper"] = is_array($arParams["UI_THUMBNAILS_SWIPER_JS_PARAMS"]) ? $arParams["UI_THUMBNAILS_SWIPER_JS_PARAMS"] : [];
			$arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["thumbnailSwiper"]["el"] = "." . $arParams["UI_MAIN_LIST_CLASS"] . "--thumbnails-wrap:not(.ui-empty)";	
			
			if (! isset($arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["thumbnailSwiper"]["threshold"]))
				$arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["thumbnailSwiper"]["threshold"] = 10;	
			?> 
			<script type="text/javascript">
			$(".<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--thumbnails-wrap").addClass("swiper-container");
			$(".<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--thumbnails").addClass("swiper-wrapper");
			$(".<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--thumbnail").addClass("swiper-slide");
			<? if ($arParams["UI_PICTURES_SWIPER_SHOW"] != "Y") : ?>
			new Swiper("<?= htmlspecialchars($arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["thumbnailSwiper"]["el"]); ?>", <?= \Bitrix\Main\Web\Json::encode($arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["thumbnailSwiper"], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK); ?>);
			<? endif; ?>
			</script>
		<?
		}
		//--
		?>
		<? //Swiper (pictures)
		if ($arParams["UI_PICTURES_SWIPER_SHOW"] == "Y" && $arItem["PICTURES"])
		{
			$arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["mainSwiperParams"] = is_array($arParams["UI_PICTURES_SWIPER_JS_PARAMS"]) ? $arParams["UI_PICTURES_SWIPER_JS_PARAMS"] : [];
			$arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["mainSwiperParams"]["el"] = "." . $arParams["UI_MAIN_LIST_CLASS"] . "--pictures-wrap";
			
			if ($arParams["UI_THUMBNAILS_SWIPER_SHOW"] == "Y")
				$arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["mainSwiperParams"]["thumbs"]["swiper"] = $arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["thumbnailSwiper"];
			
			if (! isset($arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["mainSwiperParams"]["slidesPerView"]))
				$arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["mainSwiperParams"]["slidesPerView"] = 1;
			if (! isset($arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["mainSwiperParams"]["effect"]))
				$arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["mainSwiperParams"]["effect"] = "fade";
			if (! isset($arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["mainSwiperParams"]["fadeEffect"]))
				$arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["mainSwiperParams"]["fadeEffect"]["crossfade"] = true;

			if (! isset($arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["mainSwiperParams"]["loop"]))
				$arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["mainSwiperParams"]["loop"] = true;

			
			if (! isset($arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["mainSwiperParams"]["threshold"]))
				$arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["mainSwiperParams"]["threshold"] = 3;		
			?> 
			<script class="uni-ajax-script" type="text/javascript">
			$(".<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--pictures-wrap").addClass("swiper-container");
			$(".<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--pictures").addClass("swiper-wrapper");
			$(".<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--picture").addClass("swiper-slide");
			
			<? foreach($arResult["ELEMENTS"] as $arSubItem)
			{
				$arSwiperParams = $arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["mainSwiperParams"];
				$arSwiperParams["el"] = "." . $arParams["UI_MAIN_LIST_CLASS"] . "--item[data-iblock-element-id='" . htmlspecialchars($arSubItem["ID"]) . "'] " . $arSwiperParams["el"];
				if ($arSwiperParams["thumbs"]["swiper"]["el"])
					$arSwiperParams["thumbs"]["swiper"]["el"] = "." . $arParams["UI_MAIN_LIST_CLASS"] . "--item[data-iblock-element-id='" . htmlspecialchars($arSubItem["ID"]) . "'] " . $arSwiperParams["thumbs"]["swiper"]["el"];
				?>
				new Swiper("<?= htmlspecialchars($arSwiperParams["el"]); ?>", <?= \Bitrix\Main\Web\Json::encode($arSwiperParams, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK); ?>);
			<?
			}
			?>
			</script>
			<?
		}
		//--
		?>
		
		<? if ($arParams["UI_THUMBNAILS_SWIPER_SHOW"] == "Y" && $arParams["UI_PICTURES_SWIPER_SHOW"] == "Y" && $arItem["PICTURES"]) : ?>
		<script class="uni-ajax-script" type="text/javascript">
		window["<?= $arParams["UI_MAIN_LIST_CLASS"] ?>ThumbnailTimeout"] = null;
		$(".<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--thumbnail").on("mouseenter", function () {
			clearTimeout(window["<?= $arParams["UI_MAIN_LIST_CLASS"] ?>ThumbnailTimeout"]);
			
			var itemNode = $(this).parents("*[data-iblock-element-id]").eq(0);
			var index = itemNode.find(".<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--thumbnail").index(this);
			
			window["<?= $arParams["UI_MAIN_LIST_CLASS"] ?>ThumbnailTimeout"] = setTimeout(function (itemNode, index) {
				clearTimeout(window["<?= $arParams["UI_MAIN_LIST_CLASS"] ?>ThumbnailTimeout"]);
				
				var itemEl = itemNode.find(".<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--pictures-wrap");
				if (itemEl[0])
					if (itemEl[0].swiper)
						itemEl[0].swiper.slideToLoop(index);
			}, 125, itemNode, index);

		}); 
		$(".<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--thumbnail").on("mouseleave", function () {
			clearTimeout(window["<?= $arParams["UI_MAIN_LIST_CLASS"] ?>ThumbnailTimeout"]);
		});
		
		$(".<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--item").on("mouseleave", function () {
			var els = $(this).find(".<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--pictures-wrap");
			if (els[0])
				if (els[0].swiper)	
					els[0].swiper.slideToLoop(0);
		});
		</script>
		<? endif; ?>
		
		<?
		$arResult["UNI_CATALOG_ITEM"]["JS_PARAMS"]["uniCatalog"] = [];
		?>
	
	
	
									<div class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--buy-modal ui-modal ui-modal--normal ">
										<div class="ui-modal-overlay ui-overlay"></div>
										<div class="ui-modal-inner ui-inner">
											<div class="ui-modal-header">
												<div class="ui-modal-title">Товар добавлен</div>
												<span class="ui-modal-close"><span class="ui-modal-close-icon ui-icon"></span></span>
											</div>
											<div class="ui-modal-content ui-modal-main">
												
												
												<a class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--buy-modal-order-button ui-button ui-button-normal" href="/personal/cart/">Оформить заказ</a>
												<span class="<?= $arParams["UI_MAIN_LIST_CLASS"] ?>--buy-modal-catalog-button ui-button ui-button-inverted ui-modal-close" href="/personal/cart/">Продолжить покупки</a>
												
												
												
								
											</div>
										</div>
										
									</div>	
	

		
		
	
		
	</div><?
}
?>