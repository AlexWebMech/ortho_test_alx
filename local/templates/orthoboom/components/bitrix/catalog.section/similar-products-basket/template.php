<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


    <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
        $strMainID = $this->GetEditAreaId($arItem['ID']);
        $productImage = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width'=>125, 'height'=>125), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        ?>
        <?if (in_array($arItem['ID'], $arResult["WISHLIST"])):?>
            <?$wishlist = "delete";?>
        <?else:?>
            <?$wishlist = "add";?>
        <?endif;?>
        <div class="col-6 col-sm-4 col-md-2" id="<?= $strMainID ?>">
            <div class="product__similar">
                <div class="product__similar-container">
                    <div class="product__similar-image">
                        <img class="img" src="<?= $productImage["src"] ?>"/>
                    </div>
                    <div class="product__similar-title">
                        <?= $arItem["NAME"] ?>
                    </div>
                    <div class="product__similar-price">
                        <?
                        $arItem["MIN_PRICE"]["DISCOUNT_DIFF"] != 0 ? $priceStyle = 'product__similar-price-value product__similar-price-value--discount' : $priceStyle = 'product__similar-price-value';
                        ?>
                        <div class="<?=$priceStyle?>">
                            <?= $arItem["MIN_PRICE"]["PRINT_DISCOUNT_VALUE"]?>
                        </div>
                        <?php if ($arItem["MIN_PRICE"]["DISCOUNT_DIFF"] != 0) : ?>
                            <div class="product__similar-price-value product__similar-price-value--old"><?= $arItem["MIN_PRICE"]["PRINT_VALUE"] ?></div>
                        <?php elseif (!$arItem["MIN_PRICE"]["DISCOUNT_VALUE"]) : ?>
                            <div><?php echo "Нет в наличии"; ?></div>
                        <?php endif; ?>
                    </div>
                    <a class="product__similar-link" href="<?=$arItem["DETAIL_PAGE_URL"]?>"></a>
                </div>
            </div>
        </div>
    <? endforeach; ?>
