<? if(! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? //vard($arResult); ?>

<div class="preca-list">
	<div class="preca-items">
	<? foreach($arResult["ELEMENTS"] as $arItem) : ?>
			<a class="preca-items-item" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"   ><!--
				
				-->
				<div class="preca-items-flex">
				
					<div class="preca-items-picture-col ui-picture-col">
						
						<? //Picture ?>
						<? //print_r($arItem);?>
						<? if ($arPicture = $arItem["PICTURE--INFO"]["PREVIEW_PICTURE"] ? $arItem["PICTURE--INFO"]["PREVIEW_PICTURE"] : $arItem["PICTURE--INFO"]["PICTURE"]) : ?>
							<span class="preca-items-picture">
								<img src="<?= $arPicture["SRC"] ?>" width="<?= $arPicture["WIDTH"] ?>" height="<?= $arPicture["HEIGHT"] ?>" alt="">
							</span>
						<? else : ?>
							<span class="preca-items-picture ui-empty"></span>
						<? endif; ?>
						<? //-- ?>
					</div>
					
					<div class="preca-items-main-col ui-main-col">
						
						<div class="preca-items-title">
							<?= $arItem["NAME"] ?>
						</div>
						
						<? /* if ($arItem["PROPERTY_CML2_ARTICLE"]) : ?>
						<div class="preca-items-article">
							Артикул: 
							<div class="preca-items-article-value"><?= $arItem["PROPERTY_CML2_ARTICLE"] ?></div>
						</div>
						<? endif ; */ ?>
						
						<div class="preca-items-grow"></div>
						
						<? //Price ?>
						<? if ($arItem["MIN_PRICE"]["PRICE"] > 0) : ?>
							<span class="preca-items-price">
								<span class="ui-price" data-currency-code="<?= htmlspecialchars($arItem["MIN_PRICE"]["CURRENCY"]) ?>">
									<? if ($arItem["BASE_PRICE"]["PRICE"] > $arItem["MIN_PRICE"]["DISCOUNT_PRICE"]) : ?>
									<span class="ui-price-old-value"><span class="ui-text"><?= $arItem["BASE_PRICE"]["PRICE--HTML"] !== NULL ? $arItem["BASE_PRICE"]["PRICE--HTML"] : CCurrencyLang::CurrencyFormat($arItem["BASE_PRICE"]["PRICE"], $arItem["BASE_PRICE"]["CURRENCY"], false) ?></span></span>
									<? endif; ?>
									<span class="ui-price-value"><?= $arItem["MIN_PRICE"]["DISCOUNT_PRICE--HTML"] !== NULL ? $arItem["MIN_PRICE"]["DISCOUNT_PRICE--HTML"] : CCurrencyLang::CurrencyFormat($arItem["MIN_PRICE"]["DISCOUNT_PRICE"], $arItem["MIN_PRICE"]["CURRENCY"], false); ?></span>
									<span class="ui-price-currency">  <?= $arItem["MIN_PRICE"]["CURRENCY--HTML"] !== NULL ? $arItem["MIN_PRICE"]["CURRENCY--HTML"] : str_replace("0", "", CCurrencyLang::CurrencyFormat("0", $arItem["MIN_PRICE"]["CURRENCY"], true)); ?></span>
								</span>
							</span>
						<? endif; ?>
						<? // ?>
							
						<div class="preca-items-detail">
							<span href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="preca-items-detail-link ui-button--normal">Купить</span>
						</div>
							
				
					</div>

				</div>
				
			</a>
	<? endforeach; ?>

	<? for ($i = 1; $i <= 6; $i++)
	{
		?><div class="preca-items-item ui-flex-empty"></div><?
	}		
	?>	
	</div>	
</div>