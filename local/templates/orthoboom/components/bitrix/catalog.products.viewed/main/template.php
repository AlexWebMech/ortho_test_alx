<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 */

$this->setFrameMode(true);
//$this->addExternalCss('/bitrix/css/main/bootstrap.css');

$templateLibrary = ['popup'];
$currencyList = '';

if (!empty($arResult['CURRENCIES'])) {
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = [
    'TEMPLATE_THEME'   => $arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES'       => $currencyList
];
unset($currencyList, $templateLibrary);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = ['CONFIRM' => GetMessage('CT_CPV_TPL_ELEMENT_DELETE_CONFIRM')];

$positionClassMap = [
    'left'   => 'product-item-label-left',
    'center' => 'product-item-label-center',
    'right'  => 'product-item-label-right',
    'bottom' => 'product-item-label-bottom',
    'middle' => 'product-item-label-middle',
    'top'    => 'product-item-label-top'
];

$discountPositionClass = '';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION'])) {
    foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos) {
        $discountPositionClass .= isset($positionClassMap[$pos]) ? ' ' . $positionClassMap[$pos] : '';
    }
}

$labelPositionClass = '';
if (!empty($arParams['LABEL_PROP_POSITION'])) {
    foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos) {
        $labelPositionClass .= isset($positionClassMap[$pos]) ? ' ' . $positionClassMap[$pos] : '';
    }
}

$arParams['~MESS_BTN_BUY'] = $arParams['~MESS_BTN_BUY'] ?: Loc::getMessage('CT_CPV_TPL_MESS_BTN_BUY');
$arParams['~MESS_BTN_DETAIL'] = $arParams['~MESS_BTN_DETAIL'] ?: Loc::getMessage('CT_CPV_TPL_MESS_BTN_DETAIL');
$arParams['~MESS_BTN_COMPARE'] = $arParams['~MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_CPV_TPL_MESS_BTN_COMPARE');
$arParams['~MESS_BTN_SUBSCRIBE'] = $arParams['~MESS_BTN_SUBSCRIBE'] ?: Loc::getMessage('CT_CPV_TPL_MESS_BTN_SUBSCRIBE');
$arParams['~MESS_BTN_ADD_TO_BASKET'] = $arParams['~MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_CPV_TPL_MESS_BTN_ADD_TO_BASKET');
$arParams['~MESS_NOT_AVAILABLE'] = $arParams['~MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_CPV_TPL_MESS_PRODUCT_NOT_AVAILABLE');
$arParams['~MESS_SHOW_MAX_QUANTITY'] = $arParams['~MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_CPV_CATALOG_SHOW_MAX_QUANTITY');
$arParams['~MESS_RELATIVE_QUANTITY_MANY'] = $arParams['~MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_CPV_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['~MESS_RELATIVE_QUANTITY_FEW'] = $arParams['~MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_CPV_CATALOG_RELATIVE_QUANTITY_FEW');

$generalParams = [
    'SHOW_DISCOUNT_PERCENT'        => $arParams['SHOW_DISCOUNT_PERCENT'],
    'PRODUCT_DISPLAY_MODE'         => $arParams['PRODUCT_DISPLAY_MODE'],
    'SHOW_MAX_QUANTITY'            => $arParams['SHOW_MAX_QUANTITY'],
    'RELATIVE_QUANTITY_FACTOR'     => $arParams['RELATIVE_QUANTITY_FACTOR'],
    'MESS_SHOW_MAX_QUANTITY'       => $arParams['~MESS_SHOW_MAX_QUANTITY'],
    'MESS_RELATIVE_QUANTITY_MANY'  => $arParams['~MESS_RELATIVE_QUANTITY_MANY'],
    'MESS_RELATIVE_QUANTITY_FEW'   => $arParams['~MESS_RELATIVE_QUANTITY_FEW'],
    'SHOW_OLD_PRICE'               => $arParams['SHOW_OLD_PRICE'],
    'USE_PRODUCT_QUANTITY'         => $arParams['USE_PRODUCT_QUANTITY'],
    'PRODUCT_QUANTITY_VARIABLE'    => $arParams['PRODUCT_QUANTITY_VARIABLE'],
    'ADD_TO_BASKET_ACTION'         => $arParams['ADD_TO_BASKET_ACTION'],
    'ADD_PROPERTIES_TO_BASKET'     => $arParams['ADD_PROPERTIES_TO_BASKET'],
    'PRODUCT_PROPS_VARIABLE'       => $arParams['PRODUCT_PROPS_VARIABLE'],
    'SHOW_CLOSE_POPUP'             => $arParams['SHOW_CLOSE_POPUP'],
    'DISPLAY_COMPARE'              => $arParams['DISPLAY_COMPARE'],
    'COMPARE_PATH'                 => $arParams['COMPARE_PATH'],
    'COMPARE_NAME'                 => $arParams['COMPARE_NAME'],
    'PRODUCT_SUBSCRIPTION'         => $arParams['PRODUCT_SUBSCRIPTION'],
    'PRODUCT_BLOCKS_ORDER'         => $arParams['PRODUCT_BLOCKS_ORDER'],
    'LABEL_POSITION_CLASS'         => $labelPositionClass,
    'DISCOUNT_POSITION_CLASS'      => $discountPositionClass,
    'SLIDER_INTERVAL'              => $arParams['SLIDER_INTERVAL'],
    'SLIDER_PROGRESS'              => $arParams['SLIDER_PROGRESS'],
    '~BASKET_URL'                  => $arParams['~BASKET_URL'],
    '~ADD_URL_TEMPLATE'            => $arResult['~ADD_URL_TEMPLATE'],
    '~BUY_URL_TEMPLATE'            => $arResult['~BUY_URL_TEMPLATE'],
    '~COMPARE_URL_TEMPLATE'        => $arResult['~COMPARE_URL_TEMPLATE'],
    '~COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
    'TEMPLATE_THEME'               => $arParams['TEMPLATE_THEME'],
    'USE_ENHANCED_ECOMMERCE'       => $arParams['USE_ENHANCED_ECOMMERCE'],
    'DATA_LAYER_NAME'              => $arParams['DATA_LAYER_NAME'],
    'MESS_BTN_BUY'                 => $arParams['~MESS_BTN_BUY'],
    'MESS_BTN_DETAIL'              => $arParams['~MESS_BTN_DETAIL'],
    'MESS_BTN_COMPARE'             => $arParams['~MESS_BTN_COMPARE'],
    'MESS_BTN_SUBSCRIBE'           => $arParams['~MESS_BTN_SUBSCRIBE'],
    'MESS_BTN_ADD_TO_BASKET'       => $arParams['~MESS_BTN_ADD_TO_BASKET'],
    'MESS_NOT_AVAILABLE'           => $arParams['~MESS_NOT_AVAILABLE']
];

$obName = 'ob' . preg_replace('/[^a-zA-Z0-9_]/', 'x', $this->GetEditAreaId($this->randString()));
$containerName = 'catalog-products-viewed-container';

?>
<? if (count($arResult['ITEMS']) > 0 && !empty($arResult['ITEMS'])) { ?>
    <div class="seo-title slider-introduce">ВЫ СМОТРЕЛИ</div>
    <div class="row d-block">
        <div class="product-slider overflow-holder">
            <? foreach ($arResult['ITEMS'] as $key => $arItem) { ?>
                <div>
                    <a class="goods-item" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                        <div class="goods-img mb-4"><img class="img"
                                                         src="<?= SITE_TEMPLATE_PATH ?>/images/catalog/product.jpg">
                            <? if ($arItem["PROPERTIES"]["NEWS"]["VALUE"]) { ?>
                                <div class="goods-label">новинка</div>
                            <? } ?>
                        </div>
                        <div class="goods-article some-text text-center mb-2">артикул:
                            <?= $arItem["PROPERTIES"]["CML2_ARTICLE"]["VALUE"] ?></div>
                        <div class="goods-title small-title text-center mb-3 masonry-grid__link"
                            title="<?= $arItem["NAME"] ?>">
                            <?= $arItem["NAME"] ?>
                        </div>
                        <div class="goods-bottom d-flex flewx-warp justify-content-between align-items-center">
                            <button class="btn-favourite circle-btn btn-initial"><span class="icon-favourite"></span>
                            </button>
                            <div class="goods-price__holder">
                                <div class="new-price mb-1"><?= $arItem["MIN_PRICE"]["PRINT_RATIO_PRICE"] ?></div>
                                <? if ($arItem["MIN_PRICE"]["PRINT_RATIO_PRICE"] != $arItem["MIN_PRICE"]["PRINT_BASE_PRICE"]): ?>
                                    <div class="old-price"><?= $arItem["MIN_PRICE"]["PRINT_BASE_PRICE"] ?></div>
                                <? endif; ?>
                            </div>
                            <div class="circle-btn btn-next"><span class="icon-bold_arrow-right"></span></div>
                        </div>
                        <div class="goods-infoblock">
                            <div class="goods-info goods-info__highlighted">
                                <div class="small-title">размеры в наличии:</div>
                                <div class="text-center"><?= $arItem["RAZMERA"] ?>.</div>
                            </div>
                            <div class="goods-info text-center">Срок доставки от 2
                                до 7 дней
                            </div>
                        </div>
                    </a>
                </div>
            <? } ?>
        </div>
    </div>
<? } ?>