<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) {
    die();
}
foreach ($arResult["ITEMS"] as $key => &$item) {
    $item['MIN_PRICE'] = false;
    $item['MIN_BASIS_PRICE'] = false;
    foreach ($item['OFFERS'] as $keyOffer => &$arOffer)
    {
        if (empty($item['MIN_PRICE']) && $arOffer['CAN_BUY'])
        {
            $intSelected = $keyOffer;
            $item['MIN_PRICE'] = (isset($arOffer['ITEM_PRICES'][0]['RATIO_PRICE']) ? $arOffer['ITEM_PRICES'][0] : $arOffer['MIN_PRICE']);
            $item['MIN_BASIS_PRICE'] = $arOffer['ITEM_PRICES'][0];
        }
        if ($arOffer["PROPERTIES"]["OBUV_RAZMER"]["VALUE"]) {
            $size = str_replace("р.", "", $arOffer["PROPERTIES"]["OBUV_RAZMER"]["VALUE"]);
            $item["PROPERTIES"]["OBUV_RAZMER"]["VALUE"][$size] = $size;
        }
    }
    ksort($item["PROPERTIES"]["OBUV_RAZMER"]["VALUE"]);
    $item["RAZMERA"] = implode(", ", $item["PROPERTIES"]["OBUV_RAZMER"]["VALUE"]);
}
