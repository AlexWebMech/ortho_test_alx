<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

CModule::IncludeModule("iblock");
$arResult["COMPANY_DATA"] = array();
$arResult["AGENTS"] = array();

$deleteID = intval($_REQUEST["delete"]);
if ($deleteID > 0) {
	$res = CIBlockElement::GetList(Array("CREATE_DATE"=>"ASC"), array("ID"=>$deleteID, "PROPERTY_USER"=>$USER->GetID()), false, false, $arSelect);
	if ($ob = $res->GetNextElement())
	{
		CIBlockElement::Delete($deleteID);
		$arResult["DELETED_COMPANY"] = true;
	}
}


if (isset($_REQUEST["save"])) {
	$fieldEmpty = "Не заполнено обязательное поле";

	if ($arResult["arUser"]["NAME"] == "")
		$arResult["ERRORS"]["NAME"] = $fieldEmpty;

	if ($arResult["arUser"]["LAST_NAME"] == "")
		$arResult["ERRORS"]["LAST_NAME"] = $fieldEmpty;

	if ($arResult["arUser"]["PERSONAL_PHONE"] == "")
		$arResult["ERRORS"]["PERSONAL_PHONE"] = $fieldEmpty;
	else {
		$phoneClear = preg_replace('~\D+~', '', $arResult["arUser"]["PERSONAL_PHONE"]);
		if (strlen($phoneClear) != 11) {
			$arResult["ERRORS"]["PERSONAL_PHONE"] = "Телефон введен неверно";
		}
	}

	if ($arResult["arUser"]["EMAIL"] == "")
		$arResult["ERRORS"]["EMAIL"] = $fieldEmpty;

	if ($arResult["arUser"]["PERSONAL_CITY"] == "")
		$arResult["ERRORS"]["PERSONAL_CITY"] = $fieldEmpty;

	if ($arResult["arUser"]["PERSONAL_STREET"] == "")
		$arResult["ERRORS"]["PERSONAL_STREET"] = $fieldEmpty;


	if (isset($_REQUEST["OLD_PASSWORD"]) && $_REQUEST["OLD_PASSWORD"] != "" && $arResult['DATA_SAVED'] != 'Y') {
		$arUser["PASSWORD"] = $USER->GetParam("PASSWORD_HASH");
		$arParams["PASSWORD"] = htmlspecialcharsbx(trim($_REQUEST["OLD_PASSWORD"]));

		if(strlen($arUser["PASSWORD"]) > 32)
		{
		    $salt = substr($arUser["PASSWORD"], 0, strlen($arUser["PASSWORD"]) - 32);
		    $db_password = substr($arUser["PASSWORD"], -32);
		}
		else
		{
		    $salt = "";
		    $db_password = $arUser["PASSWORD"];
		}


		$user_password =  md5($salt.$arParams["PASSWORD"]);
		if ($db_password != $user_password) {
			$arResult["ERRORS"]["OLD_PASSWORD"] = "Текущий пароль введен неверно";
		} else if ($arResult["arUser"]["NEW_PASSWORD"] != $arResult["arUser"]["NEW_PASSWORD_CONFIRM"]) {
			$arResult["ERRORS"]["PASSWORD"] = "Пароли не совпадают";
		} else if (strlen($arResult["arUser"]["NEW_PASSWORD"]) < 6) {
			$arResult["ERRORS"]["PASSWORD"] = "Пароль должеть быть не менее 6 символов";
		}
	} else {
		if ($arResult["arUser"]["NEW_PASSWORD"] != "" || $arResult["arUser"]["NEW_PASSWORD_CONFIRM"] != "") {
			$arResult["ERRORS"]["OLD_PASSWORD"] = "Для смены пароля нужно указать текущий пароль";
		}
	}

	foreach ($_REQUEST["COMPANY_DATA"] as $key => $arCompany) {
		foreach ($arCompany as $k => $v) {
			$arResult["COMPANY_DATA"][$key][$k] = $v;
		}
	}

	foreach ($_REQUEST["AGENTS"] as $key => $arAgents) {
		foreach ($arAgents as $k => $v) {
			if ($v == "")
				continue;

			$arResult["AGENTS"][$k][$key] = $v;
		}
	}
// echo '<pre>';print_r($_REQUEST["AGENTS"]);echo '</pre>';
// echo '<pre>';print_r($arResult["AGENTS"]);echo '</pre>';

	if (isset($_REQUEST["birthdate_day_head"]) && isset($_REQUEST["birthdate_month_head"]) && isset($_REQUEST["birthdate_year_head"])) {
		$birthdate = date("d.m.Y", strtotime($_REQUEST["birthdate_day_head"] . "." . $_REQUEST["birthdate_month_head"] . "." . $_REQUEST["birthdate_year_head"]));

		$arResult["COMPANY_DATA"][0]["PROPERTY_DATE_BIRTH_HEAD_VALUE"] = $birthdate;
	}

	if (isset($_REQUEST["birthdate_day"]) && isset($_REQUEST["birthdate_month"]) && isset($_REQUEST["birthdate_year"])) {
		$birthdate = date("d.m.Y", strtotime($_REQUEST["birthdate_day"] . "." . $_REQUEST["birthdate_month"] . "." . $_REQUEST["birthdate_year"]));

		$arResult["arUser"]["PERSONAL_BIRTHDAY"] = $birthdate;
	}


} else if (!isset($_REQUEST["add"])) {
	$arSelect = Array(
		"ID", 
		"NAME", 
		"PROPERTY_NAME_HEAD",
		"PROPERTY_LAST_NAME_HEAD",
		"PROPERTY_PHONE_HEAD",
		"PROPERTY_EMAIL_HEAD",
		"PROPERTY_DATE_BIRTH_HEAD",
		"PROPERTY_LEGAL_ADDRESS",
		"PROPERTY_ACTUAL_ADDRESS",
		"PROPERTY_OGRN",
		"PROPERTY_INN",
		"PROPERTY_KPP",
		"PROPERTY_BANK_NAME",
		"PROPERTY_RASCH_SCHET",
		"PROPERTY_BANK_ADDRESS",
		"PROPERTY_BANK_BIK",
		"PROPERTY_KORR_SCHET",
		"PROPERTY_I_AM_HEAD",
		"PROPERTY_CITY",
		"PROPERTY_ADDRESS"
	);
	$arFilter = Array("IBLOCK_ID"=>49, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_USER"=>$USER->GetID());
	$edit = intval($_REQUEST["edit"]);
	if ($edit > 0) {
		$arFilter["ID"] = $edit;
	}
	$res = CIBlockElement::GetList(Array("CREATE_DATE"=>"ASC"), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$db_props = CIBlockElement::GetProperty(49, $arFields["ID"], array("sort" => "asc"), Array("CODE"=>"ADDRESSES"));
		while ($ar_props = $db_props->Fetch()) {
			if ($ar_props["VALUE"] == "")
				continue; 
			$arFields["PROPERTY_ADDRESSES_VALUE"][] = $ar_props["VALUE"];
			$arFields["SET_MAIN_ADDRESS"][] = $ar_props["DESCRIPTION"];
		}

		$arResult["COMPANY_DATA"][] = $arFields;
	}

	$arAgents = array();
	$db_props = CIBlockElement::GetProperty(49, $arFields["ID"], array("sort" => "asc"), Array("CODE"=>"AGENTS"));
	while ($ar_props = $db_props->Fetch()) {
		if (intval($ar_props["VALUE"]) == 0)
			continue;
		
		$arAgents[] = $ar_props["VALUE"];
	}

	if (count($arAgents) > 0) {
		$arFilter = Array("IBLOCK_ID"=>48, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID"=>$arAgents);
		$res = CIBlockElement::GetList(Array("CREATE_DATE"=>"ASC"), $arFilter, false, false, array("ID", "PROPERTY_NAME", "PROPERTY_LAST_NAME", "PROPERTY_POSITION", "PROPERTY_PHONE"));
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$arResult["AGENTS"][] = $arFields;
		}
	}
}

$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();
//echo "<pre>"; print_r($arUser); echo "</pre>";

$arResult["PHONES"] = array();
if (isset($_REQUEST['PHONE'])) {
	foreach ($_REQUEST['PHONE'] as $phone) {
		if (empty($phone))
			continue;

		$arResult["PHONES"][] = htmlspecialcharsbx($phone);
	}
} else {
	$arResult["PHONES"] = $arUser["UF_PHONES"];
}

$arResult["EMAILS"] = array();
if (isset($_REQUEST['EMAILS'])) {
	foreach ($_REQUEST['EMAILS'] as $email) {
		if (empty($email))
			continue;
		
		$arResult["EMAILS"][] = htmlspecialcharsbx($email);
	}
} else {
	$arResult["EMAILS"] = $arUser["UF_EMAILS"];
}

$el = new CIBlockElement;
// Добавление ЮЛ
if (isset($_REQUEST["add"]) && isset($_REQUEST["save"])) {
	//echo "<pre>"; print_r($arResult["COMPANY_DATA"][0]); echo "</pre>";

	if (
		$arResult["COMPANY_DATA"][0]["NAME"] != "" &&
		//$arResult["COMPANY_DATA"][0]["PROPERTY_CITY_VALUE"] != "" &&
		$arResult["COMPANY_DATA"][0]["PROPERTY_LEGAL_ADDRESS_VALUE"] != "" &&
		$arResult["COMPANY_DATA"][0]["PROPERTY_ACTUAL_ADDRESS_VALUE"] != "" &&
		//$arResult["COMPANY_DATA"][0]["PROPERTY_OGRN_VALUE"] != "" &&
		$arResult["COMPANY_DATA"][0]["PROPERTY_INN_VALUE"] != "" &&
		//$arResult["COMPANY_DATA"][0]["PROPERTY_KPP_VALUE"] != "" &&
		//$arResult["COMPANY_DATA"][0]["PROPERTY_BANK_NAME_VALUE"] != "" &&
		$arResult["COMPANY_DATA"][0]["PROPERTY_RASCH_SCHET_VALUE"] != "" &&
		$arResult["COMPANY_DATA"][0]["PROPERTY_BANK_ADDRESS_VALUE"] != "" &&
		$arResult["COMPANY_DATA"][0]["PROPERTY_BANK_BIK_VALUE"] != "" &&
		$arResult["COMPANY_DATA"][0]["PROPERTY_KORR_SCHET_VALUE"] != ""
		//$arResult["COMPANY_DATA"][0]["PROPERTY_ADDRESS_VALUE"] != ""
	) {
		$PROP = array(
			"CITY" => $arResult["COMPANY_DATA"][0]["PROPERTY_CITY_VALUE"],
			"LEGAL_ADDRESS" => $arResult["COMPANY_DATA"][0]["PROPERTY_LEGAL_ADDRESS_VALUE"],
			"ACTUAL_ADDRESS" => $arResult["COMPANY_DATA"][0]["PROPERTY_ACTUAL_ADDRESS_VALUE"],
			"OGRN" => $arResult["COMPANY_DATA"][0]["PROPERTY_OGRN_VALUE"],
			"INN" => $arResult["COMPANY_DATA"][0]["PROPERTY_INN_VALUE"],
			"KPP" => $arResult["COMPANY_DATA"][0]["PROPERTY_KPP_VALUE"],
			"BANK_NAME" =>$arResult["COMPANY_DATA"][0]["PROPERTY_BANK_NAME_VALUE"],
			"RASCH_SCHET" => $arResult["COMPANY_DATA"][0]["PROPERTY_RASCH_SCHET_VALUE"],
			"BANK_ADDRESS" => $arResult["COMPANY_DATA"][0]["PROPERTY_BANK_ADDRESS_VALUE"],
			"BANK_BIK" => $arResult["COMPANY_DATA"][0]["PROPERTY_BANK_BIK_VALUE"],
			"KORR_SCHET" => $arResult["COMPANY_DATA"][0]["PROPERTY_KORR_SCHET_VALUE"],
			"ADDRESS" => $arResult["COMPANY_DATA"][0]["PROPERTY_ADDRESS_VALUE"],
			"USER" => $USER->GetID()
		);

		$arCompanyAdd = Array(
		  "IBLOCK_SECTION_ID" => false,
		  "IBLOCK_ID"      => 49,
		  "PROPERTY_VALUES"=> $PROP,
		  "NAME"           => $arResult["COMPANY_DATA"][0]["NAME"],
		  "ACTIVE"         => "Y"
		);

		if ($companyID = $el->Add($arCompanyAdd)) {
			if (count($arResult["COMPANY_DATA"][0]["PROPERTY_ADDRESSES_VALUE"]) > 0) {
				$arValues = array();
				foreach ($arResult["COMPANY_DATA"][0]["PROPERTY_ADDRESSES_VALUE"] as $k => $value) {
					$setMain = $arResult["COMPANY_DATA"][0]["SET_MAIN_ADDRESS"][$k];
					$arValues[] = array("VALUE"=>$value,"DESCRIPTION"=>$setMain);
				}

				CIBlockElement::SetPropertyValueCode($companyID, "ADDRESSES", $arValues); 
			} 

			$arResult["ADDED_COMPANY"] = "Y";
		}


	}
}

// Обновляем ЮЛ
if (isset($_REQUEST["edit"]) && isset($_REQUEST["save"])) {

	if (
		$arResult["COMPANY_DATA"][0]["NAME"] != "" &&
		//$arResult["COMPANY_DATA"][0]["PROPERTY_CITY_VALUE"] != "" &&
		$arResult["COMPANY_DATA"][0]["PROPERTY_LEGAL_ADDRESS_VALUE"] != "" &&
		$arResult["COMPANY_DATA"][0]["PROPERTY_ACTUAL_ADDRESS_VALUE"] != "" &&
		//$arResult["COMPANY_DATA"][0]["PROPERTY_OGRN_VALUE"] != "" &&
		$arResult["COMPANY_DATA"][0]["PROPERTY_INN_VALUE"] != "" &&
		//$arResult["COMPANY_DATA"][0]["PROPERTY_KPP_VALUE"] != "" &&
		//$arResult["COMPANY_DATA"][0]["PROPERTY_BANK_NAME_VALUE"] != "" &&
		$arResult["COMPANY_DATA"][0]["PROPERTY_RASCH_SCHET_VALUE"] != "" &&
		$arResult["COMPANY_DATA"][0]["PROPERTY_BANK_ADDRESS_VALUE"] != "" &&
		$arResult["COMPANY_DATA"][0]["PROPERTY_BANK_BIK_VALUE"] != "" &&
		$arResult["COMPANY_DATA"][0]["PROPERTY_KORR_SCHET_VALUE"] != ""
		//$arResult["COMPANY_DATA"][0]["PROPERTY_ADDRESS_VALUE"] != ""
	) {
		CIBlockElement::SetPropertyValueCode($arResult["COMPANY_DATA"][0]["ID"], "CITY", $arResult["COMPANY_DATA"][0]["PROPERTY_CITY_VALUE"]);
		CIBlockElement::SetPropertyValueCode($arResult["COMPANY_DATA"][0]["ID"], "LEGAL_ADDRESS", $arResult["COMPANY_DATA"][0]["PROPERTY_LEGAL_ADDRESS_VALUE"]);
		CIBlockElement::SetPropertyValueCode($arResult["COMPANY_DATA"][0]["ID"], "ACTUAL_ADDRESS", $arResult["COMPANY_DATA"][0]["PROPERTY_ACTUAL_ADDRESS_VALUE"]);
		CIBlockElement::SetPropertyValueCode($arResult["COMPANY_DATA"][0]["ID"], "OGRN", $arResult["COMPANY_DATA"][0]["PROPERTY_OGRN_VALUE"]);
		CIBlockElement::SetPropertyValueCode($arResult["COMPANY_DATA"][0]["ID"], "INN", $arResult["COMPANY_DATA"][0]["PROPERTY_INN_VALUE"]);
		CIBlockElement::SetPropertyValueCode($arResult["COMPANY_DATA"][0]["ID"], "KPP", $arResult["COMPANY_DATA"][0]["PROPERTY_KPP_VALUE"]);
		CIBlockElement::SetPropertyValueCode($arResult["COMPANY_DATA"][0]["ID"], "BANK_NAME", $arResult["COMPANY_DATA"][0]["PROPERTY_BANK_NAME_VALUE"]);
		CIBlockElement::SetPropertyValueCode($arResult["COMPANY_DATA"][0]["ID"], "RASCH_SCHET", $arResult["COMPANY_DATA"][0]["PROPERTY_RASCH_SCHET_VALUE"]);
		CIBlockElement::SetPropertyValueCode($arResult["COMPANY_DATA"][0]["ID"], "BANK_ADDRESS", $arResult["COMPANY_DATA"][0]["PROPERTY_BANK_ADDRESS_VALUE"]);
		CIBlockElement::SetPropertyValueCode($arResult["COMPANY_DATA"][0]["ID"], "BANK_BIK", $arResult["COMPANY_DATA"][0]["PROPERTY_BANK_BIK_VALUE"]);
		CIBlockElement::SetPropertyValueCode($arResult["COMPANY_DATA"][0]["ID"], "KORR_SCHET", $arResult["COMPANY_DATA"][0]["PROPERTY_KORR_SCHET_VALUE"]);
		CIBlockElement::SetPropertyValueCode($arResult["COMPANY_DATA"][0]["ID"], "ADDRESS", $arResult["COMPANY_DATA"][0]["PROPERTY_ADDRESS_VALUE"]);

		$arCompanyUpdate = Array(
			"NAME"  => $arResult["COMPANY_DATA"][0]["NAME"]
		);
		$res = $el->Update($arResult["COMPANY_DATA"][0]["ID"], $arCompanyUpdate);

		if (count($arResult["COMPANY_DATA"][0]["PROPERTY_ADDRESSES_VALUE"]) > 0) {
			$arValues = array();
			foreach ($arResult["COMPANY_DATA"][0]["PROPERTY_ADDRESSES_VALUE"] as $k => $value) {
				$setMain = $arResult["COMPANY_DATA"][0]["SET_MAIN_ADDRESS"][$k];
				$arValues[] = array("VALUE"=>$value,"DESCRIPTION"=>$setMain);
			}

			CIBlockElement::SetPropertyValueCode($arResult["COMPANY_DATA"][0]["ID"], "ADDRESSES", $arValues); 
		} 

		$arResult["EDITED_COMPANY"] = "Y";
	}


}
?>