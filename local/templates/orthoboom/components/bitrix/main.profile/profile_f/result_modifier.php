<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (isset($_REQUEST["save"])) {
	$fieldEmpty = "Не заполнено обязательное поле";

	if ($arResult["arUser"]["NAME"] == "")
		$arResult["ERRORS"]["NAME"] = $fieldEmpty;

	if ($arResult["arUser"]["LAST_NAME"] == "")
		$arResult["ERRORS"]["LAST_NAME"] = $fieldEmpty;

	if ($arResult["arUser"]["PERSONAL_PHONE"] == "")
		$arResult["ERRORS"]["PERSONAL_PHONE"] = $fieldEmpty;

	if ($arResult["arUser"]["EMAIL"] == "")
		$arResult["ERRORS"]["EMAIL"] = $fieldEmpty;

	if ($arResult["arUser"]["PERSONAL_CITY"] == "")
		$arResult["ERRORS"]["PERSONAL_CITY"] = $fieldEmpty;

	if ($arResult["arUser"]["PERSONAL_STREET"] == "")
		$arResult["ERRORS"]["PERSONAL_STREET"] = $fieldEmpty;


	if (isset($_REQUEST["OLD_PASSWORD"]) && $_REQUEST["OLD_PASSWORD"] != "" && $arResult['DATA_SAVED'] != 'Y') {
		$arUser["PASSWORD"] = $USER->GetParam("PASSWORD_HASH");
		$arParams["PASSWORD"] = htmlspecialcharsbx(trim($_REQUEST["OLD_PASSWORD"]));

		if(strlen($arUser["PASSWORD"]) > 32)
		{
		    $salt = substr($arUser["PASSWORD"], 0, strlen($arUser["PASSWORD"]) - 32);
		    $db_password = substr($arUser["PASSWORD"], -32);
		}
		else
		{
		    $salt = "";
		    $db_password = $arUser["PASSWORD"];
		}


		$user_password =  md5($salt.$arParams["PASSWORD"]);
		if ($db_password != $user_password) {
			$arResult["ERRORS"]["OLD_PASSWORD"] = "Текущий пароль введен неверно";
		} else if ($arResult["arUser"]["NEW_PASSWORD"] != $arResult["arUser"]["NEW_PASSWORD_CONFIRM"]) {
			$arResult["ERRORS"]["PASSWORD"] = "Пароли не совпадают";
		} else if (strlen($arResult["arUser"]["NEW_PASSWORD"]) < 6) {
			$arResult["ERRORS"]["PASSWORD"] = "Пароль должеть быть не менее 6 символов";
		}
	} else {
		if ($arResult["arUser"]["NEW_PASSWORD"] != "" || $arResult["arUser"]["NEW_PASSWORD_CONFIRM"] != "") {
			$arResult["ERRORS"]["OLD_PASSWORD"] = "Для смены пароля нужно указать текущий пароль";
		}
	}
}

$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();
//echo "<pre>"; print_r($arUser); echo "</pre>";

$arResult["PHONES"] = array();
if (isset($_REQUEST['PHONE'])) {
	foreach ($_REQUEST['PHONE'] as $phone) {
		if (empty($phone))
			continue;

		$arResult["PHONES"][] = htmlspecialcharsbx($phone);
	}
} else {
	$arResult["PHONES"] = $arUser["UF_PHONES"];
}

$arResult["EMAILS"] = array();
if (isset($_REQUEST['EMAILS'])) {
	foreach ($_REQUEST['EMAILS'] as $email) {
		if (empty($email))
			continue;
		
		$arResult["EMAILS"][] = htmlspecialcharsbx($email);
	}
} else {
	$arResult["EMAILS"] = $arUser["UF_EMAILS"];
}
?>