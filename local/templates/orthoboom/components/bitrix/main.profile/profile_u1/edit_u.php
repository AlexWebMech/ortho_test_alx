
<?if (isset($arResult["EDITED_COMPANY"])):?>
	<div class="success-msg">
	<p><font class="notetext" style="color: green;">Юридическое лицо отредактировано.</font> <br><a href="/personal/profile/">&laquo; Вернуться в профиль</a></p></div>
<?endif;?>


<form action="<?=$APPLICATION->GetCurPageParam("edit=" . $arResult["COMPANY_DATA"][0]["ID"], array("add", "edit"))?>" method="POST">

	<input type="hidden" name="COMPANY_DATA[0][ID]" value="<?=$arResult["COMPANY_DATA"][0]["ID"]?>">
	<div class="user-profile">	
		<h2>Редактирование юридического лица</h2>										
			<div>
				<label for="">Название компании*</label><input type="text" name="COMPANY_DATA[0][NAME]" value="<?=$arResult["COMPANY_DATA"][0]["NAME"]?>"<?=($arResult["COMPANY_DATA"][0]["NAME"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>> 												
			</div>

			<?if ($arResult["COMPANY_DATA"][0]["NAME"] == "" && isset($_REQUEST["save"])):?>
				<p class="error-message">Обязательно для заполнения</p>
				<br><br>
			<?endif;?>		
			
			<div><label for="">Город*</label><input type="text" name="COMPANY_DATA[0][PROPERTY_CITY_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_CITY_VALUE"]?>"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_CITY_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>></div>
			<div>


			<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_CITY_VALUE"] == "" && isset($_REQUEST["save"])):?>
				<p class="error-message">Обязательно для заполнения</p>
				<br><br>
			<?endif;?>
				<label for="">Адреса*</label>
				<div class="addresses">
						<?
							$setMain = "Y";
							foreach($arResult["COMPANY_DATA"][0]["SET_MAIN_ADDRESS"] as $setValue){
								if ($setValue == "Y") {
									$setMain = "N";
									break;
								}
							}
						?>
						<div class="address-radio<?=($setMain == "Y") ? ' active' : '';?>">Сделать основным:</div>
						<textarea name="COMPANY_DATA[0][PROPERTY_ADDRESS_VALUE]" <?=($arResult["COMPANY_DATA"][0]["PROPERTY_ADDRESS_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>><?=$arResult["COMPANY_DATA"][0]["PROPERTY_ADDRESS_VALUE"]?></textarea>	<br><br>

						<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_ADDRESS_VALUE"] == "" && isset($_REQUEST["save"])):?>
							<p style="margin-left: 0;" class="error-message">Обязательно для заполнения</p>
							<br><br>
						<?endif;?>	

					<?if (count($arResult["COMPANY_DATA"][0]["PROPERTY_ADDRESSES_VALUE"]) > 0):?>
						<?foreach($arResult["COMPANY_DATA"][0]["PROPERTY_ADDRESSES_VALUE"] as $k => $addr):?>
							<div class="address-block"><div class="address-radio<?=($arResult["COMPANY_DATA"][0]["SET_MAIN_ADDRESS"][$k] == "Y") ? ' active' : '';?>">Сделать основным:
								<input type="hidden" name="COMPANY_DATA[0][SET_MAIN_ADDRESS][]" value="<?=$arResult["COMPANY_DATA"][0]["SET_MAIN_ADDRESS"][$k]?>">
							</div><div class="more-inputs"><textarea name="COMPANY_DATA[0][PROPERTY_ADDRESSES_VALUE][]"><?=$addr?></textarea><div class="remove"></div></div></div>
						<?endforeach;?>
					<?endif;?>											
				</div>
				<p class="address-add">+ <a href="#">Добавить еще один</a></p>							
				<div class="clearfix"></div><br> 
			</div>
			<?
				$legalAddress = trim(toLower($arResult["COMPANY_DATA"][0]["PROPERTY_LEGAL_ADDRESS_VALUE"]));
				$actualAddress = trim(toLower($arResult["COMPANY_DATA"][0]["PROPERTY_ACTUAL_ADDRESS_VALUE"]));

				$equalAddress = $legalAddress == $actualAddress && $legalAddress != "" && $actualAddress != "";
			?>
			<div class="address-toggler">
				<div>
					<label for="">Юридический адрес*</label><textarea data-address="legal" name="COMPANY_DATA[0][PROPERTY_LEGAL_ADDRESS_VALUE]"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_LEGAL_ADDRESS_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>><?=$arResult["COMPANY_DATA"][0]["PROPERTY_LEGAL_ADDRESS_VALUE"]?></textarea>	
					<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_LEGAL_ADDRESS_VALUE"] == "" && isset($_REQUEST["save"])):?>
						<p class="error-message">Обязательно для заполнения</p>
						<br><br>
					<?endif;?>	

					<p class="checkbox-add"><input type="checkbox" data-address="equal" name="" id="check_3" <?=($equalAddress) ? ' checked' : ''?>><label for="check_3">Юридический адрес совпадает с фактическим адресом</label></p>													
				</div>
		

				<div>
					<label for="">Фактический адрес*</label><textarea data-address="actual" name="COMPANY_DATA[0][PROPERTY_ACTUAL_ADDRESS_VALUE]"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_ACTUAL_ADDRESS_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>><?=$arResult["COMPANY_DATA"][0]["PROPERTY_ACTUAL_ADDRESS_VALUE"]?></textarea>			
				</div>	<br> 
				<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_ACTUAL_ADDRESS_VALUE"] == "" && isset($_REQUEST["save"])):?>
					<p class="error-message">Обязательно для заполнения</p>
					<br><br>
				<?endif;?>
			</div>

			<div><label for="">ОГРН*</label><input type="text" name="COMPANY_DATA[0][PROPERTY_OGRN_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_OGRN_VALUE"]?>"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_OGRN_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>> </div>

			<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_OGRN_VALUE"] == "" && isset($_REQUEST["save"])):?>
				<p class="error-message">Обязательно для заполнения</p>
				<br><br>
			<?endif;?>	

			<div><label for="">ИНН*</label><input type="text" name="COMPANY_DATA[0][PROPERTY_INN_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_INN_VALUE"]?>"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_INN_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>> </div>	

			<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_INN_VALUE"] == "" && isset($_REQUEST["save"])):?>
				<p class="error-message">Обязательно для заполнения</p>
				<br><br>
			<?endif;?>	

			<div><label for="">КПП*</label><input type="text" name="COMPANY_DATA[0][PROPERTY_KPP_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_KPP_VALUE"]?>"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_KPP_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>> </div>	

			<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_KPP_VALUE"] == "" && isset($_REQUEST["save"])):?>
				<p class="error-message">Обязательно для заполнения</p>
				<br><br>
			<?endif;?>

			<div><label for="">Наименование банка*</label><input type="text" name="COMPANY_DATA[0][PROPERTY_BANK_NAME_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_BANK_NAME_VALUE"]?>"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_BANK_NAME_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>> </div>	

			<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_BANK_NAME_VALUE"] == "" && isset($_REQUEST["save"])):?>
				<p class="error-message">Обязательно для заполнения</p>
				<br><br>
			<?endif;?>

			<div><label for="">Расчетный счет*</label><input type="text" name="COMPANY_DATA[0][PROPERTY_RASCH_SCHET_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_RASCH_SCHET_VALUE"]?>"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_RASCH_SCHET_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>> </div>	

			<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_RASCH_SCHET_VALUE"] == "" && isset($_REQUEST["save"])):?>
				<p class="error-message">Обязательно для заполнения</p>
				<br><br>
			<?endif;?>

			<div><label for="">Адрес банка*</label><textarea name="COMPANY_DATA[0][PROPERTY_BANK_ADDRESS_VALUE]"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_BANK_ADDRESS_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>><?=$arResult["COMPANY_DATA"][0]["PROPERTY_BANK_ADDRESS_VALUE"]?></textarea></div>

			<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_BANK_ADDRESS_VALUE"] == "" && isset($_REQUEST["save"])):?>
				<p class="error-message">Обязательно для заполнения</p>
				<br><br>
			<?endif;?>

			<div><label for="">БИК банка*</label><input type="text" name="COMPANY_DATA[0][PROPERTY_BANK_BIK_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_BANK_BIK_VALUE"]?>"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_BANK_BIK_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>> </div>	

			<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_BANK_BIK_VALUE"] == "" && isset($_REQUEST["save"])):?>
				<p class="error-message">Обязательно для заполнения</p>
				<br><br>
			<?endif;?>

			<div><label for="">Кор. счет банка*</label><input type="text" name="COMPANY_DATA[0][PROPERTY_KORR_SCHET_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_KORR_SCHET_VALUE"]?>"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_KORR_SCHET_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>> </div>

			<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_KORR_SCHET_VALUE"] == "" && isset($_REQUEST["save"])):?>
				<p class="error-message">Обязательно для заполнения</p>
				<br><br>
			<?endif;?>
	</div>
	<div class="form-bottom">	
		<div class="cancell-button"><a href="/profile/">Отменить</a></div> 
		<div class="login-button"><input type="submit" name="save" class="btn-submit" value="Сохранить изменения"></div> 	

	</div>	
</form>