function removeElement(arr, sElement)
{
	var tmp = new Array();
	for (var i = 0; i<arr.length; i++) if (arr[i] != sElement) tmp[tmp.length] = arr[i];
	arr=null;
	arr=new Array();
	for (var i = 0; i<tmp.length; i++) arr[i] = tmp[i];
	tmp = null;
	return arr;
}

function SectionClick(id)
{
	var div = document.getElementById('user_div_'+id);
	if (div.className == "profile-block-hidden")
	{
		opened_sections[opened_sections.length]=id;
	}
	else
	{
		opened_sections = removeElement(opened_sections, id);
	}

	document.cookie = cookie_prefix + "_user_profile_open=" + opened_sections.join(",") + "; expires=Thu, 31 Dec 2020 23:59:59 GMT; path=/;";
	div.className = div.className == 'profile-block-hidden' ? 'profile-block-shown' : 'profile-block-hidden';
}

$(function(){
	$("#check_2").on("click", function(){
		if ($(this).is(":checked")) {
			var name = $("[name=NAME]").val(),
			    lastName = $("[name=LAST_NAME]").val(),
			    phone = $("[name=PERSONAL_PHONE]").val(),
			    email = $("[name=EMAIL]").val(),
			    birthDay = $("[name=birthdate_day] option:selected").val(),
			    birthMonth = $("[name=birthdate_month] option:selected").val(),
			    birthYear = $("[name=birthdate_year] option:selected").val();

			$("[name='COMPANY_DATA[0][PROPERTY_NAME_HEAD_VALUE]']").val(name);
			$("[name='COMPANY_DATA[0][PROPERTY_LAST_NAME_HEAD_VALUE]']").val(lastName);
			$("[name='COMPANY_DATA[0][PROPERTY_PHONE_HEAD_VALUE]']").val(phone);
			$("[name='COMPANY_DATA[0][PROPERTY_EMAIL_HEAD_VALUE]']").val(email);

			$("[name=birthdate_day_head] option[value="+birthDay+"]").prop("selected", true).attr("selected","selected");
			$("[name=birthdate_month_head] option[value="+birthMonth+"]").prop("selected", true).attr("selected","selected");
			$("[name=birthdate_year_head] option[value="+birthYear+"]").prop("selected", true).attr("selected","selected");

			$("[name=birthdate_day_head], [name=birthdate_month_head], [name=birthdate_year_head]").removeClass("outtaHere")
																								   .prev(".selectArea").remove();

			$('select').customSelect();

		}
	});

	$("[name=NAME]").on("blur", function(){
		if ($("#check_2").is(":checked")) {
			$("[name='COMPANY_DATA[0][PROPERTY_NAME_HEAD_VALUE]']").val($(this).val());
		}
	});

	$("[name=LAST_NAME]").on("blur", function(){
		if ($("#check_2").is(":checked")) {
			$("[name='COMPANY_DATA[0][PROPERTY_LAST_NAME_HEAD_VALUE]']").val($(this).val());
		}
	});

	$("[name=PERSONAL_PHONE]").on("blur", function(){
		if ($("#check_2").is(":checked")) {
			$("[name='COMPANY_DATA[0][PROPERTY_PHONE_HEAD_VALUE]']").val($(this).val());
		}
	});

	$("[name=EMAIL]").on("blur", function(){
		if ($("#check_2").is(":checked")) {
			$("[name='COMPANY_DATA[0][PROPERTY_EMAIL_HEAD_VALUE]']").val($(this).val());
		}
	});

	$("[name=birthdate_day]").on("change", function(){
		if ($("#check_2").is(":checked")) {
			$("[name=birthdate_day_head] option[value="+$(this).val()+"]").prop("selected", true).attr("selected","selected");

			$("[name=birthdate_day_head]").removeClass("outtaHere")
										  .prev(".selectArea").remove();

			$('select').customSelect();
		}
	});

	$("[name=birthdate_month]").on("change", function(){
		if ($("#check_2").is(":checked")) {
			$("[name=birthdate_month_head] option[value="+$(this).val()+"]").prop("selected", true).attr("selected","selected");

			$("[name=birthdate_month_head]").removeClass("outtaHere")
										  .prev(".selectArea").remove();

			$('select').customSelect();
		}
	});

	$("[name=birthdate_year]").on("change", function(){
		if ($("#check_2").is(":checked")) {
			$("[name=birthdate_year_head] option[value="+$(this).val()+"]").prop("selected", true).attr("selected","selected");

			$("[name=birthdate_year_head]").removeClass("outtaHere")
										  .prev(".selectArea").remove();

			$('select').customSelect();
		}
	});

	$("body").on("blur", "[data-address=legal]", function(){
		var $wrap = $(this).parents(".address-toggler");
		var $check = $wrap.find("[type=checkbox]");
		if ($check.length == 1) {
			if ($check.is(":checked")) {
				$wrap.find("[data-address=actual]").val($(this).val());
			}
		}
	});

	$("body").on("click", "[data-address=equal]", function(){
		if ($(this).is(":checked")) {
			var $wrap = $(this).parents(".address-toggler");
			var legalAddress = $wrap.find("[data-address=legal]").val();
			$wrap.find("[data-address=actual]").val(legalAddress);
		}
	});

	$('.address-add a').click(function(){			
		$(this).parent().parent().find('.addresses').append('<div class="address-block"><div class="address-radio">пїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ:<input type="hidden" name="COMPANY_DATA[0][SET_MAIN_ADDRESS][]" value="N"></div><div class="more-inputs"><textarea name="COMPANY_DATA[0][PROPERTY_ADDRESSES_VALUE][]"></textarea><div class="remove"></div></div></div>');
		return false;		
	});		
	
	$(document).on('click','.address-radio',function(e) {
		$(this).parents(".addresses").find('.address-radio').removeClass('active');
		$(this).parents(".addresses").find('input[type=hidden]').val("N");
		$(this).addClass('active');		

		$(this).find('input[type=hidden]').val("Y");
	});
	
	
	$(document).on('click','.address-radio .remove',function(e) {	
		$(this).parent().parent().remove();
		return false; 
	});	

	$('.manager-add a').click(function(){	
		$card=$('.manager-card-example').html();
		$('.new-manager-cards').append($card);
		$('input[name*=PHONE]').mask('+7 (999) 999 99 99',{placeholder:" "});
		return false;
	});		
	
	$(document).on('click','.manager-card img',function(e) {
		$(this).parent().parent().remove();
		return false;
	});	
});

