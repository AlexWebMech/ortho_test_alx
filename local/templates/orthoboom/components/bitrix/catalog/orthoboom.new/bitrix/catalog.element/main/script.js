BX.ready(function(){
	var fixed_price = $('input[name=fixed_price]').val();
	
    $('a.js-add_cart-plus').on('click', function () {
        var choose_quantity = parseInt($('input[name=bootSize]:checked').attr("data-quantity"));
        var current_quantity = parseInt($('.cart__ammount-num.js-add_cart-quantity').text());
        if(current_quantity < choose_quantity) {
            $('.cart__ammount-num.js-add_cart-quantity').text(current_quantity+1);
		}
    });
    $('a.js-add_cart-minus').on('click', function () {
        var current_quantity = parseInt($('.cart__ammount-num.js-add_cart-quantity').text());
        if(current_quantity-1 > 0) {
            $('.cart__ammount-num.js-add_cart-quantity').text(current_quantity-1);
        }
    });
    $('input[name=bootSize]:checked').on('change', function () {
            $('.cart__ammount-num.js-add_cart-quantity').text(1);
    });


    var anchor = location.hash.slice(1);
    if (anchor)
        $('.razmer_input[data-id=' + anchor + ']').trigger('click');

    // $('input[name=size_buyoneclick]').val($('.razmer_input:checked').val());

    $('a[data-target="#buyOneClick"]').on('click', function (e) {
        $('input[name=amount_buyoneclick]').val($('.cart__ammount-num').text());
    });
    $('input[name=bootSize]').on('click', function (e) {
        var choose = parseInt($(this).attr("data-id"));

        $('input[name=size_buyoneclick]').val($(this).val());
        $('input[name=current_product_id]').val(choose);
		if (!fixed_price) {
			$('.quantity-cost-price').text(ofrs_prices[choose]['PRINT_DISCOUNT_VALUE']);
		}
        $('.quantity-cost-oldPrice').text(ofrs_prices[choose]['PRINT_VALUE']);
    });
    $('.razmer_input:first').click();

    $('.btn-grouph.d-flex.w-100.flex-wrap.justify-content-between button').on('click', function (e) {
    	e.preventDefault();
    	if(!$(this).hasClass( "noclick" )){
			var choose = parseInt($('input[name=bootSize]:checked').attr("data-id"));
			var current_quantity_for_ajax = parseInt($('.cart__ammount-num.js-add_cart-quantity').text());
			var product_name = $('input[name=product_name]').val() +  ' (' + $('input[name=bootSize]:checked').val() + ')';
			if(choose){
				//$(this).toggleClass( "noclick" );
				$.ajax({
					method: "POST",
					url: path_to_ajax,
					data: { id: choose, quantity: current_quantity_for_ajax, fixed_price: fixed_price, product_name: product_name }
				})
                    .done(function( data ) {
						console.log(data);
                        //data = JSON.parse(data);
                        var inCartClick = $('#inCartClick');

                        if(data.error || !data){
                            if(data.error.indexOf('": ', 0)){
                                var error = inCartClick.find('.modal-title').attr('data-error_kol');
                                var closeModalText = inCartClick.find('.closeModal').attr('data-error_kol');
                                inCartClick.addClass('error');
                                inCartClick.find('.modal-title').text(error);
                                inCartClick.find('.closeModal').text(closeModalText);
                                inCartClick.find('.js-hidden').removeClass('js-hidden');
                                inCartClick.modal();
                            }
                            else{
                                alert(data.error);
                            }

                            $(this).toggleClass( "noclick" );
						}
                        else{
                            inCartClick.modal();
                            $('.cart-icon span').text(data.CART);
                            //location.href = path_to_basket;
						}
					});
			}
        }
    });
    $( "#NEW_REVIEW" ).submit(function( event ) {
        event.preventDefault();
        alert( "Handler for .submit() called." );
        var form = $('#NEW_REVIEW');
        $.ajax( {
            type: "POST",
            url: path_to_add_review,
            data: form.serialize(),
            success: function( response ) {
                console.log( response );
                if(response == "OK"){
                	location.href = location.href;
				}
            }
        });

    });
    var loading;
    $('body').on('click', '.paggination__item', function (e) {
        e.preventDefault();
        if (!loading) {
            loading = true;
            $.post($(this).attr('href'), {is_ajax: 'y'}, function (data) {
                $('body').find('#ajax_rewiew').html("");
                var result = data.split("<!--RestartBufferPagination-->");
                $('body').find('#ajax_rewiew').append(result[1]);
                $('.rating-stat').each(function() {
                    $(this).barrating({
                        readonly: true,
                        initialRating: $(this).data('rate'),
                        theme: 'fontawesome-stars-o'
                    });
                });
                toggleText();
                loading = false;
            });
        }
    });
    $(document).on('click', '.closeModal', function(){
        $('.modal').modal('hide');
    });

    /*Кастомные функции для валидатора телефона, так как маска мешает*/
    $.validator.addMethod("minLengthPhone", function(value, element) {
        return value.replace(/\D+/g, '').length > 10;
    }, "Телефон: минимум 10 символов");

    $.validator.addMethod("requiredphone", function (value, element) {
            return value.replace(/\D+/g, '').length > 1;
    }, "Телефон: поле обязательно для заполнения");

    $('form#buyOneClickForm').validate(
    {
        rules:
        {
            USER_PHONE:
            {
                requiredphone: true,
                minLengthPhone: true,
            }
        },
        errorPlacement: function(error, element) {
            error.appendTo('#error-phone-block');
        },
        submitHandler: function (form) {
            $(form).removeClass('show').css('display', 'none');
            $('#thanksForOrder').addClass('show').css('display', 'block');
            setTimeout(function () {
                form.submit();
            }, 1000);
        }
    });
});