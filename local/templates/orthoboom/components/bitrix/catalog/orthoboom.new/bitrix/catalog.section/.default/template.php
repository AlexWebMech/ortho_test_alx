<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>    <div class="row mb-4 catalog__list">

    <?if(count($arResult["ITEMS"]) > 0): ?>


        <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
            $strMainID = $this->GetEditAreaId($arItem['ID']);
            ?>
            <?if (in_array($arItem['ID'], $arResult["WISHLIST"])):?>
                <?$wishlist = "delete";?>
            <?else:?>
                <?$wishlist = "add";?>
            <?endif;?>
            <div class="col-sm-6 col-md-4 mb-4 catalog__item">
                <a class="goods-item" title="<?= $arItem["NAME"] ?>" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" id="<?= $strMainID ?>" >
                    <?if ($arItem["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"]):?>
                        <div class="price-discount-percent" style="right:5px;">- <?=$arItem["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"]?>%</div>
                    <?endif?>
                    <div class="goods-img mb-4">
                        <img class="img" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"/>
                        <div class="goods-label-block">
                            <?php if ($arItem["PROPERTIES"]["NOVINKA_INTERNET_MAGAZIN"]["VALUE"] == "Да") : ?>
                                <div class="goods-label">новинка</div>
                            <?php endif; ?>
                            <?php if ($arItem["PROPERTIES"]["KHIT_PRODAZH_INTERNET_MAGAZIN"]["VALUE"] == "Да") : ?>
                                <div class="goods-label-hit">хит</div>
                            <?php endif; ?>
                            <?php if ($arItem["PROPERTIES"]["TREND_INTERNET_MAGAZIN"]["VALUE"] == "Да") : ?>
                                <div class="goods-label-trend">тренд</div>
                            <?php endif; ?>
                            <?php if ($arItem["PROPERTIES"]["FIKSIROVANNAYA_TSENA_ZNACHENIE"]["VALUE"]) : ?>
                                <div class="goods-label-fixprice">фиксированная цена</div>
                        <?php endif; ?>
                        </div>
                    </div>
                    <div class="goods-article some-text text-center mb-2">артикул:
                        <?= $arItem["PROPERTIES"]["CML2_ARTICLE"]["VALUE"] ?></div>
                    <div class="goods-title small-title text-center mb-3 masonry-grid__link"
                        title="<?= $arItem["NAME"] ?>">
                        <?= $arItem["NAME"] ?>
                    </div>
                    <div class="goods-bottom d-flex flewx-warp justify-content-between align-items-center">
                        <button id="id<?=$arItem["ID"]?>" class="btn-favourite circle-btn btn-initial add2wish add2wish-normal
                            <?if ($wishlist == "delete"):?>active<?endif;?>" onclick="
                        <?=($USER->IsAuthorized()) ? 'WishList.'.$wishlist.'('.$arItem["ID"].');' :
                            'WishList.showUnregister();';?>ym(25218083, 'reachGoal', 'favorite_add');return false;">
                                <span class="icon-favourite">
                                </span>
                        </button>
                        <div class="goods-price__holder">
                            <?
                            $arItem["MIN_PRICE"]["DISCOUNT_DIFF"] != 0 ? $priceStyle = 'new-price-if-old-exists' : $priceStyle = 'new-price';
                            ?>
                            <div class="<?=$priceStyle?> mb-1">
                                <?= round($arItem["MIN_PRICE"]["DISCOUNT_VALUE"])?> руб.
                            </div>
                            <?php if ($arItem["MIN_PRICE"]["DISCOUNT_DIFF"] != 0) : ?>
                                <div class="old-price"><?= $arItem["MIN_PRICE"]["PRINT_VALUE"] ?></div>
                            <?php elseif (!$arItem["MIN_PRICE"]["DISCOUNT_VALUE"]) : ?>
                                <div><?php echo "Нет в наличии"; ?></div>
                            <?php endif; ?>
                        </div>
                        <div class="circle-btn btn-next">
                            <span class="icon-bold_arrow-right"></span>
                        </div>
                    </div>
                    <div class="goods-infoblock">
                        <div class="goods-info goods-info__highlighted">
                            <? if ($arItem["PROPERTIES"]["OBUV_RAZMER"]["VALUE"]): ?>
                                <div class="small-title">
                                    размеры в наличии:
                                </div>
                                <div class="text-center">
                                    <? $i = 0; ?>
                                    <? $count = count($arItem["PROPERTIES"]["OBUV_RAZMER"]["VALUE"]); ?>
                                    <? foreach ($arItem["PROPERTIES"]["OBUV_RAZMER"]["VALUE"] as $val): ?>
                                        <? $i++; ?>
                                        <? if ($count == $i): ?>
                                            <?= $val ?>.
                                        <? else: ?>
                                            <?= $val ?>,
                                        <? endif; ?>
                                    <? endforeach; ?>
                                </div>
                            <? endif; ?>
                        </div>
                        <div class="goods-info text-center">
                           Срок доставки от 2 до 7 дней
                        </div>
                    </div>
                </a>
            </div>
        <? endforeach; ?>
    <? else: ?>
        <div class="col">
            По вашим параметрам ничего не найдено, попробуйте упростить поиск
        </div>
    <? endif; ?>
</div>
<script>
	$(document).ready(function() {
		$('.goods-item').each(function() {
			var top = 0;
			$(this).find('.goods-label-block').children().each(function() {
				$(this).css('top', top + 'px');
				top += 21;
			});
		});

<? 
$arHideFilterOptions = [2323, 2324, 2325, 2379];
foreach($arHideFilterOptions as $val): ?>
		$('.filter__block [href=#collapse<?=$val?>]').parent().css('visibility', 'hidden');
		$('.filter__block [href=#collapse<?=$val?>]').parent().css('height', '0');
		$('.filter__block [href=#collapse<?=$val?>]').parent().css('margin', '0');
<?
endforeach;
?>
	});
</script>

<?
if ($arParams["DISPLAY_BOTTOM_PAGER"]) {
    ?><?= $arResult["NAV_STRING"]; ?><?
}
?>

<? if(!strpos($arResult['ORIGINAL_PARAMETERS']['CURRENT_BASE_PAGE'], "/filter/") && !(isset($_GET["PAGEN_1"]) && $_GET["PAGEN_1"]>1)){ ?>
<div class="content">

<?=$arResult["DESCRIPTION"]?>

</div>
<?}?>
<?/*retail rocket*/?>
<script type="text/javascript">
    (window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function() {
        try { rrApi.categoryView(<?=$arResult["ID"]?>); } catch(e) {}
    })
</script>