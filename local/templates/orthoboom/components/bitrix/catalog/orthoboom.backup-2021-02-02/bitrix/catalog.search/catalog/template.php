<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if ( isset($arParams["SHOW_STATIC"]) && $arParams["SHOW_STATIC"] == "Y" )
{
    $request = $_GET["q"];

    ?>
    <?php
    $APPLICATION->IncludeComponent(
        "bitrix:search.title",
        "catalog",
        array(
            "NUM_CATEGORIES" => "1",
            "TOP_COUNT" => "5",
            "ORDER" => "date",
            "USE_LANGUAGE_GUESS" => "Y",
            "CHECK_DATES" => "N",
            "SHOW_OTHERS" => "N",
            "PAGE" => "#SITE_DIR#catalog2/",
            "CATEGORY_0_TITLE" => "",
            "CATEGORY_0" => array(
                0 => "iblock_1c_catalog",
            ),
            "COMPONENT_TEMPLATE" => ".default",
            "SHOW_INPUT" => "Y",
            "INPUT_ID" => "title-search-input",
            "CONTAINER_ID" => "title-search",
            "CATEGORY_0_iblock_1c_catalog" => array(
                0 => "94",
            )
        ),
        false
    );
    ?>
    <!--<div class="search-page">
        <form action="<?/*=$arParams['SEARCH_PAGE']*/?>" method="get">
            <?/*if($arParams["USE_SUGGEST"] === "Y"):
                if(strlen($arResult["REQUEST"]["~QUERY"]) && is_object($arResult["NAV_RESULT"]))
                {
                    $arResult["FILTER_MD5"] = $arResult["NAV_RESULT"]->GetFilterMD5();
                    $obSearchSuggest = new CSearchSuggest($arResult["FILTER_MD5"], $arResult["REQUEST"]["~QUERY"]);
                    $obSearchSuggest->SetResultCount($arResult["NAV_RESULT"]->NavRecordCount);
                }
                */?>
                <?/*$APPLICATION->IncludeComponent(
                "bitrix:search.suggest.input",
                "",
                array(
                    "NAME" => "q",
                    "VALUE" => $arResult["REQUEST"]["~QUERY"],
                    "INPUT_SIZE" => 40,
                    "DROPDOWN_SIZE" => 10,
                    "FILTER_MD5" => $arResult["FILTER_MD5"],
                ),
                $component, array("HIDE_ICONS" => "Y")
            );*/?>
            <?/*else:*/?>
                <input type="text" name="q"
                       class="search-catalog-text"
                       placeholder="<?/*=GetMessage('PLACEHOLDER_SEARCH')*/?>"
                       value="<?/*=$request*/?>" size="40"
                />
            <?/*endif;*/?>
            <input type="submit" class="search-catalog-button" value="<?/*=GetMessage("SEARCH_GO")*/?>" />
            <input type="hidden" name="how" value="<?/*echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"*/?>" />
            <?/*if($arParams["SHOW_WHEN"]):*/?>
                <script>
                    var switch_search_params = function()
                    {
                        var sp = document.getElementById('search_params');
                        var flag;

                        if(sp.style.display == 'none')
                        {
                            flag = false;
                            sp.style.display = 'block'
                        }
                        else
                        {
                            flag = true;
                            sp.style.display = 'none';
                        }

                        var from = document.getElementsByName('from');
                        for(var i = 0; i < from.length; i++)
                            if(from[i].type.toLowerCase() == 'text')
                                from[i].disabled = flag

                        var to = document.getElementsByName('to');
                        for(var i = 0; i < to.length; i++)
                            if(to[i].type.toLowerCase() == 'text')
                                to[i].disabled = flag

                        return false;
                    }
                </script>
                <br /><a class="search-page-params" href="#" onclick="return switch_search_params()"><?/*echo GetMessage('CT_BSP_ADDITIONAL_PARAMS')*/?></a>
                <div id="search_params" class="search-page-params" style="display:<?/*echo $arResult["REQUEST"]["FROM"] || $arResult["REQUEST"]["TO"]? 'block': 'none'*/?>">
                    <?/*$APPLICATION->IncludeComponent(
                        'bitrix:main.calendar',
                        '',
                        array(
                            'SHOW_INPUT' => 'Y',
                            'INPUT_NAME' => 'from',
                            'INPUT_VALUE' => $arResult["REQUEST"]["~FROM"],
                            'INPUT_NAME_FINISH' => 'to',
                            'INPUT_VALUE_FINISH' =>$arResult["REQUEST"]["~TO"],
                            'INPUT_ADDITIONAL_ATTR' => 'size="10"',
                        ),
                        null,
                        array('HIDE_ICONS' => 'Y')
                    );*/?>
                </div>
            <?/*endif*/?>
        </form>
        <br/>
        <?/*if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
            */?>
            <div class="search-language-guess">
                <?/*echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))*/?>
            </div><br /><?/*
        endif;*/?>
    </div>-->
    <?
    return;
}

$GLOBALS[$arParams["FILTER_NAME"]]["ACTIVE"] = "Y";
$arElements = $APPLICATION->IncludeComponent(
	"bitrix:search.page",
	"catalog",
	Array(
	    "HIDE_TEMPLATE" => $arParams["HIDE_TEMPLATE"],
		"RESTART" => $arParams["RESTART"],
		"NO_WORD_LOGIC" => $arParams["NO_WORD_LOGIC"],
		"USE_LANGUAGE_GUESS" => $arParams["USE_LANGUAGE_GUESS"],
		"CHECK_DATES" => $arParams["CHECK_DATES"],
		"arrFILTER" => array("iblock_".$arParams["IBLOCK_TYPE"]),
		"arrFILTER_iblock_".$arParams["IBLOCK_TYPE"] => array($arParams["IBLOCK_ID"]),
		"USE_TITLE_RANK" => "N",
		"DEFAULT_SORT" => "rank",
        "SEARCH_PAGE" => $arParams['SEARCH_PAGE'],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
		"SHOW_WHERE" => "N",
		"arrWHERE" => array(),
		"SHOW_WHEN" => "N",
		"PAGE_RESULT_COUNT" => 1000,
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "N",
	),
	$component,
	array('HIDE_ICONS' => 'Y')
);
global $searchFilter;
$searchFilter = array(
    "=ID" => $arElements,
);
if ( isset($arParams["HIDE_TEMPLATE"]) && $arParams["HIDE_TEMPLATE"] == "Y" )
{
    return $searchFilter;
}
else
{
    if ( !empty($arElements) && is_array($arElements) && $APPLICATION->GetCurPage(false) == "/catalog2/" )
    {
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            "search",
            array(
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
                "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                "PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
                "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                "OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
                "OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
                "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                "OFFERS_LIMIT" => $arParams["OFFERS_LIMIT"],
                "SECTION_URL" => $arParams["SECTION_URL"],
                "DETAIL_URL" => $arParams["DETAIL_URL"],
                "BASKET_URL" => $arParams["BASKET_URL"],
                "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
                "PRICE_CODE" => $arParams["PRICE_CODE"],
                "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
                "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
                "USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
                "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
                "CURRENCY_ID" => $arParams["CURRENCY_ID"],
                "HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
                "HIDE_NOT_AVAILABLE_OFFERS" => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
                "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                "FILTER_NAME" => "searchFilter",
                "SECTION_ID" => "",
                "SECTION_CODE" => "",
                "SECTION_USER_FIELDS" => array(),
                "INCLUDE_SUBSECTIONS" => "Y",
                "SHOW_ALL_WO_SECTION" => "Y",
                "META_KEYWORDS" => "",
                "META_DESCRIPTION" => "",
                "BROWSER_TITLE" => "",
                "ADD_SECTIONS_CHAIN" => "N",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",
            ),
            $arResult["THEME_COMPONENT"],
            array('HIDE_ICONS' => 'Y')
        );
    }
    elseif (is_array($arElements))
    {
        echo GetMessage("CT_BCSE_NOT_FOUND");
    }
}