<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Page\Asset;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Lib\Classes\Helper\LenalHelp;

$this->addExternalCss("/bitrix/css/main/bootstrap.css");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/components/bitrix/catalog/orthoboom/search.css");
if ($arParams["USE_COMPARE"] == "Y")
{
    $APPLICATION->IncludeComponent(
        "bitrix:catalog.compare.list",
        "",
        array(
            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "NAME" => $arParams["COMPARE_NAME"],
            "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
            "COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
            "ACTION_VARIABLE" => (!empty($arParams["ACTION_VARIABLE"]) ? $arParams["ACTION_VARIABLE"] : "action"),
            "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
            'POSITION_FIXED' => isset($arParams['COMPARE_POSITION_FIXED']) ? $arParams['COMPARE_POSITION_FIXED'] : '',
            'POSITION' => isset($arParams['COMPARE_POSITION']) ? $arParams['COMPARE_POSITION'] : ''
        ),
        $component,
        array("HIDE_ICONS" => "Y")
    );
}

if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
{
    $basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '');
}
else
{
    $basketAction = (isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '');
}
//Костыль, господи помилуй
if ( !$_REQUEST["ajax"] )
{
    //region Формируем список, не выводим шаблон
    $additionalSearchParams = array('HIDE_TEMPLATE' => 'Y');
    include "parts/search_component.php";
    //endregion
    global $searchFilter;
    $showItems = true;

    if ( count($searchFilter['=ID']) == 0 || !$searchFilter['=ID'] )
    {
        $this->SetViewTarget('search_empty');
        echo '<p>Сожалеем, но ничего не найдено.</p>';
        $this->EndViewTarget();
        $showItems = false;
    }
    else
        $GLOBALS[$arParams['FILTER_NAME']]["ID"] = $searchFilter["=ID"];
}
else
{
    $name = $_GET["q"];

    if ( mb_detect_encoding($name) == "UTF-8" && LANG_CHARSET == "windows-1251" )
        $name = mb_convert_encoding($name, "CP-1251");

    $arFilter["NAME"] = '%' . $name . '%';
    $GLOBALS[$arParams['FILTER_NAME']]["NAME"] = '%' . $name . '%';
}
?>

<section class="catalog" style="margin-top: 20px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 pl-sm-0">
                <?php
                $APPLICATION->IncludeComponent(
                    (defined('SMART_FILTER_UUID_VALUE') && SMART_FILTER_UUID_VALUE)?"gorgoc:catalog.smart.filter":"bitrix:catalog.smart.filter",
                    "series",
                    array(
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "SECTION_ID" => $arCurSection['ID'],
                        "FILTER_NAME" => $arParams["FILTER_NAME"],
                        "PRICE_CODE" => $arParams["PRICE_CODE"],
                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                        "CACHE_TIME" => 0,//$arParams["CACHE_TIME"],
                        "CACHE_GROUPS" => "N",
                        "SAVE_IN_SESSION" => "N",
                        "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
                        "XML_EXPORT" => "Y",
                        "SECTION_TITLE" => "NAME",
                        "SECTION_DESCRIPTION" => "DESCRIPTION",
                        'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                        "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                        'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                        'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                        "SEF_MODE" => $arParams["SEF_MODE"],
                        "SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
                        "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                        "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                        "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
                        "SHOW_ALL_WO_SECTION"=>"Y"
                    ),
                    $component,
                    array('HIDE_ICONS' => 'Y')
                );
                ?>

                </div>
                <div class="col-lg-9 pr-sm-0">
                    <?php LenalHelp::getTitle();?>
                    <h1 class="titled mb-3"><?$APPLICATION->ShowTitle(false);?></h1>
                    <div class="d-flex flex-wrap justify-content-between">
                        <?
                        //region Выводим статику, без запросов к бд
                        $additionalSearchParams = array('SHOW_STATIC' => 'Y');
                        include "parts/search_component.php";
                        if ( !$showItems )
                        {
                            $APPLICATION->ShowViewContent("search_empty");
                        }
                        //endregion
                        ?>
                    </div>
                    <? if ( $showItems ): ?>
                        <div class="d-flex flex-wrap justify-content-between">
                            <div class="filter-btns mb-3">
                                <a class="mb-2 btn btn-cheap <?if ($_REQUEST["sort"] == "price_asc"):?>active<?endif;?>" href="
                            <?=$APPLICATION->GetCurPageParam("sort=price_asc", array('sort'));?>">
                                    Дешевле
                                </a>
                                <a class="mb-2 btn btn-expensive <?if ($_REQUEST["sort"] == "price_desc"):?>active<?endif;?>"
                                   href="<?=$APPLICATION->GetCurPageParam("sort=price_desc", array('sort'));?>">
                                    Дороже
                                </a>
                                <a class="mb-2 btn btn-new <?if ($_REQUEST["sort"] == "new"):?>active<?endif;?>" href="
                            <?=$APPLICATION->GetCurPageParam("sort=new", array('sort'));?>">
                                    Новинки
                                </a>
                                <a class="mb-2 btn btn-special <?if ($_REQUEST["sort"] == "spec_pred"):?>active<?endif;?>"
                                   href="<?=$APPLICATION->GetCurPageParam("sort=spec_pred", array('sort'));?>">
                                    Спец. предложения
                                </a>
                            </div>
                            <select name="show" class="selectpicker mb-4">
                                <option <?if ($_REQUEST["show"] == "15"):?>selected<?endif;?>
                                        value="<?=$APPLICATION->GetCurPageParam("show=15", array('show'));?>">15</option>
                                <option <?if ($_REQUEST["show"] == "24"):?>selected<?endif;?>
                                        value="<?=$APPLICATION->GetCurPageParam("show=24", array('show'));?>">24</option>
                            </select>
                        </div>
                        <script>
                            $('.selectpicker').on('changed.bs.select', function (e) {
                                var val  = $(this).val();
                                location = val;
                            });
                        </script>
                        <?
                        switch ($_REQUEST["sort"]) {
                            case "price_asc":
                                $forSort = "property_MIN_PRICE";
                                $forOrder = 'asc,nulls';
                                break;
                            case "price_desc":
                                $forSort = "property_MIN_PRICE";
                                $forOrder = 'desc,nulls';
                                break;
                            case "new":
                                $forSort = "property_NOVINKA_INTERNET_MAGAZIN";
                                $forOrder = 'asc,nulls';
                                break;
                            case "spec_pred":
                                $forSort = "property_AKTSIYA_INTERNET_MAGAZIN";
                                $forOrder = 'asc,nulls';
                                break;
                        }

                        if ($_REQUEST["show"]) {
                            $show = $_REQUEST["show"];
                        } Else {
                            $show = 15;
                        }

                        $intSectionID = $APPLICATION->IncludeComponent(
                            "bitrix:catalog.section",
                            "",
                            array(
                                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                "ELEMENT_SORT_FIELD" => $forSort,
                                "ELEMENT_SORT_ORDER" => $forOrder,
                                "ELEMENT_SORT_FIELD2" => $forSort,
                                "ELEMENT_SORT_ORDER2" =>  $forOrder,
                                "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                                "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                                "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                                "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                                "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                                "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
                                "BASKET_URL" => $arParams["BASKET_URL"],
                                "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                                "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                                "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                                "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                                "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                                "FILTER_NAME" => $arParams["FILTER_NAME"],
                                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                "CACHE_TIME" => $arParams["CACHE_TIME"],
                                "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                                "SET_TITLE" => $arParams["SET_TITLE"],
                                "MESSAGE_404" => $arParams["MESSAGE_404"],
                                "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                                "SHOW_404" => $arParams["SHOW_404"],
                                "FILE_404" => $arParams["FILE_404"],
                                "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
                                "PAGE_ELEMENT_COUNT" => $show,
                                "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                                "PRICE_CODE" => $arParams["PRICE_CODE"],
                                "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                                "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

                                "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                                "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
                                "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                                "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                                "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

                                "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                                "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                                "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                                "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                                "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                                "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                                "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                                "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                                "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],

                                "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                                "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                                "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
                                "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                                "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                                "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                                "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                                "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

                                /*"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],*/
                                "SECTION_ID" => $arCurSection['ID'],
                                /*"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],*/
                                "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                                "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
                                "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
                                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                                'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

                                'LABEL_PROP' => $arParams['LABEL_PROP'],
                                'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                                'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

                                'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                                'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                                'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                                'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                                'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                                'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
                                'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
                                'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
                                'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
                                'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

                                'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                                "ADD_SECTIONS_CHAIN" => "Y",
                                'ADD_TO_BASKET_ACTION' => $basketAction,
                                'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                                'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
                                'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
                                'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
                                "SHOW_ALL_WO_SECTION" => "Y"
                            ),
                            $component
                        );?>
                    <? endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="seo-text-holder parent-js">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" =>"/include/".$lang."/seo_text.php",
                        )
                    );?>
                </div>
            </div>
        </div>
    </section>
<?
unset($basketAction);

$h1 = "Результат поиска: \"{$_GET['q']}\"";
$APPLICATION->SetTitle($h1);