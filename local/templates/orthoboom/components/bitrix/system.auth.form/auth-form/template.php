<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="login no-authorised">
	<div class="cabinet-link"><a href="#"><?=GetMessage("CABINET");?></a></div>
	<div class="auth-form hide">
		<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>" id="auth-drop">

			<?
			// if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
			// 	ShowMessage($arResult['ERROR_MESSAGE']);
			?>
			<div id="auth-drop-error" class="alert alert-error hidden text-center"></div>

			<?if($arResult["BACKURL"] <> ''):?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
			<?endif?>
			<?foreach ($arResult["POST"] as $key => $value):?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?endforeach?>
			<input type="hidden" name="AUTH_FORM" value="Y" />
			<input type="hidden" name="TYPE" value="AUTH" />

			<input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" size="17" placeholder="<?=GetMessage("AUTH_LOGIN")?>">
			<input type="password" name="USER_PASSWORD" maxlength="50" size="17" autocomplete="off" placeholder="<?=GetMessage("AUTH_PASSWORD")?>">
			<input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>">
		</form>
		<div class="form-links"> 
			<a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow" class="forget-link"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>

			<?if($arResult["NEW_USER_REGISTRATION"] == "Y"):?>
				<a href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow" class="reg-link"><?=GetMessage("AUTH_REGISTER")?></a>
			<?endif?>
									
		</div>
	</div>
</div>