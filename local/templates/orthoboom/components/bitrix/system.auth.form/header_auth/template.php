<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>



<?if($arResult["FORM_TYPE"] == "login"):?>

    <form id="authForm" onsubmit="return checkForm(this)" name="system_auth_form<?=$arResult["RND"]?>" method="post"
          target="_top" action="/ajax/login.php" class="authentication__form">
        <div class="error">
            <?
            if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
                ShowMessage($arResult['ERROR_MESSAGE']);
            ?>
        </div>
        <?=bitrix_sessid_post()?>
        <?if($arResult["BACKURL"] <> ''):?>
            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
        <?endif?>
        <?foreach ($arResult["POST"] as $key => $value):?>
            <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
        <?endforeach?>
        <input type="hidden" name="AUTH_FORM" value="Y" />
        <input type="hidden" name="TYPE" value="AUTH" />

        <div class="auth-input-wrapper">
            <input id="fio" type="text" required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Введите логин')" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>"
                   size="17" placeholder="Логин" class="input-text required authentication__input" />
        </div>
        <div class="auth-input-wrapper">
            <input type="password" required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Введите пароль')" name="USER_PASSWORD" maxlength="50" size="17" autocomplete="off"
               placeholder="Пароль" class="input-text required authentication__input" />
        </div>
        <?if($arResult["SECURE_AUTH"]):?>
            <span class="bx-auth-secure" id="bx_auth_secure<?=$arResult["RND"]?>"
                  title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
            </span>
            <noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
            </noscript>
            <script type="text/javascript">
                document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
            </script>
        <?endif?>
        <div class="auth-link-wrapper flex-container flex-side">
            <a data-target="#register" data-toggle="modal">Регистрация</a>
            <a id="forgotPass" href="#">Забыли пароль?</a>
        </div>
        <?/*<?if ($arResult["STORE_PASSWORD"] == "Y"):?>
            <fieldset class="authentication__fieldset fieldset-checkbox pull-left">
                <input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" />
                <label for="USER_REMEMBER_frm" title="<?=GetMessage("AUTH_REMEMBER_ME")?>"><?echo GetMessage("AUTH_REMEMBER_SHORT")?></label>
            </fieldset>
        <?endif?>*/?>
        <div class="auth-btn-wrap">
            <input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>"
               class="mybtn transition yourCity-btn" style="text-transform: uppercase; margin-bottom: 8px;"/>
        </div>
        <?if ($arResult["CAPTCHA_CODE"]):?>

            <?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
            <input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
            <img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>"
                 width="180" height="40" alt="CAPTCHA" /><br /><br />
            <input type="text" name="captcha_word" maxlength="50" value="" />

        <?endif?>
    </form>
    <script type="text/javascript">
        function checkForm(form){
            if (document.getElementById('fio').value=="") {
                document.getElementById('err_fio').innerHTML='Введите имя';
                return false;
            };
            return true;
        };
    </script>



<?endif;?>


<?/*$APPLICATION->IncludeComponent(
    "bitrix:system.auth.forgotpasswd",
    ".default",
    Array()
);*/?>

