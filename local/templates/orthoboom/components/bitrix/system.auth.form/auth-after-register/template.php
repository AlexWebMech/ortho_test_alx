<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<div class="navibar"><a href="/catalog/">Продолжить выбор товаров</a></div>
<h1 class="center">Регистрация завершена!</h1>
<div class="order-done center">
	<p>Благодарим Вас за регистрацию на нашем сайте. Теперь Вы можете войти в личный кабинет.<br> 
	Для этого введите Ваш email и пароль в форме.</p>
</div>

<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>" class="change-pass">


	<?if($arResult["BACKURL"] <> ''):?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
	<?endif?>
	<?foreach ($arResult["POST"] as $key => $value):?>
		<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
	<?endforeach?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />

	<div class="registration">
		<div class="form-body">

			<?
			if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']) {
				echo '<div class="alert alert-danger">';
				//ShowMessage($arResult['ERROR_MESSAGE']);
				echo "Неверный email или пароль";
				echo '</div>';
			}
			?>

			<form class="change-pass">
				<div><label for="">EMAIL</label><input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>"></div>
				<div><label for="">Пароль</label><input type="password" name="USER_PASSWORD" maxlength="50" size="17" autocomplete="off" id="confirm-pass"><input value="Показать" type="checkbox" onchange="if ($('#confirm-pass').get(0).type=='password') {$('#confirm-pass').get(0).type='text'; $('#confirm-pass').addClass('active') }  else {$('#confirm-pass').get(0).type='password';  $('#confirm-pass').removeClass('active');} "></div>
			</form>	
		</div>
		<div class="form-bottom">	
			<div class="cancell-button"><a href="/">Отменить</a></div> 
			<div class="login-button">
				<input type="submit" class="btn-submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>">
			</div> 						
		</div>					
	</div>
</form>