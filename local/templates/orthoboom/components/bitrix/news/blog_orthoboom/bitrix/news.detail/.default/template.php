<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$APPLICATION->SetPageProperty('title', $arResult["NAME"] . '. Orthoboom - правильная обувь.');
?>
	  <div class="container" itemscope itemtype="http://schema.org/Article">
         <h1 class="center" itemprop="name"><?=$arResult["NAME"]?></h1>
         <meta itemprop="headline" content="<?=$arResult["NAME"]?>">
         <link itemprop="mainEntityOfPage" href="<?=$_SERVER['REQUEST_URI'];?>" />
         <div class="contact" style="margin-top:0;">
		 <?
		 if($arResult["DETAIL_PICTURE"]["SRC"]){
			 $src = $arResult["DETAIL_PICTURE"]["SRC"];
		 }else{
			$src = SITE_TEMPLATE_PATH."/images/not_logo.png";
		 }
		 ?>
			 <img itemprop="image" alt="" src="<?=$src;?>" style="max-width: 267px; float:left; margin-right: 30px;">
		 <?if($arResult["DETAIL_TEXT"]){?>
			<p><?echo $arResult["DETAIL_TEXT"];?></p>
		<?}?>
         </div>
         <div style="display: none;">
         	<span itemprop="author">ORTHOBOOM</span>
         	<meta itemprop="datePublished" content="<?=date("Y-m-d", strtotime($arResult['~TIMESTAMP_X']))?>">
         	<p itemprop="articleBody">
         		<?
	         	$string = strip_tags($arResult["DETAIL_TEXT"]);
				$string = substr($string, 0, 120);
				$string = rtrim($string, "!,.-");
				$string = substr($string, 0, strrpos($string, ' '));
				echo $string."… "
				?>
			</p>
			<meta itemprop="description" content="<? $string."… "; ?>">
			<div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
		        <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
		            <img itemprop="url image" src="/local/templates/orthoboom/img/header-logo.png" style="display:none;"/>
		        </div>
		        <meta itemprop="name" content="ORTHOBOOM">
		        <meta itemprop="telephone" content="88002346640">
		        <meta itemprop="address" content="Россия">

		    </div>
         </div>
      </div>
