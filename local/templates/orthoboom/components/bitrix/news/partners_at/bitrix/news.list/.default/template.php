<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="list">
<!--
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<?if($arItem["PROPERTY_TOP_VALUE"] != "y") continue;?>
	<?//echo "<pre>"; print_r($arItem); echo "</pre>";?>
		<div class="block" id="<?=$this->GetEditAreaId($arItem['ID'])?>">
			<?if($arItem["PREVIEW_PICTURE"]["SRC"]){
				$src = $arItem["PREVIEW_PICTURE"]["SRC"];
			}else{
				$src = SITE_TEMPLATE_PATH."/images/not_logo.png";
			}?>
      		<img src="<?=$src?>">
			<h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
            <p class="city_in_block">
				<?
				if(is_array($arItem["PROPERTIES"]["CITY"]["VALUE"])){
					$index = 0;
					foreach($arItem["PROPERTIES"]["CITY"]["VALUE"] as $value){
						echo $value;
						$index++;
						if($index!=count($arItem["PROPERTIES"]["CITY"]["VALUE"])) echo ", ";
					}
				}else{
					echo $arItem["PROPERTIES"]["CITY"]["VALUE"][0];
				}
				?>
			</p>
		</div>
<?endforeach;?>
-->
<table class="our-partners-table">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<?//=$this->GetEditAreaId($arItem['ID']);?>
		<?//echo "<pre>"; print_r($arItem); echo "</pre>";?>
		<?if($arItem["PROPERTY_TOP_VALUE"] == "y") continue;?>
			<tr data-href="<?=$arItem["DETAIL_PAGE_URL"]?>" id="<?=$this->GetEditAreaId($arItem['ID'])?>">
				<td><div class="h2"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div></td>
				<td>
					<p>
					<?
					if(is_array($arItem["PROPERTIES"]["CITY"]["VALUE"])){
						$index = 0;
						foreach($arItem["PROPERTIES"]["CITY"]["VALUE"] as $value){
							echo $value;
							$index++;
							if($index!=count($arItem["PROPERTIES"]["CITY"]["VALUE"])) echo ", ";
						}
					}else{
						echo $arItem["PROPERTIES"]["CITY"]["VALUE"][0];
					}
					?>
					</p>
				</td>
			</tr>
	<?endforeach;?>
</table>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>