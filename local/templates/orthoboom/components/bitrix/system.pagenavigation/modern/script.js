$(document).ready(function(){

	$(document).on('click', '.catalog__more-button', function(){

		var targetContainer = $('.catalog__list'),
			url =  $('.catalog__more-button').attr('data-url');

		if (url !== undefined) {
			$.ajax({
				type: 'GET',
				url: url,
				dataType: 'html',
				success: function(data){

					$('.catalog__more-button').remove();

					var elements = $(data).find('.catalog__item'),
						paginationButton = $(data).find('.catalog__more-button');

					targetContainer.append(elements);
					$('.catalog__more-container').append(paginationButton);

				}
			});
		}

	});

});