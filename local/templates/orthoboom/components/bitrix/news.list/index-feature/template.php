<? if(! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="index-feature">
	<div class="index-feature-items">
		<? foreach($arResult["ITEMS"] as $arItem) : ?>
		<a class="index-feature-items-item" <? if ($arItem["DETAIL_PAGE_URL"]) : ?>href="<?= $arItem["DETAIL_PAGE_URL"] ?>"<? else : ?><? endif; ?>>
			<div class="index-feature-items-flex">
				<div class="ui-picture-col">
					<? $arPicture = $arItem["PREVIEW_PICTURE"]; ?>
					<? if ($arPicture) : ?><img src="<?= $arPicture["SRC"] ?>" width="<?= $arPicture["WIDTH"] ?>" height="<?= $arPicture["HEIGHT"] ?>" alt=""><? endif; ?>
				</div>
				<div class="ui-main-col">
					<?= $arItem["PREVIEW_TEXT"] ?>
				</div>
			</div>
		</a>
		<? endforeach; ?>
		<div class="index-feature-items-item ui-flex-empty"></div>
		<div class="index-feature-items-item ui-flex-empty"></div>
		<div class="index-feature-items-item ui-flex-empty"></div>
		<div class="index-feature-items-item ui-flex-empty"></div>
		<div class="index-feature-items-item ui-flex-empty"></div>
	</div>
</div>