<? if(! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

if (\Bitrix\Main\Loader::includeSharewareModule("uni.data"))
{
	\Uni\Data\Tools\BitrixPicture::proccessItems($arResult["ITEMS"], $arParams);
}

foreach($arResult["ITEMS"] as &$arItem)
{
	if (! $arItem["PREVIEW_TEXT"])
		$arItem["PREVIEW_TEXT"] = $arItem["NAME"];
	
	$arItem["DETAIL_PAGE_URL"] = $arItem["PROPERTIES"]["DETAIL_PAGE_URL"]["VALUE"];
}
unset($arItem);
?>