<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); $this->setFrameMode(true);?>

<div id="owl-demo" class="owl-carousel owl-theme">
	
<?$lang = GetLang();?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	//echo $lang;
	if ($lang == 'ru') {
		if (!in_array(693550, $arItem['PROPERTIES']['LANG']['VALUE_ENUM_ID'])) continue;
		$name = trim($arItem["NAME"]);
		$text = $arItem["PREVIEW_TEXT"];
	} 
	if ($lang == 'en') {
		if (!in_array(693551, $arItem['PROPERTIES']['LANG']['VALUE_ENUM_ID'])) continue;
		$name = trim($arItem["PROPERTIES"]["NAME_ENG"]["VALUE"]);
		$text = htmlspecialcharsBack($arItem["PROPERTIES"]["TEXT_ENG"]["VALUE"]["TEXT"]);
	} 
	if ($lang == 'de') {
		if (!in_array(693552, $arItem['PROPERTIES']['LANG']['VALUE_ENUM_ID'])) continue;
		$name = trim($arItem["PROPERTIES"]["NAME_ENG"]["VALUE"]);
		$text = htmlspecialcharsBack($arItem["PROPERTIES"]["TEXT_ENG"]["VALUE"]["TEXT"]);
	} 
	?>
	<div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>" style="cursor: pointer" onclick="location.href='<? echo $arItem["DETAIL_TEXT"] ?>'">
		<div class="container">
			<?if ($name):?>
			<h2><?echo $name?></h2>
			<?endif;?>
			<?if ($text) :?><p><?echo $text?></p><?endif;?>
				
		</div>
		<img src="<?echo $arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?echo $arItem["NAME"]?>" />		
	</div>
<?endforeach;?>

</div>	