<? if(! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

?> 
<div class="index-banners">

	<div class="index-banners-small-items-1 index-banners-small-items">
	<? $i = 0; foreach($arResult["ITEMS"] as $arItem)
	{
		$i++; 
		if ($i >= 3)
			break; ?>
		<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><? $arPicture = $arItem["PREVIEW_PICTURE"]; 
			?><? if ($arPicture) : ?><img src="<?= $arPicture["SRC"] ?>" width="<?= $arPicture["WIDTH"] ?>" height="<?= $arPicture["HEIGHT"] ?>" alt=""><? endif; 
		?></a>
		<?
	}
	?></div>

	<div class="index-banners-big-item">
		
	<? $APPLICATION->IncludeComponent("bitrix:news.list", "index-banner-big", Array(
		"IBLOCK_TYPE" => "index",
		"IBLOCK_ID" => "102",
		
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"CHECK_DATES" => "N",
		"FILTER_NAME" => "",

		"NEWS_COUNT" => "100",
		"PROPERTY_CODE" => array(
			0 => "*",
		),
		"FIELD_CODE" => array(
			0 => "",
		),

		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER2" => "DESC",
		
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",	
		"ADD_SECTIONS_CHAIN" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		
		"PREVIEW_PICTURE_WIDTH" => 795,
		"PREVIEW_PICTURE_HEIGHT" => 465,
		"PREVIEW_PICTURE_RESIZE_TYPE" => "",
		"PREVIEW_PICTURE_CREATE" => "DETAIL_PICTURE",
		
		
	),
	false
	); ?>	
		
	</div>
	
	<div class="index-banners-small-items-2 index-banners-small-items">
	<? $i = 0; foreach($arResult["ITEMS"] as $arItem)
	{
		$i++; 
		if ($i < 3)
			continue;
		if ($i >= 5)
			break; ?>
		<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><? $arPicture = $arItem["PREVIEW_PICTURE"]; 
			?><? if ($arPicture) : ?><img src="<?= $arPicture["SRC"] ?>" width="<?= $arPicture["WIDTH"] ?>" height="<?= $arPicture["HEIGHT"] ?>" alt=""><? endif; 
		?></a>
		<?
	}
	?></div>
	
</div>	