<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); $this->setFrameMode(true);?>

<?
$lang = GetLang();
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	
	//$name = $arItem["NAME"];
	//$text = $arItem["PREVIEW_TEXT"];
	
	//$name = $arItem["PROPERTIES"]["NAME_ENG"]["VALUE"];
	//$text = htmlspecialcharsBack($arItem["PROPERTIES"]["TEXT_ENG"]["VALUE"]["TEXT"]);

	
	if ($lang == 'en') {
		$name = $arItem["PROPERTIES"]["NAME_ENG"]["VALUE"];
		$text = htmlspecialcharsBack($arItem["PROPERTIES"]["TEXT_ENG"]["VALUE"]["TEXT"]);
	} else if ($lang == 'de') {
		$name = $arItem["PROPERTIES"]["NAME_ENG"]["VALUE"];
		$text = htmlspecialcharsBack($arItem["PROPERTIES"]["TEXT_ENG"]["VALUE"]["TEXT"]);
	} else {
		$name = $arItem["NAME"];
		$text = $arItem["PREVIEW_TEXT"];	
	}

	?>
	<div class="span4" id="<?=$this->GetEditAreaId($arItem['ID']);?>" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);">
		<h4><?echo $name?></h4>
		<p><?echo $text;?></p>

	</div>
<?endforeach;?>
