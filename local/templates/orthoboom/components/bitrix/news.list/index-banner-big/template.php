<? if(! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

?>
    <div class="swiper-container">
      <div class="swiper-wrapper">
	<?
foreach($arResult["ITEMS"] as $arItem )
{
?>
	<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="swiper-slide"><? $arPicture = $arItem["PREVIEW_PICTURE"]; 
		?><? if ($arPicture) : ?><img src="<?= $arPicture["SRC"] ?>" width="<?= $arPicture["WIDTH"] ?>" height="<?= $arPicture["HEIGHT"] ?>" alt=""><? endif; 
	?></a>
	<?
}
?>
	 
	  </div>
	   <div class="swiper-pagination"></div>
	</div>

<script>

new Swiper(".swiper-container", {
	 slidesPerView: 1,
      pagination: {
        el: '.swiper-pagination',
      },	
		autoplay: {
			delay: 5000,
		},	  
});
</script>