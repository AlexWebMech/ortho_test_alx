<? if(! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

?>

<div class="<?= $arParams["WRAP_CLASS"] ?> picture-list">
	<div class="items">
	<? foreach($arResult["ITEMS"] as $arItem) : ?>
		<div class="item">
			<div class="item-picture">
				<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
				<? $arPicture = $arItem["PREVIEW_PICTURE"]; ?>
				<? if ($arPicture) : ?><img src="<?= $arPicture["SRC"] ?>" width="<?= $arPicture["WIDTH"] ?>" height="<?= $arPicture["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>"><? endif; ?>
				</a>
			</div>
		</div>
	<? endforeach; ?>
	<? for ($i = 1; $i <= 5; $i++)
	{
		?><div class="item ui-flex-empty"></div><?
	}
	?>	
	</div>
</div>

