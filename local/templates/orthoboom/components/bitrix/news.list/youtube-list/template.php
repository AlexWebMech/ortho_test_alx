<? if(! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

?>

<div class="<?= $arParams["WRAP_CLASS"] ?>">
	<div class="items">
	<? foreach($arResult["ITEMS"] as $arItem) : ?>
		<div class="item">
			<?= $arItem["PREVIEW_TEXT"] ?>
		</div>
	<? endforeach; ?>
	<? for ($i = 1; $i <= 5; $i++)
	{
		?><div class="item ui-flex-empty"></div><?
	}
	?>	
	</div>
</div>

