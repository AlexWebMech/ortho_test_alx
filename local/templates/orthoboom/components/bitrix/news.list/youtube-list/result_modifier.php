<? if(! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

if (\Bitrix\Main\Loader::includeSharewareModule("uni.data"))
{
	\Uni\Data\Tools\BitrixPicture::proccessItems($arResult["ITEMS"], $arParams);
}

foreach($arResult["ITEMS"] as &$arItem)
{
	if ($arItem["PROPERTIES"]["YOUTUBE_LINK"]["VALUE"])
		$arItem["PREVIEW_TEXT"] = '<div><iframe width="" height="" src="' . $arItem["PROPERTIES"]["YOUTUBE_LINK"]["VALUE"] . '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>' . $arItem["PREVIEW_TEXT"];
}
unset($arItem);
?>