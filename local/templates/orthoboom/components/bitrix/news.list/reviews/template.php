<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); $this->setFrameMode(true);?>

<? //vard($arResult); ?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?//print_r($arItem);
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="review-item" itemscope itemtype="https://schema.org/Review" id="<?=$this->GetEditAreaId($arItem['ID']);?>" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);">
			
		<p><blockquote itemprop="reviewBody"><?echo $arItem["PROPERTIES"]["REVIEW"]["VALUE"]["TEXT"];?></blockquote></p>
		<meta itemprop="datePublished" content="<?=$arItem["DATE_ACTIVE_FROM"]?>"/>
		<div class="date"><?=$arItem["DATE_ACTIVE_FROM"]?></div>
		<div class="sign" itemprop="name"><?=$arItem["NAME"]?></div>

		<div style="display: none;">
	        <span itemprop="author" itemscope itemtype="https://schema.org/Person">
	            <p itemprop="name"><?=$arItem["NAME"]?></p>
	        </span>
	    </div>

		<div style="display: none;" itemprop="itemReviewed" itemscope itemtype="https://schema.org/Organization">
	        <meta itemprop="name" content="Отзыв о компании ORTHOBOOM">
	        <meta itemprop="telephone" content="88002346640">
	        <link itemprop="url" href="https://orthoboom.ru/"/>	        
	        <meta itemprop="email" content="zakaz@orthoboom.ru">
	        <p itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
	            <meta itemprop="addressLocality" content="Москва">
	            <meta itemprop="streetAddress" content='Кировоградская, д.13А, ТРЦ "Columbus"'>
	        </p>
	    </div>
	</div>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
