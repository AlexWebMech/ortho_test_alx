<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<h1 class="center">Смена пароля</h1>
<div class="registration">
	<form method="post" action="<?=$arResult["AUTH_FORM"]?>" class="change-pass">
		<?if (strlen($arResult["BACKURL"]) > 0): ?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
		<? endif ?>
		<input type="hidden" name="AUTH_FORM" value="Y">
		<input type="hidden" name="TYPE" value="CHANGE_PWD">



		<div class="form-body">
			<?if (!empty($arResult["SUCCESS_MSG"])):?>
				<div class="alert alert-success">
					<?=$arResult["SUCCESS_MSG"]?>
				</div>
			<?elseif(!empty($arResult["ERROR_MSG"])):?>
				<div class="alert alert-danger">
					<?=$arResult["ERROR_MSG"]?>
				</div>
			<?endif;?>
			<div>
				<input type="hidden" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>">
				<input type="hidden" name="USER_CHECKWORD" maxlength="50" value="<?=$arResult["USER_CHECKWORD"]?>">
			</div>
			<div>
			<label for=""><?=GetMessage("AUTH_NEW_PASSWORD_REQ")?></label><input type="password" name="USER_PASSWORD" maxlength="50" value="<?=$arResult["USER_PASSWORD"]?>" autocomplete="off" id="new-pass"><input value="Показать" type="checkbox" onchange="if ($('#new-pass').get(0).type=='password') {$('#new-pass').get(0).type='text'; $('#new-pass').addClass('active') }  else {$('#new-pass').get(0).type='password';  $('#new-pass').removeClass('active');} "></div>
			<div><label for=""><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?></label><input type="password" name="USER_CONFIRM_PASSWORD" maxlength="50" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" autocomplete="off" id="confirm-pass"><input value="Показать" type="checkbox" onchange="if ($('#confirm-pass').get(0).type=='password') {$('#confirm-pass').get(0).type='text'; $('#confirm-pass').addClass('active') }  else {$('#confirm-pass').get(0).type='password';  $('#confirm-pass').removeClass('active');} "></div>
		</div>
		<div class="form-bottom">	
			<div class="cancell-button"><a href="/">Отменить</a></div> 
			<div class="login-button">
				<input type="submit" name="change_pwd" value="Отправить" class="btn-submit">
			</div> 						
		</div>
	</form>				
</div>