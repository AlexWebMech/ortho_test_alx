<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Page\Asset;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/validation/jquery.validate.min.js");
$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES'])) {
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
    'TEMPLATE_THEME'   => $this->GetFolder() . '/themes/' . $arParams['TEMPLATE_THEME'] . '/style.css',
    'TEMPLATE_CLASS'   => 'bx_' . $arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES'       => $currencyList
);
unset($currencyList, $templateLibrary);
CJSCore::Init(array('ajax', 'window'));

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
    'ID'                 => $strMainID,
    'PICT'               => $strMainID . '_pict',
    'DISCOUNT_PICT_ID'   => $strMainID . '_dsc_pict',
    'STICKER_ID'         => $strMainID . '_sticker',
    'BIG_SLIDER_ID'      => $strMainID . '_big_slider',
    'BIG_IMG_CONT_ID'    => $strMainID . '_bigimg_cont',
    'SLIDER_CONT_ID'     => $strMainID . '_slider_cont',
    'SLIDER_LIST'        => $strMainID . '_slider_list',
    'SLIDER_LEFT'        => $strMainID . '_slider_left',
    'SLIDER_RIGHT'       => $strMainID . '_slider_right',
    'OLD_PRICE'          => $strMainID . '_old_price',
    'PRICE'              => $strMainID . '_price',
    'DISCOUNT_PRICE'     => $strMainID . '_price_discount',
    'SLIDER_CONT_OF_ID'  => $strMainID . '_slider_cont_',
    'SLIDER_LIST_OF_ID'  => $strMainID . '_slider_list_',
    'SLIDER_LEFT_OF_ID'  => $strMainID . '_slider_left_',
    'SLIDER_RIGHT_OF_ID' => $strMainID . '_slider_right_',
    'QUANTITY'           => $strMainID . '_quantity',
    'QUANTITY_DOWN'      => $strMainID . '_quant_down',
    'QUANTITY_UP'        => $strMainID . '_quant_up',
    'QUANTITY_MEASURE'   => $strMainID . '_quant_measure',
    'QUANTITY_LIMIT'     => $strMainID . '_quant_limit',
    'BASIS_PRICE'        => $strMainID . '_basis_price',
    'BUY_LINK'           => $strMainID . '_buy_link',
    'ADD_BASKET_LINK'    => $strMainID . '_add_basket_link',
    'BASKET_ACTIONS'     => $strMainID . '_basket_actions',
    'NOT_AVAILABLE_MESS' => $strMainID . '_not_avail',
    'COMPARE_LINK'       => $strMainID . '_compare_link',
    'PROP'               => $strMainID . '_prop_',
    'PROP_DIV'           => $strMainID . '_skudiv',
    'DISPLAY_PROP_DIV'   => $strMainID . '_sku_prop',
    'OFFER_GROUP'        => $strMainID . '_set_group_',
    'BASKET_PROP_DIV'    => $strMainID . '_basket_prop',
);
$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (
isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) &&
$arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
    : $arResult['NAME']
);
$strAlt = (
isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) &&
$arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
    : $arResult['NAME']
);

//vard($arResult, 'aaa');
?>

<?if (in_array($arResult['ID'], $arResult["WISHLIST"])):?>
    <?$wishlist = "delete";?>
<?else:?>
    <?$wishlist = "add";?>
<?endif;?>

<section id="<?= $arResult["ID"]; ?>" class="product catalog-detail__orthoboom" itemtype="http://schema.org/Product" itemscope >
<div class="catalog-detail--item" data-iblock-element-id="<?= htmlspecialchars($arResult["ID"]) ?>" data-item-id="<?= htmlspecialchars($arResult["ID"]) ?>">
    <div class="container">
        <div class="row">
            <div class="col col-12 col-md-6 col-lg-7">
                <h1 class="product__title" itemprop="name"><?= $arResult["NAME"]; ?></h1>
                <div class="product__vendor-code"><?= $arResult["PROPERTIES"]["CML2_ARTICLE"]["VALUE"]; ?></div>
                <div class="product__images">
                    <div class="product__slider">
                        <div class="product__slider-item">
                            <div class="product__slider-image">
                                <a href="<?php echo $arResult["DETAIL_PICTURE"]["SRC"]?>" class="product__slider-link fancybox" rel="product-<?=$arResult["ID"]?>">
                                    <img itemprop="image" src="<?php echo $arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?= $arResult["NAME"]; ?>">
                                    <span class="product__slider-magnifier"></span>
                                </a>
                            </div>
                        </div>
                        <?php foreach ($arResult["MORE_PHOTOS"] as $propsPhoto) : ?>
                            <div class="product__slider-item">
                                <div class="product__slider-image">
                                    <a href="<?php echo $propsPhoto?>" class="product__slider-link fancybox" rel="product-<?=$arResult["ID"]?>">
                                        <img itemprop="image" src="<?php echo $propsPhoto?>" alt="<?= $arResult["NAME"]; ?>">
                                        <span class="product__slider-magnifier"></span>
                                    </a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="product__slider-nav">
                        <div class="product__slider-nav-item">
                            <div class="product__slider-nav-image">
                                <img itemprop="image" src="<?php echo $arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?= $arResult["NAME"]; ?>">
                            </div>
                        </div>
                        <?php foreach ($arResult["MORE_PHOTOS"] as $propsPhoto) : ?>
                            <div class="product__slider-nav-item">
                                <div class="product__slider-nav-image">
                                    <img itemprop="image" src="<?php echo $propsPhoto?>" alt="<?= $arResult["NAME"]; ?>">
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="goods-label-block">
                        <?php if ($arResult["PROPERTIES"]["NOVINKA_INTERNET_MAGAZIN"]["VALUE"] == "Да") : ?>
                            <div class="goods-label">новинка</div>
                        <?php endif; ?>
                        <?php if ($arResult["PROPERTIES"]["KHIT_PRODAZH_INTERNET_MAGAZIN"]["VALUE"] == "Да") : ?>
                            <div class="goods-label-hit">хит</div>
                        <?php endif; ?>
                        <?php if ($arResult["PROPERTIES"]["TREND_INTERNET_MAGAZIN"]["VALUE"] == "Да") : ?>
                            <div class="goods-label-trend">тренд</div>
                        <?php endif; ?>
                        <?php if ($arResult["PROPERTIES"]["FIKSIROVANNAYA_TSENA_ZNACHENIE"]["VALUE"]) : ?>
                            <div class="goods-label-fixprice">фиксированная цена</div>
                        <?php endif; ?>
                        <?php if ($arResult["PROPERTIES"]["AMOUNT_MANUAL"]["VALUE"]) : ?>
                            <div class="amount-manual-label">Осталось <?=$arResult["PROPERTIES"]["AMOUNT_MANUAL"]["VALUE"]?> шт.</div>
                        <?php endif; ?>
                    </div>
                    <?if ($arResult["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"]):?>
                        <div class="price-discount-percent">- <?=$arResult["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"]?>%</div>
                    <?endif?>
                </div>
            </div>
            <div class="col col-12 col-md-6 col-lg-5">
                <div class="product__rating">
                    <div class="product__rating-stars">
                        <div class="product__rating-star"></div>
                        <div class="product__rating-star"></div>
                        <div class="product__rating-star"></div>
                        <div class="product__rating-star"></div>
                        <div class="product__rating-star"></div>
                        <div class="product__rating-stars product__rating-stars--filled" style="width: <?=$arResult["RATING_ELEMENT_WIDTH"]?>%;">
                            <div class="product__rating-star _filled"></div>
                            <div class="product__rating-star _filled"></div>
                            <div class="product__rating-star _filled"></div>
                            <div class="product__rating-star _filled"></div>
                            <div class="product__rating-star _filled"></div>
                        </div>
                    </div>
                    <a href="#product-reviews" class="product__rating-reviews">Отзывы</a>
                    <span id="id<?=$arResult["ID"]?>"
                          class="product__favorites<?if ($wishlist == "delete"):?> active<?endif;?>"
                          onclick="<?=($USER->IsAuthorized()) ? 'WishList.'.$wishlist.'('.$arResult["ID"].');' : 'WishList.showUnregister();';?>ym(25218083, 'reachGoal', 'favorite_add');return false;">
                    </span>
                </div>
                <div class="product__settings">
                    <form class="d-flex flex-column justify-content-between">
                        <?if ($arResult["PROPERTIES"]["FIKSIROVANNAYA_TSENA_ZNACHENIE"]["VALUE"]):?>
                            <input type="hidden" name="fixed_price" value="<?=$arResult["PROPERTIES"]["FIKSIROVANNAYA_TSENA_ZNACHENIE"]["VALUE"]?>">
                            <input type="hidden" name="product_name" value="<?= $arResult["NAME"];?>">
                        <?endif;?>
                        <? if($arResult["PROPERTIES"]["PRODUCT_COLOR"]["VALUE"]): ?>
                            <div class="product__color">
                                <div class="product__color-title">Цвет</div>
                                <div class="product__color-items">
                                    <div class="product__color-item">
                                        <div class="product__color-value" style="background: #<?=$arResult["PROPERTIES"]["PRODUCT_COLOR"]["VALUE"]?>;"></div>
                                        <? if($arResult["PROPERTIES"]["PRODUCT_COLOR_DESCRIPTION"]["VALUE"]): ?>
                                            <div class="product__color-caption"><?=$arResult["PROPERTIES"]["PRODUCT_COLOR_DESCRIPTION"]["VALUE"]?></div>
                                        <? endif; ?>
                                    </div>
                                </div>
                                <? if (count($arResult["PRODUCT_COLOR_SIMILAR"]) > 0): ?>
                                    <div class="product__colors">
                                    <? foreach($arResult["PRODUCT_COLOR_SIMILAR"] as $productColorSimilar): ?>
                                        <div class="product__colors-item">
                                            <a href="<?=$productColorSimilar["URL"]?>" class="product__colors-link">
                                                <img src="<?=$productColorSimilar["IMAGE"]?>" alt="<?=$productColorSimilar["NAME"]?>">
                                            </a>
                                        </div>
                                    <? endforeach; ?>
                                    </div>
                                <? endif; ?>
                            </div>
                        <? endif; ?>

                        <div class="product__info">
                            <div class="product__info-stock">
                                <? if($arResult["CATALOG_AVAILABLE"] === "Y"): ?>
                                    <div class="product__in-stock">В наличии</div>
                                <? else: ?>
                                    <div class="product__out-of-stock">Нет в наличии</div>
                                <? endif; ?>
                            </div>
                            <div class="product__info-items">
                                <? if($arResult["PRODUCT_SIZE_TABLE"]): ?>
                                    <div class="product__info-item">
                                        <a data-target="#chooseSize" data-toggle="modal" class="product__info-link" href="javascript:void(0);" role="button">Таблица размеров</a>
                                    </div>
                                <? endif; ?>
                                <div class="product__info-item">
                                    <a data-target="#measureFoot" data-toggle="modal" class="product__info-link" href="javascript:void(0);" role="button">Как измерить стопу</a>
                                </div>
                            </div>
                            <? if($arResult["PRODUCT_SIZE_TABLE"]): ?>
                                <!-- Modal Size Table-->
                                <div class="modal fade" id="chooseSize" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-shadow"></div>
                                            <div class="modal-header">
                                                <div class="modal-title h5">Таблица размеров</div>
                                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col">
                                                        <a href="<?=$arResult["PRODUCT_SIZE_TABLE"]?>" class="fancybox">
                                                            <img src="<?=$arResult["PRODUCT_SIZE_TABLE"]?>" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endif; ?>
                            <!-- Modal-->
                            <div class="modal fade" id="measureFoot" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-shadow"></div>
                                        <div class="modal-header">
                                            <div class="modal-title h5">Как измерить стопу</div>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="content">
                                                        <?
                                                        $APPLICATION->IncludeFile(
                                                            SITE_DIR."include/".LANGUAGE_ID."/catalog_element/razmer_inc.php",
                                                            array(),
                                                            array(
                                                                "MODE"      => "html",
                                                                "NAME"      => "Редактирование включаемой области раздела",
                                                                "TEMPLATE"  => ""
                                                            )
                                                        );
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <? if(count($arResult["RAZMERA"]) > 0): ?>
                            <div class="product__size">
                                <div class="product__size-caption">
                                    Размер обуви в наличии
                                </div>
                                <div class="product__size-items">
                                    <?
                                    foreach ($arResult["RAZMERA"] as $key => $value) { ?>
                                        <div class="product__size-item">
                                            <input
                                                    class="razmer_input"
                                                    id="boots<?=$key?>"
                                                    type="radio"
                                                    value="<?=$key?>"
                                                    data-price="<?=$value["PRICE"]?>"
                                                    data-id="<?=$value["ID"]?>"
                                                    data-CanBuy="<?=$value["CAN_BUY"]?>"
                                                    data-Quantity="<?=$value["CATALOG_QUANTITY"]?>"
                                                    name="bootSize"
                                                <?=($value["CAN_BUY"] == "N")?" disabled":""?>
                                                <? if($value["CAN_BUY"] == "Y" && !$check && $arResult["MIN_PRICE"]["PRINT_VALUE"]==$value["PRICE"]) {?>
                                                    checked="checked"
                                                    <?
                                                    $check = true;
                                                } ?>
                                            >
                                            <label class="product__size-item-label" for="boots<?=$key?>"><?=$key?></label>
                                        </div>
                                    <? } ?>
                                </div>
                            </div>
                        <? endif; ?>
                        <div class="product__price">
                            <div class="product__quantity">
                                <div class="product__quantity-control">
                                    <a class="product__quantity-minus js-add_cart-minus" href="javascript:void(0);">
                                        <span class="icon">-</span>
                                    </a>
                                    <span class="product__quantity-num js-add_cart-quantity">1</span>
                                    <a class="product__quantity-plus js-add_cart-plus" href="javascript:void(0);">
                                        <span class="icon">+</span>
                                    </a>
                                </div>
                            </div>
                            <div style="display: none;">
                                <?if(count($arResult["REVIEWS"])>0){ ?>
                                <span itemprop="aggregateRating" itemscope itemtype="https://schema.org/AggregateRating">
                                    <span itemprop="ratingValue"><? echo $arResult["RATING_ELEMENT_WIDTH"]/20;?></span>
                                    <span itemprop="ratingCount"><?=count($arResult["REVIEWS"])?></span>
                                    
                                  </span>
                                <? } ?>
                                <div  itemprop="offers" itemtype="http://schema.org/Offer" itemscope>
                                    <link itemprop="url" href="<?=$APPLICATION->GetCurDir();?>" />
                                    <meta itemprop="availability" content="https://schema.org/InStock" />
                                    <meta itemprop="priceCurrency" content="RUB" />
                                    <meta itemprop="itemCondition" content="https://schema.org/UsedCondition" />
                                    <meta itemprop="price" content="<?=$arResult["MIN_PRICE"]["DISCOUNT_VALUE"];?>" />
                                                                 
                                </div>

                                <div itemprop="brand" itemtype="http://schema.org/Brand" itemscope>
                                    <meta itemprop="name" content="orthoboom" />
                                </div>
                            </div>
                            <div class="product__price-value product__price-value--current">
                                <?=str_replace('руб.', '&#8381;', $arResult["MIN_PRICE"]["PRINT_DISCOUNT_VALUE"])?>
                            </div>
                            <?if ($arResult["MIN_PRICE"]["VALUE"] != $arResult["MIN_PRICE"]["DISCOUNT_VALUE"]):?>
                                <div class="product__price-old">
                                    <div class="product__price-value product__price-value--old">
                                        <?=str_replace('руб.', '&#8381;', $arResult["MIN_PRICE"]["PRINT_VALUE"])?>
                                    </div>
                                    <div class="product__price-note">
                                        <div class="product__price-note-item">
                                            <span>Ваша экономия составляет</span>
                                            <? $economy = intval($arResult["MIN_PRICE"]["VALUE"]) - intval($arResult["MIN_PRICE"]["DISCOUNT_VALUE"]); ?>
                                            <span><?=number_format($economy, 0, '', ' ').'&nbsp;&#8381;'?></span>
                                        </div>
                                        <?if ($arResult["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"]):?>
                                            <div class="product__price-note-item product__price-note-item--discount">
                                                <span>Скидка <?=$arResult["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"]?>%</span>
                                                <span><?=str_replace('руб.', '&#8381;', $arResult["MIN_PRICE"]["PRINT_DISCOUNT_VALUE"]);?></span>
                                            </div>
                                        <? endif; ?>
                                    </div>
                                </div>
                            <?endif;?>
                        </div>
                        <div class="product__buy">
						
							<span class="catalog-detail--fav-button ui-icon"></span>
						
						
                            <button class="product__button" type="submit" onclick="ym(25218083, 'reachGoal', 'ZAKAZ');" onmousedown="try { rrApi.addToBasket(<?=$arResult["ID"]?>) } catch(e) {}">В корзину</button>
                            <a
                                    data-target="#buyOneClick" data-toggle="modal"
                                    class="product__button product__button--inverse product__button--small"
                                    href="javascript:void(0);"
                                    role="button"
                            >Купить в 1 клик</a>
                        </div>
                        <?/*
                        <?
                        $finish = '';
                        $arDiscounts = CCatalogDiscount::GetDiscountByProduct($arResult['ID'], $USER->GetUserGroupArray(), 'N');
                        //echo "<div style='display:none'>";
                        //print_r($arDiscounts);
                        //echo "</div>";
                        if (count($arDiscounts) == 1) {
                            reset($arDiscounts);
                            $firstKey = key($arDiscounts);
                            $ar = explode('#COUNTDOWN#', $arDiscounts[$firstKey]['NAME']);
                            if (isset($ar[1])) $finish = trim($ar[1]);
                        }
                        if ($finish):?>
                            <div class="countdown">ДО КОНЦА СКИДКИ ОСТАЛОСЬ</div>
                            <div id="timer"></div>
                        <?endif;?>
                        */?>
                    </form>
                </div>
                <?if(\Local\Location::getLocationTitle()): ?>
                    <div class="product__region">
                        <p><span class="product__region-label">Ваш регион:</span> <?=\Local\Location::getLocationTitle();?></p>
                        <?/*<p><span class="product__region-label">Доставка:</span> ориентировочно 20 ноября</p>*/?>
                    </div>
                <? endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col col-12 col-md-6 product__content product__content--left">
                <div class="product__tabs">
                    <div class="product__tabs-caption">
                        <div class="product__tabs-control _active" js-tabs-target="description">Описание</div>
                        <div class="product__tabs-control" js-tabs-target="properties">Характеристики</div>
                        <div class="product__tabs-control" js-tabs-target="question">Задать вопрос</div>
                    </div>
                    <div class="product__tabs-content">
                        <div class="product__tabs-section _active" js-tabs-data="description">
                            <div class="product__tabs-control _active" js-tabs-target="description">Описание</div>
                            <div class="product__tabs-section-content content" itemprop="description">
                                <?=$arResult["DETAIL_TEXT"]?>
                            </div>
                        </div>
                        <div class="product__tabs-section" js-tabs-data="properties">
                            <div class="product__tabs-control" js-tabs-target="properties">Характеристики</div>
                            <div class="product__tabs-section-content">
                                <? foreach ($arResult["DISPLAY_PROPERTIES"] as $displayProperty):?>
                                    <div class="product__charecteristic">
                                        <div class="product__charecteristic-name">
                                            <div>
                                                <span><?=$displayProperty["NAME"]?></span>
                                            </div>
                                        </div>
                                        <div class="product__charecteristic-value">
                                            <? if (is_array($displayProperty["~VALUE"])): ?>
                                                <?= implode(', ', $displayProperty["~VALUE"]);?>
                                            <? else: ?>
                                                <?= $displayProperty["~VALUE"]?>
                                            <? endif; ?>
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                        <div class="product__tabs-section" js-tabs-data="question">
                            <div class="product__tabs-control" js-tabs-target="question">Задать вопрос</div>
                            <div class="product__tabs-section-content">
                                <form method="#" class="product__question" name="productQuestion" id="product-question">
                                    <input id="product-id" name="product-id" type="hidden" value="<?=$arResult["ID"];?>">
                                    <input id="question-date" name="question-date" type="hidden" value="<?=mktime();?>">
                                    <div class="row">
                                        <div class="col col-12 col-md-6">
                                            <div class="product__question-field">
                                                <label class="product__question-label" for="question-name">Имя</label>
                                                <input class="product__question-input" type="text" id="question-name" name="question-name" placeholder="Ваше имя">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-12 col-md-6">
                                            <div class="product__question-field">
                                                <label class="product__question-label" for="question-phone">Телефон</label>
                                                <input class="product__question-input phone-mask" type="text" id="question-phone" name="question-phone" placeholder="+7 ( __ ) ___ __ __">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-12 col-md-6">
                                            <div class="product__question-field">
                                                <label class="product__question-label" for="question-email">Email</label>
                                                <input class="product__question-input" type="email" id="question-email" name="question-email" placeholder="example@mail.ru">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-12">
                                            <div class="product__question-field product__question-field--no-label">
                                                <textarea class="product__question-input product__question-input--textarea" name="question-text" placeholder="Введите Ваш вопрос"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-12">
                                            <? include $_SERVER["DOCUMENT_ROOT"] . "/include/agreement.php";?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-12">
                                            <div class="d-flex flex-center">
                                                <input class="product__button product__button--inverse" type="submit" name="qiestionClick" value="Отправить" disabled="disabled">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-12 col-md-6 product__content product__content--right">
                <? if(count($arResult["PRODUCT_ADVANTAGES"]) > 0): ?>
                    <div class="product__subtitle product__subtitle--advantages product__subtitle--inverse">Почему наша обувь?</div>
                    <div class="row product__advantages">
                        <? $advantagesIndex = 1 ?>
                        <? foreach ($arResult["PRODUCT_ADVANTAGES"] as $productAdvantage): ?>
                            <div class="col col-12 col-md-6 col-xl-3 product__advantage">
                                <div class="product__advantage-content">
                                    <div class="product__advantage-preview">
                                        <div class="product__advantage-border-frame">
                                            <div class="product__advantage-image" style="background-image: url('<?=$productAdvantage["IMAGE"]?>')"></div>
                                        </div>
                                        <div class="product__advantage-name">
                                            <?=$productAdvantage["NAME"]?>
                                        </div>
                                    </div>
                                    <div class="product__advantage-description">
                                        <?=$productAdvantage["DESCRIPTION"]?>
                                    </div>
                                </div>
                            </div>
                            <? $advantagesIndex++; ?>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="product__subtitle">Похожие товары</div>

                <div class="row">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "similar-products",
                        Array(
                            "ACTION_VARIABLE" => "action",
                            "ADD_PICT_PROP" => "MORE_PHOTO",
                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "ADD_TO_BASKET_ACTION" => "ADD",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "BACKGROUND_IMAGE" => "",
                            "BASKET_URL" => "/personal/cart/",
                            "BRAND_PROPERTY" => "",
                            "BROWSER_TITLE" => "-",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "N",
                            "COMPATIBLE_MODE" => "Y",
                            "CONVERT_CURRENCY" => "Y",
                            "CURRENCY_ID" => "RUB",
                            "CUSTOM_FILTER" => "",
                            "DATA_LAYER_NAME" => "dataLayer",
                            "DETAIL_URL" => "",
                            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                            "DISCOUNT_PERCENT_POSITION" => "bottom-right",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "ELEMENT_SORT_FIELD" => "rand",
                            "ELEMENT_SORT_FIELD2" => "",
                            "ELEMENT_SORT_ORDER" => "asc",
                            "ELEMENT_SORT_ORDER2" => "",
                            "ENLARGE_PRODUCT" => "PROP",
                            "ENLARGE_PROP" => "NEWPRODUCT",
                            "FILTER_NAME" => "arrFilter",
                            "HIDE_NOT_AVAILABLE" => "Y",
                            "HIDE_NOT_AVAILABLE_OFFERS" => "Y",
                            "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                            "IBLOCK_TYPE" => "1c_catalog",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "LABEL_PROP" => array("NEWPRODUCT"),
                            "LABEL_PROP_MOBILE" => array(),
                            "LABEL_PROP_POSITION" => "top-left",
                            "LAZY_LOAD" => "Y",
                            "LINE_ELEMENT_COUNT" => "3",
                            "LOAD_ON_SCROLL" => "N",
                            "MESSAGE_404" => "",
                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                            "MESS_BTN_BUY" => "Купить",
                            "MESS_BTN_DETAIL" => "Подробнее",
                            "MESS_BTN_LAZY_LOAD" => "Показать ещё",
                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                            "META_DESCRIPTION" => "-",
                            "META_KEYWORDS" => "-",
                            "OFFERS_CART_PROPERTIES" => array("ARTNUMBER","COLOR_REF","SIZES_SHOES","SIZES_CLOTHES"),
                            "OFFERS_FIELD_CODE" => array("",""),
                            "OFFERS_LIMIT" => "6",
                            "OFFERS_PROPERTY_CODE" => array(
                                0 => "OBUV_RAZMER",
                                1 => "ARTNUMBER",
                                2 => "SIZES_SHOES",
                                3 => "SIZES_CLOTHES",
                                4 => "COLOR_REF",
                                5 => "READ_PREVIEW",
                                6 => "GALLERY",
                            ),
                            "OFFERS_SORT_FIELD" => "rand",
                            "OFFERS_SORT_FIELD2" => "asc",
                            "OFFERS_SORT_ORDER" => "",
                            "OFFERS_SORT_ORDER2" => "",
                            "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
                            "OFFER_TREE_PROPS" => array("COLOR_REF","SIZES_SHOES","SIZES_CLOTHES"),
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Товары",
                            "PAGE_ELEMENT_COUNT" => "6",
                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                            "PRICE_CODE" => array("MAIN_BASE_PRICE"),
                            "PRICE_VAT_INCLUDE" => "Y",
                            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
                            "PRODUCT_DISPLAY_MODE" => "Y",
                            "PRODUCT_ID_VARIABLE" => "id",
                            "PRODUCT_PROPERTIES" => array("NEWPRODUCT","MATERIAL"),
                            "PRODUCT_PROPS_VARIABLE" => "prop",
                            "PRODUCT_QUANTITY_VARIABLE" => "",
                            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':true}]",
                            "PRODUCT_SUBSCRIPTION" => "Y",
                            "PROPERTY_CODE" => array("NEWPRODUCT",""),
                            "PROPERTY_CODE_MOBILE" => array(),
                            "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                            "RCM_TYPE" => "personal",
                            "SECTION_CODE" => "",
                            "SECTION_ID" => $arResult["IBLOCK_SECTION_ID"],
                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                            "SECTION_URL" => "",
                            "SECTION_USER_FIELDS" => array("",""),
                            "SEF_MODE" => "N",
                            "SET_BROWSER_TITLE" => "Y",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "Y",
                            "SET_META_KEYWORDS" => "Y",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "Y",
                            "SHOW_404" => "N",
                            "SHOW_ALL_WO_SECTION" => "Y",
                            "SHOW_CLOSE_POPUP" => "N",
                            "SHOW_DISCOUNT_PERCENT" => "Y",
                            "SHOW_FROM_SECTION" => "N",
                            "SHOW_MAX_QUANTITY" => "N",
                            "SHOW_OLD_PRICE" => "Y",
                            "SHOW_PRICE_COUNT" => "1",
                            "SHOW_SLIDER" => "Y",
                            "SLIDER_INTERVAL" => "3000",
                            "SLIDER_PROGRESS" => "N",
                            "TEMPLATE_THEME" => "blue",
                            "USE_ENHANCED_ECOMMERCE" => "Y",
                            "USE_MAIN_ELEMENT_SECTION" => "N",
                            "USE_PRICE_COUNT" => "N",
                            "USE_PRODUCT_QUANTITY" => "N"
                        )
                    );?>
                </div>
            </div>
        </div>

        <? if(count($arResult["SHOW_ELEMENT"]) > 0):
            global $arrFilter_show;
            $arrFilter_show = array("ID" => $arResult["SHOW_ELEMENT"]);
            ?>
            <div class="row">
                <div class="col">
                    <div class="product__subtitle">Вы смотрели</div>
                    <div class="row">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:catalog.section",
                            "similar-products",
                            Array(
                                "ACTION_VARIABLE" => "action",
                                "ADD_PICT_PROP" => "MORE_PHOTO",
                                "ADD_PROPERTIES_TO_BASKET" => "Y",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "ADD_TO_BASKET_ACTION" => "ADD",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "BACKGROUND_IMAGE" => "",
                                "BASKET_URL" => "/personal/cart/",
                                "BRAND_PROPERTY" => "",
                                "BROWSER_TITLE" => "-",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_TYPE" => "N",
                                "COMPATIBLE_MODE" => "Y",
                                "CONVERT_CURRENCY" => "Y",
                                "CURRENCY_ID" => "RUB",
                                "CUSTOM_FILTER" => "",
                                "DATA_LAYER_NAME" => "dataLayer",
                                "DETAIL_URL" => "",
                                "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                                "DISCOUNT_PERCENT_POSITION" => "bottom-right",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "ELEMENT_SORT_FIELD" => "rand",
                                "ELEMENT_SORT_ORDER" => "asc",
                                "ELEMENT_SORT_FIELD2" => "",
                                "ELEMENT_SORT_ORDER2" =>  "",
                                "ENLARGE_PRODUCT" => "PROP",
                                "ENLARGE_PROP" => "NEWPRODUCT",
                                "FILTER_NAME" => "arrFilter_show",
                                "HIDE_NOT_AVAILABLE" => "Y",
                                "HIDE_NOT_AVAILABLE_OFFERS" => "Y",
                                "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                                "IBLOCK_TYPE" => "1c_catalog",
                                "INCLUDE_SUBSECTIONS" => "Y",
                                "LABEL_PROP" => array("NEWPRODUCT"),
                                "LABEL_PROP_MOBILE" => array(),
                                "LABEL_PROP_POSITION" => "top-left",
                                "LAZY_LOAD" => "Y",
                                "LINE_ELEMENT_COUNT" => "3",
                                "LOAD_ON_SCROLL" => "N",
                                "MESSAGE_404" => "",
                                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                                "MESS_BTN_BUY" => "Купить",
                                "MESS_BTN_DETAIL" => "Подробнее",
                                "MESS_BTN_LAZY_LOAD" => "Показать ещё",
                                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                                "META_DESCRIPTION" => "-",
                                "META_KEYWORDS" => "-",
                                "OFFERS_CART_PROPERTIES" => array("ARTNUMBER","COLOR_REF","SIZES_SHOES","SIZES_CLOTHES"),
                                "OFFERS_FIELD_CODE" => array("",""),
                                "OFFERS_LIMIT" => "6",
                                "OFFERS_PROPERTY_CODE" => array(
                                    0 => "OBUV_RAZMER",
                                    1 => "ARTNUMBER",
                                    2 => "SIZES_SHOES",
                                    3 => "SIZES_CLOTHES",
                                    4 => "COLOR_REF",
                                    5 => "READ_PREVIEW",
                                    6 => "GALLERY",
                                ),
                                "OFFERS_SORT_FIELD" => "rand",
                                "OFFERS_SORT_FIELD2" => "asc",
                                "OFFERS_SORT_ORDER" => "",
                                "OFFERS_SORT_ORDER2" => "",
                                "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
                                "OFFER_TREE_PROPS" => array("COLOR_REF","SIZES_SHOES","SIZES_CLOTHES"),
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => ".default",
                                "PAGER_TITLE" => "Товары",
                                "PAGE_ELEMENT_COUNT" => "6",
                                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                "PRICE_CODE" => array("MAIN_BASE_PRICE"),
                                "PRICE_VAT_INCLUDE" => "Y",
                                "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
                                "PRODUCT_DISPLAY_MODE" => "Y",
                                "PRODUCT_ID_VARIABLE" => "id",
                                "PRODUCT_PROPERTIES" => array("NEWPRODUCT","MATERIAL"),
                                "PRODUCT_PROPS_VARIABLE" => "prop",
                                "PRODUCT_QUANTITY_VARIABLE" => "",
                                "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':true}]",
                                "PRODUCT_SUBSCRIPTION" => "Y",
                                "PROPERTY_CODE" => array("NEWPRODUCT",""),
                                "PROPERTY_CODE_MOBILE" => array(),
                                "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                                "RCM_TYPE" => "personal",
                                "SECTION_CODE" => "",
                                "SECTION_ID" => $arResult["IBLOCK_SECTION_ID"],
                                "SECTION_ID_VARIABLE" => "SECTION_ID",
                                "SECTION_URL" => "",
                                "SECTION_USER_FIELDS" => array("",""),
                                "SEF_MODE" => "N",
                                "SET_BROWSER_TITLE" => "Y",
                                "SET_LAST_MODIFIED" => "N",
                                "SET_META_DESCRIPTION" => "Y",
                                "SET_META_KEYWORDS" => "Y",
                                "SET_STATUS_404" => "N",
                                "SET_TITLE" => "Y",
                                "SHOW_404" => "N",
                                "SHOW_ALL_WO_SECTION" => "Y",
                                "SHOW_CLOSE_POPUP" => "N",
                                "SHOW_DISCOUNT_PERCENT" => "Y",
                                "SHOW_FROM_SECTION" => "N",
                                "SHOW_MAX_QUANTITY" => "N",
                                "SHOW_OLD_PRICE" => "Y",
                                "SHOW_PRICE_COUNT" => "1",
                                "SHOW_SLIDER" => "Y",
                                "SLIDER_INTERVAL" => "3000",
                                "SLIDER_PROGRESS" => "N",
                                "TEMPLATE_THEME" => "blue",
                                "USE_ENHANCED_ECOMMERCE" => "Y",
                                "USE_MAIN_ELEMENT_SECTION" => "N",
                                "USE_PRICE_COUNT" => "N",
                                "USE_PRODUCT_QUANTITY" => "N"
                            )
                        );?>
                    </div>
                </div>
            </div>
        <? endif; ?>

        <? if(count($arResult["VIDEO"]) > 0): ?>
            <div class="row">
                <div class="col col-12">
                    <div class="product__subtitle">Видео</div>
                    <div class="product__video">
                        <div class="row">
                            <? foreach($arResult["VIDEO"] as $video): ?>
                                <div class="col-12 col-sm-6 col-lg-4">
                                    <div class="product__video-item">
                                        <iframe src="<?=$video?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        <? endif; ?>
        <div class="row" id="product-reviews">
            <div class="col col-12">
                <div class="d-flex">
                    <div class="product__subtitle">Отзывы</div>
                    <a data-target="#review-modal" data-toggle="modal" class="product__button product__button--inverse product__button--small" href="javascript:void(0);" role="button">Написать отзыв</a>
                </div>
                <div class="product__reviews">

                    <? foreach($arResult["REVIEWS"] as $review): ?>
                        <div class="product__reviews-item" itemprop="review" itemtype="http://schema.org/Review" itemscope>
                            <div style="display: none;">
                                <meta itemprop="datePublished" content="<?= date_format(date_create($review["FIELDS"]["DATE_ACTIVE_FROM"]), 'Y-m-d')?>">
                                
                                <div itemprop="author" itemtype="http://schema.org/Person" itemscope>
                                  <meta itemprop="name" content="<?=$review["PROPERTIES"]["AUTHOR"]["VALUE"]?>" />
                                </div>
                                <div itemprop="reviewRating" itemtype="http://schema.org/Rating" itemscope>
                                    <meta itemprop="worstRating" content="1">
                                  <meta itemprop="ratingValue" content="<?=$review["PROPERTIES"]["RATING"]["VALUE"];?>" />
                                  <meta itemprop="bestRating" content="5" />
                                </div>

                                <div itemprop="itemReviewed" itemscope itemtype="https://schema.org/Organization">
                                    <meta itemprop="name" content="ORTHOBOOM">
                                    <meta itemprop="telephone" content="+7 (499) 430-42-38">
                                    <link itemprop="url" href="<?=$APPLICATION->GetCurDir();?>"/>
                                    <meta itemprop="email" content="zakaz@orthoboom.ru">
                                    <p itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
                                        <meta itemprop="addressLocality" content="Москва">
                                        <meta itemprop="streetAddress" content="Кировоградская, д.13А">
                                    </p>
                                </div>

                            </div>
                            <div class="product__reviews-meta">
                                <div class="product__reviews-rating">
                                    <div class="product__rating-stars">
                                        <div class="product__rating-star"></div>
                                        <div class="product__rating-star"></div>
                                        <div class="product__rating-star"></div>
                                        <div class="product__rating-star"></div>
                                        <div class="product__rating-star"></div>
                                        <div class="product__rating-stars product__rating-stars--filled" style="width: <?=$review["PROPERTIES"]["RATING"]["VALUE"] /5 * 100?>%;">
                                            <div class="product__rating-star _filled"></div>
                                            <div class="product__rating-star _filled"></div>
                                            <div class="product__rating-star _filled"></div>
                                            <div class="product__rating-star _filled"></div>
                                            <div class="product__rating-star _filled"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product__reviews-info">
                                    <div class="product__reviews-author">
                                        <?=$review["PROPERTIES"]["AUTHOR"]["VALUE"]?>
                                    </div>
                                    <div class="product__reviews-date">
                                        <?=$review["FIELDS"]["DATE_ACTIVE_FROM"]?>
                                    </div>
                                </div>
                            </div>
                            <div class="product__reviews-text" itemprop="reviewBody">
                                <?=$review["PROPERTIES"]["TEXT"]["~VALUE"]["TEXT"]?>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal review-modal fade" id="review-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <form class="modal-content" name="NEW_REVIEW" id="NEW_REVIEW">
            <div class="modal-header">
                <div class="modal-title h5">ОТЗЫВ О Товаре</div>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="rating">
                    <input id="ratingValue" type="text" hidden>
                    <select class="to-rate" name="rating" autocomplete="off">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5" selected="selected">5</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="userName">Имя</label>
                    <input class="form-control" id="userName" name="author" type="text" required>
                </div>
                <div class="form-group">
                    <label for="reviewArea">Отзыв</label>
                    <textarea class="form-control" id="reviewArea" name="text" rows="4" required></textarea>
                </div>
                <input class="form-control" id="element" name="element" type="hidden" value="<?=$arResult["ID"];?>">
                <input class="form-control" id="timestamp" name="timestamp" type="hidden" value="<?=mktime();?>">
                <? include $_SERVER["DOCUMENT_ROOT"] . "/include/agreement.php";?>
                <button class="btn-main btn transition d-block ml-auto mr-auto" type="submit">Отправить</button>
            </div>
        </form>
    </div>
</div>

<div class="modal form-modal fade" id="buyOneClick" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title text-center h5"><? echo 'Быстрый заказ'; ?></div>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times</span>
                </button>
            </div>
            <form method="#" class="modal-body" id="buyOneClickForm" name="buyOneClickForm" onsubmit="ym(25218083, 'reachGoal', 'bistriyzakaz');" novalidate>
                <input type="hidden" name="size_buyoneclick" value="" />
                <input type="hidden" name="amount_buyoneclick" value="1" />
                <input type="hidden" name="current_product_id" value="" />
                <div class="row orm-modal">
                    <div class="col-12">
                        <div id="error-phone-block"></div>
                        <div class="modal-input-wrapper flex-container-w100 flex-side">
                            <label>Ваше Ф.И.О.</label>
                            <input type="text" name="USER_NAME" maxlength="50"/>
                        </div>
                        <div class="modal-input-wrapper flex-container-w100 flex-side">
                            <label>Телефон*</label>
                            <input class="phone-mask" type="text" name="USER_PHONE" maxlength="50" required minlength="10"/>
                        </div>
                        <?/*
                        <div class="modal-input-wrapper flex-container-w100 flex-side">
                            <label>Адрес доставки</label>
                            <input type="text" name="USER_ADDRESS" />
                        </div>
                        */?>
                        <? include $_SERVER["DOCUMENT_ROOT"] . "/include/agreement.php";?>
                        <div class="modal-input-wrapper flex-container-w100 flex-center">
                            <input class="mybtn transition yourCity-btn" type="submit" name="oneClick"
                                   value="Купить" disabled="disabled">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- data-target="#thanksForOrder" data-toggle="modal" -->

<div class="modal form-modal fade" id="thanksForOrder" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title text-center h5" style="margin-bottom:20px;">Спасибо за заказ! Менеджер свяжется с вами как можно скорее.</div>
            </div>
            <!-- <div class="row">
                <div class="col-auto modal_full">
                    <a href="#" class="mybtn transition yourCity-btn closeModal" data-error_kol="Закрыть">Закрыть</a>
                </div>
            </div> -->
        </div>
    </div>
</div>
<div class="modal form-modal fade" id="inCartClick" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered inCartModal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title text-center h5" data-error_kol="Извините, но на складе нет выбранного количества модели">Товар добавлен в корзину</div>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times</span>
                </button>
            </div>
            <div class="row js-hidden desc">
                <div class="col-12">Вы можете скорректировать количество данного товара или выбрать другую модель</div>
            </div><div class="row">
                <div class="col-6 modal_full">
                    <a href="#" class="mybtn transition yourCity-btn closeModal" data-error_kol="Выбрать другую модель">Продолжить покупки</a>
                </div>
                <div class="col-6 modal_full">
                    <a href="/personal/cart/" class="mybtn transition yourCity-btn full">Перейти в корзину</a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.plugin.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.countdown.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.countdown-ru.js"></script>
<script>
    $(document).ready(function() {
        var top = 0;
        $(this).find('.goods-label-block').children().each(function() {
            if (!$(this).hasClass('amount-manual-label')) {
                $(this).css('top', top + 'px');
                top += 21;
            } else {
                var h = $('.slider-for').height() - 21;
                $(this).css('top', h + 'px');
            }
        });
        $('.goods-label-block .goods-label-fixprice').css('left', '15px');


        <?if ($finish):?>
        var finish = '<?=$finish?>';
        var ar = finish.trim().split(' ');
        var d = ar[0];
        var t = ar[1];
        var arD = d.trim().split('.');
        var arT = t.trim().split(':');
        var finishDay = new Date(arD[2], parseInt(arD[1])-1, arD[0], arT[0], arT[1], arT[2], 0);
        $("#timer").countdown({until: finishDay, format: 'dHMS'});
        <?endif;?>

    });
</script>
<script type="text/javascript">
    var path_to_ajax = '<?=SITE_DIR."include/".LANGUAGE_ID."/catalog_element/ajax_add_basket.php"?>';
    var path_to_basket = '<?=$arParams["BASKET_URL"]?>';
    var path_to_add_review = '<?=SITE_DIR."include/".LANGUAGE_ID."/catalog_element/ajax_add_review.php"?>';
    var path_to_add_question = '<?=SITE_DIR."include/".LANGUAGE_ID."/catalog_element/ajax_add_question.php"?>';
    var ofrs_prices = <?= !empty($arResult['OFRS_PRICES']) ? \Bitrix\Main\Web\Json::encode($arResult['OFRS_PRICES']) : "''" ?>;
    //console.log(ofrs_prices);
</script>

<?
if (isset($_GET['USER_NAME']) && isset($_GET['USER_PHONE']) && isset($_GET['size_buyoneclick']) && isset($_GET['amount_buyoneclick'])/*  && isset($_GET['oneClick']) */) {

// Формируем заказ
// Определяем товар/торговое предложение
    $productId = $arResult['ID'];
    $quantity = 1;
    if(isset($_GET['current_product_id']) && !empty($_GET['current_product_id'])) {
        $productId = $_GET['current_product_id'];
    }
    if(isset($_GET['amount_buyoneclick']) && !empty($_GET['amount_buyoneclick'])) {
        $quantity = $_GET['amount_buyoneclick'];
    }


// Входящие параметры и константы
    define('PAY_SYSTEM_ID', 4); // Yandex
    define('DEFAULT_LOCATION_ID', '0000073738'); // Москва

// Получаем данные о товаре по его ID
    $dbItems = CIBlockElement::GetList(
        array(),
        array('ID' => IntVal($productId)),
        false,
        false,
        array('ID', 'IBLOCK_ID', 'NAME', 'IBLOCK_EXTERNAL_ID', 'EXTERNAL_ID')
    );
    if ($arItem = $dbItems->GetNext()) {
        $arItem['PRICE'] = CCatalogProduct::GetOptimalPrice($arItem['ID'], 1, $USER->GetUserGroupArray(), "N");

        // Создаем корзину и добавляем туда товар, 1шт
        $basket = \Bitrix\Sale\Basket::create(SITE_ID);
        $basketItem = $basket->createItem("catalog", $arItem['ID']);

        $basketItem->setFields(
            array(
                'PRODUCT_ID' => $arItem['ID'],
                'NAME' => $arItem['NAME'],
                'BASE_PRICE' =>$arItem['PRICE']['RESULT_PRICE']['BASE_PRICE'],
                'PRICE' => $arItem['PRICE']['RESULT_PRICE']['DISCOUNT_PRICE'],
                'CURRENCY' => $arItem['PRICE']['RESULT_PRICE']['CURRENCY'],
                'QUANTITY' => $quantity,

                "PRODUCT_PROVIDER_CLASS" => "Bitrix\Catalog\Product\CatalogProvider",
                "CATALOG_XML_ID" => $arItem["IBLOCK_EXTERNAL_ID"],
                "PRODUCT_XML_ID" => $arItem["EXTERNAL_ID"],
            )
        );

        // Создаем заказ и привязываем корзину, перерасчет происходит автоматически
        $order = \Bitrix\Sale\Order::create(SITE_ID, ($USER->IsAuthorized()) ? $USER->GetID() : \CSaleUser::GetAnonymousUserID());
        $order->setPersonTypeId(1); // Физ. лицо
        $order->setBasket($basket);

        // Создаём одну отгрузку и устанавливаем способ доставки - "Без доставки" (он служебный)
        $shipmentCollection = $order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem();
        $service = \Bitrix\Sale\Delivery\Services\Manager::getById(\Bitrix\Sale\Delivery\Services\EmptyDeliveryService::getEmptyDeliveryServiceId());
        $shipment->setFields(array(
            'DELIVERY_ID' => $service['ID'],
            'DELIVERY_NAME' => $service['NAME'],
        ));
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        $arResult['basket'] = $basket;
        foreach ($basket as $item) {
            $shipmentItem = $shipmentItemCollection->createItem($item);
            $shipmentItem->setQuantity($item->getQuantity());
        }

        // Создаём оплату
        $paymentCollection = $order->getPaymentCollection();
        $payment = $paymentCollection->createItem();
        $paySystemService = \Bitrix\Sale\PaySystem\Manager::getObjectById(PAY_SYSTEM_ID);
        $payment->setFields(array(
            'PAY_SYSTEM_ID' => $paySystemService->getField("PAY_SYSTEM_ID"),
            'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
            'SUM' => $order->getPrice(),
        ));

        // Устанавливаем свойства
        $propertyCollection = $order->getPropertyCollection();
        $nameProp = $propertyCollection->getPayerName();
        $nameProp->setValue(htmlspecialcharsbx($_GET['USER_NAME']));
        $emailProp = $propertyCollection->getPhone();
        $emailProp->setValue(htmlspecialcharsbx($_GET['USER_PHONE']));
        //todo: включить локацию для быстрого заказа
        //$locProp = $propertyCollection->getDeliveryLocation();
        //$locProp->setValue(DEFAULT_LOCATION_ID);

        // Сохраняем
        $order->doFinalAction(true);
        $order->save();
    }


// Формируем письмо
    $url = 'https://orthoboom.ru' . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

    $to = 'rusak.ai@julianna.ru';
    //$to = 'ko-alex@mail.ru';
    $headers =  'From: "Интернет-магазин «ORTHOBOOM»" <noreply@orthoboom.ru>' . "\r\n" .
        'Reply-To: "Интернет-магазин «ORTHOBOOM»" <noreply@orthoboom.ru>' . "\r\n" .
        'MIME-Version: 1.0' . "\r\n".
        'Content-type: text/html; charset=windows-1251' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    $message = '<p><b>Товар</b>: ' . '<a href="' . $url . '">' . $url . '</a></p>';
    $message .= '<p><b>Размер</b>: ' . $_GET['size_buyoneclick'] . '</p>';
    $message .= '<p><b>Количество</b>: ' . $_GET['amount_buyoneclick'] . '</p>';
    $message .= '<p><b>Имя</b>: ' . $_GET['USER_NAME'] . '</p>';
    $message .= '<p><b>Адрес доставки</b>: ' . $_GET['USER_ADDRESS'] . '</p>';
    $message .= '<p><b>Телефон</b>: ' . $_GET['USER_PHONE'] . '</p>';
    $message .= '<p><small>Это сообщение имейет информационный характер и сформировано автоматически. Пожалуйста, не отвечайте на него.</small></p>';
    mail($to, 'Заказ в 1 клик из интернет-магазина «ORTHOBOOM»', $message, $headers);

    $arEventFields = array(
        'ORDER_ID'             => mt_rand(),
        'ORDER_LIST'           => str_replace("'", '"', $arResult['NAME']) . ' (' . $_GET['size_buyoneclick'] . ') x ' . $_GET['amount_buyoneclick'] . ' пар',
        'PRICE'            	   => round($arResult['MIN_PRICE']['DISCOUNT_VALUE'] * $_GET['amount_buyoneclick']),
        'CONTACT_FULL_NAME'    => $_GET['USER_NAME'],
        'CONTACT_PHONE'   	   => $_GET['USER_PHONE'],
        'CONTACT_FULL_ADDRESS' => $_GET['USER_ADDRESS'],
    );
    CEvent::SendImmediate('SALE_NEW_ORDER_1CLICK', 's1', $arEventFields, 'N');

    ?>
    <script>
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            'ecommerce': {
                'currencyCode': 'RUB',
                'purchase': {
                    'actionField': {
                        'id': 'QUICK_ORDER_<?=$arResult["ID"]?>',
                        'affiliation': 'ORTHOBOOM',
                    },
                    'products': [
                        {
                            'name': '<?=str_replace("'", '"', $arResult["NAME"])?>',
                            'id': 'QUICK_ORDER_PRODUCT_<?=$arResult["ID"]?>',
                            'price': <?=$arResult["MIN_PRICE"]["DISCOUNT_VALUE"]?>,
                            'quantity': 1
                        }
                    ]
                }
            },
            'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Purchase',
            'gtm-ee-event-non-interaction': 'False',
        });
        location.href = '<?=$url?>';
    </script>
    <?
}
?>

<?/*retail rocket*
<script type="text/javascript">
    (window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function() {
        try{ rrApi.view(<?=$arResult["ID"]?>); } catch(e) {}
    })
</script>
*/?>

<script>

			$(".catalog-detail__orthoboom").find(".catalog-detail--fav-button").on("click", function () {
				
				var itemId = $(this).parents(".catalog-detail--item").eq(0).data("iblock-element-id");
				localCatalog.toggleFav(itemId);
			});
</script>