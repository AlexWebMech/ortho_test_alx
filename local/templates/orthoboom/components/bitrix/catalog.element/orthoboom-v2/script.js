
function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};


BX.ready(function(){
	var fixed_price = $('input[name=fixed_price]').val();

    $('a.js-add_cart-plus').on('click', function () {
        var choose_quantity = parseInt($('input[name=bootSize]:checked').attr("data-quantity"));
        var current_quantity = parseInt($('.product__quantity-num.js-add_cart-quantity').text());
        if(current_quantity < choose_quantity) {
            $('.product__quantity-num.js-add_cart-quantity').text(current_quantity+1);
		}
    });
    $('a.js-add_cart-minus').on('click', function () {
        var current_quantity = parseInt($('.product__quantity-num.js-add_cart-quantity').text());
        if(current_quantity-1 > 0) {
            $('.product__quantity-num.js-add_cart-quantity').text(current_quantity-1);
        }
    });
    $('input[name=bootSize]:checked').on('change', function () {
            $('.product__quantity-num.js-add_cart-quantity').text(1);
    });


    var anchor = location.hash.slice(1);
    if (anchor)
        $('.razmer_input[data-id=' + anchor + ']').trigger('click');

    // $('input[name=size_buyoneclick]').val($('.razmer_input:checked').val());

    $('a[data-target="#buyOneClick"]').on('click', function (e) {
        $('input[name=amount_buyoneclick]').val($('.product__quantity-num').text());
    });
    $('input[name=bootSize]').on('click', function (e) {
        var choose = parseInt($(this).attr("data-id"));

        // исключаем повторную отправку данных, если кликнули по уже выбранному размеру
        if(parseInt($('input[name=current_product_id]').val()) !== choose) {
            dataLayer.push({
                'event': 'view_item',
                'items': [
                    {
                        'id': choose,
                        'google_business_vertical': 'retail'
                    }
                ]
            });
            console.log('выбран размер торгового предложения с ID ' + choose);
        }

        $('input[name=size_buyoneclick]').val($(this).val());
        $('input[name=current_product_id]').val(choose);
		if (ofrs_prices[choose]) {
			if (!fixed_price) {
				$('.product__price-value.product__price-value--current').html(ofrs_prices[choose]['PRINT_DISCOUNT_VALUE'].replace(/руб./, '&#8381;'));
			}
			$('.product__price-value.product__price-value--old').html(ofrs_prices[choose]['PRINT_VALUE'].replace(/руб./, '&#8381;'));
		}
    });
    $('.razmer_input:first').click();

    var active_prod = getUrlParameter('prod');
    $(".razmer_input[data-id='" + active_prod + "']").click();

    $('.product__buy button').on('click', function (e) {
    	e.preventDefault();
    	if(!$(this).hasClass( "noclick" )){
			var choose = parseInt($('input[name=bootSize]:checked').attr("data-id"));
			var current_quantity_for_ajax = parseInt($('.product__quantity-num.js-add_cart-quantity').text());
			var product_name = $('input[name=product_name]').val() +  ' (' + $('input[name=bootSize]:checked').val() + ')';
			if(choose){
				//$(this).toggleClass( "noclick" );
				$.ajax({
					method: "POST",
					url: path_to_ajax,
					data: { id: choose, quantity: current_quantity_for_ajax, fixed_price: fixed_price, product_name: product_name }
				})
                    .done(function( data ) {
						//console.log(data);
                        //data = JSON.parse(data);
                        var inCartClick = $('#inCartClick');

                        if(data.error || !data){
                            if(data.error.indexOf('": ', 0)){
                                var error = inCartClick.find('.modal-title').attr('data-error_kol');
                                var closeModalText = inCartClick.find('.closeModal').attr('data-error_kol');
                                inCartClick.addClass('error');
                                inCartClick.find('.modal-title').text(error);
                                inCartClick.find('.closeModal').text(closeModalText);
                                inCartClick.find('.js-hidden').removeClass('js-hidden');
                                inCartClick.modal();
                            }
                            else{
                                alert(data.error);
                            }

                            $(this).toggleClass( "noclick" );
						}
                        else{
                            inCartClick.modal();
                            $('.header-cart-icon span').text(data.CART);
                            //location.href = path_to_basket;
						}
					});
			}
        }
    });
    $( "#NEW_REVIEW" ).submit(function( event ) {
        event.preventDefault();
        var form = $('#NEW_REVIEW');
        $.ajax( {
            type: "POST",
            url: path_to_add_review,
            data: form.serialize(),
            success: function( response ) {
                console.log( response );
                if(response == "OK"){
                	location.href = location.href;
				}
            }
        });

    });
    $( "#product-question" ).submit(function( event ) {
        event.preventDefault();
        var form = $('#product-question');
        $.ajax( {
            type: "POST",
            url: path_to_add_question,
            data: form.serialize(),
            success: function( response ) {
                console.log( response );
                if(response == "OK"){
                	location.href = location.href;
				}
            }
        });

    });
    var loading;
    $('body').on('click', '.paggination__item', function (e) {
        e.preventDefault();
        if (!loading) {
            loading = true;
            $.post($(this).attr('href'), {is_ajax: 'y'}, function (data) {
                $('body').find('#ajax_rewiew').html("");
                var result = data.split("<!--RestartBufferPagination-->");
                $('body').find('#ajax_rewiew').append(result[1]);
                $('.rating-stat').each(function() {
                    $(this).barrating({
                        readonly: true,
                        initialRating: $(this).data('rate'),
                        theme: 'fontawesome-stars-o'
                    });
                });
                toggleText();
                loading = false;
            });
        }
    });
    $(document).on('click', '.closeModal', function(){
        $('.modal').modal('hide');
    });

    //Кастомные функции для валидатора телефона, так как маска мешает
    $.validator.addMethod("minLengthPhone", function(value, element) {
        return value.replace(/\D+/g, '').length > 10;
    }, "Телефон: минимум 10 символов");

    $.validator.addMethod("requiredphone", function (value, element) {
            return value.replace(/\D+/g, '').length > 1;
    }, "Телефон: поле обязательно для заполнения");

    $('form#buyOneClickForm').validate(
    {
        rules:
        {
            USER_PHONE:
            {
                requiredphone: true,
                minLengthPhone: true,
            }
        },
        errorPlacement: function(error, element) {
            error.appendTo('#error-phone-block');
        },
        submitHandler: function (form) {
            $(form).removeClass('show').css('display', 'none');
            $('#thanksForOrder').addClass('show').css('display', 'block');
            setTimeout(function () {
                form.submit();
            }, 1000);
        }
    });

    $(function () {
        var productTabs = $('.product__tabs'),
            productTabsControl = productTabs.find('.product__tabs-control'),
            productTabsSection = productTabs.find('.product__tabs-section'),
            activeClass = '_active';

        $(productTabsControl).click(function() {
            var controlTarget = $(this).attr('js-tabs-target');
            productTabsControl.removeClass(activeClass);
            productTabsSection.removeClass(activeClass);
            $('.product__subtitle.product__subtitle--advantages').removeClass(activeClass);
            $('.product__advantages').removeClass(activeClass);
            $('.product__tabs-control[js-tabs-target=' + controlTarget + ']').addClass(activeClass);
            $('.product__tabs-section[js-tabs-data=' + controlTarget + ']').addClass(activeClass);
        });
    });

    $(function () {
        var productAdvantagescontrol = $('.product__subtitle.product__subtitle--advantages'),
            productAdvantages = $('.product__advantages'),
            activeClass = '_active';

        $(productAdvantagescontrol).click(function() {
            if ($(window).width() <= '767') {
                if (!$(this).hasClass(activeClass)) {
                    $('.product__tabs-control').removeClass(activeClass);
                    $('.product__tabs-section').removeClass(activeClass);

                    $(this).toggleClass(activeClass);
                    productAdvantages.toggleClass(activeClass);
                }
            }
        });
    });

    $(function () {
        $('.product__slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: '<div class="product__slider-btn product__slider-btn--prev"><span class="icon-angle-left"></span></div>',
            nextArrow: '<div class="product__slider-btn product__slider-btn--next"><span class="icon-angle-right"></span></div>',
            dots: false,
            fade: true,
            asNavFor: '.product__slider-nav'
        });

        $('.product__slider-nav').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.product__slider',
            arrows: true,
            dots: false,
            focusOnSelect: true,
            prevArrow: '<div class="product__slider-nav-btn product__slider-nav-btn--prev"><span class="icon-angle-left"></span></div>',
            nextArrow: '<div class="product__slider-nav-btn product__slider-nav-btn--next"><span class="icon-angle-right"></span></div>',
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 575,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    });



});