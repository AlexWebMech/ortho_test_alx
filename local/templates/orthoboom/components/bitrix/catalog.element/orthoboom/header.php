<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$lang =  GetLang(); // defined in init.php
require_once('lang_' . $lang . '.php');

require_once 'include/Mobile_Detect.php';
$detect = new Mobile_Detect;
$isMobile = $detect->isMobile();

$curPage = $APPLICATION->GetCurPage(true);
?><!DOCTYPE html>
<html>
<!--[if lte IE 9]> <html class="ie8"> <![endif]-->
<head>

    <title><?$APPLICATION->ShowTitle();?></title>
	<meta name="description" content="Интернет-магазин детской ортопедической обуви ORTHOBOOM с доставкой по всей России. Собственное производство и гарантия от производителя 45 дней." />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-PNKTQ7Z');</script>
	<!-- End Google Tag Manager -->
	<meta name="google-site-verification" content="Y5qrPJSwzEB1WN3uj5J1n9SuVegkF42IwSQC1xGrCZo" />
	<meta name="yandex-verification" content="a02db5d319195e2a" />
    <?$APPLICATION->ShowHead();?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/external/bootstrap/css/bootstrap.min.css");?>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/external/bootstrap/css/bootstrap-reboot.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/external/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/external/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/external/rateYo/rateYo.min.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/fonts/icomoon/style.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/external/slick/slick.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/external/jquery.fullpage/jquery.fullpage.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/external/animate/animate.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/external/nouislider/nouislider.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/external/swipebox-master/swipebox.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/external/bootstrap-select/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/external/fontawesome/fontawesome.css"/>
    <!--<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/component/at/sale.order.ajax/style.css"/>-->
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/external/jquery-bar-rating/dist/themes/fontawesome-stars-o.css"/>
    <!-- fancybox -->
    <link href="<?=SITE_TEMPLATE_PATH?>/js/fancy/jquery.fancybox.css" rel="stylesheet" />
    <!-- fancybox -->

    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/custom.css">
    <!--[if IE]>
    <link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/images/favicon/favicon.ico"><![endif]-->
    <!-- favicon-->
    <link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/images/favicon/favicon.ico">
    <!--link(rel="icon", href="./images/favicon/favicon.png")-->
    <!--link(rel="apple-touch-icon-precomposed", href="./images/favicon/apple-touch-icon-precomposed.png")-->
    <!--link(rel="apple-touch-icon-precomposed", sizes="72x72",  href="./images/favicon/apple-touch-icon-72x72-precomposed.png")-->
    <!--link(rel="apple-touch-icon-precomposed", sizes="114x114",  href="./images/favicon/apple-touch-icon-114x114-precomposed.png")-->
    <!--link(rel="apple-touch-icon-precomposed", sizes="144x144",  href="./images/favicon/apple-touch-icon-144x144-precomposed.png")-->
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script><![endif]-->
    <!-- fonts-->
    <link href="https://fonts.googleapis.com/css?family=Istok+Web:400,700&amp;amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico&amp;amp;subset=cyrillic" rel="stylesheet">
    <!-- jquery-->
    <script src="<?=SITE_TEMPLATE_PATH?>/external/jquery/jquery.js"></script>
    <!-- fancybox -->
    <script src="<?=SITE_TEMPLATE_PATH?>/js/fancy/jquery.fancybox.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/fancy/helpers/jquery.fancybox-media.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.maskedinput.min.js"></script>
    <!-- fancybox -->
    <script src="https://www.google.com/recaptcha/api.js"></script>
<!--    <div id="p_prldr">-->
<!--        <div class="contpre"><span class="svg_anm"></span><span>-->
<!--            --><?//$APPLICATION->IncludeComponent(
//                "bitrix:main.include",
//                "",
//                Array(
//                    "AREA_FILE_SHOW" => "file",
//                    "PATH" =>"/include/".$lang."/loader.php",
//                )
//            );?>
<!--        </div>-->
<!--    </div>-->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PNKTQ7Z"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="panel">
    <?$APPLICATION->ShowPanel();?>
</div>
<?include("include/city.php");?>
<div class="additional">
    <div class="container">
        <div class="row">
            <div class="col">

                <div class="additional-block text-center">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" =>"/include/".$lang."/sale_text.php",
                        )
                    );?>
                    <span class="additional-close">&times;</span>
                </div>
            </div>
        </div>
    </div>
</div>
<header class="header" id="header">
    <div class="container">
        <div class="row">
            <div class="header-top flex-container-w100">
                <a id="toggle-menu" href="#"><span></span></a>
                <div class="header-top-item logo-wrapper header__logo-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" =>"/include/".$lang."/logo.php",
                        )
                    );?>
                </div>
                <div class="header-top-item">
                    <a class="offline-icon" href="/contact/">Фирменные магазины</a>
                </div>
                <div class="header-top-item relative">
                    <a class="city-icon customodal-btn" href="#"><?=$City?></a>
                    <div class="yourCity" id="yourCity">
                        <p class="yourCity-title">Ваш регион:</p>
                        <p class="yourCity-text"><?=$City?></p>
                        <div class="yourCity-btn-wrap">
                            <a class="mybtn transition yourCity-btn yes" href="?code=<?=$Code?>">Да</a>
                            <button class="mybtn transition yourCity-btn change" data-target="#chooseCity"
                                    data-toggle="modal">
                                Изменить
                            </button>
                        </div>
                    </div>
                    <div class="yourCity-add yourCityBlock"></div>
                </div>
                <div class="header-top-item relative">
                    <span class="login-icon">
                        <?if (!$USER->IsAuthorized()):?>
                            <a id="authOpen" class="customodal-btn">Войти</a>
                            <i>/</i>
                            <a data-target="#register" data-toggle="modal" href="/register/">Регистрация</a>
                        <?else:?>
                            <a href="/personal/">Личный кабинет</a>
                        <?endif;?>
                    </span>
                    <div class="yourCity" id="auth">
                        <?$APPLICATION->IncludeComponent("bitrix:system.auth.form","header_auth",Array(
                                "REGISTER_URL" => "/personal/profile/index.php",
                                "FORGOT_PASSWORD_URL" => "/personal/profile/?forgot_password=yes",
                                "PROFILE_URL" => "/personal/index.php",
                                "SHOW_ERRORS" => "Y"
                            )
                        );?>
                        <?/*<form id="authForm" action="/" method="POST">
                            <div class="auth-input-wrapper">
                                <input name="email" type="email" placeholder="Email" required>
                            </div>
                            <div class="auth-input-wrapper">
                                <input name="password" type="password" placeholder="Пароль" required>
                            </div>
                            <div class="auth-link-wrapper flex-container flex-side">
                                <a data-target="#register" data-toggle="modal">Регистрация</a>
                                <a id="forgotPass" href="#">Забыли пароль?</a>
                            </div>
                            <div class="auth-btn-wrap">
                                <input class="mybtn transition yourCity-btn" type="submit" name="submit" value="Войти">
                            </div>
                        </form>*/?>
                    </div>
                    <div class="yourCity-add auth"></div>
                </div>
                <a class="btn-favourite btn-initial" href="<?=SITE_DIR . 'personal/wishlist/'?>"><span class="icon-favourite"></span></a>
                <?$APPLICATION->IncludeComponent("at:sale.basket.basket.line", "basket",
                    Array(
                        "HIDE_ON_BASKET_PAGES" => "Y",	// Не показывать на страницах корзины и оформления заказа
                        "PATH_TO_BASKET" => SITE_DIR."personal/cart/",	// Страница корзины
                        "PATH_TO_ORDER" => SITE_DIR."personal/order/make/",	// Страница оформления заказа
                        "PATH_TO_PERSONAL" => SITE_DIR."personal/",	// Страница персонального раздела
                        "PATH_TO_PROFILE" => SITE_DIR."personal/",	// Страница профиля
                        "PATH_TO_REGISTER" => SITE_DIR."login/",	// Страница регистрации
                        "POSITION_FIXED" => "N",	// Отображать корзину поверх шаблона
                        "POSITION_HORIZONTAL" => "right",
                        "POSITION_VERTICAL" => "top",
                        "SHOW_AUTHOR" => "N",	// Добавить возможность авторизации
                        "SHOW_DELAY" => "N",	// Показывать отложенные товары
                        "SHOW_EMPTY_VALUES" => "N",	// Выводить нулевые значения в пустой корзине
                        "SHOW_IMAGE" => "Y",	// Выводить картинку товара
                        "SHOW_NOTAVAIL" => "N",	// Показывать товары, недоступные для покупки
                        "SHOW_NUM_PRODUCTS" => "Y",	// Показывать количество товаров
                        "SHOW_PERSONAL_LINK" => "N",	// Отображать персональный раздел
                        "SHOW_PRICE" => "Y",	// Выводить цену товара
                        "SHOW_PRODUCTS" => "Y",	// Показывать список товаров
                        "SHOW_SUBSCRIBE" => "N",	// Показывать товары, на которые подписан покупатель
                        "SHOW_SUMMARY" => "Y",	// Выводить подытог по строке
                        "SHOW_TOTAL_PRICE" => "Y",	// Показывать общую сумму по товарам
                    ),
                    false
                );?>
            </div>


            <?$APPLICATION->IncludeComponent(
                "bitrix:catalog.section.list",
                "sub_menu",
                Array(
                    "IBLOCK_TYPE" => "main",
                    "IBLOCK_ID" => "93",
                    "SECTION_ID" => "",
                    "COUNT_ELEMENTS" => "Y",
                    "TOP_DEPTH" => "2",
                    "SECTION_URL" => "/news/#SECTION_ID#/",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600",
                    "DISPLAY_PANEL" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "SECTION_USER_FIELDS" => ["UF_VID"],
                )
            );?>
        </div>
    </div>
</header>
<main class="main" id="main"></main>
