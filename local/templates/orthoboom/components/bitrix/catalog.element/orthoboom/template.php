<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Page\Asset;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/validation/jquery.validate.min.js");
$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES'])) {
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
    'TEMPLATE_THEME'   => $this->GetFolder() . '/themes/' . $arParams['TEMPLATE_THEME'] . '/style.css',
    'TEMPLATE_CLASS'   => 'bx_' . $arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES'       => $currencyList
);
unset($currencyList, $templateLibrary);
CJSCore::Init(array('ajax', 'window'));

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
    'ID'                 => $strMainID,
    'PICT'               => $strMainID . '_pict',
    'DISCOUNT_PICT_ID'   => $strMainID . '_dsc_pict',
    'STICKER_ID'         => $strMainID . '_sticker',
    'BIG_SLIDER_ID'      => $strMainID . '_big_slider',
    'BIG_IMG_CONT_ID'    => $strMainID . '_bigimg_cont',
    'SLIDER_CONT_ID'     => $strMainID . '_slider_cont',
    'SLIDER_LIST'        => $strMainID . '_slider_list',
    'SLIDER_LEFT'        => $strMainID . '_slider_left',
    'SLIDER_RIGHT'       => $strMainID . '_slider_right',
    'OLD_PRICE'          => $strMainID . '_old_price',
    'PRICE'              => $strMainID . '_price',
    'DISCOUNT_PRICE'     => $strMainID . '_price_discount',
    'SLIDER_CONT_OF_ID'  => $strMainID . '_slider_cont_',
    'SLIDER_LIST_OF_ID'  => $strMainID . '_slider_list_',
    'SLIDER_LEFT_OF_ID'  => $strMainID . '_slider_left_',
    'SLIDER_RIGHT_OF_ID' => $strMainID . '_slider_right_',
    'QUANTITY'           => $strMainID . '_quantity',
    'QUANTITY_DOWN'      => $strMainID . '_quant_down',
    'QUANTITY_UP'        => $strMainID . '_quant_up',
    'QUANTITY_MEASURE'   => $strMainID . '_quant_measure',
    'QUANTITY_LIMIT'     => $strMainID . '_quant_limit',
    'BASIS_PRICE'        => $strMainID . '_basis_price',
    'BUY_LINK'           => $strMainID . '_buy_link',
    'ADD_BASKET_LINK'    => $strMainID . '_add_basket_link',
    'BASKET_ACTIONS'     => $strMainID . '_basket_actions',
    'NOT_AVAILABLE_MESS' => $strMainID . '_not_avail',
    'COMPARE_LINK'       => $strMainID . '_compare_link',
    'PROP'               => $strMainID . '_prop_',
    'PROP_DIV'           => $strMainID . '_skudiv',
    'DISPLAY_PROP_DIV'   => $strMainID . '_sku_prop',
    'OFFER_GROUP'        => $strMainID . '_set_group_',
    'BASKET_PROP_DIV'    => $strMainID . '_basket_prop',
);
$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (
isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) &&
$arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
    : $arResult['NAME']
);
$strAlt = (
isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) &&
$arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
    : $arResult['NAME']
);
?>

<div class="breadcrumbs">
    <div class="container" itemscope itemtype="https://schema.org/BreadcrumbList">
        <div class="row">
            <div class="breadcrumbs-wrapper">
                <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs", Array(
                    "PATH" => "",
                    "SITE_ID" => "s1",
                    "START_FROM" => "0",
                ),
                    false
                );?>
            </div>
        </div>
    </div>
</div>

<section class="card-page">
    <div class="container pl-md-0 pr-md-0">
        <div class="row">
            <div class="col-md-6 col-lg-5 col-xl-4 mb-4 mb-xl-0 card-slider__holder">
                <div class="goods-label-block">
                    <?php if ($arResult["PROPERTIES"]["NOVINKA_INTERNET_MAGAZIN"]["VALUE"] == "Да") : ?>
                        <div class="goods-label">новинка</div>
                    <?php endif; ?>
                    <?php if ($arResult["PROPERTIES"]["KHIT_PRODAZH_INTERNET_MAGAZIN"]["VALUE"] == "Да") : ?>
                        <div class="goods-label-hit">хит</div>
                    <?php endif; ?>
                    <?php if ($arResult["PROPERTIES"]["TREND_INTERNET_MAGAZIN"]["VALUE"] == "Да") : ?>
                        <div class="goods-label-trend">тренд</div>
                    <?php endif; ?>
                    <?php if ($arResult["PROPERTIES"]["FIKSIROVANNAYA_TSENA_ZNACHENIE"]["VALUE"]) : ?>
                        <div class="goods-label-fixprice">фиксированная цена</div>
                    <?php endif; ?>
                    <?php if ($arResult["PROPERTIES"]["AMOUNT_MANUAL"]["VALUE"]) : ?>
                        <div class="amount-manual-label">Осталось <?=$arResult["PROPERTIES"]["AMOUNT_MANUAL"]["VALUE"]?> шт.</div>
                    <?php endif; ?>
                </div>
                <?if ($arResult["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"]):?>
                    <div class="price-discount-percent">- <?=$arResult["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"]?>%</div>
                <?endif?>
                <div class="slider-for card-slider__main">
                    <div>
                        <div class="card-slider__slide">
                            <div class="card-slider__img">
                                <img src="
                                    <?php echo $arResult["DETAIL_PICTURE"]["SRC"]?>
                                ">
                            </div>
                        </div>
                    </div>

                    <?php foreach ($arResult["MORE_PHOTOS"] as $propsPhoto) : ?>
                        <div>
                            <div class="card-slider__slide">
                                <div class="card-slider__img">
                                    <img src="<?php echo $propsPhoto?>">
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="slider-nav card-slider__nav">
                    <div>
                        <div class="card-slider__slide">
                            <div class="card-slider__img">
                                <img src="
                                    <?php echo $arResult["DETAIL_PICTURE"]["SRC"]?>
                                ">
                            </div>
                        </div>
                    </div>
                    <?php foreach ($arResult["MORE_PHOTOS"] as $propsPhoto) : ?>
                        <div>
                            <div class="card-slider__slide">
                                <div class="card-slider__img">
                                    <img src="<?php echo $propsPhoto?>">
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="col-md-6 col-lg-7 col-xl-5 mb-4 mb-xl-0 card-info">
                <form class="d-flex flex-column justify-content-between h-100 d-xl-block">
                    <?if ($arResult["PROPERTIES"]["FIKSIROVANNAYA_TSENA_ZNACHENIE"]["VALUE"]):?>
                        <input type="hidden" name="fixed_price" value="<?=$arResult["PROPERTIES"]["FIKSIROVANNAYA_TSENA_ZNACHENIE"]["VALUE"]?>">
                        <input type="hidden" name="product_name" value="<?= $arResult["NAME"];?>">
                    <?endif;?>
                    <div class="card-info-block">
                        <div class="card-title">
                            <h1><?= $arResult["NAME"];?></h1>
                            <?/*<b><?= $arResult["NAME"];?></b>*/?>
                            <!--<b>АРТ.<?= $arResult["PROPERTIES"]["CML2_ARTICLE"]["VALUE"]?>
                                <?= $arResult["PROPERTIES"]["KRASKA_TSVET"]["VALUE"]?></b>-->
                        </div>
                        <div class="card-article">артикул: <?= $arResult["PROPERTIES"]["CML2_ARTICLE"]["VALUE"]?></div>
                    </div>
                    <div class="row card-info-block">
                        <div class="card-charecteristics col-sm-6 d-flex flex-wrap">
                            <b>Пол:</b>
                            <span><?= $arResult["PROPERTIES"]["POL"]["VALUE"]?></span>
                        </div>
                        <div class="card-charecteristics col-sm-6 d-flex flex-wrap">
                            <b>Верх:</b>
                            <span><?= $arResult["PROPERTIES"]["OBUV_MATERIALVERKHA"]["VALUE"]?></span>
                        </div>
                        <div class="card-charecteristics col-sm-6 d-flex flex-wrap">
                            <b>Подкладка:</b>
                            <span><?= $arResult["PROPERTIES"]["OBUV_PODKLADKA"]["VALUE"]?></span>
                        </div>
                        <div class="card-charecteristics col-sm-6 d-flex flex-wrap">
                            <b>Сезон:</b>
                            <span><?= $arResult["PROPERTIES"]["OBUV_SEZONNOST"]["VALUE"]?></span>
                        </div>
                        <div class="card-charecteristics col-sm-6 d-flex flex-wrap">
                            <b>Тип:</b>
                            <span><?php echo $arResult["PROPERTIES"]["OBUV_TIP"]["~VALUE"]; ?></span>
                        </div>
                    </div>
                    <div class="card-info-block card-info__detail">
                        <div class="card-title__simple">РАЗМЕРЫ В НАЛИЧИИ:</div>
                        <div class="tiled-grid d-flex flex-wrap">
                            <?
                            foreach ($arResult["RAZMERA"] as $key => $value) { ?>
                                <div class="tiled-checkbox show-element">
                                    <input
                                            class="razmer_input"
                                            id="boots<?=$key?>"
                                            type="radio"
                                            value="<?=$key?>"
                                            data-price="<?=$value["PRICE"]?>"
                                            data-id="<?=$value["ID"]?>"
                                            data-CanBuy="<?=$value["CAN_BUY"]?>"
                                            data-Quantity="<?=$value["CATALOG_QUANTITY"]?>"
                                            name="bootSize"
                                        <?=($value["CAN_BUY"] == "N")?" disabled":""?>
                                        <? if($value["CAN_BUY"] == "Y" && !$check && $arResult["MIN_PRICE"]["PRINT_VALUE"]==$value["PRICE"]) {?>
                                            checked="checked"
                                            <?
                                            $check = true;
                                        } ?>
                                    >
                                    <label class="tiled__label" for="boots<?=$key?>"><?=$key?></label>
                                </div>
                            <? } ?>
                        </div>
                        <!-- Modal-->
                        <div class="modal choose-size__modal fade" id="chooseSize" tabindex="-1" role="dialog"
                             aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-shadow"></div>
                                    <div class="modal-header">
                                        <div class="modal-title h5"><b>Как выбрать размер обуви</b></div>
                                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-11">
                                                Чтобы определить свой размер обуви нужно измерить
                                                стопу. Рекомендуют делать это в конце дня, т.к.
                                                ноги растаптываются и становятся больше.
                                                Измерьте расстояние от пятки до кончика большого
                                                пальца на каждой стопе. Если размеры отличаются,
                                                возьмите за основу наибольшее значение.
                                            </div>
                                        </div>
                                        <table class="sizing-table text-center">
                                            <tr>
                                                <th>Размер</th>
                                                <th>Длинна стопы</th>
                                            </tr>
                                            <tr>
                                                <td>17</td>
                                                <td>10.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>18</td>
                                                <td>11 см</td>
                                            </tr>
                                            <tr>
                                                <td>19</td>
                                                <td>11.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>19.5</td>
                                                <td>12 см</td>
                                            </tr>
                                            <tr>
                                                <td>20</td>
                                                <td>12.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>21</td>
                                                <td>13 см</td>
                                            </tr>
                                            <tr>
                                                <td>22</td>
                                                <td>13.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>22.5</td>
                                                <td>14 см</td>
                                            </tr>
                                            <tr>
                                                <td>23</td>
                                                <td>14.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>24</td>
                                                <td>15 см</td>
                                            </tr>
                                            <tr>
                                                <td>25</td>
                                                <td>15.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>25.5</td>
                                                <td>16 см</td>
                                            </tr>
                                            <tr>
                                                <td>26</td>
                                                <td>16.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>27</td>
                                                <td>17 см</td>
                                            </tr>

                                            <tr>
                                                <td>28</td>
                                                <td>17.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>28.5</td>
                                                <td>18 см</td>
                                            </tr>
                                            <tr>
                                                <td>29</td>
                                                <td>18.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>30</td>
                                                <td>19 см</td>
                                            </tr>
                                            <tr>
                                                <td>31</td>
                                                <td>19.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>31.5</td>
                                                <td>20 см</td>
                                            </tr>
                                            <tr>
                                                <td>32</td>
                                                <td>20.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>33</td>
                                                <td>21 см</td>
                                            </tr>
                                            <tr>
                                                <td>34</td>
                                                <td>21.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>34.5</td>
                                                <td>22 см</td>
                                            </tr>
                                            <tr>
                                                <td>35</td>
                                                <td>22.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>36</td>
                                                <td>23 см</td>
                                            </tr>
                                            <tr>
                                                <td>37</td>
                                                <td>23.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>37.5</td>
                                                <td>24 см</td>
                                            </tr>
                                            <tr>
                                                <td>38</td>
                                                <td>24.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>39</td>
                                                <td>25 см</td>
                                            </tr>
                                            <tr>
                                                <td>40</td>
                                                <td>25.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>40.5</td>
                                                <td>26 см</td>
                                            </tr>
                                            <tr>
                                                <td>41</td>
                                                <td>26.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>42</td>
                                                <td>27 см</td>
                                            </tr>
                                            <tr>
                                                <td>43</td>
                                                <td>27.5 см</td>
                                            </tr>
                                            <tr>
                                                <td>43.5</td>
                                                <td>28 см</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-info-block card-info__detail">
                        <div class="quantity-cost d-flex w-100 flex-wrap align-items-center m-2">
                            <div class="cart__ammount-control">
                                <a class="cart__ammount-minus js-add_cart-minus" href="javascript:void(0);">
                                    <span class="icon">-</span>
                                </a>
                                <span class="cart__ammount-num js-add_cart-quantity">1</span>
                                <a class="cart__ammount-plus js-add_cart-plus" href="javascript:void(0);">
                                    <span class="icon">+</span>
                                </a>
                            </div>
                            <div class="quantity-cost-price">
                                <?=$arResult["MIN_PRICE"]["PRINT_DISCOUNT_VALUE"]?>
                            </div>
                            <?if ($arResult["MIN_PRICE"]["VALUE"] != $arResult["MIN_PRICE"]["DISCOUNT_VALUE"]):?>
                                <div class="quantity-cost-oldPrice"><?=$arResult["MIN_PRICE"]["PRINT_VALUE"]?></div>
                            <?endif;?>
                        </div>
                        <div class="btn-grouph d-flex w-100 flex-wrap justify-content-between">
                            <button class="btn btn-upper btn-fill mb-2 mb-sm-0" type="submit" onclick="ym(25218083, 'reachGoal', 'ZAKAZ');" onmousedown="try { rrApi.addToBasket(<?=$arResult["ID"]?>) } catch(e) {}">Купить</button>
                            <a
                                    data-target="#buyOneClick" data-toggle="modal"
                                    class="btn btn-upper btn-path mb-2 mb-sm-0"
                                    href="javascript:void(0);"
                                    role="button"
                            >
                                <? echo 'Быстрый заказ'; ?>
                            </a>
                        </div>
                    </div>
                    <?
                    $finish = '';
                    $arDiscounts = CCatalogDiscount::GetDiscountByProduct($arResult['ID'], $USER->GetUserGroupArray(), 'N');
                    //echo "<div style='display:none'>";
                    //print_r($arDiscounts);
                    //echo "</div>";
                    if (count($arDiscounts) == 1) {
                        reset($arDiscounts);
                        $firstKey = key($arDiscounts);
                        $ar = explode('#COUNTDOWN#', $arDiscounts[$firstKey]['NAME']);
                        if (isset($ar[1])) $finish = trim($ar[1]);
                    }
                    if ($finish):?>
                        <div class="countdown">ДО КОНЦА СКИДКИ ОСТАЛОСЬ</div>
                        <div id="timer"></div>
                    <?endif;?>
                </form>
            </div>
            <nav class="col-xl-3 card-sidebar__right">
                <div class="row d-xl-block mr-xl-0 ml-xl-0">
                    <div class="col-sm-6 col-xl-12">
                        <div class="card-title__simple">ПОЧЕМУ НАША ОБУВЬ?</div>
                        <ul>
                            <li>
                                <a class="card-tab-nav d-flex align-items-center" href="#insole"
                                   data-tab-toggle="#nav-profile-tab">
                                    <div class="border-frame">
                                        <div class="image-bg insole"
                                             style="background-image:
                                                     url(<?=SITE_TEMPLATE_PATH?>/images/card/insole.png)">
                                        </div>
                                    </div>
                                    <div>Съемная  сводоформирующая стелька</div>
                                </a>
                            </li>
                            <li>
                                <a class="card-tab-nav d-flex align-items-center" href="#backdrop"
                                   data-tab-toggle="#nav-profile-tab">
                                    <div class="border-frame">
                                        <div class="image-bg backdrop"
                                             style="background-image:
                                                     url(<?=SITE_TEMPLATE_PATH?>/images/card/backdrop.png)"></div>
                                    </div>
                                    <div>Жесткий задник</div>
                                </a>
                            </li>
                            <li><a class="card-tab-nav d-flex align-items-center" href="#nosepiece"
                                   data-tab-toggle="#nav-profile-tab">
                                    <div class="border-frame">
                                        <div class="image-bg nosepiece" style="background-image:
                                                url(<?=SITE_TEMPLATE_PATH?>/images/card/nosepiece.png)"></div>
                                    </div>
                                    <div>Жесткий и широкий подносок</div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-xl-12 delivery-payment">
                        <div class="card-title__simple">ОПЛАТА/ДОСТАВКА</div>
                        <ul>
                            <li class="d-flex align-items-center">
                                <a class="card-tab-nav d-flex align-items-center" href="#info-ship" data-tab-toggle="#info-ship-tab">
                                    <div class="border-frame">
                                        <div class="image-bg delivery" style="background-image:
                                                url(<?=SITE_TEMPLATE_PATH?>/images/card/delivery-truck.png)"></div>
                                    </div>
                                    <div>Информация<br>о доставке</div>
                                </a>
                            </li>
                            <li class="d-flex align-items-center">
                                <a class="card-tab-nav d-flex align-items-center" href="#info-pay" data-tab-toggle="#info-pay-tab">
                                    <div class="border-frame">
                                        <div class="image-bg cash" style="background-image:
                                                url(<?=SITE_TEMPLATE_PATH?>/images/card/delivery-cash.png)"></div>
                                    </div>
                                    <div>Информация<br>об оплате</div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <div class="card-main">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="description-tab" data-toggle="tab" href="#description"
                       role="tab" aria-controls="description" aria-selected="true">ОПИСАНИЕ</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#characteristics"
                       role="tab" aria-controls="characteristics" aria-selected="false">ХАРАКТЕРИСТИКИ</a>
                    <a class="nav-item nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab"
                       aria-controls="reviews" aria-selected="false">ОТЗЫВЫ</a>
                    <a class="nav-item nav-link" id="info-pay-tab" data-toggle="tab" href="#info-pay" role="tab"
                       aria-controls="info-pay" aria-selected="false">Об оплате</a>
                    <a class="nav-item nav-link" id="info-ship-tab" data-toggle="tab" href="#info-ship" role="tab"
                       aria-controls="info-ship" aria-selected="false">О доставке</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane slide show active" id="description" role="tabpanel"
                     aria-labelledby="description-tab">
                    <div class="row">
                        <div class="col-md-9 mb-4">
                            <div class="text-block mb-5">
                                <!--<div class="card-title"><b>ORTHOBOOM ЭТО НЕ ПРОСТО ДЕТСКАЯ ОБУВЬ</b></div>-->
                                <?
                                /*$APPLICATION->IncludeFile(
                                    SITE_DIR."include/".LANGUAGE_ID."/catalog_element/orto_text_inc.php",
                                    array(),
                                    array(
                                        "MODE"      => "html",
                                        "NAME"      => "Редактирование включаемой области раздела",
                                        "TEMPLATE"  => ""
                                    )
                                );*/
                                echo $arResult["DETAIL_TEXT"];
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="card-title"><b>ПЛЮСЫ ОБУВИ ORTHOBOOM</b></div>
                    <div class="row benefits-block mb-4">
                        <div class="col-md-4 mb-4 mb-md-0 text-center">
                            <div class="border-frame">
                                <div class="image-bg certification" style="background-image:
                                        url(<?=SITE_TEMPLATE_PATH?>/images/card/certification.png)"></div>
                            </div>
                            <div class="border-frame__title">Сертификация</div>
                            <div>
                                <?
                                $APPLICATION->IncludeFile(
                                    SITE_DIR."include/".LANGUAGE_ID."/catalog_element/sertification_inc.php",
                                    array(),
                                    array(
                                        "MODE"      => "html",
                                        "NAME"      => "Редактирование включаемой области раздела",
                                        "TEMPLATE"  => ""
                                    )
                                );
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4 mb-4 mb-md-0 text-center">
                            <div class="border-frame">
                                <div class="image-bg medical" style="background-image:
                                        url(<?=SITE_TEMPLATE_PATH?>/images/card/medicine.png)"></div>
                            </div>
                            <div class="border-frame__title">Медицинский уклон</div>
                            <div>
                                <?
                                $APPLICATION->IncludeFile(
                                    SITE_DIR."include/".LANGUAGE_ID."/catalog_element/medical_inc.php",
                                    array(),
                                    array(
                                        "MODE"      => "html",
                                        "NAME"      => "Редактирование включаемой области раздела",
                                        "TEMPLATE"  => ""
                                    )
                                );
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4 mb-4 mb-md-0 text-center">
                            <div class="border-frame">
                                <div class="image-bg anatomy" style="background-image:
                                        url(<?=SITE_TEMPLATE_PATH?>/images/card/anatomy.png)"></div>
                            </div>
                            <div class="border-frame__title">Анатомия</div>
                            <div>
                                <?
                                $APPLICATION->IncludeFile(
                                    SITE_DIR."include/".LANGUAGE_ID."/catalog_element/anatomy_inc.php",
                                    array(),
                                    array(
                                        "MODE"      => "html",
                                        "NAME"      => "Редактирование включаемой области раздела",
                                        "TEMPLATE"  => ""
                                    )
                                );
                                ?>
                            </div>
                        </div>
                    </div>
                    <?
                    //print_r($arResult['PROPERTIES']['ADDIT_INFO']);
                    $additInfo = trim($arResult['PROPERTIES']['ADDIT_INFO']['~VALUE']['TEXT']);
                    if ($additInfo):?>
                        <div class="row">
                            <div class="col-md-9 text-block">
                                <div class="card-title"><b>ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ</b></div>
                                <?
                                echo $additInfo;
                                /* $APPLICATION->IncludeFile(
                                    SITE_DIR."include/".LANGUAGE_ID."/catalog_element/orto_text_inc.php",
                                    array(),
                                    array(
                                        "MODE"      => "html",
                                        "NAME"      => "Редактирование включаемой области раздела",
                                        "TEMPLATE"  => ""
                                    )
                                ); */
                                ?>
                            </div>
                        </div>
                    <?endif;?>
                </div>
                <div class="tab-pane slide" id="characteristics" role="tabpanel" aria-labelledby="characteristics-tab">
                    <div class="row">
                        <div class="col-md-9 mb-4">
                            <div class="text-block mb-5" id="insole">
                                <div class="card-title"><b>СЪЕМНАЯ  СВОДОФОРМИРУЮЩАЯ СТЕЛЬКА</b></div>
                                <?
                                $APPLICATION->IncludeFile(
                                    SITE_DIR."include/".LANGUAGE_ID."/catalog_element/stelka_inc.php",
                                    array(),
                                    array(
                                        "MODE"      => "html",
                                        "NAME"      => "Редактирование включаемой области раздела",
                                        "TEMPLATE"  => ""
                                    )
                                );
                                ?>
                            </div>
                            <div class="text-block mb-5" id="backdrop">
                                <div class="card-title"><b>ЖЕСТКИЙ ЗАДНИК</b></div>
                                <?
                                $APPLICATION->IncludeFile(
                                    SITE_DIR."include/".LANGUAGE_ID."/catalog_element/zadnic_inc.php",
                                    array(),
                                    array(
                                        "MODE"      => "html",
                                        "NAME"      => "Редактирование включаемой области раздела",
                                        "TEMPLATE"  => ""
                                    )
                                );
                                ?>
                            </div>
                            <div class="text-block" id="nosepiece">
                                <div class="card-title"><b>ПОДНОСОК</b></div>
                                <?
                                $APPLICATION->IncludeFile(
                                    SITE_DIR."include/".LANGUAGE_ID."/catalog_element/podnosok_inc.php",
                                    array(),
                                    array(
                                        "MODE"      => "html",
                                        "NAME"      => "Редактирование включаемой области раздела",
                                        "TEMPLATE"  => ""
                                    )
                                );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane slide" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
                    <div class="card-title"><b>Отзывы покупателей о <?=$arResult["NAME"]?></b></div>
                </div>
                <div class="tab-pane slide" id="info-pay" role="tabpanel" aria-labelledby="info-pay-tab">
                    <div style="float:left; margin-right:20px">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/delivery1.png" alt="" class="">
                    </div>
                    <div class="card-title" style="padding:20px;">
                        Наличными
                    </div>
                    <div class="text-block" style="clear:both;margin-bottom:40px;">
                        <p>
                            <?/* if ($arResult['ID'] != 102684): */?>
                            <!--Оплатить наличными можно курьеру, если покупка совершается в Нижнем Новгороде. -->
                            <?/* endif */;?>
                            Оплатить наличными можно курьеру или в пункте выдачи транспортной компании, а также в отделении почты России.</p>
                    </div>
                    <div style="float:left; margin-right:20px">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/delivery2.png" alt="" class="">
                    </div>
                    <div class="card-title" style="padding:20px;">
                        Банковской картой
                    </div>
                    <div class="text-block" style="clear:both;">
                        <p>Вы можете оплатить заказ любой банковской картой: МИР, VISA, Mastercard, Maestro.</p>
                    </div>
                </div>
                <div class="tab-pane slide" id="info-ship" role="tabpanel" aria-labelledby="info-ship-tab">
                    <div style="float:left;  margin:0 20px 0 -10px">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/delivery3.png" alt="" class="">
                    </div>
                    <div class="card-title" style="padding:20px;">
                        Почта России
                    </div>
                    <div class="text-block" style="clear:both;margin-bottom:40px;padding:15px;">
                        <ul style="list-style-type:initial;">
<!--                            <li><p>При заказе от 4000 руб. доставка бесплатная!</p></li>-->
                            <li><p>Доставка осуществляется до ближайшего почтового отделения по указанному Вами адресу согласно тарифам и срокам почты России.</p></li>
                            <li><p>После оформления заказа через сайт с Вами свяжется менеджер для уточнения данных и адреса доставки.</p></li>
                            <li><p>После оплаты заказ формируется и отправляется.</p></li>
                            <li><p>На Ваш email мы отправим трек-номер для отслеживания посылки.</p></li>
                        </ul>
                    </div>
                    <div style="float:left;  margin:0 20px 0 -10px">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/delivery4.png" alt="" class="">
                    </div>
                    <div class="card-title" style="padding:15px;">
                        Курьерская доставка
                    </div>
                    <div class="text-block" style="clear:both;margin-bottom:40px;padding:15px;">
                        <ul style="list-style-type:initial;">
                            <li><p>Курьерская доставка осуществляется по Нижнему Новгороду в день заказа при наличии на складе, а также по всей территории РФ. Стоимость доставки зависит от объема заказа и города доставки. Точную цену назовет менеджер при оформлении заказа.</p></li>
                            <li><p>В остальные города доставка осуществляется курьерской компанией СДЭК.</p></li>
                            <li><p>Сроки зависят от удаленности, но обычно не превышают 3-5 дней.</p></li>
                            <li><p>Стоимость доставки зависит от веса посылки и удаленности и рассчитывается по специальным условиям.</p></li>
                            <li><p>После оформления заказа через сайт с Вами свяжется менеджер для уточнения данных и адреса доставки.</p></li>
                            <li><p>После оплаты заказ формируется и отправляется.</p></li>
                            <li><p>На Ваш e-mail мы отправим трек-номер для отслеживания посылки.</p></li>
                        </ul>
                    </div>
                    <div style="float:left; margin:0 20px 0 -10px">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/delivery5.png" alt="" class="">
                    </div>
                    <div class="card-title">
                        Доставка для оптовых покупателей
                    </div>
                    <div class="text-block" style="clear:both;margin-bottom:40px;padding:15px;">
                        <ul style="list-style-type:initial;">
                            <li><p>Доставка возможна по территории России, Казахстана, Белоруссии.</p></li>
                            <li><p>После оформления заказа через сайт с Вами свяжется оператор для уточнения даты, времени и места доставки.</p></li>
                            <li><p>Стоимость доставки зависит от объема заказа.</p></li>
                            <li><p>Срок доставки зависит от местонахождения заказчика и выбранной транспортной компании.</p></li>
                            <li><p>Доставка осуществляется до терминала любой транспортной компании, удобной для покупателя и имеющей представительство в г.Нижний Новгород и г.Москва.</p></li>
                        </ul>
                    </div>
                </div>
            </div>
            <? if(count($arResult["YOU_MAY_LIKE"]) > 0) {
                global $arrFilter_like;
                $arrFilter_like = array("ID" => $arResult["YOU_MAY_LIKE"]);
                ?>
                <div class="seo-title slider-introduce">ВАМ МОГУТ ПОНРАВИТСЯ</div>
                <div class="row d-block mb-5">
                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "main_element",
                        array(
                            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                            "ELEMENT_SORT_FIELD" => 'PROPERTYSORT_AVAILABILITY',
                            "ELEMENT_SORT_ORDER" => 'desc',
                            "ELEMENT_SORT_FIELD2" => "catalog_PROPERTY_NEW",
                            "ELEMENT_SORT_ORDER2" =>  'asc',
                            "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                            "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                            "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                            "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                            "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                            "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
                            "BASKET_URL" => $arParams["BASKET_URL"],
                            "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                            "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                            "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                            "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                            "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                            "FILTER_NAME" => 'arrFilter_like',
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                            "SET_TITLE" => $arParams["SET_TITLE"],
                            "MESSAGE_404" => $arParams["MESSAGE_404"],
                            "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                            "SHOW_404" => $arParams["SHOW_404"],
                            "FILE_404" => $arParams["FILE_404"],
                            "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
                            "PAGE_ELEMENT_COUNT" => 15,
                            "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                            "PRICE_CODE" => $arParams["PRICE_CODE"],
                            "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                            "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

                            "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                            "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
                            "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ?
                                $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                            "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ?
                                $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                            "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

                            "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                            "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                            "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                            "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                            "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                            "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                            "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                            "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                            "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                            "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],

                            "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                            "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                            "OFFERS_PROPERTY_CODE" => "OBUV_RAZMER",//$arParams["LIST_OFFERS_PROPERTY_CODE"],
                            "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                            "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                            "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                            "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                            "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

                            "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
                            "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                            "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                            "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
                            "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
                            'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                            'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                            'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

                            'LABEL_PROP' => $arParams['LABEL_PROP'],
                            'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                            'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

                            'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                            'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                            'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                            'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
                            'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
                            'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
                            'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
                            'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

                            'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                            "ADD_SECTIONS_CHAIN" => "Y",
                            'ADD_TO_BASKET_ACTION' => $basketAction,
                            'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ?
                                $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                            'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
                            'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ?
                                $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
                            'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ?
                                $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
                            "SHOW_ALL_WO_SECTION" => "Y"
                        ),
                        $component
                    );
                    ?>
                </div>
            <?}?>

            <? if(count($arResult["BUY_WITH_ELEMENT"]) > 0) {
                global $arrFilter_like;
                $arrFilter_like = array("ID" => $arResult["BUY_WITH_ELEMENT"]);
                ?>
                <div class="seo-title slider-introduce">С ЭТИМ ТОВАРОМ ПОКУПАЮТ</div>
                <div class="row d-block mb-5">
                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "main_element",
                        array(
                            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                            "ELEMENT_SORT_FIELD" => 'PROPERTYSORT_AVAILABILITY',
                            "ELEMENT_SORT_ORDER" => 'desc',
                            "ELEMENT_SORT_FIELD2" => "catalog_PROPERTY_NEW",
                            "ELEMENT_SORT_ORDER2" =>  'asc',
                            "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                            "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                            "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                            "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                            "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                            "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
                            "BASKET_URL" => $arParams["BASKET_URL"],
                            "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                            "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                            "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                            "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                            "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                            "FILTER_NAME" => 'arrFilter_like',
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                            "SET_TITLE" => $arParams["SET_TITLE"],
                            "MESSAGE_404" => $arParams["MESSAGE_404"],
                            "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                            "SHOW_404" => $arParams["SHOW_404"],
                            "FILE_404" => $arParams["FILE_404"],
                            "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
                            "PAGE_ELEMENT_COUNT" => 15,
                            "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                            "PRICE_CODE" => $arParams["PRICE_CODE"],
                            "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                            "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

                            "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                            "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
                            "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ?
                                $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                            "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ?
                                $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                            "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

                            "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                            "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                            "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                            "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                            "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                            "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                            "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                            "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                            "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                            "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],

                            "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                            "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                            "OFFERS_PROPERTY_CODE" => "OBUV_RAZMER",//$arParams["LIST_OFFERS_PROPERTY_CODE"],
                            "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                            "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                            "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                            "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                            "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

                            "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
                            "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                            "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                            "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
                            "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
                            'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                            'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                            'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

                            'LABEL_PROP' => $arParams['LABEL_PROP'],
                            'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                            'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

                            'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                            'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                            'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                            'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
                            'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
                            'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
                            'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
                            'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

                            'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                            "ADD_SECTIONS_CHAIN" => "Y",
                            'ADD_TO_BASKET_ACTION' => $basketAction,
                            'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ?
                                $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                            'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
                            'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ?
                                $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
                            'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ?
                                $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
                            "SHOW_ALL_WO_SECTION" => "Y"
                        ),
                        $component
                    );
                    ?>
                </div>
            <?}?>
            <? if(count($arResult["SHOW_ELEMENT"]) > 0) {
                global $arrFilter_show;
                $arrFilter_show = array("ID" => $arResult["SHOW_ELEMENT"]);
                ?>
                <div class="seo-title slider-introduce">ВЫ СМОТРЕЛИ</div>
                <div class="row d-block mb-5">
                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "main_element",
                        array(
                            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                            "ELEMENT_SORT_FIELD" => 'PROPERTYSORT_AVAILABILITY',
                            "ELEMENT_SORT_ORDER" => 'desc',
                            "ELEMENT_SORT_FIELD2" => "catalog_PROPERTY_NEW",
                            "ELEMENT_SORT_ORDER2" =>  'asc',
                            "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                            "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                            "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                            "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                            "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                            "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
                            "BASKET_URL" => $arParams["BASKET_URL"],
                            "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                            "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                            "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                            "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                            "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                            "FILTER_NAME" => 'arrFilter_show',
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                            "SET_TITLE" => $arParams["SET_TITLE"],
                            "MESSAGE_404" => $arParams["MESSAGE_404"],
                            "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                            "SHOW_404" => $arParams["SHOW_404"],
                            "FILE_404" => $arParams["FILE_404"],
                            "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
                            "PAGE_ELEMENT_COUNT" => 15,
                            "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                            "PRICE_CODE" => $arParams["PRICE_CODE"],
                            "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                            "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

                            "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                            "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
                            "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                            "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                            "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

                            "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                            "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                            "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                            "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                            "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                            "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                            "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                            "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                            "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                            "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],

                            "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                            "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                            "OFFERS_PROPERTY_CODE" => "OBUV_RAZMER",//$arParams["LIST_OFFERS_PROPERTY_CODE"],
                            "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                            "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                            "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                            "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                            "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

                            "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
                            "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                            "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                            "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
                            "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
                            'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                            'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                            'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

                            'LABEL_PROP' => $arParams['LABEL_PROP'],
                            'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                            'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

                            'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                            'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                            'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                            'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
                            'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
                            'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
                            'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
                            'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

                            'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                            "ADD_SECTIONS_CHAIN" => "Y",
                            'ADD_TO_BASKET_ACTION' => $basketAction,
                            'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                            'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
                            'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
                            'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
                            "SHOW_ALL_WO_SECTION" => "Y"
                        ),
                        $component
                    );
                    ?>
                </div>
            <?}?>
            <?/*$APPLICATION->IncludeComponent(
                    "bitrix:catalog.products.viewed",
                    "main",
                    Array(
                        "ACTION_VARIABLE" => "action_cpv",
                        "ADDITIONAL_PICT_PROP_2" => "MORE_PHOTO",
                        "ADDITIONAL_PICT_PROP_3" => "-",
                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                        "ADD_TO_BASKET_ACTION" => "BUY",
                        "BASKET_URL" => "/personal/basket.php",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "3600",
                        "CACHE_TYPE" => "A",
                        "CART_PROPERTIES_2" => array("NEWPRODUCT","NEWPRODUCT,SALELEADER","OBUV_RAZMER"),
                        "CART_PROPERTIES_3" => array("COLOR_REF","SIZES_SHOES","OBUV_RAZMER"),
                        "CONVERT_CURRENCY" => "Y",
                        "CURRENCY_ID" => "RUB",
                        "DATA_LAYER_NAME" => "dataLayer",
                        "DEPTH" => "",
                        "DISCOUNT_PERCENT_POSITION" => "top-right",
                        "ENLARGE_PRODUCT" => "STRICT",
                        "ENLARGE_PROP_2" => "NEWPRODUCT",
                        "HIDE_NOT_AVAILABLE" => "N",
                        "HIDE_NOT_AVAILABLE_OFFERS" => "L",
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "IBLOCK_MODE" => "single",
                        "IBLOCK_TYPE" => "1c_catalog",
                        "LABEL_PROP_2" => array("NEWPRODUCT"),
                        "LABEL_PROP_MOBILE_2" => array(),
                        "LABEL_PROP_POSITION" => "top-left",
                        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                        "MESS_BTN_BUY" => "Купить",
                        "MESS_BTN_DETAIL" => "Подробнее",
                        "MESS_BTN_SUBSCRIBE" => "Подписаться",
                        "MESS_NOT_AVAILABLE" => "Нет в наличии",
                        "MESS_RELATIVE_QUANTITY_FEW" => "мало",
                        "MESS_RELATIVE_QUANTITY_MANY" => "много",
                        "MESS_SHOW_MAX_QUANTITY" => "Наличие",
                        "OFFER_TREE_PROPS_3" => array("OBUV_RAZMER"),
                        "PAGE_ELEMENT_COUNT" => "10",
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "PRICE_CODE" => $arParams["PRICE_CODE"],
                        "PRICE_VAT_INCLUDE" => "Y",
                        "PRODUCT_BLOCKS_ORDER" => "price,props,quantityLimit,sku,quantity,buttons,compare",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PRODUCT_QUANTITY_VARIABLE" => "",
                        "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false}]",
                        "PRODUCT_SUBSCRIPTION" => "Y",
                        "PROPERTY_CODE_68" => array("NEWS","SALELEADER","SPECIALOFFER","MANUFACTURER","MATERIAL",
                            "COLOR","SALELEADER,SPECIALOFFER,MATERIAL,COLOR,KEYWORDS,BRAND_REF","OBUV_RAZMER"),
                        "PROPERTY_CODE_69" => array("ARTNUMBER","COLOR_REF","SIZES_SHOES","SIZES_CLOTHES",
                            "OBUV_RAZMER"),
                        "PROPERTY_CODE_MOBILE_2" => array(),
                        "RELATIVE_QUANTITY_FACTOR" => "5",
                        "SECTION_CODE" => "",
                        "SECTION_ELEMENT_CODE" => "",
                        "SECTION_ELEMENT_ID" => "",
                        "SECTION_ID" => "",
                        "SHOW_CLOSE_POPUP" => "N",
                        "SHOW_DISCOUNT_PERCENT" => "Y",
                        "SHOW_FROM_SECTION" => "N",
                        "SHOW_MAX_QUANTITY" => "M",
                        "SHOW_OLD_PRICE" => "Y",
                        "SHOW_PRICE_COUNT" => "1",
                        "SHOW_PRODUCTS_2" => "N",
                        "SHOW_SLIDER" => "Y",
                        "SLIDER_INTERVAL" => "3000",
                        "SLIDER_PROGRESS" => "Y",
                        "TEMPLATE_THEME" => "blue",
                        "USE_ENHANCED_ECOMMERCE" => "N",
                        "USE_PRICE_COUNT" => "N",
                        "USE_PRODUCT_QUANTITY" => "Y"
                    )
                );*/?>
        </div>
    </div>
</section>

<div class="modal review-modal fade" id="review-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <form class="modal-content" name="NEW_REVIEW" id="NEW_REVIEW">
            <div class="modal-header">
                <div class="modal-title h5">ОТЗЫВ О Товаре</div>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="rating">
                    <input id="ratingValue" type="text" hidden>
                    <select class="to-rate" name="rating" autocomplete="off">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4" selected="selected">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="userName">Имя</label>
                    <input class="form-control" id="userName" name="AUTHOR" type="text" required>
                </div>
                <div class="form-group">
                    <label for="reviewArea">Отзыв</label>
                    <textarea class="form-control" id="reviewArea" name="TEXT" rows="4" required></textarea>
                </div>
                <input class="form-control" id="element" name="ELEMENT" type="hidden" value="<?=$arResult["ID"];?>">
                <input class="form-control" id="timestamp" name="TIMESTAMP" type="hidden" value="<?=mktime();?>">
                <? include $_SERVER["DOCUMENT_ROOT"] . "/include/agreement.php";?>
                <button class="btn-main btn transition d-block ml-auto mr-auto" type="submit">Отправить</button>
            </div>
        </form>
    </div>
</div>

<div class="modal form-modal fade" id="buyOneClick" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title text-center h5"><? echo 'Быстрый заказ'; ?></div>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times</span>
                </button>
            </div>
            <form method="#" class="modal-body" id="buyOneClickForm" name="buyOneClickForm" onsubmit="ym(25218083, 'reachGoal', 'bistriyzakaz');" novalidate>
                <input type="hidden" name="size_buyoneclick" value="" />
                <input type="hidden" name="amount_buyoneclick" value="1" />
                <input type="hidden" name="current_product_id" value="" />
                <div class="row orm-modal">
                    <div class="col-12">
                        <div id="error-phone-block"></div>
                        <div class="modal-input-wrapper flex-container-w100 flex-side">
                            <label>Ваше Ф.И.О.</label>
                            <input type="text" name="USER_NAME" maxlength="50"/>
                        </div>
                        <div class="modal-input-wrapper flex-container-w100 flex-side">
                            <label>Телефон*</label>
                            <input class="phone-mask" type="text" name="USER_PHONE" maxlength="50" required minlength="10"/>
                        </div>
                        <?/*
                        <div class="modal-input-wrapper flex-container-w100 flex-side">
                            <label>Адрес доставки</label>
                            <input type="text" name="USER_ADDRESS" />
                        </div>
                        */?>
                        <? include $_SERVER["DOCUMENT_ROOT"] . "/include/agreement.php";?>
                        <div class="modal-input-wrapper flex-container-w100 flex-center">
                            <input class="mybtn transition yourCity-btn" type="submit" name="oneClick"
                                   value="Купить" disabled="disabled">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- data-target="#thanksForOrder" data-toggle="modal" -->

<div class="modal form-modal fade" id="thanksForOrder" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title text-center h5" style="margin-bottom:20px;">Спасибо за заказ! Менеджер свяжется с вами как можно скорее.</div>
            </div>
            <!-- <div class="row">
                <div class="col-auto modal_full">
                    <a href="#" class="mybtn transition yourCity-btn closeModal" data-error_kol="Закрыть">Закрыть</a>
                </div>
            </div> -->
        </div>
    </div>
</div>
<div class="modal form-modal fade" id="inCartClick" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered inCartModal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title text-center h5" data-error_kol="Извините, но на складе нет выбранного количества модели">Товар добавлен в корзину</div>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times</span>
                </button>
            </div>
            <div class="row js-hidden desc">
                <div class="col-12">Вы можете скорректировать количество данного товара или выбрать другую модель</div>
            </div><div class="row">
                <div class="col-6 modal_full">
                    <a href="#" class="mybtn transition yourCity-btn closeModal" data-error_kol="Выбрать другую модель">Продолжить покупки</a>
                </div>
                <div class="col-6 modal_full">
                    <a href="/personal/cart/" class="mybtn transition yourCity-btn full">Перейти в корзину</a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.plugin.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.countdown.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.countdown-ru.js"></script>
<script>
    $(document).ready(function() {
        var top = 0;
        $(this).find('.goods-label-block').children().each(function() {
            if (!$(this).hasClass('amount-manual-label')) {
                $(this).css('top', top + 'px');
                top += 21;
            } else {
                var h = $('.slider-for').height() - 21;
                $(this).css('top', h + 'px');
            }
        });
        $('.goods-label-block .goods-label-fixprice').css('left', '15px');


        <?if ($finish):?>
        var finish = '<?=$finish?>';
        var ar = finish.trim().split(' ');
        var d = ar[0];
        var t = ar[1];
        var arD = d.trim().split('.');
        var arT = t.trim().split(':');
        var finishDay = new Date(arD[2], parseInt(arD[1])-1, arD[0], arT[0], arT[1], arT[2], 0);
        $("#timer").countdown({until: finishDay, format: 'dHMS'});
        <?endif;?>

    });
</script>
<script type="text/javascript">
    var path_to_ajax = '<?=SITE_DIR."include/".LANGUAGE_ID."/catalog_element/ajax_add_basket.php"?>';
    var path_to_basket = '<?=$arParams["BASKET_URL"]?>';
    var path_to_add_review = '<?=SITE_DIR."include/".LANGUAGE_ID."/catalog_element/ajax_add_review.php"?>';
    var ofrs_prices = <?= !empty($arResult['OFRS_PRICES']) ? \Bitrix\Main\Web\Json::encode($arResult['OFRS_PRICES']) : [] ?>;
    console.log(ofrs_prices);
</script>

<?
if (isset($_GET['USER_NAME']) && isset($_GET['USER_PHONE']) && isset($_GET['size_buyoneclick']) && isset($_GET['amount_buyoneclick'])/*  && isset($_GET['oneClick']) */) {

// Формируем заказ
// Определяем товар/торговое предложение
    $productId = $arResult['ID'];
    $quantity = 1;
    if(isset($_GET['current_product_id']) && !empty($_GET['current_product_id'])) {
        $productId = $_GET['current_product_id'];
    }
    if(isset($_GET['amount_buyoneclick']) && !empty($_GET['amount_buyoneclick'])) {
        $quantity = $_GET['amount_buyoneclick'];
    }


// Входящие параметры и константы
    define('PAY_SYSTEM_ID', 4); // Yandex
    define('DEFAULT_LOCATION_ID', '0000073738'); // Москва

// Получаем данные о товаре по его ID
    $dbItems = CIBlockElement::GetList(
        array(),
        array('ID' => IntVal($productId)),
        false,
        false,
        array('ID', 'IBLOCK_ID', 'NAME', 'IBLOCK_EXTERNAL_ID', 'EXTERNAL_ID')
    );
    if ($arItem = $dbItems->GetNext()) {
        $arItem['PRICE'] = CCatalogProduct::GetOptimalPrice($arItem['ID'], 1, $USER->GetUserGroupArray(), "N");

        // Создаем корзину и добавляем туда товар, 1шт
        $basket = \Bitrix\Sale\Basket::create(SITE_ID);
        $basketItem = $basket->createItem("catalog", $arItem['ID']);

        $basketItem->setFields(
            array(
                'PRODUCT_ID' => $arItem['ID'],
                'NAME' => $arItem['NAME'],
                'BASE_PRICE' =>$arItem['PRICE']['RESULT_PRICE']['BASE_PRICE'],
                'PRICE' => $arItem['PRICE']['RESULT_PRICE']['DISCOUNT_PRICE'],
                'CURRENCY' => $arItem['PRICE']['RESULT_PRICE']['CURRENCY'],
                'QUANTITY' => $quantity,

                "PRODUCT_PROVIDER_CLASS" => "Bitrix\Catalog\Product\CatalogProvider",
                "CATALOG_XML_ID" => $arItem["IBLOCK_EXTERNAL_ID"],
                "PRODUCT_XML_ID" => $arItem["EXTERNAL_ID"],
            )
        );

        // Создаем заказ и привязываем корзину, перерасчет происходит автоматически
        $order = \Bitrix\Sale\Order::create(SITE_ID, ($USER->IsAuthorized()) ? $USER->GetID() : \CSaleUser::GetAnonymousUserID());
        $order->setPersonTypeId(1); // Физ. лицо
        $order->setBasket($basket);

        // Создаём одну отгрузку и устанавливаем способ доставки - "Без доставки" (он служебный)
        $shipmentCollection = $order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem();
        $service = \Bitrix\Sale\Delivery\Services\Manager::getById(\Bitrix\Sale\Delivery\Services\EmptyDeliveryService::getEmptyDeliveryServiceId());
        $shipment->setFields(array(
            'DELIVERY_ID' => $service['ID'],
            'DELIVERY_NAME' => $service['NAME'],
        ));
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        $arResult['basket'] = $basket;
        foreach ($basket as $item) {
            $shipmentItem = $shipmentItemCollection->createItem($item);
            $shipmentItem->setQuantity($item->getQuantity());
        }

        // Создаём оплату
        $paymentCollection = $order->getPaymentCollection();
        $payment = $paymentCollection->createItem();
        $paySystemService = \Bitrix\Sale\PaySystem\Manager::getObjectById(PAY_SYSTEM_ID);
        $payment->setFields(array(
            'PAY_SYSTEM_ID' => $paySystemService->getField("PAY_SYSTEM_ID"),
            'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
            'SUM' => $order->getPrice(),
        ));

        // Устанавливаем свойства
        $propertyCollection = $order->getPropertyCollection();
        $nameProp = $propertyCollection->getPayerName();
        $nameProp->setValue(htmlspecialcharsbx($_GET['USER_NAME']));
        $emailProp = $propertyCollection->getPhone();
        $emailProp->setValue(htmlspecialcharsbx($_GET['USER_PHONE']));
        //todo: включить локацию для быстрого заказа
        //$locProp = $propertyCollection->getDeliveryLocation();
        //$locProp->setValue(DEFAULT_LOCATION_ID);

        // Сохраняем
        $order->doFinalAction(true);
        $order->save();
    }


// Формируем письмо
    $url = 'https://orthoboom.ru' . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

    $to = 'rusak.ai@julianna.ru';
    //$to = 'ko-alex@mail.ru';
    $headers =  'From: "Интернет-магазин «ORTHOBOOM»" <noreply@orthoboom.ru>' . "\r\n" .
        'Reply-To: "Интернет-магазин «ORTHOBOOM»" <noreply@orthoboom.ru>' . "\r\n" .
        'MIME-Version: 1.0' . "\r\n".
        'Content-type: text/html; charset=windows-1251' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    $message = '<p><b>Товар</b>: ' . '<a href="' . $url . '">' . $url . '</a></p>';
    $message .= '<p><b>Размер</b>: ' . $_GET['size_buyoneclick'] . '</p>';
    $message .= '<p><b>Количество</b>: ' . $_GET['amount_buyoneclick'] . '</p>';
    $message .= '<p><b>Имя</b>: ' . $_GET['USER_NAME'] . '</p>';
    $message .= '<p><b>Адрес доставки</b>: ' . $_GET['USER_ADDRESS'] . '</p>';
    $message .= '<p><b>Телефон</b>: ' . $_GET['USER_PHONE'] . '</p>';
    $message .= '<p><small>Это сообщение имейет информационный характер и сформировано автоматически. Пожалуйста, не отвечайте на него.</small></p>';
    mail($to, 'Заказ в 1 клик из интернет-магазина «ORTHOBOOM»', $message, $headers);

    $arEventFields = array(
        'ORDER_ID'             => mt_rand(),
        'ORDER_LIST'           => str_replace("'", '"', $arResult['NAME']) . ' (' . $_GET['size_buyoneclick'] . ') x ' . $_GET['amount_buyoneclick'] . ' пар',
        'PRICE'            	   => round($arResult['MIN_PRICE']['DISCOUNT_VALUE'] * $_GET['amount_buyoneclick']),
        'CONTACT_FULL_NAME'    => $_GET['USER_NAME'],
        'CONTACT_PHONE'   	   => $_GET['USER_PHONE'],
        'CONTACT_FULL_ADDRESS' => $_GET['USER_ADDRESS'],
    );
    CEvent::SendImmediate('SALE_NEW_ORDER_1CLICK', 's1', $arEventFields, 'N');

    ?>
    <script>
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            'ecommerce': {
                'currencyCode': 'RUB',
                'purchase': {
                    'actionField': {
                        'id': 'QUICK_ORDER_<?=$arResult["ID"]?>',
                        'affiliation': 'ORTHOBOOM',
                    },
                    'products': [
                        {
                            'name': '<?=str_replace("'", '"', $arResult["NAME"])?>',
                            'id': 'QUICK_ORDER_PRODUCT_<?=$arResult["ID"]?>',
                            'price': <?=$arResult["MIN_PRICE"]["DISCOUNT_VALUE"]?>,
                            'quantity': 1
                        }
                    ]
                }
            },
            'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Purchase',
            'gtm-ee-event-non-interaction': 'False',
        });
        location.href = '<?=$url?>';
    </script>
    <?
}
?>

<?/*retail rocket*/?>
<script type="text/javascript">
    (window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function() {
        try{ rrApi.view(<?=$arResult["ID"]?>); } catch(e) {}
    })
</script>