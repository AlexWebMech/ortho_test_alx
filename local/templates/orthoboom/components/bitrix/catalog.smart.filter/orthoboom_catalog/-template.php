<? if(! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

$this->setFrameMode(true);

if (! $arParams["UI_MAIN_CLASS"])
	$arParams["UI_MAIN_CLASS"] = "catalog";
if (! $arParams["UI_MAIN_FILTER_CLASS"])
	$arParams["UI_MAIN_FILTER_CLASS"] = $arParams["UI_MAIN_CLASS"] . "-filter";
if (! $arParams["UI_WRAP_CLASS"])
	$arParams["UI_WRAP_CLASS"] = "ui-filter--box";

//vard($arResult["ELEMENT_COUNT"], 'aaa');

unset($arResult["COMBO"]);
?>

<form class="<?= $arParams["UI_MAIN_FILTER_CLASS"] ?>-filter ui-filter <?= $arParams["UI_WRAP_CLASS"] ?>">

<span class="ui-filter-toggle--normal">
	Показать фильтр
</span>

<div class="ui-filter-block">

	<input type="hidden" name="set_filter" value="Y" /> 
	
	<div class="ui-filter-header">
		<div class="ui-filter-title"><span class="ui-icon"></span><span class="ui-text">Фильтр</span></div>
		<div class="ui-filter-header-tools">
			<span class="ui-filter-hide-all-fields">Скрыть все</span>
			<span class="ui-filter-show-all-fields">Показать все</span>
		</div>
	</div>
	
	<div class="ui-filter-fields ui-fields">
	<? foreach($arResult["FIELDS"] as $sFieldCode => $arField) : 
		if ($arField["ACTIVE"] == "N")
			continue; ?> 
		<div class="ui-filter-field-row ui-filter-field field-code-<?= $sLowerFieldCode = tolower($sFieldCode) ?> field-type-<?= tolower($arField["FILTER_FIELD_TYPE"]); ?> <?= $arField["ATTR_CLASS"] ?>" data-field-code="<?= htmlspecialchars($sFieldCode) ?>" data-field-type="<?= htmlspecialchars(tolower($arField["FILTER_FIELD_TYPE"])); ?>"> 
			<div class="ui-filter-field-title-col ui-title-col ui-filter-field-js-toggle-field">
				<div class="ui-filter-field-title"><span class="ui-filter-field-title-icon ui-icon"></span><span class="ui-text"><?= $arField["TITLE"] ?></span></div>
				<div class="ui-filter-field-clear ui-filter-field-js-clear-field"><span class="ui-icon"></span><span class="ui-text">Reset</span></div>
			</div>
			<div class="ui-filter-field-main-col ui-main-col">
			<div class="ui-filter-field-main-col-inner ui-inner">	
				<? if ($arField["FILTER_FIELD_TYPE"] == "SELECTOR") : ?>
					<div class="ui-option-items">
					<? foreach($arField["LIST_ITEMS"] as $sOptionItemValue => $arOptionItem) : ?>
						<div class="ui-option-item">
							<label class="ui-checkbox ui-checkbox--normal">
								
								<input type="hidden" name="<?= htmlspecialchars($arOptionItem["INPUT_NAME"] !== NULL ? $arOptionItem["INPUT_NAME"] : $arField["CODE"]); ?>" value="" />
								<input type="<? if ($arField["MULTIPLE"] == "Y") : ?>checkbox<? else : ?>radio<? endif; ?>" name="<?= htmlspecialchars($arOptionItem["INPUT_NAME"] !== NULL ? $arOptionItem["INPUT_NAME"] : $arField["CODE"]); ?>" <? if (in_array($sOptionItemValue, $arField["VALUE"])) : ?> checked<? endif; ?><? if ($arOptionItem["DISABLED"] == "Y") : ?> disabled<? endif; ?> value="<?= htmlspecialchars($arOptionItem["INPUT_VALUE"]); ?>">
								<span class="ui-box">
									<span class="ui-checkbox-icon ui-icon"></span>
									<span class="ui-text"><?= $arOptionItem["HTML"] ?></span>
									<span class="ui-value"><?= $arOptionItem["COUNT"] ?></span>
								</span>
							</label>
						</div>
					<? endforeach; ?>
					</div>
				<? elseif ($arField["FILTER_FIELD_TYPE"] == "LIST") : ?>
						
					
				
				<? elseif ($arField["FILTER_FIELD_TYPE"] == "NUMBER" || $arField["FILTER_FIELD_TYPE"] == "RANGE_SLIDER") : ?>
					<div class="filter-field-type-number">
						<div class="filter-field-type-number-flex">
							<span class="filter-field-type-number-min-title">от</span>
							<? if ($arSubField = $arField["FIELDS"]["MIN"]) : ?>
								<input type="text" name="<?= htmlspecialchars($arSubField["INPUT_NAME"]) ?>" value="<?= htmlspecialchars($arSubField["VALUE"]) ?>" placeholder="<?= htmlspecialchars($arSubField["MIN_VALUE"]) ?>" />
							<? endif; ?>	
							<span class="filter-field-type-number-max-title"> до </span>
							<? if ($arSubField = $arField["FIELDS"]["MAX"]) : ?>
								<input type="text" name="<?= htmlspecialchars($arSubField["INPUT_NAME"]) ?>" value="<?= htmlspecialchars($arSubField["VALUE"]) ?>" placeholder="<?= htmlspecialchars($arSubField["MAX_VALUE"]) ?>"  />
							<? endif; ?>
							<span class="ui-measure"></span>
						</div>
						<? if ($arField["FILTER_FIELD_TYPE"] == "RANGE_SLIDER") : ?>
							<? if (($nMinValue = $arField["FIELDS"]["MIN"]["MIN_VALUE"]) === NULL || ($nMaxValue = $arField["FIELDS"]["MAX"]["MAX_VALUE"]) === NULL) : ?>
								...
							<? else: ?>
								<div class="ui-slider"></div>
								<script type="text/javascript">
									$(".ui-filter--normal .ui-row.field-code-<?= $sLowerFieldCode ?> .ui-slider").slider({
										range: true,
										min: <?= $nMinValue ?>,
										max: <?= $nMaxValue ?>,
										values: [<?= $arField["FIELDS"]["MIN"]["FILTRED_VALUE"] ? $arField["FIELDS"]["MIN"]["FILTRED_VALUE"] : $nMinValue ?>, <?= $arField["FIELDS"]["MAX"]["FILTRED_VALUE"] ? $arField["FIELDS"]["MAX"]["FILTRED_VALUE"] : $nMaxValue ?>],
										slide: function(event, ui) {
											$(".ui-filter--normal .ui-row.field-code-<?= $sLowerFieldCode ?> input").eq(0).prop("value", ui.values[0]);
											$(".ui-filter--normal .ui-row.field-code-<?= $sLowerFieldCode ?> input").eq(1).prop("value", ui.values[1]);
										}
									});
									
								</script>
								<? //debug($arField); ?>
							<? endif; ?>
						<? endif; ?>
					</div>	
				<? else : ?>
					...
				<? endif; ?>
				
			</div>
			</div>
		</div>
	<? endforeach; ?>
	</div>
	
	<div class="ui-filter-footer">
		<button class="ui-filter-clear ui-button ui-button--normal" name="clear_filter" value="Y">Сбросить</button>
		<button class="ui-filter-submit ui-button ui-button--normal" name="set_filter" value="Y">Показать</button>
	</div>
	
</div>

<? //Result ?>
<? if (strlen($arResult["ELEMENT_COUNT"])) : ?>
	<div class="ui-filter-result hide">
		<span class="ui-filter-result-close ui-popup-close">x</span>
		<div class="ui-filter-result-label">Найдено товаров: <?= $arResult["ELEMENT_COUNT"] ?></div>
		<a class="ui-filter-result-link" href="#">Показать</a>
	</div>
<? endif; ?>
<? //-- ?>

</form>	

<?
//Js params
$arResult["UNI_FILTER"]["JS_PARAMS"] = is_array($arParams["UI_UNI_FILTER_JS_PARAMS"]) ? $arParams["UI_UNI_FILTER_JS_PARAMS"] : [];
$arResult["UNI_FILTER"]["JS_PARAMS"]["uiMainClass"] = $arParams["UI_MAIN_CLASS"];

if ($arParams["INSTANT_RELOAD"] == "Y")
	$arResult["UNI_FILTER"]["JS_PARAMS"]["instantRefreshItems"] = true;

if (! isset($arResult["UNI_FILTER"]["JS_PARAMS"]["animationFieldDuration"]))
	$arResult["UNI_FILTER"]["JS_PARAMS"]["animationFieldDuration"] = 380;
//--
?>

<script type="text/javascript">
$(document).ready(function() {
	$(".<?= $arParams["UI_MAIN_CLASS"] ?>-filter").uniFilter(<?= \Bitrix\Main\Web\Json::encode($arResult["UNI_FILTER"]["JS_PARAMS"]) ?>);
});
</script>

<?
//Ajax profiles
$arAjaxProfile = [];
$arAjaxProfile["wrapTarget"] = "." . $arParams["UI_MAIN_CLASS"] . "-filter";
$arAjaxProfile["formTarget"] = "." . $arParams["UI_MAIN_CLASS"] . "-filter";
$arAjaxProfile["cut"] = true;
$arAjaxProfile["dataEx"] = "uni_ajax_profile=" . $arParams["UI_MAIN_CLASS"] . "-filter";

foreach($arResult["FIELDS"] as $sFieldCode => $arField)
{
	if (TRUE)
		$arAjaxProfile["commands"][] =  ["target" => ".field-code-" . tolower($sFieldCode), "cut" => true, "command" => "html"];
}
$arAjaxProfile["commands"][] = ["target" => ".ui-filter-result", "cut" => true, "command" => "html"];

$arResult["UNI_AJAX"]["PROFILES"]["FILTER"] = $arAjaxProfile;
$arResult["UNI_AJAX"]["PROFILES"]["FILTER-RESULT"] = $arAjaxProfile;
//--
?>

<? if (TRUE) : ?>
<script type="text/javascript">
	if (typeof(uniAjaxLib) != "undefined") {
		uniAjaxLib.addProfile("<?= $arParams["UI_MAIN_CLASS"] ?>-filter", <?= \Bitrix\Main\Web\Json::encode($arResult["UNI_AJAX"]["PROFILES"]["FILTER"]) ?>);
		uniAjaxLib.addProfile("<?= $arParams["UI_MAIN_CLASS"] ?>-filter-result", <?= \Bitrix\Main\Web\Json::encode($arResult["UNI_AJAX"]["PROFILES"]["FILTER-RESULT"]) ?>);
	}
</script>
<? endif; ?>

