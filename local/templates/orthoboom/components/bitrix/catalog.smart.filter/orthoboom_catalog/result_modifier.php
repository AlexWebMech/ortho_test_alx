<? if(! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

//vard($arResult["ITEMS"], 'fff');

if (TRUE)
{
	$arFilter = $this->__component->makeFilter($arParams["FILTER_NAME"]);
	if (is_array($GLOBALS[$arParams["PREFILTER_NAME"]]))
		$arFilter = array_merge($GLOBALS[$arParams["PREFILTER_NAME"]], $arFilter);
	$arResult["ELEMENT_COUNT"] = CIBlockElement::GetList(array(), $arFilter, array(), false);
}

if  ($arParams["SEF_MODE"] == "Y")
{
	$arParams["SEF_RULE"] = str_replace(["#SECTION_ID#", "#SECTION_CODE#", "#SECTION_CODE_PATH#"], [$arParams["SECTION_ID"], $arParams["SECTION_CODE"], $arParams["SECTION_CODE_PATH"]], $arParams["SEF_RULE"]);
	$arResult["URL"] = str_replace('//', '/', $this->__component->makeSmartUrl($arParams["SEF_RULE"], true));
	$arResult["URL"] = str_replace("/filter/clear/apply/", "/", $arResult["URL"]);
}

foreach($arResult["ITEMS"] as $arBxFilter)
{
	if (! $arBxFilter["IBLOCK_ID"])
		$sFieldCode = $arBxFilter["CODE"];
	else
		$sFieldCode = "PROPERTY_" . $arBxFilter["CODE"];
	
	$arField = ["FILTER_FIELD_TYPE" => null, "TITLE" => $arBxFilter["NAME"], "MULTIPLE" => "N"];
	if ($arBxFilter["PROPERTY_TYPE"] == "L")
	{
		$arField["MULTIPLE"] = "Y";

		if ($arBxFilter["DISPLAY_EXPANDED"] == "N")
			$arField["COLLAPSED"] = true;
		else
			$arField["COLLAPSED"] = false;
		
		if ($arBxFilter["DISPLAY_TYPE"] == "F")
			$arField["FILTER_FIELD_TYPE"] = "SELECTOR";
		else
			$arField["FILTER_FIELD_TYPE"] = "SELECTOR";
		
		if ($arBxFilter["DISPLAY_EXPANDED"] == "N")
			$arField["ATTR_CLASS"] = " hide";
 		
		
		$arField["LIST_ITEMS"] = [];
		foreach($arBxFilter["VALUES"] as $arBxFilterValue)
		{
			$arOptionItem = ["TITLE" => $arBxFilterValue["VALUE"], "VALUE" => $arBxFilterValue["CONTROL_NAME"], "COUNT" => $arBxFilterValue["ELEMENT_COUNT"]];
			//if ($arBxFilterValue["ELEMENT_COUNT"])
			//	$arOptionItem["HTML"] .= " <span class=\"ui-count\">" . $arBxFilterValue["ELEMENT_COUNT"] . "</span>"; 
			
			$arOptionItem["INPUT_NAME"] = $arBxFilterValue["CONTROL_NAME"];
			$arOptionItem["INPUT_VALUE"] = "Y";
			
			if ($arBxFilterValue["DISABLED"])
				$arOptionItem["DISABLED"] = "Y";	
			
			$arField["ITEMS"][$arBxFilterValue["CONTROL_NAME"]] = $arOptionItem;
			
			if ($arBxFilterValue["CHECKED"] === true)
				$arField["VALUE"][] = $arBxFilterValue["CONTROL_NAME"];
		}
		if (! $arBxFilter["VALUES"])
			continue;
	}
	elseif ($arBxFilter["PROPERTY_TYPE"] == "N" || $arBxFilter["PRICE"] === true)
	{
		if (1 || $arBxFilter["DISPLAY_TYPE"] == "A" || $arBxFilter["PRICE"])
			$arField["FILTER_FIELD_TYPE"] = "RANGE_SLIDER";
		else
			$arField["FILTER_FIELD_TYPE"] = "RANGE";
		
		if (is_array($arBxFilter["VALUES"]["MIN"]))
			$arField["FIELDS"]["MIN"] = ["FIELD_TYPE" => "NUMBER", "INPUT_NAME" => $arBxFilter["VALUES"]["MIN"]["CONTROL_NAME"], "VALUE" => $arBxFilter["VALUES"]["MIN"]["HTML_VALUE"], "MIN_VALUE" => $arBxFilter["VALUES"]["MIN"]["VALUE"], "MAX_VALUE" => $arBxFilter["VALUES"]["MAX"]["VALUE"]];
		if (is_array($arBxFilter["VALUES"]["MAX"]))
			$arField["FIELDS"]["MAX"] = ["FIELD_TYPE" => "NUMBER", "INPUT_NAME" => $arBxFilter["VALUES"]["MAX"]["CONTROL_NAME"], "VALUE" => $arBxFilter["VALUES"]["MAX"]["HTML_VALUE"], "MIN_VALUE" => $arBxFilter["VALUES"]["MIN"]["VALUE"], "MAX_VALUE" => $arBxFilter["VALUES"]["MAX"]["VALUE"]];	
	}	
	
	if (is_array($arParams["FIELD_MASKS"][$sFieldCode]))
		$arField = array_replace($arField, $arParams["FIELD_MASKS"][$sFieldCode]);
	
	$arResult["FIELDS"][$sFieldCode] = $arField;	
}

//Orthoboom
if ($arField = &$arResult["FIELDS"]["PROPERTY_OBUV_RAZMER"])
{
	foreach($arField["ITEMS"] as &$arItem)
		if (mb_substr($arItem["TITLE"], 0, 2) == "р.")
			$arItem["TITLE"] = mb_substr($arItem["TITLE"], 2);
	unset($arItem);
}
unset($arField);
//--

$arResult["TAGS"] = [];
foreach($arResult["FIELDS"] as $sFieldCode => $arField)
{
	if ($arField["FILTER_FIELD_TYPE"] == "SELECTOR" || $arField["FILTER_FIELD_TYPE"] == "LIST")
	{
		if ($arField["MULTIPLE"] != "Y" && mb_strlen($arField["VALUE"]))
		{
			$arTag = ["FIELD_CODE" => $sFieldCode, "TITLE" => $arField["TITLE"]];
			$arTag["HTML"] = $arField["ITEMS"][$arField["VALUE"]]["TITLE"];
			$arResult["TAGS"][] = $arTag;
		}
		elseif ($arField["MULTIPLE"] == "Y" && is_array($arField["VALUE"]) && count($arField["VALUE"]))
		{
			$arTag = ["FIELD_CODE" => $sFieldCode, "TITLE" => $arField["TITLE"], "HTML" => ""];
			foreach($arField["VALUE"] as $varFieldSubValue)
				$arTag["HTML"] .= (mb_strlen($arTag["HTML"]) ? ", " : "") . $arField["ITEMS"][$varFieldSubValue]["TITLE"];
			$arResult["TAGS"][] = $arTag;
		}
	}
	elseif ($arField["FILTER_FIELD_TYPE"] == "RANGE" || $arField["FILTER_FIELD_TYPE"] == "RANGE_SLIDER")
	{
		if ($arField["MULTIPLE"] != "Y" && (mb_strlen($arField["FIELDS"]["MIN"]["VALUE"]) || mb_strlen($arField["FIELDS"]["MAX"]["VALUE"])))
		{
			$arTag = ["FIELD_CODE" => $sFieldCode, "TITLE" => $arField["TITLE"], "HTML" => ""];
			if (mb_strlen($arField["FIELDS"]["MIN"]["VALUE"]))
				$arTag["HTML"] .= "от " . $arField["FIELDS"]["MIN"]["VALUE"];
			if (mb_strlen($arField["FIELDS"]["MAX"]["VALUE"]))
				$arTag["HTML"] .= " до " . $arField["FIELDS"]["MAX"]["VALUE"];
			if (mb_strlen($arField["MEASURE"]) || mb_strlen($arField["MEASURE_HTML"]))
				$arTag["HTML"] .= " " . (mb_strlen($arField["MEASURE_HTML"]) ? $arField["MEASURE_HTML"] : $arField["MEASURE"]);
			$arTag["HTML"] = trim($arTag["HTML"]);
			$arResult["TAGS"][] = $arTag;
		}
	}
}

unset($arResult["COMBO"]);

//vard($arParams, 'bbb');
//vard($arResult["FIELDS"], 'bbb');

?>