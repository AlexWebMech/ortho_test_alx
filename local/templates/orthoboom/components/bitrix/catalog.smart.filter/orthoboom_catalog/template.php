<? if(! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

$this->setFrameMode(true);

//Main
if (! $arParams["UI_MAIN_CLASS"])
	$arParams["UI_MAIN_CLASS"] = "catalog";
if (! $arParams["UI_MAIN_FILTER_CLASS"])
	$arParams["UI_MAIN_FILTER_CLASS"] = $arParams["UI_MAIN_CLASS"] . "-filter";

//Wrap
if (! $arParams["UI_FILTER_WRAP_CLASS"])
	if ($arParams["UI_WRAP_CLASS"])
		$arParams["UI_FILTER_WRAP_CLASS"] = $arParams["UI_WRAP_CLASS"];
	else
		$arParams["UI_FILTER_WRAP_CLASS"] = "ui-filter--box";

//Ui
if (! $arParams["UI_FILTER_TOGGLE_ACTION_SHOW"])
	$arParams["UI_FILTER_TOGGLE_ACTION_SHOW"] = "Y";
if (! $arParams["UI_FILTER_TOGGLE_ACTION_CLASS"])
	$arParams["UI_FILTER_TOGGLE_ACTION_CLASS"] = "ui-button ui-button--normal";

//Fields
if (! $arParams["UI_FIELD_COLLAPSE_BY_TITLE"])
	$arParams["UI_FIELD_COLLAPSE_BY_TITLE"] = "Y";

//Field types
if (! $arParams["UI_CHECKBOX_INPUT_CLASS"])
	$arParams["UI_CHECKBOX_INPUT_CLASS"] = "ui-checkbox ui-checkbox--normal ui-selector";
if (! $arParams["UI_RADIO_INPUT_CLASS"])
	$arParams["UI_RADIO_INPUT_CLASS"] = "ui-radio ui-radio--normal ui-selector";

if (! $arParams["UI_LIST_CLASS"])
	$arParams["UI_LIST_CLASS"] = "ui-select ui-select--normal";
if (! $arParams["UI_RANGE_SLIDER_FIELD_CLASS"])
	$arParams["UI_RANGE_SLIDER_FIELD_CLASS"] = "ui-slider ui-slider--normal";
if (! $arParams["UI_RANGE_FIELD_CLASS"])
	$arParams["UI_RANGE_FIELD_CLASS"] = "ui-range-field ui-range-field--normal";






?>

<form class="<?= $arParams["UI_FILTER_WRAP_CLASS"] ?> <?= $arParams["UI_MAIN_FILTER_CLASS"] ?> ui-filter">

	<? 
	//////////// TAGS ////////////
	if ($arParams["UI_TAGS_SHOW"] == "Y") : ?>
		<? if (mb_strlen($arParams["UI_TAGS_VIEW_TARGET"]) && ! $_REQUEST["uni_ajax"]) :
			$this->SetViewTarget($arParams["UI_TAGS_VIEW_TARGET"]); ?>
			<div class="<?= $arParams["UI_FILTER_WRAP_CLASS"] ?>--outer ui-filter--outer ui-filter--tags-outer ">
		<? endif; ?>
			<div class="ui-filter--tags-wrap">
			<? if ($arResult["TAGS"]) : ?>
				<div class="ui-filter--tags">
				<? foreach($arResult["TAGS"] as $arTag) : ?>
					<span class="ui-filter--tag" data-field-code="<?= htmlspecialchars($arTag["FIELD_CODE"]); ?>">
						<span class="ui-filter--tag-icon ui-icon"></span>
						<span class="ui-filter--tag-text ui-text">
							<span class="ui-filter--tag-title"><?= $arTag["TITLE"] ?></span><span class="">: </span>
							<span class="ui-filter--tag-main"><?= $arTag["HTML"] ?></span>
						</span>
						<span class="ui-filter--tag-clear" data-target-field-code="<?= htmlspecialchars($arTag["FIELD_CODE"]); ?>"><span class="ui-filter--tag-clear-icon ui-icon"></span><span class="ui-text"></span></span>
					</span>
				<? endforeach; ?>
				</div>
			<? endif; ?>
			</div>
		<? if (mb_strlen($arParams["UI_TAGS_VIEW_TARGET"]) && ! $_REQUEST["uni_ajax"]) : ?>
			</div>
		<? $this->EndViewTarget(); 
		endif; ?>	
	<? endif; 
	//////////// --
	?>
	
	<? //Mobile toggle button
	if ($arParams["UI_FILTER_TOGGLE_ACTION_SHOW"] == "Y") : ?>
	<span class="ui-filter--toggle">
		<span class="ui-filter--toggle-action <?= $arParams["UI_FILTER_TOGGLE_ACTION_CLASS"] ?>">Показать фильтр</span>
	</span>
	<? endif; 
	//-- ?>
	
	<? if (! $_REQUEST["uni_ajax"]) : ?>
	<div class="ui-filter--block">
	
		<input type="hidden" name="set_filter" value="Y" /> 
		
		<div class="ui-filter--header">
			<div class="ui-filter--title"><span class="ui-icon"></span><span class="ui-text">Фильтр</span></div>
			<div class="ui-filter--header-tools">
				<span class="ui-filter--hide-all-fields">Скрыть все</span>
				<span class="ui-filter--show-all-fields">Показать все</span>
			</div>
		</div>
		
		<? //////////// FIELDS //////////// ?>
		<div class="ui-filter--fields ui-fields">
		<? foreach($arResult["FIELDS"] as $sFieldCode => $arField) : 
			if ($arField["ACTIVE"] == "N")
				continue; 
			$sLowerFieldCode = tolower($sFieldCode); ?> 
			<div class="ui-filter--field-row ui-filter--field field-code-<?= $sLowerFieldCode ?> field-type-<?= tolower($arField["FILTER_FIELD_TYPE"]); ?> <?= $arField["ATTR_CLASS"] ?>" data-field-code="<?= htmlspecialchars($sFieldCode) ?>" data-field-type="<?= htmlspecialchars(tolower($arField["FILTER_FIELD_TYPE"])); ?>"> 
				<div class="ui-filter--field-title-col ui-title-col <? if ($arParams["UI_FIELD_COLLAPSE_BY_TITLE"] == "Y") : ?>ui-filter--js-field-toggle-field<? endif; ?>">
					<div class="ui-filter--field-title"><span class="ui-filter--field-title-icon ui-icon"></span><span class="ui-text"><?= $arField["TITLE"] ?></span></div>
					<div class="ui-filter--field-clear ui-filter--field-js-clear-field"><span class="ui-icon"></span><span class="ui-text">Сбросить</span></div>
				</div>
				<div class="ui-filter--field-main-col ui-main-col">
				<div class="ui-filter--field-main-col-inner ui-inner">	
					<? if ($arField["FILTER_FIELD_TYPE"] == "SELECTOR") : 
						$sInputType = $arField["MULTIPLE"] != "Y" ? "radio" : "checkbox"; ?>
						<div class="ui-option-items">
						<? foreach($arField["ITEMS"] as $sOptionItemValue => $arOptionItem) : ?>
							<div class="ui-option-item ui-selector--wrap ui-<?= $sInputType ?>--wrap">
								<label class="ui-<?= $sInputType ?> ui-selector <?= $arField["MULTIPLE"] != "Y" ? ($arField["RADIO_CLASS"] ? $arField["RADIO_CLASS"] : $arParams["UI_RADIO_INPUT_CLASS"]) : ($arField["CHECKBOX_CLASS"] ? $arField["CHECKBOX_CLASS"] : $arParams["UI_CHECKBOX_INPUT_CLASS"]); ?>">
									<input type="hidden" name="<?= htmlspecialchars($arOptionItem["INPUT_NAME"] !== NULL ? $arOptionItem["INPUT_NAME"] : $arField["CODE"]); ?>" value="" />
									<input type="<? if ($arField["MULTIPLE"] == "Y") : ?>checkbox<? else : ?>radio<? endif; ?>" name="<?= htmlspecialchars($arOptionItem["INPUT_NAME"] !== NULL ? $arOptionItem["INPUT_NAME"] : $arField["CODE"]); ?>" <? if (in_array($sOptionItemValue, $arField["VALUE"])) : ?> checked<? endif; ?><? if ($arOptionItem["DISABLED"] == "Y") : ?> disabled<? endif; ?> value="<?= htmlspecialchars($arOptionItem["INPUT_VALUE"]); ?>">
									<span class="ui-box">
										<span class="ui-checkbox--icon ui-icon"></span>
										<span class="ui-text"><?= $arOptionItem["TITLE"] ?></span>
										<span class="ui-value"><?= $arOptionItem["COUNT"] ?></span>
									</span>
								</label>
							</div>
						<? endforeach; ?>
						</div>
					<? elseif ($arField["FILTER_FIELD_TYPE"] == "LIST") : ?>
							
						
					
					<? elseif ($arField["FILTER_FIELD_TYPE"] == "RANGE" || $arField["FILTER_FIELD_TYPE"] == "RANGE_SLIDER") : ?>
						<div class="">
						
							<div class="<?= $arParams["UI_RANGE_FIELD_CLASS"] ?>">
								<div class="ui-range-field--flex">
									<span class="ui-range-field--min-title">от</span>
									<? if ($arSubField = $arField["FIELDS"]["MIN"]) : ?>
										<input class="ui-range-field--input" type="text" name="<?= htmlspecialchars($arSubField["INPUT_NAME"]) ?>" value="<?= htmlspecialchars($arSubField["VALUE"]) ?>" placeholder="<?= htmlspecialchars($arSubField["MIN_VALUE"]) ?>" />
									<? endif; ?>	
									<span class="ui-range-field--max-title"> до </span>
									<? if ($arSubField = $arField["FIELDS"]["MAX"]) : ?>
										<input class="ui-range-field--input" type="text" name="<?= htmlspecialchars($arSubField["INPUT_NAME"]) ?>" value="<?= htmlspecialchars($arSubField["VALUE"]) ?>" placeholder="<?= htmlspecialchars($arSubField["MAX_VALUE"]) ?>"  />
									<? endif; ?>
									<span class="ui-range-field--measure ui-measure"><?= mb_strlen($arField["MEASURE_HTML"]) ? $arField["MEASURE_HTML"] : $arField["MEASURE"] ?></span>
								</div>
							</div>
							<? if ($arField["FILTER_FIELD_TYPE"] == "RANGE_SLIDER") : ?>
								<? if (($nMinValue = $arField["FIELDS"]["MIN"]["MIN_VALUE"]) === NULL || ($nMaxValue = $arField["FIELDS"]["MAX"]["MAX_VALUE"]) === NULL) : ?>
									...
								<? else: ?>
									<div class="<?= $arParams["UI_RANGE_SLIDER_FIELD_CLASS"] ?>">
										<div class="ui-slider1"></div>
									</div>
									<script type="text/javascript">
										var slider = $(".<?= $arParams["UI_FILTER_WRAP_CLASS"] ?> .field-code-<?= $sLowerFieldCode ?> .ui-slider1").slider({
											range: true,
											min: <?= $nMinValue ?>,
											max: <?= $nMaxValue ?>,
											values: [<?= $arField["FIELDS"]["MIN"]["VALUE"] ? $arField["FIELDS"]["MIN"]["VALUE"] : $nMinValue ?>, <?= $arField["FIELDS"]["MAX"]["VALUE"] ? $arField["FIELDS"]["MAX"]["VALUE"] : $nMaxValue ?>],
											slide: function(event, ui) {
												var minInputNode = $(".<?= $arParams["UI_FILTER_WRAP_CLASS"] ?> .ui-filter--field-row.field-code-<?= $sLowerFieldCode ?> input").eq(0);
												var maxInputNode = $(".<?= $arParams["UI_FILTER_WRAP_CLASS"] ?> .ui-filter--field-row.field-code-<?= $sLowerFieldCode ?> input").eq(1);
												
												if (ui.values[0] <= $(this).slider("option", "min"))
													minInputNode.prop("value", "").change();
												else
													minInputNode.prop("value", ui.values[0]).change();
												
												if (ui.values[1] >= $(this).slider("option", "max"))
													maxInputNode.prop("value", "").change();
												else
													maxInputNode.prop("value", ui.values[1]).change();
											}
										});
										
										$(".<?= $arParams["UI_FILTER_WRAP_CLASS"] ?> .ui-filter--field-row.field-code-<?= $sLowerFieldCode ?> input").on("change", function () {
											if (typeof(uniFilterTimer) != "undefined")
												clearTimeout(uniFilterTimer);
											uniFilterTimer = setTimeout(function (node) {
												//$(".<?= $arParams["UI_MAIN_FILTER_CLASS"] ?>").uniFilter().refreshResult({"node": node});
												$(".<?= $arParams["UI_MAIN_FILTER_CLASS"] ?>").data("uniFilter").refreshResult({"node": $(node)});
											}, 750, $(this));
										});
										
									</script>
									<? //debug($arField); ?>
								<? endif; ?>
							<? endif; ?>
						</div>	
					<? else : ?>
						...
					<? endif; ?>
					
				</div>
				</div>
			</div>
		<? endforeach; ?>
		</div>
		<? //////////// -- FIELDS //////////// ?>
		
		<div class="ui-filter--footer">
			<button class="ui-filter--clear ui-button ui-button--normal" name="clear_filter" value="Y">Сбросить</button>
			<button class="ui-filter--submit ui-button ui-button--normal" name="set_filter" value="Y">Показать</button>
		</div>
		
	</div>
	<? endif; ?>
	
	<? if ($_REQUEST["uni_ajax"]) : ?>
	<script class="uni-ajax-script">
		<? foreach($arResult["FIELDS"] as $arField)
		{
			if ($arField["FILTER_FIELD_TYPE"] == "SELECTOR")
				foreach($arField["ITEMS"] as $arOptionItem)
				{
					if ($arOptionItem["DISABLED"] == "Y") : ?>
						$("input[name='<?= $arOptionItem["INPUT_NAME"] ?>']").prop("disabled", true);
					<? else : ?>
						$("input[name='<?= $arOptionItem["INPUT_NAME"] ?>']").prop("disabled", false);
					<? endif; ?>
					$("input[name='<?= $arOptionItem["INPUT_NAME"] ?>']").parents(".ui-option-item").find(".ui-value").html("<?= htmlspecialchars($arOptionItem["COUNT"] ? $arOptionItem["COUNT"] : "") ?>");
				<?
				}
		} ?>
	</script>
	<? endif; ?>
	
	<? //Result ?>
	<? if (mb_strlen($arResult["ELEMENT_COUNT"])) : ?>
		<div class="ui-filter--result"  <? if (! $_REQUEST["uni_ajax_filter_show_result"]) : ?> style="display: none"<? endif; ?>>
			<span class="ui-filter--result-close ui-popup-close">x</span>
			<div class="ui-filter--result-title">Найдено товаров: <?= $arResult["ELEMENT_COUNT"] ?></div>
			<div class="ui-filter--result-content">
				<span class="ui-filter--result-link" <? if ($arResult["URL"]) : ?>href="<?= $arResult["URL"] ?>"<? endif; ?>>Показать</span>
			</div>
		</div>
	<? endif; ?>
	<? //-- ?>
	
	<?
	//Js params
	$arResult["UNI_FILTER"]["JS_PARAMS"] = is_array($arParams["UI_UNI_FILTER_JS_PARAMS"]) ? $arParams["UI_UNI_FILTER_JS_PARAMS"] : [];
	$arResult["UNI_FILTER"]["JS_PARAMS"]["uiMainClass"] = $arParams["UI_MAIN_CLASS"];
	
	if ($arParams["INSTANT_RELOAD"] == "Y")
		$arResult["UNI_FILTER"]["JS_PARAMS"]["instantRefreshItems"] = true;
	
	if (! isset($arResult["UNI_FILTER"]["JS_PARAMS"]["animationFieldDuration"]))
		$arResult["UNI_FILTER"]["JS_PARAMS"]["animationFieldDuration"] = 380;
	
	if ($arParams["UI_FIELD_COLLAPSE_DISABLE" == "Y"])
		$arResult["UNI_FILTER"]["JS_PARAMS"]["collapseFieldsDisallow"] = true;
	
	if ($arResult["URL"])
		$arResult["UNI_FILTER"]["JS_PARAMS"]["resultUrl"] = $arResult["URL"];
	//--
	
	
	?>
	
	<script class="uni-ajax-script" type="text/javascript">
	<? if (! $_REQUEST["uni_ajax"]) : ?>
		$(document).ready(function() {
			$(".<?= $arParams["UI_MAIN_FILTER_CLASS"] ?>").uniFilter(<?= \Bitrix\Main\Web\Json::encode($arResult["UNI_FILTER"]["JS_PARAMS"]) ?>);
		});
	<? else : ?>
		<? if ($arResult["URL"]) : ?>$(".<?= $arParams["UI_MAIN_FILTER_CLASS"] ?>").uniFilter().setParam("resultUrl", "<?= htmlspecialchars($arResult["URL"]); ?>");<? endif; ?>
		$(".<?= $arParams["UI_MAIN_FILTER_CLASS"] ?>").uniFilter("init");
	<? endif; ?>
		</script>
</form>

<?
//////////// AJAX ////////////
if ($arParams["UNI_AJAX"] != "N")
{	
	//Ajax profiles
	$arAjaxProfile = $arParams["UI_AJAX_PROFILE_JS_PARAMS"] ? $arParams["UI_AJAX_PROFILE_JS_PARAMS"] : [];
	$arAjaxProfile["wrapTarget"] = "." . $arParams["UI_MAIN_FILTER_CLASS"];
	
	$arAjaxProfile["wrapTarget"] .= ", .ui-filter--tags-outer";
	$arAjaxProfile["formTarget"] = "." . $arParams["UI_MAIN_FILTER_CLASS"];
	if ($arResult["URL"])
		$arAjaxProfile["url"] = $arResult["URL"];
	$arAjaxProfile["cut"] = true;
	$arAjaxProfile["dataEx"] = "uni_ajax_profile=" . $arParams["UI_MAIN_CLASS"] . "--filter";

	$arAjaxProfile["commands"][] = ["target" => ".ui-filter--result", "cut" => true, "command" => "html"];
	$arAjaxProfile["commands"][] = ["target" => ".uni-ajax-script", "command" => "append", "cut" => true];
	
	$arResult["UNI_AJAX"]["PROFILES"]["FILTER-RESULT"] = $arAjaxProfile;
	
	$arAjaxProfile["commands"][] = ["target" => ".ui-filter--tags-wrap", "command" => "html", "cut" => true];
	if (! isset($arAjaxProfile["scrollTo"]))
		//$arAjaxProfile["scrollTo"]["target"] = "." . $arParams["UI_MAIN_FILTER_CLASS"];
		$arAjaxProfile["scrollTo"]["target"] = ".catalog-main-col";
	$arResult["UNI_AJAX"]["PROFILES"]["FILTER"] = $arAjaxProfile;
	
	
	if ($arParams["UI_LIST_WRAP_CLASS"])
	{
		$arAjaxAllProfile = $arAjaxProfile;
		$arAjaxAllProfile["wrapTarget"] .= ", ." . $arParams["UI_LIST_WRAP_CLASS"];
		$arAjaxAllProfile["commands"][] = ["target" => "." . $arParams["UI_LIST_WRAP_CLASS"], "cut" => true];
		$arResult["UNI_AJAX"]["PROFILES"]["FILTER-ALL"] = $arAjaxAllProfile;
	}
	
	//--
	?>
	
	<? if (TRUE) : ?>
	<script type="text/javascript">
		if (typeof(uniAjaxLib) != "undefined") {
			uniAjaxLib.addProfile("<?= $arParams["UI_MAIN_CLASS"] ?>--filter", <?= \Bitrix\Main\Web\Json::encode($arResult["UNI_AJAX"]["PROFILES"]["FILTER"]) ?>);
			uniAjaxLib.addProfile("<?= $arParams["UI_MAIN_CLASS"] ?>--filter-result", <?= \Bitrix\Main\Web\Json::encode($arResult["UNI_AJAX"]["PROFILES"]["FILTER-RESULT"]) ?>);
			<? if ($arResult["UNI_AJAX"]["PROFILES"]["FILTER-ALL"]) : ?>uniAjaxLib.addProfile("<?= $arParams["UI_MAIN_CLASS"] ?>--all", <?= \Bitrix\Main\Web\Json::encode($arResult["UNI_AJAX"]["PROFILES"]["FILTER-ALL"]) ?>);<? endif; ?>
		}
	</script>
	<? endif; ?><?
}
//////////// -- AJAX ////////////
?>
