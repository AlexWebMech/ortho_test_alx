<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
if($arResult['ITEMS'])
{
?>
    <h2>Также ищут:</h2>
<?
    $cnt_links = 10;
    $i = 0;
	foreach($arResult['ITEMS'] as $Item)
	{
        $class = ($i>=10)?'link_hide': '';
        if($Item['TITLE'] && $Item['URL']) {
            ?>
            <div class="sotbit-seometa-tags-wrapper <?=$class?>">
                <div class="sotbit-seometa-tag">
                    <a class="sotbit-seometa-tag-link" href="<?=$Item['URL'] ?>" title="<?=$Item['TITLE'] ?>"><?=$Item['TITLE'] ?></a>
                </div>
            </div>
		    <?
        }
        $i++;
	}

    if($i>=10){?>
        <div class="link_open_hide">Показать еще</div>
    <?}
}
?>
<script type="text/javascript">

jQuery(document).ready(function($) {  
    $(".link_hide").toggle();
    $(".link_open_hide").click(function(){   
      $(".link_hide").toggle();
      if($(this).html()=="Показать еще") {
        $(this).html("Скрыть");
      }else{
        $(this).html("Показать еще");
      }
    });

});


</script>