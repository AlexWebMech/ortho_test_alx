<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); $this->setFrameMode(true);

if(!preg_match("/(.*),(.*)/", $arParams["ICON_SIZE"])) $arParams["ICON_SIZE"] = '46,31';
if(!preg_match("/(.*),(.*)/", $arParams["ICON_OFFSET"])) $arParams["ICON_OFFSET"] = '-15,-30';

if($arParams["INCLUDE_JQUERY"])
$APPLICATION->AddHeadString('
<script type="text/javascript" src="/bitrix/components/ithive/offices.list/js/jquery.js"></script>
');
/* $APPLICATION->AddHeadString('
<script type="text/javascript" src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey='.$arParams["KEY"].'"></script>
<script type="text/javascript">
	jQuery().ready(function(){
		setTimeout(\'$("tr.result_item:nth-child(odd)").addClass("odd");$("tr.result_item").hover(function(){$(this).addClass("hover");},function(){$(this).removeClass("hover");});\',300);
		setTimeout(\'if(!$("#YMapsID").hasClass("YMaps")) $("<p></p>").appendTo("#YMapsID").addClass("dealer_invite").html("'.GetMessage("ITHIVE_OFFICES_NOTHINGS_FOUND").'").fadeIn(500);\',1000);
	});
</script>
'); */
$APPLICATION->AddHeadString('
<script type="text/javascript" src="https://api-maps.yandex.ru/1.1/index.xml?key='.$arParams["KEY"].'"></script>
<script type="text/javascript">
	jQuery().ready(function(){
		setTimeout(\'$("tr.result_item:nth-child(odd)").addClass("odd");$("tr.result_item").hover(function(){$(this).addClass("hover");},function(){$(this).removeClass("hover");});\',300);
		setTimeout(\'if(!$("#YMapsID").hasClass("YMaps")) $("<p></p>").appendTo("#YMapsID").addClass("dealer_invite").html("'.GetMessage("ITHIVE_OFFICES_NOTHINGS_FOUND").'").fadeIn(500);\',1000);
	});
</script>
');?>

<script type="text/javascript">
// Создание обработчика для события window.onLoad
YMaps.jQuery(function () {
	// Создание экземпляра карты и его привязка к созданному контейнеру
	var map = new YMaps.Map(YMaps.jQuery("#YMapsID")[0]);

	// Установка для карты ее центра и масштаба
//	map.setCenter(new YMaps.GeoPoint(43.97287940378828,56.323510212689236), 10); // почти вся Россия без дальнего востока
	var initZoom = <?if (intval($_GET['arrFilter_pf']['CITY'][0]))  echo '10'; else echo '3';?>
	<?
	intval($_GET['arrFilter_pf']['CITY'][0]) ? $shift = 0 : $shift = 47;
	?>

	map.setCenter(new YMaps.GeoPoint(<?
		GetLang()=='ru' ? $idx= 0 : $idx = 258; list($lon,$lat)=explode(",",$arResult["ITEMS"][$idx]["PROPERTIES"]["YMAP_POINT"]["VALUE"]); echo $lat+$shift.",".$lon
	?>), initZoom);

	var toolbar = new YMaps.ToolBar([new YMaps.ToolBar.MoveButton(), new YMaps.ToolBar.MagnifierButton(), new YMaps.ToolBar.RulerButton()]);

	if (YMaps.location) {	// Если местоположение пользователя удалось определить
		place = new YMaps.GeoPoint(YMaps.location.longitude, YMaps.location.latitude);

		// Создание кнопки показать все магазины
		var button_show_myplace = new YMaps.ToolBarButton({ 
			caption: "<?=GetMessage("ITHIVE_OFFICES_MY_LOCATION")?>", 
			hint: "<?=GetMessage("ITHIVE_OFFICES_SHOW_YOUR_LOCATION")?>"
		});
		
		var zoom = 10;

		// При щелчке на кнопке добавляется новая кнопка
		YMaps.Events.observe(button_show_myplace, button_show_myplace.Events.Click, function () {
			map.closeBalloon();
			if (YMaps.location.zoom) {
				zoom = YMaps.location.zoom;
			}
			map.setCenter(place, zoom);
			map.openBalloon(place, "<?=GetMessage("ITHIVE_OFFICES_YOUR_LOCATION")?>"
				+ (YMaps.location.country || "")
				+ (YMaps.location.region ? ", " + YMaps.location.region : "")
				+ (YMaps.location.city ? ", " + YMaps.location.city : "")
			)
		}, map);

		// Добавление кнопки на панель инструментов
		toolbar.add(button_show_myplace);
	}

	// Создание кнопки "Вся карта"
	var button_show_all = new YMaps.ToolBarButton({ 
		caption: "<?=GetMessage("ITHIVE_OFFICES_ENTIRE_MAP")?>", 
		hint: "<?=GetMessage("ITHIVE_OFFICES_SHOW_ALL_SHOPS")?>"
	});

	// При щелчке на кнопке устанавливается исходный центр и масштаб
	YMaps.Events.observe(button_show_all, button_show_all.Events.Click, function () {
		map.closeBalloon();
		map.setCenter(new YMaps.GeoPoint(90.984106746033,56.327765373358), 3);
	}, map);

	// Добавление кнопки "Вся карта" на панель инструментов
	toolbar.add(button_show_all);

	// Добавление панели инструментов на карту
	map.addControl(toolbar);

	// Создаем стиль
	var pin = new YMaps.Style();

	// Создаем стиль значка метки
	pin.iconStyle = new YMaps.IconStyle();
	pin.iconStyle.href = "<?=$arParams["ICON_FILE"]?>";

	pin.iconStyle.size = new YMaps.Point(<?=$arParams["ICON_SIZE"]?>);
	pin.iconStyle.offset = new YMaps.Point(<?=$arParams["ICON_OFFSET"]?>);

	// Создание группы объектов и добавление ее на карту
	var group = new YMaps.GeoObjectCollection(pin);
//	map.addControl(new YMaps.TypeControl());
	map.addControl(new YMaps.Zoom());	
	map.addControl(new YMaps.ScaleLine());
	map.enableScrollZoom();
	
<?foreach($arResult["ITEMS"] as $arItem):
if (GetLang() != 'ru')
	if ($arItem['ID'] != 56110) 
		continue;
//$row++;
$style = "default#redSmallPoint";
if ($arItem["PROPERTIES"]["SALE_TYPE"]["VALUE"] == "Опт") $style = "default#lightbluePoint";
?>	group.add(createPlacemark(new YMaps.GeoPoint(<?list($lon,$lat)=explode(",",$arItem["PROPERTIES"]["YMAP_POINT"]["VALUE"]); echo $lat.",".$lon?>), 
		"<?=$arItem["PROPERTIES"]["NAME_SHORT"]["VALUE"]?> <?=$arItem["PROPERTIES"]["YMAP_PLACE"]["VALUE"]?>", 
		"<h2><?=$arItem["NAME"]?></h2><?if($arItem["PREVIEW_PICTURE"]["ID"]):?><img class='map_shop_img' src='<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>' width='<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>' height='<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>' alt='<?=$arItem["NAME"]?>' title='<?=$arItem["NAME"]?>' /><?endif;?><div class='map_shop_info'><div class='map_shop_adress'><?=$arItem["PROPERTIES"]["ADDRESS"]["VALUE"]?></div><div class='map_shop_phone'><?=$arItem["PROPERTIES"]["PHONES"]["VALUE"]?></div><div class='map_shop_anounce'><?=str_replace("\r", "", str_replace("\n", "", $arItem["PREVIEW_TEXT"]));?></div><?/*if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?><div class='map_shop_detailUrl'><a href='<?=$arItem["DETAIL_PAGE_URL"]?>'><?=GetMessage("ITHIVE_OFFICES_DETAIL")?></a></div><?endif*/?><?if($arItem["PROPERTIES"]["WWW"]["VALUE"]):?><div class='map_shop_www'><a href='<?="http://".str_replace("http://", "", $arItem["PROPERTIES"]["WWW"]["VALUE"])?>' target='_blank'><?=$arItem["PROPERTIES"]["WWW"]["VALUE"]?></a></div><?endif;?></div>", 
		'<?=$arItem["PROPERTIES"]["ADDRESS"]["VALUE"]?>', 
		'<?=$arItem["PROPERTIES"]["PHONES"]["VALUE"]?>', 
		'<?=($arItem["PROPERTIES"]["WWW"]["VALUE"]) ? "http://".str_replace("http://", "", $arItem["PROPERTIES"]["WWW"]["VALUE"]) : ""?>', 
		'<?=$arItem["DETAIL_PAGE_URL"]?>',
		'<?=$arItem["ID"]?>',
		'<?//=$arItem["PROPERTIES"]["EMAIL"]["VALUE"]?>',
		'<?=$style?>'
	));
<?endforeach;?>

	map.addOverlay(group);

	// Создание управляющего элемента "Путеводитель по офисам"
	map.addControl(new OfficeNavigator(group));
});

// Функия создания метки
function createPlacemark (geoPoint, name, description, adress, phone, www, detailUrl, id, email, pmStyle) {
	//alert(style);
	var placemark = new YMaps.Placemark(geoPoint, {style: pmStyle});
	placemark.name = name;
	placemark.description = description;
	placemark.adress = adress;
	placemark.phone = phone;
	placemark.www = www;
	placemark.detailUrl = detailUrl;
	placemark.id = id;
	placemark.email = email;
	return placemark;
}

// Управляющий элемент "Путеводитель по офисам", реализиует интерфейс YMaps.IControl
function OfficeNavigator (offices) {
	// Добавление на карту
	this.onAddToMap = function (map, position) {
			this.container = YMaps.jQuery(
				'			<div class="scrollable"><table class="shop_search_result_table" id="shop_search_result_table"></table><table class="shop_search_result_table" id="shop_search_result_table_p"></table></div>');
			this.map = map;
			this.position = position || new YMaps.ControlPosition(YMaps.ControlPosition.TOP_RIGHT, new YMaps.Size(10, 10));

			// Выставление необходимых CSS-свойств
/*			this.container.css({
				position: "absolute",
				zIndex: YMaps.ZIndex.CONTROL,
				background: '#fff',
				listStyle: 'none',
				padding: '10px',
				margin: 0
			});*/
			
			// Формирование списка офисов
			this._generateList();
			
			// Применение позиции к управляющему элементу
			this.position.apply(this.container);
			
			// Добавление на карту
			this.container.appendTo("div.shop_search_result");
	}

	// Удаление с карты
	this.onRemoveFromMap = function () {
		this.container.remove();
		this.container = this.map = null;
	};

	// Пока "летим" игнорируем клики по ссылкам
	this.isFlying = 0;

	// Формирование списка офисов
	this._generateList = function () {
		var _this = this;

		// Для каждого объекта вызываем функцию-обработчик
		var countFirmShops = 0;
		var countPartner = 0;
		var cFirmShops = this.container.find("#shop_search_result_table");
		var cPartner = this.container.find("#shop_search_result_table_p");
		offices.forEach(function (obj) {
			var addrLower = obj.adress.toLowerCase();
			var isFirmShops = /* (addrLower.indexOf('юлианна') > -1) || */ (addrLower.indexOf('orthoboom') > -1);
			
			// Создание ссылки на объект
			var tr = YMaps.jQuery("<tr class='result_item'><td><?if(in_array("ADDRESS",$arParams["PROPERTY_CODE"])):?>" + obj.adress + "<?endif;if(in_array("PHONES",$arParams["PROPERTY_CODE"])):?><br />тел.: " + obj.phone + "<?endif;?></td></tr>");/* ,
				td = tr.find("td");  */
			
			// Создание обработчика щелчка по ссылке
			tr.bind("click", function () {
				if(!$(tr).hasClass("first"))
				{
					if (!_this.isFlying) {
						_this.isFlying = 1;
						_this.map.panTo(obj.getGeoPoint(), {
							flying: 0,
							callback: function () {
								if(_this.map.getZoom()<10) _this.map.setCenter(obj.getGeoPoint(),10);
								obj.openBalloon();
								_this.isFlying = 0;
								//document.getElementById('YMapsID').scrollTop();
								window.scrollTo(0, document.getElementById('YMapsID').getBoundingClientRect().top - document.body.getBoundingClientRect().top);
							}
						});
					}
					return false;
				}
			});

			// Слушатели событий на открытие и закрытие балуна у объекта
			YMaps.Events.observe(obj, obj.Events.BalloonOpen, function () {
					$(tr).addClass("selected");
					$("div.YMaps-b-balloon-content > b").remove();
			});
			
			YMaps.Events.observe(obj, obj.Events.BalloonClose, function () {
					$(tr).removeClass("selected");
			});

			// Добавление ссылки на объект в общий список
			if (isFirmShops) {
				countFirmShops++;
				tr.appendTo(cFirmShops);
			} else {
				countPartner++;
				tr.appendTo(cPartner);
			}
		});

		if (countFirmShops) YMaps.jQuery("<h4>Наши фирменные магазины:</h4>").prependTo(cFirmShops);
		if (countPartner) YMaps.jQuery("<h4>Магазины партнеров:</h4>").prependTo(cPartner);
	};
}
</script>
	<div class="shop_search_result">

	</div>
<div id="YMapsID"></div>
<div class="clear"></div>

