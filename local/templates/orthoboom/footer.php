<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
if ($_REQUEST["uni_ajax_main"])
	return;
?>
<?define(SITE_TEMPLATE_PATH2, "/local/templates/new_ortoboom");?>
	<? if (LOCAL_TEMPLATE_WIDE != "Y") : ?>
		</div>
	<? endif; ?>
		
</main>

<footer id="footer" class="footer">

	<div class="footer-logo">
		<img src="<?= SITE_TEMPLATE_PATH ?>/img/footer-logo.png" width="255" height="75" alt="Ortoboom" />
	</div>

	<div class="ui-container container">
	
		<div class="footer-info">
			<div class="footer-info-row">
				<div class="footer-soc-col">
					<div class="footer-soc-flex">
						<span class="footer-soc-icon ui-icon"></span>
						<div class="footer-soc-text">
								<?$APPLICATION->IncludeComponent(
									"bitrix:main.include",
									"",
									Array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_TEMPLATE_PATH . "/include/footer-soc.php",
										"EDIT_TEMPLATE" => "",
										"AREA_FILE_SUFFIX" => "",
										"COMPOSITE_FRAME_MODE" => "A",
										"COMPOSITE_FRAME_TYPE" => "AUTO",
									)
								);?>				
								
								<div>
								<img class="footer-soc-text-img" src="<?= SITE_TEMPLATE_PATH ?>/img/footer-soc-text.png" width="222" height="16" alt="" />
								</div>
						</div>
						<div class="footer-soc-links">
							<a class="footer-soc-icons ui-icon code-inst" rel="nofollow" href="//www.instagram.com/orthoboom/" target="_blank"></a>&nbsp;
							<a class="footer-soc-icons ui-icon code-vk" rel="nofollow" href="//vk.com/public73756667" target="_blank"></a>&nbsp;
							<a class="footer-soc-icons ui-icon code-facebook" rel="nofollow" href="//www.facebook.com/Orthoboom/" target="_blank"></a><br/>
							<a class="footer-soc-icons ui-icon code-youtube" rel="nofollow" href="//www.youtube.com/channel/UCaSgO1Z8pHcxHaqlvsJznYw" target="_blank"></a>&nbsp;
							<a class="footer-soc-icons ui-icon code-ok" rel="nofollow" href="//ok.ru/group/52352615120962" target="_blank"></a>&nbsp;
							<a class="footer-soc-icons ui-icon code-whatsapp" rel="nofollow" href="//wa.me/79101050145" target="_blank"></a>
						</div>
					</div>
				</div>
				
				
				<div class="footer-info-grow"></div>
				
				
				
				<div class="footer-subscribe-col">
					<div class="footer-subscribe-flex">
						<span class="footer-subscribe-icon ui-icon"></span>
							<div class="footer-subscribe-text">
									<?$APPLICATION->IncludeComponent(
										"bitrix:main.include",
										"",
										Array(
											"AREA_FILE_SHOW" => "file",
											"PATH" => SITE_TEMPLATE_PATH . "/include/footer-subscribe.php",
											"EDIT_TEMPLATE" => "",
											"AREA_FILE_SUFFIX" => "",
											"COMPOSITE_FRAME_MODE" => "A",
											"COMPOSITE_FRAME_TYPE" => "AUTO",
										)
									);?>
								
								<div class="footer-subscribe-text-2">
								
								<?/*
								<button sp-show-form="161976" class="ui-ubtton ui-button--normal">Подписаться</button>
								<script src="//web.webformscr.com/apps/fc3/build/loader.js" sp-form-id="afd6eeedd3d26685520ffdec9f2f2c3a0756626383f7d5600404a327d6c8c5f8"></script>
								*/?>
								
								<script src="//web.webformscr.com/apps/fc3/build/loader.js" sp-form-id="e4b0959e8ae4004c9e2b029b45e69b084ef08145661c999abb53b09922d90c4e"></script>

								
								</div>
								<?/*
								<input class="footer-subscribe-input" type="text" value="" placeholder="E-mail" />
								*/?>

							</div>
						
						
						
					</div>
				</div>
			</div>
		</div>	
		<div class="footer-main">
			<div class="footer-main-row">
		
				<div class="footer-col">
					<div class="footer-col-title">Компания</div>
						
					<div class="footer-col-menu">
									<? $APPLICATION->IncludeComponent(
										"bitrix:menu",
										"ui-menu",
										Array(
											"ROOT_MENU_TYPE" => "bottom1",
											"CHILD_MENU_TYPE" => "",
											"MAX_LEVEL" => "1",
											"USE_EXT" => "N",
											"ALLOW_MULTI_SELECT" => "N",
											"DELAY" => "N",
											"MENU_CACHE_GET_VARS" => array(""),
											"MENU_CACHE_TIME" => "36000",
											"MENU_CACHE_TYPE" => "A",
											"MENU_CACHE_USE_GROUPS" => "N",
										)
									); ?>	
					</div>
				
				
				
				</div>
		
				<div class="footer-col">
					<div class="footer-col-title">Информация</div>
					
					<div class="footer-col-menu">
					<? $APPLICATION->IncludeComponent(
						"bitrix:menu",
						"ui-menu",
						Array(
							"ROOT_MENU_TYPE" => "bottom2",
							"CHILD_MENU_TYPE" => "",
							"MAX_LEVEL" => "1",
							"USE_EXT" => "N",
							"ALLOW_MULTI_SELECT" => "N",
							"DELAY" => "N",
							"MENU_CACHE_GET_VARS" => array(""),
							"MENU_CACHE_TIME" => "36000",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_USE_GROUPS" => "N",
						)
					); ?>	
					</div>			
				
				
				</div>
			
				<div class="footer-main-col">
					<div class="footer-col-title">Помощь</div>
					
					<div class="footer-col-menu">
					<? $APPLICATION->IncludeComponent(
						"bitrix:menu",
						"ui-menu",
						Array(
							"ROOT_MENU_TYPE" => "bottom3",
							"CHILD_MENU_TYPE" => "",
							"MAX_LEVEL" => "1",
							"USE_EXT" => "N",
							"ALLOW_MULTI_SELECT" => "N",
							"DELAY" => "N",
							"MENU_CACHE_GET_VARS" => array(""),
							"MENU_CACHE_TIME" => "36000",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_USE_GROUPS" => "N",
						)
					); ?>	
					</div>			
				
				</div>	
				
				<div class="footer-main-col">
					<div class="footer-col-title">Способ оплаты</div>
					
					<div class="footer-payment-items">
						<span class="footer-payment-item ui-icon code-mastercard"></span>
						<span class="footer-payment-item ui-icon code-visa"></span>
						<span class="footer-payment-item ui-icon code-mir"></span>
					</div>
				</div>	
		
				<div class="footer-contacts-col footer-main-col">
					<div class="footer-col-title">Контакты</div>
					
								<?$APPLICATION->IncludeComponent(
									"bitrix:main.include",
									"",
									Array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_TEMPLATE_PATH . "/include/footer-contacts.php",
										"EDIT_TEMPLATE" => "",
										"AREA_FILE_SUFFIX" => "",
										"COMPOSITE_FRAME_MODE" => "A",
										"COMPOSITE_FRAME_TYPE" => "AUTO",
									)
								);?>	
				</div>	
				
				<div class="footer-main-col ui-flex-empty"></div>
				<div class="footer-main-col ui-flex-empty"></div>
				<div class="footer-main-col ui-flex-empty"></div>
				<div class="footer-main-col ui-flex-empty"></div>
		
		
			</div>
	
	
	
		</div>
	</div>
</footer>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


<? if (! $USER->IsAdmin()) : ?>
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function(){ document.jivositeloaded=0;var widget_id = 'LQtgM99joT';var d=document;var w=window;function l(){var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}//эта строка обычная для кода JivoSite
        function zy(){
            //удаляем EventListeners
            if(w.detachEvent){//поддержка IE8
                w.detachEvent('onscroll',zy);
                w.detachEvent('onmousemove',zy);
                w.detachEvent('ontouchmove',zy);
                w.detachEvent('onresize',zy);
            }else {
                w.removeEventListener("scroll", zy, false);
                w.removeEventListener("mousemove", zy, false);
                w.removeEventListener("touchmove", zy, false);
                w.removeEventListener("resize", zy, false);
            }
            //запускаем функцию загрузки JivoSite
            if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}
            //Устанавливаем куку по которой отличаем первый и второй хит
            var cookie_date = new Date ( );
            cookie_date.setTime ( cookie_date.getTime()+60*60*28*1000); //24 часа для Москвы
            d.cookie = "JivoSiteLoaded=1;path=/;expires=" + cookie_date.toGMTString();
        }
        if (d.cookie.search ( 'JivoSiteLoaded' )<0){//проверяем, первый ли это визит на наш сайт, если да, то назначаем EventListeners на события прокрутки, изменения размера окна браузера и скроллинга на ПК и мобильных устройствах, для отложенной загрузке JivoSite.
            if(w.attachEvent){// поддержка IE8
                w.attachEvent('onscroll',zy);
                w.attachEvent('onmousemove',zy);
                w.attachEvent('ontouchmove',zy);
                w.attachEvent('onresize',zy);
            }else {
                w.addEventListener("scroll", zy, {capture: false, passive: true});
                w.addEventListener("mousemove", zy, {capture: false, passive: true});
                w.addEventListener("touchmove", zy, {capture: false, passive: true});
                w.addEventListener("resize", zy, {capture: false, passive: true});
            }
        }else {zy();}
    })();</script>
<!-- {/literal} END JIVOSITE CODE -->
<? endif; ?>

	<?$lang = GetLang();?>
	<?if ($lang == 'ru'):?>
	<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">
	<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=8bbd85b7710aebe24686341b8cd2c1ee" charset="UTF-8" async></script>
	<?endif;?>

<script type="text/javascript">
    var ajaxDir = "<?=SITE_DIR?>ajax";
    var SITE_DIR = "<?=SITE_DIR?>";
    var SITE_ID  = "<?=SITE_ID?>";
    var TEMPLATE_PATH = "<?=SITE_TEMPLATE_PATH?>";
</script>

    <!--script(src="https://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.22&key=AIzaSyDXxt25L07gPgZKMHPZSK7mj0obyHqwPqc")-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="<?=SITE_TEMPLATE_PATH2?>/external/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH2?>/external/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH2?>/external/jquery.validate/jquery.validate.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH2?>/external/mobile-detect/mobile-detect.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH2?>/external/jquery.steps/jquery.steps.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH2?>/external/slick/slick.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH2?>/external/rateYo/rateYo.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH2?>/external/nouislider/nouislider.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH2?>/external/swipebox-master/jquery.swipebox.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH2?>/external/jquery.fullpage/jquery.fullpage.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH2?>/external/animate/wow.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH2?>/external/bootstrap-select/js/bootstrap-select.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH2?>/external/jquery-bar-rating/jquery.barrating.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH2?>/external/sourcebuster/dist/sourcebuster.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH2?>/js/fast-order.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH2?>/js/application.js"></script>
    <script src="/js/d-goals.js"></script>



	
	</div>

<? if (! $USER->IsAdmin()) : ?>
<script>
    (function(w, d, u, i, o, s, p) {
        if (d.getElementById(i)) { return; } w['MangoObject'] = o;
        w[o] = w[o] || function() { (w[o].q = w[o].q || []).push(arguments) }; w[o].u = u; w[o].t = 1 * new Date();
        s = d.createElement('script'); s.async = 1; s.id = i; s.src = u;
        p = d.getElementsByTagName('script')[0]; p.parentNode.insertBefore(s, p);
    }(window, document, '//widgets.mango-office.ru/widgets/mango.js', 'mango-js', 'mgo'));
    mgo({calltracking: {id: 22911, elements: [{"numberText":"78002346640"}]}});
</script>
<? endif; ?>

<div id="backtop"></div>

<?
if((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443){ //Получаем протокол сайта.
	$protocol = 'https://';
}else{
	$protocol = 'http://';
}
$title = $APPLICATION->GetPageProperty("title");
if($title == ''){
    $title = $APPLICATION->GetTitle();
}
if($title == ''){
    $title = $APPLICATION->GetDirProperty("title");
}

$description = $APPLICATION->GetPageProperty("description");
if($description == ''){
	$description = $APPLICATION->GetDirProperty("description");
}

$APPLICATION->AddHeadString('<meta property="og:title" content="'.$title.'"/>',true); //Тайтл сайта.
$APPLICATION->AddHeadString('<meta property="og:type" content="website"/>',true); //Указываем, что по ссылке передаётся страница сайта.
$APPLICATION->AddHeadString('<meta property="og:url" content="'.$protocol.$_SERVER["SERVER_NAME"].$APPLICATION->GetCurPage(false).'" />',true); //Ссылка на старницу.

if($description != ''){
	$APPLICATION->AddHeadString('<meta property="og:description" content="'.$description.'"/>',true); //Мета описание, если оно не пусто.
}
$APPLICATION->AddHeadString('<meta property="og:image" content="'.$protocol.'local/templates/orthoboom/img/header-logo.png"/>',true);

?>



<?
if($_GET['PAGEN_1']){
        $APPLICATION->SetPageProperty("title",$APPLICATION->GetPageProperty("title").'. Страница '.$_GET['PAGEN_1']);
        $APPLICATION->SetPageProperty("description",$APPLICATION->GetPageProperty("description").' Страница '.$_GET['PAGEN_1']);
    }
?>
	</body>
</html>