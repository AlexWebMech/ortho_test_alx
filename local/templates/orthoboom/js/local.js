$(document).ready(function() {
	$(".header-menu-icon").on("click", function () {
		if (! $(".header-mob-menu").outerHeight()) {
			var offset = $(".header-main-container").height() - 6;
			$(".header-mob-menu").css("height", "calc(100vh - " + offset + "px)");
		} else {
			$(".header-mob-menu").css("height", "0");
		}
	});
});

uniAjaxLib.addProfile("uni-main-cart", {
	url: "/",
	wrapElement: ".js-cart",
	cut: true,
});


localLocation = {};

localLocation.show = function ()
{
	if ($(".location-modal").length) {
		$(".location-modal").uniModal("show");
	} else {
		$.ajax({
			url: "/ajax/location.php?action=get_modal",
			success: function (html) {
				$("body").append(html);
				$(".location-modal").uniModal("show");
			}
		});
	}
}