var localCatalog = {};

localCatalog.addToCart = function (item, quantity)
{
	var itemsTarget = ".catalog-list--item";
	var offersInputTarget = "input[name=id]";
	
	if (parseInt(item))
		var itemId = parseInt(item);
	else if (item.jQuery) {
		var itemNode = item.parents(".catalog-list--item");
		if (itemNode.length)
			itemId = itemNode.eq(0).data("iblock-element-id");
	}
	
	var quantity = parseInt(quantity) || 1;
	
	var offerId = false;
	if (! offerId && ! itemNode)
		var itemNode = $(".catalog-list--item[data-iblock-element-id=" + itemId + "]");
	if (! offerId && itemNode.length) {
		var offerInputNode = itemNode.find("input[name=id]").filter("[type=radio], [type=checkbox]");
	
		if (offerInputNode.length) {
			offerId = offerInputNode.filter("[type=radio]:checked, [type=checkbox]:checked").prop("value");
			if (! offerId) {
				
				window.location.href = itemNode.find("a").eq(0).prop("href");
				return true;
				
				//return alert("Пожалуйста, выберите размер");
			}
		} else {
			offerId = itemId;
		}	
	} else if (! offerId) {
		var offerInputNode = itemNode.find("input[name=id]").filter("[value=" + itemId + "]");
		if (offerInputNode.length)
			offerId = itemId;
	}
	
	if (! offerId) {
		alert("Произошла ошибка при добавлении товара в корзину");
		return;
	}
	
	var bitrixCartUrl = "?action=BUY&id=" + offerId + "&ajax_basket=Y&quantity=" + quantity;
	
	uniAjaxLib.ajax({
		wrapTarget: "body",
		url: bitrixCartUrl,
		success: function () {
			//$(".catalog-list--sku-modal:visible").uniModal().hide();
			$(".catalog-list--buy-modal:visible").uniModal().show();				
		},
		ereror: function () {
			alert("Произошла ошибка при добавлении товара в корзину");
		}
	});
	
}



localCatalog.toggleFav = function (item) {
	var itemsTarget = ".catalog-list--item";
	
	if (parseInt(item))
		var itemId = parseInt(item);
	
	var itemNode = $("*[data-iblock-element-id=" + itemId + "]"); //var itemNode = $(this).parents(itemsTarget).eq(0);

	if (! itemNode)
		return;
	if (! itemNode.hasClass("fav-set")) {
		
		itemNode.addClass("fav-set");
		
		$.ajax({
			url: '/ajax/catalog_fav.php',
			data: 'COMMAND=ADD&ELEMENT_ID=' + itemId,
			success: function (ajaxResponse) {
				
			},
			error: function () {
				alert("Не удалось добавить товар в избранное");
			},
		});
		
	} else {
		
		itemNode.removeClass("fav-set");
		
		$.ajax({
			url: '/ajax/catalog_fav.php',
			data: 'COMMAND=DELETE&ELEMENT_ID=' + itemId,
			success: function () {		
				itemNode.removeClass("fav-set");
			},
			error: function () {
				alert("Не удалось убрать товар из избранного");
			}			
		});
	}
}
