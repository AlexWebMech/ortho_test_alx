<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$lang =  GetLang(); // defined in init.php
require_once('lang_' . $lang . '.php');

$curPage = $APPLICATION->GetCurPage(true);

/*	
// ????? ??? ????? ????
include_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/classes/general/captcha.php");
$cpt = new CCaptcha();
$captchaPass = COption::GetOptionString("main", "captcha_password", "");
if(strlen($captchaPass) <= 0) {
    $captchaPass = randString(10);
    COption::SetOptionString("main", "captcha_password", $captchaPass);
}
$cpt->SetCodeCrypt($captchaPass);
*/
define(SITE_TEMPLATE_PATH2, "/local/templates/new_ortoboom");
CJSCore::Init(array("fx"));
?><!DOCTYPE html>
<html>
<!--[if lte IE 9]> <html class="ie8"> <![endif]-->

	<head>
		<title><?$APPLICATION->ShowTitle();?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <script>
            // GTM
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PNKTQ7Z');
        </script>
        <!-- jquery-->
        <script async src="<?=SITE_TEMPLATE_PATH2?>/external/jquery/jquery.js"></script>
        <?$APPLICATION->AddHeadScript("//code.jquery.com/ui/1.11.4/jquery-ui.min.js")?>

		<!--<meta name="description" content="Интернет-магазин детской ортопедической обуви ORTHOBOOM с доставкой по всей России. Собственное производство и гарантия от производителя 45 дней." />-->
		<meta name="google-site-verification" content="Y5qrPJSwzEB1WN3uj5J1n9SuVegkF42IwSQC1xGrCZo" />
        <meta name="yandex-verification" content="a02db5d319195e2a" />
        <?$APPLICATION->ShowHead();?>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.jpg" />

		<!--[if lt IE 9]>
			<script async src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->


		<!-- fancybox -->
		<script defer src="<?=SITE_TEMPLATE_PATH?>/js/fancy/jquery.fancybox.js"></script>
		<script defer src="<?=SITE_TEMPLATE_PATH?>/js/fancy/helpers/jquery.fancybox-media.js"></script>
		<link href="<?=SITE_TEMPLATE_PATH?>/js/fancy/jquery.fancybox.css" rel="stylesheet" />
		<!-- fancybox -->


		<style type="text/css">	
			#owl-demo .item img{
				display: block;
				width: 100%;
				height: auto;
			}
		</style>

		<?
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.scrollbar.js");
		if (strpos($curPage, '/personal/') !== false)
			$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/script2.0.js");
		?>

		<?
        $APPLICATION->SetAdditionalCSS("/css/jquery.lightbox-0.5.css");
        $APPLICATION->SetAdditionalCSS("/css/cloud-zoom.css");
        $APPLICATION->SetAdditionalCSS("/css/owl.carousel.css");
        $APPLICATION->SetAdditionalCSS("/css/owl.theme.css");

		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery.lightbox-0.5.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery.scrollbar.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/styles.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/cabinet.css");

		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH2."/external/bootstrap/css/bootstrap.min.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH2 . "/external/rateYo/rateYo.min.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH2 . "/fonts/icomoon/style.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH2 . "/external/slick/slick.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH2 . "/external/jquery.fullpage/jquery.fullpage.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH2 . "/external/animate/animate.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH2 . "/external/nouislider/nouislider.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH2 . "/external/swipebox-master/swipebox.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH2 . "/external/bootstrap-select/css/bootstrap-select.min.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH2 . "/external/fontawesome/fontawesome.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH2 . "/external/jquery-bar-rating/dist/themes/fontawesome-stars-o.css");
        ?>

        <!-- styles-->
        <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH2 . "/css/style.css"); ?>
        <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH2 . "/css/custom.css"); ?>

        <!-- fonts-->
        <link rel="preconnect" href="//fonts.gstatic.com/" crossorigin>
        <link href="https://fonts.googleapis.com/css?family=Istok+Web:400,700&subset=cyrillic&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Pacifico&subset=cyrillic&display=swap" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700&subset=latin,cyrillic-ext,cyrillic&display=swap' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700&subset=latin,cyrillic-ext,cyrillic&display=swap' rel='stylesheet' type='text/css'>

        <!--[if IE]>
        <link rel="icon" href="<?=SITE_TEMPLATE_PATH2?>/images/favicon/favicon.ico">
        <![endif]-->
        <!-- favicon-->
        <link rel="icon" href="<?=SITE_TEMPLATE_PATH2?>/images/favicon/favicon.ico">

        <!--link(rel="icon", href="./images/favicon/favicon.png")-->
        <!--link(rel="apple-touch-icon-precomposed", href="./images/favicon/apple-touch-icon-precomposed.png")-->
        <!--link(rel="apple-touch-icon-precomposed", sizes="72x72",  href="./images/favicon/apple-touch-icon-72x72-precomposed.png")-->
        <!--link(rel="apple-touch-icon-precomposed", sizes="114x114",  href="./images/favicon/apple-touch-icon-114x114-precomposed.png")-->
        <!--link(rel="apple-touch-icon-precomposed", sizes="144x144",  href="./images/favicon/apple-touch-icon-144x144-precomposed.png")-->

        <!--[if lt IE 9]>
        <script async src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script>
        <script async src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>
        <![endif]-->

        <!-- jquery-->
       <!-- <script src="<?=SITE_TEMPLATE_PATH2?>/external/jquery/jquery.js"></script> -->
        <?/*<script src="https://www.google.com/recaptcha/api.js"></script>*/?>
</head>
	<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PNKTQ7Z" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->	
	<? SetCookie();  ?> 
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
    <?include("include/city.php");?>
    <div class="additional">
        <div class="container">
            <div class="row">
                <div class="col">

                    <div class="additional-block text-center">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" =>"/include/".$lang."/sale_text.php",
                            )
                        );?>
                        <span class="additional-close">&times;</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <header class="header" id="header">
        <div class="container">
            <div class="row">
                <div class="header-top flex-container-w100">
                    <a id="toggle-menu" href="#"><span></span></a>
                    <div class="header-top-item logo-wrapper header__logo-wrapper">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" =>"/include/".$lang."/logo.php",
                            )
                        );?>
                    </div>
                    <div class="header-top-item">
						<a class="offline-icon" href="/contact/">Фирменные магазины</a>
                    </div>
					<!--
                    <div class="header-top-item relative">
                        <a class="city-icon" href="#"><?/* =$City */?></a>
                        <div class="yourCity" id="yourCity">
                            <p class="yourCity-title">Ваш город:</p>
                            <p class="yourCity-text"><?/* =$City */?></p>
                            <div class="yourCity-btn-wrap">
                                <a class="mybtn transition yourCity-btn yes" href="?code=<?/* =$Code */?>">Да</a>
                                <button class="mybtn transition yourCity-btn change" data-target="#chooseCity"
                                        data-toggle="modal">
                                    Изменить
                                </button>
                            </div>
                        </div>
                        <div class="yourCity-add yourCityBlock"></div>
                    </div>
					-->
                    <div class="header-top-item relative header-login-items">
                        <?if (!$USER->IsAuthorized()):?>
                            <?php if(defined('REGISTRATION_SMS_ENABLE') && REGISTRATION_SMS_ENABLE) { ?>
                                <?php
                                $APPLICATION->IncludeComponent(
                                    "gorgoc:authorization.ozon", "loginlink", []
                                );
                                ?>
                                </span>
                            <?php }else { ?>
                                <span class="login-block login-icon">
                            <a id="authOpen" class="customodal-btn">Войти</a>
                            <i>/</i>
                            <a data-target="#register" data-toggle="modal" href="/register/">Регистрация</a>
                        </span>
                            <?php } ?>
                        <?else:?>
                            <span class="login-block">
                                <a class="header-login-items__personal-button" href="/personal/">Личный кабинет</a>
                                <i>/</i>
                                <a class="header-login-items__personal-button" href="<?echo $APPLICATION->GetCurPageParam("logout=yes", array(
                                    "login",
                                    "logout",
                                    "register",
                                    "forgot_password",
                                    "change_password"));?>">Выход</a>
                            </span>
                        <?endif;?>
                        <div class="yourCity" id="auth">
                            <?$APPLICATION->IncludeComponent("bitrix:system.auth.form","header_auth",Array(
                                    "REGISTER_URL" => "/personal/profile/index.php",
                                    "FORGOT_PASSWORD_URL" => "/personal/profile/?forgot_password=yes",
                                    "PROFILE_URL" => "/personal/index.php",
                                    "SHOW_ERRORS" => "Y"
                                )
                            );?>
                            <?/*<form id="authForm" action="/" method="POST">
                            <div class="auth-input-wrapper">
                                <input name="email" type="email" placeholder="Email" required>
                            </div>
                            <div class="auth-input-wrapper">
                                <input name="password" type="password" placeholder="Пароль" required>
                            </div>
                            <div class="auth-link-wrapper flex-container flex-side">
                                <a data-target="#register" data-toggle="modal">Регистрация</a>
                                <a id="forgotPass" href="#">Забыли пароль?</a>
                            </div>
                            <div class="auth-btn-wrap">
                                <input class="mybtn transition yourCity-btn" type="submit" name="submit" value="Войти">
                            </div>
                        </form>*/?>
                        </div>
                        <div class="yourCity-add auth"></div>
                    </div>
                    <a class="btn-favourite btn-initial" href="#"><span class="icon-favourite"></span></a>
                    <?$APPLICATION->IncludeComponent("at:sale.basket.basket.line", "basket",
                        Array(
                            "HIDE_ON_BASKET_PAGES" => "Y",	// Не показывать на страницах корзины и оформления заказа
                            "PATH_TO_BASKET" => SITE_DIR."personal/cart/",	// Страница корзины
                            "PATH_TO_ORDER" => SITE_DIR."personal/order/make/",	// Страница оформления заказа
                            "PATH_TO_PERSONAL" => SITE_DIR."personal/",	// Страница персонального раздела
                            "PATH_TO_PROFILE" => SITE_DIR."personal/",	// Страница профиля
                            "PATH_TO_REGISTER" => SITE_DIR."login/",	// Страница регистрации
                            "POSITION_FIXED" => "N",	// Отображать корзину поверх шаблона
                            "POSITION_HORIZONTAL" => "right",
                            "POSITION_VERTICAL" => "top",
                            "SHOW_AUTHOR" => "N",	// Добавить возможность авторизации
                            "SHOW_DELAY" => "N",	// Показывать отложенные товары
                            "SHOW_EMPTY_VALUES" => "N",	// Выводить нулевые значения в пустой корзине
                            "SHOW_IMAGE" => "Y",	// Выводить картинку товара
                            "SHOW_NOTAVAIL" => "N",	// Показывать товары, недоступные для покупки
                            "SHOW_NUM_PRODUCTS" => "Y",	// Показывать количество товаров
                            "SHOW_PERSONAL_LINK" => "N",	// Отображать персональный раздел
                            "SHOW_PRICE" => "Y",	// Выводить цену товара
                            "SHOW_PRODUCTS" => "Y",	// Показывать список товаров
                            "SHOW_SUBSCRIBE" => "N",	// Показывать товары, на которые подписан покупатель
                            "SHOW_SUMMARY" => "Y",	// Выводить подытог по строке
                            "SHOW_TOTAL_PRICE" => "Y",	// Показывать общую сумму по товарам
                        ),
                        false
                    );?>
                </div>

                <?$APPLICATION->IncludeComponent(
                    "bitrix:catalog.section.list",
                    "sub_menu",
                    Array(
                        "IBLOCK_TYPE" => "main",
                        "IBLOCK_ID" => "93",
                        "SECTION_ID" => "",
                        "COUNT_ELEMENTS" => "Y",
                        "TOP_DEPTH" => "2",
                        "SECTION_URL" => "/news/#SECTION_ID#/",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600",
                        "DISPLAY_PANEL" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "SECTION_USER_FIELDS" => ["UF_VID"],
                    )
                );?>
            </div>
        </div>
    </header>
    <main class="fixed-old">

    <div class="breadcrumbs">
        <div class="container">
            <div class="breadcrumbs-wrapper">
                <?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb", 
	"breadcrumbs", 
	array(
		"PATH" => "",
		"SITE_ID" => "s1",
		"START_FROM" => "0",
		"COMPONENT_TEMPLATE" => "breadcrumbs",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
            </div>
        </div>
    </div>