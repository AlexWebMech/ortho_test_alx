<?
define(YOUR_CITY, 'Ваш город');
define(SELECT_CITY, 'выберите');
define(CALL_ME, 'Перезвоните мне');
define(SITE_SEARCH, 'Поиск по сайту');
define(SEARCH_RESULT, 'Результаты поиска');

define(FORM_NAME_LABEL, 'Как вас зовут');
define(FORM_PHONE_LABEL, 'Контактный телефон');
define(FORM_EMAIL_LABEL, 'Email');
define(FORM_APTMNT_LABEL, 'Дата приема');
define(FORM_COMPANY_LABEL, 'Компания');
define(FORM_ARTICLE_LABEL, 'Артикуд');
define(FORM_COLOR_LABEL, 'Цвет');
define(FORM_AMOUNT_LABEL, 'Количество');
define(FORM_SIZE_LABEL, 'Размер');
define(FORM_DEST_LABEL, 'Адрес доставки');
define(FORM_MESSAGE_LABEL, 'Текст сообщения');
define(FORM_SEND_BUTTON, 'Отправить');
define(FORM_MESSAGE_OK, 'Сообщение отправлено!');
define(FORM_CATEGORY_SELECT_NAME, 'Выберите категорию');
define(FORM_GUARANTEE_TEXT, 'Гарантируем нераспространение введенных вами данных.');

define(FORM_ERROR_ALL_EMPTY, 'Ни одно поле не заполнено.');
define(FORM_ERROR_PHONE_EMPTY, 'Поле "Контактный телефон" должно быть заполнено.');
define(FORM_ERROR_WRONG_CAPTCHA, 'Введён неправильный защитный код.');

define(ITN_YOU_CAN, 'Можно');
define(ITN_ASK, 'Задать вопрос');
define(ITN_OR, 'или');
define(ITN_APPLY, 'Записаться на прием');

define(CATALOG_SMART_FILTER_DROPDOWN_SELECT, '--- Выбрать ---');
define(CATALOG_BACK_TO_CATALOG_LINK, 'Назад в каталог');
define(CATALOG_GO_CATALOG_LINK, 'Перейти в каталог');
define(CATALOG_ORDER_LINK, 'Заказать эту пару');
define(CATALOG_DESCR, 'Описание');

define(PAGENAV_CAPTION, 'Страницы:');
define(PAGENAV_PREV, 'Назад');
define(PAGENAV_NEXT, 'Вперед');
define(PAGENAV_ALL, 'Все');
define(PAGENAV_PAGINAL, 'Постранично');

define(FILTER_SELECTED, 'Выбрано');
?>