<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>
<div class="header-top-item header-top-item-cart">
    <a class="cart-icon" href="<?=$arParams["PATH_TO_BASKET"]?>">
        <?if (!$compositeStub)
        {
        if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y'))
        {
        echo '<span>' . $arResult['NUM_PRODUCTS'] . '</span>';
        } else {
        echo '<span>0</span>';
        }

        }?>
        шт.
    </a>
</div>
