<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CModule::IncludeModule("iblock");

$arProperty_LNS = array();
$rsProp = CIBlockProperty::GetList(array("sort" => "asc", "name" => "asc"), array("ACTIVE" => "Y", "IBLOCK_ID" => $arParams["IBLOCK_STR"]));
while ($arr = $rsProp->Fetch()) {
    $arProperty[$arr["CODE"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];
    if (in_array($arr["PROPERTY_TYPE"], array("L", "N", "S"))) {
        $arProperty_LNS["PROPERTY_" . $arr["CODE"] . "_VALUE"] = $arr["NAME"];
    }
}

foreach ($arResult["GRID"]["ROWS"] as $key => $arProduct) {

    $arXml = explode("#", $arProduct["data"]["PRODUCT_XML_ID"]);
    $arResult["GRID"]["ROWS"][$key]["data"]["DISPLAY_PROPERTIES"] = array();
    $arResult["GRID"]["ROWS"][$key]["data"]["DISPLAY_PHOTOS"] = array();
    $arResult["GRID"]["ROWS"][$key]["data"]["ARTICUL"] = "";

    $arSelect = $arParams["PROPERTY_CODE"];
    if (!empty($arParams["PROPERTY_ARTICUL"])) {
        $arSelect = array_merge($arSelect, array("PROPERTY_" . $arParams["PROPERTY_ARTICUL"]));
    }

    $arFilter = array("IBLOCK_ID" => $arParams["IBLOCK_STR"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "ID" => $arXml[0]);
    $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        foreach ($arFields as $k => $v) {
            if (!isset($arProperty_LNS[$k]))
                continue;

            if ($k == "PROPERTY_" . $arParams["PROPERTY_ARTICUL"] . "_VALUE") {
                $arResult["GRID"]["ROWS"][$key]["data"]["ARTICUL"] = $v;
                continue;
            }

            $arResult["GRID"]["ROWS"][$key]["data"]["DISPLAY_PROPERTIES"][$k] = array(
                "NAME" => $arProperty_LNS[$k],
                "VALUE" => $v
            );
        }
    }

    if (isset($arParams["PROPERTY_PHOTOS"]) && !empty($arParams["PROPERTY_PHOTOS"])) {
        $res = CIBlockElement::GetProperty(57, $arProduct["data"]["PRODUCT_ID"], "sort", "asc", array("CODE" => "MORE_PHOTO"));
        while ($ob = $res->GetNext()) {
            $resizedSmall = CFile::ResizeImageGet($ob['VALUE'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true);
            $resizedBig = CFile::ResizeImageGet($ob['VALUE'], array('width' => 990, 'height' => 990), BX_RESIZE_IMAGE_PROPORTIONAL, true);
            $arResult["GRID"]["ROWS"][$key]["data"]["DISPLAY_PHOTOS"][] = array(
                "BIG" => $resizedBig["src"],
                "SMALL" => $resizedSmall["src"]
            );
        }


    }

    $arResult["GRID"]["ROWS"][$key]["data"]["DETAIL_TEXT"] = "";
    $arResult["GRID"]["ROWS"][$key]["data"]["PREVIEW_TEXT"] = "";
    $arFilter = array("IBLOCK_ID" => 57, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "ID" => $arProduct["data"]["PRODUCT_ID"]);
    $res = CIBlockElement::GetList(array(), $arFilter, false, false, array("DETAIL_TEXT", "PREVIEW_TEXT"));
    if ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arResult["GRID"]["ROWS"][$key]["data"]["DETAIL_TEXT"] = $arFields["DETAIL_TEXT"];
        $arResult["GRID"]["ROWS"][$key]["data"]["PREVIEW_TEXT"] = $arFields["PREVIEW_TEXT"];
    }

    $arResult["GRID"]["ROWS"][$key]["data"]["SHOW_PROPERTIES"] = array();
    $res = CIBlockElement::GetProperty(57, $arProduct["data"]["PRODUCT_ID"], "sort", "asc", array("CODE" => "CML2_ATTRIBUTES"));
    while ($ob = $res->GetNext()) {
        $arResult["GRID"]["ROWS"][$key]["data"]["SHOW_PROPERTIES"][] = array(
            "NAME" => $ob["DESCRIPTION"],
            "VALUE" => $ob["VALUE"]
        );
    }
}


$arResult["ORGANIZATION_LIST"] = $arResult["ORGANIZATION_DATA"] = $arResult["ADDRESS_DATA"] = array();
if (getPersonType() == "U") {
    $arSelect = array("ID", "NAME");
    $arFilter = array("IBLOCK_ID" => 49, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "PROPERTY_USER" => $USER->GetID());
    $res = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, $arSelect);
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arResult["ORGANIZATION_LIST"][] = $arFields;
    }

    $curOrg = (isset($_REQUEST["ORG_ID"])) ? intval($_REQUEST["ORG_ID"]) : $arResult["ORGANIZATION_LIST"][0]["ID"];

    if ($curOrg != 0) {
        $arSelect = array(
            "ID",
            "NAME",
            "PROPERTY_LEGAL_ADDRESS",
            "PROPERTY_ACTUAL_ADDRESS",
            "PROPERTY_OGRN",
            "PROPERTY_INN",
            "PROPERTY_KPP",
            "PROPERTY_BANK_NAME",
            "PROPERTY_RASCH_SCHET",
            "PROPERTY_BANK_ADDRESS",
            "PROPERTY_BANK_BIK",
            "PROPERTY_KORR_SCHET",
            "PROPERTY_CITY"
        );
        $arFilter = array("IBLOCK_ID" => 49, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "ID" => $curOrg, "PROPERTY_USER" => $USER->GetID());
        $res = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arResult["ORGANIZATION_DATA"] = $arFields;
        }
    }

    // if (!empty($arResult["ORGANIZATION_DATA"]["PROPERTY_LEGAL_ADDRESS_VALUE"])) {
    // 	$arResult["ADDRESS_DATA"][] = $arResult["ORGANIZATION_DATA"]["PROPERTY_LEGAL_ADDRESS_VALUE"];
    // }

    // if (!empty($arResult["ORGANIZATION_DATA"]["PROPERTY_ACTUAL_ADDRESS_VALUE"]) && $arResult["ORGANIZATION_DATA"]["PROPERTY_ACTUAL_ADDRESS_VALUE"] != $arResult["ORGANIZATION_DATA"]["PROPERTY_LEGAL_ADDRESS_VALUE"]) {
    // 	$arResult["ADDRESS_DATA"][] = $arResult["ORGANIZATION_DATA"]["PROPERTY_ACTUAL_ADDRESS_VALUE"];
    // }

    $arFilter = array("IBLOCK_ID" => 49, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "PROPERTY_USER" => $USER->GetID());
    $res = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, array("PROPERTY_ACTUAL_ADDRESS"));
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arResult["ADDRESS_DATA"][] = $arFields["PROPERTY_ACTUAL_ADDRESS_VALUE"];
    }
}

?>