<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if ($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y") {
    if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y") {
        if (strlen($arResult["REDIRECT_URL"]) > 0) {
            $APPLICATION->RestartBuffer();
            ?>
            <script type="text/javascript">
                window.top.location.href = '<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
            </script>
            <?
            die();
        }

    }
}

$_SESSION['GA_ON'] = true; // метка в сессии нужна для дедубликации отправки заказа в google tag manager

$APPLICATION->SetAdditionalCSS($templateFolder . "/style_cart.css");
$APPLICATION->SetAdditionalCSS($templateFolder . "/style.css");
?>

<div class="cabinet">
    <div class="container">
        <div class="navibar"><a href="/catalog/"> </a></div>
        <h1 class="center"></h1>

        <a name="order_form"></a>

        <div id="order_form_div" class="order-checkout">
            <NOSCRIPT>
                <div class="errortext"><?= GetMessage("SOA_NO_JS") ?></div>
            </NOSCRIPT>

            <?
            if (!function_exists("getColumnName")) {
                function getColumnName($arHeader)
                {
                    return (strlen($arHeader["name"]) > 0) ? $arHeader["name"] : GetMessage("SALE_" . $arHeader["id"]);
                }
            }

            if (!function_exists("cmpBySort")) {
                function cmpBySort($array1, $array2)
                {
                    if (!isset($array1["SORT"]) || !isset($array2["SORT"]))
                        return -1;

                    if ($array1["SORT"] > $array2["SORT"])
                        return 1;

                    if ($array1["SORT"] < $array2["SORT"])
                        return -1;

                    if ($array1["SORT"] == $array2["SORT"])
                        return 0;
                }
            }
            ?>

            <div class="bx_order_make">
                <?
                if (!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N")
                {
                    if (!empty($arResult["ERROR"])) {
                        foreach ($arResult["ERROR"] as $v)
                            echo ShowError($v);
                    } elseif (!empty($arResult["OK_MESSAGE"])) {
                        foreach ($arResult["OK_MESSAGE"] as $v)
                            echo ShowNote($v);
                    }

                    include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/auth.php");
                }
                else
                {
                if (!empty($arResult["ERROR"])) {
                    foreach ($arResult["ERROR"] as $v)
                        echo ShowError($v);
                }
                if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
                {
                    if (strlen($arResult["REDIRECT_URL"]) == 0) {
                        include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/confirm.php");
                    }
                }
                else
                {
                ?>
                <script type="text/javascript">

                    <?if(CSaleLocation::isLocationProEnabled()):?>

                    <?
                    // spike: for children of cities we place this prompt
                    $city = \Bitrix\Sale\Location\TypeTable::getList(array('filter' => array('=CODE' => 'CITY'), 'select' => array('ID')))->fetch();
                    ?>

                    BX.saleOrderAjax.init(<?=CUtil::PhpToJSObject(array(
                        'source' => $this->__component->getPath() . '/get.php',
                        'cityTypeId' => intval($city['ID']),
                        'messages' => array(
                            'otherLocation' => '--- ' . GetMessage('SOA_OTHER_LOCATION'),
                            'moreInfoLocation' => '--- ' . GetMessage('SOA_NOT_SELECTED_ALT'), // spike: for children of cities we place this prompt
                            'notFoundPrompt' => '<div class="-bx-popup-special-prompt">' . GetMessage('SOA_LOCATION_NOT_FOUND') . '.<br />' . GetMessage('SOA_LOCATION_NOT_FOUND_PROMPT', array(
                                    '#ANCHOR#' => '<a href="javascript:void(0)" class="-bx-popup-set-mode-add-loc">',
                                    '#ANCHOR_END#' => '</a>'
                                )) . '</div>'
                        )
                    ))?>);

                    <?endif?>

                    var BXFormPosting = false;
                    var con_id = '';

                    function submitForm(val) {
                        if ($('div.agreement-block input[name="agreement"]').prop('checked') === false) {
                            $('#agreement-order-error').html('<p>Пожалуйста, примите соглашение на обработку персональных данных</p>');
                            return false;
                        } else
                            $('#agreement-order-error').html('');

                        if (BXFormPosting === true)
                            return true;

                        BXFormPosting = true;
                        if (val != 'Y')
                            BX('confirmorder').value = 'N';

                        var orderForm = BX('ORDER_FORM');
                        var email = $(orderForm).find('input[name="ORDER_PROP_4"]').val();
                        // BX.showWait();
                        con_id = showWait();

                        <?if(CSaleLocation::isLocationProEnabled()):?>
                        BX.saleOrderAjax.cleanUp();
                        <?endif?>
// console.log(orderForm);return true;
                        BX.ajax.submit(orderForm, ajaxResult);
                        if (email.length) {
                            (window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function () {
                                rrApi.setEmail(email);
                            });
                        }
                        // BX.closeWait();
                        return true;
                    }

                    function ajaxResult(res) {
                        var orderForm = BX('ORDER_FORM');
                        try {
                            // if json came, it obviously a successfull order submit

                            var json = JSON.parse(res);
                            BX.closeWait();

                            if (json.error) {
                                BXFormPosting = false;
                                return;
                            } else if (json.redirect) {
                                window.top.location.href = json.redirect;
                            }
                        } catch (e) {
                            // json parse failed, so it is a simple chunk of html

                            BXFormPosting = false;
                            BX('order_form_content').innerHTML = res;


                            <?if(CSaleLocation::isLocationProEnabled()):?>
                            BX.saleOrderAjax.initDeferredControl();
                            <?endif?>
                        }

                        // BX.closeWait();
                        closeWait(con_id);
                        BX.onCustomEvent(orderForm, 'onAjaxSuccess');

                        if ($(".lightbox").length) {
                            $(".lightbox").lightBox();
                        }
                        /*$('input:radio').customRadio();*/

                        $('#ORDER_PROP_3, #ORDER_PROP_30, #ORDER_PROP_13').mask('+7(999)9999999', {
                                placeholder: " ",
                                completed: function () {
                                    $(this).removeClass('error-field');
                                    $(this).next('.error-message').remove();
                                }
                            }
                        );

                        Recaptchafree.reset();

                        $("#total-place").text($("#total-place-copy").text());
                    }

                    function SetContact(profileId) {
                        BX("profile_change").value = "Y";
                        submitForm();
                    }
                </script>
                <? if ($_POST["is_ajax_post"] != "Y")
                {
                ?>
                <form action="<?= $APPLICATION->GetCurPage(); ?>" method="POST" name="ORDER_FORM" id="ORDER_FORM"
                      enctype="multipart/form-data">
                    <?= bitrix_sessid_post() ?>

                    <div id="order_form_content">
                        <?
                        }
                        else {
                            $APPLICATION->RestartBuffer();
                        }

                        //    ,

                        unset($_SESSION["ORDER_ID"]);
                        $_SESSION["ORDER_TOTAL_PRICE_FORMATED"] = $arResult["ORDER_TOTAL_PRICE_FORMATED"];
                        $_SESSION["BASKET_GRID"] = $arResult["GRID"];

                        // =============================================================================

                        if ($_REQUEST['PERMANENT_MODE_STEPS'] == 1) {
                            ?>
                            <input type="hidden" name="PERMANENT_MODE_STEPS" value="1"/>
                            <?
                        }

                        if (!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y") {
                            // foreach($arResult["ERROR"] as $v)
                            // 	echo ShowError($v);

                            $GLOBALS["ERR_ORDER"] = $arResult["ERROR"]; //  ,    PrintPropsForm()

                            ?>
                            <script type="text/javascript">
                                top.BX.scrollToNode(top.BX('ORDER_FORM'));
                            </script>
                            <?
                        }

                        include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/summary.php");
                        ?>


                        <div class="user-data">
                            <?
                            include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/person_type.php");
                            include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/props.php");
                            if ($arParams["DELIVERY_TO_PAYSYSTEM"] == "p2d") {
                                include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/paysystem.php");
                                include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/delivery.php");
                            } else {
                                //include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
                                include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/paysystem.php");
                            }

                            include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/related_props.php");


                            if (strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
                                echo $arResult["PREPAY_ADIT_FIELDS"];
                            ?>

                            <p><?= GetMessage("REQ_INFO"); ?></p>
                            <div class="blue-buttons">
                                <a href="/personal/order/make/print.php"
                                   target="_blank"><?= GetMessage("PRINT_INFO"); ?></a>
                                <? if (getPersonType() == "U"):?>
                                    <a href="/personal/order/make/excel.php"><?= GetMessage("EXPORT_EXCEL"); ?></a>
                                <?endif; ?>
                            </div>
                            <?
                            //$code = $APPLICATION->CaptchaGetCode();
                            ?>
                            <!--<div class="cart-captcha">
				<input type="hidden" name="captcha_sid" value="<?= $code ?>">
				<img src="/bitrix/tools/captcha.php?captcha_sid=<?= $code ?>" width="140" height="30" alt="CAPTCHA">

				<input type="text" name="captcha_word" size="30" maxlength="50" value="" >-->
                            <?/*if (count($GLOBALS["ERR_ORDER"]) == 1 && $GLOBALS["ERR_ORDER"][0] != ""):*/
                            ?>
                            <!--<p class="error-message error-message--captcha">Поставьте галочку</p>-->
                            <?/*endif;*/
                            ?>
                        </div>

                        <? if (getPersonType() == "U"):?>
                            <input type="hidden" name="ORG_ID"
                                   value="<?= (intval($arResult["ORGANIZATION_DATA"]["ID"])) ?>">
                        <?endif; ?>

                    </div><!-- .user-data -->

                    <? if ($_POST["is_ajax_post"] != "Y")
                    {
                    ?>
            </div>
        <input type="hidden" name="confirmorder" id="confirmorder" value="Y">
        <input type="hidden" name="profile_change" id="profile_change" value="N">
        <input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
        <input type="hidden" name="json" value="Y">
            <div class="order-bottom" style="height: 100px;">
                <div class="order-bottom__price">
                    : <span id="total-place"><?= $arResult["ORDER_TOTAL_PRICE_FORMATED"] ?></span>
                </div>
                <div>
                    <a href="javascript:void();" class="ok-button yes-button"
                       onclick="try{yaCounter25218083.reachGoal('ZAKAZ');} catch{} submitForm('Y'); return false;"
                       id="ORDER_CONFIRM_BUTTON"><?= GetMessage("SOA_TEMPL_BUTTON") ?></a>
                </div>
                <div id="agreement-order-block">
                    <? include $_SERVER["DOCUMENT_ROOT"] . "/include/agreement.php";?>
                </div>
                <div style="clear:both;"></div>
                <div style="font-size: 12px; text-align: center; margin-top: 20px;">
                    <a href="/consent-for-personal-data-processing" target="_blank"
                       style="font-weight: bold;text-decoration:underline !important;"> </a>
                </div>
                <div id="agreement-order-error"></div>
            </div>

            </form>
        <?
        if ($arParams["DELIVERY_NO_AJAX"] == "N")
        {
        ?>
            <div style="display:none;"><? $APPLICATION->IncludeComponent("bitrix:sale.ajax.delivery.calculator", "", array(), null, array('HIDE_ICONS' => 'Y')); ?></div>
        <?
        }
        }
        else
        {
        ?>
            <script type="text/javascript">
                top.BX('confirmorder').value = 'Y';
                top.BX('profile_change').value = 'N';
            </script>
        <?
        die();
        }
        }
        }
        ?>
        </div>
    </div>

    <? if (CSaleLocation::isLocationProEnabled()): ?>

        <div style="display: none">
            <?// we need to have all styles for sale.location.selector.steps, but RestartBuffer() cuts off document head with styles in it?>
            <?$APPLICATION->IncludeComponent(
            "bitrix:sale.location.selector.steps",
            ".default",
            array(
            ),
            false
            );?>
            <?$APPLICATION->IncludeComponent(
            "bitrix:sale.location.selector.search",
            ".default",
            array(
            ),
            false
            );?>
        </div>

    <? endif ?>

</div>
</div>

<div class="popup-change" id="popup-change-org">
    <a href="#" class="popup-change__close"></a>
    <h2></h2>

    <ul class="popup-change__list">
        <? foreach ($arResult["ORGANIZATION_LIST"] as $arOrg): ?>
            <li><a href="#"
                   onclick="BX.saleOrderAjax.changeOrganizationAjax(<?= $arOrg["ID"] ?>); return false;"><?= $arOrg["NAME"] ?></a>
            </li>
        <? endforeach; ?>
    </ul>
</div>
<div class="popup-change" id="popup-change-adr">
    <a href="#" class="popup-change__close"></a>
    <h2></h2>

    <ul class="popup-change__list">
        <? foreach ($arResult["ADDRESS_DATA"] as $address): ?>
            <li><a href="#"
                   onclick="BX.saleOrderAjax.changeAddressAjax('<?= $address ?>'); return false;"><?= $address ?></a>
            </li>
        <? endforeach; ?>
    </ul>
</div>
<script src="/js/inputmask.min.js"></script>
<script src="/local/templates/new_ortoboom/js/jquery.maskedinput.min.js"></script>
<script src="/js/jquery.lightbox-0.5.js"></script>
<script>
    $(function () {
        if ($(".lightbox").length) {
            $(".lightbox").lightBox();
        }
        //$('input:radio').customRadio();

        $('#ORDER_PROP_3, #ORDER_PROP_30, #ORDER_PROP_13').mask('+7(999)9999999', {
                placeholder: " ",
                completed: function () {
                    $(this).removeClass('error-field');
                    $(this).next('.error-message').remove();
                }
            }
        );

        $("body").on("click", ".popup-change__close", function (e) {
            e.preventDefault();

            var $wrap = $(this).parents(".popup-change");
            $wrap.fadeOut();
        });

        $("body").on("click", "[name=ORDER_PROP_8]", function () {
            var val = $(this).val();
            var $label = $("[data-property-id-row=9] label");
            $label.find("span").remove();
            if (val != "d1") {
                $label.append("<span>*</span>");
            }
        });
    });
</script>
<? if (getPersonType() == "U"): //   ,        ?>
    <script>
        $(function () {
            $("#PERSON_TYPE_2").click();
        });
    </script>
<? endif; ?>
<script>
    $(document).ready(function () {
        $('body').on('click', '.reload_captcha', function () {
            var $wrap = $(this).parents(".captcha-img");
            var $self = $(this);
            $.getJSON('<?=SITE_TEMPLATE_PATH?>/reload_captcha.php', function (data) {
                $wrap.find(".cap_pic").attr('src', '/bitrix/tools/captcha.php?captcha_sid=' + data);
                $wrap.find(".captcha_sid").val(data);
                $self.blur();
            });
            return false;
        });
    });
</script>