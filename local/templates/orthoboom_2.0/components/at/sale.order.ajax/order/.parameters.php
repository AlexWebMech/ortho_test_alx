<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule("iblock");

$arProperty_LNS = array();
if (isset($arCurrentValues["IBLOCK_STR"])) {
	$rsProp = CIBlockProperty::GetList(array("sort"=>"asc", "name"=>"asc"), array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arCurrentValues["IBLOCK_STR"]));
	while ($arr=$rsProp->Fetch())
	{
		$arProperty[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
		if (in_array($arr["PROPERTY_TYPE"], array("L", "N", "S")))
		{
			$arProperty_LNS["PROPERTY_" . $arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
		}
	}
}

$arTemplateParameters = array(
	"ALLOW_NEW_PROFILE" => array(
		"NAME"=>GetMessage("T_ALLOW_NEW_PROFILE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT"=>"Y",
		"PARENT" => "BASE",
	),
	"SHOW_PAYMENT_SERVICES_NAMES" => array(
		"NAME" => GetMessage("T_PAYMENT_SERVICES_NAMES"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" =>"Y",
		"PARENT" => "BASE",
	),
	"SHOW_STORES_IMAGES" => array(
		"NAME" => GetMessage("T_SHOW_STORES_IMAGES"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" =>"N",
		"PARENT" => "BASE",
	),
	"IBLOCK_STR" => array(
		"PARENT" => "DATA_SOURCE",
		"NAME" => GetMessage("IBLOCK_STR"),
		"TYPE" => "STRING",
		"DEFAULT" => "",
		"REFRESH" => "Y",
	),
	"PROPERTY_CODE" => array(
		"PARENT" => "DATA_SOURCE",
		"NAME" => GetMessage("PROPERTY_CODE"),
		"TYPE" => "LIST",
		"MULTIPLE" => "Y",
		"VALUES" => $arProperty_LNS,
		"ADDITIONAL_VALUES" => "Y",
	),
	"PROPERTY_PHOTOS" => array(
		"PARENT" => "DATA_SOURCE",
		"NAME" => GetMessage("PROPERTY_PHOTOS"),
		"TYPE" => "STRING",
		"DEFAULT" => "",
		"REFRESH" => "Y",
	),
	"PROPERTY_ARTICUL" => array(
		"PARENT" => "DATA_SOURCE",
		"NAME" => GetMessage("PROPERTY_ARTICUL"),
		"TYPE" => "STRING",
		"DEFAULT" => "",
		"REFRESH" => "Y",
	),
);