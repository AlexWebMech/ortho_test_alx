<?
define("STOP_STATISTICS", true);

include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");

function removeItemFromCart($id)
{
	if (preg_match('/^[0-9]+$/', $id) !== 1) {
		echo json_encode(array("error" => "Y"));
		return false;
	}

	$numProducts = CSaleBasket::GetList(
		array(),
		array("ID" => $id),
		array()
	);

	if ($numProducts > 0)
		CSaleBasket::Delete($id);

	echo json_encode(array("success" => "Y"));
}

function updateQuantityItem($id, $quantity)
{
	if (preg_match('/^[0-9]+$/', $id) !== 1) {
		echo json_encode(array("error" => "Y"));
		return false;
	}

	if (preg_match('/^[0-9]+$/', $quantity) !== 1) {
		echo json_encode(array("error" => "Y"));
		return false;
	}

	$numProducts = CSaleBasket::GetList(
		array(),
		array("ID" => $id),
		array()
	);

	if ($numProducts > 0) {
		$arFields = array(
		   "QUANTITY" => $quantity
		);
		CSaleBasket::Update($id, $arFields);
	}

	echo json_encode(array("success" => "Y"));
}

function clearBasket()
{
	CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
	echo json_encode(array("success" => "Y"));
}

$act = htmlspecialcharsbx($_REQUEST["act"]);

if ($act == "clearBasket") {
	clearBasket();
	die();
}

$item = intval($_REQUEST["item"]);

if ($item == 0) {
	echo json_encode(array("error" => "Y"));
	die();
}

if ($act == "remove")
	removeItemFromCart($item);

if ($act == "quantity") {
	$quantity = intval($_REQUEST["quantity"]);
	if ($quantity == 0) {
		echo json_encode(array("error" => "Y"));
		die();
	}

	updateQuantityItem($item, $quantity);
}

?>