<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>
    <div class="header-bottom flex-container-w100">
        <ul class="header__flex-menu flex-container-w100 menu header-menu">
        <?if($arResult["SECTIONS"]){?>
            <?$i=0;?>
            <?foreach( $arResult["SECTIONS"] as $arItems ){
                $this->AddEditAction($arItems['ID'], $arItems['EDIT_LINK'],
                    CIBlock::GetArrayByID($arItems["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItems['ID'], $arItems['DELETE_LINK'],
                    CIBlock::GetArrayByID($arItems["IBLOCK_ID"], "ELEMENT_DELETE"),
                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
                <li class="header__flex-item flex-container flex-center
                <?if ($arItems["SECTIONS"] || $arItems["ELEMENT"]):?>header__flex-item-has-children<?endif;?>">
                    <a class="transition header__flex-link <?=$arItems["XML_ID"]?>" href="<?=$arItems["CODE"]?>">
                        <?=$arItems["NAME"]?>
                    </a>
                    <?if ($arItems["SECTIONS"]):?>
                        <svg class="header-svg transition" xmlns="http://www.w3.org/2000/svg" width="12px"
                             height="18px">
                            <path fill-rule="evenodd"  stroke="rgb(89, 120, 141)" stroke-width="2px"
                                  stroke-linecap="butt" stroke-linejoin="miter" fill="none"
                                  d="M2.566,1.175 L9.625,8.226 L2.566,15.278 "/>
                        </svg>
                        <?include "sub1.php";?>
                    <?elseif ($arItems["ELEMENT"]):?>
                        <svg class="header-svg transition" xmlns="http://www.w3.org/2000/svg" width="12px"
                             height="18px">
                            <path fill-rule="evenodd"  stroke="rgb(89, 120, 141)" stroke-width="2px"
                                  stroke-linecap="butt" stroke-linejoin="miter" fill="none"
                                  d="M2.566,1.175 L9.625,8.226 L2.566,15.278 "/>
                        </svg>
                        <?include "sub2.php";?>
                    <?endif;?>
                </li>

            <?}?>
            <li class="header__flex-item flex-container flex-center only-mob">
                <a class="transition header__flex-link offline-icon" href="/contact">Оффлайн-сеть</a>
            </li>
            <li class="header__flex-item flex-container flex-center only-mob">
                <a class="transition header__flex-link" data-target="#register" data-toggle="modal">Войти</a>
            </li>
        <?}?>
    </ul>
</div>

