<?
$arSelect = Array("ID", "NAME", "CODE", "IBLOCK_SECTION_ID", "PREVIEW_PICTURE", "IBLOCK_ID", "PROPERTY_LIST");
$arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(array("sort"=>"asc"), $arFilter, false, false, $arSelect);
while($ob = $res->GetNext())
{
    $arFields = $ob;
    $arFields["PREVIEW_PICTURE"]  = CFile::GetPath($arFields["PREVIEW_PICTURE"]);
    $arElements[$arFields["IBLOCK_SECTION_ID"]][] = $arFields;
}

$arSections = array();
$arSectionsDepth3 = array();
foreach( $arResult["SECTIONS"] as $arItem ) {
    $arItem["ELEMENT"] = $arElements[$arItem["ID"]];
    if( $arItem["DEPTH_LEVEL"] == 1 ) { $arSections[$arItem["ID"]] = $arItem;}
    elseif( $arItem["DEPTH_LEVEL"] == 2 ) {$arSections[$arItem["IBLOCK_SECTION_ID"]]["SECTIONS"][$arItem["ID"]] =
        $arItem;}
    elseif( $arItem["DEPTH_LEVEL"] == 3 ) {$arSectionsDepth3[] = $arItem;}
}
if($arSectionsDepth3){
    foreach( $arSectionsDepth3 as $arItem) {
        foreach( $arSections as $key => $arSection) {
            if (is_array($arSection["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]) &&
                !empty($arSection["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]])) {
                $arSections[$key]["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["SECTIONS"][$arItem["ID"]] = $arItem;
            }
        }
    }
}
$arResult["SECTIONS"] = $arSections;