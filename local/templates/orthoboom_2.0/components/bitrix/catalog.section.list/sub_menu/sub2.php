<ul class="dropdown flex-container-w100 menu">
    <?if ($arItems["UF_VID"] == 1):?>
    <div class="dropdown-column flex-5">
    </div>
    <div class="dropdown-column flex-5">
    </div>
    <div class="dropdown-column flex-5">
    <?endif;?>
    <?foreach ($arItems["ELEMENT"] as $subItems):?>
        <li class="dropdown-item">
            <a class="dropdown-link" href="<?=$subItems["CODE"]?>">
                <?=$subItems["NAME"]?>
            </a>
        </li>
    <?endforeach;?>
    <?if ($arItems["UF_VID"] == 1):?>
    </div>
    <?endif;?>
</ul>
