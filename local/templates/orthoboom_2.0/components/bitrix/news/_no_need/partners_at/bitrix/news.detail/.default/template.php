<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$APPLICATION->SetPageProperty('title', $arResult["NAME"] . ' - партнер компании Orthoboom в городе ' . $arResult["PROPERTIES"]["CITY"]["VALUE"][0]);
?>
	<div class="container small-container">
         <h1 class="center"><?=$arResult["NAME"]?></h1>
         <div class="span4 logo_content">
		 <?
		 if($arResult["DETAIL_PICTURE"]["SRC"]){
			 $src = $arResult["DETAIL_PICTURE"]["SRC"];
		 }else{
			$src = SITE_TEMPLATE_PATH."/images/not_logo.png";
		 }
		 ?>
           <img alt="" src="<?=$src;?>" style="max-width: 267px;">
         </div>
         <div class="span8 contact"<?if($arResult['ID'] == 2179) echo 'style="width:280px;"';?>>
		 <?if($arResult["PROPERTIES"]["ADDRESS"]["VALUE"]){?>
			<p><span>Адрес: </span><?=$arResult["PROPERTIES"]["ADDRESS"]["VALUE"]?></p>
		 <?}?>
		 <?if($arResult["PROPERTIES"]["PHONE"]["VALUE"]){?>
           <p><span>Телефон: </span><?=$arResult["PROPERTIES"]["PHONE"]["VALUE"]?></p>
		 <?}?>
		 <?if($arResult["PROPERTIES"]["EMAIL"]["VALUE"]){?>
           <p><span>E-mail: </span><?=$arResult["PROPERTIES"]["EMAIL"]["VALUE"]?></p>
		 <?}?>
		 <?if($arResult["PROPERTIES"]["SCHEDULE"]["VALUE"]){?>
           <p><span>График работы: </span><?=$arResult["PROPERTIES"]["SCHEDULE"]["VALUE"]?></p>
		 <?}?>
         </div>
	  <?if($arResult['ID'] == 2179):?>
	  <div class="span4 logo_content" style="float:left;">
	  <?
		 if($arResult["PREVIEW_PICTURE"]["SRC"]){
			 $src = $arResult["PREVIEW_PICTURE"]["SRC"];
		 }else{
			$src = SITE_TEMPLATE_PATH."/images/not_logo.png";
		 }
		 ?>
			<img alt="" src="<?=$src;?>" style="max-width: 127px;">
	  </div>
	  <?endif;?>
	</div>
	<div style="clear: both;"></div>
	<div class="container small-container">
         <div class="company_info">
			<?if($arResult["PREVIEW_TEXT"]){?>
				<h2>О компании</h2>
				<p><?echo $arResult["PREVIEW_TEXT"];?></p>
			<?}?>
			<?if($arResult["DETAIL_TEXT"]){?>
				<h2>Сферы деятельности</h2>
				<p><?echo $arResult["DETAIL_TEXT"];?></p>
			<?}?>
			<?if(!empty($arResult["PROPERTIES"]["OTHER_ADDRESS"]["VALUE"])){?>
				<h2>Дополнительные адреса</h2>
				<?
				foreach($arResult["PROPERTIES"]["OTHER_ADDRESS"]["VALUE"] as $value){
					?>
					<p class="diop_address"><?=htmlspecialchars_decode($value["TEXT"])?></p>
					<?
				}
				?>
				<div class="clear:both;"></div>
			<?}?>
         </div>
	</div>