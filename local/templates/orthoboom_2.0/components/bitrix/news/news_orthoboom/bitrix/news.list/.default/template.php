<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="list">
<table class="our-partners-table">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
			<tr data-href="<?=$arItem["DETAIL_PAGE_URL"]?>" id="<?=$this->GetEditAreaId($arItem['ID'])?>">
				<td style="vertical-align:top;">
					<?if($arItem["PREVIEW_PICTURE"]["SRC"]){
						$src = $arItem["PREVIEW_PICTURE"]["SRC"];
					}else{
						$src = SITE_TEMPLATE_PATH."/images/not_logo.png";
					}?>
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$src?>" style="width: 200px; max-width: 200px; margin-right: 20px;"></a>
				</td>
				<td style="vertical-align:top; width:70%">
					<div class="h2"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
					<?if($arItem["PREVIEW_TEXT"]):?>
					<p><?=$arItem["PREVIEW_TEXT"]?></p>
					<?endif;?>
				</td>
				<td style="text-align:right; vertical-align:top;"><p><?=$arItem["DATE_ACTIVE_FROM"]?></p></td>
			</tr>
	<?endforeach;?>
</table>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>