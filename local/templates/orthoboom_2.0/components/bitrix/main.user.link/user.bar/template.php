<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(strlen($arResult["FatalError"])>0)
{
	?><span class='errortext'><?=$arResult["FatalError"]?></span><br /><br /><?
}
else
{?>
<div class="logout"><a href="/?logout=yes"><?=GetMessage("LOGOUT");?></a></div>					
<div class="login"><div class="cabinet-link"><a href="<?=(!empty($arParams["PROFILE_LINK"])) ? $arParams["PROFILE_LINK"] : '#';?>"><?=$arResult["User"]["NAME_FORMATTED"]?></a></div></div>
<?}
?>