<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"DISPLAY_DATE" => Array(
		"NAME" => GetMessage("PROFILE_LINK"),
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
);
?>
