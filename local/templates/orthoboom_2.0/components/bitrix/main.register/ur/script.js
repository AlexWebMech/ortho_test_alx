$(function(){
	$('input:checkbox').customCheckbox();

	$('body').on("change", '[name="REGISTER[EMAIL]"]', function(){
		$('[name="REGISTER[LOGIN]"]').val($(this).val());
	});

	$("body").on("click", "#check_1", function(){
		if ($(this).is(":checked")) {
			var legalAddress = $("[name=LEGAL_ADDRESS]").val();
			$("[name=ACTUAL_ADDRESS]").val(legalAddress);
		}
	});

	$("body").on("keyup", "[name=LEGAL_ADDRESS]", function(){
		if ($("#check_1").is(":checked")) {
			var legalAddress = $(this).val();
			$("[name=ACTUAL_ADDRESS]").val(legalAddress);
		}
	});

	$("body").on("keydown", ".int", function (e) {
	    // Allow: backspace, delete, tab, escape, enter
	    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
	         // Allow: Ctrl+A, Command+A
	        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
	         // Allow: home, end, left, right, down, up
	        (e.keyCode >= 35 && e.keyCode <= 40)) {
	             // let it happen, don't do anything
	             return;
	    }
	    // Ensure that it is a number and stop the keypress
	    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	        e.preventDefault();
	    }
	});

});