<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (count($arResult["ERRORS"]) > 0) {
	if (!isset($arResult["ERRORS"]["EMAIL"]) && !empty($arResult["VALUES"]["EMAIL"]) && !filter_var($arResult["VALUES"]["EMAIL"], FILTER_VALIDATE_EMAIL)) {
		$arResult["ERRORS"]["EMAIL"] = "E-mail указан неверно";
	}
	if (!isset($arResult["ERRORS"]["PASSWORD"]) && !empty($arResult["VALUES"]["PASSWORD"]) && strlen($arResult["VALUES"]["PASSWORD"]) < 6) {
		$arResult["ERRORS"]["PASSWORD"] = "Пароль не может быть меньше 6 символов";
	}	
	if (!isset($arResult["ERRORS"]["CONFIRM_PASSWORD"]) && !empty($arResult["VALUES"]["CONFIRM_PASSWORD"]) && strlen($arResult["VALUES"]["CONFIRM_PASSWORD"]) < 6) {
		$arResult["ERRORS"]["CONFIRM_PASSWORD"] = "Пароль не может быть меньше 6 символов";
	}
	if (isset($arResult["ERRORS"][0]) && $arResult["ERRORS"][0] == "Неверно введено слово с картинки") {
		$arResult["ERRORS"]["CAPTCHA"] = $arResult["ERRORS"][0];
	}

	if (filter_var($arResult["VALUES"]["EMAIL"], FILTER_VALIDATE_EMAIL)) {
		$filter = Array
		(
		    "EMAIL" => $arResult["VALUES"]["EMAIL"]
		);
		$rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $filter);
		if ($rsUsers->SelectedRowsCount() > 0) {
			$arResult["ERRORS"]["EMAIL"] = "Данный e-mail уже существует в системе";
		}
	}

	if ($arResult["VALUES"]["PERSONAL_PHONE"] != "") {
		$phoneClear = preg_replace('~\D+~', '', $arResult["VALUES"]["PERSONAL_PHONE"]);
		if (strlen($phoneClear) != 11) {
			$arResult["ERRORS"]["PERSONAL_PHONE"] = "Неверно введен телефон";
		}
	}
}



$arResult["PHONES"] = array();
if (isset($_REQUEST['PHONE'])) {
	foreach ($_REQUEST['PHONE'] as $phone) {
		if (empty($phone))
			continue;

		$arResult["PHONES"][] = htmlspecialcharsbx($phone);
	}
}

$arResult["EMAILS"] = array();

$arResult["F_CUSTOM"] = array(
	"ORGANIZATION" => htmlspecialcharsbx($_REQUEST["ORGANIZATION"]),
	"ACTUAL_ADDRESS" => htmlspecialcharsbx($_REQUEST["ACTUAL_ADDRESS"]),
	"LEGAL_ADDRESS" => htmlspecialcharsbx($_REQUEST["LEGAL_ADDRESS"]),
	"INN" => htmlspecialcharsbx($_REQUEST["INN"])
);

$legal_is_actual = htmlspecialcharsbx($_REQUEST['LEGAL_IS_ACTUAL']);
if ($legal_is_actual == "Y") {
	$arResult["F_CUSTOM"]["LEGAL_IS_ACTUAL"] = "Y";

	$arResult["F_CUSTOM"]["ACTUAL_ADDRESS"] = $arResult["F_CUSTOM"]["LEGAL_ADDRESS"];
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
	if ($arResult["F_CUSTOM"]["LEGAL_ADDRESS"] == "")
		$arResult["ERRORS"]["LEGAL_ADDRESS"] = "Обязательно для заполнения";

	if ($arResult["F_CUSTOM"]["ACTUAL_ADDRESS"] == "")
		$arResult["ERRORS"]["ACTUAL_ADDRESS"] = "Обязательно для заполнения";

	if ($arResult["F_CUSTOM"]["ORGANIZATION"] == "")
		$arResult["ERRORS"]["ORGANIZATION"] = "Обязательно для заполнения";

	if ($arResult["F_CUSTOM"]["INN"] == "")
		$arResult["ERRORS"]["INN"] = "Обязательно для заполнения";
	elseif (intval($arResult["F_CUSTOM"]["INN"]) == 0)
		$arResult["ERRORS"]["INN"] = "ИНН может содержать только числа";
}
?>