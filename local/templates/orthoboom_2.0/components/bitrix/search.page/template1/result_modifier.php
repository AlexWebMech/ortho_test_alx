<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//print_r($arResult["SEARCH"]);


/*
//echo getPersonType().'!<br>';
//echo (count($arResult["SEARCH"])).'<br>';

$arNew = array();
foreach($arResult["SEARCH"] as $arItem) { 
	$propFilter = array();
	$propFilter['ID'] = array(); 
	$select = array('ID', 'CATALOG_PRICE_53', 'CATALOG_PRICE_55', 'CATALOG_GROUP_53', 'CATALOG_GROUP_55');
	$arOfr = CCatalogSKU::getOffersList(array($arItem['ITEM_ID']), 0, array('ACTIVE' => 'Y'), $select, $propFilter);
	
	(getPersonType() == 'F' || getPersonType() == '') ? $priceType = 'CATALOG_PRICE_55' : $priceType = 'CATALOG_PRICE_53';
	$firstOfr = current($arOfr[$arItem['ITEM_ID']]);
	//print_r($firstOfr);
	if ($firstOfr[$priceType]) $arNew[] = $arItem;
}
*/

if (getPersonType() == 'F' || getPersonType() == '') {
	$arFilter = array('CODE' => 'TOP');  //
	$searchVal = 692915;
} else {
	$arFilter = array('CODE' => 'NE_POKAZYVAT_DLYA_FL_SAYT_ORTOBUM'); 
	$searchVal = 538150;
}

$arNew = array();
foreach($arResult['SEARCH'] as $arItem) { 
	$db_props = CIBlockElement::GetProperty(56, $arItem['ITEM_ID'], array("sort" => "asc"), $arFilter);
	if ($ar_props = $db_props->Fetch()) {
		if (intval($ar_props['VALUE']) == $searchVal)
			$arNew[] = $arItem;
	}
}

//echo count($arResult['SEARCH']) . ' - ' . count($arNew);

$arResult["SEARCH"] = $arNew;

$rs = new CDBResult;
$rs->InitFromArray($arResult["SEARCH"]);

$navComponentObject = null;
$arResult["NAV_STRING"] = $rs->GetPageNavStringEx($navComponentObject,  $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
$arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
$arResult["NAV_RESULT"] = $rs;
