<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
    return "";

$strReturn = '';

//we can't use $APPLICATION->SetAdditionalCSS() here because we are inside the buffered function GetNavChain()
$css = $APPLICATION->GetCSSArray();
if(!is_array($css) || !in_array("/bitrix/css/main/font-awesome.css", $css))
{
    $strReturn .= '<link href="'.CUtil::GetAdditionalFileURL("/bitrix/css/main/font-awesome.css").'" type="text/css" rel="stylesheet" />'."\n";
}

$strReturn .= '';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);

    $nextRef = ($index < $itemSize-2 && $arResult[$index+1]["LINK"] <> ""? '' : '');
    $child = ($index > 0? '' : '');
    $arrow = ($index > 0? '' : '');

    if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
    {
        //$classLast = ($index ==  $itemSize-2)? ' breadcrumbs__elem--last': '';
        $strReturn .= '<span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" 
		  class="breadcrumbs-link" style="color:#6b757f;">
		<a  itemprop="item" 		 
		  href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="url"
		  class="breadcrumbs-link" style="color:#6b757f;"
		  >' . $title .
            '<meta itemprop="name" content="'. $title .'" />
		 <meta itemprop="position" content="'. ($index+1) .'" />
		</a></span> / ';
    }
    else
    {
        $strReturn .= '<span
 			class="breadcrumbs-link current" 
 			itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">'
            . $title .
            '<meta itemprop="name" content="'. $title .'" />
			 <meta itemprop="position" content="'. ($index+1) .'" />
			</span>';
    }
}

$strReturn .= '';

return $strReturn;
