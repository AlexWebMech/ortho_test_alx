<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (isset($_REQUEST["BasketClear"]) && CModule::IncludeModule("sale")) {
   CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
   LocalRedirect($APPLICATION->GetCurPageParam("", array("BasketClear")));
   die();
}
?>