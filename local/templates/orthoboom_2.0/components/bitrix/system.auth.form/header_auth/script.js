$(document).on('ready', function() {
    var authForm = $('#authForm');
    authForm.on('submit', function() {
        var params = authForm.serializeArray();

        $.ajax({
            url:  '/ajax/login.php',
            data: params,
            type: 'POST',
            success: function(data){
                var message = "Ошибка авторизации";
                if(data) {
                    var responce = JSON.parse(data);
                    if(responce===true) {
                        window.location.href=window.location.href + "?login=yes";
                        return;
                    }
                    if(responce.MESSAGE) message = responce.MESSAGE;
                }
                authForm.find('.error').html('<p><font class="errortext">' + message + '<br></font></p>');
            },
            error: function() {
                var message = "Ошибка авторизации";
                authForm.find('.error').html('<p><font class="errortext">' + message + '<br></font></p>');
            }
        });
        return false;
    })
});