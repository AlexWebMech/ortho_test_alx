<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) == 0)
	return;

$sortBy = htmlspecialcharsbx($_REQUEST["by"]);
$sortOrder = htmlspecialcharsbx($_REQUEST["order"]);

if ($sortBy == "") {
	$sortOrderPrimary = "DESC";
	$sortOrderSecondary = "DESC";
} else if ($sortBy == "NAME") {
	if ($sortOrder == "DESC") {
		$sortOrderPrimary = "ASC";
	} else {
		$sortOrderPrimary = "DESC";
	}

	$sortOrderSecondary = "DESC";
} else if ($sortBy == "PROPERTY_FILE_SIZE") {
	if ($sortOrder == "DESC") {
		$sortOrderSecondary = "ASC";
	} else {
		$sortOrderSecondary = "DESC";
	}

	$sortOrderPrimary = "DESC";
}

if ($sortOrder == "")
	$sortOrder = "DESC";

?>

<div class="documents">
	<table>
		<thead>
			<tr>
				<td colspan="2">
					<?if ($sortBy == "NAME"):?>
						<strong<?=($sortOrderPrimary == "ASC") ? ' class="toper"' : '';?>>
					<?endif;?>

					<span><a href="<?=$APPLICATION->GetCurPageParam("by=NAME&order=" . $sortOrderPrimary, array("by", "order")); ?>">Документ</a>

					<?if ($sortBy == "NAME"):?>
						</strong>
					<?endif;?>
				</td>
				<td colspan="3">
					<?if ($sortBy == "PROPERTY_FILE_SIZE"):?>
						<strong<?=($sortOrderSecondary == "ASC") ? ' class="toper"' : '';?>>
					<?endif;?>

					<span>
						<a href="<?=$APPLICATION->GetCurPageParam("by=PROPERTY_FILE_SIZE&order=" . $sortOrderSecondary, array("by", "order")); ?>">Размер</a>
					</span>

					<?if ($sortBy == "PROPERTY_FILE_SIZE"):?>
						</strong>
					<?endif;?>
				</td>	
			</tr>
		</thead>
		<tbody>
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<tr id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<td><img src="<?=SITE_TEMPLATE_PATH?>/images/doc-down.png"></td>
					<td style="width: 48%;">
						<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
							<?echo $arItem["NAME"]?>
						<?endif;?>
					</td>
					<td>
						<?=$arItem["FILE_SIZE"]?>
					</td>	
					<td>
						<a href="<?=$arItem["FILE_PATH"]?>" class="download-link" target="_blank">Скачать</a>  
					</td>	
					<td>
						<a href="<?=$APPLICATION->GetCurPageParam("remove_file=" . $arItem["ID"], array("remove_file")); ?>" class="remove-link"><img src="<?=SITE_TEMPLATE_PATH?>/images/delete.png"></a>  
					</td>											
				</tr> 	
			<?endforeach;?>
		</tbody>	
	</table>
</div>



<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
