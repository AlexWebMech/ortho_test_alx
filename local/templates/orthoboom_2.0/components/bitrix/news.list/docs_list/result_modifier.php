<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

function formatBytes($size, $precision = 1)
{
    $base = log($size, 1024);
    $suffixes = array('', 'Кб', 'Мб', 'Гб', 'Тб');   

    $sizeNum = round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
    return str_replace(".", ",", $sizeNum);
}

foreach($arResult["ITEMS"] as $key => $arItem) {
	$arFile = CFile::GetFileArray($arItem["PROPERTIES"]["FILE"]["VALUE"]);
	$arResult["ITEMS"][$key]["FILE_SIZE"] = formatBytes($arFile["FILE_SIZE"]);
	$arResult["ITEMS"][$key]["FILE_PATH"] = $arFile["SRC"];
}
?>