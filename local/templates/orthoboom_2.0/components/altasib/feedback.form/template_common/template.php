<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); $this->setFrameMode(true);?>
<? $fVerComposite = (defined("SM_VERSION") && version_compare(SM_VERSION, "14.5.0") >= 0 ? true : false); ?>
<? if($fVerComposite) $this->setFrameMode(true); ?>
<?$ALX = "FID".$arParams["FORM_ID"];?>
<script type="text/javascript">
<?if($arParams["REWIND_FORM"] == "Y" && ((count($arResult["FORM_ERRORS"]) > 0) || ($_REQUEST["success_".$ALX] == "yes"))):?>
$(document).ready(function(){
        document.location.hash = "alx_position_feedback";
});
<?endif?>
if (typeof ALX_ReloadCaptcha != 'function')
{
  function ALX_ReloadCaptcha(csid, ALX)
  {
          document.getElementById("alx_cm_CAPTCHA_"+ALX).src = '/bitrix/tools/captcha.php?captcha_sid='+csid+'&rnd='+Math.random();
  }
  function ALX_SetNameQuestion(obj, ALX)
  {
          var qw = obj.selectedIndex;
          document.getElementById("type_question_name_"+ALX).value = obj.options[qw].text;
  }
}
</script>
<?
$lang = GetLang();
?>
<?foreach($arResult["FORM_ERRORS"] as $error):?>
        <?foreach($error as $k => $v):?>
                <?$errorField[] = $k;?>
        <?endforeach?>
<?endforeach?>
<?if($arParams["REWIND_FORM"] == "Y" && ((count($arResult["FORM_ERRORS"]) > 0) || ($_REQUEST["success_".$ALX] == "yes"))):?>
        <a name="alx_position_feedback"></a>
<?endif?>
<div class="alx_feed_back_form alx_feed_back_default" id="alx_feed_back_default_<?=$ALX?>">
<?if((count($arResult["FORM_ERRORS"]) == 0) && ($_REQUEST["success_".$ALX] == "yes")):?>
        <div class="alx_feed_back_form_error_block">
                <table cellpadding="0" cellspacing="0" border="0" class="alx_feed_back_form_error_block_tbl">
                <tr>
                        <td class="alx_feed_back_form_error_pic"><?=CFile::ShowImage($arParams["IMG_OK"])?></td>
                        <td class="alx_feed_back_form_mess_ok_td_list">
                                <div class="alx_feed_back_form_mess_ok"><?=$arParams["MESSAGE_OK"];?></div>
                        </td>
                </tr>
                </table>
        </div>
<?endif?>
<?if($arParams["CHECK_ERROR"] == "Y"):?>
<?if(count($arResult["FORM_ERRORS"]) > 0):?>
        <div class="alx_feed_back_form_error_block" style="margin-left:50px; margin-bottom: 20px;">
                        <table cellpadding="0" cellspacing="0" border="0" class="alx_feed_back_form_error_block_tbl">
                        <tr>
                                <td class="alx_feed_back_form_error_pic"><?=CFile::ShowImage($arParams["IMG_ERROR"])?></td>
                                <td class="alx_feed_back_form_error_td_list">
                                        <ul class="alx_feed_back_form_error_list">
                                                <?foreach($arResult["FORM_ERRORS"] as $error):?>
                                                        <?foreach($error as $v):?>
                                                                <li><span>-</span> <?=$v?></li>
                                                        <?endforeach?>
                                                <?endforeach?>
                                        </ul>
                                </td>
                        </tr>
                        </table>
        </div>
<?endif?>
<?endif?>
<?
$hide = false;
if($arParams["HIDE_FORM"] == "Y" && $_REQUEST["success_".$ALX] == "yes")
        $hide = true;

$actionPage = $APPLICATION->GetCurPage();
if(strpos($actionPage, "index.php") !== false)
        $actionPage = $APPLICATION->GetCurDir();
?>
<?if(!$hide):?>
<div class="alx_feed_back_form_feedback_poles">
<form id="f_feedbackf_<?=$ALX?>" name="f_feedback_<?=$ALX?>" action="<?=$actionPage?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="FEEDBACK_FORM_<?=$ALX?>" value="Y" />
        <?echo bitrix_sessid_post()?>
        <?if(count($arResult["TYPE_QUESTION"]) >= 1):?>
                        <div class="alx_feed_back_form_item_pole" style="margin-left:50px;">
                                <!--<div class="alx_feed_back_form_name"><?=$arParams["CATEGORY_SELECT_NAME"]?></div>-->
                                <!--<div class="alx_feed_back_form_inputtext_bg">-->

<? /* FID1 - форма "Оставить заявку на покупку обуви" на странице index.php */ ?>
<? /* FID2 - форма "Оставить предложение о сотрудничестве" на странице opt.php */ ?>
<? /* FID3 - форма "Задать вопрос" на странице orthoped.php и vopros.php */ ?>
<? /* FID4 - форма "Записаться на прием к ортопеду" на странице orthoped.php */ ?>
<? /* FID5 - форма "Заказать эту пару обуви" на странице detail.php */ ?>
<? /* в $arResult["TYPE_QUESTION"] - список категорий инфоблока "Вопрос-ответ" (ID=8), отсортированный по ID категории */ ?>
<? $sectID = $arResult["TYPE_QUESTION"][$arParams["FORM_ID"]-1]["ID"];?>
<? $sectName = $arResult["TYPE_QUESTION"][$arParams["FORM_ID"]-1]["NAME"];?>

<!--
<? switch ($ALX): ?>
<? case 'FID1': /* форма "Оставить заявку на покупку обуви" на странице index.php */ ?>
	<? $sectID = $arResult["TYPE_QUESTION"][0]["ID"];?>
	<? $sectName = $arResult["TYPE_QUESTION"][0]["NAME"];?>
	<? break;?>
<? case 'FID2': /* форма "Оставить предложение о сотрудничестве" на странице opt.php */ ?>
	<? $sectID = $arResult["TYPE_QUESTION"][1]["ID"];?>
	<? $sectName = $arResult["TYPE_QUESTION"][1]["NAME"];?>
	<? break;?>
<? case 'FID3': /* форма "Задать вопрос" на странице orthoped.php и vopros.php */ ?>
	<? $sectID = $arResult["TYPE_QUESTION"][2]["ID"];?>
	<? $sectName = $arResult["TYPE_QUESTION"][2]["NAME"];?>
	<? break;?>
<? case 'FID4': /* форма "Записаться на прием к ортопеду" на странице orthoped.php */ ?>
	<? $sectID = $arResult["TYPE_QUESTION"][3]["ID"];?>
	<? $sectName = $arResult["TYPE_QUESTION"][3]["NAME"];?>
	<? break;?>
<? case 'FID5': /* форма "Заказать эту пару обуви" на странице detail.php */ ?>
	<? $sectID = $arResult["TYPE_QUESTION"][4]["ID"];?>
	<? $sectName = $arResult["TYPE_QUESTION"][4]["NAME"];?>
	<? break;?>
<? endswitch ?>
-->

										<input type="hidden" id="type_question_<?=$ALX?>" name="type_question_<?=$ALX?>" value="<?=$sectID?>">
										<input type="hidden" id="type_question_name_<?=$ALX?>" name="type_question_name_<?=$ALX?>" value="<?=$sectName?>">
										<!--<input type="hidden" id="type_question_name_<?=$ALX?>" name="type_question_name_<?=$ALX?>" value="<?=$arResult["TYPE_QUESTION"][0]["NAME"]?>">-->

                                        <!--<select id="type_question_<?=$ALX?>" name="type_question_<?=$ALX?>" onchange="ALX_SetNameQuestion(this, '<?=$ALX?>');">
                                                <?foreach($arResult["TYPE_QUESTION"] as $arField):?>
                                                        <?if(trim(htmlspecialcharsEx($_POST["type_question"])) == $arField["ID"]):?>
                                                                <option value="<?=$arField["ID"]?>" selected><?=$arField["NAME"]?></option>
                                                        <?else:?>
                                                                <option value="<?=$arField["ID"]?>"><?=$arField["NAME"]?></option>
                                                        <?endif?>
                                                <?endforeach?>
                                        </select> -->
                                <!--</div>-->
                        </div>
        <?endif?>
        <?$k = 0;?>
        <?if(in_array("FEEDBACK_TEXT", $arParams["PROPERTY_FIELDS"])):?>
        <div class="alx_feed_back_form_item_pole callback-text">
                        <?if(in_array("FEEDBACK_TEXT_".$ALX, $errorField, true)):?><div class="alx_feed_back_form_item_pole alx_feed_back_form_error_pole"><?endif?>
                        <div class="alx_feed_back_form_name"><?=/*GetMessage("ALX_TP_MESSAGE_TEXTMESS")*/FORM_MESSAGE_LABEL?>: <?if(in_array("FEEDBACK_TEXT_".$ALX, $arParams["PROPERTY_FIELDS_REQUIRED"])):?><span class="alx_feed_back_form_required_text">*</span><?endif?></div>
                        <div class="alx_feed_back_form_inputtext_bg" id="error_EMPTY_TEXT<?=$ALX?>"><textarea cols="1" rows="1" id="EMPTY_TEXT1<?=$ALX;?>" name="FEEDBACK_TEXT_<?=$ALX?>"><?=$arResult["FEEDBACK_TEXT"]?></textarea></div>
                        <?if(in_array("FEEDBACK_TEXT_".$ALX, $errorField, true)):?></div><?endif?>
        </div>
        <?endif?>

        <?foreach($arResult["FIELDS"] as $arField): /*print_r($arField);*/?>
                        <div class="alx_feed_back_form_item_pole" style="margin-left:40px">
                                <div class="alx_feed_back_form_name">
                                        <?=$arField["NAME"]?> <?if($arField["REQUIRED"]):?><span class="alx_feed_back_form_required_text">*</span><?endif?>
                                        <div class="alx_feed_back_form_hint"><?=$arField["HINT"]?></div>
                                </div>
                        <?/*LIST*/?>
            <?if($arField["TYPE"] == "L"):?>
                                <?if($arField["LIST_TYPE"] == "L"):?>
                                        <div class="alx_feed_back_form_inputtext_bg">
                                        <?if($arField["MULTIPLE"] == "Y"):?>
                                                <select name="FIELDS[<?=$arField["CODE"]?>][]" multiple="multiple">
                                        <?else:?>
                                                <select name="FIELDS[<?=$arField["CODE"]?>]">
                                        <?endif;?>
                                        <?foreach($arField["ENUM"] as $v):?>
                                                <?if(!isset($_POST["FIELDS"][$arField["CODE"]]) && !isset($arResult["FORM_ERRORS"]["EMPTY_FIELD"][$arField["CODE"]])):?>
                                                        <option value="<?=$v["ID"]?>" <?if($v['DEF'] == 'Y') echo 'selected="selected"';?> ><?=$v["VALUE"]?></option>
                                                <?else:?>
                                                        <?if($arField["MULTIPLE"] == "Y"):?>
                                                                <option value="<?=$v["ID"]?>" <?if(in_array($v['ID'], $_POST["FIELDS"][$arField["CODE"]])) echo 'selected="selected"';?> ><?=$v["VALUE"]?></option>
                                                        <?else:?>
                                                                <option value="<?=$v["ID"]?>" <?if($v['ID'] == $_POST["FIELDS"][$arField["CODE"]]) echo 'selected="selected"';?> ><?=$v["VALUE"]?></option>
                                                        <?endif;?>
                                                <?endif;?>
                                        <?endforeach?>
                                                </select>
                                        </div>
                                <?elseif($arField["LIST_TYPE"] == "C"):?>
                                        <?if($arField["MULTIPLE"] == "Y"):?>
                                                <?foreach($arField["ENUM"] as $v):?>
                                                        <?if(!isset($_POST["FIELDS"][$arField["CODE"]]) && !isset($arResult["FORM_ERRORS"]["EMPTY_FIELD"][$arField["CODE"]])):?>
                                                                <input id="<?=$v["ID"]?>" type="checkbox" name="FIELDS[<?=$arField["CODE"]?>][]" value="<?=$v["ID"]?>" <?if($v["DEF"] == "Y") echo 'checked="checked"';?>><label for="<?=$v["ID"]?>"><?=$v["VALUE"]?></label><br />
                                                        <?else:?>
                                                                <input id="<?=$v["ID"]?>" type="checkbox" name="FIELDS[<?=$arField["CODE"]?>][]" value="<?=$v["ID"]?>" <?if(in_array($v['ID'], $_POST["FIELDS"][$arField["CODE"]])) echo 'checked="checked"';?>><label for="<?=$v["ID"]?>"><?=$v["VALUE"]?></label><br />
                                                        <?endif;?>
                                                <?endforeach?>
                                        <?else:?>
                                                <?foreach($arField["ENUM"] as $v):?>
                                                        <?if(!isset($_POST["FIELDS"][$arField["CODE"]]) && !isset($arResult["FORM_ERRORS"]["EMPTY_FIELD"][$arField["CODE"]])):?>
                                                                <input id="<?=$v["ID"]?>" type="radio" name="FIELDS[<?=$arField["CODE"]?>]" value="<?=$v["ID"]?>" <?if($v['DEF'] == 'Y') echo 'checked="checked"';?>><label for="<?=$v["ID"]?>"><?=$v["VALUE"]?></label><br />
                                                        <?else:?>
                                                                <input id="<?=$v["ID"]?>" type="radio" name="FIELDS[<?=$arField["CODE"]?>]" value="<?=$v["ID"]?>" <?if($v['ID'] == $_POST["FIELDS"][$arField["CODE"]]) echo 'checked="checked"';?>><label for="<?=$v["ID"]?>"><?=$v["VALUE"]?></label><br />
                                                        <?endif;?>
                                                <?endforeach?>
                                        <?endif?>
                                <?endif?>
                        <?/*HTML/TEXT*/?>
            <?elseif($arField["USER_TYPE"] == "HTML"):?>
                                <div class="alx_feed_back_form_inputtext_bg" id="error_<?=$arField["CODE"]?>">
                                        <?if(!empty($_POST["FIELDS"][$arField["CODE"]])):?>
                                                <textarea cols="1" rows="1" id="<?=$arField["CODE"]?>1" name="FIELDS[<?=$arField["CODE"]?>]" style="height:<?=$arField["USER_TYPE_SETTINGS"]["height"]?>px;"><?=trim(htmlspecialcharsEx($_POST["FIELDS"][$arField["CODE"]]))?></textarea>
                                        <?else:?>
                                                <textarea cols="1" rows="1" id="<?=$arField["CODE"]?>1" name="FIELDS[<?=$arField["CODE"]?>]" style="height:<?=$arField["USER_TYPE_SETTINGS"]["height"]?>px;" onblur="if(this.value==''){this.value='<?=$arField["DEFAULT_VALUE"]["TEXT"]?>'}" onclick="if(this.value=='<?=$arField["DEFAULT_VALUE"]["TEXT"]?>'){this.value=''}"><?=$arField["DEFAULT_VALUE"]["TEXT"]?></textarea>
                                        <?endif;?>
                                </div>
            <?/*DATE*/?>
            <?elseif($arField["USER_TYPE"] == "DateTime"):?>
                                <div class="alx_feed_back_form_inputtext_bg alx_feed_back_form_inputtext_bg_calendar" id="error_<?=$arField["CODE"]?>">
                                        <?if(!empty($_POST["FIELDS"][$arField["CODE"]])):?>
                                                <input type="text" size="40" id="<?=$arField["CODE"]?>1" name="FIELDS[<?=$arField["CODE"]?>]" value="<?=trim(htmlspecialcharsEx($_POST["FIELDS"][$arField["CODE"]]))?>" class="alx_feed_back_form_inputtext" readonly="readonly" onclick="BX.calendar({node:this, field:'FIELDS[<?=$arField["CODE"]?>]', form: '', bTime: false, currentTime: '<?=(time()+date("Z")+CTimeZone::GetOffset())?>', bHideTime: false});" />
                                        <?else:?>
                                                <input type="text" size="40" id="<?=$arField["CODE"]?>1" name="FIELDS[<?=$arField["CODE"]?>]" value="<?=$arField["DEFAULT_VALUE"]?>" class="alx_feed_back_form_inputtext" readonly="readonly" onclick="BX.calendar({node:this, field:'FIELDS[<?=$arField["CODE"]?>]', form: '', bTime: false, currentTime: '<?=(time()+date("Z")+CTimeZone::GetOffset())?>', bHideTime: false});" />
                                        <?endif;?>
                                        <div class="alx_feed_back_form_calendar_icon">
                                                <?
                                                require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/interface/admin_lib.php");
                                                define("ADMIN_THEME_ID", CAdminTheme::GetCurrentTheme());
                                                echo CAdminPage::ShowScript();
                                                echo Calendar("FIELDS[".$arField["CODE"]."]", "f_feedback_".$ALX);
                                                ?>
                                        </div>
                                </div>
                        <?/*STRING*/?>
            <?elseif($arField["TYPE"] != "F"):?>
                                <div class="alx_feed_back_form_inputtext_bg" id="error_<?=$arField["CODE"]?>">
                                        <?if(!empty($_POST["FIELDS"][$arField["CODE"]])):?>
                                                <input type="text" size="40" id="<?=$arField["CODE"]?>1" name="FIELDS[<?=$arField["CODE"]?>]" value="<?=trim(htmlspecialcharsEx($_POST["FIELDS"][$arField["CODE"]]))?>" class="alx_feed_back_form_inputtext" />
                                        <?else:?>
                                                <input type="text" size="40" id="<?=$arField["CODE"]?>1" name="FIELDS[<?=$arField["CODE"]?>]" value="<?=$arField["DEFAULT_VALUE"]?>" class="alx_feed_back_form_inputtext" onblur="if(this.value==''){this.value='<?=$arField["DEFAULT_VALUE"]?>'}" onclick="if(this.value=='<?=$arField["DEFAULT_VALUE"]?>'){this.value=''}" />
                                        <?endif;?>
                                </div>
                        <?/*FILE*/?>
            <?elseif($arField["TYPE"] == "F"):?>
                <input type="hidden" id="codeFileFields" name="codeFileFields[<?=$arField['CODE']?>]" value="<?=$arField['CODE']?>">
                                <div class="alx_feed_back_form_inputtext_bg_file">
                                        <input type="hidden" name="FIELDS[myFile][<?=$arField["CODE"]?>]">
                                        <input type="file" id="alx_feed_back_form_file_input_add<?=$k++?>" name="myFile[<?=$arField['CODE']?>]" class="alx_feed_back_form_file_input_add" size="<?=$arParams["WIDTH_FORM"]?>" />
                                </div>
            <?endif?>
                </div>
        <?endforeach?>

	<?if($arParams["USE_CAPTCHA"]):?>
		<?if($arParams["CAPTCHA_TYPE"] != 'recaptcha'):?>
			<!--<div class="alx_feed_back_form_item_pole">-->
<table style="width:95%;"><tr>
<td style="padding: 0 0 0 20px;"> 
				<div class="alx_feed_back_form_name">Код <span class="alx_feed_back_form_required_text">*</span></div>

				<? if($fVerComposite) $frame = $this->createFrame()->begin('loading... <img src="/bitrix/themes/.default/start_menu/main/loading.gif">');?>
				<?$capCode = $GLOBALS["APPLICATION"]->CaptchaGetCode();?>
				<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsEx($capCode)?>">
				<div><img id="alx_cm_CAPTCHA_<?=$ALX?>" src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsEx($capCode)?>" width="110" height="34"></div>
</td>
<td style="padding: 0 0 0 20px;"> 
				<div style="margin-bottom:6px;"><small><a href="#" onclick="capCode='<?=htmlspecialcharsEx($capCode)?>'; ALX_ReloadCaptcha(capCode, '<?=$ALX?>'); return false;">обновить</a></small></div>
				<? if($fVerComposite) $frame->end();?>

				<div class="alx_feed_back_form_inputtext_bg"><input type="text" class="alx_feed_back_form_inputtext" id="captcha_word-2" name="captcha_word" size="30" maxlength="50" value="" style="width:100px; margin-left:10px"></div>
</td></tr>
</table>
			<!--</div>-->
		<?else:?>
			<?if (isset($arResult["SITE_KEY"])):?>
				<div class="alx_feed_back_form_item_pole">
					<div class="alx_feed_back_form_name"><?=GetMessage("ALX_TP_MESSAGE_RECAPTCHA")?><span class="alx_feed_back_form_required_text">*</span></div>

			<? if($fVerComposite) $frame2 = $this->createFrame()->begin('loading... <img src="/bitrix/themes/.default/start_menu/main/loading.gif">');?>
					<script type="text/javascript">
					var AltasibFeedbackOnload = function() {
						grecaptcha.render('html_element_recaptcha', {'sitekey' : '<?=$arResult["SITE_KEY"];?>',
							'theme' : '<?=$arParams["RECAPTCHA_THEME"];?>', 'type' : '<?=$arParams["RECAPTCHA_TYPE"];?>' });
					};
					<?if($arParams['ALX_CHECK_NAME_LINK']=='Y'):?>
					$(window).load(function () {
						if(typeof AltasibFeedbackOnload != 'undefined')
							AltasibFeedbackOnload();
					});
					<?endif?>
					<?if($arParams['AJAX_MODE']=='Y'):?>
					var AltasibFeedbackOnAjaxSuccess = function(data, config) {
						if(typeof AltasibFeedbackOnload != 'undefined')
							AltasibFeedbackOnload();
						top.BX.removeCustomEvent(window, 'onAjaxSuccess', AltasibFeedbackOnAjaxSuccess);
					};
					top.BX.addCustomEvent(window, "onAjaxSuccess", AltasibFeedbackOnAjaxSuccess);
					<?endif?>
					</script>
					<div class="g-recaptcha" id="html_element_recaptcha" onload="AltasibFeedbackOnload()" data-sitekey="<?=$arResult["SITE_KEY"]?>"></div>

			<? if($fVerComposite) $frame2->end();?>

				</div>
			<?endif;?>
		<?endif;?>
	<?endif?>
        <div class="alx_feed_back_form_submit_block for-submit">
			<div style="clear:both;"></div>
			<?if ($lang == 'ru'):?>
			<div style="text-align:center !important;">
				Нажимая кнопку "Отправить", я подтверждаю своё согласие с <a href="/consent-for-personal-data-processing" target="_blank" style="font-weight: bold;text-decoration:underline !important;">соглашением об обработке персональных данных</a>
			</div>
			<?endif;?>
			<input type="submit" onclick="try{yaCounter25218083.reachGoal('VOPROS');} catch{} return true;" name="SEND_FORM_<?=$ALX?>" value="<?=/*GetMessage('ALX_TP_MESSAGE_SUBMIT')*/FORM_SEND_BUTTON?>" />
        </div>
</form>

</div>
<?endif?>
</div>

