<?$lang = GetLang();?>
<?require_once('lang_' . $lang . '.php');?>
<?define(SITE_TEMPLATE_PATH2, "/local/templates/new_ortoboom");?>
<!--<div class="container">
<div style="text-align: center;"><a href="#top">Вверх</a></div>
</div>-->
<?if ($curPage != '/index.php' && strpos($curPage, '/personal/') === false && strpos($curPage, '/catalog2/') === false):?>
</div> <!-- class="content" -->
	</div> <!-- class="container" -->
<?endif;?>

<?if ($curPage == '/orthoped/index.php'):?>
		<div class="webform">
			<div class="container">
				<div class="variant">
					<?=ITN_YOU_CAN?>  <span class="active" id="vopros"><?=ITN_ASK?></span> <?=ITN_OR?> <span class="active" id="zapis"><?=ITN_APPLY?></span>
				</div>
				<!--<div class="callback hide form1 arrow_box">-->
				<div class="callback hide form1">

						<div class="center h2">Записаться на прием к ортопеду</div>
<?$APPLICATION->IncludeComponent(
	"altasib:feedback.form", 
	"template_common", 
	array(
		"IBLOCK_TYPE" => "feedback_structured",
		"IBLOCK_ID" => "8",
		"FORM_ID" => "4",
		"EVENT_TYPE" => "ALX_FEEDBACK_FORM",
		"PROPERTY_FIELDS" => array(
			0 => "FIO",
			1 => "TEL",
			2 => "TIME",
			3 => "FEEDBACK_TEXT",
		),
		"PROPERTY_FIELDS_REQUIRED" => array(
		),
		"NAME_ELEMENT" => "ALX_DATE",
		"BBC_MAIL" => "kalishina@yandex.ru, ev@julianna.ru, feedback@julianna.ru",
		"MESSAGE_OK" => "Сообщение отправлено!",
		"CHECK_ERROR" => "Y",
		"ACTIVE_ELEMENT" => "Y",
		"USE_CAPTCHA" => "Y",
		"SEND_MAIL" => "N",
		"HIDE_FORM" => "Y",
		"USERMAIL_FROM" => "N",
		"SHOW_MESSAGE_LINK" => "Y",
		"REWIND_FORM" => "N",
		"WIDTH_FORM" => "50%",
		"SIZE_NAME" => "12px",
		"COLOR_NAME" => "#000000",
		"SIZE_HINT" => "10px",
		"COLOR_HINT" => "#000000",
		"SIZE_INPUT" => "12px",
		"COLOR_INPUT" => "#727272",
		"BACKCOLOR_ERROR" => "#ffffff",
		"COLOR_ERROR_TITLE" => "#A90000",
		"COLOR_ERROR" => "#8E8E8E",
		"IMG_ERROR" => "/upload/altasib.feedback.gif",
		"BORDER_RADIUS" => "3px",
		"COLOR_MESS_OK" => "#963258",
		"IMG_OK" => "/upload/altasib.feedback.ok.gif",
		"CATEGORY_SELECT_NAME" => "Выберите категорию",
		"SECTION_MAIL_ALL" => "",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"ALX_CHECK_NAME_LINK" => "N",
		"CAPTCHA_TYPE" => "default",
		"JQUERY_EN" => "Y",
		"SECTION_MAIL9" => "",
		"SECTION_MAIL11" => "",
		"SECTION_MAIL10" => "",
		"SECTION_MAIL7" => "",
		"SECTION_MAIL8" => ""
	),
	false
);?>

				</div>
				<!--<div class="callback hide form2 arrow_box">-->
				<div class="callback hide form2">
					
						<h2 class="center">Задать вопрос</h2>
<?$APPLICATION->IncludeComponent(
	"altasib:feedback.form", 
	"template_common", 
	array(
		"IBLOCK_TYPE" => "feedback_structured",
		"IBLOCK_ID" => "8",
		"FORM_ID" => "3",
		"EVENT_TYPE" => "ALX_FEEDBACK_FORM",
		"PROPERTY_FIELDS" => array(
			0 => "FIO",
			1 => "TEL",
			2 => "EMAIL",
			3 => "FEEDBACK_TEXT",
		),
		"PROPERTY_FIELDS_REQUIRED" => array(
		),
		"NAME_ELEMENT" => "ALX_DATE",
		"BBC_MAIL" => "kalishina@yandex.ru, ev@julianna.ru, feedback@julianna.ru",
		"MESSAGE_OK" => "Сообщение отправлено!",
		"CHECK_ERROR" => "Y",
		"ACTIVE_ELEMENT" => "Y",
		"USE_CAPTCHA" => "Y",
		"SEND_MAIL" => "N",
		"HIDE_FORM" => "Y",
		"USERMAIL_FROM" => "N",
		"SHOW_MESSAGE_LINK" => "Y",
		"REWIND_FORM" => "N",
		"WIDTH_FORM" => "50%",
		"SIZE_NAME" => "12px",
		"COLOR_NAME" => "#000000",
		"SIZE_HINT" => "10px",
		"COLOR_HINT" => "#000000",
		"SIZE_INPUT" => "12px",
		"COLOR_INPUT" => "#727272",
		"BACKCOLOR_ERROR" => "#ffffff",
		"COLOR_ERROR_TITLE" => "#A90000",
		"COLOR_ERROR" => "#8E8E8E",
		"IMG_ERROR" => "/upload/altasib.feedback.gif",
		"BORDER_RADIUS" => "3px",
		"COLOR_MESS_OK" => "#963258",
		"IMG_OK" => "/upload/altasib.feedback.ok.gif",
		"CATEGORY_SELECT_NAME" => "Выберите категорию",
		"SECTION_MAIL_ALL" => "",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"ALX_CHECK_NAME_LINK" => "N",
		"CAPTCHA_TYPE" => "default",
		"JQUERY_EN" => "Y",
		"SECTION_MAIL9" => "",
		"SECTION_MAIL11" => "",
		"SECTION_MAIL10" => "",
		"SECTION_MAIL7" => "",
		"SECTION_MAIL8" => ""
	),
	false
);?>

				</div>			
			</div>
		</div>
<?endif;?>

<?if ($curPage == '/oplata/index.php'):?>
		<div class="go-catalog">
			<div class="container">
				<a href="/catalog2/"><span class="go-to"><?=CATALOG_GO_CATALOG_LINK?></span></a>
			</div>
		</div>
<?endif;?>

<script>
// Кнопка ВВЕРХ
$(function(){
	$.fn.scrollToTop = function(){
		$(this).hide().removeAttr("href");
		if ($(window).scrollTop() >= "280") $(this).fadeIn("slow")
		var scrollDiv = $(this);
		$(window).scroll(function(){
			if ($(window).scrollTop() <= "280") $(scrollDiv).fadeOut("slow") /* 280 = эти значения лучше ставить одинаковыми, овтечают за появление иконки для прокрутки */
			else $(scrollDiv).fadeIn("slow")
		});
		$(this).click(function(){
			$("html, body").animate({scrollTop: 0}, 1000) /* 1000 = Задаём скорость прокрутки */
		})
	}
});
$(function(){
	$("#Go_Top").scrollToTop();
});
</script>

<script>
	$('.for-submit input').click(function(){
		$(this).parent().parent().find('p').hide();
	});
</script>

<a href='#' id='Go_Top'><img class="img-responsive" src="/images/up-arrow-icon-top.png" alt="Наверх" title="Наверх"></a>

<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>

<?if ($lang == 'ru'):?>
<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">
<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=8bbd85b7710aebe24686341b8cd2c1ee" charset="UTF-8" async></script>
<?endif;?>

</main>
    <footer class="footer" id="footer">
        <div class="container">
            <div class="row">
                <div class="footer-soc flex-container flex-center w-100">

                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" =>"/include/".$lang."/soc_items.php",
                        )
                    );?>
<!--                    <div class="footer-soc-item"><a class="transition ok" href="https://vk.com/club73756667" title="link" target="_blank">Odnoklassniki</a>-->
<!--                        <div class="footer-soc-item-add"></div>-->
<!--                    </div>-->
<!--                    <div class="footer-soc-item"><a class="transition fb" href="https://www.facebook.com/Orthoboom/" title="link" target="_blank">Facebook</a>-->
<!--                        <div class="footer-soc-item-add"></div>-->
<!--                    </div>-->
<!--                    <div class="footer-soc-item"><a class="transition inst" href="https://ok.ru/group/52352615120962" title="link" target="_blank">Instagramm</a>-->
<!--                        <div class="footer-soc-item-add"></div>-->
<!--                    </div>-->
<!--                    <div class="footer-soc-item"><a class="transition vk" href="https://www.instagram.com/orthoboom/" title="link" target="_blank">Vkontakte</a>-->
<!--                        <div class="footer-soc-item-add"></div>-->
<!--                    </div>-->
                </div>
            </div>

            <div class="row footer-menu">
                <div class="col-6 col-sm-6 col-md-3">
                    <div class="footer-menu-title">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" =>"/include/".$lang."/menu_catalog.php",
                            )
                        );?>
                    </div>
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", Array(
                        "ROOT_MENU_TYPE" => "bottoom_1_".$lang,	// Тип меню для первого уровня
                        "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                        "CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        "COMPONENT_TEMPLATE" => "template1",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO"
                    ),
                        false
                    );?>
                </div>
                <div class="col-6 col-sm-6 col-md-3">
                    <div class="footer-menu-title">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" =>"/include/".$lang."/menu_int_mag.php",
                            )
                        );?>
                    </div>
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", Array(
                        "ROOT_MENU_TYPE" => "bottoom_2_".$lang,	// Тип меню для первого уровня
                        "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                        "CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        "COMPONENT_TEMPLATE" => "template1",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO"
                    ),
                        false
                    );?>
                </div>
                <div class="col-6 col-sm-6 col-md-3">
                    <div class="footer-menu-title">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" =>"/include/".$lang."/menu_vug_poc.php",
                            )
                        );?>
                    </div>
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", Array(
                        "ROOT_MENU_TYPE" => "bottoom_3_".$lang,	// Тип меню для первого уровня
                        "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                        "CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        "COMPONENT_TEMPLATE" => "template1",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO"
                    ),
                        false
                    );?>
                </div>
                <div class="col-6 col-sm-6 col-md-3">
                    <div class="footer-menu-title">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" =>"/include/".$lang."/bottom_comp.php",
                            )
                        );?>
                    </div>
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", Array(
                        "ROOT_MENU_TYPE" => "bottoom_4_".$lang,	// Тип меню для первого уровня
                        "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                        "CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        "COMPONENT_TEMPLATE" => "template1",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO"
                    ),
                        false
                    );?>
                </div>
            </div>
            <div class="row">
                <div class="footer-rainbow flex-container">
                    <div class="footer-rainbow-item red"></div>
                    <div class="footer-rainbow-item orange"></div>
                    <div class="footer-rainbow-item yellow"></div>
                    <div class="footer-rainbow-item green1" style="background-color: #94d82d"></div>
                    <div class="footer-rainbow-item lightblue"></div>
                    <div class="footer-rainbow-item blue"></div>
                    <div class="footer-rainbow-item violet"></div>
                </div>
            </div>
            <div class="row">
                <div class="footer-contacts flex-center flex-container-w100">
                    <div class="footer-contacts-item flex-center">
                        <div class="footer-contacts-item-wrapper phone"></div>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" =>"/include/".$lang."/footer_phon.php",
                            )
                        );?>
                    </div>
                    <div class="footer-contacts-item flex-center">
                        <div class="footer-contacts-item-wrapper mail"></div>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" =>"/include/".$lang."/footer_email.php",
                            )
                        );?>
                    </div>
                    <div class="footer-contacts-item flex-center">
                        <div class="footer-contacts-item-wrapper shedule"></div>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" =>"/include/".$lang."/footer_time.php",
                            )
                        );?>
                    </div>
                </div>
            </div>
        </div>

        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="copyright-flex flex-side">
                        <div class="copyright-text basic-text">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" =>"/include/".$lang."/footer_text.php",
                                )
                            );?>
                        </div>
                        <div class="copyright-text basic-text">Created by <a class="copyright-link" title="LENAL" href="lenal.eu">LENAL</a></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="modal fade" id="chooseCity" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title h5">Выберите город</div>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 city-column">
                            <?$arCities = GetCities();?>
                            <?foreach ($arCities as $arCity):?>
                                    <a href="#" data-id="<?=$arCity['ID']?>" data-name="<?=$arCity['NAME']?>" data-id="<?=$arCity['ID']?>" data-code="<?=$arCity['CODE']?>" class="modal-city-link"><?=$arCity['NAME']?></a>
                            <?endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title h5">Регистрация</div>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times</span></button>
                </div>
                <?
                $APPLICATION->IncludeComponent("bitrix:system.auth.registration", ".default",
                    array(
                    ),
                    false
                );
                ?>
            </div>
        </div>
    </div>

    <!-- BEGIN JIVOSITE CODE {literal} -->
    <script type='text/javascript'>
        (function(){ document.jivositeloaded=0;var widget_id = 'LQtgM99joT';var d=document;var w=window;function l(){var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}//эта строка обычная для кода JivoSite
            function zy(){
                //удаляем EventListeners
                if(w.detachEvent){//поддержка IE8
                    w.detachEvent('onscroll',zy);
                    w.detachEvent('onmousemove',zy);
                    w.detachEvent('ontouchmove',zy);
                    w.detachEvent('onresize',zy);
                }else {
                    w.removeEventListener("scroll", zy, false);
                    w.removeEventListener("mousemove", zy, false);
                    w.removeEventListener("touchmove", zy, false);
                    w.removeEventListener("resize", zy, false);
                }
                //запускаем функцию загрузки JivoSite
                if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}
                //Устанавливаем куку по которой отличаем первый и второй хит
                var cookie_date = new Date ( );
                cookie_date.setTime ( cookie_date.getTime()+60*60*28*1000); //24 часа для Москвы
                d.cookie = "JivoSiteLoaded=1;path=/;expires=" + cookie_date.toGMTString();
            }
            if (d.cookie.search ( 'JivoSiteLoaded' )<0){//проверяем, первый ли это визит на наш сайт, если да, то назначаем EventListeners на события прокрутки, изменения размера окна браузера и скроллинга на ПК и мобильных устройствах, для отложенной загрузке JivoSite.
                if(w.attachEvent){// поддержка IE8
                    w.attachEvent('onscroll',zy);
                    w.attachEvent('onmousemove',zy);
                    w.attachEvent('ontouchmove',zy);
                    w.attachEvent('onresize',zy);
                }else {
                    w.addEventListener("scroll", zy, {capture: false, passive: true});
                    w.addEventListener("mousemove", zy, {capture: false, passive: true});
                    w.addEventListener("touchmove", zy, {capture: false, passive: true});
                    w.addEventListener("resize", zy, {capture: false, passive: true});
                }
            }else {zy();}
        })();</script>
    <!-- {/literal} END JIVOSITE CODE -->

    <!--script(src="https://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.22&key=AIzaSyDXxt25L07gPgZKMHPZSK7mj0obyHqwPqc")-->
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script defer src="/js/cloud-zoom.1.0.2.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH?>/js/jquery.matchHeight-min.js"></script>
    <script defer src="/js/owl.carousel.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/js/jquery.maskedinput.min.js"></script>
    <script defer src="/js/jquery.lightbox-0.5.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/external/bootstrap/js/bootstrap.min.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/external/jquery-ui/jquery-ui.min.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/external/jquery.validate/jquery.validate.min.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/external/mobile-detect/mobile-detect.min.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/external/jquery.steps/jquery.steps.min.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/external/slick/slick.min.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/external/rateYo/rateYo.min.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/external/nouislider/nouislider.min.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/external/swipebox-master/jquery.swipebox.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/external/jquery.fullpage/jquery.fullpage.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/external/animate/wow.min.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/external/bootstrap-select/js/bootstrap-select.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/external/jquery-bar-rating/jquery.barrating.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/external/sourcebuster/dist/sourcebuster.min.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/external/saveforms/ntsaveforms.js"></script>
    <script defer src="<?=SITE_TEMPLATE_PATH2?>/js/application.js"></script>
	</body>
</html>