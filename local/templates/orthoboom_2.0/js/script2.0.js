$(function () {
    if (typeof mask === 'function') {
        $('input[name*=PHONE]').mask('+7 (999) 999 99 99', {placeholder: " ", autoclear: false});
    }

    /* аккордион */

    if (typeof accordion === 'function') {
        $("#accordion").accordion({
            collapsible: true,
            active: false,
            heightStyle: "content",
            beforeActivate: function (event, ui) {
                if (ui.newHeader[0]) {
                    var currHeader = ui.newHeader;
                    var currContent = currHeader.next('.ui-accordion-content');
                } else {
                    var currHeader = ui.oldHeader;
                    var currContent = currHeader.next('.ui-accordion-content');
                }
                var isPanelSelected = currHeader.attr('aria-selected') == 'true';
                currHeader.toggleClass('ui-corner-all', isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top', !isPanelSelected).attr('aria-selected', ((!isPanelSelected).toString()));
                currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e', isPanelSelected).toggleClass('ui-icon-triangle-1-s', !isPanelSelected);
                currContent.toggleClass('accordion-content-active', !isPanelSelected)
                if (isPanelSelected) {
                    currContent.slideUp();
                } else {
                    currContent.slideDown();
                }

                return false;
            }

        });
    }

    if (typeof scrollbar === 'function') {
        $('.scrollbar-inner').scrollbar();
        $('.scrollbar-chat').scrollbar();
    }

    $('.level-line a').tooltip();


    $('body').on('click', '.plus', function () {
        $plus = $(this).parent().find('input').val();
        $plus = parseInt($plus) + 1;
        $(this).parent().find('input').val($plus);

        $(this).parent().find('input').trigger("change");
        return false;
    });

    $('body').on('click', '.minus', function () {
        $plus = $(this).parent().find('input').val();
        $plus = parseInt($plus) - 1;
        if ($plus > 0)
            $(this).parent().find('input').val($plus);

        $(this).parent().find('input').trigger("change");
        return false;
    });

    $("body").on("keydown", ".counter-input", function (e) {
        // Allow: backspace, delete, tab, escape, enter
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }

    });

    // $('.cart-delete a').click(function(){
    // 	$(this).parent().parent().remove();
    // 	return false;
    // });


    /* добавление текстового поля */

    $('body').on("click", ".input-add a", function () {
        $(this).parent().before('<div><div class="more-inputs"><input type="text" name="EMAILS[]" value=""><div class="remove"></div></div></div>');
        $(this).parent().css({'marginTop': '0'});
        return false;
    });

    /* добавление телефона */

    $('body').on("click", ".tel-add a", function () {
        $(this).parent().before('<div><div class="more-inputs"><input type="text" name="PHONE[]" value=""><div class="remove"></div></div></div>');
        $(this).parent().css({'marginTop': '0'});
        $('input[name*="PHONE[]"]').mask('+7 (999) 999 99 99', {placeholder: " "});
        return false;
    });
    /* добавление textarea */

    $('.change-addr').click(function () {
        $(this).parent().append('<div><div class="more-inputs"><textarea></textarea><div class="remove"></div></div></div>');
        return false;
    });


    /* добавление и удаление адреса, Сделать основным*/

    // $('.address-add a').click(function(){
    // 	$(this).parent().parent().find('.address-radio').removeClass('active');
    // 	$(this).parent().parent().find('.addresses').append('<div class="address-block"><div class="address-radio active">Сделать основным:</div><div class="more-inputs"><textarea></textarea><div class="remove"></div></div></div>');
    // 	return false;
    // });

    // $(document).on('click','.address-radio',function(e) {
    // 	$(this).parent().find('.address-radio').removeClass('active');
    // 	$(this).addClass('active');

    // });


    // $(document).on('click','.address-radio .remove',function(e) {
    // 	$(this).parent().parent().remove();
    // 	return false;
    // });


    /* удаление строк */	 	

    $(document).on('click', '.more-inputs .remove', function (e) {
        $(this).parent().parent().remove();
        return false;
    });


    /* восставновление пароля*/	 	


    $('.recovery .radio').click(function () {
        $(this).parent().find('.radio').removeClass('active');
        $(this).parent().find('.radio input').attr('disabled', 'disabled');
        $(this).addClass('active');
        $(this).find('input').removeAttr('disabled');
    });


    /*  кнопки свернуть и развернуть */

    $('#accordion h3').click(function () {
        $(this).find('.to-show').toggleClass('to-hide');

        $value = $(this).find('.to-show').hasClass('to-hide');

        if ($value) {
            $(this).find('.to-show').html('<span>Свернуть</span>');
        }
        else {
            $(this).find('.to-show').html('<span>Развернуть</span>');
        }
        return false;


    });

    /* удаление документов */

    // $('.documents .remove-link').click(function(){
    // 	$(this).parent().parent().remove();
    // 	return false;
    // });

    /* детальное описание */	

    $('body').on('click', '.cart-link .readmore-btn', function (e) {
        e.preventDefault();
        $(this).parent().parent().toggleClass('active');
        $(this).parent().parent().find('.cart-link .readmore-btn').toggleClass('hide-in');
        $value = $(this).parent().parent().hasClass('active');
        if ($value) {
            $(this).parent().parent().find('.cart-link .readmore-btn').html('<span>Свернуть</span>');
            $(this).parent().parent().find('.detail-info').fadeIn(100);
        }
        else {
            $(this).parent().parent().find('.cart-link .readmore-btn').html('Описание');
            $(this).parent().parent().find('.detail-info').fadeOut(100);
        }


    });

    $('.personal-order-link .readmore-btn').click(function () {
        $(this).parent().parent().toggleClass('active');
        $(this).parent().parent().find('.personal-order-link .readmore-btn').toggleClass('hide-in');
        $value = $(this).parent().parent().hasClass('active');
        if ($value) {
            $(this).parent().parent().find('.personal-order-link .readmore-btn').html('<span>Свернуть</span>');
            $(this).parent().parent().find('.detail-info').fadeIn(100);
        }
        else {
            $(this).parent().parent().find('.personal-order-link .readmore-btn').html('Оформить заказ');
            $(this).parent().parent().find('.detail-info').fadeOut(100);
        }
        return false;

    });

    $('.warehouse-order .readmore-btn').click(function () {
        $(this).parent().parent().toggleClass('active');
        $(this).parent().parent().find('.warehouse-order .readmore-btn').toggleClass('hide-in');
        $value = $(this).parent().parent().hasClass('active');
        if ($value) {
            $(this).parent().parent().find('.warehouse-order .readmore-btn').html('<span>Свернуть</span>');
            $(this).parent().parent().find('.detail-info').fadeIn(100);
        }
        else {
            $(this).parent().parent().find('.warehouse-order .readmore-btn').html('Заказать');
            $(this).parent().parent().find('.detail-info').fadeOut(100);
        }
        return false;

    });


    /* добавление представителя */

    // $('.manager-add a').click(function(){
    // 	$card=$('.manager-card').html();
    // 	$('.new-manager-cards').append('<div class=manager-card>'+$card+'</div>');
    // 	return false;
    // });

    // $(document).on('click','.manager-card img',function(e) {
    // 	$(this).parent().parent().remove();
    // 	return false;
    // });


    /*   ???????? */
    // $(window).on('load', function () {
    //     setTimeout(function(){
    //         $(".owl-carousel-slider").owlCarousel({
    //             loop: true,
    //             items: 3,
    //             responsive: false,
    //             navText: ["", ""],
    //             nav: false,
    //             afterInit: console.log('inited')
    //         })}, 10000);
    // });
    $(document).ready(function() {
        // if (typeof owlCarousel === 'function') {
        //     $(".slider-on-main").not(".slider-on-main--fix").owlCarousel({
        //         loop: true,
        //         items: 1,
        //         responsive: false,
        //         navigationText: ["", ""],
        //         nav: false,
        //         autoPlay: 5000
        //     });
        //     $(".owl-carousel").not(".slider-on-main").owlCarousel({
        //         loop: true,
        //         items: 1,
        //         responsive: false,
        //         navText: ["", ""],
        //         nav: false
        //     });
        //     $(".owl-carousel2").owlCarousel({
        //         center: true,
        //         loop: true,
        //         items: 3,
        //         responsive: false,
        //         navText: ["", ""]
        //     });
        // }

        if($(".owl-carousel-slider").length){
            $(".owl-carousel-slider").owlCarousel({
                loop: true,
                items: 3,
                responsive: false,
                navigationText: ["", ""],
                navigation: true,
                autoPlay: true,
            })
        }
    });
    });
