<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>


<?if($USER->IsAuthorized()):?>

<p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>

<?else:?>
<script>
	$(function($){
		Recaptchafree.reset();
	});
</script>
<div class="navibar"><a href="/catalog/">Продолжить выбор товаров</a></div>
<h1 class="center">Регистрация новой организации</h1>

	<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
		<input type="hidden" name="REGISTER[LOGIN]" value="<?=$arResult["VALUES"]["EMAIL"]?>">
		<div class="user-profile register">								
				<div class="register-wrap">

						<?
						if (count($arResult["ERRORS"]) > 0):
							foreach ($arResult["ERRORS"] as $key => $error) {
								if (intval($key) == 0 && $key !== 0) {
									$arResult["ERRORS"][$key] = str_replace('Поле #FIELD_NAME#', '', $error);
									$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $arResult["ERRORS"][$key]);
								}
							}


							//ShowError(implode("<br />", $arResult["ERRORS"]));

						elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
						?>
						<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
						<?endif?>
					
						<?
						if($arResult["BACKURL"] <> ''):
						?>
							<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
						<?
						endif;
						?>

						<div><label for="">Имя*</label><input size="30" type="text" name="REGISTER[NAME]" value="<?=$arResult["VALUES"]["NAME"]?>"<?=(isset($arResult["ERRORS"]["NAME"])) ? ' class="error-field"' : '';?>>

							<?if (isset($arResult["ERRORS"]["NAME"])):?>
								<p class="error-message"><?=$arResult["ERRORS"]["NAME"]?></p>
								<br><br>
							<?endif;?>
						</div>
						<div><label for="">Фамилия*</label><input size="30" type="text" name="REGISTER[LAST_NAME]" value="<?=$arResult["VALUES"]["LAST_NAME"]?>"<?=(isset($arResult["ERRORS"]["LAST_NAME"])) ? ' class="error-field"' : '';?>>
							<?if (isset($arResult["ERRORS"]["LAST_NAME"])):?>
								<p class="error-message"><?=$arResult["ERRORS"]["LAST_NAME"]?></p>
								<br><br>
							<?endif;?>
						</div>
						<div>
							<label for="">Контактный телефон*</label><input size="30" type="text" name="REGISTER[PERSONAL_PHONE]" value="<?=$arResult["VALUES"]["PERSONAL_PHONE"]?>"<?=(isset($arResult["ERRORS"]["PERSONAL_PHONE"])) ? ' class="error-field"' : '';?>>
							<?if (isset($arResult["ERRORS"]["PERSONAL_PHONE"])):?>
								<p class="error-message"><?=$arResult["ERRORS"]["PERSONAL_PHONE"]?></p>
								<br><br>
							<?endif;?>

							<?foreach($arResult["PHONES"] as $phone):
								$phoneClear = preg_replace('~\D+~', '', $phone);
								$additionalClass = (strlen($phoneClear) != 11) ? ' class="error-field"' : '';
								?>
								<div><div class="more-inputs"><input type="text" name="PHONE[]" value="<?=$phone?>"<?=$additionalClass?>><div class="remove"></div></div></div>
							<?endforeach;?>

							<!--<p class="tel-add">+ <a href="#">Добавить еще один</a></p><br>-->
						</div>	
						
						<div >
							<label for="">Email*</label><input size="30" type="text" name="REGISTER[EMAIL]" value="<?=$arResult["VALUES"]["EMAIL"]?>"<?=(isset($arResult["ERRORS"]["EMAIL"])) ? ' class="error-field"' : '';?>>										

							<?if (isset($arResult["ERRORS"]["EMAIL"])):?>
								<p class="error-message"><?=$arResult["ERRORS"]["EMAIL"]?></p>
								<br><br>
							<?endif;?>
						</div>	
						<div >
							<label for="">Название организации*</label><input type="text" name="ORGANIZATION" value="<?=$arResult["F_CUSTOM"]["ORGANIZATION"]?>"<?=(isset($arResult["ERRORS"]["ORGANIZATION"])) ? ' class="error-field"' : '';?>> 

							<?if (isset($arResult["ERRORS"]["ORGANIZATION"])):?>
								<p class="error-message"><?=$arResult["ERRORS"]["ORGANIZATION"]?></p>
								<br><br>
							<?endif;?>												
						</div>	
						<div>
							<label for="">ИНН организации*</label><input type="text" name="INN" value="<?=$arResult["F_CUSTOM"]["INN"]?>"<?=(isset($arResult["ERRORS"]["INN"])) ? ' class="error-field int"' : ' class="int"';?>> 

							<?if (isset($arResult["ERRORS"]["INN"])):?>
								<p class="error-message"><?=$arResult["ERRORS"]["INN"]?></p>
								<br><br>
							<?endif;?>												
						</div>										
						<div><label for="">Город*</label><input size="30" type="text" name="REGISTER[PERSONAL_CITY]" value="<?=$arResult["VALUES"]["PERSONAL_CITY"]?>"<?=(isset($arResult["ERRORS"]["PERSONAL_CITY"])) ? ' class="error-field"' : '';?>>
							<?if (isset($arResult["ERRORS"]["PERSONAL_CITY"])):?>
								<p class="error-message"><?=$arResult["ERRORS"]["PERSONAL_CITY"]?></p>
								<br><br>
							<?endif;?>
						</div>
						<div>
							<label for="">Юридический адрес*</label><textarea name="LEGAL_ADDRESS"<?=(isset($arResult["ERRORS"]["LEGAL_ADDRESS"])) ? ' class="error-field"' : '';?>><?=$arResult["F_CUSTOM"]["LEGAL_ADDRESS"]?></textarea>	

							<?if (isset($arResult["ERRORS"]["LEGAL_ADDRESS"])):?>
								<p class="error-message"><?=$arResult["ERRORS"]["LEGAL_ADDRESS"]?></p>
								<br><br>
							<?endif;?>

							<p class="checkbox-add"><input type="checkbox" id="check_1" name="LEGAL_IS_ACTUAL" value="Y"<?=($arResult["F_CUSTOM"]["LEGAL_IS_ACTUAL"] == "Y") ? ' checked' : '';?>><label for="check_1">Юридический адрес совпадает с фактическим адресом</label></p>													
						</div>
						<div>
							<label for="">Фактический адрес*</label><textarea name="ACTUAL_ADDRESS"<?=(isset($arResult["ERRORS"]["ACTUAL_ADDRESS"])) ? ' class="error-field"' : '';?>><?=$arResult["F_CUSTOM"]["ACTUAL_ADDRESS"]?></textarea>	<br><br>	

							<?if (isset($arResult["ERRORS"]["ACTUAL_ADDRESS"])):?>
								<p class="error-message"><?=$arResult["ERRORS"]["ACTUAL_ADDRESS"]?></p>
								<br><br>
							<?endif;?>					
						</div>	
						<div class="change-pass">
							<div><label for="">Пароль*</label><input size="30" type="password" name="REGISTER[PASSWORD]" value="<?=$arResult["VALUES"]["PASSWORD"]?>" autocomplete="off" id="cur-pass"<?=(isset($arResult["ERRORS"]["PASSWORD"])) ? ' class="error-field"' : '';?>><input value="Показать" type="checkbox" onchange="if ($('#cur-pass').get(0).type=='password') {$('#cur-pass').get(0).type='text'; $('#cur-pass').addClass('active') }  else {$('#cur-pass').get(0).type='password'; $('#cur-pass').removeClass('active');}">
								<?if (isset($arResult["ERRORS"]["PASSWORD"])):?>
									<p class="error-message"><?=$arResult["ERRORS"]["PASSWORD"]?></p>
									<br><br>
								<?endif;?>
							</div>		
							<div><label for="">Повторите пароль*</label><input size="30" type="password" name="REGISTER[CONFIRM_PASSWORD]" value="<?=$arResult["VALUES"]["CONFIRM_PASSWORD"]?>" autocomplete="off" id="confirm-pass"<?=(isset($arResult["ERRORS"]["CONFIRM_PASSWORD"])) ? ' class="error-field"' : '';?>><input value="Показать" type="checkbox" onchange="if ($('#confirm-pass').get(0).type=='password') {$('#confirm-pass').get(0).type='text'; $('#confirm-pass').addClass('active') }  else {$('#confirm-pass').get(0).type='password'; $('#confirm-pass').removeClass('active');}"> <span class="pass-ok"></span>
								<?if (isset($arResult["ERRORS"]["CONFIRM_PASSWORD"])):?>
									<p class="error-message"><?=$arResult["ERRORS"]["CONFIRM_PASSWORD"]?></p>
									<br><br>
								<?endif;?>
							</div>													
						</div>
						<p>* — Поля, отмеченные звездочкой, обязательны для заполнения!</p>
						<?
						/* CAPTCHA */
						if ($arResult["USE_CAPTCHA"] == "Y")
						{
							?>
								<div class="cart-captcha">
									<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
									<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
									<input type="text" name="captcha_word" maxlength="50" value="" style="width: 137px; float: right; height: 30px;"></td>
									<br><br>
									<?if (isset($arResult['ERRORS']['CAPTCHA'])):?>
										<p class="error-message">Пройдите проверку на то, что Вы не робот</p>
										<br><br>
									<?endif;?>
									<div class="clearfix"></div>
								</div>
							<?
						}
						/* !CAPTCHA */
						?>
							
				</div>
				<div class="advantages ur-advantages">
					<div class="adv-wrap">
						<h2 class="center">Преимущества личного кабинета для организаций</h2><br> 
						<div class="adv adv-1">Персональные условия</div> 
						<div class="adv adv-2">Купите первыми - список желаний</div> 	
						<div class="adv adv-3">История заказов</div> 	
						<div class="adv adv-4">Эксклюзивные акции только для вас</div> 	
						<div class="adv adv-5">Рекомендуемые товары</div> 	
						<div class="adv adv-6">Просмотр наличия на складе</div> 		
						<div class="adv adv-7">Документооборот</div> 	
						<div class="adv adv-8">Узнайте первыми о новинках</div> 
						<div class="adv adv-9">Чат с персональным менеджером</div> 	
						<div class="adv adv-10">Заказ через сайт</div> 											
					</div> 
				
				</div>
				<div class="clearfix"></div>
		</div>
		<div class="form-bottom">	
						<div class="cancell-button"><a href="/">Отменить</a></div> 
						<div class="login-button">
							<input type="submit" name="register_submit_button" class="btn-submit" value="Зарегистрироваться">
						</div> 						
		</div>
	</form>	


<?endif?>




<script>
	$('input:checkbox').customCheckbox();
	$('[name="REGISTER[PERSONAL_PHONE]"], [name="PHONE[]"]').mask('+7 (999) 999 99 99',{placeholder:" "});
	$('[name="REGISTER[CONFIRM_PASSWORD]"]').on("blur", function(){
		if ($.trim($("#cur-pass").val()) == $.trim($("#confirm-pass").val()) && $.trim($("#confirm-pass").val()) != "") {
			$(".pass-ok").fadeIn();
		} else {
			$(".pass-ok").fadeOut();
		}
	});
	$('[name="REGISTER[CONFIRM_PASSWORD]"]').blur();

	$(".error-field").on("blur", function(){
		if ($(this).val() != "") {
			$(this).removeClass("error-field");
			$(this).parents("div").eq(0).find(".error-message").remove();
		}
	});
</script>