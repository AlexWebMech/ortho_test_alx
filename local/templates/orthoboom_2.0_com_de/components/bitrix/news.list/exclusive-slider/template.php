<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="slider-on-main owl-carousel">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<?if(!empty($arItem["PREVIEW_TEXT"])):?>
				<div class="slide-desc"><?echo $arItem["PREVIEW_TEXT"]?></div>
			<?endif;?>
			<div class="blue-buttons">
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">Подробнее</a>
			</div>
			<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
				<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
			<?endif;?>
		</div>
	<?endforeach;?>						
</div>
<script>
	$(function(){
		 $(".slider-on-main").owlCarousel({
		    center: true,
			loop:true,
			items:3,				
			responsive:false,
			navigationText: ["",""],
			navigation: true,
			dots: false,
			autoPlay: 5000
		});
	});
</script>
<?if (count($arResult["ITEMS"]) == 0):?>
	<style>.exclusive-header {display: none;}</style>
<?endif;?>