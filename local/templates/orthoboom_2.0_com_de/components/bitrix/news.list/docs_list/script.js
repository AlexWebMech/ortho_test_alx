$(function(){
	$("#doc-field").on("change", function(){
		if ($(this).val() != "") {
			$("body").append('<div class="preloader-overlay"></div>');
			$(this).parents("form").submit();
		}
	});

	$('.documents .remove-link').click(function(e){
		if (confirm('Вы действительно хотите удалить документ?')) {
			$("body").append('<div class="preloader-overlay"></div>');
			return true;
		} else {
			return false;
		}
	});
});