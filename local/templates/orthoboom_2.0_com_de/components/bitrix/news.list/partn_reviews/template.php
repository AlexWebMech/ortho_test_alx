<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//if(count($arResult["ITEMS"]) != 0):
?>
<h2>Отзывы клиентов</h2>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<?//=$this->GetEditAreaId($arItem['ID']);?>
	<div class="comment_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
     	<blockquote>
			<?=$arItem["PREVIEW_TEXT"]?>
		</blockquote>
		<p class="sign"><?=$arItem["NAME"]?></p>
	</div>
<?endforeach;?>
<?//endif;?>