<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

?>

<?if (isset($_REQUEST["add"])):?>

	<?require_once("add_u.php");?>

<?elseif (isset($_REQUEST["edit"])):?>

	<?require_once("edit_u.php");?>


<?else:?>

<div class="bx-auth-profile">

<div style="display: none;">
	<?ShowError($arResult["strProfileError"]);?>
</div>
<?if (isset($arResult["DELETED_COMPANY"])):?>
	<div class="success-msg">
	<p><font class="notetext" style="color: #c0005d;">Юридическое лицо успешно удалено</font></p></div>
<?endif;?>

<?
if ($arResult['DATA_SAVED'] == 'Y') {
	echo '<div class="success-msg">';
	ShowNote(GetMessage('PROFILE_DATA_SAVED'));
	echo '</div>';
}

?>







<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data" style="margin-bottom: 0;" id="ur-profile-form">
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
<input type="hidden" name="LOGIN" value="<? echo $arResult["arUser"]["LOGIN"]?>">

<div class="user-profile">								
	<h2>Контактные данные</h2>	





		<div><label for=""><?=GetMessage('NAME')?>*</label><input type="text" name="NAME" value="<?=$arResult["arUser"]["NAME"]?>" <?=(isset($arResult["ERRORS"]["NAME"])) ? ' class="error-field"' : '';?>>
			<?if (isset($arResult["ERRORS"]["NAME"])):?>
				<p class="error-message"><?=$arResult["ERRORS"]["NAME"]?></p>
				<br><br>
			<?endif;?>
		</div>

		<div><label for=""><?=GetMessage('LAST_NAME')?>*</label><input type="text" name="LAST_NAME" value="<?=$arResult["arUser"]["LAST_NAME"]?>" <?=(isset($arResult["ERRORS"]["LAST_NAME"])) ? ' class="error-field"' : '';?>></div>
		<?if (isset($arResult["ERRORS"]["LAST_NAME"])):?>
			<p class="error-message"><?=$arResult["ERRORS"]["LAST_NAME"]?></p>
			<br><br>
		<?endif;?>

		<div><label for=""><?=GetMessage('USER_PHONE')?>*</label><input type="text" name="PERSONAL_PHONE" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" <?=(isset($arResult["ERRORS"]["PERSONAL_PHONE"])) ? ' class="error-field"' : '';?> name="PHONE"> 
		<?if (isset($arResult["ERRORS"]["PERSONAL_PHONE"])):?>
			<p class="error-message"><?=$arResult["ERRORS"]["PERSONAL_PHONE"]?></p>
			<br><br>
		<?endif;?>

		<?foreach($arResult["PHONES"] as $phone):?>
			<div><div class="more-inputs"><input type="text" name="PHONE[]" value="<?=$phone?>"><div class="remove"></div></div></div>
		<?endforeach;?>

		<p class="tel-add">+ <a href="#">Добавить еще один</a></p><br> </div>	
		<div><label for=""><?=GetMessage('EMAIL')?>*</label><input type="text" name="EMAIL" value="<? echo $arResult["arUser"]["EMAIL"]?>" <?=(isset($arResult["ERRORS"]["EMAIL"])) ? ' class="error-field"' : '';?>> 	 
		<?if (isset($arResult["ERRORS"]["EMAIL"])):?>
			<p class="error-message"><?=$arResult["ERRORS"]["EMAIL"]?></p>
			<br><br>
		<?endif;?>

					
		<?
			$arUserField = $arResult["USER_PROPERTIES"]["DATA"]["UF_NOTIFY_ACTION"];
		?>
		<input type="hidden" value="0" name="UF_NOTIFY_ACTION">
		<p class="checkbox-add"><input type="checkbox" id="check_1" value="1" name="UF_NOTIFY_ACTION" <?=($arUserField["VALUE"]) ? ' checked' : ''?>><label for="check_1">Уведомлять о новинках и акциях</label></p>		


		<div>
			<label for="">Дата рождения</label>
			<div class="birthdate">
				<?
					if (!empty($arResult["arUser"]["PERSONAL_BIRTHDAY"])) {
						$birthdateUnix = strtotime($arResult["arUser"]["PERSONAL_BIRTHDAY"]);
					}
				?>
				<div class="birthdate-day">
					<select name="birthdate_day">
						<?for($i = 1; $i <= 31; $i++):
							$selected = '';
							if (isset($birthdateUnix) && $i == intval(date("d", $birthdateUnix))) {
								$selected = ' selected';
							} else if (!isset($birthdateUnix) && $i == 1) {
								$selected = ' selected';
							}
							?>
							<option value="<?=$i?>"<?=$selected?>><?=$i?></option>
						<?endfor;?>												
					</select>
				</div>
				<div class="birthdate-month">
					<select name="birthdate_month">
						<?
						    $months = array(
						        "1" => "января",
						        "2" => "февраля",
						        "3" => "марта",
						        "4" => "апреля",
						        "5" => "мая",
						        "6" => "июня",
						        "7" => "июля",
						        "8" => "августа",
						        "9" => "сентября",
						        "10" => "октября",
						        "11" => "ноября",
						        "12" => "декабря"
						    );
						?>
						<?for($i = 1; $i <= 12; $i++):
							$selected = '';
							if (isset($birthdateUnix) && $i == intval(date("m", $birthdateUnix))) {
								$selected = ' selected';
							} else if (!isset($birthdateUnix) && $i == 1) {
								$selected = ' selected';
							}
							?>
							<option value="<?=$i?>"<?=$selected?>><?=$months[$i]?></option>
						<?endfor;?>															
					</select>
				</div>											
				<div class="birthdate-year">
					<select name="birthdate_year">
						<?for($i = 1970; $i <= date("Y") - 10; $i++):
							$selected = '';
							if (isset($birthdateUnix) && $i == intval(date("Y", $birthdateUnix))) {
								$selected = ' selected';
							} else if (!isset($birthdateUnix) && $i == 1970) {
								$selected = ' selected';
							}
							?>
							<option value="<?=$i?>"<?=$selected?>><?=$i?></option>
						<?endfor;?>														
					</select>
				</div>
				<?unset($birthdateUnix);?>											
			</div>
			<div class="clearfix"></div>
		</div>		
		<p class="checkbox-add2"><input type="checkbox" name="COMPANY_DATA[0][PROPERTY_I_AM_HEAD_VALUE]" value="Да" id="check_2"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_I_AM_HEAD_VALUE"] == "Да") ? ' checked': ''?>><label for="check_2">Я являюсь руководителем данной компании</label></p>


		<h2>Руководство компании</h2>	

		<input type="hidden" name="COMPANY_DATA[0][ID]" value="<?=$arResult["COMPANY_DATA"][0]["ID"]?>">

		<div><label for="">Имя</label><input type="text" name="COMPANY_DATA[0][PROPERTY_NAME_HEAD_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_NAME_HEAD_VALUE"]?>"></div>



		<div><label for="">Фамилия</label><input type="text" name="COMPANY_DATA[0][PROPERTY_LAST_NAME_HEAD_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_LAST_NAME_HEAD_VALUE"]?>"></div>


		<div>
			<?
				$classStr = "";
				if ($classStr == "") {
					$phoneClear = preg_replace('~\D+~', '', $arResult["COMPANY_DATA"][0]["PROPERTY_PHONE_HEAD_VALUE"]);
					$classStr = (strlen($phoneClear) != 11 && isset($_REQUEST["save"]) && $phoneClear != "") ? ' class="error-field"' : '';
				}
			?>
			<label for="">Контактный телефон</label><input type="text" name="COMPANY_DATA[0][PROPERTY_PHONE_HEAD_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_PHONE_HEAD_VALUE"]?>" name="PHONE"<?=$classStr?>>
		</div>
		<?if (isset($_REQUEST["save"])):?>
			<?
			$phoneClear = preg_replace('~\D+~', '', $arResult["COMPANY_DATA"][0]["PROPERTY_PHONE_HEAD_VALUE"]);
			if (strlen($phoneClear) != 11 && $phoneClear != ""):?>
				<p class="error-message">Телефон введен неверно</p>
				<br><br>
			<?endif;?>
		<?endif;?>



		<p class="tel-add">+ <a href="#">Добавить еще один</a></p><br>									
		<div><label for="">Email</label><input type="text" name="COMPANY_DATA[0][PROPERTY_EMAIL_HEAD_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_EMAIL_HEAD_VALUE"]?>" <?=(!filter_var($arResult["COMPANY_DATA"][0]["PROPERTY_EMAIL_HEAD_VALUE"], FILTER_VALIDATE_EMAIL) && isset($_REQUEST["save"]) && $arResult["COMPANY_DATA"][0]["PROPERTY_EMAIL_HEAD_VALUE"] != "") ? ' class="error-field"' : '';?>> </div>		
		<div>
		<?if(!filter_var($arResult["COMPANY_DATA"][0]["PROPERTY_EMAIL_HEAD_VALUE"], FILTER_VALIDATE_EMAIL) && isset($_REQUEST["save"]) && $arResult["COMPANY_DATA"][0]["PROPERTY_EMAIL_HEAD_VALUE"] != ""):?>
			<p class="error-message">Email руководства указан неверно</p>
			<br><br>
		<?endif;?>

			<?
				if (!empty($arResult["COMPANY_DATA"][0]["PROPERTY_DATE_BIRTH_HEAD_VALUE"])) {
					$birthdateUnix = strtotime($arResult["COMPANY_DATA"][0]["PROPERTY_DATE_BIRTH_HEAD_VALUE"]);
				}
			?>
			<label for="">Дата рождения</label> 
			<div class="birthdate">
				<div class="birthdate-day">
					<select name="birthdate_day_head">
						<?for($i = 1; $i <= 31; $i++):
							$selected = '';
							if (isset($birthdateUnix) && $i == intval(date("d", $birthdateUnix))) {
								$selected = ' selected';
							} else if (!isset($birthdateUnix) && $i == 1) {
								$selected = ' selected';
							}
							?>
							<option value="<?=$i?>"<?=$selected?>><?=$i?></option>
						<?endfor;?>												
					</select>
				</div>
				<div class="birthdate-month">
					<select name="birthdate_month_head">
						<?
						    $months = array(
						        "1" => "января",
						        "2" => "февраля",
						        "3" => "марта",
						        "4" => "апреля",
						        "5" => "мая",
						        "6" => "июня",
						        "7" => "июля",
						        "8" => "августа",
						        "9" => "сентября",
						        "10" => "октября",
						        "11" => "ноября",
						        "12" => "декабря"
						    );
						?>
						<?for($i = 1; $i <= 12; $i++):
							$selected = '';
							if (isset($birthdateUnix) && $i == intval(date("m", $birthdateUnix))) {
								$selected = ' selected';
							} else if (!isset($birthdateUnix) && $i == 1) {
								$selected = ' selected';
							}
							?>
							<option value="<?=$i?>"<?=$selected?>><?=$months[$i]?></option>
						<?endfor;?>															
					</select>
				</div>											
				<div class="birthdate-year">
					<select name="birthdate_year_head">
						<?for($i = 1970; $i <= date("Y") - 10; $i++):
							$selected = '';
							if (isset($birthdateUnix) && $i == intval(date("Y", $birthdateUnix))) {
								$selected = ' selected';
							} else if (!isset($birthdateUnix) && $i == 1970) {
								$selected = ' selected';
							}
							?>
							<option value="<?=$i?>"<?=$selected?>><?=$i?></option>
						<?endfor;?>														
					</select>
				</div>											
			</div>									
		
		</div>	


	<h2>Информация о компании</h2>										
		<div>
			<label for="">Название компании*</label><input type="text" name="COMPANY_DATA[0][NAME]" value="<?=$arResult["COMPANY_DATA"][0]["NAME"]?>"<?=($arResult["COMPANY_DATA"][0]["NAME"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>> 												
		</div>

		<?if ($arResult["COMPANY_DATA"][0]["NAME"] == "" && isset($_REQUEST["save"])):?>
			<p class="error-message">Обязательно для заполнения</p>
			<br><br>
		<?endif;?>		
		
		<div><label for="">Город</label><input type="text" name="COMPANY_DATA[0][PROPERTY_CITY_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_CITY_VALUE"]?>"></div>
		<div>

			<label for="">Адреса</label>
			<div class="addresses">
					<?
						$setMain = "Y";
						foreach($arResult["COMPANY_DATA"][0]["SET_MAIN_ADDRESS"] as $setValue){
							if ($setValue == "Y") {
								$setMain = "N";
								break;
							}
						}
					?>
					<div class="address-radio<?=($setMain == "Y") ? ' active' : '';?>">Сделать основным:</div>
					<textarea name="COMPANY_DATA[0][PROPERTY_ADDRESS_VALUE]"><?=$arResult["COMPANY_DATA"][0]["PROPERTY_ADDRESS_VALUE"]?></textarea>	<br><br>



				<?if (count($arResult["COMPANY_DATA"][0]["PROPERTY_ADDRESSES_VALUE"]) > 0):?>
					<?foreach($arResult["COMPANY_DATA"][0]["PROPERTY_ADDRESSES_VALUE"] as $k => $addr):?>
						<div class="address-block"><div class="address-radio<?=($arResult["COMPANY_DATA"][0]["SET_MAIN_ADDRESS"][$k] == "Y") ? ' active' : '';?>">Сделать основным:
							<input type="hidden" name="COMPANY_DATA[0][SET_MAIN_ADDRESS][]" value="<?=$arResult["COMPANY_DATA"][0]["SET_MAIN_ADDRESS"][$k]?>">
						</div><div class="more-inputs"><textarea name="COMPANY_DATA[0][PROPERTY_ADDRESSES_VALUE][]"><?=$addr?></textarea><div class="remove"></div></div></div>
					<?endforeach;?>
				<?endif;?>											
			</div>
			<p class="address-add">+ <a href="#">Добавить еще один</a></p>							
			<div class="clearfix"></div><br> 
		</div>
		<?
			$legalAddress = trim(toLower($arResult["COMPANY_DATA"][0]["PROPERTY_LEGAL_ADDRESS_VALUE"]));
			$actualAddress = trim(toLower($arResult["COMPANY_DATA"][0]["PROPERTY_ACTUAL_ADDRESS_VALUE"]));

			$equalAddress = $legalAddress == $actualAddress && $legalAddress != "" && $actualAddress != "";
		?>
		<div class="address-toggler">
			<div>
				<label for="">Юридический адрес*</label><textarea data-address="legal" name="COMPANY_DATA[0][PROPERTY_LEGAL_ADDRESS_VALUE]"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_LEGAL_ADDRESS_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>><?=$arResult["COMPANY_DATA"][0]["PROPERTY_LEGAL_ADDRESS_VALUE"]?></textarea>	
				<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_LEGAL_ADDRESS_VALUE"] == "" && isset($_REQUEST["save"])):?>
					<p class="error-message">Обязательно для заполнения</p>
					<br><br>
				<?endif;?>	

				<p class="checkbox-add"><input type="checkbox" data-address="equal" name="" id="check_3" <?=($equalAddress) ? ' checked' : ''?>><label for="check_3">Юридический адрес совпадает с фактическим адресом</label></p>													
			</div>
	

			<div>
				<label for="">Фактический адрес*</label><textarea data-address="actual" name="COMPANY_DATA[0][PROPERTY_ACTUAL_ADDRESS_VALUE]"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_ACTUAL_ADDRESS_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>><?=$arResult["COMPANY_DATA"][0]["PROPERTY_ACTUAL_ADDRESS_VALUE"]?></textarea>			
			</div>	<br> 
			<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_ACTUAL_ADDRESS_VALUE"] == "" && isset($_REQUEST["save"])):?>
				<p class="error-message">Обязательно для заполнения</p>
				<br><br>
			<?endif;?>
		</div>

		<div><label for="">ОГРН</label><input type="text" name="COMPANY_DATA[0][PROPERTY_OGRN_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_OGRN_VALUE"]?>"> </div>


		<div><label for="">ИНН*</label><input type="text" name="COMPANY_DATA[0][PROPERTY_INN_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_INN_VALUE"]?>"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_INN_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>> </div>	

		<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_INN_VALUE"] == "" && isset($_REQUEST["save"])):?>
			<p class="error-message">Обязательно для заполнения</p>
			<br><br>
		<?endif;?>	

		<div><label for="">КПП</label><input type="text" name="COMPANY_DATA[0][PROPERTY_KPP_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_KPP_VALUE"]?>"> </div>	

		<div><label for="">Наименование банка</label><input type="text" name="COMPANY_DATA[0][PROPERTY_BANK_NAME_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_BANK_NAME_VALUE"]?>"> </div>	


		<div><label for="">Расчетный счет*</label><input type="text" name="COMPANY_DATA[0][PROPERTY_RASCH_SCHET_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_RASCH_SCHET_VALUE"]?>"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_RASCH_SCHET_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>> </div>	

		<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_RASCH_SCHET_VALUE"] == "" && isset($_REQUEST["save"])):?>
			<p class="error-message">Обязательно для заполнения</p>
			<br><br>
		<?endif;?>

		<div><label for="">Адрес банка*</label><textarea name="COMPANY_DATA[0][PROPERTY_BANK_ADDRESS_VALUE]"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_BANK_ADDRESS_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>><?=$arResult["COMPANY_DATA"][0]["PROPERTY_BANK_ADDRESS_VALUE"]?></textarea></div>

		<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_BANK_ADDRESS_VALUE"] == "" && isset($_REQUEST["save"])):?>
			<p class="error-message">Обязательно для заполнения</p>
			<br><br>
		<?endif;?>

		<div><label for="">БИК банка*</label><input type="text" name="COMPANY_DATA[0][PROPERTY_BANK_BIK_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_BANK_BIK_VALUE"]?>"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_BANK_BIK_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>> </div>	

		<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_BANK_BIK_VALUE"] == "" && isset($_REQUEST["save"])):?>
			<p class="error-message">Обязательно для заполнения</p>
			<br><br>
		<?endif;?>

		<div><label for="">Кор. счет банка*</label><input type="text" name="COMPANY_DATA[0][PROPERTY_KORR_SCHET_VALUE]" value="<?=$arResult["COMPANY_DATA"][0]["PROPERTY_KORR_SCHET_VALUE"]?>"<?=($arResult["COMPANY_DATA"][0]["PROPERTY_KORR_SCHET_VALUE"] == "" && isset($_REQUEST["save"])) ? ' class="error-field"' : '';?>> </div>

		<?if ($arResult["COMPANY_DATA"][0]["PROPERTY_KORR_SCHET_VALUE"] == "" && isset($_REQUEST["save"])):?>
			<p class="error-message">Обязательно для заполнения</p>
			<br><br>
		<?endif;?>

		<div class="green-buttons">
			<a href="<?=$APPLICATION->GetCurPageParam("add=Y", array("add"))?>">Добавить юридическое лицо</a>
		</div>

		<?if (count($arResult["COMPANY_DATA"]) >= 2):?>
			<div class="clearfix"></div><br> 	
			<h2>Добавленные ранее юридические лица</h2>	
			<?foreach($arResult["COMPANY_DATA"] as $k => $arCompany):
				if ($k == 0)
					continue;
				?>
				<div class="added_company"><a href="<?=$APPLICATION->GetCurPageParam("edit=" . $arCompany["ID"], array("edit"))?>"><?=$arCompany["NAME"]?></a>
					<a href="<?=$APPLICATION->GetCurPageParam("delete=" . $arCompany["ID"], array("delete"));?>" title="Удалить" onclick="return confirm('Вы действительно хотите удалить?');" class="added_company__delete"><img src="<?=SITE_TEMPLATE_PATH?>/images/delete.png"></a>
				</div>
			<?endforeach;?>
		<?endif;?>

		<div class="clearfix"></div><br> 									
	<h2>Представители компании</h2>	
	<div style="display: none;" class="manager-card-example">
		<div class="manager-card">
			<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/images/delete.png"></a>
			<div><label for="">Должность*</label><input type="text" name="AGENTS[PROPERTY_POSITION_VALUE][]" value=""></div>
			<div><label for="">Имя*</label><input type="text" name="AGENTS[PROPERTY_NAME_VALUE][]" value=""></div>								
			<div><label for="">Фамилия*</label><input type="text" name="AGENTS[PROPERTY_LAST_NAME_VALUE][]" value=""></div>									
			<div><label for="">Контактный телефон*</label><input type="text" name="AGENTS[PROPERTY_PHONE_VALUE][]" value=""> </div>						
		</div>
	</div>
	<?foreach($arResult["AGENTS"] as $arAgent):?>
		<div class="manager-card">
			<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/images/delete.png"></a>
			<div><label for="">Должность*</label><input<?=($arAgent["PROPERTY_POSITION_VALUE"] == "") ? ' class="error-field"' : '';?> type="text" name="AGENTS[PROPERTY_POSITION_VALUE][]" value="<?=$arAgent["PROPERTY_POSITION_VALUE"]?>"></div>
			<div><label for="">Имя*</label><input<?=($arAgent["PROPERTY_NAME_VALUE"] == "") ? ' class="error-field"' : '';?> type="text" name="AGENTS[PROPERTY_NAME_VALUE][]" value="<?=$arAgent["PROPERTY_NAME_VALUE"]?>"></div>								
			<div><label for="">Фамилия*</label><input<?=($arAgent["PROPERTY_LAST_NAME_VALUE"] == "") ? ' class="error-field"' : '';?> type="text" name="AGENTS[PROPERTY_LAST_NAME_VALUE][]" value="<?=$arAgent["PROPERTY_LAST_NAME_VALUE"]?>"></div>				
			<?
			$classStr = "";
			$phoneClear = preg_replace('~\D+~', '', $arAgent["PROPERTY_PHONE_VALUE"]);
			if (strlen($phoneClear) != 11){
				$classStr = ' class="error-field"';
			}?>			
			<div><label for="">Контактный телефон*</label><input<?=$classStr?> type="text" name="AGENTS[PROPERTY_PHONE_VALUE][]" value="<?=$arAgent["PROPERTY_PHONE_VALUE"]?>"> </div>				
		</div>
	<?endforeach;?>
	<div class="new-manager-cards">
	</div>								
	<div class="manager-add">	
		<div class="green-buttons">
			<a href="">Добавить представителя</a>
		</div>									
	</div>								
	<div class="clearfix"></div><br> 


	<h2>Сменить пароль</h2>										

	<div class="change-pass">
		<div><label for="">Текущий пароль</label><input <?=(isset($arResult["ERRORS"]["OLD_PASSWORD"])) ? ' class="error-field"' : '';?>type="password" id="cur-pass"  name="OLD_PASSWORD"><input value="Показать" type="checkbox" onchange="if ($('#cur-pass').get(0).type=='password') {$('#cur-pass').get(0).type='text'; $('#cur-pass').addClass('active') }  else {$('#cur-pass').get(0).type='password'; $('#cur-pass').removeClass('active');}">

			<?if (isset($arResult["ERRORS"]["OLD_PASSWORD"])):?>
				<p class="error-message"><?=$arResult["ERRORS"]["OLD_PASSWORD"]?></p>
				<br><br>
			<?endif;?>
		</div>
		<div><label for=""><?=GetMessage('NEW_PASSWORD_REQ')?></label><input <?=(isset($arResult["ERRORS"]["PASSWORD"])) ? ' class="error-field"' : '';?>type="password" id="new-pass" name="NEW_PASSWORD"><input value="Показать" type="checkbox" onchange="if ($('#new-pass').get(0).type=='password') {$('#new-pass').get(0).type='text'; $('#new-pass').addClass('active') }  else {$('#new-pass').get(0).type='password'; $('#new-pass').removeClass('active');}">

			<?if (isset($arResult["ERRORS"]["PASSWORD"])):?>
				<p class="error-message"><?=$arResult["ERRORS"]["PASSWORD"]?></p>
				<br><br>
			<?endif;?>
		</div>
		<div><label for=""><?=GetMessage('NEW_PASSWORD_CONFIRM')?></label><input type="password" id="confirm-pass" name="NEW_PASSWORD_CONFIRM"><input value="Показать" type="checkbox" onchange="if ($('#confirm-pass').get(0).type=='password') {$('#confirm-pass').get(0).type='text'; $('#confirm-pass').addClass('active') }  else {$('#confirm-pass').get(0).type='password';  $('#confirm-pass').removeClass('active');}"></div>											
	</div>	
	<p>* — Поля, отмеченные звездочкой, обязательны для заполнения!</p>
</div>


<?
$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();
$managerId = intval($arUser["UF_MANAGER"]);

$managerId = 26;
$manageOnline = CUser::IsOnLine($managerId);

if ($managerId != 0):
	$rsUser = CUser::GetByID($managerId);
	$arUser = $rsUser->Fetch();
?>
<h2>Ваш персональный менеджер</h2>	
	<div class="personal-manager<?/*=($manageOnline) ? ' active' : '';*/?> active">
		<div class="answer">								
			<div class="manager-img">
				<?
					$avatarPath = SITE_TEMPLATE_PATH . "/images/consultant2.png";
					if (intval($arUser["PERSONAL_PHOTO"]) != 0) {
						$avatarPath = CFile::GetPath($arUser["PERSONAL_PHOTO"]);
					}
				?>
				<img src="<?=$avatarPath?>">
				<div class="manager-status">
					<ul>
						<?if ($manageOnline):?>
							<li>В сети</li>
						<?else:?>
							<li>В сети</li>
						<?endif;?>
					</ul>
				</div>
			</div>								
			<div class="answer-block">
				<p><?=$arUser["NAME"]?><br> <?=$arUser["LAST_NAME"]?></p>
				<p class="personal-title">Персональный менеджер</p>
				<table>
					<tr>
						<td width="200">Email:</td><td><strong><?=$arUser["EMAIL"]?></strong></td>
					</tr>	
					<?if ($arUser["PHONE"] != ""):?>												
						<tr>	
							<td>Контактный телефон:</td><td><strong><?=$arUser["PHONE"]?></strong></td>													
						</tr>
					<?endif;?>															
				</table>
			</div>
		</div>
		<div class="clearfix"></div><br> 									
	</div>
<?endif;?>
	
</div>
	<div class="form-bottom">	
		<div class="cancell-button"><a href="/profile/">Отменить</a></div> 
		<div class="login-button"><input type="submit" name="save" class="btn-submit" value="Сохранить изменения"></div> 	

	</div>	
</form>
<?
if($arResult["SOCSERV_ENABLED"])
{
	$APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
			"SHOW_PROFILES" => "Y",
			"ALLOW_DELETE" => "Y"
		),
		false
	);
}
?>
</div>

<?endif;?>
<script>
	$(function(){
		$('input:checkbox').customCheckbox();
	});
</script>

<?/*$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.form",
	"profile-company",
	Array(
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_NAME" => "Название компании",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_TAGS" => "",
		"DEFAULT_INPUT_SIZE" => "30",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"GROUPS" => array(),
		"IBLOCK_ID" => "49",
		"IBLOCK_TYPE" => "work_ob",
		"LEVEL_LAST" => "Y",
		"LIST_URL" => "",
		"MAX_FILE_SIZE" => "0",
		"MAX_LEVELS" => "100000",
		"MAX_USER_ENTRIES" => "100000",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"PROPERTY_CODES" => array("1196", "1202", "1203", "1204", "1205", "1206", "1207", "1208", "1209", "1210", "1212", "1415", "1416", "1418", "NAME"),
		"PROPERTY_CODES_REQUIRED" => array(),
		"RESIZE_IMAGES" => "N",
		"SEF_MODE" => "N",
		"STATUS" => "ANY",
		"STATUS_NEW" => "N",
		"USER_MESSAGE_ADD" => "Юридическое лицо успешно добавлено",
		"USER_MESSAGE_EDIT" => "Юридическое лицо обновлено",
		"USE_CAPTCHA" => "N",
		"AJAX_MODE" => "Y"
	)
);*/?>