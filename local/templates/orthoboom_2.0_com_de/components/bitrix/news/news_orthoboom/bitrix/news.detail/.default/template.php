<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$APPLICATION->SetPageProperty('title', $arResult["NAME"] . '. Orthoboom - правильная обувь.');
?>
	  <div class="container">
         <h1 class="center"><?=$arResult["NAME"]?></h1>
         <div class="span4 logo_content" style="border:0;">
		 <?
		 if($arResult["DETAIL_PICTURE"]["SRC"]){
			 $src = $arResult["DETAIL_PICTURE"]["SRC"];
		 }else{
			$src = SITE_TEMPLATE_PATH."/images/not_logo.png";
		 }
		 ?>
           <img alt="" src="<?=$src;?>" style="max-width: 267px;">
         </div>
         <div class="span8 contact" style="margin-top:0;">
		 <?if($arResult["DETAIL_TEXT"]){?>
			<p><?echo $arResult["DETAIL_TEXT"];?></p>
		<?}?>
         </div>
      </div>
