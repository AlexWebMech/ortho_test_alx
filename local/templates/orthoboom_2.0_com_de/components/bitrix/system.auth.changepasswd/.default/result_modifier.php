<?
if (isset($_REQUEST["change_pwd"])) {

	$password = trim(htmlspecialcharsbx($_REQUEST["USER_PASSWORD"]));
	$passwordConfirm = trim(htmlspecialcharsbx($_REQUEST["USER_CONFIRM_PASSWORD"]));

	if ($password == "" || $passwordConfirm == "") {
		$arResult["ERROR_MSG"] = "Введите новый пароль";
	} else if ($password != $passwordConfirm) {
		$arResult["ERROR_MSG"] = "Введенные пароли не совпадают";
	} else if (strlen($password) < 6) {
		$arResult["ERROR_MSG"] = "Пароль не может быть меньше 6 символов";
	} else {
		$rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), array("LOGIN" => $arResult["LAST_LOGIN"]));
		if ($ar_fields = $rsUsers->GetNext()) {

			$user = new CUser;
		    $fields = Array(
		      "PASSWORD"          => $password,
		      "CONFIRM_PASSWORD"  => $password,
		    );
		    if ($user->Update($ar_fields["ID"], $fields)) {
		    	$arResult["SUCCESS_MSG"] = "Пароль успешно изменен. <a style='color: inherit; text-decoration: underline;' href='/auth/'>Войти на сайт</a>";
		    } else {
		    	$arResult["ERROR_MSG"] = $user->LAST_ERROR;
		    }

		    
		}

	}
}
?>