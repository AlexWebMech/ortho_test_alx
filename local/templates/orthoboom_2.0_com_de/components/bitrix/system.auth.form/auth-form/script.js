$(function(){
	$('#auth-drop').submit(function(e){

		e.preventDefault();
	             
        var path = '/ajax/auth.php'; // объявляем путь к ajax-скрипту авторизации
        var formData = $(this).serialize(); // выдергиваем данные из формы
         
        // объявляем функцию, которая принимает данные из скрипта path
        var success = function( response ){
            if (response == 'Y')
            {
                var loc = "/personal/",
                    index = loc.indexOf('#');

                if (index > 0) {
                    loc = loc.substring(0, index);
                }
                window.location.href = loc;
            }
            else
            {
                $('#auth-drop-error').html( response ).removeClass("hidden");
            }         
        };
 
        // явно указываем тип возвращаемых данных
        var responseType = 'html';
 
        // делаем ajax-запрос
        $.post( path, formData, success, responseType );
	 
	});
});