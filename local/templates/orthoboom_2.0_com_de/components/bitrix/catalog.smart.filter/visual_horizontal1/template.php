<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

CJSCore::Init(array("fx"));

//echo $this->GetFolder().'/themes/'.$arParams["TEMPLATE_THEME"].'/colors.css';

if (file_exists($_SERVER["DOCUMENT_ROOT"].$this->GetFolder().'/themes/'.$arParams["TEMPLATE_THEME"].'/colors.css'))
	$APPLICATION->SetAdditionalCSS($this->GetFolder().'/themes/'.$arParams["TEMPLATE_THEME"].'/colors.css');

$lang = GetLang();
?>
<div class="bx_filter_horizontal bx_<?=$arParams["TEMPLATE_THEME"]?>">
	<div class="bx_filter_section m4">

		<form style="margin-top:20px" name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
			<?foreach($arResult["HIDDEN"] as $arItem):?>
				<input
					type="hidden"
					name="<?echo $arItem["CONTROL_NAME"]?>"
					id="<?echo $arItem["CONTROL_ID"]?>"
					value="<?echo $arItem["HTML_VALUE"]?>"
				/>
			<?endforeach;?>
			<?foreach($arResult["ITEMS"] as $key=>$arItem):
				$key = md5($key);
				?>
				<?if(isset($arItem["PRICE"])):?>
					<?
					if (!$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"])
						continue;
					?>
					<div class="bx_filter_container price">
						<span class="bx_filter_container_title" onclick="hideFilterProps(this)"><?=$arItem["NAME"]?></span>
						<div class="bx_filter_param_area">
							<div class="bx_filter_param_area_block"><div class="bx_input_container">
									<input
										class="min-price"
										type="text"
										name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
										id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
										value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
										size="5"
										onkeyup="smartFilter.keyup(this)"
									/>
							</div></div>
							<div class="bx_filter_param_area_block"><div class="bx_input_container">
									<input
										class="max-price"
										type="text"
										name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
										id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
										value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
										size="5"
										onkeyup="smartFilter.keyup(this)"
									/>
							</div></div>
							<div style="clear: both;"></div>
						</div>
						<div class="bx_ui_slider_track" id="drag_track_<?=$key?>">
							<div class="bx_ui_slider_range" style="left: 0; right: 0%;"  id="drag_tracker_<?=$key?>"></div>
							<a class="bx_ui_slider_handle left"  href="javascript:void(0)" style="left:0;" id="left_slider_<?=$key?>"></a>
							<a class="bx_ui_slider_handle right" href="javascript:void(0)" style="right:0%;" id="right_slider_<?=$key?>"></a>
						</div>
						<div class="bx_filter_param_area">
							<div class="bx_filter_param_area_block" id="curMinPrice_<?=$key?>"><?=$arItem["VALUES"]["MIN"]["VALUE"]?></div>
							<div class="bx_filter_param_area_block" id="curMaxPrice_<?=$key?>"><?=$arItem["VALUES"]["MAX"]["VALUE"]?></div>
							<div style="clear: both;"></div>
						</div>
					</div>

					<script type="text/javascript" defer="defer">
						var DoubleTrackBar<?=$key?> = new cDoubleTrackBar('drag_track_<?=$key?>', 'drag_tracker_<?=$key?>', 'left_slider_<?=$key?>', 'right_slider_<?=$key?>', {
							OnUpdate: function(){
								BX("<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>").value = this.MinPos;
								BX("<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>").value = this.MaxPos;
							},
							Min: parseFloat(<?=$arItem["VALUES"]["MIN"]["VALUE"]?>),
							Max: parseFloat(<?=$arItem["VALUES"]["MAX"]["VALUE"]?>),
							MinInputId : BX('<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>'),
							MaxInputId : BX('<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>'),
							FingerOffset: 8,
							MinSpace: 1,
							RoundTo: 0.01,
							Precision: 2
						});
					</script>
				<?endif?>
			<?endforeach?>

<table><tr>
<?
$arPropNamesEn = array('22' => 'Gender', '26' => 'Season', '27' => 'Type', '1899' => 'Collection');
$arPropValuesEn = array('arrFilter_22_3510096238' => 'girl', 
	'arrFilter_22_2788221432' => 'boy', 
	'arrFilter_22_4252452532' => 'unisex', 
	'arrFilter_22_3493968518' => 'man', 
	'arrFilter_22_1229515580' => 'woman', 
	'arrFilter_26_2944839123' => 'spring / autumn', 
	'arrFilter_26_1060745282' => 'winter', 
	'arrFilter_26_1212055764' => 'summer', 
	'arrFilter_27_3632373061' => 'support',
	'arrFilter_27_2322626082' => 'preventive',
	'arrFilter_1899_4074415470' => 'KIDS Permanent',
	'arrFilter_1899_2687192585' => 'KIDS Spring-Summer',
	'arrFilter_1899_3610017439' => 'DIABETES',
	'arrFilter_1899_1311059749' => 'COMFORT',
	'arrFilter_1899_958537651' => 'CORK',
	'arrFilter_1899_2806417936' => 'AIR'
);
?>
			<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
				<?if($arItem["PROPERTY_TYPE"] == "N" ):?>
					<?
					if (!$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"])
						continue;
					?>
		

					<div class="bx_filter_container price">
						<span class="bx_filter_container_title" onclick="hideFilterProps(this)"><?=$arItem["NAME"]?></span>
						<div class="bx_filter_param_area">
							<div class="bx_filter_param_area_block"><div class="bx_input_container">
								<input
									class="min-price"
									type="text"
									name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
									id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
									value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
									size="5"
									onkeyup="smartFilter.keyup(this)"
								/>
							</div></div>
							<div class="bx_filter_param_area_block"><div class="bx_input_container">
								<input
									class="max-price"
									type="text"
									name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
									id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
									value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
									size="5"
									onkeyup="smartFilter.keyup(this)"
								/>
							</div></div>
							<div style="clear: both;"></div>
						</div>
						<div class="bx_ui_slider_track" id="drag_track_<?=$key?>">
							<div class="bx_ui_slider_range" style="left: 0; right: 0%;"  id="drag_tracker_<?=$key?>"></div>
							<a class="bx_ui_slider_handle left"  href="javascript:void(0)" style="left:0;" id="left_slider_<?=$key?>"></a>
							<a class="bx_ui_slider_handle right" href="javascript:void(0)" style="right:0%;" id="right_slider_<?=$key?>"></a>
						</div>
						<div class="bx_filter_param_area">
							<div class="bx_filter_param_area_block" id="curMinPrice_<?=$key?>"><?=$arItem["VALUES"]["MIN"]["VALUE"]?></div>
							<div class="bx_filter_param_area_block" id="curMaxPrice_<?=$key?>"><?=$arItem["VALUES"]["MAX"]["VALUE"]?></div>
							<div style="clear: both;"></div>
						</div>
					</div>
					<script type="text/javascript" defer="defer">
						var DoubleTrackBar<?=$key?> = new cDoubleTrackBar('drag_track_<?=$key?>', 'drag_tracker_<?=$key?>', 'left_slider_<?=$key?>', 'right_slider_<?=$key?>', {
							OnUpdate: function(){
								BX("<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>").value = this.MinPos;
								BX("<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>").value = this.MaxPos;
							},
							Min: parseFloat(<?=$arItem["VALUES"]["MIN"]["VALUE"]?>),
							Max: parseFloat(<?=$arItem["VALUES"]["MAX"]["VALUE"]?>),
							MinInputId : BX('<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>'),
							MaxInputId : BX('<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>'),
							FingerOffset: 8,
							MinSpace: 1,
							RoundTo: 1
						});
					</script>
				<?elseif(!empty($arItem["VALUES"]) && !isset($arItem["PRICE"])):?>

				<td style="width:320px; vertical-align:top;"><div class="bx_filter_container">
					<div class="span2" style="float:initial; text-align:left;">
					<?
					if ($lang == 'en') {
						$propName = $arPropNamesEn[$arItem['ID']];
					} else if ($lang == 'de') {
						$propName = $arPropNamesEn[$arItem['ID']];
					} else {
						$propName = $arItem['NAME'];
					}
					if($arItem["CODE"] == "OBUV_SEZONNOST_1"){
						$propName = "Сезон";
					}
					if($arItem["CODE"] == "OBUV_TIP"){
						$propName = "Тип";
					}
					if($arItem["CODE"] == "TSVETOVAYA_GAMMA"){
						$propName = "Пол";
					}
					?>
						<?=$propName;/*$arItem["NAME"]*/?>
					</div>
					<span class="bx_filter_container_title" onclick="hideFilterProps(this)"><?=CATALOG_SMART_FILTER_DROPDOWN_SELECT;?></span>
					<div class="bx_filter_block" style="width:177px; white-space:nowrap; margin-left:0; <?if ($_GET["set_filter"]) echo 'display: block';?>">
						
						<?foreach($arItem["VALUES"] as $val => $ar):?>
						<div class="<?echo $ar["DISABLED"] ? 'disabled': ''?>">
							<input
								type="checkbox" <?echo $ar["CHECKED"]? 'class="hello"': ''?>
								value="<?echo $ar["HTML_VALUE"]?>"
								name="<?echo $ar["CONTROL_NAME"]?>"
								id="<?echo $ar["CONTROL_ID"]?>"
								<?echo $ar["CHECKED"]? 'checked="checked"': ''?>
								onclick="smartFilter.click(this)"
								<?if ($ar["DISABLED"]):?>disabled<?endif?>
							/>
							<label for="<?echo $ar["CONTROL_ID"]?>">
							<?
								if ($lang == 'en') {
									$propValue = $arPropValuesEn[$ar['CONTROL_ID']];
								} else if ($lang == 'de') {
									$propValue = $arPropValuesEn[$ar['CONTROL_ID']];
								} else {
									$propValue = $ar['VALUE'];
								}
							?>
								<?echo $propValue; /*$ar["VALUE"];*/?>
							</label>
						</div>
						<?endforeach;?>
						<?if ($arItem['ID'] == 1899):?>
						<div style="text-align: center; margin-top: 10px;"><a style="color: #0D6897;" href="/catalog-list/">Download catalogue</a></div>
						<?endif;?>
					</div>
				</div></td>
				<?endif;?>


			<?endforeach;?>

</tr></table>
			<div style="clear: both;"></div>
			<div class="bx_filter_control_section">
				<span class="icon"></span>

				<div class="bx_filter_popup_result" id="modef" style="<?if(!isset($arResult["ELEMENT_COUNT"])):?>display:none;<?else:?>display: inline-block;<?endif;?>top: 75px;left: 25px;right: 25px;">
					<?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
					
					<a href="<?echo $arResult["FILTER_URL"]?>"><?/*echo GetMessage("CT_BCSF_FILTER_SHOW")*/?></a>
				</div>
			</div>
		</form>
		<div style="clear: both;"></div>
	</div>
</div>

<script>

	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>');

	$(document).ready(function() {
		$('input[type=checkbox]').removeClass('outtaHere');
	});
</script>

