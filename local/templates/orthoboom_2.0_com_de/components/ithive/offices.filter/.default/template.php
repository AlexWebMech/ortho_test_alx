<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); $this->setFrameMode(true);?>
<div class="container" style="margin-top:-40px"> 
	<div class="shop_search">
		<form id="shop_search_form" class="shop_search_form" name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get">
			<?foreach($arResult["ITEMS"] as $arItem):
				if(array_key_exists("HIDDEN", $arItem)):
					echo $arItem["INPUT"];
				endif;
			endforeach;?>
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?if(!array_key_exists("HIDDEN", $arItem)):?>
					<p><?=$arItem["NAME"]?></p>
					<?=$arItem["INPUT"]?>
				<?endif?>
			<?endforeach;?>
			<input id="filter_submit" style="float:left" type="submit" name="set_filter" value="Показать" /><input type="hidden" name="set_filter" value="Y" />
		</form>
	</div>
</div>
<br> 
<script>
	// скрыть все города, где адрес - не салон Юлианна
	/*
	$('#shop_search_form select option').hide();
	$('#shop_search_form select option[value=6]').show(); // Нижний Новгород
	$('#shop_search_form select option[value=39]').show(); // Арзамас
	$('#shop_search_form select option[value=40]').show(); // Богородск
	$('#shop_search_form select option[value=42]').show(); // Городец
	$('#shop_search_form select option[value=41]').show(); // Выкса
	$('#shop_search_form select option[value=43]').show(); // Дзержинск
	$('#shop_search_form select option[value=44]').show(); // Кстово
	$('#shop_search_form select option[value=45]').show(); // Павлово
	$('#shop_search_form select option[value=46]').show(); // Саров
	$('#shop_search_form select option[value=36]').show(); // Йошкар-Ола
	$('#shop_search_form select option[value=38]').show(); // Новочебоксарск
	$('#shop_search_form select option[value=37]').show(); // Чебоксары
	$('#shop_search_form select option[value=]').show(); // Чебоксары
	*/
	
	// EOF скрыть все города, где адрес - не салон Юлианна
 
	$('#shop_search_form select option:nth-child(1)').text('Все регионы');
/*
	$('#shop_search_form select option:nth-child(2)').text('Нижний Новгород');
	$('#shop_search_form select option:nth-child(3)').text('Нижегородская область');
	$('#shop_search_form select option:nth-child(4)').text('Республика Мордовия');
	$('#shop_search_form select option:nth-child(5)').text('Республика Чувашия');
	$('#shop_search_form select option:nth-child(6)').text('Республика Марий Эл');
*/
</script>