<?$lang = GetLang();?>
<?require_once('lang_' . $lang . '.php');?>

<?if ($curPage != '/index_int.php'):?>
</div> <!-- class="content" -->
	</div> <!-- class="container" -->
<?endif;?>

		<footer <?if($curPage == '/contact.php'  ) :?> style="margin-top:-20px" <?endif;?>>
			<div class="container">
				<div class="span3">
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "sect",
							"AREA_FILE_SUFFIX" => "copy_" . $lang,
							"EDIT_TEMPLATE" => "",
							"AREA_FILE_RECURSIVE" => "Y"
						)
					);?>
				</div>
				<div class="span6">
					<?$APPLICATION->IncludeComponent(
						"bitrix:menu",
						"template2",
						Array(
							"ROOT_MENU_TYPE" => "left_" . $lang,
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => "",
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "N",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N"
						)
					);?>
					<!--
					<div style="text-align: center; margin-top:30px;">
						<a href="https://vk.com/club73756667" target="_blank" title="Мы на ВКонтакте"><img width="29" alt="Мы на ВКонтакте" src="/images/vk.png" height="29"></a>
						<a href="https://www.facebook.com/Orthoboom/" target="_blank" title="Мы на Facebook"><img width="29" alt="Мы на Facebook" src="/images/fb.png" height="29"></a>
						<a href="https://ok.ru/group/52352615120962" target="_blank" title="Мы в Одноклассниках"><img width="31" alt="Мы в Одноклассниках" src="/images/ok.png" height="30" border="0"></a>
						<a href="https://www.instagram.com/orthoboom/" target="_blank" title="Мы в Инстаграме"><img width="29" alt="Мы в Инстаграме" src="/images/instagram.png" height="29" border="0"></a>
					</div>					
					-->
				</div>
				<div class="span3 cont">
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "sect",
							"AREA_FILE_SUFFIX" => "contact_" . $lang,
							"EDIT_TEMPLATE" => "",
							"AREA_FILE_RECURSIVE" => "Y"
						)
					);?>
	
				</div>&nbsp;
			</div>
		</footer>		

        <div id="modal-callback" class="modal hide fade in"  aria-hidden="false">
		<div class="modal-body">
				<div class="callback">
					<h2><?=CALL_ME?></h2>
					<a class="close" aria-hidden="true" data-dismiss="modal">
						<img alt="" src="/images/close.png">
					</a>

					<?$APPLICATION->IncludeComponent(
	"altasib:feedback.form", 
	"template_common2", 
	array(
		"IBLOCK_TYPE" => "feedback_structured",
		"IBLOCK_ID" => "8",
		"FORM_ID" => "11",
		"EVENT_TYPE" => "ALX_FEEDBACK_FORM",
		"PROPERTY_FIELDS" => array(
			0 => "FIO",
			1 => "TEL",
		),
		"PROPERTY_FIELDS_REQUIRED" => array(
			0 => "TEL",
		),
		"NAME_ELEMENT" => "TEL",
		"BBC_MAIL" => "feedback@julianna.ru",
		"MESSAGE_OK" => FORM_MESSAGE_OK,
		"CHECK_ERROR" => "Y",
		"ACTIVE_ELEMENT" => "Y",
		"USE_CAPTCHA" => "N",
		"SEND_MAIL" => "N",
		"HIDE_FORM" => "Y",
		"USERMAIL_FROM" => "N",
		"SHOW_MESSAGE_LINK" => "Y",
		"REWIND_FORM" => "N",
		"WIDTH_FORM" => "50%",
		"SIZE_NAME" => "12px",
		"COLOR_NAME" => "#000000",
		"SIZE_HINT" => "10px",
		"COLOR_HINT" => "#000000",
		"SIZE_INPUT" => "12px",
		"COLOR_INPUT" => "#727272",
		"BACKCOLOR_ERROR" => "#ffffff",
		"COLOR_ERROR_TITLE" => "#A90000",
		"COLOR_ERROR" => "#8E8E8E",
		"IMG_ERROR" => "/upload/altasib.feedback.gif",
		"BORDER_RADIUS" => "3px",
		"COLOR_MESS_OK" => "#963258",
		"IMG_OK" => "/upload/altasib.feedback.ok.gif",
		"CATEGORY_SELECT_NAME" => FORM_CATEGORY_SELECT_NAME,
		"SECTION_MAIL_ALL" => "",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"ALX_CHECK_NAME_LINK" => "N",
		"CAPTCHA_TYPE" => "default",
		"JQUERY_EN" => "F",
		"SECTION_MAIL9" => "",
		"SECTION_MAIL11" => "",
		"SECTION_MAIL10" => "",
		"SECTION_MAIL7" => "",
		"SECTION_MAIL8" => ""
	),
	false
);?>

				</div>				
		</div>	
	</div>	

		<script type="text/javascript">
			$(document).ready(function() {
				$('.callback-form a').click(function(){
					$('#modal-callback').show();
				});
				$('.callback .close').click(function(){
					$('#modal-callback').hide();
				});
				
				
				/* $('#modal-mini .close').click(function(){
					$('#modal-mini').hide();
				}); */
							 
			});

			$('.catalog .span3').hover(function(){
					$(this).find('img').css({'opacity':'0.3','transition':'0.1s'});
					$href = $(this).find('a').attr('href');
					$(this).click(function(){ 
						window.location.href=$href;
					});
				$(this).append('<img src="/images/plus.png" class="plus">');
					$(this).find('.item-name a').css({'color':'#F86B73','borderColor':'#F86B73'});
				},
				function(){
					$(this).find('img').css({'opacity':'1'});	
					$('.plus').remove();
					$(this).find('.item-name a').css({'color':'#0C6695','borderColor':'#BBCBE8'});
			});
	
		</script>

<script>
// Кнопка ВВЕРХ
$(function(){
	$.fn.scrollToTop = function(){
		$(this).hide().removeAttr("href");
		if ($(window).scrollTop() >= "280") $(this).fadeIn("slow")
		var scrollDiv = $(this);
		$(window).scroll(function(){
			if ($(window).scrollTop() <= "280") $(scrollDiv).fadeOut("slow") /* 280 = эти значения лучше ставить одинаковыми, овтечают за появление иконки для прокрутки */
			else $(scrollDiv).fadeIn("slow")
		});
		$(this).click(function(){
			$("html, body").animate({scrollTop: 0}, 1000) /* 1000 = Задаём скорость прокрутки */
		})
	}
});
$(function(){
	$("#Go_Top").scrollToTop();
});
</script>	

<script>
/* 	$('.for-submit input').click(function(){
		$(this).parent().parent().find('p').hide();
	}); */
</script>
	
<a href='#' id='Go_Top'><img class="img-responsive" src="/images/up-arrow-icon-top.png" alt="Наверх" title="Наверх"></a>



<?
/* if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
 */?>

	</body>
</html>