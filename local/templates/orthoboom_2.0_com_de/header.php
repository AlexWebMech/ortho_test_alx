<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$lang =  GetLang(); // defined in init.php
require_once('lang_' . $lang . '.php');
	
$curPage = $APPLICATION->GetCurPage(true);

?><!DOCTYPE html>
<html>
<!--[if lte IE 9]> <html class="ie8"> <![endif]-->
	<head>

		<title><?$APPLICATION->ShowTitle();?></title>
		
		<meta name="google-site-verification" content="Y5qrPJSwzEB1WN3uj5J1n9SuVegkF42IwSQC1xGrCZo" />
		
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.jpg" /> 
		<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700&amp;subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>	
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
		<!--
		<link rel="stylesheet" href="/css/bootstrap.css">	
		-->
		
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		
		<?/*if($curPage != '/opt/index.php'  ) :*/?>		
		<link rel="stylesheet" href="/css/select-and-radio.css">	
		<?/*endif;*/?>
		<link rel="stylesheet" href="/css/jquery.lightbox-0.5.css">
		<!--<link rel="stylesheet" href="//cdn.callbackhunter.com/widget2/tracker.css"> -->
		
		<link href="/css/cloud-zoom.css" rel="stylesheet" type="text/css" />
		
		<!--
     	<script src="/js/bootstrap.js" type="text/javascript"></script>	
		-->
		
		<script type="text/javascript" src="/js/cloud-zoom.1.0.2.js"></script>	
		
		
		<!-- fancybox -->
		<script src="<?=SITE_TEMPLATE_PATH?>/js/fancy/jquery.fancybox.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/fancy/helpers/jquery.fancybox-media.js"></script>
		<link href="<?=SITE_TEMPLATE_PATH?>/js/fancy/jquery.fancybox.css" rel="stylesheet" />
		<!-- fancybox -->


		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.matchHeight-min.js"></script> 

		<style type="text/css">	
			#owl-demo .item img{
				display: block;
				width: 100%;
				height: auto;
			}
		</style>

		<link href="/css/owl.carousel.css" rel="stylesheet">
		<link href="/css/owl.theme.css" rel="stylesheet">
		<script src="/js/owl.carousel.js"></script>	
		
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/select.js"></script>
       	<script src="/js/select-and-radio.js" type="text/javascript"></script>	
		
		<script src="/js/jquery.inputmask.min.js" type="text/javascript"></script>
		<script src="/js/inputmask.min.js" type="text/javascript"></script>
       	<script src="/js/jquery.lightbox-0.5.js" type="text/javascript"></script>
		<!-- <script type="text/javascript" src="/js/script.js"></script> -->
		<?/*?><script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/script.js"></script><?*/?>
		<?$APPLICATION->ShowHead();?>
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css">
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/select.css">	


		<?
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.scrollbar.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.maskedinput.min.js");
		if (strpos($curPage, '/personal/') !== false)
			$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/script2.0.js");
		?>

		<?
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/bootstrap.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/select-and-radio.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery.lightbox-0.5.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery.scrollbar.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/styles.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/cabinet.css");
		?>
		<?
		$langReal = GetLangReal();
		if($langReal == 'en'):?>
		<!-- Yandex.Metrika counter -->
		<script type="text/javascript" >
		   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
		   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
		   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

		   ym(53181178, "init", {
				clickmap:true,
				trackLinks:true,
				accurateTrackBounce:true,
				webvisor:true
		   });
		</script>
		<noscript><div><img src="https://mc.yandex.ru/watch/53181178" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->
		<?endif;?>
		<?if($langReal == 'de'):?>
		<!-- Yandex.Metrika counter -->
		<script type="text/javascript" >
		   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
		   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
		   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

		   ym(53181193, "init", {
				clickmap:true,
				trackLinks:true,
				accurateTrackBounce:true,
				webvisor:true
		   });
		</script>
		<noscript><div><img src="https://mc.yandex.ru/watch/53181193" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->
		<?endif;?>
</head>
	<body>
	<? SetCookie();  ?> 
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>

		<header>
			<div class="container">
				<div class="span3 logo">	
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "sect",
							"AREA_FILE_SUFFIX" => "logo_" . $lang,
							"EDIT_TEMPLATE" => "",
							"AREA_FILE_RECURSIVE" => "Y"
						)
					);?>
				</div>
				
				<div class="span6" style="line-height:1"><br><br>
					<?
$APPLICATION->IncludeComponent(
	"bitrix:search.title", 
	"template1", 
	array(
		"NUM_CATEGORIES" => "1",
		"TOP_COUNT" => "5",
		"ORDER" => "date",
		"USE_LANGUAGE_GUESS" => "Y",
		"CHECK_DATES" => "N",
		"SHOW_OTHERS" => "N",
		"PAGE" => "/search/",
		"SHOW_INPUT" => "Y",
		"INPUT_ID" => "title-search-input",
		"CONTAINER_ID" => "title-search",
		"CATEGORY_0_TITLE" => "Товары",
		"CATEGORY_0" => array(
			0 => 'iblock_catalog',
		),
		"COMPONENT_TEMPLATE" => "template1",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"/*,
		"CATEGORY_0_iblock_catalog" => array(
			0 => "5",
		),
		"CATEGORY_0_iblock_1c_catalog" => array(
		)*/
	),
	false
);?>
				</div>

					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "sect",
							"AREA_FILE_SUFFIX" => "tel_" . $lang,
							"EDIT_TEMPLATE" => "",
							"AREA_FILE_RECURSIVE" => "Y"
						)
					);?>
							
			</div>	
		</header>
		<div class="menu">
			<div class="container">
				<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"template1", 
	array(
		"ROOT_MENU_TYPE" => "top_" . $lang,
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "template1",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
			</div>
		</div>
<?	
$contentClass = '';	
if ($curPage == '/catalog/index.php') $contentClass = ' green-background';	
?>	
<?if ($curPage != '/index.php'):?>
<div class="content<?=$contentClass?>">
	<div class="container">
<?endif;?>