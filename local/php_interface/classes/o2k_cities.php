<?php

namespace o2k;

use Bitrix\Main\Text\Encoding;
use Bitrix\Iblock;
use Bitrix\Main;


class IPGeoBase
{

    private static $IP_RANGE_HLBLOCK_ID = 7;
    private static $CITY_IBLOCK_ID = 98;

    public static function UpdateCitiesList($filePath, $iblockId = false) {
        if(!$iblockId) $iblockId = self::$CITY_IBLOCK_ID;
        $fileHandler = fopen($filePath, 'r') or die("Cannot open $filePath");

        $result = array();

        rewind($fileHandler);
        while(!feof($fileHandler))
        {
            $str = fgets($fileHandler);
            $arRecord = explode("\t", trim($str));
            $result[] = array(
                'id' => $arRecord[0],
                'city' => $arRecord[1],
                'region' => $arRecord[2],
                'district' => $arRecord[3],
                'lat' => $arRecord[4],
                'lng' => $arRecord[5]
            );
        }

        //  чистим инфоблок
        \Bitrix\Main\Loader::includeModule('iblock');
        $arSelect = array("ID");
        $arFilter = array("IBLOCK_ID" => $iblockId);
        $res = \CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        while ($r = $res->GetNext())
            \CIBlockElement::Delete($r['ID']);

        $el = new \CIBlockElement;
        foreach($result as $arResult) {

            $arElement = Array(
                "IBLOCK_SECTION_ID" => false,
                "IBLOCK_ID"      => $iblockId,
                "PROPERTY_VALUES"=> array(
                    "CITY_ID" => $arResult['id'],
                    "REGION" => $arResult['region'],
                    "DISTRICT" => $arResult['district'],
                    "LAT" => $arResult['lat'],
                    "LON" => $arResult['lng'],
                ),
                "NAME"           => $arResult['city'],
                "CODE"           => $arResult['id'],
                "ACTIVE"         => "Y",
            );
            $el->Add($arElement);
        }
        return false;
    }

    public static function UpdateIPRangesList($filePath, $hlBlock = false, $cityIblockId = false) {
        if(!$hlBlock) $hlBlock = self::$IP_RANGE_HLBLOCK_ID;
        if(!$cityIblockId) $cityIblockId = self::$CITY_IBLOCK_ID;
        $fileHandler = fopen($filePath, 'r') or die("Cannot open $filePath");
        $result = array();
        rewind($fileHandler);
        $arCities = array();
        //выгружаем данные
        while(!feof($fileHandler))
        {
            $str = fgets($fileHandler);
            $arRecord = explode("\t", trim($str));
            if($arRecord[3]!='RU') continue;
            $result[] = array(
                'start' => $arRecord[0],
                'end' => $arRecord[1],
                'range' => $arRecord[2],
                'country_code' => $arRecord[3],
                'city_id' => $arRecord[4]
            );
            if(IntVal($arRecord[4])) $arCities[$arRecord[4]] = true;
        }

        if(count($arCities)==0) die('Нет данных по городам');

        $arCitiesNames = array();
        //получаем данные по городам из выгруженного
        $arFilter = array("IBLOCK_ID"=>$cityIblockId, "PROPERTY_CITY_ID" => array_keys($arCities));
        $res = \CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, Array("ID", "NAME", "CODE", "PROPERTY_CITY_ID"));
        while($ar_fields = $res->GetNext())
        {
            $arCitiesNames[$ar_fields['PROPERTY_CITY_ID_VALUE']] = array('NAME' => $ar_fields["NAME"], 'CODE' => $ar_fields["CODE"]);
        }
        //загружаем их в hl-блок
        \CModule::IncludeModule("highloadblock");
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlBlock)->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();
        foreach($result as $arResult) {
            if(!IntVal($arResult['city_id'])) continue;
            if($arResult['country_code']!='RU') continue;
            $currentCity = $arCitiesNames[$arResult['city_id']];
            $arElement = Array(
                "UF_IP_START" => $arResult['start'],
                "UF_IP_END" => $arResult['end'],
                "UF_RANGE" => $arResult['range'],
                "UF_CITY_CODE" => $arResult['city_id'],
                "UF_CITY_ID" => $arResult['city_id'],
                "UF_CITY_NAME" => $currentCity['NAME'],
            );
            $entityClass::add($arElement);
        }
    }

    public static function GetCityByIP($ip, $hlBlock=false) {
        if(!$hlBlock) $hlBlock = self::$IP_RANGE_HLBLOCK_ID;

        if(!$ip) return false;
        $ip = sprintf('%u', ip2long($ip));

        $obCache = new \CPHPCache();
        $cacheId = $ip;
        //кеш на целый год
        if($obCache->InitCache(60*60*24*365, $cacheId, "/")) {
            $vars = $obCache->GetVars();
            $result = $vars['city'];
        }
        elseif( $obCache->StartDataCache()) {

            \CModule::IncludeModule("highloadblock");
            $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlBlock)->fetch();
            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
            $entityClass = $entity->getDataClass();

            $arFilter = Array(
                "<UF_IP_START" => $ip,
                ">UF_IP_END" => $ip
            );
            $rsData = $entityClass::getList(array(
                'select' => array('UF_CITY_NAME', 'UF_CITY_ID', 'UF_CITY_CODE'),
                'filter' => $arFilter
            ));

            if ($el = $rsData->fetch()) {
                $result = array(
                    'ID'   => $el['UF_CITY_ID'],
                    'CODE'   => $el['UF_CITY_CODE'],
                    'NAME' => $el['UF_CITY_NAME'],
                );
            } else $result = false;

            $obCache->EndDataCache(array('city' => $result));
        }
        return $result;
    }

    public static function GetDefaultCity() {
        //Get item from Moscow IP range
        return self::GetCityByIP("178.219.186.12");
    }

    public static function GetCities($cityIblockId = false) {
        if(!$cityIblockId) $cityIblockId = self::$CITY_IBLOCK_ID;

        $obCache = new \CPHPCache();
        $cacheId = "";
        //кеш на целый год
        if($obCache->InitCache(60*60*24*365, $cacheId, "/")) {
            $vars = $obCache->GetVars();
            $result = $vars['cities'];
        }
        elseif( $obCache->StartDataCache()) {

            \CModule::IncludeModule("iblock");

            $arFilter = array("IBLOCK_ID"=>$cityIblockId, "!PROPERTY_DISPLAY_IN_LIST" => false);
            $res = \CIBlockElement::GetList(Array("SORT"=>"ASC", "NAME" => "ASC"), $arFilter, false, false, Array("ID", "NAME", "CODE", "PROPERTY_CITY_ID"));
            while($ar_fields = $res->GetNext())
            {
                $result[] = array('NAME' => $ar_fields["NAME"], 'CODE' => $ar_fields["CODE"], 'ID' => $ar_fields['PROPERTY_CITY_ID_VALUE']);
            }

            $obCache->EndDataCache(array('cities' => $result));
        }
        return $result;
    }
}