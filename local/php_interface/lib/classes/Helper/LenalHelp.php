<?php

namespace Lib\Classes\Helper;

use Bitrix\Main\Application;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity\Query;
use Bitrix\Main\UserTable;
use Bitrix\Main\Web\Json;
use Bitrix\Main\Localization\Loc;
use Bitrix\Iblock\SectionTable;
use Bitrix\Iblock\PropertyEnumerationTable;
use Bitrix\Main\Loader;
use CDBResult;
use Bitrix\Conversion\Internals\MobileDetect;
use Bitrix\Main\UserGroupTable;
use CIBlockSection;
use CMain;
use Bitrix\Iblock\IblockTable;

class LenalHelp
{

    public static function getHighLoadBlockList($hlbl, $arOrder = [], $filter = [], $arSelect = ['*'], $limit = null)
    {
        $hlblock = HL\HighloadBlockTable::getList(
            array("filter" => array('TABLE_NAME' => $hlbl))
        )->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $Query = new Query($entity);

        $Query->setSelect($arSelect);
        $Query->setFilter($filter);
        $Query->setOrder($arOrder);
        $Query->setLimit($limit);

        $result = $Query->exec();

        $arResult = new CDBResult($result);
        return $arResult;
    }

    public static function addToHighloadBlock($hlbl, $data = array(), $unical = '')
    {
        $res = null;
        $id = 0;
        if (!empty($unical)) {
            $res = self::getHighLoadBlockList(
                $hlbl, array(), array($unical => $data[$unical]), array('ID')
            )->Fetch();
        }

        if (!empty($data)) {
            $hlblock = HL\HighloadBlockTable::getList(
                array("filter" => array('TABLE_NAME' => $hlbl))
            )->fetch();
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entityDataClass = $entity->getDataClass();

            if ($res != null) {
                $id = $res["ID"];
                $entityDataClass::update($res["ID"], $data);
            } else {
                $id = $entityDataClass::add($data);
                return $id->getId();
            }
        }

        if ($id > 0) {
            return true;
        }
    }

    /** deleteFromHighloadBlock - пїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ пїЅ hl пїЅпїЅпїЅпїЅпїЅ
     * @param $hlbl - пїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅ hl пїЅпїЅпїЅпїЅпїЅ
     * @param $id - ID пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ
     * @return $result;
     */
    public static function deleteFromHighloadBlock($hlbl, $id)
    {
        $hlblock = HL\HighloadBlockTable::getList(
            array("filter" => array('TABLE_NAME' => $hlbl))
        )->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityDataClass = $entity->getDataClass();
        $result = $entityDataClass::delete($id);

        return $result;
    }

    public static function superpre($arr)
    {
        global $USER;
        if ($USER->isAdmin()) {
            echo '<pre style="color:red; font-style: italic; font-size: 12px;">';
            print_r($arr);
            echo '</pre>';
        }
    }

    public static function print_c(array $param)
    {

        $json = (!empty($param)) ? true : null;

        if ($json) {
            echo "<script> console.log('" . Json::encode($param) . "')</script>";
        } else {
            return $json;
        }

    }

    public static function img($id, $width, $height, $resize_type = "BX_RESIZE_IMAGE_PROPORTIONAL", $waterMark = false)
    {
        $arWaterMark = array();
        if ($waterMark) {
            $arWaterMark = [
                [
                    "name" => "watermark",
                    "position" => "center", // пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ
                    "alpha_level" => 20,
                    "type" => "image",
                    "size" => "real",
                    "file" => $_SERVER["DOCUMENT_ROOT"] . '/local/templates/fame/img/logo_b.svg', // пїЅпїЅпїЅпїЅ пїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ
                    "fill" => "exact",
                ]
            ];
        }
        if (is_numeric($id) && !empty($id)) {
            $resize_img = \CFile::ResizeImageGet(
                $id, array("width" => $width, "height" => $height), $resize_type, false, $arWaterMark
            );
        } else {
            $resize_img['src'] = $id;
        }
        $path = SITE_TEMPLATE_PATH . "/img/logo_b.svg";

        if (empty($resize_img['src']) && file_exists($_SERVER["DOCUMENT_ROOT"] . $path)) {
            $resize_img['src'] = $path;
        }
        return $resize_img['src'];
    }

    public static function declensionWord($number, $word)
    {
        $ar = array(2, 0, 1, 1, 1, 2);
        return $word[($number % 100 > 4 && $number % 100 < 20) ? 2 : $ar[min($number % 10, 5)]];
    }

    public static function getIblockIdByCode($iblockCode)
    {
        $cache = new \CPHPCache;
        $cache_time = 3600;
        $cache_id = 'get_id_' . $iblockCode;
        if ($cache->InitCache($cache_time, $cache_id, '/' . SITE_ID . '/iblock/helper/')) {
            $arVars = $cache->GetVars();
            return $arVars['DATA']['ID'];
        } else {
            $cache->StartDataCache($cache_time, $cache_id);
            $arIblock = IblockTable::getList([
                'filter' => ['CODE' => $iblockCode],
                'select' => ['ID']
            ])->fetch();
            if ($arIblock) {
                $cache->EndDataCache(['DATA' => $arIblock]);
                return $arIblock['ID'];
            } else {
                return false;
            }
        }
    }

    public static function getTitle()
    {
        global $APPLICATION;
        preg_match('/(ortopedicheskaya_i_komfortnaya_obuv_ortopedicheskaya_obuv_vzroslaya)/',
            $APPLICATION->GetCurPage(), $matchesMature, PREG_OFFSET_CAPTURE);
        if ($matchesMature) {
            return $APPLICATION->SetTitle(GetMessage('MATURE_SHOES'));
        }
    }

    /**
     * get all sizes and length from trade offers
     *
     * @param $arResult
     * @param $arrFilter_checked
     * @param $size
     * @param $lenght
     * @return mixed
     */
    public static function getSizeAndLendgth($arResult, $arrFilter_checked, $size, $lenght)
    {
        $cache = new \CPHPCache;
        $cache_time = 31536000;
        $cache_id = 'get_size_and_length';
        if ($cache->InitCache($cache_time, $cache_id, '/' . SITE_ID . '/iblock/helper/' . $arrFilter_checked . '/')) {
            $arVars = $cache->GetVars();
            return $arVars['DATA'];
        } else {
            $cache->StartDataCache($cache_time, $cache_id);

            $curSize = [];
            $curLength = [];
            foreach ($arResult as $iblock) {
                if (self::getIblockIdByCode('catalog_erp3') == (int)$iblock["IBLOCK_ID"]) {
                    if ((int)$iblock["IBLOCK_ID"] <= 0) {
                        return;
                    }

                    $arSelect = ["ID", "IBLOCK_ID"];
                    $arFilter = ["IBLOCK_ID" => IntVal($iblock["IBLOCK_ID"]), "ACTIVE" => "Y"];
                    $res = \CIBlockElement::GetList([], $arFilter, false, [], $arSelect);
                    while ($ob = $res->GetNextElement()) {
                        $arFields = $ob->GetFields();
                        $offers = \CCatalogSKU::getOffersList(
                            $arFields['ID'],
                            0,
                            ['ACTIVE' => 'Y'],
                            ['NAME'],
                            ["CODE" => ['OBUV_RAZMER', 'DLINA_STELKI_SM']]
                        )[$arFields['ID']];
                        foreach ($offers as $props) {
                            if ($props["PROPERTIES"][$lenght]["VALUE"]
                                && $props["PROPERTIES"][$size]["VALUE"]
                            ) {
                                if (!in_array($props["PROPERTIES"][$size]["VALUE"],
                                    $curSize[$props["PROPERTIES"][$lenght]["VALUE"]])) {
                                    $curSize[$props["PROPERTIES"][$lenght]["VALUE"]][] =
                                        $props["PROPERTIES"][$size]["VALUE"];
                                }
                            }
                        }
                    }
                }
            }

            foreach ($arResult as $iblock) {
                if (self::getIblockIdByCode('catalog_erp3') == (int)$iblock["IBLOCK_ID"]) {
                    if ((int)$iblock["IBLOCK_ID"] <= 0) {
                        return;
                    }

                    $arSelect = ["ID", "IBLOCK_ID"];
                    $arFilter = ["IBLOCK_ID" => IntVal($iblock["IBLOCK_ID"]), "ACTIVE" => "Y"];
                    $res = \CIBlockElement::GetList([], $arFilter, false, [], $arSelect);
                    while ($ob = $res->GetNextElement()) {
                        $arFields = $ob->GetFields();
                        $offers = \CCatalogSKU::getOffersList(
                            $arFields['ID'],
                            0,
                            ['ACTIVE' => 'Y'],
                            ['NAME'],
                            ["CODE" => ['OBUV_RAZMER', 'DLINA_STELKI_SM']]
                        )[$arFields['ID']];
                        foreach ($offers as $props) {
                            if ($props["PROPERTIES"][$lenght]["VALUE"]
                                && $props["PROPERTIES"][$size]["VALUE"]
                            ) {
                                if (!in_array($props["PROPERTIES"][$lenght]["VALUE"],
                                    $curLength[$props["PROPERTIES"][$size]["VALUE"]])) {
                                    $curLength[$props["PROPERTIES"][$size]["VALUE"]][] =
                                        $props["PROPERTIES"][$lenght]["VALUE"];
                                }
                            }
                        }
                    }
                }
            }
            $whatUnset['size'] = $curSize;
            $whatUnset['length'] = $curLength;

            $cache->EndDataCache(['DATA' => $whatUnset]);
            return $whatUnset;
        }
    }


    /**
     * show appropriate length with filtered size (unset others)
     *
     * @param $arResult
     * @param $arrFilter_1
     * @param $arrFilter_2
     * @param $lengthOrSize
     * @return array|void
     */
    public static function unsetFilters($arResult, $arrFilter_1, $arrFilter_2, $lengthOrSize)
    {
        $whatUnset = [];
        $curValuesChecked = [];
        $unsetLengthOrSize = [];
        $arValues = [];
        foreach ($arResult as $items) {
            foreach ($items["VALUES"] as $key => $item) {
                $arValues[$key] = $item;
            }
        }

        foreach ($arValues as $key => $item) {
            if ($item["CHECKED"] && $item["CONTROL_NAME_ALT"] == "arrFilter_" . $arrFilter_1) {
                $curValuesChecked[$item["FACET_VALUE"]] = $item["VALUE"];
            }
        }

        if (!$curValuesChecked) {
            return;
        }

        $curCheckVals = [];
        $curCheckVals = array_unique($curCheckVals);
        foreach ($curValuesChecked as $curValuesCheckedKey => $curValuesCheckedVal) {
            foreach ($lengthOrSize as $lengthOrSizeKey => $lengthOrSizeVal) {
                foreach ($lengthOrSizeVal as $lengthOrSizeKeyItem => $lengthOrSizeValItem) {
                    $curCheckVals[] = $lengthOrSize[$curValuesCheckedVal][$lengthOrSizeKeyItem];
                }
            }
        }

        foreach ($curValuesChecked as $curValuesCheckedKey => $curValuesCheckedVal) {
            foreach ($lengthOrSize as $lengthOrSizeKey => $lengthOrSizeVal) {
                foreach ($lengthOrSizeVal as $lengthOrSizeKeyItem => $lengthOrSizeValItem) {
                    if (!in_array($lengthOrSizeValItem, $curCheckVals)) {
                        $unsetLengthOrSize[] = $lengthOrSizeValItem;
                    }
                }
            }
        }


        foreach ($unsetLengthOrSize as $itemLengthOrSize) {
            foreach ($arValues as $key => $item) {
                if ($itemLengthOrSize == $item["VALUE"] && $item["CONTROL_NAME_ALT"] == "arrFilter_" . $arrFilter_2) {
                    $whatUnset[$key] = $item["FACET_VALUE"];
                }
            }
        }
        return $whatUnset;
    }

    /**
     * swap dlina_stelki_sm and $obuv_razmer with $obuv_podkladka and $obuv_materialverkha
     *
     * @param $arResult
     * @return array
     */
    public static function swapFilterVals($arResult)
    {
        $keys = array_keys($arResult);
        /*echo "<pre>";
        print_r($arResult);
        echo "</pre>";*/
        $obuv_podkladka = array_search(OBUV_PODKLADKA, $keys);
        $dlina_stelki_sm = array_search(ARRFILTER_LENGTH, $keys);
        $obuv_materialverkha = array_search(OBUV_MATERIALVERKHA, $keys);
        $obuv_razmer = array_search(ARRFILTER_SIZE, $keys);

        if ($obuv_podkladka !== false && $dlina_stelki_sm !== false
            && $obuv_materialverkha !== false && $obuv_razmer !== false) {
            $keys[$obuv_razmer] = OBUV_MATERIALVERKHA;
            $keys[$obuv_materialverkha] = ARRFILTER_SIZE;
            $keys[$obuv_podkladka] = ARRFILTER_LENGTH;
            $keys[$dlina_stelki_sm] = OBUV_PODKLADKA;
        }

        $swap = [];

        foreach ($keys as $key) {
            $swap[$key] = $arResult[$key];
        }
        return $swap;
    }

    /**
     * checked Filter Values
     *
     * @param $arResult
     * @param $arrFilter_val
     * @return bool
     */
    public static function checkedFilterVal($arResult, $arrFilter_val)
    {
        $checkedFilterLength = false;
        foreach ($arResult as $items) {
            foreach ($items["VALUES"] as $key => $item) {
                if ($item["CHECKED"] && $item["CONTROL_NAME_ALT"] == "arrFilter_" . $arrFilter_val) {
                    $checkedFilterLength = true;
                }
            }
        }
        return $checkedFilterLength;
    }
}