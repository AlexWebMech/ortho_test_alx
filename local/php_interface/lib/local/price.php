<?
namespace Local;

$GLOBALS["UNI"]["PRICE_CALCULATOR"] = [
	"IBLOCK_IDS" => [94],
	"SKU_IBLOCK_IDS" => [95],
	"ELEMENT_SELECT" => ["PROPERTY_FIKSIROVANNAYA_TSENA_ZNACHENIE"],
	"PROPERTIES" => [
		"MIN_BASE_PRICE" => ["CATALOG_GROUP_IDS" => [91], "TYPE" => "MIN_BASE_PRICE"],
		"MIN_NORMAL_PRICE" => ["CATALOG_GROUP_IDS" => [91], "TYPE" => "MIN_PRICE"],
		"MAX_NORMAL_PRICE" => ["CATALOG_GROUP_IDS" => [91], "TYPE" => "MAX_PRICE"],
	],
	"DIFF_PROPERTIES" => [
	//	"DISCOUNT_PERCENT" => ["PROPERTY_1", "PROPERTY_2"],
	],
];

$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler(
    "iblock",
    "OnAfterIBlockElementAdd",
    "\Local\PriceCalculator::OnCatalogElementSave"
);

$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler(
    "iblock",
    "OnAfterIBlockElementUpdate",
    "\Local\PriceCalculator::OnCatalogElementSave"
);

$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler(
    "catalog",
    "\Bitrix\Catalog\Price::OnAfterAdd",
    "\Local\PriceCalculator::OnCatalogElementPriceSave"
); 

$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler(
    "catalog",
    "\Bitrix\Catalog\Price::OnAfterUpdate",
    "\Local\PriceCalculator::OnCatalogElementPriceSave"
); 

class PriceCalculator
{
	function OnCatalogElementSave($arElement)
	{
		
		if ($arElement["ID"] && $arElement["IBLOCK_ID"])
			if (in_array($arElement["IBLOCK_ID"], $GLOBALS["UNI"]["PRICE_CALCULATOR"]["IBLOCK_IDS"]))
				self::addCalcElement($arElement["ID"]);
			elseif (in_array($arElement["IBLOCK_ID"], $GLOBALS["UNI"]["PRICE_CALCULATOR"]["SKU_IBLOCK_IDS"]))
				self::addCalcElement($arElement["ID"]);
				
		if ($arElement["ID"] && ! $arElement["IBLOCK_ID"])
			self::addCalcElement($arElement["ID"]);
	}
	
	function OnCatalogElementPriceSave($event)
	{
		$arPrice = $event->getParameters()["fields"];
		if ($arPrice["PRODUCT_ID"])
			self::addCalcElement($arPrice["PRODUCT_ID"]);
	}
	
	function addCalcElement($iElementID)
	{
		if (! $iElementID)
			return;
		//self::calcElements($iElementID);
		
		$GLOBALS["UNI"]["PRICE_CALCULATOR_ELEMENT_IDS"][] = $iElementID;
		
		//Init
		static $bInited;
		if ($bInited)
			return;

		\Bitrix\Main\Application::getInstance()->addBackgroundJob([__CLASS__, "calcBufferedElements"]);
		register_shutdown_function([__CLASS__, "calcBufferedElements"]);
		$bInited = true;
	}
	
	function calcBufferedElements()
	{
		if (! is_array($GLOBALS["UNI"]["PRICE_CALCULATOR_ELEMENT_IDS"]) || ! count($GLOBALS["UNI"]["PRICE_CALCULATOR_ELEMENT_IDS"]))
			return;
		self::calcElements($GLOBALS["UNI"]["PRICE_CALCULATOR_ELEMENT_IDS"]);
		$GLOBALS["UNI"]["PRICE_CALCULATOR_ELEMENT_IDS"] = [];
	}
	
	// /Local/PriceCalculator::recalcAllElements();
	function recalcAllElements($arSettings)
	{
		\Bitrix\Main\Loader::includeModule("iblock");
		
		//$obRes = \CIBlockElement::GetList(["ID" => "ASC"], ["IBLOCK_ID" => $GLOBALS["UNI"]["PRICE_CALCULATOR"]["IBLOCK_IDS"]], false, false, ["ID"]);

		$arQuery = [];
		$arQuery["filter"] = ["IBLOCK_ID" => $GLOBALS["UNI"]["PRICE_CALCULATOR"]["IBLOCK_IDS"]];
		$arQuery["order"] = ["ID" => "ASC"];
		if ($arSettings["LIMIT"] > 0)
			$arQuery["limit"] = $arSettings["LIMIT"];
		if ($arSettings["OFFSET"] > 0)
			$arQuery["offset"] = $arSettings["OFFSET"];
		$arQuery["select"] = ["ID"];
		
		$obRes = \Bitrix\IBlock\ElementTable::getList($arQuery);
		while ($a = $obRes->fetch())
			self::addCalcElement($a["ID"]);
		
		self::calcBufferedElements();
	}
	
	public function recalcAllElementsByCron()
	{
		\Bitrix\Main\Loader::includeModule("iblock");
		
		$arQuery = [];
		$arQuery["filter"] = ["IBLOCK_ID" => $GLOBALS["UNI"]["PRICE_CALCULATOR"]["IBLOCK_IDS"]];
		$arQuery["order"] = ["ID" => "ASC"];
		$arQuery["select"] = ["ID"];
		
		$arCatalogElementIDs = [];
		$obRes = \Bitrix\IBlock\ElementTable::getList($arQuery);
		while ($a = $obRes->fetch())
		{
			//vard($a);
			$arCatalogElementIDs[] = $a["ID"];
			if (count($arCatalogElementIDs) >= 30)
			{
				self::addCalcElementsAgent($arCatalogElementIDs);
				$arCatalogElementIDs = [];
			}
		}
		if ($arCatalogElementIDs)
			self::addCalcElementsAgent($arCatalogElementIDs);
		
		return "\Local\PriceCalculator::recalcAllElementsByCron();";
	}
	
	
	
	public function addCalcElementsAgent($arElementIDs)
	{
		if (is_numeric($arElementIDs))
			$arElementIDs = [$arElementIDs];
		if (! $arElementIDs)
			return;
		
		$arAgent = [
			"FUNCTION" => '\Local\PriceCalculator::calcElements([' . implode(", ", $arElementIDs) . ']);',
		];
		
		$a = \CAgent::AddAgent($arAgent["FUNCTION"]);
		
		if (! $a)
			echo "Err";
		/*
		if ($a)
			print_r($a);
		else
			echo "Err";
		//print_r($a);
		*/
	}
	
	
	
	//function CalcElement($iElementID)
	function calcElements($arElementIDs)
	{
		if (is_numeric($arElementIDs))
			$arElementIDs = [$arElementIDs];
		if (! $arElementIDs)
			return;
		
		\Bitrix\Main\Loader::includeSharewareModule("uni.data");
		\Bitrix\Main\Loader::includeModule("iblock");
		\Bitrix\Main\Loader::includeModule("sale");
		
		$arResult["ELEMENTS"] = [];
		
		$arElementSelect = ["ID", "IBLOCK_ID", "PROPERTY_CML2_LINK"];
		if (is_array($GLOBALS["UNI"]["PRICE_CALCULATOR"]["ELEMENT_SELECT"]) && count($GLOBALS["UNI"]["PRICE_CALCULATOR"]["ELEMENT_SELECT"]))
			$arElementSelect = array_merge($arElementSelect, $GLOBALS["UNI"]["PRICE_CALCULATOR"]["ELEMENT_SELECT"]);
		
		//Load
		$obRes = \CIBlockElement::GetList([], ["ID" => $arElementIDs, "ACTIVE" => ""], false, false, $arElementSelect);
		while ($a = $obRes->fetch())
			$arResult["ELEMENTS"][$a["ID"]] = $a;
		
		//Load Catalog Elements
		$arCatalogElementIDs = [];
		foreach($arResult["ELEMENTS"] as $arElement)
			if (in_array($arElement["IBLOCK_ID"], $GLOBALS["UNI"]["PRICE_CALCULATOR"]["SKU_IBLOCK_IDS"]) && $arElement["PROPERTY_CML2_LINK_VALUE"])
				$arCatalogElementIDs[$arElement["PROPERTY_CML2_LINK_VALUE"]] = $arElement["PROPERTY_CML2_LINK_VALUE"];
			
		$arCatalogElementIDs = array_diff_key($arCatalogElementIDs, $arResult["ELEMENTS"]);
		if ($arCatalogElementIDs)
		{
			$obRes = \CIBlockElement::GetList([], ["ID" => $arElementIDs, "!ID" => array_keys($arResult["ELEMENTS"]), "ACTIVE" => ""], false, false, $arElementSelect);
			while ($a = $obRes->fetch())
				$arResult["ELEMENTS"][$a["ID"]] = $a;			
		}
		
		//Load Offers
		$arCatalogElementIDs = [];
		foreach($arResult["ELEMENTS"] as $arElement)
			if (in_array($arElement["IBLOCK_ID"], $GLOBALS["UNI"]["PRICE_CALCULATOR"]["IBLOCK_IDS"]))
				$arCatalogElementIDs[$arElement["ID"]] = $arElement["ID"];

		if ($arCatalogElementIDs)
		{
			$obRes = \CIBlockElement::GetList([], ['=AVAILABLE' => 'Y', "IBLOCK_ID" => $GLOBALS["UNI"]["PRICE_CALCULATOR"]["SKU_IBLOCK_IDS"], "PROPERTY_CML2_LINK" => $arCatalogElementIDs, "!ID" => array_keys($arResult["ELEMENTS"]), "ACTIVE" => ""], false, false, $arElementSelect);
			while ($a = $obRes->fetch())
				$arResult["ELEMENTS"][$a["ID"]] = $a;			
		}
		
		//\vard($arResult["ELEMENTS"], 'die');
		
		$bCalcBasket = false;
		foreach($GLOBALS["UNI"]["PRICE_CALCULATOR"]["PROPERTIES"] as $sPropertyCode => $arProfile)
			if (in_array($arProfile["TYPE"], ["MIN_BASE_PRICE", "MIN_PRICE", "MAX_PRICE", "MAX_BASE_PRICE"]))
				$bCalcBasket = true;
		
		/////////////////////////////////Calc basket prices
		if ($bCalcBasket)
		{
			$obBasket = \Bitrix\Sale\Basket::create("s1"); //$obBasket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), "s1");
			foreach ($arResult["ELEMENTS"] as $iElementID => $arElement)
			{
				$obBasketIetm = $obBasket->createItem("catalog", $arElement["ID"]);
				$obBasketIetm->setField('PRODUCT_PROVIDER_CLASS',  \Bitrix\Catalog\Product\Basket::getDefaultProviderName());
				$obBasketIetm->setField('QUANTITY', 1);
				$obBasketIetm->setField('LID', "s1");
			}
			
			$obBasket->refreshData(['PRICE', 'DISCOUNT']);

			$colBasketItems = $obBasket->getBasketItems();
			
			foreach ($arResult["ELEMENTS"] as $iElementID => &$arElement)
			{
				$bOfferFound = false;
				
				if (in_array($arElement["IBLOCK_ID"], $GLOBALS["UNI"]["PRICE_CALCULATOR"]["IBLOCK_IDS"]))
				{			
					foreach ($arResult["ELEMENTS"] as $iElementID => $arOffer)
					{
						$arBasketItem = [];
						foreach($colBasketItems as $obBasketItem)
							if ($obBasketItem->getProductId() == $arOffer["ID"])
								$arBasketItem = $obBasketItem->getFieldValues();
						if (! $arBasketItem)
							continue;
						
						if ($arBasketItem["CAN_BUY"] == "N")
						{
							/////////////////////////////////
							continue;
						}
						//vard($arBasketItem);
						
						if ($arOffer["PROPERTY_CML2_LINK_VALUE"] == $arElement["ID"])
						{	
							$bOfferFound = true;
							
							foreach($GLOBALS["UNI"]["PRICE_CALCULATOR"]["PROPERTIES"] as $sPropertyCode => $arProfile)
							{
								if ($arProfile["TYPE"] == "MIN_PRICE" && $arBasketItem["PRICE"])
									if ($arElement["SET_PROPERTY_VALUES"][$sPropertyCode] === NULL || $arBasketItem["PRICE"] < $arElement["SET_PROPERTY_VALUES"][$sPropertyCode])
										$arElement["SET_PROPERTY_VALUES"][$sPropertyCode] = $arBasketItem["PRICE"];
									
								if ($arProfile["TYPE"] == "MIN_BASE_PRICE" && $arBasketItem["BASE_PRICE"])
									if ($arElement["SET_PROPERTY_VALUES"][$sPropertyCode] === NULL || $arBasketItem["BASE_PRICE"] < $arElement["SET_PROPERTY_VALUES"][$sPropertyCode])
										$arElement["SET_PROPERTY_VALUES"][$sPropertyCode] = $arBasketItem["BASE_PRICE"];
									
								if ($arProfile["TYPE"] == "MAX_PRICE" && $arBasketItem["PRICE"])
									if ($arElement["SET_PROPERTY_VALUES"][$sPropertyCode] === NULL || $arBasketItem["PRICE"] > $arElement["SET_PROPERTY_VALUES"][$sPropertyCode])
										$arElement["SET_PROPERTY_VALUES"][$sPropertyCode] = $arBasketItem["PRICE"];
									
								if ($arProfile["TYPE"] == "MAX_BASE_PRICE" && $arBasketItem["BASE_PRICE"])
									if ($arElement["SET_PROPERTY_VALUES"][$sPropertyCode] === NULL || $arBasketItem["BASE_PRICE"] > $arElement["SET_PROPERTY_VALUES"][$sPropertyCode])
										$arElement["SET_PROPERTY_VALUES"][$sPropertyCode] = $arBasketItem["BASE_PRICE"];								
							}
						}
					}
				}
				
				if ($bOfferFound)
				{
					//vard($arElement["SET_PROPERTY_VALUES"], 'die');
				}
			}
			unset($arElement);
		}
		///////////////////////////////// --

		$bCalcByPriceType = false;
		foreach($GLOBALS["UNI"]["PRICE_CALCULATOR"]["PROPERTIES"] as $sPropertyCode => $arProfile)
			if (in_array($arProfile["TYPE"], ["MIN_PRICE_BY_TYPE", "MAX_PRICE_BY_TYPE"]))
				$bCalcByPriceType = true;
			
		///////////////////////////////// Calc prices by type
		if ($bCalcByPriceType)
		{
			foreach($arResult["ELEMENTS"] as $iElementID => &$arElement)
			{
				if (in_array($arElement["IBLOCK_ID"], $GLOBALS["UNI"]["PRICE_CALCULATOR"]["IBLOCK_IDS"]))
				{
					$arElement["SET_PROPERTY_VALUES"] = [];
					
					//Load prices
					foreach($GLOBALS["UNI"]["PRICE_CALCULATOR"]["PROPERTIES"] as $sPropertyCode => $arProfile)
					{
						if ($arProfile["TYPE"] == "MIN_PRICE_BY_TYPE" || $arProfile["TYPE"] == "MAX_PRICE_BY_TYPE")
						{
							$arOfferIDs = [];
							foreach($arResult["ELEMENTS"] as $arCheckElement)
								if ($arCheckElement["PROPERTY_CML2_LINK_VALUE"] == $arElement["ID"])
									$arOfferIDs[] = $arCheckElement["PROPERTY_CML2_LINK_VALUE"];
								
							if (! count($arOfferIDs))
								$arOfferIDs = [$arElement["ID"]];					
							
							$arPriceFilter = [];
							if ($arProfile["CATALOG_GROUP_IDS"])
								$arPriceFilter["CATALOG_GROUP_ID"] = $arProfile["CATALOG_GROUP_IDS"];
							$arPriceFilter["PRODUCT_ID"] = $arOfferIDs;
							
							//\vard($arPriceFilter);
							
							$obPrice = \Bitrix\Catalog\PriceTable::GetList(Array(
								"order" => Array("PRICE" => $arProfile["TYPE"] == "MAX_PRICE_BY_TYPE" ? "desc" : "asc"),
								"filter" => $arPriceFilter,
							));
							if ($arPrice = $obPrice->fetch())
								$arElement["SET_PROPERTY_VALUES"][$sPropertyCode] = $arPrice["PRICE"];
							
							//Patch orthoboom
							if ($sPropertyCode == "MIN_NORMAL_PRICE")
								if ($arElement["PROPERTY_FIKSIROVANNAYA_TSENA_ZNACHENIE_VALUE"] && $arElement["PROPERTY_FIKSIROVANNAYA_TSENA_ZNACHENIE_VALUE"] < $arElement["SET_PROPERTY_VALUES"][$sPropertyCode])
									$arElement["SET_PROPERTY_VALUES"][$sPropertyCode] = $arElement["PROPERTY_FIKSIROVANNAYA_TSENA_ZNACHENIE_VALUE"];
							//--
						}
					}
	
					//\vard($arElement["SET_PROPERTY_VALUES"], 'die');
				}
			}	
			unset($arElement);
		}
		///////////////////////////////// --
		
		// Orthoboom patch
		foreach($arResult["ELEMENTS"] as $iElementID => &$arElement)
		{
			if ($arElement["PROPERTY_FIKSIROVANNAYA_TSENA_ZNACHENIE_VALUE"])
			{
				$arElement["SET_PROPERTY_VALUES"]["MIN_NORMAL_PRICE"] = $arElement["PROPERTY_FIKSIROVANNAYA_TSENA_ZNACHENIE_VALUE"];
			}
		}			
		unset($arElement);
		//--
		
		//Result
		foreach($arResult["ELEMENTS"] as $iElementID => $arElement)
		{
			//if (is_array($arElement["SET_PROPERTY_VALUES"]) && count($arElement["SET_PROPERTY_VALUES"]))
			//vard($arElement["SET_PROPERTY_VALUES"], $arElement["ID"]);
			
			
			
			
			if (is_array($arElement["SET_PROPERTY_VALUES"]) && count($arElement["SET_PROPERTY_VALUES"]))
			{
				
				\CIBlockElement::SetPropertyValuesEx($arElement["ID"], $arElement["IBLOCK_ID"], $arElement["SET_PROPERTY_VALUES"]);
				
				$obElement = new \CIBlockElement;
				$obElement->Update($arElement["ID"], []);
			}
		}
		
		
	}	
}
?>