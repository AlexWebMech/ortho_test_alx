<?
namespace Local;

class Location
{
	public function init()
	{
		static $bInited = false;
		if ($bInited === true)
			return;
		
		if (! $_SESSION["UNI_SESSION"]["MAIN"]["LOCATION"]["CURRENT_LOCATION"])
		{
			if ($iLocationID = \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getCookie("UNI__LOCATION_ID"))
			{
				self::setLocationId($iLocationID);
			}
			elseif ($_SESSION["UNI_SESSION"]["MAIN"]["LOCATION"]["DETECTED_BY_ID"] != "Y")
			{
				self::detectByIp();
			}
		}
		
		$bInited = true;
	}

	public function setLocationId($iLocationId)
	{
		if (\Bitrix\Main\Loader::includeModule("sale"))
		{
			$arLocation = \Bitrix\Sale\Location\LocationTable::getList([
				"filter" => ["=ID" => $iLocationId],
				"select" => ["*", "TITLE" => "NAME.NAME", "NAME_NAME" => "NAME.NAME"],
			])->fetch();
			
			if ($arLocation)
				self::setLocation($arLocation);
		}
	}

	public function setLocation($arLocation)
	{
		if (! $arLocation["ID"] || ! $arLocation["CODE"] || ! $arLocation["TITLE"])
			return false;
		
		$_SESSION["UNI_SESSION"]["MAIN"]["LOCATION"]["CURRENT_LOCATION"] = $arLocation;
		
		$obCookie = new \Bitrix\Main\Web\Cookie("UNI__LOCATION_ID", $arLocation["ID"]);
		\Bitrix\Main\Application::getInstance()->getContext()->getResponse()->addCookie($obCookie);
	}
	
	public function getLocationId()
	{
		return $_SESSION["UNI_SESSION"]["MAIN"]["LOCATION"]["CURRENT_LOCATION"]["ID"];
	}
	
	public function getLocationCode()
	{
		
		return $_SESSION["UNI_SESSION"]["MAIN"]["LOCATION"]["CURRENT_LOCATION"]["CODE"];
	}
	public function getLocationTitle()
	{
		
		return $_SESSION["UNI_SESSION"]["MAIN"]["LOCATION"]["CURRENT_LOCATION"]["TITLE"];
	}
	public function getLocationPath()
	{
		if (isset($_SESSION["UNI_SESSION"]["MAIN"]["LOCATION"]["CURRENT_LOCATION"]["PATH"]))
			return $_SESSION["UNI_SESSION"]["MAIN"]["LOCATION"]["CURRENT_LOCATION"]["PATH"];
		
		$iParentID = $_SESSION["UNI_SESSION"]["MAIN"]["LOCATION"]["CURRENT_LOCATION"]["PARENT_ID"];
		
		if ($iParentID && \Bitrix\Main\Loader::includeModule("sale"))
		{	
			$_SESSION["UNI_SESSION"]["MAIN"]["LOCATION"]["CURRENT_LOCATION"]["PATH"] = [];
			while ($iParentID)
			{
				$arLocation = \Bitrix\Sale\Location\LocationTable::getList([
					"filter" => ["=ID" => $iParentID],
					"select" => ["*", "TITLE" => "NAME.NAME", "NAME_NAME" => "NAME.NAME"],
				])->fetch();
				$_SESSION["UNI_SESSION"]["MAIN"]["LOCATION"]["CURRENT_LOCATION"]["PATH"] = array_replace([$arLocation["DEPTH_LEVEL"] => $arLocation], $_SESSION["UNI_SESSION"]["MAIN"]["LOCATION"]["CURRENT_LOCATION"]["PATH"]);
				$iParentID = $arLocation["PARENT_ID"];
			}
		}
		
		return $_SESSION["UNI_SESSION"]["MAIN"]["LOCATION"]["CURRENT_LOCATION"]["PATH"];
	}
	
	public function isDetectedNow($bSet = null)
	{
		static $bDetectedNow = false;
		if ($bSet === null)
			return $bDetectedNow;
		if ($bSet === true)
			return $bDetectedNow = true;
	}
	
	public function detectByIp()
	{
		$_SESSION["UNI_SESSION"]["MAIN"]["LOCATION"]["DETECTED_BY_ID"] = "Y";
		
		if(
			stripos($_SERVER['HTTP_USER_AGENT'], "googlebot") !== false 
			|| stripos($_SERVER['HTTP_USER_AGENT'], "yandex.ru") !== false
			|| stripos($_SERVER['HTTP_USER_AGENT'], "yandex.com") !== false
			|| stripos($_SERVER['HTTP_USER_AGENT'], "yandex.net") !== false
		)
			return false;
			
		$sIp = \Bitrix\Main\Service\GeoIp\Manager::getRealIp();
        $sCityTitle = \Bitrix\Main\Service\GeoIp\Manager::getCityName($sIp, LANGUAGE_ID);

		if ($sCityTitle && \Bitrix\Main\Loader::includeModule("sale"))
		{
			$arFilter = [];
			$arFilter['=NAME.NAME'] = $sCityTitle;
			$arFilter['=NAME.LANGUAGE_ID'] = LANGUAGE_ID;
			
			$arLocation = \Bitrix\Sale\Location\LocationTable::getList([
				"filter" => $arFilter,
				"select" => ["*", "TITLE" => "NAME.NAME", "NAME.NAME" => "NAME.NAME"],
			])->fetch();
			
			if ($arLocation)
				self::setLocation($arLocation);
		}
		
		self::isDetectedNow(true);
	}
}

//\Local\Location::init();