<?

$eventManager = \Bitrix\Main\EventManager::getInstance();

$eventManager->addEventHandler("catalog", "OnSuccessCatalogImport1C", array('CCatalogImport1C', 'onSuccessCatalogImport1C'));
$eventManager->addEventHandler('catalog', 'OnStoreProductAdd', array('CCatalogImport1C', 'onStoreProductAddUpdate'));
$eventManager->addEventHandler('catalog', 'OnStoreProductUpdate', array('CCatalogImport1C', 'onStoreProductAddUpdate'));
$eventManager->addEventHandler("catalog", "OnCompleteCatalogImport1C", array('CCatalogImport1C', 'onCompleteCatalogImport1C'));

class CCatalogImport1C {

    // внешний код базовой цены
    const BASE_PRICE_XML_ID = '40ff571d-9ecb-11e8-93f3-d850e6e3b156';
    // внешний код цены из второго каталога
    const CATALOG_2_PRICE_XML_ID = 'eb101f8b-3121-11e9-80cf-d850e6e3b156';
    // инфоблок товаров
    const CATALOG_IBLOCK_ID = 94;
    // инфоблок SKU
    const OFFERS_IBLOCK_ID = 95;

    // актуализация суммарных остатков товара
    public static function onSuccessCatalogImport1C($arParams, $arFields) {

        AddMessage2Log('OnSuccessCatalogImport1C' . print_r($arFields, true) , '');

        global $APPLICATION;
        if(0) { //disable product in admin-panel

            \Bitrix\Main\Loader::includeModule('catalog');

            // если обработанный файл выгрузки - файл остатков
            if (strpos($arFields, 'rests') !== false) {

                AddMessage2Log('rests' , '');

                //--XML--

                $obXMLFile = new CIBlockXMLFile(CBitrixCatalogImport1C::XML_TREE_TABLE_NAME);

                $rsParents = $obXMLFile->GetList(
                    array("ID" => "asc"),
                    array(//"PARENT_ID" => $obCatalog->next_step["XML_ELEMENTS_PARENT"],

                        ">ID" => 0//$obCatalog->next_step["XML_LAST_ID"]
                    ),
                    array("ID",  "LEFT_MARGIN",
                        "RIGHT_MARGIN", "PARENT_ID",  "DEPTH_LEVEL", "NAME", "VALUE","ATTRIBUTES")
                );
                $res = [];
                $prods = [];
                $cnt = 0;
                $cnt_margin = 0;
                $offer_xml_id = '';
                $iblock_xml_id = '';
                while($arParent = $rsParents->Fetch())
                {
                    $field_name = $APPLICATION->ConvertCharset('ИдКаталога', "UTF-8", "windows-1251");
                    if($arParent['NAME']==$field_name){
                        $iblock_xml_id = $arParent['VALUE'];
                    }

                    $field_name = $APPLICATION->ConvertCharset('Ид', "UTF-8", "windows-1251");
                    if($arParent['NAME']==$field_name){
                        $cnt_margin = $arParent['LEFT_MARGIN'];
                        $offer_xml_id = $arParent['VALUE'];
//                        if($arParent['ID'] == 3){
//                            $iblock_xml_id = $offer_xml_id;   //offers
//                        }
                    }

                    $field_name = $APPLICATION->ConvertCharset('Количество', "UTF-8", "windows-1251");
                    if($cnt && $cnt_margin==$arParent['LEFT_MARGIN']-4     //ofset
                        &&
                        $arParent['NAME']==$field_name){
                        $res[$offer_xml_id] += $arParent['VALUE'];

                        if(strrpos($offer_xml_id, "#")){
                            list($prod_xml, $description) = explode("#", $offer_xml_id);
                            $prods[$prod_xml]=$prod_xml;
//                            if(!in_array('',$prods)){
//                            }
                        }
                    }
                    $cnt++;
                }
                $offer_xml_ids = array_keys($res);

                //--/XML--


                $offers = [];
                //--iBlock--
                $db_res = CIBlock::GetList(Array(),Array('TYPE'=>'1c_catalog',"=XML_ID"=>'1c_catalog-'.$iblock_xml_id.'#'),true);
                if($ar_res = $db_res->Fetch()){

//                    echo('<pre>');
//                    print_r($ar_res);
//                    echo('</pre>');
//
//                    exit;


                    $updated_p=[];
                    AddMessage2Log('Start update quantity (iblock='.$ar_res['ID'].')' , '');
                    //--Offers--
                    $rsElements = CCatalogProduct::GetList(array(), array(
                        "ELEMENT_IBLOCK_ID" => $ar_res['ID'],
//                            "ELEMENT_XML_ID" => $offer_xml_ids,
                        "%ELEMENT_XML_ID" => $prods,
                    ), false, false, array("ID","ELEMENT_XML_ID","ELEMENT_NAME","QUANTITY","QUANTITY_RESERVED","AVAILABLE"));
                    while ($arElement = $rsElements->Fetch())
                    {
//                        $offers[$arElement["ELEMENT_XML_ID"]]=$arElement;
                        $quantity = (array_key_exists($arElement["ELEMENT_XML_ID"],$res)) ?
                            $res[$arElement["ELEMENT_XML_ID"]] :
                            0;

                        if(//$arElement["ID"]==110639 &&
                        $ar_res['ID'] == 84 &&  //julianna offers
                        $arElement["QUANTITY"] > 0 && //available only
                        $arElement["QUANTITY"] != $quantity &&  //different only
                            $quantity - $arElement["QUANTITY_RESERVED"] >= 0
                        ){
                            $updated_p[$arElement["ID"]]=$quantity;
                            CCatalogProduct::Update($arElement["ID"], array(
                                "QUANTITY" => $quantity
                            ));
                        }
                    }

                    AddMessage2Log('Quantity updated products = ' . print_r($updated_p, true), '');
                    AddMessage2Log('Stop update quantity.' , '');
                    //--/Offers--
                }
                //--/iBlock--
            }
        }

//        echo('<pre>');
//        print_r($res);
//        print_r($offers);
//        echo('</pre>');
//
//        exit;
        // сделано в классе выгрузки (local/components/o2k/catalog.import.1c/component.php) , отключаю
        if(0) {
            \Bitrix\Main\Loader::includeModule('catalog');

            // если обработанный файл выгрузки - файл остатков
            if (strpos($arFields, 'rests') !== false) {
                // берём массив ID товаров у которых произошло изменение количества на складах
                $idProducts = $_SESSION['BX_CML2_IMPORT_PRODUCTS_ID'];
                AddMessage2Log('$idProducts = ' . print_r($idProducts, true), '');
                // блокирую, т.к. аналог сделал в классе импорта
                return
                    // получаем список записей о наличии этих товаров на складах, где количество больше нуля
                    $rsStore = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' => $idProducts, ">AMOUNT" => 0), false, false, array('*'));
                $arProductAmount = [];
                $arStores = [];
                while ($arStore = $rsStore->Fetch()) {
                    $arStores[] = $arStore;
                    // если записи о складе ещё нет, создаём её
                    if (empty($arProductAmount[$arStore['PRODUCT_ID']]))
                        $arProductAmount[$arStore['PRODUCT_ID']] = ['PRODUCT_ID' => $arStore['PRODUCT_ID'], 'AMOUNT' => $arStore['AMOUNT']];
                    else
                        // иначе сразу увеличиваем количество
                        $arProductAmount[$arStore['PRODUCT_ID']]['AMOUNT'] = $arProductAmount[$arStore['PRODUCT_ID']]['AMOUNT'] + $arStore['AMOUNT'];
                }
                //Обновляем общие остатки товаров
                foreach ($arProductAmount as $key => $product) {
                    CCatalogProduct::Update($product['PRODUCT_ID'], ['QUANTITY' => $product['AMOUNT']]);
                }
            }

            /*if(strpos($arFields, 'price') !== false){

                //Обнеовляем цену базовую цену товара
                $arPrices = CCatalogGroup::GetListArray();
                $idProducts = $_SESSION['BX_CML2_IMPORT_PRODUCTS_ID'];
                //$arPriceXML = [];
                $ID_BASE_PRICE = 0;
                $ID_CATALOG_2_PRICE = 0;

                foreach($arPrices as $price){
                    if($price['XML_ID'] == self::BASE_PRICE_XML_ID)
                        $ID_BASE_PRICE = $price['ID'];
                    if($price['XML_ID'] == self::CATALOG_2_PRICE_XML_ID)
                        $ID_CATALOG_2_PRICE = $price['ID'];

                    //$arPriceXML[$price['XML_ID']] = $price;
                }
                //$basePrice = CCatalogGroup::GetBaseGroup();
                $rsPrices = CPrice::GetListEx(array(), array('PRODUCT_ID' =>$idProducts), false, false, array('*'));
                $arProductPrices = [];
                while($arPrice = $rsPrices->Fetch()){
                    $arProductPrices[$arPrice['PRODUCT_ID']][$arPrice['CATALOG_GROUP_ID']] = $arPrice;
                }

                foreach($arProductPrices as $id_product => $arPrice){
                    $arFieldsPrice = [];
                    if(empty($arPrice[$ID_BASE_PRICE]) && !empty($arPrice[$ID_CATALOG_2_PRICE])){
                        $arFieldsPrice = Array(
                            "PRODUCT_ID" => $id_product,
                            "CATALOG_GROUP_ID" => $ID_BASE_PRICE,
                            "PRICE" => $arPrice[$ID_CATALOG_2_PRICE]['PRICE'],
                            "CURRENCY" => $arPrice[$ID_CATALOG_2_PRICE]['CURRENCY'],
                        );
                    }

                    if(!empty($arFieldsPrice))
                        CPrice::Update($arPrice[$ID_BASE_PRICE]["ID"], $arFieldsPrice);
                }
            }*/
        }
    }

    // перенос цены товаров из второй выгрузки в первую если в первой нет никакой цены
    public static function onCompleteCatalogImport1C($arParams, $arFields){

        $arProductIDs = [];
        // собираем все активные товары из каталога
        $res = CIblockElement::GetList(
            array(),
            array("ACTIVE" => "Y", 'IBLOCK_ID' => self::CATALOG_IBLOCK_ID),
            false,
            false,
            array('ID', 'IBLOCK_ID', 'NAME')
        );
        while ($arItem = $res->fetch()) {
            $arProductIDs[] = $arItem['ID'];
        }

        // если активные товары есть
        if (!empty($arProductIDs)) {
            // cобираем список цен
            $arPrices = CCatalogGroup::GetListArray();
            $ID_BASE_PRICE = 0;
            $ID_CATALOG_2_PRICE = 0;

            // и определяем ID цен по внешнему коду
            foreach ($arPrices as $price) {
                if ($price['XML_ID'] == self::BASE_PRICE_XML_ID)
                    $ID_BASE_PRICE = $price['ID'];
                if ($price['XML_ID'] == self::CATALOG_2_PRICE_XML_ID)
                    $ID_CATALOG_2_PRICE = $price['ID'];
            }

            // собираем цены активных товаров
            $rsPrices = CPrice::GetListEx(array(), array('PRODUCT_ID' => $arProductIDs), false, false, array('*'));
            $arProductPrices = [];
            while ($arPrice = $rsPrices->Fetch()) {
                $arProductPrices[$arPrice['PRODUCT_ID']][$arPrice['CATALOG_GROUP_ID']] = $arPrice;
            }
            $arFieldsPrices = [];
            // перебираем собранные цены
            foreach ($arProductPrices as $id_product => $arPrice) {
                $arFieldsPrice = [];
                // и, если пуста основная цена и не пуста цена из второй выгрузки
                if (empty($arPrice[$ID_BASE_PRICE]) && !empty($arPrice[$ID_CATALOG_2_PRICE])) {
                    // записываем эту цену как основную
                    $arFieldsPrice = Array(
                        "PRODUCT_ID" => $id_product,
                        "CATALOG_GROUP_ID" => $ID_BASE_PRICE,
                        "PRICE" => $arPrice[$ID_CATALOG_2_PRICE]['PRICE'],
                        "CURRENCY" => $arPrice[$ID_CATALOG_2_PRICE]['CURRENCY'],
                    );
                }
                // если есть, что записывать, то и записываем
                if (!empty($arFieldsPrice)) {
                    $arFieldsPrices[] = $arFieldsPrice;
                    CPrice::Add($arFieldsPrice);
                }
            }
        }
    }

    // сохранение обновляемых товаров
    public static function onStoreProductAddUpdate($ID, $arFields){
        if ($_GET['mode'] === 'import') {
            //Собираем массив ID товаров у которых произошло изменение количества на складах
            if(!in_array($arFields['PRODUCT_ID'], $_SESSION["BX_CML2_IMPORT_PRODUCTS_ID"]))
                $_SESSION["BX_CML2_IMPORT_PRODUCTS_ID"][] = $arFields['PRODUCT_ID'];
        }
    }
}