<?php

use Lib\Classes\Helper\LenalHelp;

/**
 * Class OnProductAdd
 */
class OnProductAdd
{
    /**
     * add property to product MIN_PRICE
     * @param $arFields
     */
    public function AddMinPrice($arFields)
    {
        // заполнение свойства MIN_PRICE для товаров с SKU
        // если текущий инфоблок по коду равен инфоблоку каталога
        if (LenalHelp::getIblockIdByCode('catalog_erp3') == (int)$arFields["IBLOCK_ID"]) {
            if ((int)$arFields["ID"] <= 0) {
                return;
            }

            // получаем SKU текущего товара. 95 - ID инфоблока с SKU, 89 - ID базовой цены
            $offers = CCatalogSku::getOffersList(
                (int)$arFields["ID"],
                0,
                ['ACTIVE' => 'Y'],
                ['ID', 'CATALOG_GROUP_89', 'CATALOG_PRICE_89'],
                []
            );
            $offers=$offers[$arFields["ID"]];

            $allOffersPrices = [];
            foreach ($offers as $price) {
                $allOffersPrices[] = $price["CATALOG_PRICE_89"];
            }

            if ($allOffersPrices) {
                $minPrice = min($allOffersPrices);
                $propertyCode = "MIN_PRICE";
                \CIBlockElement::SetPropertyValuesEx($arFields["ID"], $arFields["IBLOCK_ID"],
                    [$propertyCode => $minPrice]);
            }
        }
    }
}
