<?php
use \Bitrix\Main\Loader;
use \Olegpro\IpGeoBase\IpGeoBase;
use Bitrix\Main\Text\Encoding;

require("lib/local/location.php"); //Location detecting
require("lib/local/price.php"); //Price calculator

include_once ('const.php');
// РџРѕСЃР»Рµ РїРµСЂРµРёРЅРґРµРєСЃР°С†РёРё Рё РёР·РјРµРЅРµРЅРёСЏ СЃСЃС‹Р»РѕРє РІ РєРѕРґРµ СЃР°Р№С‚Р° - РјРѕР¶РЅРѕ СѓРґР°Р»РёС‚СЊ РІРµСЃСЊ Р±Р»РѕРє
if(defined('SMART_FILTER_UUID_VALUE') && SMART_FILTER_UUID_VALUE) {
    if(\Bitrix\Main\Loader::includeModule('gorgoc.smartfilter')){
        $context = \Bitrix\Main\Application::getInstance()->getContext();
        $request = $context->getRequest();
        $targetUrl = $request->getRequestUri();
        $redirector=new \Gorgoc\Smartfilter\GenerateValueRedirector();
        if($redirector->isFilter()) {
            $seoUrl= $redirector->getSeoUrl();
            if(strlen($seoUrl)>5 && $targetUrl!==$seoUrl){
                LocalRedirect($seoUrl,true,301);
            }
        }
        
    }
}
/** Р”РµР№СЃС‚РІРёСЏ РґР»СЏ СЃРјСЃ СЂРµРіРёСЃС‚СЂР°С†РёРё */
if (defined('REGISTRATION_SMS_ENABLE') && REGISTRATION_SMS_ENABLE) {
    /** РћР±СЉРµРґРёРЅСЏРµС‚ СѓС‡С‘С‚РЅС‹Рµ Р·Р°РїРёСЃРё, РІСЂРµРјРµРЅРЅС‹Рµ Рё РѕСЃРЅРѕРІРЅСѓСЋ */
    Bitrix\Main\EventManager::getInstance()->addEventHandler(
        "main", "OnAfterUserAuthorize", function ($arUser) {
        if (\Bitrix\Main\Loader::includeModule("gorgoc.registrationsms")) {
            $class = new \Gorgoc\Registrationsms\MergingAccounts();
            $class->OnAfterUserAuthorizeHandler($arUser);
        }
    }
    );
    /** РЎРѕР·РґР°С‘С‚ СѓС‡С‘С‚РЅСѓСЋ Р·Р°РїРёСЃСЊ РїРѕР»СЊР·РѕРІР°С‚РµР»СЏ РїРѕСЃР»Рµ РѕС„РѕСЂРјР»РµРЅРёСЏ Р·Р°РєР°Р·Р° */
     Bitrix\Main\EventManager::getInstance()->addEventHandler(
        'sale',
        'OnSaleOrderBeforeSaved',
        function ($event) {
            if (\Bitrix\Main\Loader::includeModule("gorgoc.registrationsms")) {
                $request = \Bitrix\Main\Context::getCurrent()->getRequest();
                /** @var \Bitrix\Main\Type\ParameterDictionary $query */
                $query = $request->getQueryList();
                // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/new_order_event1.txt',var_export($query->getValues(),true),FILE_APPEND);
                // РўРѕР»СЊРєРѕ Р·Р°РєР°Р·С‹ РІ РћРґРёРЅРљР»РёРє
                if(!array_key_exists('size_buyoneclick',$query->getValues()) && !array_key_exists('amount_buyoneclick',$query->getValues())) {
                    return;
                }
                //file_put_contents($_SERVER['DOCUMENT_ROOT'].'/new_order_event1.txt',var_export($query->getValues(),true));
                //file_put_contents($_SERVER['DOCUMENT_ROOT'].'/new_order_event1.txt',var_export($event,true));
                $class = new \Gorgoc\Registrationsms\OrderEvents();
                $class->saleOrderBeforeSaved($event);
            }
        }
    );
    /** РњРѕРґРёС„РёРєР°С†РёСЏ РґР°РЅРЅС‹С… РїРѕР»СЊР·РѕРІР°С‚РµР»СЏ РїСЂРё СЂРµРіРёСЃС‚СЂР°С†РёРё С‡РµСЂРµР· РїСЂРѕС†РµРґСѓСЂСѓ СЃРѕР·РґР°РЅРёСЏ Р·Р°РєР°Р·Р° РєРѕРјРїРѕРЅРµРЅС‚Р° sale.order.ajax*/
    Bitrix\Main\EventManager::getInstance()->addEventHandler(
        'main',
        'OnBeforeUserAdd',
        function (&$arParams ) {
            if (\Bitrix\Main\Loader::includeModule("gorgoc.registrationsms")) {
                $request = \Bitrix\Main\Context::getCurrent()->getRequest();

                if($request->isAdminSection() || stripos($request->getRequestUri(),'sale.order.ajax') === false){
                    return;
                }
                $class = new \Gorgoc\Registrationsms\OrderEvents();
                $arParams = $class->OnBeforeUserAdd($arParams);
              
                return $arParams;
              
            }
        },
        false,
        90000
    );
    //РђРЅС‚Рё Р°РІС‚Рѕ СЂРµРіРёСЃС‚СЂР°С†РёСЏ
    $request = \Bitrix\Main\Context::getCurrent()->getRequest();
    if($request->getRequestUri()==='/personal/profile/?register=yes'){ // СЃРєРѕСЂРµРµ РІСЃРµРіРѕ СЌС‚Рѕ Р°РІС‚РѕСЂРµРіРёСЃС‚СЂР°С†РёСЏ
       // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/_service/log_register.txt',var_export($request->toArray(),TRUE),FILE_APPEND);
    }
    // РћС‚РєР»СЋС‡РёС‚СЊ СЂРµРіРёСЃС‚СЂР°С†РёСЋ
    Bitrix\Main\EventManager::getInstance()->addEventHandler(
    'main',
        'OnBeforeUserRegister',
        function($args) {
            if(!empty($args['TYPE_UPDATE'])) {
                return;
            }
            $request = \Bitrix\Main\Context::getCurrent()->getRequest();
            if($request->isAdminSection()){
                return;
            }
            $dataRequest = $request->toArray();
            if(!is_array($dataRequest) || empty($dataRequest)) {
                return;
            }
            if(isset($dataRequest['AUTH_FORM']) && isset($dataRequest['TYPE']) && $dataRequest['TYPE']==='REGISTRATION') {
                file_put_contents($_SERVER["DOCUMENT_ROOT"].'/_service/login_exit_log_blocked.txt',var_export($_REQUEST,true),FILE_APPEND);
                $message = $GLOBALS['APPLICATION']->ConvertCharset('Р§С‚Рѕ С‚Рѕ РїРѕС€Р»Рѕ РЅРµ С‚Р°Рє! РљРѕРґ РѕС€РёР±РєРё EM9982234XXX','UTF-8',SITE_CHARSET);
                $GLOBALS['APPLICATION']->ThrowException($message); // С‚РѕР»СЊРєРѕ РґР»СЏ РїРѕРёСЃРєР° РІ РєРѕРґРµ
                return false;
            }
            file_put_contents($_SERVER["DOCUMENT_ROOT"].'/_service/login_exit_log.txt',var_export($_REQUEST,true),FILE_APPEND);
            file_put_contents($_SERVER["DOCUMENT_ROOT"].'/_service/login_exit_log.txt',PHP_EOL,FILE_APPEND);
            file_put_contents($_SERVER["DOCUMENT_ROOT"].'/_service/login_exit_log.txt',file_get_contents("php://input"),FILE_APPEND);
            file_put_contents($_SERVER["DOCUMENT_ROOT"].'/_service/login_exit_log.txt',PHP_EOL,FILE_APPEND);
            file_put_contents($_SERVER["DOCUMENT_ROOT"].'/_service/login_exit_log.txt',var_export($_SERVER,true),FILE_APPEND);
            return;
        }
    );
}


define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/init_logs.php")) {
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/init_logs.php");
}
AddEventHandler("iblock", "OnAfterIBlockElementAdd", ["AfterElementAdd", "AfterElementAddSendMail"]);
AddEventHandler("iblock", "OnAfterIBlockElementAdd", ["OnProductAdd", "AddMinPrice"]);
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", ["OnProductAdd", "AddMinPrice"]);

// РїСЂРѕРІРµСЂСЏРµРј, РїСѓСЃС‚Р° Р»Рё РїРµСЂРµРјРµРЅРЅР°СЏ $USER. Р•СЃР»Рё РґР°, С‚Рѕ РѕРїСЂРµРґРµР»СЏРµРј РєРѕРЅСЃС‚Р°РЅС‚Сѓ BX_SKIP_SESSION_EXPAND С‡С‚РѕР±С‹ РѕР±РѕР№С‚Рё РѕС€РёР±РєСѓ СЃ
// $USER == null РІ bitrix/modules/main/classes/general/main.php:3423
AddEventHandler("main", "OnBeforeProlog", "onBeforePrologHandlerTask23052", 50);

function onBeforePrologHandlerTask23052()
{
    global $USER;
    if(empty($USER)) {
        $USER = new CUser();
        if(empty($USER)) {
            define("BX_SKIP_SESSION_EXPAND", true);
        }
    }
}

//\Bitrix\Main\Diag\Debug::writeToFile(date('d.m.Y H:i').' --1-1-- ', '', 'test_log.log');

//AddEventHandler("catalog", "OnBeforeGroupUpdate", "AfterCatalogUpdate");
AddEventHandler("catalog", "OnSuccessCatalogImport1C", "AfterCatalogUpdate");
function AfterCatalogUpdate(){
    yaFeedGenerate();
    //\Bitrix\Main\Diag\Debug::writeToFile(date('d.m.Y H:i').' --2=2=2-- ', '', 'test_log.log');
}

include_once ('lib/classes/Helper/LenalHelp.php');
include_once ('lib/constants/SmartFilter.php');
include_once ('classes/o2k_cities.php');
include_once ('classes/webp.php');
include_once ('classes/facetmod.php');
require_once $_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/lib/event_functions/OnProductAdd.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/lib/event_functions/ccatalogimport1c.php";

if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/init_ortoboom.php")) {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/init_ortoboom.php");
}


class AfterElementAdd
{
    public function AfterElementAddSendMail(&$arFields)
    {
        //AddMessage2Log($arFields);
        if ($arFields["IBLOCK_ID"] == 21) {
            CModule::IncludeModule("iblock");
            $SPEC = "";
            if ($arFields["PROPERTY_VALUES"]["104"]) {
                $arSelect = ["ID", "NAME"];
                $arFilter = ["IBLOCK_ID" => 18, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y",
                             "ID" => $arFields["PROPERTY_VALUES"]["104"]];
                $res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 1], $arSelect);
                if ($ob = $res->GetNextElement()) {
                    $arFieldss = $ob->GetFields();
                    $SPEC = $arFieldss["NAME"];
                }
            }
            
            //пїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ
            $arEventFields = array(
                "NAME" => $arFields["NAME"],
                "RESULT_ID" => $arFields["ID"],
                "DATE_CREATE" => $arFields["DATE_ACTIVE_FROM"],
                "PHONE" => $arFields["PROPERTY_VALUES"]["102"],
                "EMAIL" => $arFields["PROPERTY_VALUES"]["101"],
                "TEXT" => $arFields["PREVIEW_TEXT"],
                "SPEC" => $SPEC,
                //"ITEM"    => $arFields["PROPERTY_VALUES"]["105"],
                "LINK" => $arFields["PROPERTY_VALUES"]["1213"],
            );
            
            //пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ пїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅ
            CEvent::Send("NEW ELEMENT IBLOCK", "s2", $arEventFields, "Y", 64);
        }
        
        if ($arFields["IBLOCK_ID"] == 20) {
            CModule::IncludeModule("iblock");
            $SPEC = "";
            if ($arFields["PROPERTY_VALUES"]["99"]) {
                $arSelect = ["ID", "NAME"];
                $arFilter = ["IBLOCK_ID" => 18, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y",
                             "ID" => $arFields["PROPERTY_VALUES"]["99"]];
                $res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 1], $arSelect);
                if ($ob = $res->GetNextElement()) {
                    $arFieldss = $ob->GetFields();
                    $SPEC = $arFieldss["NAME"];
                }
            }
            $MARK = "";
            if ($arFields["PROPERTY_VALUES"]["98"]) {
                $property_enums = CIBlockPropertyEnum::GetList(["DEF" => "DESC", "SORT" => "ASC"],
                    ["IBLOCK_ID" => 20, "CODE" => "MARK"]);
                while ($enum_fields = $property_enums->GetNext()) {
                    $MARK = $enum_fields["VALUE"];
                }
            }
            //пїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ
            $arEventFieldss = array(
                "NAME" => $arFields["NAME"],
                "RESULT_ID" => $arFields["ID"],
                "DATE_CREATE" => $arFields["DATE_ACTIVE_FROM"],
                "EMAIL" => $arFields["PROPERTY_VALUES"]["96"],
            );
            if ($arFields["PROPERTY_VALUES"]["98"] == 181) {
                //пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ
                CEvent::Send("NEW ELEMENT IBLOCK", "s2", $arEventFieldss, "Y", 66);
            } elseif ($arFields["PROPERTY_VALUES"]["98"] == 183) {
                //пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ
                CEvent::Send("NEW ELEMENT IBLOCK", "s2", $arEventFieldss, "Y", 68);
            } else {
                //пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ
                CEvent::Send("NEW ELEMENT IBLOCK", "s2", $arEventFieldss, "Y", 67);
            }
            
            //пїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ
            $arEventFields = array(
                "NAME" => $arFields["NAME"],
                "RESULT_ID" => $arFields["ID"],
                "DATE_CREATE" => $arFields["DATE_ACTIVE_FROM"],
                "PHONE" => $arFields["PROPERTY_VALUES"]["97"],
                "EMAIL" => $arFields["PROPERTY_VALUES"]["96"],
                "TEXT" => $arFields["DETAIL_TEXT"],
                "SPEC" => $SPEC,
                "MARK" => $MARK,
                //"ITEM"    => $arFields["PROPERTY_VALUES"]["100"],
                "LINK" => $arFields["PROPERTY_VALUES"]["1214"],
            );
            
            //пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ пїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅ
            CEvent::Send("NEW ELEMENT IBLOCK", "s2", $arEventFields, "Y", 65);
        }
    }
}

function transliterate($strToTranslate) {
    $strToTranslate = mb_strtolower($strToTranslate);
    $cyr = array(
        'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', 'пїЅ', ' ');
    $lat = array(
        'a', 'b', 'v', 'g', 'd', 'e', 'yo', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'shch', '', 'y', '', 'e', 'yu', 'ya', '_');
    $result = str_replace($cyr, $lat, $strToTranslate);
    return $result;
}

function FindCity($remoteAddr = false)
{
    if($remoteAddr===false) {
        if($_SERVER['X_FORWARDED_FOR'])
            $remoteAddr = $_SERVER['X_FORWARDED_FOR'];
        elseif($_SERVER['HTTP_X_FORWARDED_FOR']) {
            if(strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')===false)
                $remoteAddr = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else
                $remoteAddr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])[0];
        }
        else
            $remoteAddr = $_SERVER['REMOTE_ADDR'];
    }
    
    $City = o2k\IPGeoBase::GetCityByIP($remoteAddr);
    
    if(!$City) {
        //пїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅ пїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ, пїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅ
        $City = o2k\IPGeoBase::GetDefaultCity();
    }
    
    return $City;
}

function GetCities()
{
    $cities = o2k\IPGeoBase::GetCities();
    return $cities;
}

/*

//РЈР±РёРІР°РµС‚ Bitrix core js
//AddEventHandler("main", "OnEndBufferContent", "deleteKernelJs"); //РЈР±СЂР°С‚СЊ js
AddEventHandler("main", "OnEndBufferContent", "deleteKernelCss"); //РЈР±СЂР°С‚СЊ css
function deleteKernelJs(&$content) {
    global $USER, $APPLICATION;
    if((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/")!==false) return;
    if($APPLICATION->GetProperty("save_kernel") == "Y") return;
    $arPatternsToRemove = Array(
        '/<script.+?src=".+?kernel_main\/kernel_main\.js\?\d+"><\/script\>/',
        '/<script.+?src=".+?bitrix\/js\/main\/core\/core[^"]+"><\/script\>/',
        '/<script.+?>BX\.(setCSSList|setJSList)\(\[.+?\]\).*?<\/script>/',
        '/<script.+?>if\(\!window\.BX\)window\.BX.+?<\/script>/',
        '/<script[^>]+?>\(window\.BX\|\|top\.BX\)\.message[^<]+<\/script>/',
    );
    $content = preg_replace($arPatternsToRemove, "", $content);
    $content = preg_replace("/\n{2,}/", "\n\n", $content);
}

function deleteKernelCss(&$content) {
    global $USER, $APPLICATION;
    if((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/")!==false) return;
    if($APPLICATION->GetProperty("save_kernel") == "Y") return;
    $arPatternsToRemove = Array(
        '/<link.+?href=".+?kernel_main\/kernel_main\.css\?\d+"[^>]+>/',
        '/<link.+?href=".+?bitrix\/js\/main\/core\/css\/core[^"]+"[^>]+>/',
        '/<link.+?href=".+?bitrix\/templates\/[\w\d_-]+\/styles.css[^"]+"[^>]+>/',
        '/<link.+?href=".+?bitrix\/templates\/[\w\d_-]+\/template_styles.css[^"]+"[^>]+>/',
    );
    $content = preg_replace($arPatternsToRemove, "", $content);
    $content = preg_replace("/\n{2,}/", "\n\n", $content);
}
*/

/*
	function vard ($val = NULL, $message = "", $skip_check_admin = false) //Debug Variable
	{
		if ($skip_check_admin !== true)
		{
			global $USER;
			if (! $USER || ! $USER->IsAdmin())
				return false;
			if (! $USER || ($USER->GetId() == 4)) // && ! $skip_check_admin
				return false;
		}
		
		echo '<div>';
			
		if ($message)
			echo '<b>' . $message . ' : </b>';
			
			if (is_array ($val) || $val instanceof ArrayAccess)
			{
			echo '<table border="1">' ;
				foreach ($val as $key => $value)
				{
				echo "<tr><td style='padding:0 8px' valign='top'>$key</td><td style='padding:2px 8px'>" ;
				vard($value, '', true) ; echo '</td></tr>';
				}
			echo '</table>';
			}
			elseif(is_object ($val))
			{
				echo "[ Object ]";
			}
			else
			{
				if ($val !== true && $val !== false)
					echo $val;
				elseif ($val === true)
					echo 'true';
				elseif ($val === false)
					echo 'false';
			}
		
		if ($message)
			echo '';	
		
		echo '</div>';
			
		if (substr($message, 0, 3) == 'die')
		{
			throw new \Bitrix\Main\SystemException("Error");
			die($message);
		}
		##throw new \Bitrix\Main\SystemException("Error");
	}
*/	