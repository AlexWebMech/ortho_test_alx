<?php
/**
 * Включить отображение seo ссылок в фильтрах вместо uuid 1C
 */
define('SMART_FILTER_UUID_VALUE',true);

/**
 * Вход на сайт через СМС, отлкючение регистрации битрикс, авто-регистрация через оформление заказа
 * true - Новый механизм
 * false - старый
 */

define('REGISTRATION_SMS_ENABLE', true);
