<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (is_array($arResult["ITEMS"]) && count($arResult["ITEMS"]))
{
	?><script type="text/javascript" class="uni-ajax-script"><?
	foreach($arResult["ITEMS"] as $iElementID => $arItem)
	{
		?>$("*[data-iblock-element-id=<?= intval($iElementID); ?>]").addClass("fav-set");<?
	}
	?></script><?
}