<? if (! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?><?

$this->setFrameMode(false);

if (! $arResult["ITEMS"])
{
	?>В вашем избранном нет товаров.<?
}
else
{
	$arElementIDs = array_keys($arResult["ITEMS"]);

	$GLOBALS["arWishlistCatalogFilter"] = ["ID" => []];
	foreach($arResult["ITEMS"] as $iElementID => $arItem)
		$GLOBALS["arWishlistCatalogFilter"]["ID"][] = $iElementID;
		
	
	?>
	
	<? $APPLICATION->IncludeComponent("bitrix:catalog.section", "orthoboom-catalog", Array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => 94, //$arParams["IBLOCK_ID"],
			
		"SECTION_ID" => "",	
		"SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"BY_LINK" => "Y",
		
		"PAGE_ELEMENT_COUNT" => "1000",		
	
		"FILTER_NAME" => "arWishlistCatalogFilter",
	
		"ELEMENT_SORT_FIELD" => "SORT",
		"ELEMENT_SORT_ORDER" => "ASC",
		"ELEMENT_SORT_FIELD2" => "ID",
		"ELEMENT_SORT_ORDER2" => "DESC",
		"CUSTOM_ELEMENT_SORT" => ["ID" => $arElementIDs],
		
		"PROPERTY_CODE" => Array("*"),
		"PRICE_CODE" => Array("MAIN_BASE_PRICE"),
		
		"DISPLAY_BOTTOM_PAGER" => "N",

		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		
		"OFFERS_FIELD_CODE" => [],
		"OFFERS_PROPERTY_CODE" => [],
		"OFFERS_SORT_FIELD" => "",
		"OFFERS_SORT_ORDER" => "",
		"OFFERS_SORT_FIELD2" => "",
		"OFFERS_SORT_ORDER2" => "",
		"OFFERS_LIMIT" => "1000",
		
		"COMPATIBLE_MODE" => "Y",
		
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		


				
						
						"UI_LIST_WRAP_CLASS" => "catalog-list__orthoboom",
						"PREVIEW_PICTURE_WIDTH" => 50,
						"UI_INNER_SHOW" => "Y",
						"UI_PROPERTIES_SHOW" => "N",
						"UI_THUMBNAILS_SHOW" => "Y",
						"UI_PICTURE_SHOW" => "N",
						"UI_PICTURES_SHOW" => "Y",
						"UI_PICTURES_ADD_FIELD_CODE" => "DETAIL_PICTURE",
						"UI_OFFERS_SHOW" => "Y",
						"UI_OFFERS_TITLE_PROPERTY_CODES" => ["OBUV_RAZMER"],



						"UI_THUMBNAILS_SWIPER_SHOW" => "Y",
						"UI_THUMBNAILS_SWIPER_JS_PARAMS" => [
							"direction" => "vertical",
							"slidesPerView" => 4,
						],
						"UI_PICTURES_SWIPER_SHOW" => "Y",


	), false); ?><?
	

}
?>