<?
namespace Uni\Data\Components;

class WishlistUpdate extends \CBitrixComponent
{
	function executeComponent()
	{
		if (! $this->arParams["ELEMENT_ID"])
		{
			$this->includeComponentTemplate();
			return $this->arResult;
		}
		if (! $this->arParams["COMMAND"])
			$this->arParams["COMMAND"] = "TOGGLE";
		
		\Bitrix\Main\Loader::includeModule("sale");
		
		$bElementFound = false;
		$obBasket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());
		$colBasketItems = $obBasket->getBasketItems();
		
		if ($this->arParams["COMMAND"] == "DELETE" || $this->arParams["COMMAND"] == "TOGGLE")
		{
			foreach ($colBasketItems as $obBasketItem) 
			{
				if ($obBasketItem->getField("DELAY") == "Y" && $obBasketItem->getField("PRODUCT_ID") == $this->arParams["ELEMENT_ID"])
				{
					$obBasketItem->delete();
					$obRes = $obBasket->save();
					
					if ($obRes->isSuccess())
						$this->arResult["RESULT_CODE"] = "success";
					else
						$this->arResult["RESULT_CODE"] = "error";
					$this->arResult["RESULT_COMMAND"] = "DELETE";
					
					$bElementFound = true;
				}
			}	
		}
		
		if ($this->arParams["COMMAND"] == "ADD" || ($this->arParams["COMMAND"] == "TOGGLE" && ! $bElementFound))
		{
			
			\Bitrix\Main\Loader::includeModule("catalog");
			$obRes = \Bitrix\Catalog\Product\Basket::addProduct(["PRODUCT_ID" => $this->arParams["ELEMENT_ID"], "QUANTITY" => 1, "DELAY" => "Y", "LID" => \Bitrix\Main\Context::getCurrent()->getSite(), "PRODUCT_PROVIDER_CLASS" => \Bitrix\Catalog\Product\Basket::getDefaultProviderName()]);
			if ($obRes->isSuccess())
				$this->arResult["RESULT_CODE"] = "success";
			else
				$this->arResult["RESULT_CODE"] = "error";
			$this->arResult["RESULT_COMMAND"] = "ADD";
		}
		
		
		//debug($this->arResult);
		$this->includeComponentTemplate();
		return $this->arResult;
	}
}






?>