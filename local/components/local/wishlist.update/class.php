<?
namespace Uni\Data\Components;

class WishlistUpdate extends \CBitrixComponent
{
	function executeComponent()
	{
		$arParams = &$this->arParams;
		$arResult = &$this->arResult;
		
		if (! $arParams["ELEMENT_ID"])
		{
			$this->includeComponentTemplate();
			return $arResult;
		}

		$arResult["ITEMS"] = json_decode(\Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getCookie("FAV_ITEMS"), true);
		
		if ($arParams["COMMAND"] == "ADD")
		{
			$arResult["ITEMS"][$arParams["ELEMENT_ID"]] = ["ID" => $arParams["ELEMENT_ID"]]; 
		}
		elseif ($arParams["COMMAND"] == "DELETE")
		{
			unset($arResult["ITEMS"][$arParams["ELEMENT_ID"]]);
		}
		
		$obCookie = new \Bitrix\Main\Web\Cookie("FAV_ITEMS", json_encode($arResult["ITEMS"], JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES));
		
		$obCookie->setDomain(\Bitrix\Main\Application::getInstance()->getContext()->getServer()->getHttpHost());
		$obCookie->setHttpOnly(false);
		\Bitrix\Main\Application::getInstance()->getContext()->getResponse()->addCookie($obCookie);
		
		\Bitrix\Main\Application::getInstance()->getContext()->getResponse()->writeHeaders();
		
		$this->includeComponentTemplate();

		return $arResult;
	}
}
?>