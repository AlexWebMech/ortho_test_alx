333<? if (! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

$this->setFrameMode(false);

$APPLICATION->RestartBuffer();

echo json_encode($arResult, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES);

die();
?>