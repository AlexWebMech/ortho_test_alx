<?
if (! class_exists("CPortalIBlockElementConvert"))
{
	class CPortalIBlockElementConverter
	{
		function ConvertElementFullToSave($arElement)
		{
			$arElement["PROPERTY_VALUES"] = Array();
			foreach($arElement["PROPERTIES"] as $sPropertyCode => $arPropertyValue)
				if (! isset($arPropertyValue["VALUE_ENUM_ID"]))
					$arElement["PROPERTY_VALUES"][$sPropertyCode] = $arPropertyValue["VALUE"];
				else
					$arElement["PROPERTY_VALUES"][$sPropertyCode] = $arPropertyValue["VALUE_ENUM_ID"];
			return $arElement;
		}
		function ConvertElementShortToSave($arElement)
		{
			foreach($arElement as $sFieldCode => $varField) {
				if (substr($sFieldCode, 0, 9) == "PROPERTY_") {
					$sPropertyCode = substr($sFieldCode, 9);
					$arElement["PROPERTY_VALUES"][$sPropertyCode] = $varField;
				}
			}
			return $arElement;
		}	
		
		function ConvertElementNormalToSave($arElement)
		{
			foreach($arElement as $sFieldCode => $varField) {
				if (substr($sFieleCode, 0, 9) == "PROPERTY_" && substr($sFieleCode, 0, -6) == "_VALUE") {
					$sPropertyCode = substr($sFieleCode, 0, 9);
					$arElement["PROPERTY_VALUES"][$sPropertyCode] = $arElement["PROPERTY_" . $sPropertyCode . "_VALUE_ENUM_ID"] ? $arElement["PROPERTY_" . $sPropertyCode . "_VALUE_ENUM_ID"] : $arElement["PROPERTY_" . $sPropertyCode . "_VALUE"];
				}
			}
			return $arElement;
		}
		
		function ConvertElementNormalToFull($arElement)
		{
			
		}
	}
}

if (! class_exists("CPortalIBlockNotify"))
{
	class CPortalIBlockNotify
	{
		function OnElementSaveSendEMail($arParams, $arItem)
		{
			if (! $arItem["IBLOCK_ID"] || ! $arItem["ID"])
				return false;
			$BR = "\r\n";
			
			$rsItem = CIBlockElement::GetByID($arItem["ID"]);
			$obItem = $rsItem->GetNextElement();
			$arItem = $obItem->GetFields();
			$arItem["PROPERTIES"] = $obItem->GetProperties();
			
			$arMail = Array(
				"TITLE" => ($arParams["TITLE"]) ? $arParams["TITLE"] : "Notify",
				"EMAIL_FROM" => ($arParams["EMAIL_FROM"]) ? $arParams["EMAIL_FROM"] : COption::GetOptionString('main','email_from'),
				"EMAIL_TO" => ($arParams["EMAIL_TO"]) ? $arParams["EMAIL_TO"] : COption::GetOptionString('main','email_from'),
				"MESSAGE" => ($arParams["MESSAGE"]) ? $arParams["MESSAGE"] : "",
			);
			
			$sProperties = "";
			foreach($arItem["PROPERTIES"] as $arProperty)
				if ($arProperty["VALUE"])
					$sProperties .= ((strlen($sProperties)) ? $BR : "") . $arProperty["NAME"] . ": " . $arProperty["~VALUE"];
			if ($sProperties)
				$arMail["MESSAGE"] .= $sProperties;
				
			if (TRUE)
				$arMail["MESSAGE"] .= str_repeat($BR, 2) . "Просмотр: http://" . (($arParams["SERVER_NAME"]) ? $arParams["SERVER_NAME"] : $_SERVER["HTTP_HOST"]) . "/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=" . $arItem["IBLOCK_ID"] . "&ID=" . $arItem["ID"] . "&type=" . $arParams["IBLOCK_TYPE"] . "&lang=ru";
			
			if (! $arParams["EVENT_TYPE"])
				$arParams["EVENT_TYPE"] = "MESSAGE";
			CEvent::Send($arParams["EVENT_TYPE"] ? $arParams["EVENT_TYPE"] : "MESSAGE", $arParams["SITE_ID"] ? $arParams["SITE_ID"] : SITE_ID, $arMail);
		}
	}	
}	
		/*
		function ConvertPropertiesFormatOLD($arParams, $arResult, $arItem, $arSettings)
		{
			if ($arSettings["FORMAT_TO"] == "ELEMENT_SAVE")
			{
				$arSetItem = $arItem;
				foreach($arItem["PROPERTIES"] as $sProperty	=> $arPropertyVal)
				{
					$arProperty = Array();
					$arSetItem["PROPERTY_VALUES"][$sProperty] = ($arProperty["PROPERTY_TYPE"] != "L" || ! isset($arPropertyVal["VALUE_ENUM_ID"])) ? $arPropertyVal["VALUE"] : $arPropertyVal["VALUE_ENUM_ID"];
				}
				if ($arSettings["CLEAR_FLAG"] == "Y")
					unset($arSetItem["PROPERTIES"]);
			
			}
			
			return $arSetItem;
		} */
		
		
?>