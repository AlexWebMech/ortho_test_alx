<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

if (is_array($arResult["ERROR_MESSAGES"]) && count($arResult["ERROR_MESSAGES"])) :
	?><div class="error-messages"><?
	foreach($arResult["ERROR_MESSAGES"] as $arMessage) :
		?><div class="error-message"><?= $arMessage["MESSAGE"] ?></div><?
	endforeach;
	?></div><?
endif;
?><? if (is_array($arResult["SUCCESS_MESSAGES"]) && count($arResult["SUCCESS_MESSAGES"])) :
	?><div class="success-messages"><?
	foreach($arResult["SUCCESS_MESSAGES"] as $arMessage) :
		?><div class="success-message"><?= $arMessage["MESSAGE"] ?></div><?
	endforeach;
	?></div><?
endif;
?>