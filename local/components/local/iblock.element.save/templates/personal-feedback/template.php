<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<form id="feedback-form" action="#feedback-form" class="message-rorm" method="post">

 <h3>Задайте вопрос</h3>
 
<?
if (is_array($arResult["ERROR_MESSAGES"]) && count($arResult["ERROR_MESSAGES"])) :
	?><div class="ui-error-messages"><?
	foreach($arResult["ERROR_MESSAGES"] as $arMessage) :
		?><div class="ui-error-message"><?= $arMessage["MESSAGE"] ?></div><?
	endforeach;
	?></div><?
endif;
?><? if (is_array($arResult["SUCCESS_MESSAGES"]) && count($arResult["SUCCESS_MESSAGES"])) :
	?><div class="ui-success-messages"><?
	foreach($arResult["SUCCESS_MESSAGES"] as $arMessage) :
		?><div class="ui-success-message"><?= $arMessage["MESSAGE"] ?></div><?
	endforeach;
	?></div><?
endif;
?>

	<? if ($arResult["SUCCESS_FLAG"] != "Y") : ?>
   
    <p class="message-gray">Опишите ваш вопрос в текстовом поле и мы обязательно вам поможем!</p>

    <div class="texarea-block">
        <div class="input-block">
            <label>Текст сообщения</label>
            <textarea name="PROPERTY_QUESTION" placeholder="Здравствуйте,...."></textarea>
        </div>

        <button class="gray-button" type="submit">Отправить</button>
    </div>
	<? endif; ?>
</form>

<?// debug($arResult); ?>