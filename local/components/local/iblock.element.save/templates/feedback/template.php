<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<form id="<?= $arParams["WRAP_ID"] ?>" action="/contacts/" data-uni-ajax-params='{"cutElement": "#feedback-form"}' data-uni-ajax-wrap data-uni-form-check class="contacts-form ui-form ui-form--normal ui-form--feedback ui-form--solid1 " onsubmit="" method="post">
	
	<?
	if (is_array($arResult["ERROR_MESSAGES"]) && count($arResult["ERROR_MESSAGES"])) :
		?><div class="error-messages ui-message-category-error"><?
		foreach($arResult["ERROR_MESSAGES"] as $arMessage) :
			?><div class="error-message"><?= $arMessage["MESSAGE"] ?></div><?
		endforeach;
		?></div><?
	endif;
	?><? if (is_array($arResult["SUCCESS_MESSAGES"]) && count($arResult["SUCCESS_MESSAGES"])) :
		?><div class="success-messages ui-message-category-info"><?
		foreach($arResult["SUCCESS_MESSAGES"] as $arMessage) :
			?><div class="success-message"><?= $arMessage["MESSAGE"] ?></div><?
		endforeach;
		?></div><?
		return;
	endif;
	
	//debug($arParams, 'arParams');
	//debug($arResult, 'arResult');
	?>
	
	<? if ($arResult["PROPERTIES"]["NAME"]) : ?>
	<div class="ui-field ui-field-row ui-field-row-default">
		<div class="ui-field-title-col ui-field-col">
			<div class="body"><span class="text"><?= $arResult["PROPERTIES"]["NAME"]["NAME"] ?>:<? if ($arResult["PROPERTIES"]["NAME"]["REQUIRED"] == "Y") : ?><span class="ui-field-required-mark">*</span><? endif; ?></div>
		</div>
		<div class="ui-field-value-col ui-field-col"><div class="body"> 
			<input type="text" name="PROPERTY_NAME" data-title="<?= $arResult["PROPERTIES"]["NAME"]["NAME"] ?>" value="<?= htmlspecialchars($arResult["ITEM_SAVE"]["PROPERTY_NAME"]); ?>" />
		</div></div>
	</div>
	<? endif; ?>
	
	<? if ($arResult["PROPERTIES"]["PHONE"]) : ?>
	<div class="ui-field ui-field-row ui-field-row-default">
		<div class="ui-field-title-col ui-field-col">
			<div class="body"><span class="text"><?= $arResult["PROPERTIES"]["PHONE"]["NAME"] ?>:<? if ($arResult["PROPERTIES"]["PHONE"]["REQUIRED"] == "Y" || 1) : ?><span class="ui-form-required-mark">*</span><? endif; ?></div>
		</div>
		<div class="ui-field-value-col ui-field-col"><div class="body"> 
			<input type="text" name="PROPERTY_PHONE" data-title="<?= $arResult["PROPERTIES"]["PHONE"]["NAME"] ?>" value="<?= htmlspecialchars($arResult["ITEM_SAVE"]["PROPERTY_PHONE"]); ?>" />
		</div></div>
	</div>
	<? endif; ?>

	<? if ($arResult["PROPERTIES"]["COMMENT"]) : ?>
	<div class="ui-field ui-field-row ui-field-row-default">
		<div class="ui-field-title-col ui-field-col">
			<div class="body"><span class="text">Комментарий: <span></span></span></div>
		</div>
		<div class="ui-field-value-col ui-field-col"><div class="body"> 
			<? if (1) : ?>
			<input type="text" name="PROPERTY_COMMENT" data-title="<?= $arResult["PROPERTIES"]["COMMENT"]["NAME"] ?>" value="<?= htmlspecialchars($arResult["ITEM_SAVE"]["PROPERTY_COMMENT"]); ?>" />
			<? else : ?>
			<textarea name="PROPERTY_COMMENT" style="height: 90px"><?= htmlspecialchars($arResult["ITEM_SAVE"]["PROPERTY_COMMENT"]); ?></textarea>
			<? endif; ?>
		</div></div>
	</div>
	<? endif; ?>

	
	<div class="ui-field ui-field-row ui-field-row-default">
		<div class="ui-field-title-col ui-field-col">
			<div class="body"><span class="text"></span></div>
		</div>
		<div class="ui-field-value-col ui-field-col"><div class="body"> 
			<label><input type="checkbox" name="CONF" data-required data-required-error-message="Требуется дать согласие на обработку персональных данных" value="Y" /> Даю согласие на обработку персональных данных</label>
		</div></div>
	</div>
	
	


<?/*
	<div class="ui-field ui-field-row">
	<div class="ui-field-title-col ui-field-col ui-field-title-col ui-field-col" style="display: none"><div calss="body"><span class="text">Примечание:</span></div>
	</div>
	<div class="ui-field-value-col ui-field-col"><div class="body">Нажимая на кнопку "Отправить", я соглашаюсь с <a target="_blank" href="/info/privacy-policy.php" style="color:inherit; text-decoration:underline">политикой конфиденциальности</a> и <a target="_blank" href="/info/user-agreement.php" style="color:inherit; text-decoration:underline">пользовательским соглашением</a>.</div>
	</div>
	</div>
*/?>

	<div class="ui-field ui-field-row">
		<div class="ui-field-title-col ui-field-col"><div class="body">
		
		</div></div>
		<div class="ui-field-value-col ui-field-col"><div class="body"> 
				<input type="submit" value="Отправить" class="ui-button ui-button--normal"> 
		</div>
		</div>
	</div>

</form>
