<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

CModule::IncludeModule("iblock");
CModule::IncludeModule("uni.data");

require_once("functions.php");

$arResult["ERROR_MESSAGES"] = Array();
if (is_array($arParams["ERRORS"]) && count($arParams["ERRORS"])) 
{	
	foreach($arParams["ERRORS"] as $sError)
		$arResult["ERROR_MESSAGES"][] = Array("TYPE" => "ERROR", "MESSAGE" => $sError);
	$arResult["ERROR_FLAG"] = "Y";
}	
if (is_array($arParams["SUCCESS_MESSAGES"]) && count($arParams["SUCCESS_MESSAGES"]))	
	$arResult = $arParams["SUCCESS_MESSAGES"];	
	
if ($arParams["ITEM_ID"])
{
	
}

if ($arParams["IBLOCK_ID"])
{
	$obR = \Bitrix\Iblock\PropertyTable::getList(Array(
		"filter" => Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y"),
		"select" => Array("ID", "CODE", "ACTIVE", "NAME", "PROPERTY_TYPE", "USER_TYPE", "MULTIPLE", "LINK_IBLOCK_ID"),
	));
	while ($arE = $obR->fetch())
		$arResult["PROPERTIES"][strlen($arE["CODE"]) ? $arE["CODE"] : $arE["ID"]] = $arE;
}


if (is_array($arParams["ITEM_SAVE"]) && ! empty($arParams["ITEM_SAVE"]))
{
	if ($arParams["CHECK_SESSION_FLAG"] == "Y" && ! check_bitrix_sessid())
	{
		$arResult["ERROR_FLAG"] = "Y";
		$arResult["ERROR_MESSAGES"][] = Array("TYPE" => "ERROR", "MESSAGE" => "Ваша сессия истекла. Пожалуйста, отправьте запрос еще раз.");
	}
	if ($arParams["CHECK_KEYWORD_FLAG"] == "Y")
	{
		if($arParams["CHECK_KEYWORD"] != $arParams["CHECK_KEYWORD_REQUEST"])
		{
			$arResult["ERROR_FLAG"] = "Y";
			$arResult["ERROR_MESSAGES"][] = Array("TYPE" => "ERROR", "MESSAGE" => "Не удалось отправить данные.");			
		}
		unset($arParams["ITEM_SAVE"]["CHECK"]);
	}
	
	$arResult["ITEM_SAVE"] = $arParams["ITEM_SAVE"];
	
	
	if (is_array($arParams["ITEM_SAVE_MASK"]) && count($arParams["ITEM_SAVE_MASK"]))
	{
		$arSetProperties = false;
		if (is_array($arResult["ITEM_SAVE"]["PROPERTIES"]) && count($arResult["ITEM_SAVE"]["PROPERTIES"]))
			$arSetProperties = $arResult["ITEM_SAVE"]["PROPERTIES"];
		
		$arResult["ITEM_SAVE"] = array_merge($arResult["ITEM_SAVE"], $arParams["ITEM_SAVE_MASK"]);
		
		if (! is_array($arParams["ITEM_SAVE_MASK"]["PROPERTIES"]))
			$arParams["ITEM_SAVE_MASK"]["PROPERTIES"] = Array();
		if (is_array($arSetProperties) && count($arSetProperties))
			$arResult["ITEM_SAVE"]["PROPERTIES"] = array_merge($arSetProperties,  $arParams["ITEM_SAVE_MASK"]["PROPERTIES"]);
	}

	if ($arParams["IBLOCK_ID"])
		$arResult["ITEM_SAVE"]["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
	
	$arResult["ITEM_SAVE"]["ID"] = ($arParams["ITEM_ID"]) ? $arParams["ITEM_ID"] : "";
	
	
	
	if ($arResult["ERROR_FLAG"] != "Y" && ! $arResult["ERROR_MESSAGES"])
	{
		//$arElement = CIBlockItemTools::ConvertPropertiesFormat($arParams, $arResult, $arResult["ITEM_SAVE"], Array("FORMAT_FROM" => "COMPACT", "FORMAT_TO" => "ELEMENT_SAVE", "CLEAR_FLAG" => ""));
		//debug($arElement, 'die arElement (Save)');
		
		$arElement = $arResult["ITEM_SAVE"];
		
		if ($arParams["PROPERTIES_FORMAT"] == "FULL") //$arElement["PROPERTIES"][$sPropertyCode]["VALUE"]
			$arElement = CPortalIBlockElementConverter::ConvertElementFullToSave($arElement);
		elseif ($arParams["PROPERTIES_FORMAT"] == "SHORT") //$arElement["PROPERTY_" . $sPropertyCode]
			$arElement = CPortalIBlockElementConverter::ConvertElementShortToSave($arElement);
		elseif ($arParams["PROPERTIES_FORMAT"] == "NORMAL") //$arElement["PROPERTY_" . $sPropertyCode . "_VALUE"]
			$arElement = CPortalIBlockElementConverter::ConvertElementNormalToSave($arElement);

		//debug($arElement, 'die arElement');
		
		$obElement = new CIBlockElement;
		
		if (! $arElement["ID"]) {
			$arElement["ID"] = $arResult["ITEM_SAVE"]["ID"] = $obElement->Add($arElement);
			$bResult = ($arElement["ID"]) ? true : false;
		} else {
			$arSetProperties = (is_array($arElement["PROPERTY_VALUES"])) ? $arElement["PROPERTY_VALUES"] : false;
			unset($arElement["PROPERTY_VALUES"]);
			$bResult = $obElement->Update($arElement["ID"], $arElement);
			if (is_array($arSetProperties) && count($arSetProperties))
				CIBlockElement::SetPropertyValuesEx($arElement["ID"], $arElement["IBLOCK_ID"], $arSetProperties);		
		}
		if ($arElement["IBLOCK_SECTION_ID"])
			CIBlockElement::SetElementSection($arElement["ID"], Array($arElement["IBLOCK_SECTION_ID"]), false, $arElement["IBLOCK_ID"]);	
		
		//debug($arElement, 'die Save OK');
		
		if ($bResult) {
			$arResult["SUCCESS_FLAG"] = "Y";
			$_REQUEST["SAVE_SUCCESS"] = $_GET["SAVE_SUCCESS"] = "Y";
			$arResult["SUCCESS_MESSAGES"][] = Array("TYPE" => "SUCCESS", "MESSAGE" => ($arParams["SUCCESS_MESSAGE"]) ? $arParams["~SUCCESS_MESSAGE"] : "OK");

			//EMail
			if ($arParams["EMAIL_EVENT_FLAG"] == "Y")
			{
				$arEMailParams = Array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"EVENT_TYPE" => $arParams["EMAIL_EVENT_TYPE"],
					"TITLE" => $arParams["EMAIL_EVENT_TITLE"],
					"EMAIL_FROM" => $arParams["EMAIL_EVENT_EMAIL_FROM"],
					"EMAIL_TO" => $arParams["EMAIL_EVENT_EMAIL_TO"],							
				);
				
				$sMessage = "";
				foreach($arResult["PROPERTIES"] as $sPropertyCode => $arProperty)
				{
					if ($arResult["ITEM_SAVE"]["PROPERTY_" . $sPropertyCode])
						$sMessage .= $arProperty["NAME"] . ": " . $arResult["ITEM_SAVE"]["PROPERTY_" . $sPropertyCode] . "\r\n";
					
					//if ($arProperty["PROPERTY_TYPE"] == "F")
					//	if ()
					
				}
				
				//$sMessage .= "\r\n" . "<a href=\"https://" . SERVER_NAME . "/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=" . $arParams["IBLOCK_ID"] . "&type=" . $arParams["IBLOCK_TYPE"] . "&ID=" . $arElement["ID"] . "&lang=ru&find_section_section=0&WF=Y\">Открыть в административной части</a>";
				$sMessage .= "\r\n" . "https://" . $_SERVER["HTTP_HOST"] . "/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=" . $arParams["IBLOCK_ID"] . "&type=" . $arParams["IBLOCK_TYPE"] . "&ID=" . $arElement["ID"] . "&lang=ru&find_section_section=0&WF=Y - Открыть в административной части";
				
				
				$arEMailParams["MESSAGE"] = $sMessage;
				$arEMailParams["CONTENT"] = $sMessage;
				/* $arEMailElement = Array(
					"ID" => $arElement["ID"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				);
				*/
				
				\Bitrix\Main\Mail\Event::sendImmediate(array(
					"EVENT_NAME" => $arParams["EMAIL_EVENT_TYPE"],
					"LID" => SITE_ID,
					"C_FIELDS" => $arEMailParams,
				));
		
				//CPortalIBlockNotify::OnElementSaveSendEMail($arEMailParams, $arEMailElement);
			}
			
			//debug($arEMailParams);
			
			if ($arParams["SUCCESS_URL"])
			{
				if ($_REQUEST["AJAX_CALL"] != "Y") {
					LocalRedirect(str_replace("#ID#", $arElement["ID"], $arParams["SUCCESS_URL"]));
				} else {
					$APPLICATION->RestartBuffer();
					?><script type="text/javascript">window.location.href = '<?= $arParams["SUCCESS_URL"] ?>';</script><?
					die();
				}
			}
		} else {
			$arResult["ERROR_MESSAGES"][] = Array("TYPE" => "ERROR", "MESSAGE" => $obElement->LAST_ERROR);
		}
	}	
}

if (is_array($arParams["FIELDS"]))
	$arResult["FIELDS"] = $arParams["FIELDS"];







$this->IncludeComponentTemplate();

return $arResult;

//return Array("SAVE_SUCCESS" => "Y");
//debug($arResult, 'arResult');

?>