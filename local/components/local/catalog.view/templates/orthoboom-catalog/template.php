<? if(! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

$this->setFrameMode(false);

//return;
?>


<div class="catalog-view__orthoboom catalog-view">
	
	<div class="catalog-view--order">
		<?
		$arField = $arResult["ORDER"]["FIELD"];
		?>
		
		<span class="catalog-view--order-title">Сортировать:</span>
		
		<span class="catalog-view--order-links catalog-view--links">
		
		
			<a class="catalog-view--order-link catalog-view--link <? if ($arField["VALUE"] == "popular_desc") : ?> active<? endif; ?>" href="?order=popular_desc"><?= $arField["ITEMS"]["popular_desc"]["TITLE"] ?></a>
			
			
			<a class="catalog-view--order-link catalog-view--link <? if ($arField["VALUE"] == "novelty_desc") : ?> active<? endif; ?>" href="?order=novelty_desc"><?= $arField["ITEMS"]["novelty_desc"]["TITLE"] ?></a>
			
			<? if ($arField["VALUE"] == "price_asc") : ?>
				<a class="catalog-view--order-link catalog-view--link active" href="?order=price_desc"><?= $arField["ITEMS"]["price_asc"]["TITLE"] ?></a>
			<? elseif ($arField["VALUE"] == "price_desc") : ?>
				<a class="catalog-view--order-link catalog-view--link active" href="?order=price_asc"><?= $arField["ITEMS"]["price_desc"]["TITLE"] ?></a>
			<? else : ?>
				<a class="catalog-view--order-link catalog-view--link" href="?order=price_asc"><?= $arField["ITEMS"]["price_asc"]["TITLE"] ?></a>
			<? endif; ?>
			
		</span>
	</div>

</div>




