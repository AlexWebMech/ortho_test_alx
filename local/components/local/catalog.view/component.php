<? if(! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

$arResult["ORDER"]["FIELD"] = $arField = [
	"ITEMS" => [
		//"popular_desc" => ["TITLE" => "По популярности", "RETURN" => ["ID" => "DESC"]],
		"novelty_desc" => ["TITLE" => "Сначала новые", "RETURN" => ["PROPERTY_NOVINKA_INTERNET_MAGAZIN" => "ASC,NULLS", "DATE_CREATE" => "DESC", "ID" => "DESC"]],
		"price_asc" => ["TITLE" => "По цене (↑)", "RETURN" => ["PROPERTY_MIN_NORMAL_PRICE" => "ASC,NULLS", "ID" => "DESC"]],
		"price_desc" => ["TITLE" => "По цене (↓)", "RETURN" => ["PROPERTY_MAX_NORMAL_PRICE" => "DESC,NULLS", "ID" => "DESC"]],
		
	],
	"VALUE" => $_SESSION["LOCAL"]["CATALOG"]["VIEW"]["ORDER"],
	"DEFAULT_VALUE" => "novelty_desc",
];

if ($_REQUEST["order"] && $arField["ITEMS"][$_REQUEST["order"]])
{
	$_SESSION["LOCAL"]["CATALOG"]["VIEW"]["ORDER"] = $_REQUEST["order"];
	LocalRedirect($APPLICATION->GetCurPage());
	$arResult["ORDER"]["FIELD"]["VALUE"] = $arField["VALUE"] = $_REQUEST["order"];
}
elseif (! $_SESSION["LOCAL"]["CATALOG"]["VIEW"]["ORDER"])
	$arResult["ORDER"]["FIELD"]["VALUE"] = $arField["VALUE"] = $arResult["ORDER"]["FIELD"]["DEFAULT_VALUE"];


if ($arField["ITEMS"][$arField["VALUE"]])
	$arResult["ORDER"]["RETURN"] = $arField["ITEMS"][$arField["VALUE"]]["RETURN"];

//vard($arResult);

$this->includeComponentTemplate();

return $arResult;

?>