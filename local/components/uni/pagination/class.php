<?
namespace Uni\Data\Components;

class Pagination extends \CBitrixComponent
{
	function executeComponent()
	{
		$arParams = &$this->arParams;
		$arResult = &$this->arResult;
		
		$arParamCodes = [
			"PAGE", "PAGE_COUNT", "LIMIT", "ITEM_COUNT", "VAR_PATH", "PAGE_LIMIT", "PAGE_FROM", "PAGE_TO", "CLEAR_VARS", "CACHE_SESSION",
			"UI_FIRST_NUM_PAGE_SHOW", "UI_FIRST_PAGE_SHOW", "UI_FIRST_PAGE_TITLE", "UI_PREV_PAGE_SHOW", "UI_PREV_PAGE_TITLE", "UI_LAST_NUM_PAGE_SHOW",
			"UI_LAST_NUM_PAGE_SHOW", "UI_NEXT_PAGE_SHOW", "UI_NEXT_PAGE_TITLE", "UI_LAST_PAGE_SHOW", "UI_LAST_PAGE_TITLE", "UI_LAST_PAGE_NUMBER_TITLE",
			"UI_MORE_SHOW", "UI_MORE_TITLE",
			"UI_LIST_ITEMS_CLASS", "UI_LIST_ITEM_CLASS",
		];
		foreach($arParamCodes as $sParam)
			if ($arParams[$sParam])
				$arResult[$sParam] = $arParams[$sParam];
	
		if (is_object($obOldNav = $arParams["NAV_OBJECT"]) && $arParams["NAV_OBJECT"] instanceof \CDBResult)
		{
			$arResult["PAGE"] = $obOldNav->NavPageNomer;
			$arResult["PAGE_COUNT"] = $obOldNav->NavPageCount;
			$arResult["LIMIT"] = $obOldNav->NavPageSize;
			$arResult["ITEM_COUNT"] = $obUiNav->NavRecordCount;
			if (! check_string($arParams["VAR_PATH"]))
				$arResult["VAR_PATH"] = "PAGEN_" . $obOldNav->NavNum;
		}
		if (($obUiNav = $arParams["NAV_OBJECT"]) && $arParams["NAV_OBJECT"] instanceof \Bitrix\Main\UI\PageNavigation)
		{
			$arResult["PAGE"] = $obUiNav->getCurrentPage() > 0 ? $obUiNav->getCurrentPage() : 1;
			$arResult["PAGE_COUNT"] = $obUiNav->getPageCount();
			$arResult["LIMIT"] = $obUiNav->getPageSize();
			$arResult["ITEM_COUNT"] = $obUiNav->getRecordCount();
			if (! check_var($arResult["VAR_PATH"]))
				$arResult["VAR_PATH"] = "page";
		}
		
		if (! is_numeric($arResult["PAGE"]))
		{
			if (is_array($arResult["VAR_PATH"]))
				$arResult["PAGE"] = \Uni\Data\Tools\FlowVar::get($_REQUEST, $arResult["VAR_PATH"]);
			else
				$arResult["PAGE"] = $_REQUEST[$arResult["VAR_PATH"]];
		}
		if (! is_numeric($arResult["PAGE"]))
			$arResult["PAGE"] = 1;
		
		if (! is_numeric($arResult["PAGE_LIMIT"]))
			$arResult["PAGE_LIMIT"] = 3;		
		
		if (! $arResult["PAGE_FROM"])
			$arResult["PAGE_FROM"] = 1;
		if ($arResult["PAGE"] > $arResult["PAGE_LIMIT"])
			$arResult["PAGE_FROM"] = $arResult["PAGE"] - $arResult["PAGE_LIMIT"];
		
		if (! $arResult["PAGE_TO"])
			$arResult["PAGE_TO"] = $arResult["PAGE_COUNT"];
		if ($arResult["PAGE_TO"] > $arResult["PAGE"] + $arResult["PAGE_LIMIT"])
			$arResult["PAGE_TO"] = $arResult["PAGE"] + $arResult["PAGE_LIMIT"];	
	
		if (! is_array($arResult["CLEAR_VARS"]))
			$arResult["CLEAR_VARS"] = [];
		if (is_array($arResult["VAR_PATH"]))
		{
			$arResult["PAGE_VAR_NAME"] = "";
			foreach(array_values($arResult["VAR_PATH"]) as $i => $sVarName)
				$arResult["PAGE_VAR_NAME"] .= (($i === 0) ? $sVarName : '[' . $sVarName . ']');
			$arResult["CLEAR_VARS"][] = $arResult["PAGE_VAR_NAME"];
		}
		else
		{
			$arResult["PAGE_VAR_NAME"] = $arResult["VAR_PATH"];
			$arResult["CLEAR_VARS"][] = $arResult["PAGE_VAR_NAME"];
		}
		
		//Ajax
		$arResult["UI_ITEM_ATTRS_STR"] = "";
		if ($arParams["UNI_AJAX_MODE"] == "Y" || $arParams["ITEM_AJAX_PROFILE"])
			$arResult["UI_ITEM_ATTRS_STR"] .= " data-uni-ajax-action";
		if ($arParams["ITEM_AJAX_PROFILE"])
			$arResult["UI_ITEM_ATTRS_STR"] .= " data-uni-ajax-profile=\"" . htmlspecialchars($arParams["ITEM_AJAX_PROFILE"]) . "\"";
		
		
		//Items
		$arResult["ITEMS"] = Array();
		for($iPage = $arResult["PAGE_FROM"]; $iPage <= $arResult["PAGE_TO"]; $iPage++)
		{
			$arItem = Array(
				"TITLE" => $iPage, 
				"URL" => $this->getPageUrl($iPage),
				"TAG_NAME" => "a",
			);
			if ($arResult["PAGE"] == $iPage)
				$arItem["SELECTED"] = "Y";
			
			$arResult["ITEMS"][$iPage] = $arItem;
		}
		
		//Special items
		$arResult["SPECIAL_ITEMS"] = [];
		
		$arSpecialItemCodes = ["FIRST_PAGE", "PREV_PAGE", "NEXT_PAGE", "LAST_PAGE", "MORE"];
		foreach($arSpecialItemCodes as $sSpecialItemCode)
			if (! check_string($arResult[$sParamCode = "UI_" . $sSpecialItemCode . "_SHOW"]))
				$arResult[$sParamCode] = "Y";
		
		if ($arResult["UI_FIRST_PAGE_SHOW"] == "Y")
		{
			$i = 1;
			$bDisabled = ($arResult["PAGE"] <= 1) ? true : false;
			$arItem = Array(
				"TITLE" => isset($arResult["UI_FIRST_PAGE_TITLE"]) ? $arResult["UI_FIRST_PAGE_TITLE"] : "",
				"URL" => $this->getPageUrl($i),
				"DISABLED" => ($arResult["PAGE"] == $i) ? "Y" : "N",
				"SELECTED" => ($arResult["PAGE"] == $i) ? "Y" : "N",
				"TAG_NAME" => ! $bDisabled ? "a" : "span",
			);
			$arResult["SPECIAL_ITEMS"]["FIRST_PAGE"] = $arItem;
		}
		if ($arResult["UI_FIRST_NUM_PAGE_SHOW"] == "Y")
		{
			$i = 1;
			$bDisabled = 1 >= $arResult["PAGE"] - $arResult["PAGE_LIMIT"] ? true : false;
			$arItem = Array(
				"TITLE" => $i,
				"URL" => $this->getPageUrl($i),
				"DISABLED" => $bDisabled,
				"TAG_NAME" => ! $bDisabled ? "a" : "span",
			);
			$arResult["SPECIAL_ITEMS"]["FIRST_NUM_PAGE"] = $arItem;
		}		
		
		if ($arResult["UI_PREV_PAGE_SHOW"] == "Y")
		{
			$i = $arResult["PAGE"] - 1;
			$bDisabled = ($i <= 0) ? true : false;
			$arItem = Array(
				"TITLE" => isset($arResult["UI_PREV_PAGE_TITLE"]) ? $arResult["UI_PREV_PAGE_TITLE"] : "",
				"URL" => $this->getPageUrl($i),
				"DISABLED" => $bDisabled ? "Y" : "N",
				"TAG_NAME" => ! $bDisabled ? "a" : "span",
			);
			$arResult["SPECIAL_ITEMS"]["PREV_PAGE"] = $arItem;
		}	
		if ($arResult["UI_NEXT_PAGE_SHOW"] == "Y")
		{
			$i = $arResult["PAGE"] + 1;
			$bDisabled = ($i > $arResult["PAGE_COUNT"]) ? true : false;
			$arItem = Array(
				"TITLE" => isset($arResult["UI_NEXT_PAGE_TITLE"]) ? $arResult["UI_NEXT_PAGE_TITLE"] : "",
				"URL" => $this->getPageUrl($i),
				"DISABLED" => $bDisabled ? "Y" : "N",
				"TAG_NAME" => ! $bDisabled ? "a" : "span",
			);
			$arResult["SPECIAL_ITEMS"]["NEXT_PAGE"] = $arItem;
		}
		if ($arResult["UI_LAST_NUM_PAGE_SHOW"] == "Y")
		{
			$i = $arResult["PAGE_COUNT"];
			$bDisabled = $arResult["PAGE_COUNT"] <= $arResult["PAGE"] + $arResult["PAGE_LIMIT"] ? true : false;
			$arItem = Array(
				"TITLE" => $i,
				"URL" => $this->getPageUrl($i),
				"DISABLED" => $bDisabled,
				"TAG_NAME" => ! $bDisabled ? "a" : "span",
			);
			$arResult["SPECIAL_ITEMS"]["LAST_NUM_PAGE"] = $arItem;
		}
		
		if ($arResult["UI_LAST_PAGE_SHOW"] == "Y")
		{
			$i = $arResult["PAGE_COUNT"];
			$bDisabled = ($arResult["PAGE"] >= $arResult["PAGE_COUNT"]) ? true : false;
			$arItem = Array(
				"TITLE" => isset($arResult["UI_LAST_PAGE_TITLE"]) ? $arResult["UI_LAST_PAGE_TITLE"] : "",
				"URL" => $this->getPageUrl($i),
				"SELECTED" => ($arResult["PAGE"] == $i) ? "Y" : "N",
				"TAG_NAME" => ! $bDisabled ? "a" : "span",
			);
			if ($arParams["UI_LAST_PAGE_NUMBER_TITLE"] == "Y")
				$arItem["TITLE"] = $arResult["PAGE_COUNT"];
			$arResult["SPECIAL_ITEMS"]["LAST_PAGE"] = $arItem;
		}
		
		if ($arResult["UI_MORE_SHOW"] == "Y")
		{
			$i = $arResult["PAGE"] + 1;
			$bDisabled = ($i > $arResult["PAGE_COUNT"]) ? true : false;			
			$arItem = Array(
				"TITLE" => isset($arResult["UI_MORE_TITLE"]) ? $arResult["UI_MORE_TITLE"] : "",
				"URL" => $this->getPageUrl($i),
				"DISABLED" => $bDisabled ? "Y" : "N",
				"TAG_NAME" => "a",
			);
			$arResult["SPECIAL_ITEMS"]["MORE"] = $arItem;
		}
		$this->IncludeComponentTemplate();
	}
	
	public function getPageUrl($iPage)
	{
		$arResult = $this->arResult;
		
		return $GLOBALS["APPLICATION"]->GetCurPageParam(($iPage > 1) || $arResult["CACHE_SESSION"] == "Y" ? $arResult["PAGE_VAR_NAME"] . "=" . $iPage : "", $arResult["CLEAR_VARS"]);
		
		
		//return ($iPage <= $arResult["PAGE_COUNT"]) ? $GLOBALS["APPLICATION"]->GetCurPageParam(($iPage > 1) ? $sPageVarName . "=" . $iPage : "", $arResult["CLEAR_VARS"]) : "";
	}
}
?>