	<? if ($arResult["UI_MORE_SHOW"] == "Y" && ! $arParams["MORE_AJAX_PROFILE"] && ! $arParams["MORE_AJAX_PARAMS"] && $arParams["UI_LIST_WRAP_CLASS"] && $arParams["UI_ITEMS_CLASS"]) : ?>
	<script type="text/javascript">
	uniAjaxLib.addProfile("<?= $arParams["UI_MAIN_CLASS"] ? $arParams["UI_MAIN_CLASS"] : "items" ?>-more", {
		wrapTarget: ".<?= $arParams["UI_LIST_WRAP_CLASS"] ?>",
		url: "<?= $arResult["SPECIAL_ITEMS"]["MORE"]["URL"] ?>",
		cut: true,
		commands: [
			{target: ".<?= htmlspecialchars($arParams["UI_ITEMS_CLASS"]); ?>", cut: true, command: "append"},
			{target: ".ui-pagination", cut: true, command: "html"},
		],
	});
	</script>
	<? endif; ?>