<? if(! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

$this->setFrameMode(true);

//debug($arResult);

if (! $arParams["UI_WRAP_CLASS"])
	$arParams["UI_WRAP_CLASS"] = "ui-pagination--box";

if ($arResult["PAGE_COUNT"] <= 1)
	return;

foreach($arResult["SPECIAL_ITEMS"] as $sItemCode => $arItem)
	if (! check_string($arItem["TITLE"]))
		if (check_string($sMessage = \Bitrix\Main\Localization\Loc::getMessage("UNI_COMPONENT_PAGINATION_SPECIAL_ITEM_" . $sItemCode . "_TITLE")))
			$arResult["SPECIAL_ITEMS"][$sItemCode]["TITLE"] = $sMessage;
?>
<div class="ui-pagination <?= $arParams["UI_WRAP_CLASS"] ?>" data-page="<?= htmlspecialchars($arResult["PAGE"]); ?>" data-var-name="<?= htmlspecialchars($arResult["PAGE_VAR_NAME"]); ?>">

	<? if ($arResult["UI_MORE_SHOW"] == "Y") : 
	$arItem = $arResult["SPECIAL_ITEMS"]["MORE"];
	?>
	<div class="ui-pagination--more">
		<<?= $arItem["TAG_NAME"] == "a" ? "a" : "span"; ?> <? if ($arItem["TAG_NAME"] == "a") : ?>href="<?= htmlspecialchars($arItem["URL"]); ?>"<? endif; ?> class="ui-pagination--more-link ui-pagination--item<? if ($arItem["DISABLED"] == "Y") : ?> disabled<? endif; ?>" data-uni-ajax-action data-uni-ajax-profile="<?= $arParams["MORE_AJAX_PROFILE"] ?>"><?= $arItem["TITLE"] ?></<?= $arItem["TAG_NAME"] == "a" ? "a" : "span"; ?>>
	</div>
	<? endif; ?>
	
	<div class="ui-pagination--items">

		<? if ($arResult["UI_FIRST_PAGE_SHOW"] == "Y") : //First page
		$arItem = $arResult["SPECIAL_ITEMS"]["FIRST_PAGE"]; 
		$arItem["TAG_ATTR_CLASS"] = "ui-pagination--first-page ui-pagination--special-item ui-pagination--item"; ?>
			<<?= $arItem["TAG_NAME"] == "a" ? "a" : "span"; ?> <? if ($arItem["TAG_NAME"] == "a") : ?>href="<?= htmlspecialchars($arItem["URL"]); ?>"<? endif; ?> class="<?= $arItem["TAG_ATTR_CLASS"] ?><? if ($arItem["SELECTED"] == "Y") : ?> active<? endif; ?><? if ($arItem["DISABLED"] == "Y") : ?> disabled<? endif; ?>"<?= $arResult["UI_ITEM_ATTRS_STR"] ?>><span class="ui-icon"></span><span class="ui-text"><?= $arItem["TITLE"] ?></span></<?= $arItem["TAG_NAME"] == "a" ? "a" : "span"; ?>>
		<? endif; ?>
		
		<span class="ui-pagination--first-grow ui-pagination--grow"></span>
		
		<? if ($arResult["UI_PREV_PAGE_SHOW"] == "Y") : //Previous page
		$arItem = $arResult["SPECIAL_ITEMS"]["PREV_PAGE"]; 
		$arItem["TAG_ATTR_CLASS"] = "ui-pagination--prev-page ui-pagination--special-item ui-pagination--item"; ?>
			<<?= $arItem["TAG_NAME"] == "a" ? "a" : "span"; ?> <? if ($arItem["TAG_NAME"] == "a") : ?>href="<?= htmlspecialchars($arItem["URL"]); ?>"<? endif; ?> class="<?= $arItem["TAG_ATTR_CLASS"] ?><? if ($arItem["SELECTED"] == "Y") : ?> active<? endif; ?><? if ($arItem["DISABLED"] == "Y") : ?> disabled<? endif; ?>"<?= $arResult["UI_ITEM_ATTRS_STR"] ?>><span class="ui-icon"></span><span class="ui-text"><?= $arItem["TITLE"] ?></span></<?= $arItem["TAG_NAME"] == "a" ? "a" : "span"; ?>>
		<? endif; ?>
		
		<span class="ui-pagination--before-grow ui-pagination--grow"></span>
	
		<? if ($arResult["UI_FIRST_NUM_PAGE_SHOW"] == "Y") : //First number page
		$arItem = $arResult["SPECIAL_ITEMS"]["FIRST_NUM_PAGE"]; 
		$arItem["TAG_ATTR_CLASS"] = "ui-pagination--first-number-item ui-pagination--number-item ui-pagination--item"; ?>
			<<?= $arItem["TAG_NAME"] == "a" ? "a" : "span"; ?> <? if ($arItem["TAG_NAME"] == "a") : ?>href="<?= htmlspecialchars($arItem["URL"]); ?>"<? endif; ?> class="<?= $arItem["TAG_ATTR_CLASS"] ?><? if ($arItem["SELECTED"] == "Y") : ?> active<? endif; ?><? if ($arItem["DISABLED"] == "Y") : ?> disabled<? endif; ?>"<?= $arResult["UI_ITEM_ATTRS_STR"] ?>><span class="ui-icon"></span><span class="ui-text"><?= $arItem["TITLE"] ?></span></<?= $arItem["TAG_NAME"] == "a" ? "a" : "span"; ?>>
			<span class="ui-pagination--first-number-page-grow<? if ($arItem["DISABLED"] == "Y") : ?> disabled<? endif; ?>"></span>
		<? endif; ?>
	
		<? foreach($arResult["ITEMS"] as $arItem) : //Number pages
			$arItem["TAG_ATTR_CLASS"] = "ui-pagination--number-item ui-pagination--item"; ?>
			<<?= $arItem["TAG_NAME"] == "a" ? "a" : "span"; ?> <? if ($arItem["TAG_NAME"] == "a") : ?>href="<?= htmlspecialchars($arItem["URL"]); ?>"<? endif; ?> class="<?= $arItem["TAG_ATTR_CLASS"] ?><? if ($arItem["SELECTED"] == "Y") : ?> active<? endif; ?><? if ($arItem["DISABLED"] == "Y") : ?> disabled<? endif; ?>"<?= $arResult["UI_ITEM_ATTRS_STR"] ?>><span class="ui-icon"></span><span class="ui-text"><?= $arItem["TITLE"] ?></span></<?= $arItem["TAG_NAME"] == "a" ? "a" : "span"; ?>>
		<? endforeach; ?>
	
		<? if ($arResult["UI_LAST_NUM_PAGE_SHOW"] == "Y") :  //Last number page
		$arItem = $arResult["SPECIAL_ITEMS"]["LAST_NUM_PAGE"]; 
		$arItem["TAG_ATTR_CLASS"] = "ui-pagination--last-number-item ui-pagination--number-item ui-pagination--item"; ?>
			<span class="ui-pagination--last-number-page-grow<? if ($arItem["DISABLED"] == "Y") : ?> disabled<? endif; ?>"></span>
			<<?= $arItem["TAG_NAME"] == "a" ? "a" : "span"; ?> <? if ($arItem["TAG_NAME"] == "a") : ?>href="<?= htmlspecialchars($arItem["URL"]); ?>"<? endif; ?> class="<?= $arItem["TAG_ATTR_CLASS"] ?><? if ($arItem["SELECTED"] == "Y") : ?> active<? endif; ?><? if ($arItem["DISABLED"] == "Y") : ?> disabled<? endif; ?>"<?= $arResult["UI_ITEM_ATTRS_STR"] ?>><span class="ui-icon"></span><span class="ui-text"><?= $arItem["TITLE"] ?></span></<?= $arItem["TAG_NAME"] == "a" ? "a" : "span"; ?>>
		<? endif; ?>
	
		<span class="ui-pagination--after-grow ui-pagination--grow"></span>
		
		<? if ($arResult["UI_NEXT_PAGE_SHOW"] == "Y") : //Next page
		$arItem = $arResult["SPECIAL_ITEMS"]["NEXT_PAGE"]; 
		$arItem["TAG_ATTR_CLASS"] = "ui-pagination--next-page ui-pagination--special-item ui-pagination--item"; ?>
			<<?= $arItem["TAG_NAME"] == "a" ? "a" : "span"; ?> <? if ($arItem["TAG_NAME"] == "a") : ?>href="<?= htmlspecialchars($arItem["URL"]); ?>"<? endif; ?> class="<?= $arItem["TAG_ATTR_CLASS"] ?><? if ($arItem["SELECTED"] == "Y") : ?> active<? endif; ?><? if ($arItem["DISABLED"] == "Y") : ?> disabled<? endif; ?>"<?= $arResult["UI_ITEM_ATTRS_STR"] ?>><span class="ui-icon"></span><span class="ui-text"><?= $arItem["TITLE"] ?></span></<?= $arItem["TAG_NAME"] == "a" ? "a" : "span"; ?>>
		<? endif; ?>
		
		<span class="ui-pagination--last-grow ui-pagination--grow"></span>
		
		<? if ($arResult["UI_LAST_PAGE_SHOW"] == "Y") : //Last page
		$arItem = $arResult["SPECIAL_ITEMS"]["LAST_PAGE"]; 
		$arItem["TAG_ATTR_CLASS"] = "ui-pagination--last-page ui-pagination--special-item ui-pagination--item"; ?>
			<<?= $arItem["TAG_NAME"] == "a" ? "a" : "span"; ?> <? if ($arItem["TAG_NAME"] == "a") : ?>href="<?= htmlspecialchars($arItem["URL"]); ?>"<? endif; ?> class="<?= $arItem["TAG_ATTR_CLASS"] ?><? if ($arItem["SELECTED"] == "Y") : ?> active<? endif; ?><? if ($arItem["DISABLED"] == "Y") : ?> disabled<? endif; ?>"<?= $arResult["UI_ITEM_ATTRS_STR"] ?>><span class="ui-icon"></span><span class="ui-text"><?= $arItem["TITLE"] ?></span></<?= $arItem["TAG_NAME"] == "a" ? "a" : "span"; ?>>
		<? endif; ?>
		
	</div>
		
</div>

