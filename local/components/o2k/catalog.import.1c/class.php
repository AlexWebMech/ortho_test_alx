<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CBitrixComponent::includeComponentClass("bitrix:catalog.import.1c");
//Наследник расширяющий функциональность:
class CO2KCatalogImport1C extends CBitrixCatalogImport1C
{
    const XML_TREE_TABLE_NAME = 'b_xml_tree_import_1c_2';
}

class CIBlockCMLO2KImport extends CIBlockCMLImport
{
    var $basePriceXmlId = false;
    var $secondaryPriceXmlId = false;
    var $forcedWarehouse = false;

    // переопределяем сохранение остатков - вместо записи пришедшего остатка будем прибавлять его к имеющемуся
    function ImportElementPrices($arXMLElement, &$counter, $arParent = false)
    {
        /** @global CMain $APPLICATION */
        global $APPLICATION;
        static $catalogs = array();

        $arElement = array(
            "ID" => 0,
            "XML_ID" => $arXMLElement[$this->mess["IBLOCK_XML2_ID"]],
        );

        $hashPosition = strrpos($arElement["XML_ID"], "#");
        if (
            $this->use_offers
            && $hashPosition === false && !$this->force_offers
            && isset($this->PROPERTY_MAP["CML2_LINK"])
            && isset($this->arProperties[$this->PROPERTY_MAP["CML2_LINK"]])
        )
        {
            $IBLOCK_ID = $this->arProperties[$this->PROPERTY_MAP["CML2_LINK"]]["LINK_IBLOCK_ID"];
            if (!isset($catalogs[$IBLOCK_ID]))
            {
                $catalogs[$IBLOCK_ID] = true;

                $rs = CCatalog::GetList(array(),array("IBLOCK_ID" => $IBLOCK_ID));
                if (!$rs->Fetch())
                {
                    $obCatalog = new CCatalog();
                    $boolFlag = $obCatalog->Add(array(
                        "IBLOCK_ID" => $IBLOCK_ID,
                        "YANDEX_EXPORT" => "N",
                        "SUBSCRIPTION" => "N",
                    ));
                    if (!$boolFlag)
                    {
                        if ($ex = $APPLICATION->GetException())
                            $this->LAST_ERROR = $ex->GetString();
                        return 0;
                    }
                }
            }
        }
        else
        {
            $IBLOCK_ID = $this->next_step["IBLOCK_ID"];
        }

        $obElement = new CIBlockElement;
        $rsElement = $obElement->GetList(
            Array("ID"=>"asc"),
            Array("=XML_ID" => $arElement["XML_ID"], "IBLOCK_ID" => $IBLOCK_ID),
            false, false,
            Array("ID", "TMP_ID", "ACTIVE")
        );
        $arDBElement = $rsElement->Fetch();
        if($arDBElement)
            $arElement["ID"] = $arDBElement["ID"];

        if(isset($arXMLElement[$this->mess["IBLOCK_XML2_STORE_AMOUNT_LIST"]]))
        {
            $arElement["STORE_AMOUNT"] = array();
            foreach($arXMLElement[$this->mess["IBLOCK_XML2_STORE_AMOUNT_LIST"]] as $storeAmount)
            {
                if(isset($storeAmount[$this->mess["IBLOCK_XML2_STORE_ID"]]))
                {
                    $storeXMLID = $storeAmount[$this->mess["IBLOCK_XML2_STORE_ID"]];
                    $amount = $this->ToFloat($storeAmount[$this->mess["IBLOCK_XML2_AMOUNT"]]);
                    $arElement["STORE_AMOUNT"][$storeXMLID] = $amount;
                }
            }
        }
        elseif(isset($arXMLElement[$this->mess["IBLOCK_XML2_RESTS"]]))
        {
            $arElement["STORE_AMOUNT"] = array();
            foreach($arXMLElement[$this->mess["IBLOCK_XML2_RESTS"]] as $xmlRest)
            {
                foreach($xmlRest as $storeAmount)
                {
                    if(is_array($storeAmount))
                    {
                        if (isset($storeAmount[$this->mess["IBLOCK_XML2_ID"]]))
                        {
                            $storeXMLID = $storeAmount[$this->mess["IBLOCK_XML2_ID"]];
                            $amount = $this->ToFloat($storeAmount[$this->mess["IBLOCK_XML2_AMOUNT"]]);
                            $arElement["STORE_AMOUNT"][$storeXMLID] = $amount;
                        }
                    }
                    else
                    {
                        if (strlen($storeAmount) > 0)
                        {
                            if($this->forcedWarehouse) {
                                $storeXMLID = $this->forcedWarehouse;
                                $amount = $this->ToFloat($storeAmount);
                                $arElement["STORE_AMOUNT"][$storeXMLID] = $amount;
                            }
                            else {
                                $amount = $this->ToFloat($storeAmount);
                                $arElement["QUANTITY"] = $amount;
                            }
                        }
                    }
                }
            }
        }
        elseif(
            $arParent
            && (
                array_key_exists($this->mess["IBLOCK_XML2_STORES"], $arXMLElement)
                || array_key_exists($this->mess["IBLOCK_XML2_STORE"], $arXMLElement)
            )
        )
        {
            $arElement["STORE_AMOUNT"] = array();
            $rsStores = $this->_xml_file->GetList(
                array("ID" => "asc"),
                array(
                    "><LEFT_MARGIN" => array($arParent["LEFT_MARGIN"], $arParent["RIGHT_MARGIN"]),
                    "NAME" => $this->mess["IBLOCK_XML2_STORE"],
                ),
                array("ID", "ATTRIBUTES")
            );
            while ($arStore = $rsStores->Fetch())
            {
                if(strlen($arStore["ATTRIBUTES"]) > 0)
                {
                    $info = unserialize($arStore["ATTRIBUTES"]);
                    if(
                        is_array($info)
                        && array_key_exists($this->mess["IBLOCK_XML2_STORE_ID"], $info)
                        && array_key_exists($this->mess["IBLOCK_XML2_STORE_AMOUNT"], $info)
                    )
                    {
                        $arElement["STORE_AMOUNT"][$info[$this->mess["IBLOCK_XML2_STORE_ID"]]] = $this->ToFloat($info[$this->mess["IBLOCK_XML2_STORE_AMOUNT"]]);
                    }
                }
            }
        }

        if(isset($arElement["STORE_AMOUNT"])) {
            $this->ImportStoresAmount($arElement["STORE_AMOUNT"], $arElement["ID"], $counter);
            $this->UpdateProductQuantity($arElement["ID"]);
        }

        if($arDBElement)
        {
            $arProduct = array(
                "ID" => $arElement["ID"],
            );

            if(isset($arXMLElement[$this->mess["IBLOCK_XML2_PRICES"]]))
            {
                $arElement["PRICES"] = array();
                foreach($arXMLElement[$this->mess["IBLOCK_XML2_PRICES"]] as $price)
                {
                    if(
                        isset($price[$this->mess["IBLOCK_XML2_PRICE_TYPE_ID"]])
                        && array_key_exists($price[$this->mess["IBLOCK_XML2_PRICE_TYPE_ID"]], $this->PRICES_MAP)
                    )
                    {
                        $price["PRICE"] = $this->PRICES_MAP[$price[$this->mess["IBLOCK_XML2_PRICE_TYPE_ID"]]];
                        $arElement["PRICES"][] = $price;

                        if(
                            array_key_exists($this->mess["IBLOCK_XML2_MEASURE"], $price)
                            && !isset($arProduct["MEASURE"])
                        )
                        {
                            $tmp = $this->convertBaseUnitFromXmlToPropertyValue($price[$this->mess["IBLOCK_XML2_MEASURE"]]);
                            if ($tmp["DESCRIPTION"] > 0)
                                $arProduct["MEASURE"] = $tmp["DESCRIPTION"];
                        }
                    }
                }

                $arElement["DISCOUNTS"] = array();
                if(isset($arXMLElement[$this->mess["IBLOCK_XML2_DISCOUNTS"]]))
                {
                    foreach($arXMLElement[$this->mess["IBLOCK_XML2_DISCOUNTS"]] as $discount)
                    {
                        if(
                            isset($discount[$this->mess["IBLOCK_XML2_DISCOUNT_CONDITION"]])
                            && $discount[$this->mess["IBLOCK_XML2_DISCOUNT_CONDITION"]] === $this->mess["IBLOCK_XML2_DISCOUNT_COND_VOLUME"]
                        )
                        {
                            $discount_value = $this->ToInt($discount[$this->mess["IBLOCK_XML2_DISCOUNT_COND_VALUE"]]);
                            $discount_percent = $this->ToFloat($discount[$this->mess["IBLOCK_XML2_DISCOUNT_COND_PERCENT"]]);
                            if($discount_value > 0 && $discount_percent > 0)
                                $arElement["DISCOUNTS"][$discount_value] = $discount_percent;
                        }
                    }
                }
            }

            if($this->bCatalog && array_key_exists($this->mess["IBLOCK_XML2_AMOUNT"], $arXMLElement))
            {
                $arElementTmp = array();
                $arElement["QUANTITY_RESERVED"] = 0;
                if($arElement["ID"])
                    $arElementTmp = CCatalogProduct::GetByID($arElement["ID"]);
                if(is_array($arElementTmp) && !empty($arElementTmp) && isset($arElementTmp["QUANTITY_RESERVED"]))
                    $arElement["QUANTITY_RESERVED"] = $arElementTmp["QUANTITY_RESERVED"];
                if($this->forcedWarehouse) {
                    // резервирование влияет только на общий остаток, поэтому при записи в склад его не учитываем
                    $arElement["STORE_AMOUNT"][$this->forcedWarehouse] = $this->ToFloat($arXMLElement[$this->mess["IBLOCK_XML2_AMOUNT"]]);
                }
                else {
                    $arElement["QUANTITY"] = $this->ToFloat($arXMLElement[$this->mess["IBLOCK_XML2_AMOUNT"]]) - doubleval($arElement["QUANTITY_RESERVED"]);
                }
            }

            if(isset($arElement["PRICES"]) && $this->bCatalog)
            {
                if(isset($arElement["QUANTITY"]))
                    $arProduct["QUANTITY"] = $arElement["QUANTITY"];
                elseif(isset($arElement["STORE_AMOUNT"]) && !empty($arElement["STORE_AMOUNT"]))
                    $arProduct["QUANTITY"] = array_sum($arElement["STORE_AMOUNT"]);

                $rsWeight = CIBlockElement::GetProperty($IBLOCK_ID, $arElement["ID"], array(), array("CODE" => "CML2_TRAITS"));
                while($arWeight = $rsWeight->Fetch())
                {
                    if($arWeight["DESCRIPTION"] == $this->mess["IBLOCK_XML2_WEIGHT"])
                        $arProduct["WEIGHT"] = $this->ToFloat($arWeight["VALUE"])*1000;
                }

                $rsUnit = CIBlockElement::GetProperty($IBLOCK_ID, $arElement["ID"], array(), array("CODE" => "CML2_BASE_UNIT"));
                while($arUnit = $rsUnit->Fetch())
                {
                    if($arUnit["DESCRIPTION"] > 0)
                        $arProduct["MEASURE"] = $arUnit["DESCRIPTION"];
                }

                //Here start VAT handling

                //Check if all the taxes exists in BSM catalog
                $arTaxMap = array();
                $rsTaxProperty = CIBlockElement::GetProperty($IBLOCK_ID, $arElement["ID"], array("sort" => "asc"), array("CODE" => "CML2_TAXES"));
                while($arTaxProperty = $rsTaxProperty->Fetch())
                {
                    if(
                        strlen($arTaxProperty["VALUE"]) > 0
                        && strlen($arTaxProperty["DESCRIPTION"]) > 0
                        && !array_key_exists($arTaxProperty["DESCRIPTION"], $arTaxMap)
                    )
                    {
                        $arTaxMap[$arTaxProperty["DESCRIPTION"]] = array(
                            "RATE" => $this->ToFloat($arTaxProperty["VALUE"]),
                            "ID" => $this->CheckTax($arTaxProperty["DESCRIPTION"], $this->ToFloat($arTaxProperty["VALUE"])),
                        );
                    }
                }

                //Try to search in main element
                if (
                    !$arTaxMap
                    && $this->use_offers
                    && $hashPosition !== false
                    && $this->arProperties[$this->PROPERTY_MAP["CML2_LINK"]]["LINK_IBLOCK_ID"] > 0
                )
                {
                    $rsLinkProperty = CIBlockElement::GetProperty($IBLOCK_ID, $arElement["ID"], array("sort" => "asc"), array("CODE" => "CML2_LINK"));
                    if( ($arLinkProperty = $rsLinkProperty->Fetch()) && ($arLinkProperty["VALUE"] > 0))
                    {
                        $rsTaxProperty = CIBlockElement::GetProperty($this->arProperties[$this->PROPERTY_MAP["CML2_LINK"]]["LINK_IBLOCK_ID"], $arLinkProperty["VALUE"], array("sort" => "asc"), array("CODE" => "CML2_TAXES"));
                        while($arTaxProperty = $rsTaxProperty->Fetch())
                        {
                            if(
                                strlen($arTaxProperty["VALUE"]) > 0
                                && strlen($arTaxProperty["DESCRIPTION"]) > 0
                                && !array_key_exists($arTaxProperty["DESCRIPTION"], $arTaxMap)
                            )
                            {
                                $arTaxMap[$arTaxProperty["DESCRIPTION"]] = array(
                                    "RATE" => $this->ToFloat($arTaxProperty["VALUE"]),
                                    "ID" => $this->CheckTax($arTaxProperty["DESCRIPTION"], $this->ToFloat($arTaxProperty["VALUE"])),
                                );
                            }
                        }
                    }
                }

                //First find out if all the prices have TAX_IN_SUM true
                $TAX_IN_SUM = "Y";
                foreach($arElement["PRICES"] as $price)
                {
                    if($price["PRICE"]["TAX_IN_SUM"] !== "true")
                    {
                        $TAX_IN_SUM = "N";
                        break;
                    }
                }
                //If there was found not included tax we'll make sure
                //that all prices has the same flag
                if($TAX_IN_SUM === "N")
                {
                    foreach($arElement["PRICES"] as $price)
                    {
                        if($price["PRICE"]["TAX_IN_SUM"] !== "false")
                        {
                            $TAX_IN_SUM = "Y";
                            break;
                        }
                    }
                    //Check if there is a mix of tax in sum
                    //and correct it by recalculating all the prices
                    if($TAX_IN_SUM === "Y")
                    {
                        foreach($arElement["PRICES"] as $key=>$price)
                        {
                            if($price["PRICE"]["TAX_IN_SUM"] !== "true")
                            {
                                $TAX_NAME = $price["PRICE"]["TAX_NAME"];
                                if(array_key_exists($TAX_NAME, $arTaxMap))
                                {
                                    $PRICE_WO_TAX = $this->ToFloat($price[$this->mess["IBLOCK_XML2_PRICE_FOR_ONE"]]);
                                    $PRICE = $PRICE_WO_TAX + ($PRICE_WO_TAX / 100.0 * $arTaxMap[$TAX_NAME]["RATE"]);
                                    $arElement["PRICES"][$key][$this->mess["IBLOCK_XML2_PRICE_FOR_ONE"]] = $PRICE;
                                }
                            }
                        }
                    }
                }

                if ($TAX_IN_SUM == "Y" && $arTaxMap)
                {
                    $vat = current($arTaxMap);
                    $arProduct["VAT_ID"] = $vat["ID"];
                }
                else
                {
                    foreach($arElement["PRICES"] as $price)
                    {
                        $TAX_NAME = $price["PRICE"]["TAX_NAME"];
                        if(array_key_exists($TAX_NAME, $arTaxMap))
                        {
                            $arProduct["VAT_ID"] = $arTaxMap[$TAX_NAME]["ID"];
                            break;
                        }
                    }
                }

                $arProduct["VAT_INCLUDED"] = $TAX_IN_SUM;

                if (CCatalogProduct::Add($arProduct))
                {
                    //TODO: replace this code after upload measure ratio from 1C
                    $iterator = \Bitrix\Catalog\MeasureRatioTable::getList(array(
                        'select' => array('ID'),
                        'filter' => array('=PRODUCT_ID' => $arElement['ID'])
                    ));
                    $ratioRow = $iterator->fetch();
                    if (empty($ratioRow))
                    {
                        $ratioResult = \Bitrix\Catalog\MeasureRatioTable::add(array(
                            'PRODUCT_ID' => $arElement['ID'],
                            'RATIO' => 1,
                            'IS_DEFAULT' => 'Y'
                        ));
                        unset($ratioResult);
                    }
                    unset($ratioRow, $iterator);
                }
                $this->SetProductPrice($arElement["ID"], $arElement["PRICES"], $arElement["DISCOUNTS"]);
                \Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex($IBLOCK_ID, $arElement["ID"]);
            }
            elseif(
                $this->bCatalog
                && isset($arElement["STORE_AMOUNT"])
                && !empty($arElement["STORE_AMOUNT"])
                && ($arElementTmp = CCatalogProduct::GetByID($arElement["ID"]))
            )
            {
                //общее количество уже было пересчитано после вызова ImportStoresAmount
                //$this->UpdateProductQuantity($arElement["ID"], array_sum($arElement["STORE_AMOUNT"]) - $arElementTmp["QUANTITY_RESERVED"]);
//                CCatalogProduct::Update($arElement["ID"], array(
//                    "QUANTITY" => array_sum($arElement["STORE_AMOUNT"]) - $arElementTmp["QUANTITY_RESERVED"]
//                ));
            }
            elseif(
                $this->bCatalog
                && isset($arElement["QUANTITY"])
                && ($arElementTmp = CCatalogProduct::GetByID($arElement["ID"]))
            )
            {
                $this->UpdateProductQuantity($arElement["ID"], $arElement["QUANTITY"] - $arElementTmp["QUANTITY_RESERVED"]);
//                CCatalogProduct::Update($arElement["ID"], array(
//                    "QUANTITY" => $arElement["QUANTITY"] - $arElementTmp["QUANTITY_RESERVED"]
//                ));
            }
        }

        $counter["UPD"]++;
        return $arElement["ID"];
    }

    // переопределяем сохранение остатков - вместо записи пришедшего остатка будем прибавлять его к имеющемуся
    function ImportElement($arXMLElement, &$counter, $bWF, $arParent)
    {

        global $USER;
        $USER_ID = is_object($USER)? intval($USER->GetID()): 0;
        $arElement = array(
            "ACTIVE" => "Y",
            "PROPERTY_VALUES" => array(),
        );

        if(isset($arXMLElement[$this->mess["IBLOCK_XML2_VERSION"]]))
            $arElement["TMP_ID"] = $arXMLElement[$this->mess["IBLOCK_XML2_VERSION"]];
        else
            $arElement["TMP_ID"] = $this->GetElementCRC($arXMLElement);

        if(isset($arXMLElement[$this->mess["IBLOCK_XML2_ID_1C_SITE"]]))
            $arElement["XML_ID"] = $arXMLElement[$this->mess["IBLOCK_XML2_ID_1C_SITE"]];
        elseif(isset($arXMLElement[$this->mess["IBLOCK_XML2_ID"]]))
            $arElement["XML_ID"] = $arXMLElement[$this->mess["IBLOCK_XML2_ID"]];

        $obElement = new CIBlockElement;
        $obElement->CancelWFSetMove();
        $rsElement = $obElement->GetList(
            Array("ID"=>"asc"),
            Array("=XML_ID" => $arElement["XML_ID"], "IBLOCK_ID" => $this->next_step["IBLOCK_ID"]),
            false, false,
            Array("ID", "TMP_ID", "ACTIVE", "CODE", "PREVIEW_PICTURE", "DETAIL_PICTURE")
        );

        $bMatch = false;
        if($arDBElement = $rsElement->Fetch())
            $bMatch = ($arElement["TMP_ID"] == $arDBElement["TMP_ID"]);

        if($bMatch && $this->use_crc)
        {
            //Check Active flag in XML is not set to false
            if($this->CheckIfElementIsActive($arXMLElement))
            {
                //In case element is not active in database we have to activate it and its offers
                if($arDBElement["ACTIVE"] != "Y")
                {
                    $obElement->Update($arDBElement["ID"], array("ACTIVE"=>"Y"), $bWF);
                    $this->ChangeOffersStatus($arDBElement["ID"], "Y", $bWF);
                    $counter["UPD"]++;
                }
            }
            $arElement["ID"] = $arDBElement["ID"];
        }
        elseif(isset($arXMLElement[$this->mess["IBLOCK_XML2_NAME"]]))
        {
            if($arDBElement)
            {
                if ($arDBElement["PREVIEW_PICTURE"] > 0)
                    $this->arElementFilesId["PREVIEW_PICTURE"] = array($arDBElement["PREVIEW_PICTURE"]);
                if ($arDBElement["DETAIL_PICTURE"] > 0)
                    $this->arElementFilesId["DETAIL_PICTURE"] = array($arDBElement["DETAIL_PICTURE"]);

                $rsProperties = $obElement->GetProperty($this->next_step["IBLOCK_ID"], $arDBElement["ID"], "sort", "asc");
                while($arProperty = $rsProperties->Fetch())
                {
                    if(!array_key_exists($arProperty["ID"], $arElement["PROPERTY_VALUES"]))
                        $arElement["PROPERTY_VALUES"][$arProperty["ID"]] = array(
                            "bOld" => true,
                        );

                    $arElement["PROPERTY_VALUES"][$arProperty["ID"]][$arProperty['PROPERTY_VALUE_ID']] = array(
                        "VALUE"=>$arProperty['VALUE'],
                        "DESCRIPTION"=>$arProperty["DESCRIPTION"]
                    );

                    if($arProperty["PROPERTY_TYPE"] == "F" && $arProperty["VALUE"] > 0)
                        $this->arElementFilesId[$arProperty["ID"]][] = $arProperty["VALUE"];
                }
            }

            if($this->bCatalog && $this->next_step["bOffer"])
            {
                $p = strpos($arXMLElement[$this->mess["IBLOCK_XML2_ID"]], "#");
                if($p !== false)
                    $link_xml_id = substr($arXMLElement[$this->mess["IBLOCK_XML2_ID"]], 0, $p);
                else
                    $link_xml_id = $arXMLElement[$this->mess["IBLOCK_XML2_ID"]];
                $arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_LINK"]] = array(
                    "n0" => array(
                        "VALUE" => $this->GetElementByXML_ID($this->arProperties[$this->PROPERTY_MAP["CML2_LINK"]]["LINK_IBLOCK_ID"], $link_xml_id),
                        "DESCRIPTION" => false,
                    ),
                );
            }

            if(isset($arXMLElement[$this->mess["IBLOCK_XML2_NAME"]]))
                $arElement["NAME"] = $arXMLElement[$this->mess["IBLOCK_XML2_NAME"]];

            if(isset($arXMLElement[$this->mess["IBLOCK_XML2_DELETE_MARK"]]))
            {
                $value = $arXMLElement[$this->mess["IBLOCK_XML2_DELETE_MARK"]];
                $arElement["ACTIVE"] = ($value=="true") || intval($value)? "N": "Y";
            }

            if(array_key_exists($this->mess["IBLOCK_XML2_BX_TAGS"], $arXMLElement))
                $arElement["TAGS"] = $arXMLElement[$this->mess["IBLOCK_XML2_BX_TAGS"]];

            if(array_key_exists($this->mess["IBLOCK_XML2_DESCRIPTION"], $arXMLElement))
            {
                if(strlen($arXMLElement[$this->mess["IBLOCK_XML2_DESCRIPTION"]]) > 0)
                    $arElement["DETAIL_TEXT"] = $arXMLElement[$this->mess["IBLOCK_XML2_DESCRIPTION"]];
                else
                    $arElement["DETAIL_TEXT"] = "";

                if(preg_match('/<[a-zA-Z0-9]+.*?>/', $arElement["DETAIL_TEXT"]))
                    $arElement["DETAIL_TEXT_TYPE"] = "html";
                else
                    $arElement["DETAIL_TEXT_TYPE"] = "text";
            }

            if(array_key_exists($this->mess["IBLOCK_XML2_FULL_TITLE"], $arXMLElement))
            {
                if(strlen($arXMLElement[$this->mess["IBLOCK_XML2_FULL_TITLE"]]) > 0)
                    $arElement["PREVIEW_TEXT"] = $arXMLElement[$this->mess["IBLOCK_XML2_FULL_TITLE"]];
                else
                    $arElement["PREVIEW_TEXT"] = "";

                if(preg_match('/<[a-zA-Z0-9]+.*?>/', $arElement["PREVIEW_TEXT"]))
                    $arElement["PREVIEW_TEXT_TYPE"] = "html";
                else
                    $arElement["PREVIEW_TEXT_TYPE"] = "text";
            }

            if(array_key_exists($this->mess["IBLOCK_XML2_INHERITED_TEMPLATES"], $arXMLElement))
            {
                $arElement["IPROPERTY_TEMPLATES"] = array();
                foreach($arXMLElement[$this->mess["IBLOCK_XML2_INHERITED_TEMPLATES"]] as $TEMPLATE)
                {
                    $id = $TEMPLATE[$this->mess["IBLOCK_XML2_ID"]];
                    $template = $TEMPLATE[$this->mess["IBLOCK_XML2_VALUE"]];
                    if(strlen($id) > 0 && strlen($template) > 0)
                        $arElement["IPROPERTY_TEMPLATES"][$id] = $template;
                }
            }
            if(array_key_exists($this->mess["IBLOCK_XML2_BAR_CODE2"], $arXMLElement))
            {
                $arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_BAR_CODE"]] = array(
                    "n0" => array(
                        "VALUE" => $arXMLElement[$this->mess["IBLOCK_XML2_BAR_CODE2"]],
                        "DESCRIPTION" => false,
                    ),
                );
            }
            elseif(array_key_exists($this->mess["IBLOCK_XML2_BAR_CODE"], $arXMLElement))
            {
                $arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_BAR_CODE"]] = array(
                    "n0" => array(
                        "VALUE" => $arXMLElement[$this->mess["IBLOCK_XML2_BAR_CODE"]],
                        "DESCRIPTION" => false,
                    ),
                );
            }

            if(array_key_exists($this->mess["IBLOCK_XML2_ARTICLE"], $arXMLElement))
            {
                $arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_ARTICLE"]] = array(
                    "n0" => array(
                        "VALUE" => $arXMLElement[$this->mess["IBLOCK_XML2_ARTICLE"]],
                        "DESCRIPTION" => false,
                    ),
                );
            }

            if(
                array_key_exists($this->mess["IBLOCK_XML2_MANUFACTURER"], $arXMLElement)
                && $this->PROPERTY_MAP["CML2_MANUFACTURER"] > 0
            )
            {
                $arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_MANUFACTURER"]] = array(
                    "n0" => array(
                        "VALUE" => $this->CheckManufacturer($arXMLElement[$this->mess["IBLOCK_XML2_MANUFACTURER"]]),
                        "DESCRIPTION" => false,
                    ),
                );
            }

            if(array_key_exists($this->mess["IBLOCK_XML2_PICTURE"], $arXMLElement))
            {
                $rsFiles = $this->_xml_file->GetList(
                    array("ID" => "asc"),
                    array("PARENT_ID" => $arParent["ID"], "NAME" => $this->mess["IBLOCK_XML2_PICTURE"])
                );
                $arFile = $rsFiles->Fetch();
                if($arFile)
                {
                    $description = "";
                    if(strlen($arFile["ATTRIBUTES"]))
                    {
                        $arAttributes = unserialize($arFile["ATTRIBUTES"]);
                        if(is_array($arAttributes) && array_key_exists($this->mess["IBLOCK_XML2_DESCRIPTION"], $arAttributes))
                            $description = $arAttributes[$this->mess["IBLOCK_XML2_DESCRIPTION"]];
                    }

                    if(strlen($arFile["VALUE"]) > 0)
                    {
                        $arElement["DETAIL_PICTURE"] = $this->ResizePicture($arFile["VALUE"], $this->detail, "DETAIL_PICTURE", $this->PROPERTY_MAP["CML2_PICTURES"]);

                        if(is_array($arElement["DETAIL_PICTURE"]))
                        {
                            $arElement["DETAIL_PICTURE"]["description"] = $description;
                            $this->arFileDescriptionsMap[$arFile["VALUE"]][] = &$arElement["DETAIL_PICTURE"]["description"];
                        }

                        if(is_array($this->preview))
                        {
                            $arElement["PREVIEW_PICTURE"] = $this->ResizePicture($arFile["VALUE"], $this->preview, "PREVIEW_PICTURE");
                            if(is_array($arElement["PREVIEW_PICTURE"]))
                            {
                                $arElement["PREVIEW_PICTURE"]["description"] = $description;
                                $this->arFileDescriptionsMap[$arFile["VALUE"]][] = &$arElement["PREVIEW_PICTURE"]["description"];
                            }
                        }
                    }
                    else
                    {
                        $arElement["DETAIL_PICTURE"] = $this->MakeFileArray($this->_xml_file->GetAllChildrenArray($arFile["ID"]));

                        if(is_array($arElement["DETAIL_PICTURE"]))
                        {
                            $arElement["DETAIL_PICTURE"]["description"] = $description;
                        }
                    }

                    $prop_id = $this->PROPERTY_MAP["CML2_PICTURES"];
                    if($prop_id > 0)
                    {
                        $i = 1;
                        while($arFile = $rsFiles->Fetch())
                        {
                            $description = "";
                            if(strlen($arFile["ATTRIBUTES"]))
                            {
                                $arAttributes = unserialize($arFile["ATTRIBUTES"]);
                                if(is_array($arAttributes) && array_key_exists($this->mess["IBLOCK_XML2_DESCRIPTION"], $arAttributes))
                                    $description = $arAttributes[$this->mess["IBLOCK_XML2_DESCRIPTION"]];
                            }

                            if(strlen($arFile["VALUE"]) > 0)
                                $arPropFile = $this->ResizePicture($arFile["VALUE"], $this->detail, $this->PROPERTY_MAP["CML2_PICTURES"], "DETAIL_PICTURE");
                            else
                                $arPropFile = $this->MakeFileArray($this->_xml_file->GetAllChildrenArray($arFile["ID"]));

                            if(is_array($arPropFile))
                            {
                                $arPropFile = array(
                                    "VALUE" => $arPropFile,
                                    "DESCRIPTION" => $description,
                                );
                            }
                            $arElement["PROPERTY_VALUES"][$prop_id]["n".$i] = $arPropFile;
                            if (strlen($arFile["VALUE"]) > 0)
                                $this->arFileDescriptionsMap[$arFile["VALUE"]][] = &$arElement["PROPERTY_VALUES"][$prop_id]["n".$i]["DESCRIPTION"];
                            $i++;
                        }

                        if(is_array($arElement["PROPERTY_VALUES"][$prop_id]))
                        {
                            foreach($arElement["PROPERTY_VALUES"][$prop_id] as $PROPERTY_VALUE_ID => $PROPERTY_VALUE)
                            {
                                if(!$PROPERTY_VALUE_ID)
                                    unset($arElement["PROPERTY_VALUES"][$prop_id][$PROPERTY_VALUE_ID]);
                                elseif(substr($PROPERTY_VALUE_ID, 0, 1)!=="n")
                                    $arElement["PROPERTY_VALUES"][$prop_id][$PROPERTY_VALUE_ID] = array(
                                        "tmp_name" => "",
                                        "del" => "Y",
                                    );
                            }
                            unset($arElement["PROPERTY_VALUES"][$prop_id]["bOld"]);
                        }
                    }
                }
            }

            $cleanCml2FilesProperty = false;
            if(
                array_key_exists($this->mess["IBLOCK_XML2_FILE"], $arXMLElement)
                && strlen($this->PROPERTY_MAP["CML2_FILES"]) > 0
            )
            {
                $prop_id = $this->PROPERTY_MAP["CML2_FILES"];
                $rsFiles = $this->_xml_file->GetList(
                    array("ID" => "asc"),
                    array("PARENT_ID" => $arParent["ID"], "NAME" => $this->mess["IBLOCK_XML2_FILE"])
                );
                $i = 1;
                while($arFile = $rsFiles->Fetch())
                {

                    if(strlen($arFile["VALUE"]) > 0)
                        $file = $this->MakeFileArray($arFile["VALUE"], array($prop_id));
                    else
                        $file = $this->MakeFileArray($this->_xml_file->GetAllChildrenArray($arFile["ID"]));

                    $arElement["PROPERTY_VALUES"][$prop_id]["n".$i] = array(
                        "VALUE" => $file,
                        "DESCRIPTION" => $file["description"],
                    );
                    if(strlen($arFile["ATTRIBUTES"]))
                    {
                        $desc = unserialize($arFile["ATTRIBUTES"]);
                        if(is_array($desc) && array_key_exists($this->mess["IBLOCK_XML2_DESCRIPTION"], $desc))
                            $arElement["PROPERTY_VALUES"][$prop_id]["n".$i]["DESCRIPTION"] = $desc[$this->mess["IBLOCK_XML2_DESCRIPTION"]];
                    }
                    $i++;
                }
                $cleanCml2FilesProperty = true;
            }

            if(isset($arXMLElement[$this->mess["IBLOCK_XML2_GROUPS"]]))
            {
                $arElement["IBLOCK_SECTION"] = array();
                foreach($arXMLElement[$this->mess["IBLOCK_XML2_GROUPS"]] as $value)
                {
                    if(array_key_exists($value, $this->SECTION_MAP))
                        $arElement["IBLOCK_SECTION"][] = $this->SECTION_MAP[$value];
                }
                if($arElement["IBLOCK_SECTION"])
                    $arElement["IBLOCK_SECTION_ID"] = $arElement["IBLOCK_SECTION"][0];
            }

            if(array_key_exists($this->mess["IBLOCK_XML2_PRICES"], $arXMLElement))
            {//Collect price information for future use
                $arElement["PRICES"] = array();
                if (is_array($arXMLElement[$this->mess["IBLOCK_XML2_PRICES"]]))
                {
                    foreach($arXMLElement[$this->mess["IBLOCK_XML2_PRICES"]] as $price)
                    {
                        if(isset($price[$this->mess["IBLOCK_XML2_PRICE_TYPE_ID"]]) && array_key_exists($price[$this->mess["IBLOCK_XML2_PRICE_TYPE_ID"]], $this->PRICES_MAP))
                        {
                            $price["PRICE"] = $this->PRICES_MAP[$price[$this->mess["IBLOCK_XML2_PRICE_TYPE_ID"]]];
                            $arElement["PRICES"][] = $price;
                        }
                    }
                }

                $arElement["DISCOUNTS"] = array();
                if(isset($arXMLElement[$this->mess["IBLOCK_XML2_DISCOUNTS"]]))
                {
                    foreach($arXMLElement[$this->mess["IBLOCK_XML2_DISCOUNTS"]] as $discount)
                    {
                        if(
                            isset($discount[$this->mess["IBLOCK_XML2_DISCOUNT_CONDITION"]])
                            && $discount[$this->mess["IBLOCK_XML2_DISCOUNT_CONDITION"]]===$this->mess["IBLOCK_XML2_DISCOUNT_COND_VOLUME"]
                        )
                        {
                            $discount_value = $this->ToInt($discount[$this->mess["IBLOCK_XML2_DISCOUNT_COND_VALUE"]]);
                            $discount_percent = $this->ToFloat($discount[$this->mess["IBLOCK_XML2_DISCOUNT_COND_PERCENT"]]);
                            if($discount_value > 0 && $discount_percent > 0)
                                $arElement["DISCOUNTS"][$discount_value] = $discount_percent;
                        }
                    }
                }
            }

            if($this->bCatalog && array_key_exists($this->mess["IBLOCK_XML2_AMOUNT"], $arXMLElement))
            {
                $arElementTmp = array();
                $arElement["QUANTITY_RESERVED"] = 0;
                if($arDBElement["ID"])
                    $arElementTmp = CCatalogProduct::GetByID($arDBElement["ID"]);
                if(is_array($arElementTmp) && !empty($arElementTmp) && isset($arElementTmp["QUANTITY_RESERVED"]))
                    $arElement["QUANTITY_RESERVED"] = $arElementTmp["QUANTITY_RESERVED"];
                if($this->forcedWarehouse) {
                    // если передан ID склада для принудительной выгрузки в него, выгружаем остатки в него
                    $arElement["STORE_AMOUNT"][$this->forcedWarehouse] = $this->ToFloat($arXMLElement[$this->mess["IBLOCK_XML2_AMOUNT"]]) - doubleval($arElement["QUANTITY_RESERVED"]);
                }
                else {
                    $arElement["QUANTITY"] = $this->ToFloat($arXMLElement[$this->mess["IBLOCK_XML2_AMOUNT"]]) - doubleval($arElement["QUANTITY_RESERVED"]);
                }
            }

            if(isset($arXMLElement[$this->mess["IBLOCK_XML2_ITEM_ATTRIBUTES"]]))
            {
                $arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_ATTRIBUTES"]] = array();
                $i = 0;
                foreach($arXMLElement[$this->mess["IBLOCK_XML2_ITEM_ATTRIBUTES"]] as $value)
                {
                    $arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_ATTRIBUTES"]]["n".$i] = array(
                        "VALUE" => $value[$this->mess["IBLOCK_XML2_VALUE"]],
                        "DESCRIPTION" => $value[$this->mess["IBLOCK_XML2_NAME"]],
                    );
                    $i++;
                }
            }

            $i = 0;
            $weightKey = false;
            if(isset($arXMLElement[$this->mess["IBLOCK_XML2_TRAITS_VALUES"]]))
            {
                $arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_TRAITS"]] = array();
                foreach($arXMLElement[$this->mess["IBLOCK_XML2_TRAITS_VALUES"]] as $value)
                {
                    if(
                        !array_key_exists("PREVIEW_TEXT", $arElement)
                        && $value[$this->mess["IBLOCK_XML2_NAME"]] == $this->mess["IBLOCK_XML2_FULL_TITLE2"]
                    )
                    {
                        $arElement["PREVIEW_TEXT"] = $value[$this->mess["IBLOCK_XML2_VALUE"]];
                        if(strpos($arElement["PREVIEW_TEXT"], "<")!==false)
                            $arElement["PREVIEW_TEXT_TYPE"] = "html";
                        else
                            $arElement["PREVIEW_TEXT_TYPE"] = "text";
                    }
                    elseif(
                        $value[$this->mess["IBLOCK_XML2_NAME"]] == $this->mess["IBLOCK_XML2_HTML_DESCRIPTION"]
                    )
                    {
                        if(strlen($value[$this->mess["IBLOCK_XML2_VALUE"]]) > 0)
                        {
                            $arElement["DETAIL_TEXT"] = $value[$this->mess["IBLOCK_XML2_VALUE"]];
                            $arElement["DETAIL_TEXT_TYPE"] = "html";
                        }
                    }
                    elseif(
                        $value[$this->mess["IBLOCK_XML2_NAME"]] == $this->mess["IBLOCK_XML2_FILE"]
                    )
                    {
                        if(strlen($value[$this->mess["IBLOCK_XML2_VALUE"]]) > 0)
                        {
                            $prop_id = $this->PROPERTY_MAP["CML2_FILES"];

                            $j = 1;
                            while (isset($arElement["PROPERTY_VALUES"][$prop_id]["n".$j]))
                                $j++;

                            $file = $this->MakeFileArray($value[$this->mess["IBLOCK_XML2_VALUE"]], array($prop_id));
                            if (is_array($file))
                            {
                                $arElement["PROPERTY_VALUES"][$prop_id]["n".$j] = array(
                                    "VALUE" => $file,
                                    "DESCRIPTION" => "",
                                );
                                unset($arElement["PROPERTY_VALUES"][$prop_id]["bOld"]);
                                $this->arFileDescriptionsMap[$value[$this->mess["IBLOCK_XML2_VALUE"]]][] = &$arElement["PROPERTY_VALUES"][$prop_id]["n".$j]["DESCRIPTION"];
                                $cleanCml2FilesProperty = true;
                            }
                        }
                    }
                    elseif(
                        $value[$this->mess["IBLOCK_XML2_NAME"]] == $this->mess["IBLOCK_XML2_FILE_DESCRIPTION"]
                    )
                    {
                        if(strlen($value[$this->mess["IBLOCK_XML2_VALUE"]]) > 0)
                        {
                            list($fileName, $description) = explode("#", $value[$this->mess["IBLOCK_XML2_VALUE"]]);
                            if (isset($this->arFileDescriptionsMap[$fileName]))
                            {
                                foreach($this->arFileDescriptionsMap[$fileName] as $k => $tmp)
                                    $this->arFileDescriptionsMap[$fileName][$k] = $description;
                            }
                        }
                    }
                    else
                    {
                        if($value[$this->mess["IBLOCK_XML2_NAME"]] == $this->mess["IBLOCK_XML2_WEIGHT"])
                        {
                            $arElement["BASE_WEIGHT"] = $this->ToFloat($value[$this->mess["IBLOCK_XML2_VALUE"]])*1000;
                            $weightKey = "n".$i;
                        }

                        $arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_TRAITS"]]["n".$i] = array(
                            "VALUE" => $value[$this->mess["IBLOCK_XML2_VALUE"]],
                            "DESCRIPTION" => $value[$this->mess["IBLOCK_XML2_NAME"]],
                        );
                        $i++;
                    }
                }
            }

            if(isset($arXMLElement[$this->mess["IBLOCK_XML2_WEIGHT"]]))
            {
                if ($weightKey !== false)
                {
                }
                elseif (!isset($arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_TRAITS"]]))
                {
                    $arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_TRAITS"]] = array();
                    $weightKey = "n0";
                }
                else // $weightKey === false && isset($arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_TRAITS"]])
                {
                    $weightKey = "n".$i;
                }
                $arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_TRAITS"]][$weightKey] = array(
                    "VALUE" => $arXMLElement[$this->mess["IBLOCK_XML2_WEIGHT"]],
                    "DESCRIPTION" => $this->mess["IBLOCK_XML2_WEIGHT"],
                );
                $arElement["BASE_WEIGHT"] = $this->ToFloat($arXMLElement[$this->mess["IBLOCK_XML2_WEIGHT"]])*1000;
            }

            if ($cleanCml2FilesProperty)
            {
                $prop_id = $this->PROPERTY_MAP["CML2_FILES"];
                if(is_array($arElement["PROPERTY_VALUES"][$prop_id]))
                {
                    foreach($arElement["PROPERTY_VALUES"][$prop_id] as $PROPERTY_VALUE_ID => $PROPERTY_VALUE)
                    {
                        if(!$PROPERTY_VALUE_ID)
                            unset($arElement["PROPERTY_VALUES"][$prop_id][$PROPERTY_VALUE_ID]);
                        elseif(substr($PROPERTY_VALUE_ID, 0, 1)!=="n")
                            $arElement["PROPERTY_VALUES"][$prop_id][$PROPERTY_VALUE_ID] = array(
                                "tmp_name" => "",
                                "del" => "Y",
                            );
                    }
                    unset($arElement["PROPERTY_VALUES"][$prop_id]["bOld"]);
                }
            }

            if(isset($arXMLElement[$this->mess["IBLOCK_XML2_TAXES_VALUES"]]))
            {
                $arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_TAXES"]] = array();
                $i = 0;
                foreach($arXMLElement[$this->mess["IBLOCK_XML2_TAXES_VALUES"]] as $value)
                {
                    $arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_TAXES"]]["n".$i] = array(
                        "VALUE" => $value[$this->mess["IBLOCK_XML2_TAX_VALUE"]],
                        "DESCRIPTION" => $value[$this->mess["IBLOCK_XML2_NAME"]],
                    );
                    $i++;
                }
            }

            $rsBaseUnit = $this->_xml_file->GetList(
                array("ID" => "asc"),
                array(
                    "><LEFT_MARGIN" => array($arParent["LEFT_MARGIN"], $arParent["RIGHT_MARGIN"]),
                    "NAME" => $this->mess["IBLOCK_XML2_BASE_UNIT"],
                ),
                array("ID", "ATTRIBUTES")
            );
            while ($arBaseUnit = $rsBaseUnit->Fetch())
            {
                if(strlen($arBaseUnit["ATTRIBUTES"]) > 0)
                {
                    $info = unserialize($arBaseUnit["ATTRIBUTES"]);
                    if(
                        is_array($info)
                        && array_key_exists($this->mess["IBLOCK_XML2_CODE"], $info)
                    )
                    {
                        $arXMLElement[$this->mess["IBLOCK_XML2_BASE_UNIT"]] = $info[$this->mess["IBLOCK_XML2_CODE"]];
                    }
                }
            }

            if(isset($arXMLElement[$this->mess["IBLOCK_XML2_BASE_UNIT"]]))
            {
                $arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_BASE_UNIT"]] = array(
                    "n0" => $this->convertBaseUnitFromXmlToPropertyValue($arXMLElement[$this->mess["IBLOCK_XML2_BASE_UNIT"]]),
                );
            }

            if(isset($arXMLElement[$this->mess["IBLOCK_XML2_PROPERTIES_VALUES"]]))
            {
                foreach($arXMLElement[$this->mess["IBLOCK_XML2_PROPERTIES_VALUES"]] as $value)
                {
                    if(!array_key_exists($this->mess["IBLOCK_XML2_ID"], $value))
                        continue;

                    $prop_id = $value[$this->mess["IBLOCK_XML2_ID"]];
                    unset($value[$this->mess["IBLOCK_XML2_ID"]]);

                    //Handle properties which is actually element fields
                    if(!array_key_exists($prop_id, $this->PROPERTY_MAP))
                    {
                        if($prop_id == "CML2_CODE")
                            $arElement["CODE"] = isset($value[$this->mess["IBLOCK_XML2_VALUE"]])? $value[$this->mess["IBLOCK_XML2_VALUE"]]: "";
                        elseif($prop_id == "CML2_ACTIVE")
                        {
                            $value = array_pop($value);
                            $arElement["ACTIVE"] = ($value=="true") || intval($value)? "Y": "N";
                        }
                        elseif($prop_id == "CML2_SORT")
                            $arElement["SORT"] = array_pop($value);
                        elseif($prop_id == "CML2_ACTIVE_FROM")
                            $arElement["ACTIVE_FROM"] = CDatabase::FormatDate(array_pop($value), "YYYY-MM-DD HH:MI:SS", CLang::GetDateFormat("FULL"));
                        elseif($prop_id == "CML2_ACTIVE_TO")
                            $arElement["ACTIVE_TO"] = CDatabase::FormatDate(array_pop($value), "YYYY-MM-DD HH:MI:SS", CLang::GetDateFormat("FULL"));
                        elseif($prop_id == "CML2_PREVIEW_TEXT")
                        {
                            if(array_key_exists($this->mess["IBLOCK_XML2_VALUE"], $value))
                            {
                                if(isset($value[$this->mess["IBLOCK_XML2_VALUE"]]))
                                    $arElement["PREVIEW_TEXT"] = $value[$this->mess["IBLOCK_XML2_VALUE"]];
                                else
                                    $arElement["PREVIEW_TEXT"] = "";

                                if(isset($value[$this->mess["IBLOCK_XML2_TYPE"]]))
                                    $arElement["PREVIEW_TEXT_TYPE"] = $value[$this->mess["IBLOCK_XML2_TYPE"]];
                                else
                                    $arElement["PREVIEW_TEXT_TYPE"] = "html";
                            }
                        }
                        elseif($prop_id == "CML2_DETAIL_TEXT")
                        {
                            if(array_key_exists($this->mess["IBLOCK_XML2_VALUE"], $value))
                            {
                                if(isset($value[$this->mess["IBLOCK_XML2_VALUE"]]))
                                    $arElement["DETAIL_TEXT"] = $value[$this->mess["IBLOCK_XML2_VALUE"]];
                                else
                                    $arElement["DETAIL_TEXT"] = "";

                                if(isset($value[$this->mess["IBLOCK_XML2_TYPE"]]))
                                    $arElement["DETAIL_TEXT_TYPE"] = $value[$this->mess["IBLOCK_XML2_TYPE"]];
                                else
                                    $arElement["DETAIL_TEXT_TYPE"] = "html";
                            }
                        }
                        elseif($prop_id == "CML2_PREVIEW_PICTURE")
                        {
                            if(!is_array($this->preview) || !$arElement["PREVIEW_PICTURE"])
                            {
                                $arElement["PREVIEW_PICTURE"] = $this->MakeFileArray($value[$this->mess["IBLOCK_XML2_VALUE"]], array("PREVIEW_PICTURE"));
                                $arElement["PREVIEW_PICTURE"]["COPY_FILE"] = "Y";
                            }
                        }

                        continue;
                    }

                    $prop_id = $this->PROPERTY_MAP[$prop_id];
                    $prop_type = $this->arProperties[$prop_id]["PROPERTY_TYPE"];

                    if(!array_key_exists($prop_id, $arElement["PROPERTY_VALUES"]))
                        $arElement["PROPERTY_VALUES"][$prop_id] = array();

                    //check for bitrix extended format
                    if(array_key_exists($this->mess["IBLOCK_XML2_PROPERTY_VALUE"], $value))
                    {
                        $i = 1;
                        $strPV = $this->mess["IBLOCK_XML2_PROPERTY_VALUE"];
                        $lPV = strlen($strPV);
                        foreach($value as $k=>$prop_value)
                        {
                            if(substr($k, 0, $lPV) === $strPV)
                            {
                                if(array_key_exists($this->mess["IBLOCK_XML2_SERIALIZED"], $prop_value))
                                    $prop_value[$this->mess["IBLOCK_XML2_VALUE"]] = $this->Unserialize($prop_value[$this->mess["IBLOCK_XML2_VALUE"]]);
                                if($prop_type=="F")
                                {
                                    $prop_value[$this->mess["IBLOCK_XML2_VALUE"]] = $this->MakeFileArray($prop_value[$this->mess["IBLOCK_XML2_VALUE"]], array($prop_id));
                                }
                                elseif($prop_type=="G")
                                    $prop_value[$this->mess["IBLOCK_XML2_VALUE"]] = $this->GetSectionByXML_ID($this->arProperties[$prop_id]["LINK_IBLOCK_ID"], $prop_value[$this->mess["IBLOCK_XML2_VALUE"]]);
                                elseif($prop_type=="E")
                                    $prop_value[$this->mess["IBLOCK_XML2_VALUE"]] = $this->GetElementByXML_ID($this->arProperties[$prop_id]["LINK_IBLOCK_ID"], $prop_value[$this->mess["IBLOCK_XML2_VALUE"]]);
                                elseif($prop_type=="L")
                                    $prop_value[$this->mess["IBLOCK_XML2_VALUE"]] = $this->GetEnumByXML_ID($this->arProperties[$prop_id]["ID"], $prop_value[$this->mess["IBLOCK_XML2_VALUE"]]);

                                if(array_key_exists("bOld", $arElement["PROPERTY_VALUES"][$prop_id]))
                                {
                                    if($prop_type=="F")
                                    {
                                        foreach($arElement["PROPERTY_VALUES"][$prop_id] as $PROPERTY_VALUE_ID => $PROPERTY_VALUE)
                                            $arElement["PROPERTY_VALUES"][$prop_id][$PROPERTY_VALUE_ID] = array(
                                                "tmp_name" => "",
                                                "del" => "Y",
                                            );
                                        unset($arElement["PROPERTY_VALUES"][$prop_id]["bOld"]);
                                    }
                                    else
                                        $arElement["PROPERTY_VALUES"][$prop_id] = array();
                                }

                                $arElement["PROPERTY_VALUES"][$prop_id]["n".$i] = array(
                                    "VALUE" => $prop_value[$this->mess["IBLOCK_XML2_VALUE"]],
                                    "DESCRIPTION" => $prop_value[$this->mess["IBLOCK_XML2_DESCRIPTION"]],
                                );
                            }
                            $i++;
                        }
                    }
                    else
                    {
                        if($prop_type == "L" && !array_key_exists($this->mess["IBLOCK_XML2_VALUE_ID"], $value))
                            $l_key = $this->mess["IBLOCK_XML2_VALUE"];
                        else
                            $l_key = $this->mess["IBLOCK_XML2_VALUE_ID"];

                        $i = 0;
                        foreach($value as $k=>$prop_value)
                        {
                            if(array_key_exists("bOld", $arElement["PROPERTY_VALUES"][$prop_id]))
                            {
                                if($prop_type=="F")
                                {
                                    foreach($arElement["PROPERTY_VALUES"][$prop_id] as $PROPERTY_VALUE_ID => $PROPERTY_VALUE)
                                        $arElement["PROPERTY_VALUES"][$prop_id][$PROPERTY_VALUE_ID] = array(
                                            "tmp_name" => "",
                                            "del" => "Y",
                                        );
                                    unset($arElement["PROPERTY_VALUES"][$prop_id]["bOld"]);
                                }
                                else
                                {
                                    $arElement["PROPERTY_VALUES"][$prop_id] = array();
                                }
                            }

                            if($prop_type == "L" && $k == $l_key)
                            {
                                $prop_value = $this->GetEnumByXML_ID($this->arProperties[$prop_id]["ID"], $prop_value);
                            }
                            elseif($prop_type == "N" && isset($this->next_step["sdp"]))
                            {
                                if (strlen($prop_value) > 0)
                                    $prop_value = $this->ToFloat($prop_value);
                            }

                            $arElement["PROPERTY_VALUES"][$prop_id]["n".$i] = array(
                                "VALUE" => $prop_value,
                                "DESCRIPTION" => false,
                            );
                            $i++;
                        }
                    }
                }
            }

            //If there is no BaseUnit specified check prices for it
            if(
                (
                    !array_key_exists($this->PROPERTY_MAP["CML2_BASE_UNIT"], $arElement["PROPERTY_VALUES"])
                    || (
                        is_array($arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_BASE_UNIT"]])
                        && array_key_exists("bOld", $arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_BASE_UNIT"]])
                    )
                )
                && isset($arXMLElement[$this->mess["IBLOCK_XML2_PRICES"]])
            )
            {
                foreach($arXMLElement[$this->mess["IBLOCK_XML2_PRICES"]] as $price)
                {
                    if(
                        isset($price[$this->mess["IBLOCK_XML2_PRICE_TYPE_ID"]])
                        && array_key_exists($price[$this->mess["IBLOCK_XML2_PRICE_TYPE_ID"]], $this->PRICES_MAP)
                        && array_key_exists($this->mess["IBLOCK_XML2_MEASURE"], $price)
                    )
                    {
                        $arElement["PROPERTY_VALUES"][$this->PROPERTY_MAP["CML2_BASE_UNIT"]] = array(
                            "n0" => $this->convertBaseUnitFromXmlToPropertyValue($price[$this->mess["IBLOCK_XML2_MEASURE"]]),
                        );
                        break;
                    }
                }
            }

            if($arDBElement)
            {
                foreach($arElement["PROPERTY_VALUES"] as $prop_id=>$prop)
                {
                    if(is_array($arElement["PROPERTY_VALUES"][$prop_id]) && array_key_exists("bOld", $arElement["PROPERTY_VALUES"][$prop_id]))
                    {
                        if($this->arProperties[$prop_id]["PROPERTY_TYPE"]=="F")
                            unset($arElement["PROPERTY_VALUES"][$prop_id]);
                        else
                            unset($arElement["PROPERTY_VALUES"][$prop_id]["bOld"]);
                    }
                }

                if(intval($arElement["MODIFIED_BY"]) <= 0 && $USER_ID > 0)
                    $arElement["MODIFIED_BY"] = $USER_ID;

                if(!array_key_exists("CODE", $arElement) && is_array($this->translit_on_update))
                {
                    $arElement["CODE"] = CUtil::translit($arElement["NAME"], LANGUAGE_ID, $this->translit_on_update);
                    //Check if name was not changed in a way to update CODE
                    if(substr($arDBElement["CODE"], 0, strlen($arElement["CODE"])) === $arElement["CODE"])
                        unset($arElement["CODE"]);
                    else
                        $arElement["CODE"] = $this->CheckElementCode($this->next_step["IBLOCK_ID"], $arElement["CODE"]);
                }

                //Check if detail picture hasn't been changed
                if (
                    isset($arElement["DETAIL_PICTURE"])
                    && !isset($arElement["PREVIEW_PICTURE"])
                    && is_array($arElement["DETAIL_PICTURE"])
                    && isset($arElement["DETAIL_PICTURE"]["external_id"])
                    && $this->arElementFilesId
                    && $this->arElementFilesId["DETAIL_PICTURE"]
                    && isset($this->arElementFiles[$this->arElementFilesId["DETAIL_PICTURE"][0]])
                    && $this->arElementFiles[$this->arElementFilesId["DETAIL_PICTURE"][0]]["EXTERNAL_ID"] === $arElement["DETAIL_PICTURE"]["external_id"]
                    && $this->arElementFiles[$this->arElementFilesId["DETAIL_PICTURE"][0]]["DESCRIPTION"] === $arElement["DETAIL_PICTURE"]["description"]
                )
                {
                    unset($arElement["DETAIL_PICTURE"]);
                }

                $updateResult = $obElement->Update($arDBElement["ID"], $arElement, $bWF, true, $this->iblock_resize);
                //In case element was not active in database we have to activate its offers
                if($arDBElement["ACTIVE"] != "Y")
                {
                    $this->ChangeOffersStatus($arDBElement["ID"], "Y", $bWF);
                }
                $arElement["ID"] = $arDBElement["ID"];
                if($updateResult)
                {
                    $counter["UPD"]++;
                }
                else
                {
                    $this->LAST_ERROR = $obElement->LAST_ERROR;
                    $counter["ERR"]++;
                }
            }
            else
            {
                if(!array_key_exists("CODE", $arElement) && is_array($this->translit_on_add))
                    $arElement["CODE"] = $this->CheckElementCode($this->next_step["IBLOCK_ID"], CUtil::translit($arElement["NAME"], LANGUAGE_ID, $this->translit_on_add));

                $arElement["IBLOCK_ID"] = $this->next_step["IBLOCK_ID"];
                $this->fillDefaultPropertyValues($arElement, $this->arProperties);

                $arElement["ID"] = $obElement->Add($arElement, $bWF, true, $this->iblock_resize);
                if($arElement["ID"])
                {
                    $counter["ADD"]++;
                }
                else
                {
                    $this->LAST_ERROR = $obElement->LAST_ERROR;
                    $counter["ERR"]++;
                }
            }
        }
        elseif(array_key_exists($this->mess["IBLOCK_XML2_PRICES"], $arXMLElement))
        {
            //Collect price information for future use
            $arElement["PRICES"] = array();
            if (is_array($arXMLElement[$this->mess["IBLOCK_XML2_PRICES"]]))
            {
                foreach($arXMLElement[$this->mess["IBLOCK_XML2_PRICES"]] as $price)
                {
                    if(isset($price[$this->mess["IBLOCK_XML2_PRICE_TYPE_ID"]]) && array_key_exists($price[$this->mess["IBLOCK_XML2_PRICE_TYPE_ID"]], $this->PRICES_MAP))
                    {
                        $price["PRICE"] = $this->PRICES_MAP[$price[$this->mess["IBLOCK_XML2_PRICE_TYPE_ID"]]];
                        $arElement["PRICES"][] = $price;
                    }
                }
            }

            $arElement["DISCOUNTS"] = array();
            if(isset($arXMLElement[$this->mess["IBLOCK_XML2_DISCOUNTS"]]))
            {
                foreach($arXMLElement[$this->mess["IBLOCK_XML2_DISCOUNTS"]] as $discount)
                {
                    if(
                        isset($discount[$this->mess["IBLOCK_XML2_DISCOUNT_CONDITION"]])
                        && $discount[$this->mess["IBLOCK_XML2_DISCOUNT_CONDITION"]]===$this->mess["IBLOCK_XML2_DISCOUNT_COND_VOLUME"]
                    )
                    {
                        $discount_value = $this->ToInt($discount[$this->mess["IBLOCK_XML2_DISCOUNT_COND_VALUE"]]);
                        $discount_percent = $this->ToFloat($discount[$this->mess["IBLOCK_XML2_DISCOUNT_COND_PERCENT"]]);
                        if($discount_value > 0 && $discount_percent > 0)
                            $arElement["DISCOUNTS"][$discount_value] = $discount_percent;
                    }
                }
            }

            if ($arDBElement)
            {
                $arElement["ID"] = $arDBElement["ID"];
                $counter["UPD"]++;
            }
        }

        if(isset($arXMLElement[$this->mess["IBLOCK_XML2_STORE_AMOUNT_LIST"]]))
        {
            $arElement["STORE_AMOUNT"] = array();
            foreach($arXMLElement[$this->mess["IBLOCK_XML2_STORE_AMOUNT_LIST"]] as $storeAmount)
            {
                if(isset($storeAmount[$this->mess["IBLOCK_XML2_STORE_ID"]]))
                {
                    $storeXMLID = $storeAmount[$this->mess["IBLOCK_XML2_STORE_ID"]];
                    $amount = $this->ToFloat($storeAmount[$this->mess["IBLOCK_XML2_AMOUNT"]]);
                    $arElement["STORE_AMOUNT"][$storeXMLID] = $amount;
                }
            }
        }
        elseif(
            array_key_exists($this->mess["IBLOCK_XML2_STORES"], $arXMLElement)
            || array_key_exists($this->mess["IBLOCK_XML2_STORE"], $arXMLElement)
        )
        {
            $arElement["STORE_AMOUNT"] = array();
            $rsStores = $this->_xml_file->GetList(
                array("ID" => "asc"),
                array(
                    "><LEFT_MARGIN" => array($arParent["LEFT_MARGIN"], $arParent["RIGHT_MARGIN"]),
                    "NAME" => $this->mess["IBLOCK_XML2_STORE"],
                ),
                array("ID", "ATTRIBUTES")
            );
            while ($arStore = $rsStores->Fetch())
            {
                if(strlen($arStore["ATTRIBUTES"]) > 0)
                {
                    $info = unserialize($arStore["ATTRIBUTES"]);
                    if(
                        is_array($info)
                        && array_key_exists($this->mess["IBLOCK_XML2_STORE_ID"], $info)
                        && array_key_exists($this->mess["IBLOCK_XML2_STORE_AMOUNT"], $info)
                    )
                    {
                        $arElement["STORE_AMOUNT"][$info[$this->mess["IBLOCK_XML2_STORE_ID"]]] = $this->ToFloat($info[$this->mess["IBLOCK_XML2_STORE_AMOUNT"]]);
                    }
                }
            }
        }

        if($bMatch && $this->use_crc)
        {
            //nothing to do
        }
        elseif($arElement["ID"] && $this->bCatalog && $this->isCatalogIblock)
        {
            $CML_LINK = $this->PROPERTY_MAP["CML2_LINK"];

            $arProduct = array(
                "ID" => $arElement["ID"],
            );

            if(isset($arElement["QUANTITY"]))
                $arProduct["QUANTITY"] = $arElement["QUANTITY"];
            elseif(isset($arElement["STORE_AMOUNT"]) && !empty($arElement["STORE_AMOUNT"]))
                $arProduct["QUANTITY"] = array_sum($arElement["STORE_AMOUNT"]);

            $CML_LINK_ELEMENT = $arElement["PROPERTY_VALUES"][$CML_LINK];
            if (is_array($CML_LINK_ELEMENT) && isset($CML_LINK_ELEMENT["n0"]))
            {
                $CML_LINK_ELEMENT = $CML_LINK_ELEMENT["n0"];
            }
            if (is_array($CML_LINK_ELEMENT) && isset($CML_LINK_ELEMENT["VALUE"]))
            {
                $CML_LINK_ELEMENT = $CML_LINK_ELEMENT["VALUE"];
            }

            if(isset($arElement["BASE_WEIGHT"]))
            {
                $arProduct["WEIGHT"] = $arElement["BASE_WEIGHT"];
            }
            elseif ($CML_LINK_ELEMENT > 0)
            {
                $rsWeight = CIBlockElement::GetProperty($this->arProperties[$CML_LINK]["LINK_IBLOCK_ID"], $CML_LINK_ELEMENT, array(), array("CODE" => "CML2_TRAITS"));
                while($arWeight = $rsWeight->Fetch())
                {
                    if($arWeight["DESCRIPTION"] == $this->mess["IBLOCK_XML2_WEIGHT"])
                        $arProduct["WEIGHT"] = $this->ToFloat($arWeight["VALUE"])*1000;
                }
            }

            if ($CML_LINK_ELEMENT > 0)
            {
                $rsUnit = CIBlockElement::GetProperty($this->arProperties[$CML_LINK]["LINK_IBLOCK_ID"], $CML_LINK_ELEMENT, array(), array("CODE" => "CML2_BASE_UNIT"));
                while($arUnit = $rsUnit->Fetch())
                {
                    if($arUnit["DESCRIPTION"] > 0)
                        $arProduct["MEASURE"] = $arUnit["DESCRIPTION"];
                }
            }

            if(isset($arElement["PRICES"]))
            {
                //Here start VAT handling

                //Check if all the taxes exists in BSM catalog
                $arTaxMap = array();
                $rsTaxProperty = CIBlockElement::GetProperty($this->arProperties[$CML_LINK]["LINK_IBLOCK_ID"], $CML_LINK_ELEMENT, "sort", "asc", array("CODE" => "CML2_TAXES"));
                while($arTaxProperty = $rsTaxProperty->Fetch())
                {
                    if(
                        strlen($arTaxProperty["VALUE"]) > 0
                        && strlen($arTaxProperty["DESCRIPTION"]) > 0
                        && !array_key_exists($arTaxProperty["DESCRIPTION"], $arTaxMap)
                    )
                    {
                        $arTaxMap[$arTaxProperty["DESCRIPTION"]] = array(
                            "RATE" => $this->ToFloat($arTaxProperty["VALUE"]),
                            "ID" => $this->CheckTax($arTaxProperty["DESCRIPTION"], $this->ToFloat($arTaxProperty["VALUE"])),
                        );
                    }
                }

                //First find out if all the prices have TAX_IN_SUM true
                $TAX_IN_SUM = "Y";
                foreach($arElement["PRICES"] as $price)
                {
                    if($price["PRICE"]["TAX_IN_SUM"] !== "true")
                    {
                        $TAX_IN_SUM = "N";
                        break;
                    }
                }
                //If there was found not included tax we'll make sure
                //that all prices has the same flag
                if($TAX_IN_SUM === "N")
                {
                    foreach($arElement["PRICES"] as $price)
                    {
                        if($price["PRICE"]["TAX_IN_SUM"] !== "false")
                        {
                            $TAX_IN_SUM = "Y";
                            break;
                        }
                    }
                    //Check if there is a mix of tax in sum
                    //and correct it by recalculating all the prices
                    if($TAX_IN_SUM === "Y")
                    {
                        foreach($arElement["PRICES"] as $key=>$price)
                        {
                            if($price["PRICE"]["TAX_IN_SUM"] !== "true")
                            {
                                $TAX_NAME = $price["PRICE"]["TAX_NAME"];
                                if(array_key_exists($TAX_NAME, $arTaxMap))
                                {
                                    $PRICE_WO_TAX = $this->ToFloat($price[$this->mess["IBLOCK_XML2_PRICE_FOR_ONE"]]);
                                    $PRICE = $PRICE_WO_TAX + ($PRICE_WO_TAX / 100.0 * $arTaxMap[$TAX_NAME]["RATE"]);
                                    $arElement["PRICES"][$key][$this->mess["IBLOCK_XML2_PRICE_FOR_ONE"]] = $PRICE;
                                }
                            }
                        }
                    }
                }
                foreach($arElement["PRICES"] as $price)
                {
                    $TAX_NAME = $price["PRICE"]["TAX_NAME"];
                    if(array_key_exists($TAX_NAME, $arTaxMap))
                    {
                        $arProduct["VAT_ID"] = $arTaxMap[$TAX_NAME]["ID"];
                        break;
                    }
                }
                $arProduct["VAT_INCLUDED"] = $TAX_IN_SUM;
            }

            if (CCatalogProduct::Add($arProduct))
            {
                //TODO: replace this code after upload measure ratio from 1C
                $iterator = \Bitrix\Catalog\MeasureRatioTable::getList(array(
                    'select' => array('ID'),
                    'filter' => array('=PRODUCT_ID' => $arElement['ID'])
                ));
                $ratioRow = $iterator->fetch();
                if (empty($ratioRow))
                {
                    $ratioResult = \Bitrix\Catalog\MeasureRatioTable::add(array(
                        'PRODUCT_ID' => $arElement['ID'],
                        'RATIO' => 1,
                        'IS_DEFAULT' => 'Y'
                    ));
                    unset($ratioResult);
                }
                unset($ratioRow, $iterator);
            }

            if(isset($arElement["PRICES"]))
                $this->SetProductPrice($arElement["ID"], $arElement["PRICES"], $arElement["DISCOUNTS"]);

            if(isset($arElement["STORE_AMOUNT"])) {
                $this->ImportStoresAmount($arElement["STORE_AMOUNT"], $arElement["ID"], $counter);
                $this->UpdateProductQuantity($arElement["ID"]);
            }
        }


        return $arElement["ID"];
    }

    // пробрасываем псевдосклад в методы на формирование остатков
    function ImportElements($start_time, $interval, $forcedWarehouse = false, $basePriceXmlId=false, $secondaryPriceXmlId=false)
    {
        if($forcedWarehouse) $this->forcedWarehouse = $forcedWarehouse;
        if($basePriceXmlId) $this->basePriceXmlId = $basePriceXmlId;
        if($secondaryPriceXmlId) $this->secondaryPriceXmlId = $secondaryPriceXmlId;


        global $DB;
        $counter = array(
            "ADD" => 0,
            "UPD" => 0,
            "DEL" => 0,
            "DEA" => 0,
            "ERR" => 0,
            "CRC" => 0,
        );
        if($this->next_step["XML_ELEMENTS_PARENT"])
        {
            $obElement = new CIBlockElement();
            $obElement->CancelWFSetMove();
            $bWF = CModule::IncludeModule("workflow");
            $rsParents = $this->_xml_file->GetList(
                array("ID" => "asc"),
                array("PARENT_ID" => $this->next_step["XML_ELEMENTS_PARENT"], ">ID" => $this->next_step["XML_LAST_ID"]),
                array("ID", "LEFT_MARGIN", "RIGHT_MARGIN")
            );
            while($arParent = $rsParents->Fetch())
            {
                if (!$arParent["RIGHT_MARGIN"])
                    continue;

                $counter["CRC"]++;

                $arXMLElement = $this->_xml_file->GetAllChildrenArray($arParent);
                $hashPosition = strrpos($arXMLElement[$this->mess["IBLOCK_XML2_ID"]], "#");

                if(!$this->next_step["bOffer"] && $this->use_offers)
                {
                    if($hashPosition !== false)
                    {
                        $this->next_step["XML_LAST_ID"] = $arParent["ID"];
                        continue;
                    }
                }
                if(array_key_exists($this->mess["IBLOCK_XML2_STATUS"], $arXMLElement) && ($arXMLElement[$this->mess["IBLOCK_XML2_STATUS"]] == $this->mess["IBLOCK_XML2_DELETED"]))
                {
                    $ID = $this->GetElementByXML_ID($this->next_step["IBLOCK_ID"], $arXMLElement[$this->mess["IBLOCK_XML2_ID"]]);
                    if($ID && $obElement->Update($ID, array("ACTIVE"=>"N"), $bWF))
                    {
                        if($this->use_offers)
                            $this->ChangeOffersStatus($ID, "N", $bWF);
                        $counter["DEA"]++;
                    }
                    else
                    {
                        $counter["ERR"]++;
                    }
                }
                elseif(array_key_exists($this->mess["IBLOCK_XML2_BX_TAGS"], $arXMLElement))
                {
                    //This is our export file
                    $ID = $this->ImportElement($arXMLElement, $counter, $bWF, $arParent);
                }
                else
                {
                    $this->arFileDescriptionsMap = array();
                    $this->arElementFilesId = array();
                    $this->arElementFiles = array();
                    //offers.xml
                    if ($this->next_step["bOffer"])
                    {
                        if (!$this->use_offers)
                        {
                            //We have only one information block
                            $ID = $this->ImportElementPrices($arXMLElement, $counter, $arParent);
                        }
                        elseif ($hashPosition === false && !$this->force_offers)
                        {
                            //We have separate offers iblock and there is element price
                            $ID = $this->ImportElementPrices($arXMLElement, $counter, $arParent);
                        }
                        else
                        {
                            $xmlKeys = array_keys($arXMLElement);
                            if ($xmlKeys == array($this->mess["IBLOCK_XML2_ID"], $this->mess["IBLOCK_XML2_PRICES"]))
                            {
                                //prices.xml
                                $ID = $this->ImportElementPrices($arXMLElement, $counter, $arParent);
                            }
                            elseif ($xmlKeys == array($this->mess["IBLOCK_XML2_ID"], $this->mess["IBLOCK_XML2_RESTS"]))
                            {
                                //rests.xml
                                $ID = $this->ImportElementPrices($arXMLElement, $counter, $arParent);
                            }
                            else
                            {
                                //It's an offer in offers iblock
                                $ID = $this->ImportElement($arXMLElement, $counter, $bWF, $arParent);
                            }
                        }
                    }
                    //import.xml
                    else
                    {
                        $ID = $this->ImportElement($arXMLElement, $counter, $bWF, $arParent);
                    }
                }

                if($ID)
                {
                    $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($this->next_step["IBLOCK_ID"], $ID);
                    $ipropValues->clearValues();

                    $DB->Query("UPDATE b_iblock_element SET TIMESTAMP_X = ".$DB->CurrentTimeFunction()." WHERE ID=".$ID);
                    $this->_xml_file->Add(array("PARENT_ID" => 0, "LEFT_MARGIN" => $ID));
                }

                $this->next_step["XML_LAST_ID"] = $arParent["ID"];

                if($interval > 0 && (time()-$start_time) > $interval)
                    break;
            }
        }
        $this->CleanTempFiles();
        return $counter;
    }


    function ImportStoresAmount($arElement, $elementID, &$counter)
    {
        $arFields = array();
        $arFields['PRODUCT_ID'] = $elementID;
        static $arStoreResult = false;
        if ($arStoreResult === false)
        {
            $arStoreResult = array();
            $resStore =  CCatalogStore::GetList(array(), array(), false, false, array("ID", "XML_ID"));
            while($arStore = $resStore->Fetch())
            {
                $arStoreResult[$arStore["XML_ID"]] = $arStore["ID"];
            }
        }

        foreach($arElement as $xmlID => $amount)
        {
            if (isset($arStoreResult[$xmlID]))
            {
                $arFields['STORE_ID'] = $arStoreResult[$xmlID];
                $arFields['AMOUNT'] = $amount;
                $res = CCatalogStoreProduct::UpdateFromForm($arFields);
                if(!$res)
                    $counter["ERR"]++;
            }
        }

        return true;
    }

    // пересчёт суммарных остатков товара, т.е. поля QUANTITY
    function UpdateProductQuantity($productId, $quantity = false) {
        if(!$quantity) {
            // обновляем общий остаток товара, собирая все склады
            $rsStore = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' => $productId, ">AMOUNT" => 0), false, false, array('*'));
            $productAmount = 0;
            $arStores = [];
            // перебираем собранные склады
            while ($arStore = $rsStore->Fetch()) {
                $arStores[] = $arStore;
                $productAmount+=$arStore['AMOUNT'];
            }
            CCatalogProduct::Update($productId, ['QUANTITY' => $productAmount]);
        }
        else {
            // обновляем остаток товара по переданному значению
            CCatalogProduct::Update($productId, ['QUANTITY' => $quantity]);
        }
    }

    /**
     * @param int $PRODUCT_ID
     * @param array $arPrices
     * @param bool|array $arDiscounts
     */
    // задание цены. Дополнительно при её отсутствии задаётся базовая цена из второй цены
    function SetProductPrice($PRODUCT_ID, $arPrices, $arDiscounts = false)
    {
        $arDBPrices = array();
        $rsPrice = CPrice::GetList(array(), array("PRODUCT_ID" => $PRODUCT_ID));
        while($ar = $rsPrice->Fetch())
            $arDBPrices[$ar["CATALOG_GROUP_ID"].":".$ar["QUANTITY_FROM"].":".$ar["QUANTITY_TO"]] = $ar["ID"];

        $arToDelete = $arDBPrices;

        if(!is_array($arPrices))
            $arPrices = array();

        foreach($arPrices as $price)
        {
            if(!isset($price[$this->mess["IBLOCK_XML2_CURRENCY"]]))
                $price[$this->mess["IBLOCK_XML2_CURRENCY"]] = $price["PRICE"]["CURRENCY"];

            $arPrice = Array(
                "PRODUCT_ID" => $PRODUCT_ID,
                "CATALOG_GROUP_ID" => $price["PRICE"]["ID"],
                "^PRICE" => $this->ToFloat($price[$this->mess["IBLOCK_XML2_PRICE_FOR_ONE"]]),
                "CURRENCY" => $this->CheckCurrency($price[$this->mess["IBLOCK_XML2_CURRENCY"]]),
            );

            if (
                strlen($price[$this->mess["IBLOCK_XML2_QUANTITY_FROM"]])
                || strlen($price[$this->mess["IBLOCK_XML2_QUANTITY_TO"]])
            )
            {
                $arPrice["QUANTITY_FROM"] = $price[$this->mess["IBLOCK_XML2_QUANTITY_FROM"]];
                $arPrice["QUANTITY_TO"] = $price[$this->mess["IBLOCK_XML2_QUANTITY_TO"]];
                $arPrice["PRICE"] = $arPrice["^PRICE"];

                $id = $arPrice["CATALOG_GROUP_ID"].":".$arPrice["QUANTITY_FROM"].":".$arPrice["QUANTITY_TO"];
                if(array_key_exists($id, $arDBPrices))
                {
                    CPrice::Update($arDBPrices[$id], $arPrice);
                    unset($arToDelete[$id]);
                }
                else
                {
                    CPrice::Add($arPrice);
                }
            }
            else
            {
                foreach($this->ConvertDiscounts($arDiscounts) as $arDiscount)
                {
                    $arPrice["QUANTITY_FROM"] = $arDiscount["QUANTITY_FROM"];
                    $arPrice["QUANTITY_TO"] = $arDiscount["QUANTITY_TO"];
                    if($arDiscount["PERCENT"] > 0)
                        $arPrice["PRICE"] = $arPrice["^PRICE"] - $arPrice["^PRICE"]/100*$arDiscount["PERCENT"];
                    else
                        $arPrice["PRICE"] = $arPrice["^PRICE"];

                    $id = $arPrice["CATALOG_GROUP_ID"].":".$arPrice["QUANTITY_FROM"].":".$arPrice["QUANTITY_TO"];
                    if(array_key_exists($id, $arDBPrices))
                    {
                        CPrice::Update($arDBPrices[$id], $arPrice);
                        unset($arToDelete[$id]);
                    }
                    else
                    {
                        CPrice::Add($arPrice);
                    }
                }
            }
        }

        // собираем цены
        $arPrices = CCatalogGroup::GetListArray();
        $ID_BASE_PRICE = false;
        $ID_CATALOG_2_PRICE = false;

        // и определяем ID цен по внешнему коду
        foreach($arPrices as $price){
            if($price['XML_ID'] == "40ff571d-9ecb-11e8-93f3-d850e6e3b156")
                $ID_BASE_PRICE = $price['ID'];
            if($price['XML_ID'] == "eb101f8b-3121-11e9-80cf-d850e6e3b156")
                $ID_CATALOG_2_PRICE = $price['ID'];
        }


        AddMessage2Log('$PRODUCT_ID = '.print_r($PRODUCT_ID, true),'');

        // есть проблемы с ценами - импорт всегда удаляет отсутствующие в нём цены. сейчас вроде исправлено, но
        // проверить не успеваю - клиент свернул задачу.
        // если не работает, нужно либо обеспечить очистку элементов массива, отвечающих за нужную цену (я шёл по этому
        // пути), либо просто не удалять цены вообще если есть принудительный склад
        foreach($arToDelete as $key => $id)
        {
            $arId = explode(':', $key);
            AddMessage2Log('$arToDelete = '.print_r($arToDelete, true),'');
            AddMessage2Log('$arId = '.print_r($arId, true),'');
            AddMessage2Log('qwe = '.print_r(in_array($arId[0], [$ID_BASE_PRICE, $ID_CATALOG_2_PRICE]), true),'');
            if(!in_array($arId[0], [$ID_BASE_PRICE, $ID_CATALOG_2_PRICE]))
                CPrice::Delete($id);
        }

        // если заданы базовая цена и доп. цена, то проверяем, есть ли у товара базовая цена
        // если её нет, заполняем её из доп. цены
        if($this->basePriceXmlId && $this->secondaryPriceXmlId) {
            if($ID_BASE_PRICE && $ID_CATALOG_2_PRICE) {
                $rsPrices = CPrice::GetListEx(array(), array('PRODUCT_ID' =>$PRODUCT_ID), false, false, array('*'));
                $arProductPrices = [];
                while($arPrice = $rsPrices->Fetch()){
                    if(in_array($arPrice['CATALOG_GROUP_ID'], [$ID_BASE_PRICE,$ID_CATALOG_2_PRICE]))
                        $arProductPrices[] = $arPrice;
                }
                AddMessage2Log('$arProductPrices = '.print_r($arProductPrices, true),'');
                if(empty($arProductPrices[$ID_BASE_PRICE]) && !empty($arProductPrices[$ID_CATALOG_2_PRICE])) {
                    $arFieldsPrice = Array(
                        "PRODUCT_ID" => $PRODUCT_ID,
                        "CATALOG_GROUP_ID" => $ID_BASE_PRICE,
                        "PRICE" => $arProductPrices[$ID_CATALOG_2_PRICE]['PRICE'],
                        "CURRENCY" => $arProductPrices[$ID_CATALOG_2_PRICE]['CURRENCY'],
                    );
                    CPrice::Add($arFieldsPrice);
                }
            }


        }
    }


}