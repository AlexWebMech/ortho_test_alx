<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

\Bitrix\Main\Loader::includeModule("gorgoc.registrationsms");
\Bitrix\Main\Loader::includeModule("mlife.smsservices");


/**
 * Компонент реагирует на восстановление пароля по смс(как единая точка восстановления)
 * Что бы получить смс с новым паролем нужно передать в запросе
 * action:	5
 * request:	ajax
 * phone:	Номер телефона из формы
 * ssid:	индификатор сессии битрикс
 *
 * Авторизует пользователя
 *
 */
class SiteAuth extends CBitrixComponent {
    
    protected $ref_start = ''; //Значение страницы из сессии
    protected $is_spam = 0; //спам или не спам
    protected $request = []; //весь запрос
    protected $login = null; //логин=телефон
    protected $phone = null; //телефон
    protected $pswd = null; //пароль
    protected $user_type = 1; //1-розница, 2 - опт
    protected $referer = null; //хеш адреса страницы с которого запущена форма
    protected $ssid = null; //Индификатор сессии битрикс
    public $error_field = []; //Поля с ошибками
    protected $autorize_disable = false; //Разрешена ли авторизация через этот компонент
    protected $group_allow = [18]; //Разрешённые группы, в которых должен быть пользователь что бы авторизоваться через компонент
    protected $ex_user_group = [1]; //Запрещённые группы для авторизации
    protected $action = null; //Тип запроса(1-запрос формы,2-установка типа учётной записи,3-выход)
    protected $salt = 'coffe'; //Соль для пароля
    
    public function executeComponent() {
        
        try {
            
            $this->setParams();
            
            $this->templateAction();
        } catch (Exception $ex) {
            $this->getJsonPage('', $ex->getMessage(), false);
        }
    }
    
    protected function templateAction() {
        
        if (!empty($this->request['request']) && $this->request['request'] == 'ajax') {//ajax запросы
            $this->ajaxTemplate();
        } else {//Обычный шаблон вход
            $this->includeComponentTemplate();
        }
    }
    
    protected function ajaxTemplate() {
        
        global $USER;
        
        if ($this->action === 1) {//Показать форму авторизации
            ob_start();
            $this->IncludeComponentTemplate('modal');
            $page = ob_get_contents();
            ob_end_clean();
            $this->getJsonPage($page);
        } elseif ($this->action === 2) {//Авторизовать под определённым пользователем
            $this->checkField();
           
            
            $arUserType = $this->checkUser();
            
            /**
             * Все доступные варианты кнопок для авторизации
             */
            $this->arResult = array_merge($this->arResult, ['USER_PROFILES' => $arUserType]);
            
//            if (count($arUserType) > 1) {//Два профиля - возрващаем кнопки
//                ob_start();
//                $this->IncludeComponentTemplate('form_user_type');
//                $page = ob_get_contents();
//                ob_end_clean();
//                $this->getJsonPage($page);
//            }
            
            if (count($arUserType) === 1) {//Один профиль
                foreach ($arUserType as $gp_id => $arValue) {
                    $remember = true;
                   
                    $back_url = null;
                   
                    if ($arValue['AUTH_CHECK'] === true) {
                        if (!$USER->Authorize($arValue['ID'], $remember)) {
                            throw new \Exception('Авторизация не удалась, попробуйте ещё раз', 200);
                        } else {
                            $USER->CheckAuthActions();
                            
                            $back_url = '/personal';
                            
                            $this->getJsonPage('', 'Вы авторизованы. Сейчас страница будете перезагружена', 'success', [
                                'action' => 'reload',
                                'bsave' => $remember,
                                'back_url' => $back_url
                            ]);
                        }
                    } else {
                        throw new \Exception('Не верный логин или пароль', 200);
                    }
                }
            }//5372
            
            throw new \Exception('Не известная ошибка авторизации. Обратитесь к администратору', 200);
        } elseif ($this->action === 3) {//Авторизация по типу пользователя
            $this->checkField();
            $this->authToUserType();
        } elseif ($this->action === 4) {//Выход
            $USER->Logout();
            $this->getJsonPage('', 'Сеанс завершён', 'success', ['action' => 'reload']);
        } elseif ($this->action === 5) {//Восстановление пароля
            $this->checkBot();
            
            if ($this->passwordRecovery()) {
                $this->getJsonPage('', 'Новый пароль выслан вам в смс сообщении.');
            }
            throw new \Exception('Ошибка восстановления пароля', 200);
        } else {
            throw new \Exception('Ошибка параметра авторизации', 200);
        }
    }
    
    /**
     * Для возврата результата в виде JSON
     * @param type $html
     * @param type $message
     * @param type $result
     * @param type $params
     */
    protected function getJsonPage($html, $message = '', $result = 'success', $params = []) {
        
        ob_end_clean();
        
        header('Content-type: application/json;');
        
        echo Bitrix\Main\Web\Json::encode([
            'modal_id' => $this->arParams['MODAL_ID'],
            'success' => $result,
            'message' => $html,
            'text' => $message,
            'params' => $params,
        ]);
        
        CMain::FinalActions();
        
        die();
    }
    
    /**
     * Восстановление пароля
     */
    protected function passwordRecovery() {
        
        $phone = \Gorgoc\Registrationsms\Helpers::isMobilPhone11($this->phone);
        
        $type = 'pswdrecovery';
        
        if (empty($phone)) {
            throw new \Exception('Вы не указали номер телефона', 200);
        }
        
        $this->phone = $phone;
        
        $rsUser = $this->getUser();
        
        if (empty($rsUser)) {
            throw new \Exception('Учётная запись не подтверждена', 200);
        }
        
        if (!\Gorgoc\Registrationsms\CheckSmsLimit::isAdd($this->phone, $type, 300)) {//Запрет на восстановление паролья по лимиту времени
            throw new \Exception('Вы уже запрашивали восстановление пароля, повторите попытку позднее', 200);
        }
        
        $newpass = rand(100000, 999999);
        
        $update_result = false;
        
        foreach ($rsUser as $arValue) {
            
            $user = new \CUser();
            $fields = [
                "PASSWORD"         => $newpass,
                "CONFIRM_PASSWORD" => $newpass,
                "TYPE_UPDATE"=>"SMS"
            
            ];
            
            $result = $user->Update($arValue["ID"], $fields);
            
            if (empty($user->LAST_ERROR)) {
                $update_result = true;
            }
        }
        
        if ($update_result) {
            if (class_exists('\CMlifeSmsServices')) {
                $obSmsServ = new \CMlifeSmsServices();
                
                $arSend = $obSmsServ->sendSms($this->phone, "Новый пароль для входа в личный кабинет " . $newpass);
                
                \Gorgoc\Registrationsms\ChecksmsTable::add(
                    [
                        "PHONE"   => $this->phone,
                        "CODE" => $newpass,
                        'TYPE'    => $type,
                        'MESSAGE' => 'Восстановление пароля из: ' . __FILE__
                    ]
                );
                
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Проверка обязательных полей
     * @throws Exception
     */
    protected function checkField() {
        
        if (empty($this->request)) {
            throw new \Exception('Пустые параметры авторизации', 200);
        }
        
        if (!isset($this->request['phone']) || empty($this->request['phone'])) {
            
            $this->error_field['phone'] = true;
            
            throw new \Exception('Не указанно обязательное поле', 200);
        }
        if (!isset($this->request['password']) || empty($this->request['password'])) {
            
            $this->error_field['password'] = true;
            
            throw new \Exception('Не указанно обязательное поле', 200);
        }
        
        if (!isset($this->request['referer']) || empty($this->request['referer'])) {
            
            throw new \Exception('Перезагрузите страницу', 200);
        }
        
        if (!isset($this->request['action']) || empty($this->request['action'])) {
            
            throw new \Exception('Не указан обязательный параметр', 200);
        }
        
        if (empty($this->phone) || empty($this->login)) {
            throw new \Exception('Пустые параметры входа', 200);
        }
        
        $this->checkBot();
    }
    
    /**
     * Проверки всех входных параметров
     * Устанавливает все необходимые значения для компонента
     * @throws \Exception выбросит искелючение, если чего то не хватило
     */
    protected function setParams() {
        
        $arParams = [];
        
        $context = \Bitrix\Main\Application::getInstance()->getContext();
        
        $request = $context->getRequest();
        
        if ($this->autorize_disable) {
            throw new \Exception('Авторизация временно не доступна', 200);
        }
        
        $arParams['MODAL_ID'] = 'gorgoc-loginform';
        
        if (!empty($request)) {
            $this->request = $request;
        }
        
        if (!empty($this->request)) {
            
            $this->login = preg_replace("/[^0-9]/", '', $this->request['phone']);
            
            $this->phone = preg_replace("/[^0-9]/", '', $this->request['phone']);
            
            $this->pswd = $this->request['password'];
            
            $this->referer = $this->request['referer'];
            
            $this->ssid = $this->request['ssid'];
            
            $this->user_type = intval($this->request['user_type']);
            
            $this->action = intval($this->request['action']);
        }
        
        $this->arParams = array_merge($this->arParams, $arParams);
    }
    
    /**
     * Не используется в рамках ТЗ
     *
     * Авторизация по профилю при помощи открытого хэша из профиля
     * @throws \Exception
     */
    protected function authToUserType() {
        
        return false;//Не используется
        
        global $USER;
        
        
        $rsUser = $this->getUser();
        
        $auth = false;
        $user_id = null;
        
        foreach ($rsUser as $gp_id => $arValue) {
            $has_pswd = $this->getHashPswd($arValue['PASSWORD']);
            
            if ($this->pswd === $has_pswd) {//хэш из формы и хэш из профиля
                $auth = true;
            }
        }
        
        $back_url = null;
        
        if ($this->user_type === 1) {
            $user_id = $rsUser[5]['ID'] ?? -1;
            $remember = false;
        } elseif ($this->user_type === 2) {
            $user_id = $rsUser[8]['ID'] ?? -1;
            $remember = true;
            $back_url = '/personal';
        } elseif ($this->user_type === 3) {
            $user_id = $rsUser[21]['ID'] ?? -1;
            $remember = true;
            $back_url = '/opt/e-order/';
        } else {
            throw new \Exception('Авторизация не удалась. Не ожиданный параметр', 200);
        }
        
        if ($auth === true && $user_id > 1) {
            if ($USER->Authorize($user_id, $remember)) {
                $USER->CheckAuthActions();
                $this->getJsonPage('', 'Вы авторизованы. Сейчас страница будете перезагружена', 'success', ['action' => 'reload', 'bsave' => $remember, 'back_url' => $back_url]);
            }
        }
        
        throw new \Exception('Авторизация не удалась, попробуйте ещё раз.', 200);
    }
    
    /**
     * Вернёт пользователя по логину(номер телефона)
     * Результирующий массив содержит вложенные массивы с найденным пользователем в группах, ключи массивов - ИД группы
     * @return type
     */
    protected function getUser()
    {
        $filter = [
            [
                'LOGIC' => 'OR',
                ['=LOGIN' => $this->phone],
                ['=LOGIN' => 'opt_' . $this->phone]
            ],
            'ACTIVE'         => 'Y',
            'GROUP.GROUP_ID' => $this->group_allow,
        ];
        
        $rsUser = \Bitrix\Main\UserTable::getlist(
            [
                'filter'  => $filter,
                'select'  => [
                    'ID',
                    'PERSONAL_PHONE',
                    'PASSWORD',
                    'GROUP_ID' => 'GROUP.GROUP_ID',
                    'LOGIN',
                ],
                'limit'   => 5,
                'runtime' => [
                    new \Bitrix\Main\Entity\ReferenceField(
                        'GROUP', '\Bitrix\Main\UserGroupTable',
                        ['=this.ID' => 'ref.USER_ID']
                    ),
                ],
                //'group'=>['LOGIN','GROUP_ID']
            ]
        )->fetchAll();
        
        $arResult = [];
        
        if(empty($rsUser)) {
            return [];
        }
        
        $rsUser = [reset($rsUser)];//1-н телефон = 1 тип учётный профиль
        
        
        foreach ($rsUser as $arValue) {
            foreach ($this->ex_user_group as $gpId) { //Проверка на запрещённую группу
                if(in_array($gpId,\CUser::GetUserGroup($arValue['ID']))){
                    break 2;
                }
            }
            $arResult[$arValue['GROUP_ID']] = $arValue;
        }
       
        return $arResult;
    }
    
    /**
     * Проверка пользователя
     * Вернёт массив с ИД групп пользователей
     * или выбросит исключение
     */
    protected function checkUser() {
        global $USER;
        
        
        $rsUser = $this->getUser();
        
        $arUsers = [];
        
        $auth = false;
        
        
        foreach ($rsUser as $arValue) {
            if ($this->checkPassword($arValue['PASSWORD'], $this->pswd)) {//Если хотя бы одна из учёток имеет валидный пароль по этому номеру
                $auth = true;
                $arValue['AUTH_CHECK'] = true;
                $arValue['HASH_PSWD'] = $this->getHashPswd($arValue['PASSWORD']);
                $this->arResult['HASH_PSWD'] = $this->getHashPswd($arValue['PASSWORD']);
                $this->arResult['PHONE'] = $this->phone;
            }
            $arUsers[$arValue['GROUP_ID']] = $arValue;
        }
        
        if (empty($auth)) {
            throw new \Exception('Не верный логин или пароль', 200);
        }
        
        return $arUsers;
    }
    
    
    /**
     * Проверка пароля по битрикс
     *
     * @param string $profile_pswd пароль из профиля пользователя
     * @param string $form_pswd    пароль из формы
     *
     * @return bool
     */
    protected function checkPassword($profile_pswd, $form_pswd):bool {
        
        $db_password = '';
        
        if (strlen($profile_pswd) > 32) {
            
            $salt = substr($profile_pswd, 0, strlen($profile_pswd) - 32);
            
            $db_password = substr($profile_pswd, -32);
        } else {
            
            $salt = "";
            
            $db_password = $profile_pswd;
        }
        
        $user_password = md5($salt . $form_pswd);
        
        if ($user_password === $db_password) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Проверка на спам
     * В перспективе сюда можно вставить проверку РеКапчи или прочие механизмы отсечения ботов и хаккеров
     *
     * @return boolean
     * @throws Exception
     */
    protected function checkBot():bool {
        
        if (bitrix_sessid() !== $this->ssid) {
            $this->is_spam = 1;
        }
        if ($this->is_spam === 1) {
            throw new \Exception('Извините,но мы не смогли идентифицировать вас. Попробуйте обновить страницу', 200);
        }
        
        return true;
    }
    
    protected function getHashPswd($hash_base) {
        return md5($this->salt . '' . $hash_base) . '&' . md5(date('dmY'));
    }
    
}
