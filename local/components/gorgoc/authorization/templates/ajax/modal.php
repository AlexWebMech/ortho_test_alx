<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<?php
/**
 * Форма входа первичная
 */
?>
    <div id = "<?php echo $component->arParams['MODAL_ID']; ?>" class = "modal fade form-modal" tabindex = "-1" role = "dialog" aria-hidden="true">
        <script>
            <?php
               echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . $templateFolder . '/script.js');
            ?>

            $(document).ready(function () {

              var form_login = FormLoginAction({MODAL_ID: '<?php echo $component->arParams['MODAL_ID'] ?>'});

              form_login.init();

            });

        </script>
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <?php echo bitrix_sessid_post(); ?>
                <div class="modal-header">
                    <div class="modal-title h5">Авторизация</div>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times</span></button>
                </div>
                <div class="modal-body">

                    <div class="form_login_content">
                        <?php include_once 'form_login.php'; ?>
                    </div>

                    <div class="modal-input-wrapper flex-container-w100 flex-center">
                        <?php if ($USER->IsAuthorized()) { ?>
                            <button
                                    onclick="CrossUtils.getFormTxt('/ajax/get/loginform/', {'back_url': '/', 'action': '4'}); return false;"
                                    type="button"
                                    class="btn btn-main sent-form"
                            >Выйти</button>
                        <?php } else { ?>
                            <button
                                    type="button"
                                    class="btn btn-main sent-form"
                            >Войти</button>
                        <?php } ?>

                    </div>

                </div>

            </div>
        </div>
        <style>
            #<?php echo $component->arParams['MODAL_ID']; ?> .pswd_sent_sms {
                font-size: 14px;
                text-align: right;
                text-decoration: none;
            }

            #<?php echo $component->arParams['MODAL_ID']; ?> .pswd_sent_sms:hover {
                color: #588fc3;
            }
            #<?php echo $component->arParams['MODAL_ID']; ?> .message {
                font-size: 14px;
                height: 20px;
                color:#334771;
            }

        </style>
    </div>

<?php
return;
