FormLoginAction = (function (params) {

    var modail_id = '#' + params.MODAL_ID;
    var url = '/ajax/get/loginform/';
    var self = this;
    $('input[type="tel"]').mask("+7(999)999-99-99");
    var tool = {

        bindEvents: function (events) {
            for (var i = 0, l = events.length; i < l; i++) {
                if (!events[i].element) {
                    $(events[i].target).on(events[i].event, events[i].handler)
                } else {
                    $(events[i].element).on(events[i].event, events[i].target, events[i].handler)
                }
            }
        },
        unbindEvents: function (events) {
            for (var i = 0, l = events.length; i < l; i++) {
                $(events[i].target).off(events[i].event);
            }
        },
    }

    var form = {
        logout: function () {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'logout': '1'
                }
            }).done(function (data) {

                $(modail_id + ' .message').html(data.text);

                if (data.message) {
                    $(modail_id).find('.message').html(data.message);
                }
                if (data.back_url) {
                    setTimeout(function () {
                        window.location.href = data.back_url;
                    }, 1000);
                }
            });
        },
        sent: function () {
            var data = {};
            var form_bad = false;
            $(modail_id + ' input.login_input').each(function (index, element) {
                var ind = $(element).attr('name');
                var val = $(element).val();
                if (typeof ($(element).attr('required')) != 'undefined') {
                    if (val.length < 3) {
                        $(element).parent('.form-group').addClass('has-error');
                        form_bad = true;
                        return false;
                    } else {
                        $(element).parent('.form-group').removeClass('has-error');
                    }
                }
                data[ind] = val;
            });
            if (form_bad) {
                return false;
            }
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data
            }).done(function (data) {

                $(modail_id + ' .message').html(data.text);

                if (data.success) {
                    if (data.message) {
                        $(modail_id + ' .form_login_content').html(data.message);
                        $(modail_id + ' .modal-footer').remove();
                    }
                } else {
                    $(modail_id + ' .message').html(data.text);
                    setTimeout(function () {
                        $(modail_id + ' .message').html('');
                    }, 3000);
                }
            }).done(function (data) {
                if (typeof data !== 'undefined' && data.params) {
                    if (data.params.back_url && (data.params.back_url).length > 4) {
                        window.location.href = data.params.back_url;
                    } else if (data.params.action && data.params.action === 'reload') {
                        window.location.reload();
                    }
                }
            });
        },
        sentUserType: function () {
            var data = {};
            var self = this;
            $(modail_id + ' input.login_input').each(function (index, element) {
                var ind = $(element).attr('name');
                var val = $(element).val();

                data[ind] = val;
            });

            data['user_type'] = $(self).attr('data-user_type');

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data
            }).done(function (data) {
                if (data.success) {
                    $(modail_id + ' .message').html(data.text);
                } else {
                    $(modail_id + ' .message').html(data.text);
                    setTimeout(function () {
                        $(modail_id + ' .message').html('');
                    }, 3000);
                }
            }).done(function (data) {
//                if (typeof data !== 'undefined' && data.params && data.params.action) {
//                    if (data.params.action === 'reload') {
//                        window.location.reload();
//                    }
//                }
                if (typeof data !== 'undefined' && data.params) {
                    if (data.params.back_url && (data.params.back_url).length > 4) {
                        window.location.href = data.params.back_url;
                    } else if (data.params.action && data.params.action === 'reload') {
                        window.location.reload();
                    }
                }
            });
        },
        pswdRecovery: function () {
            var data = {
                action: 5,
                request: 'ajax',
            };

            var phone = $(modail_id + ' input[name="phone"]').val();

            if (typeof phone === 'undefined') {
                phone = 0;
            }

            data.phone = phone;
            data.ssid = BX.bitrix_sessid();


            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data
            }).done(function (data) {
                if (data.success) {
                    $(modail_id + ' .message').html(data.text);
                } else {
                    $(modail_id + ' .message').html(data.text);

                }
                setTimeout(function () {
                    $(modail_id + ' .message').html('');
                }, 3000);
            });
        }
    }


    bindEvents = function () {
        var event = [
            {
                element: modail_id,
                target: '.sent-form-logout',
                event: 'click',
                handler: form.logout
            },
            {
                element: modail_id,
                target: '.sent-form',
                event: 'click',
                handler: form.sent
            },
            {
                element: modail_id,
                target: '.sent_user_type',
                event: 'click',
                handler: form.sentUserType
            },
            {
                element: modail_id,
                target: '.pswd_sent_sms',
                event: 'click',
                handler: form.pswdRecovery
            }

        ];
        tool.bindEvents(event);
    }

    self.init = function () {
        self.bindEvents();
    }

    return {
        init: self.init
    }
});
