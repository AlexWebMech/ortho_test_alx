<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<?php
/**
 * Форма с кнопками
 */
?>

<div class="col-xs-12">
    <form role="form">
        <?php if (empty($component->arResult['USER_PROFILES'])) { ?>
            <button data-user_type="1" type="button" class="btn btn-block btn-main sent_user_type">Розничный покупатель</button>
            <?php
        } else {
            foreach ($component->arResult['USER_PROFILES'] as $group_id => $user_data) {
                if ($group_id == '5') {
                    ?>
                    <button data-user_type="1" type="button" class="btn btn-block btn-main sent_user_type">Розничный покупатель</button>
                    <?php
                }
                if ($group_id == '8') {
                    ?>
                    <button data-user_type="2" type="button" class="btn btn-block btn-main sent_user_type">Оптовый покупатель</button>
                    <?php
                }
                if ($group_id == '21') {
                    ?>
                    <button data-user_type="3" type="button" class="btn btn-block btn-main sent_user_type">Торговый представитель</button>
                    <?php
                }
            }
        }
        ?>



    </form>
    <input name="ssid" class="login_input" type="hidden" value="<?php echo bitrix_sessid(); ?>">
    <input name="referer" class="login_input" type="hidden" value="<?php echo md5($_SERVER['REQUEST_URI']); ?>">
    <input name="action" class="login_input" type="hidden" value="3">
    <input name="back_url" class="login_input" type="hidden" value="/">
    <input name="password" type="hidden" class="login_input" value="<?php echo $component->arResult['HASH_PSWD'] ?>">
    <input name="phone" type="hidden" class="login_input" value="<?php echo $component->arResult['PHONE'] ?>">
    <input name="request" class="login_input" type="hidden" value="ajax">
</div>
<div class="col-xs-12">
    <p class="message"></p>
</div>