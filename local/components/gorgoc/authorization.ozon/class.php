<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php
use Gorgoc\Registrationsms\Helpers;
use Bitrix\Main\Web\Json;
use Gorgoc\Registrationsms\FormsTable;
use Gorgoc\Registrationsms\ChecksmsTable;
use Gorgoc\Registrationsms\CheckSmsLimit;

\Bitrix\Main\Loader::includeModule("gorgoc.registrationsms");
\Bitrix\Main\Loader::includeModule("mlife.smsservices");


/**
 * Компонент реагирует на восстановление пароля по смс(как единая точка восстановления)
 * Что бы получить смс с новым паролем нужно передать в запросе
 * action:	5
 * request:	ajax
 * phone:	Номер телефона из формы
 * ssid:	индификатор сессии битрикс
 *
 * Авторизует пользователя
 *
 */
class SiteAuthOzon extends CBitrixComponent {
    
    protected $ref_start = ''; //Значение страницы из сессии
    protected $is_spam = 0; //спам или не спам
    protected $request = []; //весь запрос
    protected $login = null; //логин=телефон
    protected $phone = null; //телефон
    protected $pswd = null; //пароль
    protected $user_type = 1; //1-розница, 2 - опт
    protected $referer = null; //хеш адреса страницы с которого запущена форма
    protected $ssid = null; //Индификатор сессии битрикс
    public $error_field = []; //Поля с ошибками
    protected $autorize_disable = false; //Разрешена ли авторизация через этот компонент
    protected $group_allow = [18]; //Разрешённые группы, в которых должен быть пользователь что бы авторизоваться через компонент
    protected $ex_user_group = [1]; //Запрещённые группы для авторизации
    protected $action = null; //Тип запроса(1-запрос формы,2-установка типа учётной записи,3-выход)
    protected $salt = 'coffe'; //Соль для пароля
    
    public function executeComponent() {
        
        try {
            
            $this->setParams();
            
            $this->templateAction();
        } catch (Exception $ex) {
            $this->getJsonPage('', $ex->getMessage(), false,$this->error_field);
        }
    }
    
    protected function templateAction() {
        
        if (!empty($this->request['request']) && $this->request['request'] == 'ajax') {//ajax запросы
            $this->ajaxTemplate();
        } else {//Обычный шаблон вход
            $this->includeComponentTemplate();
        }
    }
    
    protected function ajaxTemplate() {
        
        global $USER;
        
        if ($this->action === 1) {//Показать форму авторизации
            ob_start();
            $this->IncludeComponentTemplate('modal');
            $page = ob_get_contents();
            ob_end_clean();
            $this->getJsonPage($page);
        } elseif ($this->action === 4) {//Выход
            $USER->Logout();
            $this->getJsonPage('', 'Сеанс завершён', 'success', ['action' => 'reload']);
        } elseif ($this->action === 6) { // Обрабатывает введённый пользователем номер телефона
            $phone = Helpers::isMobilPhone11($this->request['phone-login']);
            if (empty($this->request['phone-login']) || !$phone) {
                $this->error_field['phone-login'] = true;
                throw new \Exception('Укажите номер телефона', 200);
            }
            $user = $this->getUser($phone);
            $this->arResult['PHONE'] = $phone;
            ob_start();
            if($user) {//Форма ввода кода
                $this->initAuthUser($user);
                $this->IncludeComponentTemplate('login_form_by_code');
                $page = ob_get_contents();
            }else { //Форма регистрации
                $this->IncludeComponentTemplate('registration_form_by_code');
                $page = ob_get_contents();
            }
            $this->getJsonPage($page);
            ob_end_clean();
            
        }elseif ($this->action === 7) { //Отправка данных с формы регистрации
            $phone = Helpers::isMobilPhone11($this->request['registration-phone']??'');
            $email = filter_var($this->request['registration-email']??'',FILTER_SANITIZE_EMAIL);
            $name = filter_var($this->request['registration-name']??'',FILTER_SANITIZE_STRING);
            
            if($name) {
                $name = Bitrix\Main\Text\Encoding::convertEncoding($name,'UTF-8',SITE_CHARSET);
            }
            if (!$phone) {
                $this->error_field['registration-phone'] = true;
                throw new \Exception('Укажите номер телефона', 200);
            }
            $agreement = $this->request['registration-agreement']??'';
            if(!$agreement) {
                $this->error_field['text-agreement'] = true;
                throw new \Exception('Требуется согласие', 200);
            }
            $this->arResult['PHONE'] = $phone;
            if($user = $this->userRegistration($phone, $email, $name)) {
                $this->initAuthUser($user);
                ob_start();
                $this->IncludeComponentTemplate('login_form_by_code');
                $page = ob_get_contents();
                $this->getJsonPage($page);
            }
            throw new \Exception('Что то пошло не так, попробуйте позднее', 200);
            
        }elseif ($this->action === 8) { //С формы приходит код подтверждения - и просиходит Авторизация
            $phone = Helpers::isMobilPhone11($this->request['phone-login']);
            if($checkId = $this->isValidCode($this->request['codes'],$this->ssid, $phone)) {
                $this->updateUserPassword($checkId);
                if($this->userAuthorization($checkId)) {
                    $this->getJsonPage('', 'Вы авторизованы. Сейчас страница будете перезагружена', true, [
                        'action' => 'reload',
                    ]);
                }else {
                    throw new \Exception('Что то пошло не так, попробуйте позднее', 200);
                }
                
            }else {
                throw new \Exception('Введите текст из смс', 200);
            }
        }elseif ($this->action === 9) { //Вернёт форму входа по email
            ob_start();
            $this->IncludeComponentTemplate('form_login_email');
            $page = ob_get_contents();
            $this->getJsonPage($page);
        }elseif ($this->action === 10) { //Обработка введённого email-а
            $email = filter_var($this->request['email-login'],FILTER_VALIDATE_EMAIL);
            if (empty($this->request['email-login']) || !$email) {
                $this->error_field['email-login'] = true;
                throw new \Exception('Укажите email', 200);
            }
            $email = filter_var($email,FILTER_SANITIZE_EMAIL);
            $user = $this->getUser(null, $email);
            ob_start();
            if($user && Helpers::isMobilPhone11($user['PERSONAL_PHONE'])) {//Форма ввода кода
                $this->arResult['PHONE'] = Helpers::isMobilPhone11($user['PERSONAL_PHONE']);
                $this->initAuthUser($user);
                $this->IncludeComponentTemplate('login_form_by_code');
                $page = ob_get_contents();
            }else { //Форма регистрации
                $this->arResult['EMAIL'] = $email;
                $this->IncludeComponentTemplate('registration_form_by_code');
                $page = ob_get_contents();
            }
            $this->getJsonPage($page);
            
        }elseif ($this->action === 11) { // Вернёт форму для входа по паролю
            ob_start();
            $this->IncludeComponentTemplate('form_login_password');
            $page = ob_get_contents();
            $this->getJsonPage($page);
        }elseif ($this->action === 12) { // Данные с формы входа (логин и пароль)
            $phone = Helpers::isMobilPhone11($this->request['phone-login']);
            $password = $this->request['password'];
            if (!$phone) {
                $this->error_field['phone-login'] = true;
                throw new \Exception('Укажите номер телефона', 200);
            }
            if (!$password) {
                $this->error_field['password'] = true;
                throw new \Exception('Укажите пароль', 200);
            }
            $user = $this->getUser($phone);
            
            if($user && $this->checkPassword($user['PASSWORD'],$password)) {
                if($this->userAutorize($user['ID'])) {
                    $this->getJsonPage('', 'Вы авторизованы. Сейчас страница будете перезагружена', true, [
                        'action' => 'reload',
                    ]);
                }
            }
            throw new \Exception('Не верный логин или пароль', 200);
        }else {
            throw new \Exception('Ошибка параметра авторизации', 200);
        }
    }
    
    /**
     * Для возврата результата в виде JSON
     * @param type $html
     * @param type $message
     * @param type $result
     * @param type $params
     */
    protected function getJsonPage($html, $message = '', $result = 'success', $params = []) {
        
        ob_end_clean();
        
        header('Content-type: application/json;');
        
        echo Json::encode([
            'modal_id' => $this->arParams['MODAL_ID'],
            'success' => $result,
            'message' => $html,
            'text' => $message,
            'params' => $params,
        ]);
        
        CMain::FinalActions();
        
        die();
    }
    
    /**
     * Проверка пароля по битрикс
     *
     * @param string $profile_pswd пароль из профиля пользователя
     * @param string $form_pswd    пароль из формы
     *
     * @return bool
     */
    protected function checkPassword($profile_pswd, $form_pswd):bool {
        
        $db_password = '';
        
        if (strlen($profile_pswd) > 32) {
            
            $salt = substr($profile_pswd, 0, strlen($profile_pswd) - 32);
            
            $db_password = substr($profile_pswd, -32);
        } else {
            
            $salt = "";
            
            $db_password = $profile_pswd;
        }
        
        $user_password = md5($salt . $form_pswd);
        
        if ($user_password === $db_password) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Отправка кода СМС для входа на сайт
     * @param $phone
     * @param $code
     *
     * @return bool
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectException
     */
    protected function sendVerificationCodeViaSMS($phone,$code)
    {
        
        $type = 'loginauth';
        
        if (!CheckSmsLimit::isAdd($phone, $type, 120)) {//Запрет на восстановление паролья по лимиту времени
            return false; // не отправляем, так как действует старый код
        }
        
        
        if (class_exists('\CMlifeSmsServices')) {
            $obSmsServ = new \CMlifeSmsServices();
            
            $arSend = $obSmsServ->sendSms($phone, "Код подтверждения: " . $code);
            
            ChecksmsTable::add(
                [
                    "PHONE"   => $phone,
                    "CODE" => $code,
                    'TYPE'    => $type,
                    'MESSAGE' => 'Код для входа или регистрации ' . __FILE__
                ]
            );
            
            return true;
        }
        
        return false;
    }
    
    
    
    /**
     * Проверки всех входных параметров
     * Устанавливает все необходимые значения для компонента
     * @throws \Exception выбросит искелючение, если чего то не хватило
     */
    protected function setParams() {
        
        $arParams = [];
        
        $context = \Bitrix\Main\Application::getInstance()->getContext();
        
        $request = $context->getRequest();
        
        if ($this->autorize_disable) {
            throw new \Exception('Авторизация временно не доступна', 200);
        }
        
        $arParams['MODAL_ID'] = 'gorgoc-loginform';
        
        if (!empty($request)) {
            $this->request = $request;
        }
        
        if (!empty($this->request)) {
            
            $this->login = preg_replace("/[^0-9]/", '', $this->request['phone-login']);
            
            $this->phone = preg_replace("/[^0-9]/", '', $this->request['phone-login']);
            
            $this->pswd = $this->request['password'];
            
            $this->referer = $this->request['referer'];
            
            $this->ssid = $this->request['ssid'];
            
            $this->user_type = intval($this->request['user_type']);
            
            $this->action = intval($this->request['action']);
        }
        
        $this->arParams = array_merge($this->arParams, $arParams);
    }
    
    
    /**
     * Вернёт пользователя по логину(номер телефона)
     * Результирующий массив содержит вложенные массивы с найденным пользователем в группах, ключи массивов - ИД группы
     *
     * @param      $phone
     * @param null $email
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function getUser($phone = null,$email = null, $userId = null)
    {
        
        $filter = [
            '=LOGIN'         => $this->phone,
            'ACTIVE'         => 'Y',
            'GROUP.GROUP_ID' => $this->group_allow,
        ];
        
        if($email) {
            $filter = [
                '=EMAIL' => $email,
                'ACTIVE'         => 'Y',
                'GROUP.GROUP_ID' => $this->group_allow,
            ];
        }
        if($phone) {
            $filter = [
                '=LOGIN' => $phone,
                'ACTIVE'         => 'Y',
                'GROUP.GROUP_ID' => $this->group_allow,
            ];
        }
        if($userId) {
            $filter = [
                '=ID' => $userId,
                'ACTIVE'         => 'Y',
                'GROUP.GROUP_ID' => $this->group_allow,
            ];
        }
        
        $rsUser = \Bitrix\Main\UserTable::getlist(
            [
                'filter'  => $filter,
                'select'  => [
                    'ID',
                    'PERSONAL_PHONE',
                    'PASSWORD',
                    'GROUP_ID' => 'GROUP.GROUP_ID',
                    'LOGIN',
                ],
                'limit'   => 5,
                'runtime' => [
                    new \Bitrix\Main\Entity\ReferenceField(
                        'GROUP', '\Bitrix\Main\UserGroupTable',
                        ['=this.ID' => 'ref.USER_ID']
                    ),
                ],
                //'group'=>['LOGIN','GROUP_ID']
            ]
        )->fetchAll();
        
        $arResult = [];
        
        if(empty($rsUser)) {
            return [];
        }
        
        $rsUser = [reset($rsUser)];//1-н телефон = 1 тип учётный профиль
        
        
        foreach ($rsUser as $arValue) {
            foreach ($this->ex_user_group as $gpId) { //Проверка на запрещённую группу
                if(in_array($gpId,\CUser::GetUserGroup($arValue['ID']))){
                    break 2;
                }
            }
            $arResult[$arValue['GROUP_ID']] = $arValue;
        }
        
        if(empty($arResult)) {
            return [];
        }
        
        return reset($arResult);
    }
    
    
    
    protected function getHashPswd($hash_base) {
        return md5($this->salt . '' . $hash_base) . '&' . md5(date('dmY'));
    }
    
    /**
     * Запись данных в таблицу действий модуля
     *
     * @param int|null $code
     * @param array    $form
     */
    protected function saveTheFormAction(int $userId, int $code=null, string $phone = null,string $type = null, array $form = []):void
    {
        $dbQuery = FormsTable::add(
            [
                'ACTIVE' => FormsTable::ACTIVE_ON,
                'CODE' => $code,
                'USER_ID' =>$userId,
                'SSID' => $this->ssid,
                'PHONE' => $phone,
                'TYPE' => $type,
                'FORM' => Json::encode($form)
            
            ]
        );
        if(!$dbQuery->isSuccess()) {
            throw new \Exception('Что то пошло не так, попробуйте позднее');
        }
    }
    
    /**
     * @param $codes
     * @param $sessid
     * Вернёт ИД записи из таблицы проверки или false если код не валидный
     * @return false|int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function isValidCode($codes, $sessid=false, $phone=false)
    {
        
        if(empty($codes) || !is_array($codes)){
            return false;
        }

        if(empty($sessid) && empty($phone)){
            return false;
        }
        
        $code = (int)implode('',$codes);
        
        if($code<1000){
            return false;
        }
        
        $filter = [
            '=ACTIVE' => FormsTable::ACTIVE_ON,
            '=CODE' => $code,
            '=SSID' => $sessid
        ];
        
       
        if($phone) {
            unset($filter['=SSID']);
            $filter['=PHONE'] = $phone;
        }
        
        $check = FormsTable::getList([
            'select' => ['*'],
            'filter' => $filter,
            'order' => [
                'ID'=>'DESC'
            ],
            'limit' => 1
        ])->fetch();
        if(!empty($check['ID'])) {
            FormsTable::update($check['ID'],['ACTIVE'=>FormsTable::ACTIVE_OFF]);
            return (int)$check['ID'];
        }
        return false;
    }
    
    
    protected function getCode()
    {
        return mt_rand(1000,9999);
    }
    
    /**
     * Авторизация через таблицу проверки кодов
     *
     * @param $idCheck ИД из таблицы кодов
     *
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function userAuthorization($idCheck):bool
    {
        global $USER;
        
        $userId = FormsTable::getList([
                'filter'=>['=ID' => $idCheck],
                'select' =>['USER_ID'],
                'limit' => 1
            ])->fetch()['USER_ID']??null;
        
        return $this->userAutorize($userId);
        
    }
    
    protected function userAutorize($userId) {
        global $USER;
        
        if(!$userId) {
            return false;
        }
        if (!$USER->Authorize($userId, true)) {
            return false;
        } else {
            $USER->CheckAuthActions();
            return true;
        }
        return false;
        
    }
    
    /**
     * Обновление пароля
     * @param $idCheck
     *
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function updateUserPassword($idCheck) {
        $dbQuery = FormsTable::getList([
            'filter'=>['=ID' => $idCheck],
            'select' =>['USER_ID','CODE'],
            'limit' => 1
        ]);
        
        if($tmp = $dbQuery->fetch()) {
            $user = new \CUser();
            $fields = [
                "PASSWORD"         => $tmp['CODE'],
                "CONFIRM_PASSWORD" => $tmp['CODE'],
                "TYPE_UPDATE"=>"SMS"
            
            ];
            $result = $user->Update($tmp["USER_ID"], $fields);
            if (empty($user->LAST_ERROR)) {
                return true;
            }
        }
        
        return false;
        
    }
    
    /**
     * Отправка кода оп смс, запись кода в таблицу верификации
     * @param $user
     *
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectException
     */
    protected function initAuthUser($user):void
    {
        $verifCode = $this->getCode();
        if($this->sendVerificationCodeViaSMS($user['PERSONAL_PHONE'],$verifCode)) {
            $this->saveTheFormAction(
                $user['ID'], $verifCode, $user['PERSONAL_PHONE'],
                FormsTable::TYPE_AUTHORIZATION,
                $this->request->toArray()
            );
        }
    }
    
    /**
     * Регистрация пользователя
     *
     * @param $phone
     * @param $email
     * @param $name
     *
     * @return false|array Массив данных по пользователю
     */
    protected function userRegistration($phone, $email, $name)
    {
        
        if(!empty($email) && $userProfile = $this->getUser(null,$email,null)){ // с таким email пользователь уже есть
            //Скорее всего это старая учётка и нужно её деактивировать
            if(!Helpers::isMobilPhone11($userProfile['PERSONAL_PHONE'])) {//есть емаил но нет телефона
                $user = new \CUser;
                $user->Update($userProfile['ID'],['ACTIVE'=>'N',"TYPE_UPDATE" => "SMS"]);//Деактивация старого профиля
                unset($user);
            }
            // $email = null;
        }
        
        $password = rand(100000, 9999999);
        $user = new \CUser;
        
        if (empty($email)) {
            $email = time().rand(1000,8888) . '@noemail.gav';
        }
        
        $arFields = [
            "NAME"             => $name??'',
            "EMAIL"            => $email,
            "LOGIN"            => $phone,
            "LID"              => 's1',
            "ADMIN_NOTES"      => "Регистрация - форма входа",
            "PERSONAL_PHONE"   => $phone,
            "ACTIVE"           => "Y",
            "GROUP_ID"         => [3,4,18],
            "PASSWORD"         => $password,
            "CONFIRM_PASSWORD" => $password,
            "TYPE_UPDATE" => "SMS"
        ];
        
        $createId = $user->Add($arFields);
        
        // print_r($createId);
        // var_dump($user->LAST_ERROR);die();
        
        if ($createId) {
            return $this->getUser(null,null,$createId);
        }
        return false;
    }
    
}
