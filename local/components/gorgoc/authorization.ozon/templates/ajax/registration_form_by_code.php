<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<?php
/**
 * Форма Регистрации после ввода номера телефона или email-а
 * Если пользователя нет на сайте
 */
?>

<div class="col-xs-12">
    <div class="modal-input-wrapper input-login-form">
        <label class="registration-tip-an-input-mask">* Номер телефона: +7(xxx)xxx-xx-xx</label>
        <input value="<?php echo $component->arResult['PHONE']??'' ?>" id="login-form-phone-input" class="form-field-input login_input" autocomplete="off" name="registration-phone" placeholder="+7(911)111-11-11" type="tel" placeholder="">
        <label class="registration-tip-an-input-mask">Имя</label>
        <input class="form-field-input login_input" autocomplete="off" name="registration-name" type="text">
        <label class="registration-tip-an-input-mask">Email</label>
        <input value="<?php echo $component->arResult['EMAIL']??'' ?>" class="form-field-input login_input" autocomplete="off" name="registration-email" type="email">
        <div class="agreement-box">
            <input name="registration-agreement" value="согласен" required="required" checked="checked" type="checkbox" class="login_input">
            <span class="text">
                Согласен с условиями обработки <a class="agreement" href="/consent-for-personal-data-processing/" target="_blank">персональных данных</a>
            </span>
            </input>
        </div>
    </div>
    <div class="flex-container-w100 flex-side">
        <p class="message"></p>
    </div>
    <div class="modal-input-wrapper flex-container-w100 flex-center">
        <button
                type="button"
                class="btn btn-main sent-form"
        >Зарегистрироваться</button>
    </div>
    <input name="ssid" class="login_input" type="hidden" value="<?php echo bitrix_sessid(); ?>">
    <input name="referer" class="login_input" type="hidden" value="<?php echo md5($_SERVER['REQUEST_URI']); ?>">
    <input name="action" class="login_input" type="hidden" value="7">
    <input name="request" class="login_input" type="hidden" value="ajax">

</div>

<script>
  $(document).ready(function(){
    $('#gorgoc-loginform .modal-title').text('Регистрация');
    $('#gorgoc-loginform .log-in-by-password').hide();
  })
</script>