<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<?php
/**
 * Форма запроса кода по email
 */
?>

<div class="col-xs-12">
    <div class="modal-input-wrapper input-login-form">
        <input id="login-form-phone-email" autocomplete="off" required="required" name="email-login" type="email" class="form-field-input login_input" id="login_email" placeholder="">
        <label class="tip-an-input-mask">Email адрес: xxxx@xxxx.xx</label>
    </div>
    <div class="flex-container-w100 flex-side">
        <p class="message"></p>
    </div>
    <div class="modal-input-wrapper flex-container-w100 flex-center">
        <button
                type="button"
                class="btn btn-main sent-form"
        >Получить код</button>
    </div>
    <input name="ssid" class="login_input" type="hidden" value="<?php echo bitrix_sessid(); ?>">
    <input name="referer" class="login_input" type="hidden" value="<?php echo md5($_SERVER['REQUEST_URI']); ?>">
    <input name="action" class="login_input" type="hidden" value="10">
    <input name="request" class="login_input" type="hidden" value="ajax">

</div>
<script>
    $(document).ready(function(){
      $('#gorgoc-loginform .modal-title').text('Войдите по почте');
      $('#gorgoc-loginform .log-in-by-email').hide();
      $('#gorgoc-loginform .log-in-by-password').hide();
    })
</script>