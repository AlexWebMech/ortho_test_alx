<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<?php
/**
 * Форма запроса кода по номеру телефона
 */
?>

<div class="col-xs-12">
    <div class="modal-input-wrapper  input-login-form">
        <input id="login-form-phone-input" autocomplete="off" name="phone-login" placeholder="+7(911)111-11-11" type="tel" class="form-field-input login_input" placeholder="">
        <label class="tip-an-input-mask">Номер телефона: +7(xxx)xxx-xx-xx</label>
        <input name="password" placeholder="xxxx" type="password" class="form-field-input login_input" placeholder="">
        <label class="tip-an-input-mask">Пароль</label>
    </div>
    <div class="flex-container-w100 flex-side">
        <p class="message"></p>
    </div>
    <div class="modal-input-wrapper flex-container-w100 flex-center">
        <button
                type="button"
                class="btn btn-main sent-form"
        >Войти</button>
    </div>
    <input name="ssid" class="login_input" type="hidden" value="<?php echo bitrix_sessid(); ?>">
    <input name="referer" class="login_input" type="hidden" value="<?php echo md5($_SERVER['REQUEST_URI']); ?>">
    <input name="action" class="login_input" type="hidden" value="12">
    <input name="request" class="login_input" type="hidden" value="ajax">

</div>
<script>
  $(document).ready(function(){
    $('#gorgoc-loginform .modal-title').text('Войдите при помощи пароля');
    $('#gorgoc-loginform .log-in-by-email').hide();
    $('#gorgoc-loginform .log-in-by-password').hide();
  })
  
</script>