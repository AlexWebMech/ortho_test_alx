<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<?php
/**
 * Форма запроса кода по номеру телефона
 */
?>

<div class="col-xs-12">
    <div class="modal-input-wrapper  input-login-form">
        <input id="login-form-phone-input" autocomplete="off" required="required" name="phone-login" placeholder="+7(911)111-11-11" type="tel" class="form-field-input login_input" placeholder="">
        <label class="tip-an-input-mask">Номер телефона: +7(xxx)xxx-xx-xx</label>
    </div>
    <div class="flex-container-w100 flex-side">
        <p class="message"></p>
    </div>
    <div class="modal-input-wrapper flex-container-w100 flex-center">
        <button
                type="button"
                class="btn btn-main sent-form"
        >Получить код</button>
    </div>
    <input name="ssid" class="login_input" type="hidden" value="<?php echo bitrix_sessid(); ?>">
    <input name="referer" class="login_input" type="hidden" value="<?php echo md5($_SERVER['REQUEST_URI']); ?>">
    <input name="action" class="login_input" type="hidden" value="6">
    <input name="request" class="login_input" type="hidden" value="ajax">

</div>