<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<?php
/**
 * Форма подтверждения кода полученного на телефон
 */
?>

<div class="col-xs-12">
    <div class="row justify-content-start modal-input-wrapper input-login-form">
        <input class="col-2 offset-2 registration-code login_input" autocomplete="off" required="required" data-step="1" name="registration-code1" type="text" maxlength="1">
        <input  class="col-2 registration-code login_input" autocomplete="off" required="required" data-step="2" name="registration-code2" type="text"  maxlength="1">
        <input  class="col-2 registration-code login_input" autocomplete="off" required="required" data-step="3" name="registration-code3" type="text"  maxlength="1">
        <input  class="col-2 registration-code login_input" autocomplete="off" required="required" data-step="4" name="registration-code4" type="text"  maxlength="1">
    </div>
    <div class="flex-container-w100 flex-side">
        <p class="message">Смс сообщение с кодом отправлено вам на телефон</p>
    </div>
    <input name="ssid" class="login_input" type="hidden" value="<?php echo bitrix_sessid(); ?>">
    <input name="referer" class="login_input" type="hidden" value="<?php echo md5($_SERVER['REQUEST_URI']); ?>">
    <input name="phone-login" class="login_input" type="hidden" value="<?php echo $component->arResult['PHONE']??'' ?>"  >
    <input name="action" class="login_input" type="hidden" value="8">
    <input name="request" class="login_input" type="hidden" value="ajax">

</div>
<script>
  $(document).ready(function(){
    $('#gorgoc-loginform .modal-title').text('Введите код');
    $('#gorgoc-loginform .log-in-by-password').hide();
  })
</script>