<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<?php
/**
 * Форма входа первичная
 */
?>
    <div id = "gorgoc-loginform" class = "modal fade form-modal" tabindex = "-1" role = "dialog" aria-hidden="true">
        <script>
            <?php
            echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . $templateFolder . '/imask.js');
            echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . $templateFolder . '/script.js');
            ?>

            $(document).ready(function () {

              var form_login = FormLoginAction({MODAL_ID: '<?php echo $component->arParams['MODAL_ID'] ?>'});

              form_login.init();

            });

        </script>
        <style>
            <?php
            echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . $templateFolder . '/style.css');
            ?>
        </style>
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <?php echo bitrix_sessid_post(); ?>
                <div class="modal-header">
                    <div class="modal-title h5">Войдите или зарегистрируйтесь, чтобы продолжить</div>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times</span></button>
                </div>
                <div class="modal-body">
                    <div class="form_login_content">
                        <?php
                        include_once 'form_login.php';
                       // include_once 'form_login_email.php';
                       // include_once 'registration_form_by_code.php';
                        // include_once 'login_form_by_code.php';
                        ?>
                    </div>
                 
                    <div class="row modal-foter-link modal-input-wrapper flex-container-w100 flex-center input-login-form">
                        <a class="col-12 alt-login log-in-by-email" href="#" data-action="9">Войти по email</a>
                        <a class="col-12 alt-login log-in-by-password" href="#" data-action="11">Войти при помощи пароля</a>
                        <a style="display: none;" class="log-in-by-email" href="#" data-action="-1">Не приходит смс </a>
                        <a style="display: none;" class="go-back-to-the-main-page" href="#" data-action="2">Назад</a>
                    </div>
                    
                </div>
            </div>
        </div>

    </div>

<?php
return;
