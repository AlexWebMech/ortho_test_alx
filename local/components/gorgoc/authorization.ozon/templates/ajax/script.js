var globalFormLoginAction = 0;

$('#gorgoc-loginform').on('shown.bs.modal', function() {
    initFocusElement();
    initSecurityCode();
    initInputFields();

    // Вход по email
    $('.log-in-by-email').on('click',function (){
        // Отправка
        var data = {};
        data.action = 9;
        data.request = "ajax";
        $.ajax({
            url: '/ajax/get/loginform/',
            type: 'POST',
            dataType: 'json',
            data: data
        }).done(function (data) {
            globalFormLoginAction = parseInt(data.action,10);
            if (data.success) {
                if (data.message) {
                    $('#gorgoc-loginform .form_login_content').html(data.message);
                    $('#gorgoc-loginform .modal-footer').remove();
                }
            }
        }).done(function(){
            initFocusElement();
            initSecurityCode();
        });
    });
    // Вход по паролю
    $('.log-in-by-password').on('click',function (){
        // Отправка
        var data = {};
        data.action = 11;
        data.request = "ajax";
        $.ajax({
            url: '/ajax/get/loginform/',
            type: 'POST',
            dataType: 'json',
            data: data
        }).done(function (data) {
            globalFormLoginAction = parseInt(data.action,10);
            if (data.success) {
                if (data.message) {
                    $('#gorgoc-loginform .form_login_content').html(data.message);
                    $('#gorgoc-loginform .modal-footer').remove();
                }
            }
        }).done(function(){
            initFocusElement();
            initSecurityCode();
        });
    });

})

function initFocusElement() {
    $('#login-form-phone-input').focus();
    $('#login-form-phone-email').focus();
    $('[name="registration-code1"]').focus();//Форма ввод кода


    //if(globalFormLoginAction===0) {
    initMaskPhone();
    //}

    if(globalFormLoginAction===10 || globalFormLoginAction === 6) {
        $('#gorgoc-loginform .log-in-by-email').hide();
        $('#gorgoc-loginform .go-back-to-the-main-page').hide();
    }
    if(globalFormLoginAction) {
        $('#gorgoc-loginform .log-in-by-password').hide();
    }


}

function initInputFields() {
    $('#gorgoc-loginform').on('click keyup','input',function (){
        if($(this).hasClass('error-input')) {
            $(this).removeClass('error-input');
        }
    });
    //error-input
}

function initMaskPhone() {
    if($('#gorgoc-loginform input').is('#login-form-phone-input')) {
        var phoneMask = IMask(
          document.getElementById('login-form-phone-input'), {
              mask: '+{7}(000)000-00-00'
          });
    }
}

/** Страница ввода кода **/
function initSecurityCode() {
    var sendNum = 0;
    var formCodeProtection = {
        1:'',
        2:'',
        3:'',
        4:'',
    };
    var coptyformCodeProtection = Object.assign({},formCodeProtection);

    $('#gorgoc-loginform .registration-code').click(function(e) {
        $(this).val('');
    });
    /** Отслеживает код подтверждения **/
    $('#gorgoc-loginform .registration-code').keyup(function(e) {
        var registration_code = $(this).val();
        var step =  $(this).attr('data-step');
        if(typeof step !=='undefined'){
            step = parseInt(step,10);
        }else {
            step = 1;
        }
        formCodeProtection[step] = registration_code;

        if((step==1 || step==2 || step==3) && sendNum<1){
            $('[name="registration-code' + (step+1) +'"]').attr('disabled',false);
            $('[name="registration-code' + (step+1) +'"]').focus();

        }

        if(!checkingTheSecurityCode(formCodeProtection)) {
            return;
        }

        var data = {};
        $('#gorgoc-loginform input.login_input').each(function (index, element) {
            var val = '';
            val = $(element).val();
            var ind = $(element).attr('name');

            if (typeof ($(element).attr('required')) != 'undefined') {
                if (val.length < 1) {
                    $(element).parent('.form-group').addClass('has-error');
                    form_bad = true;
                    return false;
                } else {
                    $(element).parent('.form-group').removeClass('has-error');
                }
            }
            data[ind] = val;
        });

        data['codes'] = formCodeProtection;
        globalFormLoginAction = parseInt(data.action,10);
        // Отправка
        $.ajax({
            url: '/ajax/get/loginform/',
            type: 'POST',
            dataType: 'json',
            data: data
        }).done(function (data) {

            $('#gorgoc-loginform .message').html(data.text);

            if (data.message) {
                $('#gorgoc-loginform').find('.message').html(data.message);
            }

            setTimeout(function () {
                $('#gorgoc-loginform .message').text('');
            }, 2000);


        }).done(function (data) {
            if (typeof data !== 'undefined' && data.params) {
                if (data.params.action && data.params.action === 'reload') {
                    window.location.reload();
                }
            }
        }).complete(function(){
            // formCodeProtection = coptyformCodeProtection;
            sendNum++;
        });
    });
}

function checkingTheSecurityCode(formCodeProtection) {
    if(formCodeProtection[1].length<1){
        return false;
    }
    if(formCodeProtection[2].length<1){
        return false;
    }
    if(formCodeProtection[3].length<1){
        return false;
    }
    if(formCodeProtection[4].length<1){
        return false;
    }
    return true;
}


FormLoginAction = (function (params) {

    var modail_id = '#gorgoc-loginform';
    var url = '/ajax/get/loginform/';
    var self = this;

    var tool = {

        bindEvents: function (events) {
            for (var i = 0, l = events.length; i < l; i++) {
                if (!events[i].element) {
                    $(events[i].target).on(events[i].event, events[i].handler)
                } else {
                    $(events[i].element).on(events[i].event, events[i].target, events[i].handler)
                }
            }
        },
        unbindEvents: function (events) {
            for (var i = 0, l = events.length; i < l; i++) {
                $(events[i].target).off(events[i].event);
            }
        },
    }

    var form = {
        logout: function () {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'logout': '1'
                }
            }).done(function (data) {

                $(modail_id + ' .message').html(data.text);

                if (data.message) {
                    $(modail_id).find('.message').html(data.message);
                }
                if (data.back_url) {
                    setTimeout(function () {
                        window.location.href = data.back_url;
                    }, 1000);
                }
            });
        },
        sent: function () {
            var data = {};
            var form_bad = false;
            $(modail_id + ' input.login_input').each(function (index, element) {
                var val = '';
                if($(element).attr('name')==='registration-agreement'){
                    if($(element).prop("checked")) {
                        val = $(element).val();
                    }
                }else {
                    val = $(element).val();
                }
                var ind = $(element).attr('name');

                if (typeof ($(element).attr('required')) != 'undefined') {
                    if (val.length < 3) {
                        $(element).parent('.form-group').addClass('has-error');
                        form_bad = true;
                        return false;
                    } else {
                        $(element).parent('.form-group').removeClass('has-error');
                    }
                }
                data[ind] = val;
            });

            if (form_bad) {
                return false;
            }
            globalFormLoginAction = parseInt(data.action,10);
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data
            }).done(function (data) {

                $(modail_id + ' .message').html(data.text);

                if (data.success) {
                    if (data.message) {
                        $(modail_id + ' .form_login_content').html(data.message);
                        $(modail_id + ' .modal-footer').remove();
                    }
                } else {
                    $(modail_id + ' .message').html(data.text);

                    if(typeof data.success !=='undefined' && data.success===false) {
                        if(typeof data.params === 'object') {
                            for(var item in data.params) {
                                var params = data.params;
                                if(params[item]) {
                                    $('[name="' + item + '"]').addClass('error-input');
                                }
                            }
                        }
                    }

                    setTimeout(function () {
                        $(modail_id + ' .message').html('');
                    }, 3000);
                }
            }).done(function (data) {
                if (typeof data !== 'undefined' && data.params) {
                    if (data.params.back_url && (data.params.back_url).length > 4) {
                        window.location.href = data.params.back_url;
                    } else if (data.params.action && data.params.action === 'reload') {
                        window.location.reload();
                    }
                }
            }).done(function(){
                initFocusElement();
                initSecurityCode();
            });
        },
        sentUserType: function () {
            var data = {};
            var self = this;
            $(modail_id + ' input.login_input').each(function (index, element) {
                var ind = $(element).attr('name');
                var val = $(element).val();

                data[ind] = val;
            });

            data['user_type'] = $(self).attr('data-user_type');

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data
            }).done(function (data) {
                if (data.success) {
                    $(modail_id + ' .message').html(data.text);
                } else {
                    $(modail_id + ' .message').html(data.text);
                    setTimeout(function () {
                        $(modail_id + ' .message').html('');
                    }, 3000);
                }
            }).done(function (data) {
                //                if (typeof data !== 'undefined' && data.params && data.params.action) {
                //                    if (data.params.action === 'reload') {
                //                        window.location.reload();
                //                    }
                //                }
                if (typeof data !== 'undefined' && data.params) {
                    if (data.params.back_url && (data.params.back_url).length > 4) {
                        window.location.href = data.params.back_url;
                    } else if (data.params.action && data.params.action === 'reload') {
                        window.location.reload();
                    }
                }
            });
        },
        pswdRecovery: function () {
            var data = {
                action: 5,
                request: 'ajax',
            };
            globalFormLoginAction = parseInt(data.action,10);
            var phone = $(modail_id + ' input[name="phone"]').val();

            if (typeof phone === 'undefined') {
                phone = 0;
            }

            data.phone = phone;
            data.ssid = BX.bitrix_sessid();


            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data
            }).done(function (data) {
                if (data.success) {
                    $(modail_id + ' .message').html(data.text);
                } else {
                    $(modail_id + ' .message').html(data.text);

                }
                setTimeout(function () {
                    $(modail_id + ' .message').html('');
                }, 3000);
            });
        }
    }


    bindEvents = function () {
        var event = [
            {
                element: modail_id,
                target: '.sent-form-logout',
                event: 'click',
                handler: form.logout
            },
            {
                element: modail_id,
                target: '.sent-form',
                event: 'click',
                handler: form.sent
            },
            {
                element: modail_id,
                target: '.sent_user_type',
                event: 'click',
                handler: form.sentUserType
            },
            {
                element: modail_id,
                target: '.pswd_sent_sms',
                event: 'click',
                handler: form.pswdRecovery
            }

        ];
        tool.bindEvents(event);
    }

    self.init = function () {
        self.bindEvents();
    }

    return {
        init: self.init
    }
});
$('#gorgoc-loginform').on('shown.bs.modal', function() {
    var form_app_init = FormLoginAction();
    form_app_init.init();
});