<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<?php
/**
 * Форма входа первичная
 */
?>

<div class= "row">
    <div id = "<?php echo $component->arParams['MODAL_ID']; ?>" class="modal-dialog modal-sm left" role="document">
        <div class="col-xs-12 modal-content">
            <div class="row">
                <?php echo bitrix_sessid_post(); ?>
                <div class="col-xs-12 modal-header">
                    <h4 class="title">Авторизация</h4>
                </div>
                <div class="col-xs-12 modal-body">
                    <div class="row">
                        <div class="form_login_content">
                            <?php include_once 'form_login.php'; ?>
                        </div>

                    </div>
                </div>
                <div class="col-xs-12 modal-footer nohr">
                    <button
                        type="button"
                        class="btn btn-main sent-form"
                        >Войти</button>
                </div>
            </div>
        </div>

        <style>
            #<?php echo $component->arParams['MODAL_ID']; ?> .nav {
                margin-bottom:18px;   
            }
            #<?php echo $component->arParams['MODAL_ID']; ?> .pswd_sent_sms {
                line-height: 26px;
                font-size: 12px;
                text-align: right;
                color: #ff5349;
                text-decoration: none;
                border-bottom: 1px dashed #ff5349;
            }
            #<?php echo $component->arParams['MODAL_ID']; ?> .nav-tabs > li.active > a, #<?php echo $component->arParams['MODAL_ID']; ?>  .nav-tabs > li.active > a:focus, #<?php echo $component->arParams['MODAL_ID']; ?>  .nav-tabs > li.active > a:hover {
                color: #fff;
                background-color: #3c90e6;
            }
            .modal-dialog.left {
                margin:13px 13px;
            }


        </style>

    </div>
    <script>
        BX.ready(function () {

            var form_login_static = FormLoginAction({
                MODAL_ID: '<?php echo $component->arParams['MODAL_ID'] ?>',
                URL: '<?php $APPLICATION->GetCurPage(); ?>'
            });

            form_login_static.init();

        });
    </script>
</div>
<?php

