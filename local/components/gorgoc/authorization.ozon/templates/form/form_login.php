<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<?php
/**
 * Форма с кнопками
 */
?>

<div class="col-xs-12">
    <div class="form-group">
        <label for="login_tel">Телефон</label>
        <input required="required" name="phone" placeholder="+7(921)777-88-88" type="tel" class="form-control login_input" id="login_tel" placeholder="">
    </div>
    <div class="form-group">
        <label for="login_pswd">Пароль</label>
        <input required="required" name="password" type="password" class="form-control login_input" id="login_pswd" placeholder="">
    </div>
    <div class="col-xs-12">
        <p class="message"></p>
    </div>
    <div class="col-xs-12">
        <a class="pswd_sent_sms pull-right" href="#" onclick="return false;">Выслать пароль по СМС</a>
    </div>
    <input name="ssid" class="login_input" type="hidden" value="<?php echo bitrix_sessid(); ?>">
    <input name="referer" class="login_input" type="hidden" value="<?php echo md5($_SERVER['REQUEST_URI']); ?>">
    <input name="action" class="login_input" type="hidden" value="2">
    <input name="request" class="login_input" type="hidden" value="ajax">
</div>