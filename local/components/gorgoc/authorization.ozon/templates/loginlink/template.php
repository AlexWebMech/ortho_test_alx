<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<span class="login-block login-icon">
<a id="authOpen2" onclick="GorgocLoginForm.getFormTxt('/ajax/get/loginform/', {'back_url': '<?php echo $_SERVER['REQUEST_URI']; ?>', 'action': '1'}); return false;" class="customodal-btn">Войти</a>
</span>
<script>
  var GorgocLoginForm = {
    getFormTxt: function (url, objparam) {

      var data = {};

      if (typeof objparam == 'undefined') {
        objparam = {};
      }

      for (var key in objparam) {
        data[key] = objparam[key];
      }
      if (typeof objparam.request == 'undefined') {
        data.request = 'ajax';
      }

      $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: data
      }).done(function (data) {
        if (typeof data !== 'undefined' && data.success && data.modal_id) {
          $('body').find('#' + data.modal_id).remove();
          $('body').append(data.message);

        } else {
          console.log('error');
        }

      }).done(function (data) {
        if (typeof data !== 'undefined' && data.success && data.modal_id) {
          $('#' + data.modal_id).modal('show');
          $('#' + data.modal_id).on('hidden.bs.modal', function (e) {
            $('#' + data.modal_id).remove();
          });
        }
      }).done(function (data) {
        if (typeof data !== 'undefined' && data.params && data.params.action) {
          if (data.params.action === 'reload') {
            window.location.reload();
          }
        }
      });

    }
  }
</script>