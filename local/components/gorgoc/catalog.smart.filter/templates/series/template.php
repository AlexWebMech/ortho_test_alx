<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$templateData = array(
    'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
    'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
);//pre($arResult);
?>

<div  class="smart-filter catalog-filter">
    <div class="sidebar-block bx-filter" id="accordion">
        <div class="toggle-parent sidebar-part">
            <a class="toggle filter__adaptive mb-4" href="#" role="button">
                <div class="filter__adaptive-text">
                    <span class="text-closed">ПОКАЗАТЬ ФИЛЬТРЫ&nbsp;</span>
                    <span class="text-opened">CВЕРНУТЬ ФИЛЬТРЫ&nbsp;</span>
                    <span class="icon-angle-right"></span>
                </div>
            </a>
            <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="filter toggle-item smartfilter sidebar">
                <?foreach($arResult["HIDDEN"] as $arItem):?>
                    <input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
                <?endforeach;
                //prices
                foreach($arResult["ITEMS"] as $key=>$arItem)
                {
                    $key = $arItem["ENCODED_ID"];
                    if(isset($arItem["PRICE"])):
                        if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
                            continue;
                        ?>
                        <div class="filter__block">
                            <a class="d-flex justify-content-between align-items-center" data-toggle="collapse"
                               data-parent="#accordion" href="#collapseFilter" aria-expanded="true">
                                <div class="filter__title">ЦЕНА</div>
                                <span class="icon icon-angle-right"></span>
                            </a>
                            <div class="filter-body collapse in show" id="collapseFilter">
                                <div class="bx-filter-block" data-role="bx_filter_block">
                                    <div class="d-flex flex-wrap justify-content-between align-items-center bx-filter-parameters-box-container">
                                        <div class="bx-filter-parameters-box-container-block bx-left" >
                                            <i class="bx-ft-sub">От</i>
                                            <div class="bx-filter-input-container">
                                                <input
                                                        class="min-price"
                                                        type="text"
                                                        name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
                                                        id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
                                                        value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
                                                        size="5"
                                                        placeholder="от"
                                                        onkeyup="smartFilter.keyup(this)"
                                                />
                                            </div>
                                        </div>

                                        <div class="bx-filter-parameters-box-container-block bx-right">
                                            <i class="bx-ft-sub">До</i>
                                            <div class="bx-filter-input-container">
                                                <input
                                                        class="max-price"
                                                        type="text"
                                                        name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
                                                        id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
                                                        value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
                                                        size="5"
                                                        placeholder="до"
                                                        onkeyup="smartFilter.keyup(this)"
                                                />
                                            </div>
                                        </div>
                                        <div class="w-100 bx-ui-slider-track-container">
                                            <div class="bx-ui-slider-track" id="drag_track_<?=$key?>">
                                                <?
                                                $price1 = $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] ?
                                                    $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"];
                                                $price2 = $arItem["VALUES"]["MIN"]["VALUE"] +
                                                    round(($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"])/4);
                                                $price3 = $arItem["VALUES"]["MIN"]["VALUE"] +
                                                    round(($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"])/2);
                                                $price4 = $arItem["VALUES"]["MIN"]["VALUE"] +
                                                    round((($arItem["VALUES"]["MAX"]["VALUE"] -
                                                                $arItem["VALUES"]["MIN"]["VALUE"])*3)/4);
                                                $price5 = $arItem["VALUES"]["MAX"]["VALUE"];
                                                ?>
                                                <div class="bx-ui-slider-part p1"><span><?=$price1?>&nbsp;руб.</span></div>
                                                <div class="bx-ui-slider-part p2"><span><?=$price2?></span></div>
                                                <div class="bx-ui-slider-part p3"><span><?=$price3?></span></div>
                                                <div class="bx-ui-slider-part p4"><span><?=$price4?></span></div>
                                                <div class="bx-ui-slider-part p5"><span><?=$price5?>&nbsp;руб.</span></div>

                                                <div class="bx-ui-slider-pricebar-vd" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div>
                                                <div class="bx-ui-slider-pricebar-vn" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div>
                                                <div class="bx-ui-slider-pricebar-v"  style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div>
                                                <div class="bx-ui-slider-range" id="drag_tracker_<?=$key?>"  style="left: 0%; right: 0%;">
                                                    <a class="bx-ui-slider-handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>"></a>
                                                    <a class="bx-ui-slider-handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?
                    $precision = 0;
                    if (Bitrix\Main\Loader::includeModule("currency"))
                    {
                        $res = CCurrencyLang::GetFormatDescription($arItem["VALUES"]["MIN"]["CURRENCY"]);
                        $precision = $res['DECIMALS'];
                    }
                    $arJsParams = array(
                        "leftSlider" => 'left_slider_'.$key,
                        "rightSlider" => 'right_slider_'.$key,
                        "tracker" => "drag_tracker_".$key,
                        "trackerWrap" => "drag_track_".$key,
                        "minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
                        "maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
                        "minPrice" => $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] ?
                            $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"],
                        "maxPrice" => $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] ?
                            $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
                        "curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
                        "curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
                        "fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
                        "fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
                        //						"precision" => $precision,
                        "colorUnavailableActive" => 'colorUnavailableActive_'.$key,
                        "colorAvailableActive" => 'colorAvailableActive_'.$key,
                        "colorAvailableInactive" => 'colorAvailableInactive_'.$key,
                    );
                    ?>
                        <script type="text/javascript">
                          BX.ready(function(){
                            window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
                          });
                        </script>
                    <?endif;
                }
                
                foreach($arResult["ITEMS"] as $key=>$arItem)
                {	//pre($arItem);
                    
                    if(
                        empty($arItem["VALUES"])
                        || isset($arItem["PRICE"])
                    )
                        continue;
                    
                    if (
                        $arItem["DISPLAY_TYPE"] == "A"
                        && (
                            $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
                        )
                    )
                        continue;
                    ?>
                    <div class="filter__block"<?if ($arItem["CODE"] == "STELKI_ORTHOBOOM"):?>style="visibility: hidden; height: 0px; margin: 0px;"<?endif;?>>
                        <a class="d-flex justify-content-between align-items-center" data-toggle="collapse"
                           data-parent="#accordion" href="#collapse<?=$arItem["ID"]?>"
                           aria-expanded="<?if($arItem["DISPLAY_EXPANDED"]):?>true<?else:?>false<?endif;?>">
                            <div class="filter__title"><?=$arItem["NAME"]?></div>
                            <span class="icon icon-angle-right"></span>
                        </a>
                        
                        <?if ($arItem["CODE"] == "OBUV_RAZMER" || $arItem["CODE"] == "DLINA_STELKI_SM") {?>
<!--                            <a class="filter-tooltip" href="#">Как выбрать размер?</a>-->
                            <div class="filter-body collapse in show" id="collapse<?=$arItem["ID"]?>">
                                <div class="tiled-grid d-flex flex-wrap">
                                    <?$i = 0;?>
                                    <?foreach($arItem["VALUES"] as $val => $ar):?>
                                        <?if ($ar["DISABLED"] != 1):?>
                                            <div class="tiled-checkbox show-element">
                                                <input
                                                        type="checkbox"
                                                        value="<? echo $ar["HTML_VALUE"] ?>"
                                                        name="<? echo $ar["CONTROL_NAME"] ?>"
                                                        id="<? echo $ar["CONTROL_ID"] ?>"
                                                    <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                                        onclick="smartFilter.click(this)"
                                                        style="display: none;"
                                                />
                                                <?$size = str_replace("р.", "", $ar["VALUE"]);?>
                                                <label class="tiled__label" for="<? echo $ar["CONTROL_ID"] ?>"><?=$size;?>
                                                </label>
                                            </div>
                                        <?endif;?>
                                    <?endforeach;?>
                                </div>
                            </div>
                        <? } else if($arItem['CODE']==='KOLLEKTSIYA'){?>
                            <div class="filter-body collapse in show" id="collapse<?=$arItem["ID"]?>">
                                    <?foreach($arItem["VALUES"] as $val => $ar):?>
                                        <?if ($ar["DISABLED"] != 1):?>
                                            <div class="item__check sidebar-element show-element ">
                                                <label class="check__label"><?=$ar["VALUE"];?>
                                                    <input
                                                            type="checkbox"
                                                            value="<? echo $ar["HTML_VALUE"] ?>"
                                                            name="<? echo $ar["CONTROL_NAME"] ?>"
                                                            id="<? echo $ar["CONTROL_ID"] ?>"
                                                        <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                                            onclick="smartFilter.click(this)"
                                                            style="display: none;"
                                                    />
                                                    <span class="icon icon-checked"></span>
                                                </label>

                                            </div>
                                        <?endif;?>
                                    <?endforeach;?>
                            </div>
                       
                        <?php }else { ?>
                            <div class="filter-body collapse in show" id="collapse<?=$arItem["ID"]?>">
                                <div class="elements-toShow">
                                    <?$count = count($arItem["VALUES"])?>
                                    <?foreach($arItem["VALUES"] as $val => $ar):?>
                                        <?if ($ar["DISABLED"] != 1):?>
                                            <div class="item__check sidebar-element show-element ">
                                                <label class="check__label"><?=$ar["VALUE"];?>
                                                    <input
                                                            type="checkbox"
                                                            value="<? echo $ar["HTML_VALUE"] ?>"
                                                            name="<? echo $ar["CONTROL_NAME"] ?>"
                                                            id="<? echo $ar["CONTROL_ID"] ?>"
                                                        <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                                            onclick="smartFilter.click(this)"
                                                            style="display: none;"
                                                    />
                                                    <span class="icon icon-checked"></span>
                                                </label>

                                            </div>
                                        <?endif;?>
                                    <?endforeach;?>
                                </div>
                                <?if ($count>5):?>
                                    <a class="filter__link" href="#">
									<span class="closed">показать еще&nbsp;<span class="filter-element_extra"></span>
									</span>
                                        <span class="opened">свернуть&nbsp;</span>
                                        <span class="icon icon-angle-right"></span>
                                    </a>
                                <?endif;?>
                            </div>
                        <? }?>
                    </div>
                    <?
                }
                ?>
                <div class="clb"></div>
                <div class="btn-grouph">
                    <button id="del_filter" name="del_filter" class="btn btn-upper  btn-path mb-2" type="submit"
                            value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>">сбросить</button>
                    <button class="btn btn-upper btn-fill mb-2" type="submit" id="set_filter" name="set_filter"
                            value="<?=GetMessage("CT_BCSF_SET_FILTER")?>">посмотреть</button>
                    <div class="bx-filter-popup-result <?if ($arParams["FILTER_VIEW_MODE"] == "VERTICAL") echo $arParams["POPUP_POSITION"]?>" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?>>
                        <?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
                        <span class="arrow"></span>
                        <br/>
                        <a href="<?echo $arResult["FILTER_URL"]?>" target=""><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<script>
  $('.elements-toShow, .tiled-grid').each(function() {
    if (!$(this).children().length) {
      $(this).parent().parent().hide();
    }
  });

  var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', 'vertical');
</script>

