<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Lib\Classes\Helper\LenalHelp;

if (isset($arParams["TEMPLATE_THEME"]) && !empty($arParams["TEMPLATE_THEME"])) {
    $arAvailableThemes = array();
    $dir = trim(preg_replace("'[\\\\/]+'", "/", dirname(__FILE__) . "/themes/"));
    if (is_dir($dir) && $directory = opendir($dir)) {
        while (($file = readdir($directory)) !== false) {
            if ($file != "." && $file != ".." && is_dir($dir . $file)) {
                $arAvailableThemes[] = $file;
            }
        }
        closedir($directory);
    }

    if ($arParams["TEMPLATE_THEME"] == "site") {
        $solution = COption::GetOptionString("main", "wizard_solution", "", SITE_ID);
        if ($solution == "eshop") {
            $theme = COption::GetOptionString("main", "wizard_eshop_bootstrap_theme_id", "blue", SITE_ID);
            $arParams["TEMPLATE_THEME"] = (in_array($theme, $arAvailableThemes)) ? $theme : "blue";
        }
    } else {
        $arParams["TEMPLATE_THEME"] = (in_array($arParams["TEMPLATE_THEME"], $arAvailableThemes)) ?
            $arParams["TEMPLATE_THEME"] : "blue";
    }
} else {
    $arParams["TEMPLATE_THEME"] = "blue";
}
$arParams["POPUP_POSITION"] = (isset($arParams["POPUP_POSITION"]) && in_array($arParams["POPUP_POSITION"],
        array("left", "right"))) ? $arParams["POPUP_POSITION"] : "left";

foreach ($arResult["ITEMS"] as $key => $arItem) {
    switch ($arItem['CODE']) :
        case 'SEX':
            $arResult["ITEMS"][$key]['CLASS'] = "switcher";
            foreach ($arItem['VALUES'] as $id => $value) {
                switch ($id) :
                    case '25':
                        $arResult["ITEMS"][$key]['VALUES'][$id]['ICON_CLASS'] = 'switcher__icon--primary';
                        break;
                    case '26':
                        $arResult["ITEMS"][$key]['VALUES'][$id]['ICON_CLASS'] = 'switcher__icon--secondary';
                        break;
                endswitch;
            }
            break;
        case 'AGE':
            $arResult["ITEMS"][$key]['CLASS'] = "age-filter";
            foreach ($arItem['VALUES'] as $id => $value) {
                switch ($id) :
                    case '19':
                        $arResult["ITEMS"][$key]['VALUES'][$id]['ICON_CLASS'] = 'icon-kid';
                        break;
                    case '20':
                        $arResult["ITEMS"][$key]['VALUES'][$id]['ICON_CLASS'] = 'icon-small-girl';
                        break;
                    case '21':
                        $arResult["ITEMS"][$key]['VALUES'][$id]['ICON_CLASS'] = 'icon-boy';
                        break;
                    case '22':
                        $arResult["ITEMS"][$key]['VALUES'][$id]['ICON_CLASS'] = 'icon-girl';
                        break;
                    case '23':
                        $arResult["ITEMS"][$key]['VALUES'][$id]['ICON_CLASS'] = 'icon-teenager';
                        break;
                    case '24':
                        $arResult["ITEMS"][$key]['VALUES'][$id]['ICON_CLASS'] = 'icon-cool-guy';
                        break;
                endswitch;
            }
            break;
    endswitch;
}

$arResult["ITEMS"] = LenalHelp::swapFilterVals($arResult["ITEMS"]);

unset($arResult["ITEMS"][ARRFILTER_USELESS]);

/**
 * gorgoc убрал
 */
//if (LenalHelp::checkedFilterVal($arResult["ITEMS"], ARRFILTER_LENGTH)) {
//    $getLengthAndSize = LenalHelp::getSizeAndLendgth($arResult["ITEMS"], ARRFILTER_SIZE,
//        OBUV_RAZMER, DLINA_STELKI_SM);
//    $unsetFilterLength = LenalHelp::unsetFilters($arResult["ITEMS"], ARRFILTER_LENGTH,
//        ARRFILTER_SIZE, $getLengthAndSize['size']);
//}
//
//if (LenalHelp::checkedFilterVal($arResult["ITEMS"], ARRFILTER_SIZE)) {
//    $getLengthAndSize = LenalHelp::getSizeAndLendgth($arResult["ITEMS"], ARRFILTER_SIZE,
//        OBUV_RAZMER, DLINA_STELKI_SM);
//    $unsetFilterSize = LenalHelp::unsetFilters($arResult["ITEMS"],
//        ARRFILTER_SIZE, ARRFILTER_LENGTH, $getLengthAndSize['length']);
//}
//
//if (LenalHelp::checkedFilterVal($arResult["ITEMS"], ARRFILTER_LENGTH)
//    || LenalHelp::checkedFilterVal($arResult["ITEMS"], ARRFILTER_SIZE)) {
//    foreach ($unsetFilterSize as $whatSizeUnset) {
//        unset($arResult["ITEMS"][ARRFILTER_LENGTH]["VALUES"][$whatSizeUnset]);
//    }
//
//    foreach ($unsetFilterLength as $whatLengthUnset) {
//        unset($arResult["ITEMS"][ARRFILTER_SIZE]["VALUES"][$whatLengthUnset]);
//    }
//}
