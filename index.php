<?
define("LOCAL_TEMPLATE_NO_TITLE", "Y");
define("LOCAL_TEMPLATE_NO_BREADCRUMBS", "Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Интернет-магазин ортопедической обуви для детей и взрослых с доставкой по всей России. Собственное производство и гарантия от производителя 45 дней.");
$APPLICATION->SetPageProperty("title", "Купить ортопедическую обувь для детей и взрослых в интернет-магазине ORTHOBOOM");
$APPLICATION->SetTitle("Интернет-магазин ортопедической обуви для детей и взрослых ORTHOBOOM ");
$APPLICATION->AddHeadString('<link rel="canonical" href="https://orthoboom.ru' . str_replace('index.php', '', $APPLICATION->GetCurPage(true)) . '" />');
?>

	<? $APPLICATION->IncludeComponent("bitrix:news.list", "index-banners-small", Array(
		"IBLOCK_TYPE" => "index",
		"IBLOCK_ID" => "103",
		
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"CHECK_DATES" => "N",
		"FILTER_NAME" => "",

		"NEWS_COUNT" => "4",
		"PROPERTY_CODE" => array(
			0 => "*",
		),
		"FIELD_CODE" => array(
			0 => "",
		),

		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER2" => "DESC",
		
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",	
		"ADD_SECTIONS_CHAIN" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		
		"PREVIEW_PICTURE_WIDTH" => 250 * 1.5,
		"PREVIEW_PICTURE_HEIGHT" => 220 * 1.5,
		"PREVIEW_PICTURE_RESIZE_TYPE" => "",
		"PREVIEW_PICTURE_CREATE" => "DETAIL_PICTURE",
	), false); ?>


	<? $APPLICATION->IncludeComponent("bitrix:news.list", "index-feature", Array(
		"IBLOCK_TYPE" => "index",
		"IBLOCK_ID" => "101",
		
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"CHECK_DATES" => "N",
		"FILTER_NAME" => "",

		"NEWS_COUNT" => "100",
		"PROPERTY_CODE" => array(
			0 => "*",
		),
		"FIELD_CODE" => array(
			0 => "",
		),

		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER2" => "DESC",
		
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",	
		"ADD_SECTIONS_CHAIN" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
	), false); ?>
	
	
	
	<div class="index-preca">
	
		<div class="index-preca-tabs index-tabs ui-tab ui-tab--rounded">
			<div class="ui-tab-items">
				<div class="ui-tab-item active show" role="tab">Хиты продаж</div>
				<div class="ui-tab-item" role="tab">Новинки</div>
				<div class="ui-tab-item" role="tab">Распродажа</div>
				<a class="ui-tab-item" title="Каталог" href="/catalog/">Весь каталог</a>
			</div>
		</div>	
	
		<div class="index-preca-contents ui-tab-contents">
	
		<div class="index-preca-1 show ui-tab-content show" role="tabpanel">
	
	<? $GLOBALS["arCatalogHitFilter"] = ["!PROPERTY_MARK_HIT" => false]; ?> 
	
	<? $APPLICATION->IncludeComponent("bitrix:catalog.section", "preview", Array(
		"IBLOCK_TYPE" => "",
		"IBLOCK_ID" => "94",
		
		"SECTION_ID" => "",	
		"SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"BY_LINK" => "",
	
		"PAGE_ELEMENT_COUNT" => "5",		
	
		"FILTER_NAME" => "arCatalogHitFilter",
	
		"ELEMENT_SORT_FIELD" => "PROPERTY_MARK_HIT",
		"ELEMENT_SORT_ORDER" => "DESC",
		"ELEMENT_SORT_FIELD2" => "ID",
		"ELEMENT_SORT_ORDER2" => "desc",
	
		"PROPERTY_CODE" => Array("*"),
		"PRICE_CODE" => Array("MAIN_BASE_PRICE"),


		"DISPLAY_BOTTOM_PAGER" => "N",

		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		
		"OFFERS_FIELD_CODE" => Array(),
		"OFFERS_PROPERTY_CODE" => Array(),
		"OFFERS_SORT_FIELD" => "",
		"OFFERS_SORT_ORDER" => "",
		"OFFERS_SORT_FIELD2" => "",
		"OFFERS_SORT_ORDER2" => "",
		"OFFERS_LIMIT" => "-1",
		
		"COMPATIBLE_MODE" => "Y",
		
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",

		"PREVIEW_PICTURE_WIDTH" => 250,
		"PREVIEW_PICTURE_HEIGHT" => 220,
		"PREVIEW_PICTURE_RESIZE_TYPE" => "",
		"PREVIEW_PICTURE_CREATE" => "DETAIL_PICTURE",
		
	), false); ?>	
	
		</div>
		
		<div class="index-preca-2 ui-tab-content hide" role="tabpanel">
		
		<? $GLOBALS["arCatalogNoveltyFilter"] = ["!PROPERTY_MARK_NOVELTY" => false]; ?> 
	
	<? $APPLICATION->IncludeComponent("bitrix:catalog.section", "preview", Array(
		"IBLOCK_TYPE" => "",
		"IBLOCK_ID" => "94",
		
		"SECTION_ID" => "",	
		"SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"BY_LINK" => "",
	
		"PAGE_ELEMENT_COUNT" => "5",		
	
		"FILTER_NAME" => "arCatalogNoveltyFilter",
	
		"ELEMENT_SORT_FIELD" => "PROPERTY_MARK_NOVELTY",
		"ELEMENT_SORT_ORDER" => "DESC",
		"ELEMENT_SORT_FIELD2" => "ID",
		"ELEMENT_SORT_ORDER2" => "desc",
	
		"PROPERTY_CODE" => Array("*"),
		"PRICE_CODE" => Array("MAIN_BASE_PRICE"),


		"DISPLAY_BOTTOM_PAGER" => "N",

		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		
		"OFFERS_FIELD_CODE" => Array(),
		"OFFERS_PROPERTY_CODE" => Array(),
		"OFFERS_SORT_FIELD" => "",
		"OFFERS_SORT_ORDER" => "",
		"OFFERS_SORT_FIELD2" => "",
		"OFFERS_SORT_ORDER2" => "",
		"OFFERS_LIMIT" => "-1",
		
		"COMPATIBLE_MODE" => "Y",
		
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",

		"PREVIEW_PICTURE_WIDTH" => 250,
		"PREVIEW_PICTURE_HEIGHT" => 220,
		"PREVIEW_PICTURE_RESIZE_TYPE" => "",
		"PREVIEW_PICTURE_CREATE" => "DETAIL_PICTURE",
		
	), false); ?>		

		</div>
	
		<div class="index-preca-3 ui-tab-content hide" role="tabpanel">
		
		<? $GLOBALS["arCatalogSaleFilter"] = ["!PROPERTY_MARK_SALE" => false]; ?> 
	
	<? $APPLICATION->IncludeComponent("bitrix:catalog.section", "preview", Array(
		"IBLOCK_TYPE" => "",
		"IBLOCK_ID" => "94",
		
		"SECTION_ID" => "",	
		"SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"BY_LINK" => "",
	
		"PAGE_ELEMENT_COUNT" => "5",		
	
		"FILTER_NAME" => "arCatalogSaleFilter",
	
		"ELEMENT_SORT_FIELD" => "PROPERTY_MARK_SALE",
		"ELEMENT_SORT_ORDER" => "DESC",
		"ELEMENT_SORT_FIELD2" => "ID",
		"ELEMENT_SORT_ORDER2" => "desc",
	
		"PROPERTY_CODE" => Array("*"),
		"PRICE_CODE" => Array("MAIN_BASE_PRICE"),


		"DISPLAY_BOTTOM_PAGER" => "N",

		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		
		"OFFERS_FIELD_CODE" => Array(),
		"OFFERS_PROPERTY_CODE" => Array(),
		"OFFERS_SORT_FIELD" => "",
		"OFFERS_SORT_ORDER" => "",
		"OFFERS_SORT_FIELD2" => "",
		"OFFERS_SORT_ORDER2" => "",
		"OFFERS_LIMIT" => "-1",
		
		"COMPATIBLE_MODE" => "Y",
		
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",

		"PREVIEW_PICTURE_WIDTH" => 250,
		"PREVIEW_PICTURE_HEIGHT" => 220,
		"PREVIEW_PICTURE_RESIZE_TYPE" => "",
		"PREVIEW_PICTURE_CREATE" => "DETAIL_PICTURE",
		
	), false); ?>		

		</div>

	
		</div>
		
		<script>
		$(".index-preca").uniTabs({
			
		});
		</script>
		
		
		
	</div>
	
	<div class="index-info">
	
		<div class="index-tabs ui-tab ui-tab--rounded">
			<div class="ui-tab-items">
				<div class="ui-tab-item" role="tab">Акции</div>
				<div class="ui-tab-item" role="tab">Новости</div>
				<div class="ui-tab-item" role="tab">Статьи</div>
			</div>
		</div>
		
		<div class="index-info-content">
			
			<div class="index-action ui-tab-content" role="tabpanel">	
				<? $GLOBALS["arCatalogHitFilter"] = []; ?> 
				
				<? $APPLICATION->IncludeComponent("bitrix:news.list", "picture-list", Array(
					"IBLOCK_TYPE" => "info",
					"IBLOCK_ID" => "105",
					
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "Y",
					"CHECK_DATES" => "N",
					"FILTER_NAME" => "arCatalogHitFilter",
				
					"NEWS_COUNT" => "3",
					"PROPERTY_CODE" => array(
						0 => "*",
					),
					"FIELD_CODE" => array(
						0 => "",
					),
				
					"SORT_BY1" => "SORT",
					"SORT_ORDER1" => "ASC",
					"SORT_BY2" => "ACTIVE_FROM",
					"SORT_ORDER2" => "DESC",
					
					"SET_TITLE" => "N",
					"SET_BROWSER_TITLE" => "N",
					"SET_LAST_MODIFIED" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_META_KEYWORDS" => "N",	
					"ADD_SECTIONS_CHAIN" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					
					
					"WRAP_CLASS" => "action-list",
					
					"PREVIEW_PICTURE_WIDTH" => 420,
					"PREVIEW_PICTURE_HEIGHT" => 260,
					"PREVIEW_PICTURE_RESIZE_TYPE" => "",
					"PREVIEW_PICTURE_CREATE" => "DETAIL_PICTURE",
				), false); ?>	
			</div>
			
			
			<div class="index-news ui-tab-content" role="tabpanel">	

					<? $APPLICATION->IncludeComponent("bitrix:news.list", "picture-list", Array(
					"IBLOCK_TYPE" => "info",
					"IBLOCK_ID" => "106",
					
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "Y",
					"CHECK_DATES" => "N",
					"FILTER_NAME" => "arCatalogHitFilter",
				
					"NEWS_COUNT" => "3",
					"PROPERTY_CODE" => array(
						0 => "*",
					),
					"FIELD_CODE" => array(
						0 => "",
					),
				
					"SORT_BY1" => "SORT",
					"SORT_ORDER1" => "ASC",
					"SORT_BY2" => "ACTIVE_FROM",
					"SORT_ORDER2" => "DESC",
					
					"SET_TITLE" => "N",
					"SET_BROWSER_TITLE" => "N",
					"SET_LAST_MODIFIED" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_META_KEYWORDS" => "N",	
					"ADD_SECTIONS_CHAIN" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					
					
					"WRAP_CLASS" => "action-list",
					
					"PREVIEW_PICTURE_WIDTH" => 420,
					"PREVIEW_PICTURE_HEIGHT" => 260,
					"PREVIEW_PICTURE_RESIZE_TYPE" => BX_RESIZE_IMAGE_EXACT,
					"PREVIEW_PICTURE_CREATE" => "DETAIL_PICTURE",
				), false); ?>	


	
			</div>
			
			<div class="index-articles ui-tab-content" role="tabpanel">	
			
			
					<? $APPLICATION->IncludeComponent("bitrix:news.list", "picture-list", Array(
					"IBLOCK_TYPE" => "info",
					"IBLOCK_ID" => "107",
					
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "Y",
					"CHECK_DATES" => "N",
					"FILTER_NAME" => "arCatalogHitFilter",
				
					"NEWS_COUNT" => "3",
					"PROPERTY_CODE" => array(
						0 => "*",
					),
					"FIELD_CODE" => array(
						0 => "",
					),
				
					"SORT_BY1" => "SORT",
					"SORT_ORDER1" => "ASC",
					"SORT_BY2" => "ACTIVE_FROM",
					"SORT_ORDER2" => "DESC",
					
					"SET_TITLE" => "N",
					"SET_BROWSER_TITLE" => "N",
					"SET_LAST_MODIFIED" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_META_KEYWORDS" => "N",	
					"ADD_SECTIONS_CHAIN" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					
					
					"WRAP_CLASS" => "action-list",
					
					"PREVIEW_PICTURE_WIDTH" => 420,
					"PREVIEW_PICTURE_HEIGHT" => 260,
					"PREVIEW_PICTURE_RESIZE_TYPE" => BX_RESIZE_IMAGE_EXACT,
					"PREVIEW_PICTURE_CREATE" => "DETAIL_PICTURE",
				), false); ?>				
		
			</div>			
			
		</div>
		
		<script>
		$(".index-info").uniTabs({
			
		});
		</script>
		
		
	</div>

		<div class="index-tabs ui-tab ui-tab--rounded">
			<div class="ui-tab-items">
				<div class="ui-tab-item active">Видео</div>
			</div>
		</div>


				<? $APPLICATION->IncludeComponent("bitrix:news.list", "youtube-list", Array(
					"IBLOCK_TYPE" => "info",
					"IBLOCK_ID" => "108",
					
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "Y",
					"CHECK_DATES" => "N",
					"FILTER_NAME" => "",
				
					"NEWS_COUNT" => "3",
					"PROPERTY_CODE" => array(
						0 => "*",
					),
					"FIELD_CODE" => array(
						0 => "",
					),
				
					"SORT_BY1" => "SORT",
					"SORT_ORDER1" => "ASC",
					"SORT_BY2" => "ACTIVE_FROM",
					"SORT_ORDER2" => "DESC",
					
					"SET_TITLE" => "N",
					"SET_BROWSER_TITLE" => "N",
					"SET_LAST_MODIFIED" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_META_KEYWORDS" => "N",	
					"ADD_SECTIONS_CHAIN" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					
					
					"WRAP_CLASS" => "video-list",
					
					"PREVIEW_PICTURE_WIDTH" => 420,
					"PREVIEW_PICTURE_HEIGHT" => 260,
					"PREVIEW_PICTURE_RESIZE_TYPE" => "",
					"PREVIEW_PICTURE_CREATE" => "DETAIL_PICTURE",
				), false); ?>


<div class="index-text">

					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_TEMPLATE_PATH . "/include/index-footer-text.php",
							"EDIT_TEMPLATE" => "",
							"AREA_FILE_SUFFIX" => "",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
						)
					);?>
					


</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>