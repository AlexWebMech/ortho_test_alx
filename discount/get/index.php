<html>
<head></head>
<body>

<?
$file = 'coupons.txt';

echo '<p>Предыдущие купоны:</p>';
$lines = file($file);
foreach ($lines as $line_num => $line) {
	$ar = explode('|', $line);
    $coupon = $ar[0];
	$timeFrom = date('d.m.Y H:i:s', intval($ar[1]));
	$timeTo = date('d.m.Y H:i:s', intval($ar[2]));
	$couponStatus = '<span style="color:green;">дейстителен</span>';
	if ($ar[2] < time()) {
			$couponStatus = '<span style="color:red;">просрочен</span>';
	}
	echo $line_num+1 . '. <a target="blank" href="/discount/?coupon=' . $coupon . '">https://'.$_SERVER['HTTP_HOST'] . '/discount/?coupon=' . $coupon . '</a>, создан ' . $timeFrom. ', действителен до ' . $timeTo . ' (' . $couponStatus  . ')<br />';
}
echo '<br /><br />';
?>

<form action="" method="post">
	Срок действия (часы): <input type="number" name="period" value= "48">
	<input type="submit" name="submit" value= "Создать купон">
</form>
<?
if (isset($_POST['submit'])) {
	if (isset($_POST['period']) && ($period = intval($_POST['period']))) {
		$coupon = md5(microtime());
		$fd = fopen($file, 'a');
		if(!$fd || !flock($fd,LOCK_EX)) die('Невозможно открыть файл');
		$currTime = time();
		fwrite($fd, $coupon. '|' . strval($currTime) . '|' . strval($currTime + $period * 3600) . "\n");
		flock($fd,LOCK_UN);
		 
		fclose($fd);

		$path = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'],'/'));
		echo '<p>Новый купон:</p>';
		echo '<a target="blank" href="/discount/?coupon=' . $coupon . '">https://'.$_SERVER['HTTP_HOST'] . '/discount/?coupon=' . $coupon . '</a>, создан ' . date('d.m.Y H:i:s', $currTime) . ', действителен до ' . date('d.m.Y H:i:s', $currTime + $period * 3600) . ' (' . $period  . ' ч.)';
	}
}
?>
</body>
</html>