<?
$userCoupon = '';
if (isset($_GET['coupon'])) $userCoupon = trim($_GET['coupon']);
if (!$userCoupon) {
	header('Location: /404.php');
	die();
}

$lines = file('get/coupons.txt');

foreach ($lines as $line_num => $line) {
	$ar = explode('|', $line);
    $coupon = $ar[0];
	if ($userCoupon == $coupon) {
		$timeFrom = $ar[1];
		$timeTo = $ar[2];
		$curTime = time();
		if ($curTime <= $timeTo) {
			/*if (!isset($_COOKIE['fromlanding']))*/ setcookie('fromlanding', $timeFrom, $timeTo, '/', '.orthoboom.ru');
			header('Location: /catalog2');
			die();
		} else {
			unset($_COOKIE['fromlanding']);
			setcookie('fromlanding', '', $curTime - 3600, '/'); // delete cookie
			header('Location: /404.php');
			die();
		}
	}
}
header('Location: /404.php');
die();
?>