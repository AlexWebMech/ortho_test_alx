<?
define("STOP_STATISTICS", true);

include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$arParams["EXCEL"] = false;
if (isset($_REQUEST["excel"]))
	$arParams["EXCEL"] = true;

if (!$USER->IsAdmin())
	die('Просмотр доступен только администраторам');

$arWishAll = array();
$arFilter = array("!=UF_WISHLIST" => false);
if (isset($_REQUEST["target"])) {
	$groupId = intval($_REQUEST["target"]);
	$arFilter["GROUPS_ID"] = array($groupId);
}

$rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $arFilter, array("SELECT" => array("UF_WISHLIST"), "FIELDS" => array("ID")));



while ($resUsers = $rsUsers->GetNext()) {
	foreach ($resUsers["UF_WISHLIST"] as $wishID) {
		$arWishAll[] = $wishID;
	}
}

if ($arParams["EXCEL"]) {
	$APPLICATION->RestartBuffer();

	Header("Content-Type: application/force-download");
	Header("Content-Type: application/octet-stream");
	Header("Content-Type: application/download");
	Header("Content-Disposition: attachment;filename=wishlist.xls");
	Header("Content-Transfer-Encoding: binary");

	?><meta http-equiv="Content-type" content="text/html;charset=<?echo LANG_CHARSET?>" /><?

}
?>
<table border="1">
	<thead>
		<tr>
			<th>Название</th>
			<th>Код 1C</th>
			<th>Кол-во подписок</th>
		</tr>
	</thead>
	<tbody>
	<?
	if (count($arWishAll) == 0):?>
		<tr>
			<td colspan="3">Нет товаров добавленных в список желаний</td>
		</tr>	
	<?else:
		$arWishAllGroup = array_count_values($arWishAll);
		uasort($arWishAllGroup, function ($item1, $item2) {
		    if ($item1 == $item2) return 0;
		    return $item1 > $item2 ? -1 : 1;
		});

		$arProductIds = array_keys($arWishAllGroup);

		$res = CIBlockElement::GetList(Array(), 
			Array(
				"IBLOCK_ID" => 56, 
				"ACTIVE_DATE" => "Y", 
				"ACTIVE" => "Y", 
				"ID" => $arProductIds
			), false, false, 
			Array(
				"ID", 
				"NAME", 
				"XML_ID"
			)
		);
		$arProductData = array();
		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			$arProductData[$arFields["ID"]] = $arFields;
		}

		foreach ($arWishAllGroup as $key => $count):
			if (!isset($arProductData[$key]))
				continue;
			?>
			<tr>
				<td><?=$arProductData[$key]["NAME"]?></td>
				<td><?=$arProductData[$key]["XML_ID"]?></td>
				<td><?=$count?></td>
			</tr>
		<?endforeach;
	endif;
	?>
	</tbody>
</table>
<?
if ($arParams["EXCEL"]) {
	die();
} else {
?>
<br>
<div class="excel-links">
	<a href="/wish.php?target=5&excel">Скачать excel для ФЛ</a>
</div>
<div class="excel-links">
	<a href="/wish.php?target=6&excel">Скачать excel для ЮЛ</a>
</div>
<div class="excel-links">
	<a href="/wish.php?excel">Скачать excel для ФЛ и ЮЛ</a>
</div>

<style>
	table, td, th {
		border: 1px solid #000;
		border-collapse: collapse;
		padding: 5px;
	}
	.excel-links {
		margin-bottom: 6px;
	}
</style>
<?}?>