<p>
	Заказать через интернет-магазин - Любовь Мазанова, тел. +79101050145 (пн-пт с 08:00 до 17:00), e-mail: <a href="mailto:mazanova.ll@julianna.ru">mazanova.ll@julianna.ru</a>.
</p>
<p>
	Опытные консультанты помогут подобрать правильную обувь для вашего ребенка в ортопедических центрах «Юлианна».
</p>
<p>
	<span style="color:#ff3732; font-weight:bold;">Красными</span> значками на карте отмечены офисы и точки розничных продаж, <span style="color:#3fd7fd; font-weight:bold;">голубыми</span> - города оптовых продаж.
</p>
<div style="text-align: center;">
 <a href="https://vk.com/club73756667" target="_blank" title="Мы на ВКонтакте"><img width="29" alt="Мы на ВКонтакте" src="/images/vk.png" height="29"></a> <a href="https://www.facebook.com/Orthoboom/" target="_blank" title="Мы на Facebook"><img width="29" alt="Мы на Facebook" src="/images/fb.png" height="29"></a> <a href="https://ok.ru/group/52352615120962" target="_blank" title="Мы в Одноклассниках"><img width="31" alt="Мы в Одноклассниках" src="/images/ok.png" height="30" border="0"></a> <a href="https://www.instagram.com/orthoboom/" target="_blank" title="Мы в Инстаграме"><img width="29" alt="Мы в Инстаграме" src="/images/instagram.png" height="29" border="0"></a>
</div>
<br>