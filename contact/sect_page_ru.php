<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//$APPLICATION->SetTitle("Где купить");
//$APPLICATION->SetPageProperty('title', 'Где купить ортопедическую обувь из кожи для детей');
//$APPLICATION->SetPageProperty('description', 'Где купить. «ORTHOBOOM» - правильная обувь');
?><div class="content" style="width:90%; margin:0 auto;">
    <h1><?$APPLICATION->ShowTitle(false);?></h1>
	<div class="maps">
		 <?$APPLICATION->IncludeComponent(
	"ithive:offices",
	"template1",
	Array(
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "N",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FIELD_CODE" => array("",""),
		"DETAIL_PAGER_SHOW_ALL" => "N",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PROPERTY_CODE" => array("",""),
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PANEL" => "N",
		"DISPLAY_TOP_PAGER" => "Y",
		"FILTER_FIELD_CODE" => array("",""),
		"FILTER_NAME" => "arrFilter",
		"FILTER_PROPERTY_CODE" => array("CITY",""),
		"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "1",
		"ICON_FILE" => "/images/map.png",
		"ICON_OFFSET" => "-15,-30",
		"ICON_SIZE" => "24,33",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_JQUERY" => "Y",
		"KEY" => "ADnSjVMBAAAAcugeagIAMZgG9qA9UTqX39AYbNoeespLKzoAAAAAAAAAAAAIf6Xr_HVW8xkbyaXPAvydJ4SpYg==",
		"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"LIST_FIELD_CODE" => array("",""),
		"LIST_PROPERTY_CODE" => array("NAME_SHORT","ADDRESS","WWW","PHONES",""),
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "2000",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_TITLE" => "Новости",
		"PREVIEW_TRUNCATE_LEN" => "250",
		"SEF_MODE" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "Y",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_REVIEW" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"VARIABLE_ALIASES" => Array("ELEMENT_ID"=>"ELEMENT_ID","SECTION_ID"=>"SECTION_ID")
	)
);?>
	</div>
</div>
 <br>