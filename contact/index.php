<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Контакты компании ORTHOBOOM | Интернет-магазин ортопедической обуви");
$APPLICATION->SetPageProperty("description", "На данной странице интернет-магазина ORTHOBOOM представлена контактная информация.");
$APPLICATION->SetPageProperty("title", "Контакты | Интернет-магазин ORTHOBOOM");
$APPLICATION->SetTitle("Контакты");

$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "page_".GetLang(),
		"EDIT_TEMPLATE" => ""
	)
);

?>
<div style="display: none;" itemscope itemtype="http://schema.org/Organization">
  <span itemprop="name">ORTHOBOOM</span>
  
  <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
    <span itemprop="streetAddress">Кировоградская, д.13А, ТРЦ "Columbus"</span>
    <span itemprop="postalCode">117519</span>
    <span itemprop="addressLocality">Москва</span>,
  </div>
  <span itemprop="telephone">+7 (910) 393-73-51</span>
  <span itemprop="telephone">8 (800) 234-66-40</span>
  <span itemprop="email">zakaz@orthoboom.ru</span>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>