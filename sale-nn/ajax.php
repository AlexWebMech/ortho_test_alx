<?php
require_once('config.php');

if (!isset($_POST['type'])) die();
$type = strtolower(trim($_POST['type']));

$to = 'golubeva@julianna.ru';
//$to = 'ko-alex@mail.ru';


// проверка капчи
/*
if (isset($_POST["captcha_word"]) && isset($_POST["captcha_code"])) {
	if(!$APPLICATION->CaptchaCheckCode($_POST["captcha_word"], $_POST["captcha_code"])) {
		die('0'); //Неверный проверочный код.'
	}
}
*/

foreach ($_POST as $key => $val) $_POST[$key] = iconv('UTF-8', 'Windows-1251', $val);

switch ($type) {
	case 'frm1':
		$subject = 'Уведомление сервиса "Акция МЕГАSALE - Получить промокод"';
		$message =  'Имя: ' . $_POST['name'] . "\r\n" .
			'E-mail: ' . $_POST['email'] . "\r\n" .
			'Телефон: ' . $_POST['phone'];
		$successMessage = 'Спасибо! Мы свяжемся с вами в самое ближайшее время.';
		break;
	case 'frm2':
		$subject = 'Уведомление сервиса "Акция МЕГАSALE - Обратный звонок"';
		$message =  'Имя: ' . $_POST['name'] . "\r\n" .
			'Телефон: ' . $_POST['phone'];
		$successMessage = 'Спасибо! Мы свяжемся с вами в самое ближайшее время.';
		break;
	default:
		die();
}

$headers = 'From: noreply@orthoboom.ru' . "\r\n" .
    'Reply-To: noreply@orthoboom.ru' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

if (mail($to, $subject, $message, $headers)) 
	die(iconv('windows-1251', 'utf-8', $successMessage));
else
	die('Ошибка при отправке сообщения.');
?>