<?
$arrInfo = array(
	'yaroslavl' => array(
		'logo' => 'yaroslavl.gif',
		'logoWidth' => '100%',
		'phoneLabel' => '8 (902) 330-08-08',
		'phone' => '+79023300808',
		'workingHours' => 'ежедневно с 9.00-20.00',
		'officeAddr' => '',
		'discountPercent' => '25% СКИДКА НА ВТОРУЮ ПАРУ',
		'discountPercentStyle' => '',
		'promoButtonText' => 'Получить промокод на скидку',
		'salesEmail' => 'sales@atletika-orto.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A7a5d255bba56ba1d64fd43f3b274d6d737e42521e975e4feb303c85c8d37f165&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'ДО 31 МАЯ В СЕТИ ОРТОПЕДИЧЕСКИХ САЛОНОВ &quot;АТЛЕТИКА&quot;',
		'caption' => 'ДЕТСКАЯ ОРТОПЕДИЧЕСКАЯ ОБУВЬ ORTHOBOOM В ЯРОСЛАВЛЕ',
		'yaMetrikaCounterId' => '44214884',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44214884 = new Ya.Metrika({
                    id:44214884,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44214884" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->'
	),
	
	'astrahan' => array(
		'logo' => 'astrahan.jpg',
		'logoWidth' => '100%',
		'phoneLabel' => '8 (8512) 666-700',
		'phone' => '+78512666700',
		'workingHours' => 'ежедневно с 9.00-19.00',
		'officeAddr' => '<p>г. Астрахань,</p><p>Красная Набережная, д.13</p>',
		'discountPercent' => '25% СКИДКА НА ВТОРУЮ ПАРУ',
		'discountPercentStyle' => '',
		'promoButtonText' => 'Получить промокод на скидку',
		'salesEmail' => 'ndemyanenko@orto-30.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa2b951cb6e7db93a13d3bc629fb0a9d2a36a48f4a6a09b660fff6127957633a4&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'ДО 31 МАЯ В СЕТИ САЛОНОВ &quot;ОРТОПЕДИЧЕСКИЕ ИЗДЕЛИЯ&quot;',
		'caption' => 'ДЕТСКАЯ ОРТОПЕДИЧЕСКАЯ ОБУВЬ ORTHOBOOM В АСТРАХАНИ',
		'yaMetrikaCounterId' => '44248174',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44248174 = new Ya.Metrika({
                    id:44248174,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44248174" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->'
	),
	
	'kursk' => array(
		'logo' => 'kursk.jpg',
		'logoWidth' => '100%',
		'phoneLabel' => '8 (800) 333-40-07',
		'phone' => '+78003334007',
		'workingHours' => 'ежедневно с 9.00-20.00',
		'officeAddr' => '<p>г. Курск,</p><p>Проспект Ленинского Комсомола, 57А</p>',
		'discountPercent' => '20% СКИДКА НА ВТОРУЮ ПАРУ, 40% СКИДКА НА ТРЕТЬЮ ПАРУ',
		'discountPercentStyle' => '',
		'promoButtonText' => 'Получить промокод на скидку',
		'salesEmail' => 'callcentre@orto-doktor.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A1a77e35298686ff871dad78abef06afb66c557dc0e7e0f7419a5e3ffdab6b515&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'ДО 31 МАЯ В СЕТИ САЛОНОВ ОРТОПЕДИИ &quot;ОРТО-ДОКТОР&quot;',
		'caption' => 'ДЕТСКАЯ ОРТОПЕДИЧЕСКАЯ ОБУВЬ ORTHOBOOM В КУРСКЕ',
		'yaMetrikaCounterId' => '44313804',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44313804 = new Ya.Metrika({
                    id:44313804,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44313804" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter --> '
	),
	
	'krasnodar' => array(
		'logo' => 'krasnodar.jpg',
		'logoWidth' => '100%',
		'phoneLabel' => '8 (861) 241-20-01<br>8 (988) 241-20-01',
		'phone' => '+78612412001',
		'workingHours' => 'ежедневно с 9.00-20.00',
		'officeAddr' => '<p>г. Краснодар,</p><p>ул. Айвазовского, 108</p>',
		'discountPercent' => '25% СКИДКА НА ВТОРУЮ ПАРУ',
		'discountPercentStyle' => '',
		'promoButtonText' => 'Получить промокод на скидку',
		'salesEmail' => 'maximed@yandex.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aaf7ae376cecb830da6d25a7518aa72f5016ea7c38a1d3177b6701de82a36df1b&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'ДО 31 МАЯ В СЕТИ САЛОНОВ &quot;МАКСИМЕД&quot;',
		'caption' => 'ДЕТСКАЯ ОРТОПЕДИЧЕСКАЯ ОБУВЬ ORTHOBOOM В КРАСНОДАРЕ',
		'yaMetrikaCounterId' => '44333914',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44333914 = new Ya.Metrika({
                    id:44333914,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44333914" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->'
	),
	
	'perm' => array(
		'logo' => 'perm.jpg',
		'logoWidth' => '100%',
		'phoneLabel' => '8 (804) 333-01-59',
		'phone' => '+78043330159',
		'workingHours' => 'ежедневно с 9.00-20.00',
		'officeAddr' => '<p>г. Пермь,</p><p>ул.Горького, д.9, оф.17</p>',
		'discountPercent' => 'СКИДКА 20% ПРИ ПОКУПКЕ 2 ПАР И БОЛЕЕ',
		'discountPercentStyle' => '',
		'promoButtonText' => 'Получить промокод на скидку',
		'salesEmail' => 'support@alteramed.org',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa9430ba8aca2bdb3a59f2b91746d047e7f14e595eead8c7e9634b734a4bc135f&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'ДО 31 МАЯ В СЕТИ САЛОНОВ &quot;alteraMED&quot;',
		'caption' => 'ДЕТСКАЯ ОРТОПЕДИЧЕСКАЯ ОБУВЬ ORTHOBOOM В ПЕРМИ',
		'yaMetrikaCounterId' => '44333857',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44333857 = new Ya.Metrika({
                    id:44333857,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44333857" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->'
	),
	
	'moskovskaya-obl' => array(
		'logo' => 'moskovskaya-obl.jpg',
		'logoWidth' => '100%',
		'phoneLabel' => '8 (985) 440-00-03',
		'phone' => '+79854400003',
		'workingHours' => 'ежедневно с 9.00-20.00',
		'officeAddr' => '',
		'discountPercent' => '25% СКИДКА НА ВТОРУЮ ПАРУ',
		'discountPercentStyle' => '',
		'promoButtonText' => 'Получить промокод на скидку',
		'salesEmail' => 'vzorto@yandex.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A6f314e42f914669a1c92ddeae7f0def878770239c59f182a03b6062783ad4e67&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'ДО 31 МАЯ В СЕТИ САЛОНОВ &quot;ВАШЕ ЗДОРОВЬЕ&quot;',
		'caption' => 'ДЕТСКАЯ ОРТОПЕДИЧЕСКАЯ ОБУВЬ ORTHOBOOM В МОСКОВСКОЙ ОБЛАСТИ',
		'yaMetrikaCounterId' => '44521144',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44521144 = new Ya.Metrika({
                    id:44521144,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44521144" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->'
	),
	
	'ekaterinburg' => array(
		'logo' => 'ekaterinburg.png',
		'logoWidth' => '100%',
		'phoneLabel' => '8 (343) 318-21-48',
		'phone' => '+73433182148',
		'workingHours' => 'ежедневно с 9.00-20.00',
		'officeAddr' => '',
		'discountPercent' => '50% СКИДКА НА ВТОРУЮ ПАРУ',
		'discountPercentStyle' => '',
		'promoButtonText' => 'Получить промокод на скидку',
		'salesEmail' => 'im@ortix.ru,<br>spravka@ortix.ru,<br>k.alekseeva@ortix.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa6bcafb45c3cd4ee0a2d07f8e5246ca70ac1a34910b9c6f3bcdda6cf67910d4c&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'ДО 30 ИЮНЯ В СЕТИ ОРТОПЕДИЧЕСКИХ САЛОНОВ &quot;ОРТИКС&quot;',
		'caption' => 'ДЕТСКАЯ ОРТОПЕДИЧЕСКАЯ ОБУВЬ ORTHOBOOM В ЕКАТЕРИНБУРГЕ',
		'yaMetrikaCounterId' => '44506444',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44506444 = new Ya.Metrika({
                    id:44506444,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44506444" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->'
	),
	
	'irkutsk' => array(
		'logo' => 'irkutsk.jpg',
		'logoWidth' => '100%',
		'phoneLabel' => '8 (800) 500-30-79',
		'phone' => '+78005003079',
		'workingHours' => 'ежедневно с 9.00-20.00',
		'secondPic' => 'kovrik_ortho.jpg',
		'secondText' => 'МАССАЖНЫЙ КОВРИК ОРТО ИЗ 4 МОДУЛЕЙ',
		'officeAddr' => 'г. Иркутск,<br>ул. Лунная, 1',
		'discountPercent' => 'ПРИ ПОКУПКЕ ОРТОПЕДИЧЕСКОЙ ОБУВИ ORTHOBOOM ДАРИМ МАССАЖНЫЙ КОВРИК ОРТО ИЗ 4 МОДУЛЕЙ',
		'discountPercentStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'Получить купон на подарок',
		'salesEmail' => 'shop@osnovad.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Acf3a187adf6384f998aa621ab695f227522414be5c1b6a5d06ced30344c4d2fc&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'ДО 30 ИЮНЯ В СЕТИ МАГАЗИНОВ ОРТОПЕДИИ И МЕДТЕХНИКИ &quot;ОСНОВА ДВИЖЕНИЯ&quot;',
		'caption' => 'ДЕТСКАЯ ОРТОПЕДИЧЕСКАЯ ОБУВЬ ORTHOBOOM В ИРКУТСКЕ',
		'yaMetrikaCounterId' => '44520625',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44520625 = new Ya.Metrika({
                    id:44520625,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44520625" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->'
	),
	
	'mahachkala' => array(
		'logo' => 'mahachkala.jpg',
		'logoWidth' => '60%',
		'phoneLabel' => '8 (928) 800-78-78',
		'phone' => '+79288007878',
		'workingHours' => 'ежедневно с 9.00-20.00',
		'secondPic' => '',
		'secondText' => '',
		'officeAddr' => 'г. Махачкала, пр. Р.Гамзатова, 97 Б',
		'discountPercent' => 'СКИДКА 10% ПРИ ПОКУПКЕ ОБУВИ',
		'discountPercentStyle' => '',
		'promoButtonText' => 'Получить промокод на скидку',
		'salesEmail' => 'ortositi.market@mail.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A0c0a64d829fc9b106b3d13cdcd85d842ec320fa77a4e89ce9bbc0967ba9e4662&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'ДО 31 МАЯ В СЕТИ САЛОНОВ &quot;ОРТОСИТИ&quot;',
		'caption' => 'ДЕТСКАЯ ОРТОПЕДИЧЕСКАЯ ОБУВЬ ORTHOBOOM В МАХАЧКАЛЕ',
		'yaMetrikaCounterId' => '44521894',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44521894 = new Ya.Metrika({
                    id:44521894,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44521894" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->'
	),	
	
	'vladivostok' => array(
		'logo' => 'vladivostok.png',
		'logoWidth' => '100%',
		'phoneLabel' => '8 (800) 250-47-99',
		'phone' => '+78002504799',
		'workingHours' => 'ежедневно с 9.00-20.00',
		'secondPic' => '',
		'secondText' => '',
		'officeAddr' => 'г. Владивосток, ул. Иртышская, 23',
		'discountPercent' => 'СКИДКА 50% НА ВТОРУЮ ПАРУ ПРИ ПОКУПКЕ ОБУВИ ОРТОБУМ',
		'discountPercentStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'Получить промокод на скидку',
		'salesEmail' => 'shop@ortom.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Adcccc70a2b6f6b848cba1a233e56828e42eb50c090838cfa65c25a57b15e17ba&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'ДО 20 ИЮНЯ В СЕТИ САЛОНОВ &quot;ОРТОМЕД&quot;',
		'caption' => 'ДЕТСКАЯ ОРТОПЕДИЧЕСКАЯ ОБУВЬ ORTHOBOOM ВО ВЛАДИВОСТОКЕ',
		'yaMetrikaCounterId' => '44521729',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44521729 = new Ya.Metrika({
                    id:44521729,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44521729" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->'
	),
	
	'ufa' => array(
		'logo' => 'ufa.jpg',
		'logoWidth' => '40%',
		'phoneLabel' => '8 (347) 226 97 18',
		'phone' => '+73472269718',
		'workingHours' => 'ежедневно с 9.00-20.00',
		'secondPic' => '',
		'secondText' => '',
		'officeAddr' => '',
		'discountPercent' => 'СКИДКА 10% НА ПОКУПКУ ОБУВИ ОРТОБУМ',
		'discountPercentStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'Получить промокод на скидку',
		'salesEmail' => '',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ab386850d790f104f1d10027405fa7c8e4df75b4937f14cc4413a49a0d70f6043&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'ДО 31 МАЯ В СЕТИ ОРТОПЕДИЧЕСКИХ САЛОНОВ &quot;ОРТОЛЕНД&quot;',
		'caption' => 'ДЕТСКАЯ ОРТОПЕДИЧЕСКАЯ ОБУВЬ ORTHOBOOM В УФЕ',
		'yaMetrikaCounterId' => '44521558',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44521558 = new Ya.Metrika({
                    id:44521558,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44521558" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->'
	),
	
	'kazan' => array(
		'logo' => 'kazan.jpg',
		'logoWidth' => '100%',
		'phoneLabel' => '8 (843) 226-69-99',
		'phone' => '+78432266999',
		'workingHours' => 'ежедневно с 9.00-20.00',
		'secondPic' => '',
		'secondText' => '',
		'officeAddr' => '',
		'discountPercent' => 'СКИДКА 25% НА ВТОРУЮ ПАРУ ПРИ ПОКУПКЕ ОБУВИ ОРТОБУМ',
		'discountPercentStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'Получить промокод на скидку',
		'salesEmail' => 'ortexmed@mail.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A73286a6d149e733d54f2aabe0b77fa631d4e9ad9f01bedf5dceb88f1fa105fc1&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'ДО 31 МАЯ В СЕТИ ОРТОПЕДИЧЕСКИХ САЛОНОВ &quot;ОРТЕКС МЕД&quot;',
		'caption' => 'ДЕТСКАЯ ОРТОПЕДИЧЕСКАЯ ОБУВЬ ORTHOBOOM В КАЗАНИ',
		'yaMetrikaCounterId' => '44759233',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44759233 = new Ya.Metrika({
                    id:44759233,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44759233" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->'
	),
	
	'ivanovo' => array(
		'logo' => 'ivanovo.jpg',
		'logoWidth' => '60%',
		'phoneLabel' => '8 (800) 250-62-20',
		'phone' => '+78002506220',
		'workingHours' => 'ежедневно с 9.00-20.00',
		'secondPic' => '',
		'secondText' => '',
		'officeAddr' => '',
		'discountPercent' => 'СКИДКА 25% НА ВТОРУЮ ПАРУ ПРИ ПОКУПКЕ ОБУВИ ОРТОБУМ',
		'discountPercentStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'Получить промокод на скидку',
		'salesEmail' => 'kukonh_iv@nikamed-online.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ac74a654d21d4f1bece8b7ed0d50c2a31c211851cb64201ce0606021b985b7bc7&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'ДО 30 ИЮНЯ В СЕТИ САЛОНОВ',
		'caption' => 'ДЕТСКАЯ ОРТОПЕДИЧЕСКАЯ ОБУВЬ ORTHOBOOM В ИВАНОВО',
		'yaMetrikaCounterId' => '',
		'yaMetrikaCounter' => ''
	),
	
	'nn' => array(
		'logo' => 'nn1.jpg',
		'logoWidth' => '70%',
		'phoneLabel' => '8 (831) 410-48-68',
		'phone' => '+78314104868',
		'workingHours' => '15, 16, 17, 18 июня с 9.00-20.00',
		'secondPic' => '',
		'secondText' => '',
		'officeAddr' => 'Нижний Новгород,<br>ул. Максима Горького, 156',
		'discountPercent' => 'И АБСОЛЮТНУЮ НОВИНКУ В НИЖНЕМ НОВГОРОДЕ - ИТАЛЬЯНСКУЮ ДЕТСКУЮ ОБУВЬ ПРИМИДЖИ!',
		'discountPercentStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'Получить промокод на скидку',
		'salesEmail' => 'golubeva@julianna.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A2e3044ac3b2e8e2a63547768e4c02b0d6f41ffe394966556cc911d421e958471&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'ТОЛЬКО С 15 ПО 18 ИЮНЯ И ТОЛЬКО НА УЛ. ГОРЬКОГО, Д. 156',
		'caption' => 'ТОЛЬКО 4 ДНЯ СКИДКА 50% НА ДЕТСКУЮ ОРТОПЕДИЧЕСКУЮ ОБУВЬ ORTHOBOOM',
		'yaMetrikaCounterId' => '',
		'yaMetrikaCounter' => ''
	),
);

?>