<p><b>In order for your shoes to last long, it is necessary to follow the recommendations below for using your shoes:<br></b></p>
<p>
<ul>
	<li>When purchasing your shoes, make sure you pick the correct shoe size for your feet.</li>
	<li>During the fitting, the shoes must be comfortable and your feet should not be cramped.</li>
	<li>Only take off and put on the shoes once you have untied or unfastened them completely.</li>
	<li>Put them on with the help of a specialist shoe horn.</li>
	<li>In order to prevent misshaping the shoes and breaking the seams.</li>
	<li>Do not use new shoes for long journeys, as it takes some time for the shoes to take on the individual forms of the feet.</li>
	<li>Leather shoes are not intended for damp socks, wet weather, as they are not waterproof.</li>
	<li>While the feet are excessively sweating or wet, the colour of the shoes may run and stain the inside. This does not constitute a defect; dry the shoes daily, at room temperature and away from heating device.</li>
	<li>Before using any cleaning products remove dirt and dust from the shoes with a shoe brush, a damp cloth or a rubber brush.</li>
	<li>Look after shoes with leather exterior uppers with the help of a shoe-coloured or colourless cream, which will wash off clean, dry surfaces. Once the cream has damp weather, water-repellent cream should be used.</li>
	<li>Look after shoes with velour or nubuck uppers with the help of specialist aerosols, using a cleaning rubber or a wire brush. It is not recommended that you wear these types of shoes in damp weather.</li>
	<li>хIt is necessary to clean shoes with textile uppers with a brush and rub clean with a damp cloth.</li>
	<li>Keep your shoes in a well-ventilated or airy place.</li>
	<li>Do not wear your shoes with bare feet, except for summer shoes.</li>
	<li>You must not: wash shoes by leaving them in water, expose shoes to the effects of acid, alkali, solvents or effects of a mechanical nature (blows, cuts etc.)</li>
</ul>
</p>

<p style="text-align: justify;">The shoe's warranty period is set from the date the shoe is issued to the customer or from the beginning of the season and lasts 45 days.</p>
<p style="text-align: justify;">Throughout the warranty period you can make a claim relating to the quality of the shoes. Any quality claim made following non-compliance with the cleaning recommendations will not be accepted.</p>
<p style="text-align: justify;">The warranty period will not be extended to removable parts of the shoes, that are in need of small repairs: laces, printed cloth, zips and other accessories. Compliance with the cleaning recommendations will help you to protect the shoes from the negative effects of the environment and extend the life of the shoes.</p>


<p style="text-align: justify;">The shoe's warranty period is set from the date the shoe is issued to the customer or from the beginning of the season and lasts 45 days.</p>
<p style="text-align: justify;">Throughout the warranty period you can make a claim relating to the quality of the shoes. Any quality claim made following non-compliance with the cleaning recommendations will not be accepted.</p>
<p style="text-align: justify;">he warranty period will not be extended to removable parts of the shoes, that are in need of small repairs: laces, printed cloth, zips and other accessories. Compliance with the cleaning recommendations will help you to protect the shoes from the negative effects of the environment and extend the life of the shoes.</p>
