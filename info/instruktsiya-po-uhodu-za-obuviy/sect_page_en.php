<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle("Instructions for taking care of the shoes");
$APPLICATION->SetPageProperty('title', 'Instructions for taking care of the shoes "ORTHOBOOM"');
$APPLICATION->SetPageProperty('description', 'Instructions for taking care of the shoes. �ORTHOBOOM� - real shoes');
?>
		<div class="content">
			<div class="container">
				<h1 class="center">Instructions for taking care of the shoes</h1>
				<div class="span9">
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "info_en",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>	
				</div>
<script>
$(document).ready(function() {
	$('.span9').css('width', '100%');
});
</script>
<!--
				<div class="span3 file">
					<strong>Download instructions</strong>
					<p>
						<?$APPLICATION->IncludeComponent(
								"bitrix:main.include",
								"",
								Array(
									"AREA_FILE_SHOW" => "sect",
									"AREA_FILE_SUFFIX" => "instruct_en",
									"EDIT_TEMPLATE" => "",
									"AREA_FILE_RECURSIVE" => "Y"
								)
						);?>	

					</p>
				</div>
-->
			</div>
		</div>