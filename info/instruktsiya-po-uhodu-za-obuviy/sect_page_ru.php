<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle("Инструкция по уходу за обувью");
$APPLICATION->SetPageProperty('title', 'Инструкция по уходу за ортопедической обувью "ORTHOBOOM"');
$APPLICATION->SetPageProperty('description', 'Инструкция по уходу за обувью. «ORTHOBOOM» - правильная обувь');
?>
		<div class="content">
			<div class="container">
				<h1 class="center">Инструкция по уходу за обувью</h1>
				<div class="span9">
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "info_ru",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>	
				</div>
				<div class="span3 file">
					<strong>Скачать инструкцию</strong>
					<p>
						<?$APPLICATION->IncludeComponent(
								"bitrix:main.include",
								"",
								Array(
									"AREA_FILE_SHOW" => "sect",
									"AREA_FILE_SUFFIX" => "instruct_ru",
									"EDIT_TEMPLATE" => "",
									"AREA_FILE_RECURSIVE" => "Y"
								)
						);?>	

					</p>
				</div>
			</div>
		</div>