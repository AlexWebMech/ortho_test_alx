<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle("Preventive orthopaedic shoes");
$APPLICATION->SetPageProperty('title', 'Preventive orthopaedic shoes - what is it?');
$APPLICATION->SetPageProperty('description', 'Preventive orthopaedic shoes. �ORTHOBOOM� - real shoes');
?> 		
<div class="content"> 			
  <div class="container"> 				
    <h1 class="center">Preventive orthopaedic shoes</h1>
 
<!--  				
    <table> 					
      <tbody>
        <tr> 						<td style="vertical-align: top;"> 							
            <p style="text-align: justify;">In order to prevent flat feet, shoes with extended heel caps are needed. It is important to know that all children under the age of three have physiologically flat feet and this natural process shapes the feet.</p>
           							
            <p style="text-align: justify;">It is necessary to use preventive shoes from very young ages. For children that already walk, shoes with removable shell-shaping insoles are recommended.</p>
           							
            <p style="text-align: justify;">Preventive shoes prevent the development of foot abnormalities in children, allowing us to correct the arches of the foot in the early stages of flat feet, and do not hinder the natural development of the foot.</p>
           						</td> 						<td> 							<img style="margin-left: 20px; margin-bottom: 20px;" src="/images/info-prof-ortoped-obuv-2.jpg" width="400" height="248" alt="Preventive orthopaedic shoes" title="Preventive orthopaedic shoes"  /> 						</td> 					</tr>
       				</tbody>
    </table>
-->
            <p style="text-align: justify;">In order to prevent flat feet, shoes with extended heel caps are needed. It is important to know that all children under the age of three have physiologically flat feet and this natural process shapes the feet.</p>
           							
            <p style="text-align: justify;">It is necessary to use preventive shoes from very young ages. For children that already walk, shoes with removable shell-shaping insoles are recommended.</p>
           							
            <p style="text-align: justify;">Preventive shoes prevent the development of foot abnormalities in children, allowing us to correct the arches of the foot in the early stages of flat feet, and do not hinder the natural development of the foot.</p>   				
    <table> 					
      <tbody>
        <tr> 						<td> 							<img style="margin-right: 20px;" src="/images/info-prof-ortoped-obuv-1.jpg" width="261" height="350" alt="Preventive orthopaedic shoes" title="Preventive orthopaedic shoes"  /> 						</td> 						<td style="vertical-align: top;"> 							
            <p style="text-align: justify;">They permit the even distribution of weight across the child�s feet, promote correct placement of the heels and, correspondingly, correct formation of the feet, preventing, in that way, the development of flat feet in children. Soft edges on the ankle bone take pressure off the foot and make the shoes more comfortable.</p>
           						</td> 					</tr>
       				</tbody>
    </table>
   				
    <p style="text-align: center;"><b> 					<a href="/catalog/index.php?set_filter=y&arrFilter_27_2322626082=Y" >Click here to choose Orthoboom preventive shoes</a></b></p>
   			</div>
 		</div>