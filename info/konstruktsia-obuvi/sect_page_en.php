<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle("Shoe design");
$APPLICATION->SetPageProperty('title', 'Shoe design');
$APPLICATION->SetPageProperty('description', 'Shoe design. «ORTHOBOOM» - real shoes');
?>
		<div class="content">
			<div class="container">
				<h1 class="center">Shoe design</h1>
				<p style="text-align: justify;">Our shoes have unique orthopaedic details, which are conducive to even distribution of weight across the feet, and solid and reliable fasteners. In our shoes, your children will feel comfortable while walking.</p>
				<table>
					<tr>
						<td style="vertical-align: top;">
							<img style="margin-right: 20px; margin-bottom: 20px;" src="/images/info-konstr-obuvi-3.jpg" width="316" height="150" alt="Shoe design" title="Shoe design">
						</td>
						<td style="vertical-align: top;">
							<p style="text-align: justify;"><b>Removable shell-shаped insoles.</b> Replicate the anatomical form of the foot and supoort their external and internal aches. They prevent the formation of over-pronation and excessive supination. Removable shell-shaped insoles be easily and quickly changed to individually produced insoles.</p>
							<p style="text-align: justify;"><b>Soft collars.</b> Soften the contact of the skin with the edges of the shoes during movement and ensure comfortable fit.</p>
							<p style="text-align: justify;"><b>Extended or butterfly-shaped heel caps.</b> Heel caps up to the level of the Achilles tendon guarantee the correct position of the heel bone relative to the axes of the calf. The details of the soft collar soften the contact of the skin with the shoe during movement, creating a comfortable position for the foot and protecting against rubbing.</p>
						</td>
					</tr>
				</table>
<!--
				<table>
					<tr>
						<td style="vertical-align: top;">
							<p style="text-align: justify;"><b>Comfortable fasteners.</b> The fasteners (hook-and-loop, buckles, or laces) provide solid and reliable securing of the feet, large openings and make the shoes easy to put on, with the possibility of adjusting fit. This is important, so that the shoes do not restrict movement in the ankle joint and other joints and can prevent abnormal movements.</p>
							<p style="text-align: justify;"><b>Outsoles with inverted heels.</b> Preventive measure or treatment for feet that collapse inwards (development of valgus deformity). Thomas inverted heels help to correctly form gait, ensuring comfort while walking and proper distribution of weight.</p>
							<p style="text-align: justify;"><b>Segmented diagnostic outsoles.</b> The outsoles are made to be flexible, resilient, cushioned and anti-skid with depressions where the toes sit. A grooved surface and points on test areas of rubbing allow us to discover irregular weight distribution on the feet.</p>
						</td>
						<td style="vertical-align: top;">
							<img style="margin-left: 20px;" src="/images/info-konstr-obuvi-1.jpg" width="325" height="365" alt="Shoe design" title="Shoe design">
						</td>
					</tr>
				</table>
-->
<br />
							<p style="text-align: justify;"><b>Comfortable fasteners.</b> The fasteners (hook-and-loop, buckles, or laces) provide solid and reliable securing of the feet, large openings and make the shoes easy to put on, with the possibility of adjusting fit. This is important, so that the shoes do not restrict movement in the ankle joint and other joints and can prevent abnormal movements.</p>
							<p style="text-align: justify;"><b>Outsoles with inverted heels.</b> Preventive measure or treatment for feet that collapse inwards (development of valgus deformity). Thomas inverted heels help to correctly form gait, ensuring comfort while walking and proper distribution of weight.</p>
							<p style="text-align: justify;"><b>Segmented diagnostic outsoles.</b> The outsoles are made to be flexible, resilient, cushioned and anti-skid with depressions where the toes sit. A grooved surface and points on test areas of rubbing allow us to discover irregular weight distribution on the feet.</p>
						
				<br />
				<p style="text-align: center;"><b>DIAGNOSTIC OUTSOLES, IN WHICH YOU NOTICE RUBBING OF SOLE SEGMENTS</b></p>
				<table>
					<tr>
						<td style="vertical-align: top;">
							<p style="text-align: justify;">1 - there is a risk of the beginnings of flat feet.</p>
							<p style="text-align: justify;">2,3,4 – abnormalities are showing. They have flat feet in places (flat-valgus foot deformity, X-shaped knees, “Inverted” ankle joints and hyperpronation of the feet at the point of support (excess rotation of the feet at the point of support towards the inside and downwards - a prolapse of the arch of the foot).</p>
							<p style="text-align: justify;">4,5 - possible manifestation of cerebral palsy in a light form. You must consult a neurologist, with possible treatment of manual therapy with an osteopath.</p>
							<p style="text-align: justify;">5,6,7 – signs of abnormalities (equinovarus of the feet, pigeon toes, O-shaped knees, ankles). You must consult a qualified orthopaedist.</p>
							<p style="text-align: justify;">8 – in standard healthy children’s feet, abnormalities are not discovered. Orthopaedic shoes serve as a preventive measure.</p>
						</td>
						<td style="vertical-align: top;">
							<img style="margin-left: 20px;" src="/images/info-konstr-obuvi-2.jpg" width="116" height="250" alt="Shoe design" title="Shoe design">
						</td>
					</tr>
				</table>
				<!--
				<p style="text-align: center;"><b>
					<a href="/catalog/index.php?set_filter=y&arrFilter_27_2322626082=Y">Перейти к выбору профилактической обуви</a>&nbsp;&nbsp;&nbsp;
					<a href="/catalog/index.php?set_filter=y&arrFilter_27_3632373061=Y">Перейти к выбору ортопедической обуви</a>
				</b></p>
				-->
			</div>
		</div>