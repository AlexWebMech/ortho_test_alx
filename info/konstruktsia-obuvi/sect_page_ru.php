<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle("Конструкция обуви");
$APPLICATION->SetPageProperty('title', 'Конструкция детской ортопедической обуви');
$APPLICATION->SetPageProperty('description', 'Конструкция обуви. «ORTHOBOOM» - правильная обувь');
?>
		<div class="content">
			<div class="container">
				<h1 class="center">Конструкция обуви</h1>
				<p style="text-align: justify;">Наша обувь включает в себя уникальные ортопедические детали, способствующие равномерному распределению нагрузки на стопу, и плотной и надежной фиксации. В нашей обуви ваш малыш комфортно чувствует себя при ходьбе.</p>
				<table>
					<tr>
						<td style="vertical-align: top;">
							<img style="margin-right: 10px;" src="/images/info-konstr-obuvi-1.jpg" alt="Конструкция обуви" title="Конструкция обуви">
						</td>
						<td style="vertical-align: top; text-align: left;">
							<p style="text-align: justify;">
								<span style="color:#D60093;font-weight:bold;">СЪЕМНАЯ СВОДОФОРМИРУЮЩАЯ СТЕЛЬКА</span>
								<ul>
									<li>Поддерживает наружный и внутренний своды стопы</li>
									<li>Препятствует формированию поперечного плоскостопия</li>
									<li>Амортизирует ударные нагрузки</li>
									<li>Предусмотрена возможность замены на стельки  индивидуального изготовления</li>
								</ul>
							</p>
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td style="vertical-align: top;">
							<img style="margin-right: 10px;" src="/images/info-konstr-obuvi-3.jpg" alt="Конструкция обуви" title="Конструкция обуви">
						</td>
						<td style="vertical-align: top; text-align: left;">
							<p style="text-align: justify;">
								<span style="color:#D60093;font-weight:bold;">ЖЕСТКИЙ ЗАДНИК</span>
								<ul>
									<li>Стабилизирует стопу в правильном положении</li>
									<li>Продленное крыло устраняет нестабильность голеностопного сустава</li>
									<li>Улучшает механику переката стопы</li>
								</ul>
							</p>
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td style="vertical-align: top;">
							<img style="margin-right: 10px;" src="/images/info-konstr-obuvi-4.jpg" alt="Конструкция обуви" title="Конструкция обуви">
						</td>
						<td style="vertical-align: top; text-align: left;">
							<p style="text-align: justify;">
								<span style="color:#D60093;font-weight:bold;">ЖЕСТКИЙ БЕРЦ</span>
								<ul>
									<li>Надежно фиксирует пяточно-таранный и голеностопный суставы</li>
									<li>Запатентованная ортопедическая колодка с анатомическим профилем</li>
									<li>Правильное положение пяточной кости относительно оси голени</li>
								</ul>
							</p>
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td style="vertical-align: top;">
							<img style="margin-right: 10px;" src="/images/info-konstr-obuvi-5.jpg" alt="Конструкция обуви" title="Конструкция обуви">
						</td>
						<td style="vertical-align: top; text-align: left;">
							<p style="text-align: justify;">
								<span style="color:#D60093;font-weight:bold;">МЯГКИЙ КАНТ</span>
								<ul>
									<li>Обеспечивает мягкое касание элементов обуви с голенью</li>
									<li>Предотвращает натирания и дискомфортные ощущения при ходьбе</li>
								</ul>
							</p>
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td style="vertical-align: top;">
							<img style="margin-right: 10px;" src="/images/info-konstr-obuvi-6.jpg" alt="Конструкция обуви" title="Конструкция обуви">
						</td>
						<td style="vertical-align: top; text-align: left;">
							<p style="text-align: justify;">
								<span style="color:#D60093;font-weight:bold;">ЗАСТЕЖКИ (липучки, пряжки, шнурки)</span>
								<ul>
									<li>Плотная и надежная фиксация стопы ребенка</li>
									<li>Большое раскрытие</li>
									<li>Возможность корректировки фиксации обуви на стопе при различной полноте</li>
								</ul>
							</p>
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td style="vertical-align: top; width:400px;">
							<img style="margin-right: 10px;" src="/images/info-konstr-obuvi-7.jpg" alt="Конструкция обуви" title="Конструкция обуви">
						</td>
						<td style="vertical-align: top; text-align: left;">
							<p style="text-align: justify;">
								<span style="color:#D60093;font-weight:bold;">ЖЕСТКИЙ И ШИРОКИЙ ПОДНОСОК</span>
								<ul>
									<li>Защищает ножку при ходьбе</li>
									<li>Удобное и просторное размещение пальцев</li>
								</ul>
							</p>
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td style="vertical-align: top;">
							<img style="margin-right: 10px;" src="/images/info-konstr-obuvi-8.jpg" alt="Конструкция обуви" title="Конструкция обуви">
						</td>
						<td style="vertical-align: top; text-align: left;">
							<p style="text-align: justify;">
								<span style="color:#D60093;font-weight:bold;">ШИРОКАЯ РАСКРЫВАЕМОСТЬ</span>
								<ul>
									<li>Конструкция LowOpen</li>
									<li>Использование молнии в комбинации с другими застежками Ортобум</li>
								</ul>
							</p>
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td style="vertical-align: top; width:400px;">
							<img style="margin-right: 10px;" src="/images/info-konstr-obuvi-9.jpg" alt="Конструкция обуви" title="Конструкция обуви">
						</td>
						<td style="vertical-align: top; text-align: left;">
							<p style="text-align: justify;">
								<span style="color:#D60093;font-weight:bold;">ПОДОШВА</span>
								<ul>
									<li>Эластичная, гибкая, нескользящая</li>
									<li>Упругая, долговечная, амортизирующая</li>
									<li>С перекатом в носочно-пучковой части</li>
									<li>Изготовлена из высококачественных материалов</li>
									<li>С диагностической тест-системой</li>
									<li>С инверсионным каблуком Томаса</li>
									<li>С диагностической тест системой</li>
								</ul>
							</p>
						</td>
					</tr>
				</table>
				<br />
				<p style="text-align: center;"><b>СЕГМЕНТЫ, В КОТОРЫХ ЗАМЕЧЕНО СТИРАНИЕ ПОДОШВЫ</b></p>
				<table>
					<tr>
						<td style="vertical-align: top;">
							<p style="text-align: justify;">1 – есть риск начинающегося плоскостопия.</p>
							<p style="text-align: justify;">2,3,4 – выраженная патология. Имеет местоплоскостопие (плоско-вальгусная деформация стоп, Х-образность коленей, «выворот»голеностопных суставов и гиперпронация стоп вмомент опоры (избыточное вращение стопы вмомент опоры кнутри и вниз – опущение сводастоп).</p>
							<p style="text-align: justify;">4,5 - возможно проявление ДЦП в мягкойформе. Требуется консультация невролога,возможно обращение к мануальному терапевтуи остеопату.</p>
							<p style="text-align: justify;">5,6,7 – выраженная патология (эквино-варуснаястопа, косолапие, О-образность коленей, голеней и т.п.). Требуется консультация квалифицированного ортопеда.</p>
							<p style="text-align: justify;">8 – норма, стопы ребенка здоровы, патологий невыявлено. Ортопедическая обувь служит в профилактических целях.</p>
						</td>
						<td style="vertical-align: top;">
							<img style="margin-left: 20px;" src="/images/info-konstr-obuvi-2.jpg" width="116" height="250" alt="Конструкция обуви" title="Конструкция обуви">
						</td>
					</tr>
				</table>
				<!--
				<p style="text-align: center;"><b>
					<a href="/catalog/index.php?set_filter=y&arrFilter_27_2322626082=Y">Перейти к выбору профилактической обуви</a>&nbsp;&nbsp;&nbsp;
					<a href="/catalog/index.php?set_filter=y&arrFilter_27_3632373061=Y">Перейти к выбору ортопедической обуви</a>
				</b></p>
				-->
			</div>
		</div>