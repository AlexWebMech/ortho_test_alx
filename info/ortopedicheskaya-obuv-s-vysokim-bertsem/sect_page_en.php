<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle("Support orthopaedic shoes");
$APPLICATION->SetPageProperty('title', 'Support orthopaedic shoes - what is it?');
$APPLICATION->SetPageProperty('description', 'Support orthopaedic shoes. �ORTHOBOOM� - real shoes');
?> 		
<div class="content"> 			
  <div class="container"> 				
    <h1 class="center">Support orthopaedic shoes</h1>
 <!--  				
    <table> 					
      <tbody>
        <tr> 						<td style="vertical-align: top;"> 							
            <p style="text-align: justify;">Support orthopaedic shoes, constructed to relieve the Achilles tendon, are recommended for children with:</p>
           							
            <p style="text-align: justify;"> 								</p>
          
            <ul> 									
              <li>Flat-valgus, varus, equinus, equinovarus and poly-varus deformities (removable insoles may be substituted by individual insoles);
                <br />
              
                <br />
              </li>
             									
              <li>Infant cerebral palsy and lower limb paresis.
                <br />
              </li>
             								</ul>
           							
            <p></p>
           							
            <p style="text-align: justify;">Constant wear of ORTHOBOOM support shoes with high butterfly-shaped heel caps helps the correct development of joint surfaces, changes the mechanics of rolling the feet and prevents further foot deformities from developing.</p>
           						</td> 						<td> 							<img style="margin-left: 20px;" src="/images/info-ortoped-obuv-vys-bertsem-1.jpg" width="450" height="310" alt="Support orthopaedic shoes" title="Support orthopaedic shoes"  /> 						</td> 					</tr>
       				</tbody>
    </table>
-->
<p style="text-align: justify;">Support orthopaedic shoes, constructed to relieve the Achilles tendon, are recommended for children with:</p>
           							
            <p style="text-align: justify;"> 								</p>
          
            <ul> 									
              <li>Flat-valgus, varus, equinus, equinovarus and poly-varus deformities (removable insoles may be substituted by individual insoles);
                <br />
              
                <br />
              </li>
             									
              <li>Infant cerebral palsy and lower limb paresis.
                <br />
              </li>
             								</ul>
           							
            <p></p>
           							
            <p style="text-align: justify;">Constant wear of ORTHOBOOM support shoes with high butterfly-shaped heel caps helps the correct development of joint surfaces, changes the mechanics of rolling the feet and prevents further foot deformities from developing.</p>   				
    <p style="text-align: center;"><img src="/images/info-ortoped-obuv-vys-bertsem-2.jpg" width="391" height="361" alt="Support orthopaedic shoes" title="Support orthopaedic shoes"  /></p>
   				
    <p style="text-align: center;"><b> 					<a href="/catalog/index.php?set_filter=y&arrFilter_27_3632373061=Y" >Click here to choose Orthoboom suppport shoes</a> 				</b></p>
   			</div>
 		</div>