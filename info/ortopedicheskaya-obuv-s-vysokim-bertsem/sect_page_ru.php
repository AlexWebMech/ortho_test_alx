<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle("Ортопедическая обувь с высоким берцем");
$APPLICATION->SetPageProperty('title', 'Ортопедическая обувь с жёстким берцем - что это и зачем нужно');
$APPLICATION->SetPageProperty('description', 'Ортопедическая обувь с жёстким берцем. «ORTHOBOOM» - правильная обувь');
?> 		
<div class="content"> 			
  <div class="container"> 				
    <h1 class="center">Ортопедическая обувь с жёстким берцем</h1>
   				
    <table> 					
      <tbody>
        <tr> 						<td style="vertical-align: top;"> 							
            <p style="text-align: justify;">Обувь с жестким берцем, изготовленным с освобождением ахиллова сухожилия, рекомендуется детям при:</p>
           							
            <p style="text-align: justify;"> 								</p>
          
            <ul> 									
              <li>выраженной плоско-вальгусной, варусной,эквинусной, эквиноварусной и поливарусной деформации (съемная стелька супинатор может быть заменена на индивидуальную стельку);
                <br />
              
                <br />
              </li>
             									
              <li>ДЦП, парезах нижних конечностей.
                <br />
              </li>
             								</ul>
           							
            <p></p>
           							
            <p style="text-align: justify;">Постоянное ношение ортопедической обуви ORTHOBOOM с высокими жесткими берцами формирует правильное развитие суставных поверхностей, меняет механику переката стопы и препятствует дальнейшему развитию деформации стопы.</p>
			<p style="text-align: justify;">Детская ортопедическая обувь Orthoboom сертифицирована как ортопедическая обувь на подбор.</p>
           						</td> 						<td> 							<img style="margin-left: 20px;" src="/images/info-ortoped-obuv-vys-bertsem-1.jpg" alt="Ортопедическая обувь с жёстким берцем" title="Ортопедическая обувь с жёстким берцем"  /> 						</td> 					</tr>
       				</tbody>
    </table>

    <p style="text-align: center;"><img src="/images/info-ortoped-obuv-vys-bertsem-2.jpg" width="391" height="361" alt="Ортопедическая обувь с жёстким берцем" title="Ортопедическая обувь с жёстким берцем"  /></p>

    <p style="text-align: center;"><b> 					<a href="/catalog2/index.php?MUL_MODE=&set_filter=y&arrFilter_1594_3313885498=Y" >Перейти к выбору ортопедической обуви</a> 				</b></p>

   			</div>
 		</div>