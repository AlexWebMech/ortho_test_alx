<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle("Useful information");
$APPLICATION->SetPageProperty('title', 'Useful information ORTHOBOOM');
$APPLICATION->SetPageProperty('description', 'Useful information');
?><div class="content">
	<div class="container">
		<h1 class="center">Useful information</h1>
		<table class="center" cellpadding="20">
		<tbody>
		<tr>
			<td style="vertical-align:top;">
 <a class="info-button1" href="/info/profilakticheskaya-ortopedicheskaya-obuv/">Preventive orthopaedic shoes</a><br>
 <a class="info-button2" href="/info/ortopedicheskaya-obuv-s-vysokim-bertsem/">Support Orthopaedic shoes</a><br>
 <a class="info-button3" href="/info/konstruktsia-obuvi/">Shoe design</a><br>
                <?php /*
 <a class="info-button4" href="/info/tablitsa-razmerov/">Size chart</a><br>
 */?>
 <a class="info-button5" href="/info/instruktsiya-po-uhodu-za-obuviy/">Instructions for taking care of the shoes</a><br>
 <a class="info-button6" href="/info/wholesale-buyers/">Wholesale buyers</a>
 <a class="info-button1" style="margin-top: 20px;" href="/catalog-list/">Shoe catalogs</a>
 <a class="info-button2" style="margin-top: 20px;" href="/info/privacy-policy/">Privacy policy</a>
			</td>
			<td style="vertical-align:top; text-align:justify;">
				<div class="info-text">
					<p>
						 ORTHOBOOM children's orthopaedic shoes are developed together with leading doctors and orthopaedists, with the help of new technology. We are the first in Russia to be developing innovative orthopaedic shoes in anatomically-correct moulds, which provide the optimum conditions for the correct development of children’s feet of all ages.&nbsp;
					</p>
					<p>
						 ORTHOBOOM children's orthopaedic shoes are available in seasonal collections, developed by leading Russian, German and Italian designers.
					</p>
					<p>
						 ORTHOBOOM shoes have passed all clinical and toxicology tests and their effectiveness has been proven. The shoes are recommended for a wide use of medical and preventive purposes.
					</p>
					<p>
						 ORTHOBOOM means proven efficacy, correct technology, quality materials and maximum comfort!
					</p>
					<p>
						 We are always open to collaboration and we pay great attention to all your requests and suggestions.<br>
					</p>
				</div>
			</td>
		</tr>
		</tbody>
		</table>
<!--
		<p style="color: #777; font-size: 9px">
			 Владельцем ТМ Orthoboom является ЗАО "Юлианна".<br>
			 РЕКВИЗИТЫ: Закрытое акционерное общество «ЮЛИАННА»; Юридический адрес: 603087, г. Нижний Новгород, ул. Казанское шоссе, д.10/1, оф.144; ОГРН 1045207462860 ИНН 5260135961 КПП 526001001; Р/с 40702810042050004794; Волго-Вятский банк СБ РФ г. Н.Новгород; К/с 30101810900000000603 БИК 042202603; т./ф +7 (831) 257-66-00; Генеральный директор - Чернышова Елена Викторовна
		</p>
-->
	</div>
</div>
 <br>