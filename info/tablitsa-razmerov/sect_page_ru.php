<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle("Таблица размеров");
$APPLICATION->SetPageProperty('title', 'Таблица соответствия размеров обуви');
$APPLICATION->SetPageProperty('description', 'Таблица соответствия размеров. «ORTHOBOOM» - правильная обувь');
?>

		<div class="content">
			<div class="container">
				<h1 class="center">Таблица соответствия размеров</h1>
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "razmer_ru",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>		
			</div>
		</div>