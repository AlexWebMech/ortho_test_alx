<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$lang = GetLangReal();

$APPLICATION->SetTitle("Privacy Policy");
?>
<?if ($lang=='de'):?>
<h1 class="center">Informationen zum Datenschutz </h1>
<p>
	 Wir freuen uns, dass Sie unsere Website besuchen und bedanken uns f&#252;r Ihr Interesse! Der Schutz Ihrer Privatsph&#228;re bei der Nutzung unserer Website ist uns wichtig, bitte nehmen Sie daher die nachstehenden Informationen &#252;ber den Umgang mit Ihren Daten zur Kenntnis:
</p>
<p>
	 Verantwortlicher f&#252;r die Datenverarbeitung auf dieser Website im Sinne der Datenschutz-Grundverordnung (DSGVO) ist ORTHOBOOM GmbH, Lenn&#233;str. 3, 10785 Berlin, Deutschland, Tel.: 03031959516, E-Mail: <a href="mailto:support@orthoboom.de">support@orthoboom.de</a>. Der f&#252;r die Verarbeitung von personenbezogenen Daten Verantwortliche ist diejenige nat&#252;rliche oder juristische Person, die allein oder gemeinsam mit anderen &#252;ber die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten entscheidet.
</p>
<p>
	 (1) Aus Sicherheitsgr&#252;nden benutzt unsere Website eine SSL-Verschl&#252;sselung (Secure Sockets Layer). Damit werden &#252;bertragene Daten gesch&#252;tzt und k&#246;nnen nicht von Dritten gelesen werden. Sie k&#246;nnen eine erfolgreiche Verschl&#252;sselung daran erkennen, dass sich die Protokollbezeichnung im der Statusleiste des Browsers von �http://� in �https://� &#228;ndert und dass dort ein geschlossenes Schloss-Symbol sichtbar ist.
</p>
<p>
	 (2) Unsere Website erfasst mit jedem Aufruf durch eine betroffene Person oder ein automatisiertes System eine Reihe von allgemeinen Daten und Informationen. Diese allgemeinen Daten und Informationen werden in den Logfiles des Servers gespeichert. Erfasst werden k&#246;nnen die (1) verwendeten Browsertypen und Versionen, (2) das vom zugreifenden System verwendete Betriebssystem, (3) die Internetseite, von welcher ein zugreifendes System auf unsere Internetseite gelangt (sogenannte Referrer), (4) die Unterwebseiten, welche &#252;ber ein zugreifendes System auf unserer Internetseite angesteuert werden, (5) das Datum und die Uhrzeit eines Zugriffs auf die Internetseite, (6) eine Internet-Protokoll-Adresse (IP-Adresse), (7) der Internet-Service-Provider des zugreifenden Systems und (8) sonstige &#228;hnliche Daten und Informationen, die der Gefahrenabwehr im Falle von Angriffen auf unsere informationstechnologischen Systeme dienen. Die Verarbeitung erfolgt gem&#228;&#223; Art. 6 Abs. 1 lit. f DSGVO auf Basis unseres berechtigten Interesses an der Verbesserung der Stabilit&#228;t und Funktionalit&#228;t unserer Website. Eine Weitergabe oder anderweitige Verwendung der Daten findet nicht statt. Wir behalten uns allerdings vor, die Server-Logfiles nachtr&#228;glich zu &#252;berpr&#252;fen, sollten konkrete Anhaltspunkte auf eine rechtswidrige Nutzung hinweisen.
</p>
<p>
	 (3) Um den Besuch unserer Website attraktiv zu gestalten und die Nutzung bestimmter Funktionen zu erm&#246;glichen, verwenden wir auf verschiedenen Seiten sogenannte Cookies. Hierbei handelt es sich um kleine Textdateien, die auf Ihrem Endger&#228;t abgelegt werden. Einige der von uns verwendeten Cookies werden nach dem Ende der Browser-Sitzung, also nach Schlie&#223;en Ihres Browsers, wieder gel&#246;scht (sog. Sitzungs-Cookies). Andere Cookies verbleiben auf Ihrem Endger&#228;t und erm&#246;glichen uns oder unseren Partnerunternehmen, Ihren Browser beim n&#228;chsten Besuch wiederzuerkennen (persistente Cookies). Sie k&#246;nnen Ihren Browser so einstellen, dass Sie &#252;ber das Setzen von Cookies informiert werden und einzeln &#252;ber deren Annahme entscheiden oder die Annahme von Cookies f&#252;r bestimmte F&#228;lle oder generell ausschlie&#223;en. Dazu k&#246;nnen Sie in dem Hilfsmen&#252; des jeweiligen Browsers die Einstellungen ver&#228;ndert:
</p>
<p>
	 Internet Explorer: <a href="http://windows.microsoft.com/de-DE/windows-vista/Block-or-allow-cookies">http://windows.microsoft.com/de-DE/windows-vista/Block-or-allow-cookies</a>
</p>
<p>
	 Firefox: <a href="https://support.mozilla.org/de/kb/cookies-erlauben-und-ablehnen">https://support.mozilla.org/de/kb/cookies-erlauben-und-ablehnen</a>
</p>
<p>
	 Chrome: <a href="http://support.google.com/chrome/bin/answer.py?hl=de&hlrm=en&answer=95647">http://support.google.com/chrome/bin/answer.py?hl=de&amp;amp;hlrm=en&amp;amp;answer=95647</a>
</p>
<p>
	 Safari: <a href="https://support.apple.com/kb/ph21411?locale=de_DE">https://support.apple.com/kb/ph21411?locale=de_DE</a>
</p>
<p>
	 Opera: <a href="http://help.opera.com/Windows/10.20/de/cookies.html">http://help.opera.com/Windows/10.20/de/cookies.html</a>
</p>
<p>
	 Bei der Nichtannahme von Cookies kann die Funktionalit&#228;t unserer Website eingeschr&#228;nkt sein.
</p>
<p>
	 (4) Im Rahmen der Kontaktaufnahme mit uns (z.B. per Kontaktformular oder E-Mail) werden personenbezogene Daten erhoben. Welche Daten im Falle eines Kontaktformulars erhoben werden, ist aus dem jeweiligen Kontaktformular ersichtlich. Diese Daten werden ausschlie&#223;lich zum Zweck der Beantwortung Ihres Anliegens bzw. f&#252;r die Kontaktaufnahme und die damit verbundene technische Administration gespeichert und verwendet. Rechtsgrundlage f&#252;r die Verarbeitung der Daten ist unser berechtigtes Interesse an der Beantwortung Ihres Anliegens gem&#228;&#223; Art. 6 Abs. 1 lit. f DSGVO. Zielt Ihre Kontaktierung auf den Abschluss eines Vertrages ab, so ist zus&#228;tzliche Rechtsgrundlage f&#252;r die Verarbeitung Art. 6 Abs. 1 lit. b DSGVO. Ihre personenbezogenen Daten werden gel&#246;scht, sobald die Speicherung f&#252;r diesen Zweck nicht mehr erforderlich ist oder wir schr&#228;nken die Verarbeitung ein, falls gesetzliche Aufbewahrungspflichten bestehen. Rechtsgrundlage f&#252;r die Verarbeitung der Daten ist die Durchf&#252;hrung einer vorvertraglichen Ma&#223;nahme durch Ihre Anfrage gem. Art. 6 Abs. 1 lit. b DSGVO.
</p>
<p>
	 (5) Das geltende Datenschutzrecht gew&#228;hrt Ihnen gegen&#252;ber dem Verantwortlichen hinsichtlich der Verarbeitung Ihrer personenbezogenen Daten umfassende Betroffenenrechte (Auskunfts- und Interventionsrechte), &#252;ber die wir Sie nachstehend informieren:
</p>
<p>
	 - Auskunftsrecht gem&#228;&#223; Art. 15 DSGVO: Sie haben insbesondere ein Recht auf Auskunft &#252;ber Ihre von uns verarbeiteten personenbezogenen Daten, die Verarbeitungszwecke, die Kategorien der verarbeiteten personenbezogenen Daten, die Empf&#228;nger oder Kategorien von Empf&#228;ngern, gegen&#252;ber denen Ihre Daten offengelegt wurden oder werden, die geplante Speicherdauer bzw. die Kriterien f&#252;r die Festlegung der Speicherdauer, das Bestehen eines Rechts auf Berichtigung, L&#246;schung, Einschr&#228;nkung der Verarbeitung, Widerspruch gegen die Verarbeitung, Beschwerde bei einer Aufsichtsbeh&#246;rde, die Herkunft Ihrer Daten, wenn diese nicht durch uns bei Ihnen erhoben wurden, das Bestehen einer automatisierten Entscheidungsfindung einschlie&#223;lich Profiling und ggf. aussagekr&#228;ftige Informationen &#252;ber die involvierte Logik und die Sie betreffende Tragweite und die angestrebten Auswirkungen einer solchen Verarbeitung, sowie Ihr Recht auf Unterrichtung, welche Garantien gem&#228;&#223; Art. 46 DSGVO bei Weiterleitung Ihrer Daten in Drittl&#228;nder bestehen;
</p>
<p>
	 - Recht auf Berichtigung gem&#228;&#223; Art. 16 DSGVO: Sie haben ein Recht auf unverz&#252;gliche Berichtigung Sie betreffender unrichtiger Daten und/oder Vervollst&#228;ndigung Ihrer bei uns gespeicherten unvollst&#228;ndigen Daten;
</p>
<p>
	 - Recht auf L&#246;schung gem&#228;&#223; Art. 17 DSGVO: Sie haben das Recht, die L&#246;schung Ihrer personenbezogenen Daten bei Vorliegen der Voraussetzungen des Art. 17 Abs. 1 DSGVO zu verlangen. Dieses Recht besteht jedoch insbesondere dann nicht, wenn die Verarbeitung zur Aus&#252;bung des Rechts auf freie Meinungs&#228;u&#223;erung und Information, zur Erf&#252;llung einer rechtlichen Verpflichtung, aus Gr&#252;nden des &#246;ffentlichen Interesses oder zur Geltendmachung, Aus&#252;bung oder Verteidigung von Rechtsanspr&#252;chen erforderlich ist;
</p>
<p>
	 - Recht auf Einschr&#228;nkung der Verarbeitung gem&#228;&#223; Art. 18 DSGVO: Sie haben das Recht, die Einschr&#228;nkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen, solange die von Ihnen bestrittene Richtigkeit Ihrer Daten &#252;berpr&#252;ft wird, wenn Sie eine L&#246;schung Ihrer Daten wegen unzul&#228;ssiger Datenverarbeitung ablehnen und stattdessen die Einschr&#228;nkung der Verarbeitung Ihrer Daten verlangen, wenn Sie Ihre Daten zur Geltendmachung, Aus&#252;bung oder Verteidigung von Rechtsanspr&#252;chen ben&#246;tigen, nachdem wir diese Daten nach Zweckerreichung nicht mehr ben&#246;tigen oder wenn Sie Widerspruch aus Gr&#252;nden Ihrer besonderen Situation eingelegt haben, solange noch nicht feststeht, ob unsere berechtigten Gr&#252;nde &#252;berwiegen;
</p>
<p>
	 - Recht auf Unterrichtung gem&#228;&#223; Art. 19 DSGVO: Haben Sie das Recht auf Berichtigung, L&#246;schung oder Einschr&#228;nkung der Verarbeitung gegen&#252;ber dem Verantwortlichen geltend gemacht, ist dieser verpflichtet, allen Empf&#228;ngern, denen die Sie betreffenden personenbezogenen Daten offengelegt wurden, diese Berichtigung oder L&#246;schung der Daten oder Einschr&#228;nkung der Verarbeitung mitzuteilen, es sei denn, dies erweist sich als unm&#246;glich oder ist mit einem unverh&#228;ltnism&#228;&#223;igen Aufwand verbunden. Ihnen steht das Recht zu, &#252;ber diese Empf&#228;nger unterrichtet zu werden.
</p>
<p>
	 - Recht auf Daten&#252;bertragbarkeit gem&#228;&#223; Art. 20 DSGVO: Sie haben das Recht, Ihre personenbezogenen Daten, die Sie uns bereitgestellt haben, in einem strukturierten, g&#228;ngigen und maschinenlesebaren Format zu erhalten oder die &#220;bermittlung an einen anderen Verantwortlichen zu verlangen, soweit dies technisch machbar ist;
</p>
<p>
	 - Recht auf Widerruf erteilter Einwilligungen gem&#228;&#223; Art. 7 Abs. 3 DSGVO: Sie haben das Recht, eine einmal erteilte Einwilligung in die Verarbeitung von Daten jederzeit mit Wirkung f&#252;r die Zukunft zu widerrufen. Im Falle des Widerrufs werden wir die betroffenen Daten unverz&#252;glich l&#246;schen, sofern eine weitere Verarbeitung nicht auf eine Rechtsgrundlage zur einwilligungslosen Verarbeitung gest&#252;tzt werden kann. Durch den Widerruf der Einwilligung wird die Rechtm&#228;&#223;igkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung nicht ber&#252;hrt;
</p>
<p>
	 - Recht auf Beschwerde gem&#228;&#223; Art. 77 DSGVO: Wenn Sie der Ansicht sind, dass die Verarbeitung der Sie betreffenden personenbezogenen Daten gegen die DSGVO verst&#246;&#223;t, haben Sie - unbeschadet eines anderweitigen verwaltungsrechtlichen oder gerichtlichen Rechtsbehelfs - das Recht auf Beschwerde bei einer Aufsichtsbeh&#246;rde, insbesondere in dem Mitgliedstaat Ihres Aufenthaltsortes, Ihres Arbeitsplatzes oder des Ortes des mutma&#223;lichen Versto&#223;es.
</p>
<p>
	 &nbsp;
</p>
<p>
	 - Recht auf Widerspruch gem&#228;&#223; Art. 21 DSGVO: Sie haben zudem das Recht, jederzeit gegen die Verarbeitung Ihrer personenbezogenen Daten Widerspruch zu erheben, sofern ein Widerspruchsrecht gesetzlich vorgesehen ist. Im Falle eines wirksamen Widerrufs werden Ihre personenbezogenen Daten ebenfalls automatisch durch uns gel&#246;scht.
</p>
<p>
	 M&#246;chten Sie von Ihrem Widerrufs- oder Widerspruchsrecht Gebrauch machen, gen&#252;gt eine E-Mail an: <a href="mailto:support@orthoboom.de">support@orthoboom.de</a>
</p>
<p>
	 (6) Dauer der Speicherung personenbezogener Daten
</p>
<p>
	 Die personenbezogenen Daten der betroffenen Person werden gel&#246;scht, sobald der Zweck der Speicherung entf&#228;llt. Eine Speicherung kann dar&#252;ber hinaus erfolgen, wenn dies durch europ&#228;ische oder nationale Gesetze oder sonstigen Vorschriften, denen der Verantwortliche unterliegt, vorgesehen wurde. Eine Sperrung oder L&#246;schung der Daten erfolgt auch dann, wenn eine durch die genannten Vorschriften vorgeschriebene Speicherfrist abl&#228;uft, es sei denn, dass eine Erforderlichkeit zur weiteren Speicherung der Daten f&#252;r einen Vertragsabschluss oder eine Vertragserf&#252;llung besteht.
</p>
<p>
	 ORTHOBOOM GmbH <br>
	 Lenn&#233;str. 3, <br>
	 10785 Berlin<br>
	 Deutschland<br>
 <br>
	 Tel.: +49 30 31959516<br>
	 E-Mail: <a href="mailto:support@orthoboom.de">support@orthoboom.de</a><br>
 <br>
 <br>
	 Registergericht: Amtsgericht Charlottenburg<br>
	 Registernummer: HRB 180714 B<br>
 <br>
	 Gesch&#228;ftsf&#252;hrer: Svetlana Freund<br>
 <br>
	 Umsatzsteuer-Identifikationsnummer gem&#228;&#223; � 27 a Umsatzsteuergesetz: DE309833165
</p>
<p>
 <br>
	 Plattform der EU-Kommission zur Online-Streitbeilegung: <a href="http://ec.europa.eu/consumers/odr">http://ec.europa.eu/consumers/odr</a>
</p>
<p>
</p>
<p>
</p>
<p>
	 Wir sind zur Teilnahme an einem Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle weder verpflichtet noch bereit.�&nbsp;
</p>
<?endif?>
<?if ($lang=='en'):?>
<p>ORTHOBOOM GmbH<br> 
Lenn&#233;str. 3, <br> 
10785 Berlin<br> 
</p>
<p>
Telefone: +49 30 31959516<br> 
	E-mail: <a href="mailto:support@orthoboom.de">support@orthoboom.de</a><br> 
</p>
<p>
Register court: Amtsgericht Charlottenburg<br> 
Register number: HRB 180714 B<br> 
</p>
<p>
Managing Director: Svetlana Freund 
</p>
<p>VAT identification number according to � 27a of Value Added Tax Act:<br> 
DE309833165</p>

<p>Platform of the EU Commission regarding online dispute resolution:<br> 
<a href="https://ec.europa.eu/consumers/odr">https://ec.europa.eu/consumers/odr</a></p>

<p>We are neither obliged nor prepared to attend a dispute settlement procedure before an alternative dispute resolution entity.</p>

<?endif?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>