 <div class="opt-table">
<div class="span3 margin0 center">
<img src="/images/opt1.png">
</div>
<div class="span5 margin0">
<h3>ISO - 100% quality</h3> <p>State Standard Certificate ISO 9001-2011 (ISO 9001:2008)</p>
</div>
<div class="clearfix"></div>
<div class="hide">

<ul>
<li>Produced in accordance with universal quality standards.</li>
<li>ORTHOBOOM shoes have passed all clinical tests and their effectiveness has been proven.</li>
<li>They are recommended for a wide use of medical and preventive purposes.</li>
<li>Our orthopaedic shoes are manufactured according to anatomical lasts, which give optimum conditions for the correct development of children�s feet.</li>
</ul>

</div>
</div>
<table><tbody>
<tr><td class="span5">
<div class="opt-table">
<div class="span1 margin0">
<img src="/images/opt2.png">
</div>
<div class="span3 ">
<p><strong>There are benefits for wholesale buyers</strong></p>
</div>
<div class="clearfix"></div>
<div class="hide">

<ul>
<li>The ability to order shoes in any size, without being tied to a fixed cartons amount.</li>
<li>Extensive possibilities for collaboration for wholesale orders in the most profitable conditions.</li>
<li>We work individually with each buyer.</li>
<li>Our partners always get the most current collection of fashionable orthopaedic shoes, quickly and at a reasonable price.</li>
</ul>

</div>
</div>

</td><td class="span5">

<div class="opt-table">
<div class="span1 margin0">
<img src="/images/opt3.png">
</div>
<div class="span3 ">
<p style="margin-top:10px;"><strong>Broad range of styles and sizes</strong></p>
</div>
<div class="clearfix"></div>

<div class="hide">

<ul>
<li>Broad wholesale range of styles and sizes. We always help you with the choice of styles and sizes.</li>
<li>Real Orthoboom shoes are targeted at children of various ages.</li>
<li>We regularly renew and enlarge our collections of shoes.</li>
</ul>

</div>
</div></td></tr>


<tr><td class="span5">
<div class="opt-table">
<div class="span1 margin0">
<img src="/images/opt4.png">
</div>
<div class="span3 ">
<p style="margin-top:10px;"><strong>100% quality guaranteed</strong></p>
</div>
<div class="clearfix"></div>
<div class="hide">

<ul>
<li>We are the manufacturers, therefore we are certain of the quality of Orthoboom children�s orthopaedic shoes.</li>
<li>When manufacturing the shoes, we only use natural, high-quality materials, which allow the leather to breathe and are absolutely safe for children�s feet.</li>
</ul>

</div>
</div>

</td><td class="span5">

<div class="opt-table">
<div class="span1 margin0">
<img src="/images/opt5.png">
</div>
<div class="span3 "><br>
<p><strong>Flexible pricing conditions</strong></p>
</div>
<div class="clearfix"></div>
<div class="hide">

<ul>
<li>Flexible pricing conditions, convenient payments and quick delivery.</li>
<li>Bulk buying discounts are available.</li>
<li>You can pay for goods by bank transfer to any Russian bank that is convenient for you in accordance with the invoice.</li>
</ul>

</div>
</div></td></tr>

<tr><td class="span5">
<div class="opt-table">
<div class="span1 margin0">
<img src="/images/opt6.png">
</div>
<div class="span3 ">
<p><strong>POS advertising and informational materials</strong></p>
</div>
<div class="clearfix"></div>

<div class="hide">

<ul>
<li>We provide prepared advertising and informational materials: posters, booklets, catalogues. All materials are renewed when new shoe styles come in.</li>
<li>All advertising material is created by designers especially for the Orthoboom shoe brand.</li>
</ul>

</div>
</div>

</td><td class="span5">

<div class="opt-table">
<div class="span1 margin0">
<img src="/images/opt7.png">
</div>
<div class="span3 ">
<p style="margin-top:10px;"><strong>The company has been trading for more than 20 years</strong></p>
</div>
<div class="clearfix"></div>
<div class="hide">

<ul>
<li>The company has been a market leader in orthopaedics for more than 20 years.</li>
<li>Effective service and providing clients with products of guaranteed quality.</li>
<li>Delivery is at the most acceptable cost in the shortest possible time.</li>
</ul>

</div>

</div></td></tr>

</tbody></table>

<script>
  $(function(){
     $('.opt-table').hover(function(){
          $(this).find('.hide').show();
     },function(){
          $(this).find('.hide').hide();
     });
});
</script>