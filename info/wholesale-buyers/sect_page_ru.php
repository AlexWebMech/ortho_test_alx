<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetPageProperty('title', 'Оптовым покупателям');
$APPLICATION->SetPageProperty('description', 'Оптовым покупателям. «ORTHOBOOM» - правильная обувь');
?>
		<div class="content">
			<div class="container">
				<h1 class="center">Оптовым покупателям</h1>
		<div style="width:800px; margin: 0 auto;">
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "opt_ru",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>	
		</div>
		<div style="background: #D4ECC9; padding: 20px 0; ">
			<div style="width:800px; margin: 0 auto;">
						<?$APPLICATION->IncludeComponent(
								"bitrix:main.include",
								"",
								Array(
									"AREA_FILE_SHOW" => "sect",
									"AREA_FILE_SUFFIX" => "opt-table_ru",
									"EDIT_TEMPLATE" => "",
									"AREA_FILE_RECURSIVE" => "Y"
								)
						);?>	
			</div>
			
		</div>
<br><br><br>
				<div class="callback opt">

						<h2 class="center"><br>Оставить предложение о сотрудничестве</h2>

<?$APPLICATION->IncludeComponent(
	"altasib:feedback.form", 
	"template_common", 
	array(
		"IBLOCK_TYPE" => "feedback_structured",
		"IBLOCK_ID" => "8",
		"FORM_ID" => "2",
		"EVENT_TYPE" => "ALX_FEEDBACK_FORM",
		"PROPERTY_FIELDS" => array(
			0 => "FIO",
			1 => "TEL",
			2 => "COMPANY",
			3 => "EMAIL",
			4 => "FEEDBACK_TEXT",
		),
		"PROPERTY_FIELDS_REQUIRED" => array(
		),
		"NAME_ELEMENT" => "ALX_DATE",
		"BBC_MAIL" => "info@orthoboom.ru, feedback@julianna.ru",
		"MESSAGE_OK" => "Сообщение отправлено!",
		"CHECK_ERROR" => "Y",
		"ACTIVE_ELEMENT" => "Y",
		"USE_CAPTCHA" => "Y",
		"SEND_MAIL" => "N",
		"HIDE_FORM" => "Y",
		"USERMAIL_FROM" => "N",
		"SHOW_MESSAGE_LINK" => "Y",
		"REWIND_FORM" => "N",
		"WIDTH_FORM" => "50%",
		"SIZE_NAME" => "12px",
		"COLOR_NAME" => "#000000",
		"SIZE_HINT" => "10px",
		"COLOR_HINT" => "#000000",
		"SIZE_INPUT" => "12px",
		"COLOR_INPUT" => "#727272",
		"BACKCOLOR_ERROR" => "#ffffff",
		"COLOR_ERROR_TITLE" => "#A90000",
		"COLOR_ERROR" => "#8E8E8E",
		"IMG_ERROR" => "/upload/altasib.feedback.gif",
		"BORDER_RADIUS" => "3px",
		"COLOR_MESS_OK" => "#963258",
		"IMG_OK" => "/upload/altasib.feedback.ok.gif",
		"CATEGORY_SELECT_NAME" => "Выберите категорию",
		"SECTION_MAIL_ALL" => "",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"ALX_CHECK_NAME_LINK" => "N",
		"CAPTCHA_TYPE" => "default",
		"JQUERY_EN" => "jquery",
		"SECTION_MAIL9" => "",
		"SECTION_MAIL11" => "",
		"SECTION_MAIL10" => "",
		"SECTION_MAIL7" => "",
		"SECTION_MAIL8" => "",
		"COMPONENT_TEMPLATE" => "template_common",
		"PROPS_AUTOCOMPLETE_NAME" => array(
			0 => "FIO",
		),
		"PROPS_AUTOCOMPLETE_EMAIL" => array(
			0 => "EMAIL",
		),
		"PROPS_AUTOCOMPLETE_PERSONAL_PHONE" => array(
		),
		"MASKED_INPUT_PHONE" => array(
		),
		"LOCAL_REDIRECT_ENABLE" => "N",
		"ADD_LEAD" => "N"
	),
	false
);?>
				</div>
			</div>
		</div>