 <div class="opt-table">
<div class="span3 margin0 center">
<img src="/images/opt1.png">
</div>
<div class="span5 margin0">
<h3>ISO - 100% качество</h3> <p>Сертификат ГОСТ ISO 9001-2011 (ISO 9001:2008)</p>
</div>
<div class="clearfix"></div>
<div class="hide">

<ul><li>Производится в соответствии с мировыми стандартами качества.</li>
<li>Обувь ORTHOBOOM прошла все клинические испытания и ее эффективность доказана.</li>
<li>Рекомендована к широкому использованию с лечебной и профилактической целью.</li>
<li>Производится по ортопедическим колодкам с анатомическим профилем, которые создают оптимальные условия для правильного развития стоп детей.</li>
</ul>

</div>
</div>
<table><tbody>
<tr><td class="span5">
<div class="opt-table">
<div class="span1 margin0">
<img src="/images/opt2.png">
</div>
<div class="span3 ">
<p><strong>Выгодные условия <br>
сотрудничества <br>для оптовых покупателей</strong></p>
</div>
<div class="clearfix"></div>
<div class="hide">

<ul><li>Возможность заказа обуви в любых размерах, без привязки к фиксированным коробам.</li>
<li>Широкие возможности для сотрудничества при оптовых заказах на самых выгодных условиях.</li>
<li>Работаем индивидуально с каждым покупателем.</li>
<li>Наши партнеры всегда получают самые актуальные коллекции модной ортопедической обуви быстро и по доступным ценам. </li>
</ul>

</div>
</div>

</td><td class="span5">

<div class="opt-table">
<div class="span1 margin0">
<img src="/images/opt3.png">
</div>
<div class="span3 ">
<p style="margin-top:10px;"><strong>Широкий модельный<br>
и размерный ряд</strong></p>
</div>
<div class="clearfix"></div>

<div class="hide">

<ul><li>Широкий модельный и размерный ряды оптом. Мы всегда поможем Вам с выбором модели и размера.</li>
<li>Правильная обувь Orthoboom  рассчитана на детей разного возраста. </li>
<li>Мы регулярно обновляем и пополняем коллекции обуви.</li>
</ul>

</div>
</div></td></tr>


<tr><td class="span5">
<div class="opt-table">
<div class="span1 margin0">
<img src="/images/opt4.png">
</div>
<div class="span3 ">
<p style="margin-top:10px;"><strong>Уверенность<br>
в 100% качестве</strong></p>
</div>
<div class="clearfix"></div>
<div class="hide">

<ul><li>Мы являемся&nbsp;производителями, поэтому уверены в качестве детской ортопедической обуви Orthoboom.</li>
<li>При производстве обуви используются только натуральные высококачественные материалы, позволяющие коже дышать и абсолютно безопасные для детской стопы.</li>
</ul>

</div>
</div>

</td><td class="span5">

<div class="opt-table">
<div class="span1 margin0">
<img src="/images/opt5.png">
</div>
<div class="span3 "><br>
<p><strong>Гибкие ценовые условия</strong></p>
</div>
<div class="clearfix"></div>
<div class="hide">

<ul><li>Гибкие ценовые условия, удобство оплаты&nbsp;и быстрая доставка.</li>
<li>Условия оплаты и доставки обсуждаются с каждым оптовым клиентом индивидуально. </li>
<li>Вы можете оплатить товар банковским переводом в любом удобном для Вас банке РФ на основании выставленного счёта.</li>
</ul>

</div>
</div></td></tr>

<tr><td class="span5">
<div class="opt-table">
<div class="span1 margin0">
<img src="/images/opt6.png">
</div>
<div class="span3 ">
<p><strong>Рекламные<br>
и информационные<br>
pos-материалы</strong></p>
</div>
<div class="clearfix"></div>

<div class="hide">

<ul>
<li>Предоставляем готовые рекламные и информационные материалы: плакаты, буклеты, каталоги. Все материалы обновляются по мере поступления новых моделей обуви.</li>
<li>Все рекламные материалы разработаны дизайнерами специально для марки правильной обуви Orthoboom.</li>
</ul>

</div>
</div>

</td><td class="span5">

<div class="opt-table">
<div class="span1 margin0">
<img src="/images/opt7.png">
</div>
<div class="span3 ">
<p style="margin-top:10px;"><strong>Компания на рынке<br>
более 20 лет</strong></p>
</div>
<div class="clearfix"></div>
<div class="hide">

<ul>
<li>Компания является лидером ортопедической индустрии более 20 лет.</li>
<li>Эффективное обслуживание и обеспечение клиентов продукцией гарантированного качества.</li>
<li>Поставки по наиболее приемлемым ценам в минимальные сроки.</li>
</ul>

</div>

</div></td></tr>

</tbody></table>

<script>
  $(function(){
     $('.opt-table').hover(function(){
          $(this).find('.hide').show();
     },function(){
          $(this).find('.hide').hide();
     });
});
</script>