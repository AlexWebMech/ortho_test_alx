<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle("Orthopaedic advice");
$APPLICATION->SetPageProperty('title', 'Important to know');
$APPLICATION->SetPageProperty('description', 'Important to know. �ORTHOBOOM� - real shoes');
?>
		<div class="content">
			<div class="container"  style="background: url(../images/ortoped.jpg) right 95% no-repeat;" >

				<h1 class="center">Important to know</h1>
				<div class="span12">	
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "ortoped2_en",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>
					
				</div> 
				<div class="span6">
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "ortoped_en",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>
				</div>
			</div>
		</div>
