<p style="text-align: justify;">Every parent wants to see their children looking fine and healthy. Particular attention ought to be given to their musculoskeletal system. If the musculoskeletal organs of a child develop correctly, then they will grow up cheerful, healthy and active. Well-timed orthopaedic visits give the possibility of correcting both inherent and acquired abnormalities, allowing the prevention of the development of a whole series of musculoskeletal system diseases. It is essential that parents know, in any case, when they need to turn to the orthopaedist.
<br><br><b>If you notice your child has:</b><br></p>
<ul style="text-align: justify;">
	<li>quickly become tired while walking;</li>
	<li>complained of aches and pains in their lower limbs;</li>
	<li>changed their gait or posture;</li>
	<li>a restricted range of movement in their spine and</li>
	<li>joints of their limbs;</li>
	<li>flat feet;</li>
	<li>a deformed thorax, contractions or contortions of the arm, leg, neck and spine; pain in the neck, back, joints or muscles;</li>
	<li>if the child is older than 18 months, constantly falling while walking, barely walking and is unsteady;</li>
	<li>if there has previously been a trauma.</li>
</ul>
<p style="text-align: justify;"><b>In any of the cases above, we recommend you take your child to an orthopaedist!</b></p>