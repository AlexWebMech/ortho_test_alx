<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//$APPLICATION->SetTitle("Важно знать");
//$APPLICATION->SetPageProperty('title', 'Важно знать');
//$APPLICATION->SetPageProperty('description', 'Важно знать. «ORTHOBOOM» - правильная обувь');
?>
		<div class="content">
			<div class="container">
				<h1 class="center"><?$APPLICATION->ShowTitle(false);?></h1>
				
				<table class="center" cellpadding="20">
					<tr>
						<td style="vertical-align:top;">
							<a class="info-button1" href="/orthoped/kak-pravilno-vybrat-obuv-orthoboom">Как правильно выбрать обувь Orthoboom</a><br />
							<a class="info-button2" href="/orthoped/ploskostopie-u-detey-do-3-let">Плоскостопие у детей до 3 лет</a><br />
							<a class="info-button3" href="/orthoped/ploskostopie-u-detey-ot-3-do-12-let">Плоскостопие у детей от 3 до 12 лет</a><br />
							<a class="info-button4" href="/orthoped/valgusnaya-deformatsiya-stop">Вальгусная деформация стоп</a><br />
							<a class="info-button5" href="/orthoped/varusnaya-deformatsiya-stop">Варусная деформация стоп</a><br />
							<a class="info-button6" href="/orthoped/narusheniya-osanki">Нарушения осанки</a><br />
							<a class="info-button1" href="/orthoped/skolioz-i-iskrivleniya-pozvonochnika">Сколиоз и искривления позвоночника</a><br />
						</td>
						<td style="vertical-align:top; text-align:justify;">
<div class="info-text">
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "ortoped2_ru",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>
					
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "ortoped_ru",
								"EDIT_TEMPLATE" => "",
								"AREA_FILE_RECURSIVE" => "Y"
							)
					);?>
</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
		