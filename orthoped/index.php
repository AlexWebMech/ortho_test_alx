<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Важно знать");
$APPLICATION->SetPageProperty('title', 'Важно знать');
$APPLICATION->SetPageProperty('description', 'Важно знать. «ORTHOBOOM» - правильная обувь');

$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "page_".GetLang(),
		"EDIT_TEMPLATE" => ""
	)
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>