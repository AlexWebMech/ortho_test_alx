<?
require_once('config.php');

$logo = 'nn.jpg';
$logoWidth = '50%';
//$mainImg = 'pic_r.jpg';
$mainImg = 'pic_school.jpg';
$phoneLabel = '+7 (831) 461-83-44';
$phone = '+78314618344';
$workingHours = 'РµР¶РµРґРЅРµРІРЅРѕ СЃ 9.00-20.00';
$secondPic = 'primigi.jpg';
$secondPicText = ' ';
$officeAddr = '<p>Рі. РќРёР¶РЅРёР№ РќРѕРІРіРѕСЂРѕРґ,&nbsp;</p><p>СѓР». РђРґРјРёСЂР°Р»Р° РќР°С…РёРјРѕРІР°, Рґ.20</p>';
$discountPercent = 'Р”РђР Р�Рњ РћР РўРћРџР•Р”Р�Р§Р•РЎРљР�Р™ Р Р®РљР—РђРљ Р� 15% РЎРљР�Р”РљР� РџР Р� РџРћРљРЈРџРљР•';
$textStyle = '';
$promoButtonText = 'РџРѕР»СѓС‡РёС‚СЊ РїСЂРѕРјРѕРєРѕРґ';
$salesEmail = 'reklama@orthoboom.ru';
$yamapSrc = 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A13642bcfef779fbaf3bf04732fe4e46919aefa59e32cbcaec7912601167251cc&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true';
$salonNameAndSaleDate = 'Р”Рћ 31 Р�Р®Р›РЇ Р’ РћР РўРћРџР•Р”Р�Р§Р•РЎРљР�РҐ Р¦Р•РќРўР РђРҐ &quot;Р®Р›Р�РђРќРќРђ&quot;';
$caption = 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM Р’ РќР�Р–РќР•Рњ РќРћР’Р“РћР РћР”Р•';
$yaMetrikaCounterId ='44091509';
$yaMetrikaCounter = '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
     (function (d, w, c) {
         (w[c] = w[c] || []).push(function() {
             try {
                 w.yaCounter44091509 = new Ya.Metrika({
                     id:44091509,
                     clickmap:true,
                     trackLinks:true,
                     accurateTrackBounce:true
                 });
             } catch(e) { }
         });

         var n = d.getElementsByTagName("script")[0],
             s = d.createElement("script"),
             f = function () { n.parentNode.insertBefore(s, n); };
         s.type = "text/javascript";
         s.async = true;
         s.src = "https://mc.yandex.ru/metrika/watch.js";

         if (w.opera == "[object Opera]") {
             d.addEventListener("DOMContentLoaded", f, false);
         } else { f(); }
     })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44091509" 
style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->';
$gaCounter = "<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new 
Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
   ga('create', 'UA-64147781-2', 'auto');
   ga('send', 'pageview');
</script>
<!-- /Google Analytics -->";
$roistatCounter = "<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == 'https:' ? 'https://' : 'http://';
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? '/dist/module.js' : '/api/site/1.0/'+id+'/init';
    var js = d.createElement(s); js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, 'script', 'cloud.roistat.com', '186fe73a35d2cecf21a0772eb891c706');
</script>";

$uriParts = explode('?', $_SERVER['REQUEST_URI'], 2);
$arURI = explode('/', $uriParts[0]);
$city = trim(strtolower($arURI[2]));

if ($city) {
	if (!isset($arrInfo[$city])) {
		header('HTTP/1.0 404 Not Found');
		echo '<h1>404 - РЎС‚СЂР°РЅРёС†Р° РЅРµ РЅР°Р№РґРµРЅР°</h1>';
		echo 'Р—Р°РїСЂР°С€РёРІР°РµРјР°СЏ СЃС‚СЂР°РЅРёС†Р° РЅРµ РЅР°Р№РґРµРЅР° РЅР° СЃР°Р№С‚Рµ.';
		die;
	}
	$logo = $arrInfo[$city]['logo'];
	$logoWidth = $arrInfo[$city]['logoWidth'];
	$mainImg = $arrInfo[$city]['mainImg'];
	$phone = $arrInfo[$city]['phone'];
	$phoneLabel = $arrInfo[$city]['phoneLabel'];
	$workingHours = $arrInfo[$city]['workingHours'];
	if (isset($arrInfo[$city]['secondPic']))  $secondPic = $arrInfo[$city]['secondPic']; else $secondPic = '';
	if (isset($arrInfo[$city]['secondPicText'])) $secondPicText = $arrInfo[$city]['secondPicText']; else $secondPicText = '';
	$officeAddr = $arrInfo[$city]['officeAddr']; 
	$discountPercent = $arrInfo[$city]['discountPercent'];
	$textStyle = $arrInfo[$city]['textStyle'];
	$promoButtonText = $arrInfo[$city]['promoButtonText'];
	$salesEmail = $arrInfo[$city]['salesEmail'];
	$yamapSrc = $arrInfo[$city]['yamapSrc'];
	$salonNameAndSaleDate = $arrInfo[$city]['salonNameAndSaleDate'];
	$caption = $arrInfo[$city]['caption'];
	if (isset($arrInfo[$city]['yaMetrikaCounterId'])) $yaMetrikaCounterId = $arrInfo[$city]['yaMetrikaCounterId'];
	if (isset($arrInfo[$city]['yaMetrikaCounter'])) $yaMetrikaCounter = $arrInfo[$city]['yaMetrikaCounter'];
	if (isset($arrInfo[$city]['gaCounter'])) $gaCounter = $arrInfo[$city]['gaCounter'];
}

?>




<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title><?=$caption?></title>
	
	<meta name="keywords" content="РґРµС‚СЃРєР°СЏ РѕР±СѓРІСЊ: РґРµС‚СЃРєР°СЏ РѕСЂС‚РѕРїРµРґРёС‡РµСЃРєР°СЏ РѕР±СѓРІСЊ: РѕСЂС‚РѕРїРµРґ: РѕР±СѓРІСЊ: РєСѓРїРёС‚СЊ РґРµС‚СЃРєСѓСЋ РѕСЂС‚РѕРїРµРґРёС‡РµСЃРєСѓСЋ РѕР±СѓРІСЊ.">
	<meta name="description" content="Р”РµС‚СЃРєР°СЏ РѕСЂС‚РѕРїРµРґРёС‡РµСЃРєР°СЏ РѕР±СѓРІСЊ - РїСЂРѕС„РёР»Р°РєС‚РёРєР° РѕС‚ РїР»РѕСЃРєРѕСЃС‚РѕРїРёСЏ.">
	
	<link href='http://fonts.googleapis.com/css?family=Cuprum:400,400italic,700,700italic&subset=cyrillic' rel='stylesheet' type='text/css'>
	<link href="/sale/files/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<link href="/sale/files/semantic.min.css" rel="stylesheet" type="text/css">
	<link href="/sale/files/base.css" rel="stylesheet" type="text/css">
	<link href="/sale/files/my/style.css" rel="stylesheet" type="text/css">
	<link href="/sale/files/style.css" rel="stylesheet" type="text/css" />

	
	<script>
	    CSRF_token = '2ff5183d0ebc0f4fec755e9bbc76cebc34b9a47f6f97972fd09800bf7da4f178';
	    _PreviewMode = true;
	</script>
	
<?if (!$city):?>
<!--Google analytics-->
 <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-64147781-4', 'auto'); 
 ga('require', 'displayfeatures');
 ga('send', 'pageview');

 /* Accurate bounce rate by time */
 if (!document.referrer ||
 document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'РќРѕРІС‹Р№ РїРѕСЃРµС‚РёС‚РµР»СЊ', location.pathname);
 }, 15000);</script>

 <!--Google analytics-->

<!-- Google Code for &#1050;&#1083;&#1080;&#1082; 1 Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 858049286;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "jnpECJfZuHIQhpaTmQM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/858049286/?label=jnpECJfZuHIQhpaTmQM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?else:?>
<?=$gaCounter;?>
<?endif;?>


	<script type="text/javascript" src="/sale/files/jquery.js"></script>
	<script type="text/javascript" src="/sale/files/events.js"></script>
	<script type="text/javascript" src="/sale/files/jquery.tipsy.js"></script>

	<script type="text/javascript" src="/sale/files/production.min.js"></script>

	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
		
	<script type="text/javascript" src="/sale/files/my/script.js"></script>
	


			<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
	<!-- 
		tablet: bool(false)
		mobile: bool(true)
	 -->
	 
	<script>
	
	$(document).ready(function(){
	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
	});
	});
	
	
	
	
	
		Goals = {};
		Loading = {};
		Modules = {"ab_tests":0,"ssl":"0","animation":0,"mobile":true,"footer":"0","form_autofilling":"1","compressing":"1","language":"rus","multilanding":1,"multitext":1,"shopping_cart":1,"htmlwitget":1};
		
		Loading.show = function(text, className) {
		    if(typeof text == 'undefined') {
		        text = 'РџСЂРѕРёСЃС…РѕРґРёС‚ Р·Р°РіСЂСѓР·РєР° СЂРµРґР°РєС‚РѕСЂР° Рё РЅР°СЃС‚СЂРѕР№РєР° СЃС‚СЂР°РЅРёС†С‹.';
		    }  

		    className = (typeof className == 'undefined') ? 'blue' : className;

		    $('#editor-loader').show().attr('class', className);
		    $('#editor-loader').find('.text-replace').html(text);
		}

		Loading.hide = function(callback) {
		    callback = typeof callback == 'undefined' ? function() {} : callback;
		    $('#editor-loader').fadeOut(200, function() {
		        callback();
		    });
		}

		function publishPage() {
			Loading.show('РџСЂРѕРёСЃС…РѕРґРёС‚ РїСѓР±Р»РёРєР°С†РёСЏ РІР°С€РµР№ СЃС‚СЂР°РЅРёС†С‹ <br /> Р�Р·РѕР±СЂР°Р¶РµРЅРёСЏ Рё СЃС‚РёР»Рё СѓР¶РёРјР°СЋС‚СЃСЏ Рё РѕР±СЂРµР·Р°СЋС‚СЃСЏ...');

			$.ajax({
	            url: '/scr/publishPage',
	            type: 'post',
	            data: {
	                siteID: 30091	            },
	            error: function(jqXHR) {
	                alert(jqXHR.responseText);
	            },
	            success: function (data) {
	                Loading.hide();
	            }
	        });
		}
	</script>
	
<style>	
#s-11[data-index="0"] {
	background-image: url('/sale/files/<?=$mainImg?>');
	background-repeat: no-repeat;
	/*background-size: cover;*/
	background-position: 47% 42%;
	background-color: rgba(255,255,255,1);
	background-color: ;
	padding-top: 0px;
	padding-bottom: 0px;
	border-color: transparent;
	border-top-width: 20px;
	border-bottom-width: 20px;
	border-style: solid;
}
</style>
<?=$yaMetrikaCounter;?>
</head>

<!--<body id="landing-preview" class="s-editor preview-device-desktop  mobile loaded animate-complete" data-path="/upload/21013/30091/" data-site="30091" style="overflow: auto;">-->
<body class="s-editor preview-device-desktop  mobile loaded animate-complete">
<?=$roistatCounter;?>
<img id='Go_Top' class="img-responsive" src="/sale/files/up-arrow-icon-top.png" alt="РќР°РІРµСЂС…" title="РќР°РІРµСЂС…">	

	<div id="landing-content" class="desktop">
		
		<div class="s-grid vertical-align-top" id="s-443" data-id="443" data-index="0"><div class="s-grid-overlay"><div class="s-grid-wrapper" id="s-444" data-id="444" style="" data-index="0"><div class="s-row" id="s-445" data-id="445"><div class="s-col size-4" id="s-446" data-id="446"><div class="s-col-wrapper"><div class="s-row" id="s-447" data-id="447"><div class="s-col size-24" id="s-448" data-id="448"><div class="s-element" id="s-449" data-id="449"><div class="witget-image left"><div class="image"><img src="/sale/files/logos/<?=$logo?>" style="width:<?=$logoWidth?>;"></div></div></div></div></div></div></div><div class="s-col size-14" id="s-450" data-id="450"><div class="s-col-wrapper"><div class="s-row" id="s-451" data-id="451"><div class="s-col size-24" id="s-452" data-id="452"><div class="s-element" id="s-453" data-id="453"><div class="witget-menu center"><div class="button bars mobile-show" style="display: none;"><i class="fa fa-bars"></i></div><div class="button mobile-hide"><a href="#s-326">РљР°С‚Р°Р»РѕРі</a></div><div class="button mobile-hide"><a href="#s-495">РџСЂРµРёРјСѓС‰РµСЃС‚РІР°</a></div>
		
		<?if ($city != 'nn'):?>
		<div class="button mobile-hide"><a href="#s-468">РљРѕРЅСЃСѓР»СЊС‚Р°С†РёСЏ РІСЂР°С‡Р°</a></div>
		<?endif;?>
		
		
		<div class="button mobile-hide"><a href="#s-532">РђРґСЂРµСЃР° СЃР°Р»РѕРЅРѕРІ</a></div></div></div></div></div></div></div><div class="s-col size-6" id="s-454" data-id="454"><div class="s-col-wrapper"><div class="s-row" id="s-455" data-id="455"><div class="s-col size-24" id="s-456" data-id="456"><div class="s-element" id="s-457" data-id="457"><div class="witget-text"><p class="wysiwyg-text-align-right"><a href="javascript:void();" class="call_me" style="font-weight:bold;" onclick="<? if (!$city) echo 'ga(\'send\', \'pageview\', \'/virtualpage-callback-click\');'?> yaCounter<?=$yaMetrikaCounterId?>.reachGoal('button_call_me_clicked');">Р—Р°РєР°Р·Р°С‚СЊ РѕР±СЂР°С‚РЅС‹Р№ Р·РІРѕРЅРѕРє</a></p><h5 class="wysiwyg-text-align-right"><a href="tel:<?=$phone?>" style="text-decoration:none;" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('phone_clicked');"><?=$phoneLabel?></a></h5><p class="wysiwyg-text-align-right"><?=$workingHours?></p></div></div></div></div></div></div></div></div></div></div>
		
		<?if (!$city): ?>
			<div class="image-main" style="cursor: pointer;"><img src="/sale/files/<?=$mainImg?>" style="width: 100%; max-width: 100%;" alt="" title=""></div>
		<?elseif($city == 'perm' || $city == 'omsk' || $city == 'spb') :?>
			<div class="image-main" style="cursor: pointer;"><img src="/sale/files/<?=$mainImg?>" style="width: 100%; max-width: 100%;" alt="" title=""></div>
		<?else:?>
			<div class="s-grid vertical-align-top" id="s-11" data-id="11" data-index="0" data-height="1026"><div class="s-row" id="s-304" data-id="304"><div class="s-col size-24" id="s-309" data-id="309"><div class="s-col-wrapper"><div class="s-row" id="s-579" data-id="579"><div class="s-col size-24" id="s-580" data-id="580" style="margin-top:0;"><div class="s-element" id="s-308" data-id="308"><div class="witget-text"><h2 class="wysiwyg-text-align-center"><b><span style="color: rgb(255, 255, 255);"></span></b></h2><h2 class="wysiwyg-text-align-center"<?if($textStyle) echo ' style="'.$textStyle.'"';?>><span style="color: rgb(255, 255, 255);"><b><?=$caption?></b></span></h2><span style="color: rgb(255, 255, 255);"><br></span></div></div></div></div></div></div></div><div class="s-row" id="s-26" data-id="26"><div class="s-col size-24" id="s-27" data-id="27"><div class="s-col-wrapper"><div class="s-row" id="s-505" data-id="505"><div class="s-col size-24" id="s-506" data-id="506"><div class="s-element" id="s-33" data-id="33"><div class="witget-text"><h4 class="wysiwyg-text-align-center"><span style="color: rgb(231, 76, 60);"><b><br></b></span></h4><p><span style="color: rgb(231, 76, 60);"><b><br></b></span></p><p><span style="color: rgb(231, 76, 60);"><b><br></b></span></p><p><span style="color: rgb(231, 76, 60);"><b><br><br></b></span></p><p><span style="color: rgb(231, 76, 60);"><b><br><br></b></span></p>
		
			<div style="margin-top: 300px;"></div>

			<h2 class="wysiwyg-text-align-center"<?if($textStyle) echo ' style="'.$textStyle.'"';?>><b><span style="font-size: 55px;"><?=$discountPercent?></span></b></h2><h3 class="wysiwyg-text-align-center"><b></b></h3><h6 class="wysiwyg-text-align-center"><b><br></b></h6><h4 class="wysiwyg-text-align-center"<?if($textStyle) echo ' style="'.$textStyle.'"';?>><b><?=$salonNameAndSaleDate;?></b></h4><h5 class="wysiwyg-text-align-center"><br></h5><p class="wysiwyg-text-align-center"><br></p><p class="wysiwyg-text-align-center"><br></p><p class="wysiwyg-text-align-center"><br></p><p class="wysiwyg-text-align-center"><br></p><p class="wysiwyg-text-align-center"><br></p><p class="wysiwyg-text-align-center"><br></p><h5><br></h5><br></div></div></div>&nbsp;</div><div class="s-row" id="s-577" data-id="577"></div></div></div></div><div class="clear"></div></div>		
		<?endif;?>
		
		
				
		<?if(!empty($secondPic)):?>
		<!--<div class="image" style="margin-top: 10px;"><img src="/sale/files/<?=$secondPic?>" style="width: 70%; max-width: 70%;" alt="" title=""><h5><?=$secondPicText?></h5></div>-->
		<?endif;?>
		
		<div class="s-grid vertical-align-top" id="s-106" data-id="106" data-index="0"><div class="s-grid-overlay"><div class="s-grid-wrapper" id="s-107" data-id="107" style="" data-index="0">
		
		<div class="s-row" id="s-4943" data-id="4943"><div class="s-col size-24" id="s-578" data-id="578"><div class="s-element" id="s-485" data-id="485" style="margin-top:20px;"><div class="witget-button center"><div class="button button_coupon" onclick="<? if (!$city) echo 'ga(\'send\', \'pageview\', \'/virtualpage-getpromocode-click\');'?> yaCounter<?=$yaMetrikaCounterId?>.reachGoal('button_coupon_clicked');"><i class="fa fa-arrow-circle-o-right"></i><span><?=$promoButtonText?></span></div></div></div></div></div>
		
		<div class="s-row" id="s-458" data-id="458"><div class="s-col size-24" id="s-459" data-id="459"><div class="s-col-wrapper"><div class="s-row" id="s-460" data-id="460"><div class="s-col size-24" id="s-461" data-id="461"><div class="s-element" id="s-462" data-id="462"><div class="witget-hr">
	<div class="hr empty"></div></div></div></div></div></div></div></div>
		
		<div class="s-row" id="s-494" data-id="494"><div class="s-col size-24" id="s-495" data-id="495"><div class="s-col-wrapper"><div class="s-row" id="s-496" data-id="496"><div class="s-col size-24" id="s-497" data-id="497"><div class="s-element" id="s-498" data-id="498"><div class="witget-text"><h3 class="wysiwyg-text-align-center"><b><span style="color: rgb(0, 0, 0);">РџР Р•Р�РњРЈР©Р•РЎРўР’Рђ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРћР™ РћР‘РЈР’Р� ORTHOBOOM</span></b></h3></div></div></div></div></div></div></div>

<div class="s-row" id="s-113" data-id="113"><div class="s-col size-6" id="s-114" data-id="114"><div class="s-col-wrapper"><div class="s-row" id="s-115" data-id="115"><div class="s-col size-24" id="s-116" data-id="116"><div class="s-element" id="s-117" data-id="117"><div class="witget-text"><h5 class="wysiwyg-text-align-center">РљРѕРјС„РѕСЂС‚</h5>п»ї<br></div></div></div></div><div class="s-row" id="s-118" data-id="118"><div class="s-col size-24" id="s-119" data-id="119"><div class="s-element" id="s-120" data-id="120"><div class="witget-image strech center"><div class="image" style="height: 70px;"><img src="/sale/files/cf5d6e165a.png" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-121" data-id="121"><div class="s-col size-24" id="s-122" data-id="122"><div class="s-element" id="s-123" data-id="123"><div class="witget-text"><p class="wysiwyg-text-align-center">РњС‹ РїРѕР·Р°Р±РѕС‚РёР»РёСЃСЊ Рѕ РєРѕРјС„РѕСЂС‚Рµ РґРµС‚СЃРєРѕР№ РЅРѕР¶РєРё, РїРѕСЌС‚РѕРјСѓ РѕР±СѓРІСЊ ORTHOBOOM СЃРґРµР»Р°РЅР° РёР· РЅР°С‚СѓСЂР°Р»СЊРЅС‹С… РјР°С‚РµСЂРёР°Р»РѕРІ - РєРѕР¶Рё Рё РЅСѓР±СѓРєР°, РїРѕР·РІРѕР»СЏСЋС‰РёРµ РЅРѕР¶РєРµ РґС‹С€Р°С‚СЊ.</p></div></div></div></div></div></div><div class="s-col size-6" id="s-124" data-id="124"><div class="s-col-wrapper"><div class="s-row" id="s-125" data-id="125"><div class="s-col size-24" id="s-126" data-id="126"><div class="s-element" id="s-127" data-id="127"><div class="witget-text"><h5 class="wysiwyg-text-align-center">Р”РёР·Р°Р№РЅ</h5>п»ї<br></div></div></div></div><div class="s-row" id="s-128" data-id="128"><div class="s-col size-24" id="s-129" data-id="129"><div class="s-element" id="s-130" data-id="130"><div class="witget-image strech center"><div class="image" style="height: 70px;"><img src="/sale/files/37a1039f86.png" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-131" data-id="131"><div class="s-col size-24" id="s-132" data-id="132"><div class="s-element" id="s-133" data-id="133"><div class="witget-text"><p class="wysiwyg-text-align-center">РњС‹ Р·Р°С…РѕС‚РµР»Рё СЃРѕР·РґР°С‚СЊ РїРѕ РЅР°СЃС‚РѕСЏС‰РµРјСѓ РєСЂР°СЃРёРІСѓСЋ РѕР±СѓРІСЊ РґР»СЏ РґРµС‚РµР№ Рё РїРѕСЌС‚РѕРјСѓ РѕР±СЂР°С‚РёР»РёСЃСЊ Рє РёС‚Р°Р»СЊСЏРЅСЃРєРёРј РґРёР·Р°Р№РЅРµСЂР°Рј.</p><p class="wysiwyg-text-align-center">РЎРµР№С‡Р°СЃ РІ Р°СЃСЃРѕСЂС‚РёРјРµРЅС‚Рµ Р±РѕР»РµРµ 200 РјРѕРґРµР»РµР№.</p></div></div></div></div></div></div><div class="s-col size-6" id="s-134" data-id="134"><div class="s-col-wrapper"><div class="s-row" id="s-135" data-id="135"><div class="s-col size-24" id="s-136" data-id="136"><div class="s-element" id="s-137" data-id="137"><div class="witget-text"><h5 class="wysiwyg-text-align-center" style="white-space: nowrap;">Р РµРєРѕРјРµРЅРґР°С†РёРё РІСЂР°С‡РµР№<br><br></h5></div></div></div></div><div class="s-row" id="s-138" data-id="138"><div class="s-col size-24" id="s-139" data-id="139"><div class="s-element" id="s-140" data-id="140"><div class="witget-image strech center"><div class="image" style="height: 70px;"><img src="/sale/files/76ad7b696f.png" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-141" data-id="141"><div class="s-col size-24" id="s-142" data-id="142"><div class="s-element" id="s-143" data-id="143"><div class="witget-text"><p class="wysiwyg-text-align-center">РћР±СѓРІСЊ ORTHOBOOM СЃРґРµР»Р°РЅР° РїРѕ СЂРµРєРѕРјРµРЅРґР°С†РёСЏРј РґРµС‚СЃРєРёС… РІСЂР°С‡РµР№ РќРџР¦ РёРј. РђР»СЊР±СЂРµС…С‚Р° Рі. РЎР°РЅРєС‚ РџРµС‚РµСЂР±СѓСЂРі Рё РќРёР¶РµРіРѕСЂРѕРґСЃРєРѕР№ РјРµРґРёС†РёРЅСЃРєРѕР№ Р°РєР°РґРµРјРёРё.</p></div></div></div></div></div></div><div class="s-col size-6" id="s-158" data-id="158"><div class="s-col-wrapper"><div class="s-row" id="s-159" data-id="159"><div class="s-col size-24" id="s-160" data-id="160"><div class="s-element" id="s-161" data-id="161"><div class="witget-text"><h5 class="wysiwyg-text-align-center">Р�РЅРЅРѕРІР°С†РёРё</h5>п»ї<br></div></div></div></div><div class="s-row" id="s-162" data-id="162"><div class="s-col size-24" id="s-163" data-id="163"><div class="s-element" id="s-164" data-id="164"><div class="witget-image strech center"><div class="image" style="height: 70px;"><img src="/sale/files/4c1c07bb4a.png" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-165" data-id="165"><div class="s-col size-24" id="s-166" data-id="166"><div class="s-element" id="s-167" data-id="167"><div class="witget-text"><p class="wysiwyg-text-align-center">РњС‹ СЂР°Р·СЂР°Р±РѕС‚Р°Р»Рё РѕР±СѓРІСЊ РЅР° Р±Р°Р·Рµ РѕСЂС‚РѕРїРµРґРёС‡РµСЃРєРѕР№ РєРѕР»РѕРґРєРё РїСЂР°РІРёР»СЊРЅРѕР№ Р°РЅР°С‚РѕРјРёС‡РµСЃРєРѕР№ С„РѕСЂРјС‹, РєРѕС‚РѕСЂР°СЏ СЃРѕР·РґР°РµС‚ РѕРїС‚РёРјР°Р»СЊРЅС‹Рµ СѓСЃР»РѕРІРёСЏ РґР»СЏ СЂР°Р·РІРёС‚РёСЏ РґРµС‚СЃРєРѕР№ СЃС‚РѕРїС‹.</p></div></div></div></div></div></div>

<div class="s-col size-6" id="s-114" data-id="114"><div class="s-col-wrapper"><div class="s-row" id="s-115" data-id="115"><div class="s-col size-24" id="s-116" data-id="116"><div class="s-element" id="s-117" data-id="117"><div class="witget-text"><h5 class="wysiwyg-text-align-center">РўРµСЃС‚-СЃРёСЃС‚РµРјР°</h5>п»ї<br></div></div></div></div><div class="s-row" id="s-118" data-id="118"><div class="s-col size-24" id="s-119" data-id="119"><div class="s-element" id="s-120" data-id="120"><div class="witget-image strech center"><div class="image" style="height: 70px;"><img src="/sale/files/podohva-komponenti.jpg" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-121" data-id="121"><div class="s-col size-24" id="s-122" data-id="122"><div class="s-element" id="s-123" data-id="123"><div class="witget-text"><p class="wysiwyg-text-align-center">РћР±СѓРІСЊ ORTHOBOOM  РёРјРµРµС‚ РЅР° РїРѕРґРѕС€РІРµ РґРёР°РіРЅРѕСЃС‚РёС‡РµСЃРєСѓСЋ С‚РµСЃС‚-СЃРёСЃС‚РµРјСѓ РґР»СЏ РІС‹СЏРІР»РµРЅРёСЏ РїСЂРѕР±Р»РµРј СЂР°Р·РІРёС‚РёСЏ СЃС‚РѕРїС‹ СЂРµР±РµРЅРєР° РЅР° СЂР°РЅРЅРµР№ СЃС‚Р°РґРёРё.</p></div></div></div></div></div></div>

</div>

<?if ($city != 'nn'):?>
<div class="s-row" id="s-4681" data-id="4681"><div class="s-col size-24" id="s-473" data-id="473"><div class="s-col-wrapper"><div class="s-row" id="s-474" data-id="474"><div class="s-col size-24" id="s-475" data-id="475"><div class="s-element" id="s-476" data-id="476"><div class="witget-button center"><div class="button button_getconsultpodbor" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('button_podbor_clicked');"><i class="fa fa-arrow-circle-o-right"></i><span style="font-size:17px;">РљРѕРЅСЃСѓР»СЊС‚Р°С†РёСЏ РїРѕ РїРѕРґР±РѕСЂСѓ</span></div></div></div></div></div></div></div></div>
<?endif;?>
		
		
		
		
		
		
		<div class="s-row" id="s-458" data-id="458"><div class="s-col size-24" id="s-459" data-id="459"><div class="s-col-wrapper"><div class="s-row" id="s-460" data-id="460"><div class="s-col size-24" id="s-461" data-id="461"><div class="s-element" id="s-462" data-id="462"><div class="witget-hr">
	<div class="hr empty"></div>
</div></div></div></div></div></div></div><div class="s-row" id="s-562" data-id="562"><div class="s-col size-24" id="s-567" data-id="567"><div class="s-col-wrapper"><div class="s-row" id="s-568" data-id="568"><div class="s-col size-24" id="s-569" data-id="569"><div class="s-element" id="s-570" data-id="570"><div class="witget-text"><h5><b><br></b></h5><h5 class="wysiwyg-text-align-center"><span style="color: rgb(0, 0, 0);"><b>РћР‘РЈР’Р¬ ORTHOBOOM РЎРќРђР‘Р–Р•РќРђ Р­Р›Р•РњР•РќРўРђРњР� РћР РўРћРџР•Р”Р�Р� Р� РџР Р•Р”РќРђР—РќРђР§Р•РќРђ</b></span></h5><h5 class="wysiwyg-text-align-center"><span style="color: rgb(0, 0, 0);"><b>Р”Р›РЇ РџР РћР¤Р�Р›РђРљРўР�РљР� Р� Р›Р•Р§Р•РќР�РЇ Р”Р•РўРЎРљРћР“Рћ РџР›РћРЎРљРћРЎРўРћРџР�РЇ</b></span></h5><h5 class="wysiwyg-text-align-center"><b></b></h5><h6 class="wysiwyg-text-align-center"><b></b></h6><h4 class="wysiwyg-text-align-center"><b></b></h4><h3 class="wysiwyg-text-align-center"><b></b></h3></div></div></div></div></div></div></div>

<!--<div class="s-row" id="s-108" data-id="108"><div class="s-col size-24" id="s-109" data-id="109"><div class="s-col-wrapper"><div class="s-row" id="s-405" data-id="405"><div class="s-col size-24" id="s-406" data-id="406"><div class="s-element" id="s-407" data-id="407"><div class="witget-image center"><div class="image"><img src="files/de15f11028.jpg" style="width: 90%; max-width: 90%;" alt="" title=""></div></div></div></div></div></div></div></div>-->
<div class="image"><img id="botinok" src="/sale/files/de15f11028.jpg" style="width: 60%; max-width: 60%;" alt="" title=""></div>

<?if ($city != 'nn'):?>
<div class="s-row" id="s-468" data-id="468"><div class="s-col size-24" id="s-473" data-id="473"><div class="s-col-wrapper"><div class="s-row" id="s-474" data-id="474"><div class="s-col size-24" id="s-475" data-id="475"><div class="s-element" id="s-476" data-id="476"><div class="witget-button center"><div class="button <?if($city=='irkutsk') echo 'button_getconsultpodbor'; else echo 'button_getconsult';?>" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('button_orthoped_clicked');"><i class="fa fa-arrow-circle-o-right"></i><span style="font-size:17px;"><?if($city=='irkutsk') echo 'РљРѕРЅСЃСѓР»СЊС‚Р°С†РёСЏ РїРѕ РїРѕРґР±РѕСЂСѓ'; else echo 'РљРѕРЅСЃСѓР»СЊС‚Р°С†РёСЏ РѕСЂС‚РѕРїРµРґР°';?></span></div></div></div></div></div></div></div></div>
<?endif;?>

<div class="s-row" id="s-557" data-id="557"><div class="s-col size-24" id="s-558" data-id="558"><div class="s-col-wrapper"><div class="s-row" id="s-559" data-id="559"><div class="s-col size-24" id="s-560" data-id="560"><div class="s-element" id="s-501" data-id="501"><div class="witget-hr">
	<div class="hr empty"></div>
</div></div></div></div></div></div></div>


<div class="s-row" id="s-494" data-id="494"><div class="s-col size-24" id="s-4951" data-id="4951"><div class="s-col-wrapper"><div class="s-row" id="s-496" data-id="496"><div class="s-col size-24" id="s-497" data-id="497"><div class="s-element" id="s-498" data-id="498"><div class="witget-text"><h3 class="wysiwyg-text-align-center"><b><span style="color: rgb(0, 0, 0);">РћР‘РЈР’Р¬ ORTHOBOOM </span></b></h3></div></div></div></div></div></div></div>
		
<div class="s-row" id="s-289" data-id="289"><div class="s-col size-24" id="s-290" data-id="290"><div class="s-col-wrapper"><div class="s-row" id="s-291" data-id="291"><div class="s-col size-24" id="s-292" data-id="292"><div class="s-element" id="s-293" data-id="293"><div class="witget-video">
<div style="position:relative;height:0;padding-bottom:20%"><iframe width="560" height="315" src="https://www.youtube.com/embed/l0IvPGeQn-I" frameborder="0" allowfullscreen></iframe></div>
<!--<iframe class="video-iframe capture" src="files/qQGd9KPvKR0.html" frameborder="0" allowfullscreen="" style="height: 400px; width: 711.1111111111111px; max-width: 100%;"></iframe>-->
</div></div></div></div></div></div></div>


<div class="s-row" id="s-550" data-id="550"><div class="s-col size-24" id="s-551" data-id="551"><div class="s-col-wrapper"><div class="s-row" id="s-552" data-id="552"><div class="s-col size-24" id="s-553" data-id="553"><div class="s-element" id="s-526" data-id="526"><div class="witget-hr">
	<div class="hr empty"></div>
</div></div></div></div></div></div></div></div></div></div>


<div class="s-grid vertical-align-top" id="s-325" data-id="325" data-index="0"><div class="s-grid-overlay"><div class="s-grid-wrapper" id="s-326" data-id="326" style="" data-index="0"><div class="s-row" id="s-327" data-id="327"><div class="s-col size-24" id="s-328" data-id="328"><div class="s-col-wrapper"><div class="s-row" id="s-329" data-id="329"><div class="s-col size-24" id="s-330" data-id="330"><div class="s-element" id="s-331" data-id="331"><div class="witget-text"><h3 class="wysiwyg-text-align-center"><b><span style="color: rgb(0, 0, 0);">РљРђРўРђР›РћР“ РћР‘РЈР’Р�</span></b></h3></div></div></div></div></div></div></div>


<?if ($city != 'vladivostok'): ?>
<div class="s-row" id="s-332" data-id="332">

<div class="s-col size-8" id="s-333" data-id="333"><div class="s-col-wrapper">

<div class="s-row" id="s-334" data-id="334" onclick="$('button_product1_info').click();"><div class="s-col size-24" id="s-335" data-id="335"><div class="s-element" id="s-336" data-id="336"><div class="witget-image strech center clickable"  style="cursor:default;"><div class="image" style="height: 180px;"><img src="/sale/files/4eb6985a82.jpg" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-424" data-id="424"><div class="s-col size-24" id="s-425" data-id="425"><div class="s-element" id="s-426" data-id="426"><div class="witget-text"><h5 class="wysiwyg-text-align-center"><b>4 590 СЂСѓР±.</b></h5></div></div></div></div><div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center">
<?if ($city):?>
<div class="button" id="buy1" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy1_clicked');"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
<?else:?>
<div class="button" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy1_clicked'); document.location.href='https://orthoboom.ru/catalog2/krossovki_art_37474_16_belo_seryy/';"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
<?endif;?>
</div></div></div></div>

<div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center"><div class="button button_product1_info" style="background-color: rgb(197, 195, 183); border-color: #000;"><i class="fa fa-info"></i><span>РЈР·РЅР°С‚СЊ РїРѕРґСЂРѕР±РЅРµРµ</span></div></div></div></div></div>

</div></div>


<div class="s-col size-8" id="s-340" data-id="340"><div class="s-col-wrapper">

<div class="s-row" id="s-344" data-id="344"><div class="s-col size-24" id="s-345" data-id="345"><div class="s-element" id="s-346" data-id="346"><div class="witget-image strech center clickable"  style="cursor:default;"><div class="image" style="height: 180px;"><img src="/sale/files/6c691d85c9.jpg" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-513" data-id="513"><div class="s-col size-24" id="s-514" data-id="514"><div class="s-element" id="s-429" data-id="429"><div class="witget-text"><h5 class="wysiwyg-text-align-center"><b>4 590 СЂСѓР±.</b></h5></div></div></div></div><div class="s-row" id="s-510" data-id="510"><div class="s-col size-24" id="s-511" data-id="511"><div class="s-element" id="s-343" data-id="343"><div class="witget-button center">
<?if ($city):?>
<div class="button" id="buy2" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy2_clicked');"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
<?else:?>
<div class="button" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy2_clicked'); document.location.href='https://orthoboom.ru/catalog2/krossovki_art_37474_16_malinovo_belyy/';"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
<?endif;?>
</div></div></div></div>

<div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-343" data-id="343"><div class="witget-button center"><div class="button button_product2_info" style="background-color: rgb(197, 195, 183); border-color: #000;"><i class="fa fa-info"></i><span>РЈР·РЅР°С‚СЊ РїРѕРґСЂРѕР±РЅРµРµ</span></div></div></div></div></div>

</div></div>

<div class="s-col size-8" id="s-347" data-id="347"><div class="s-col-wrapper" onclick="yaCounter44091509.reachGoal('buy3_clicked');">

<div class="s-row" id="s-348" data-id="348"><div class="s-col size-24" id="s-349" data-id="349"><div class="s-element" id="s-350" data-id="350"><div class="witget-image strech center clickable"  style="cursor:default;"><div class="image" style="height: 180px;"><img src="/sale/files/f638dcfdd3.jpg" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-430" data-id="430"><div class="s-col size-24" id="s-431" data-id="431"><div class="s-element" id="s-432" data-id="432"><div class="witget-text"><h5 class="wysiwyg-text-align-center"><b>4 590 СЂСѓР±.</b></h5></div></div></div></div><div class="s-row" id="s-351" data-id="351"><div class="s-col size-24" id="s-352" data-id="352"><div class="s-element" id="s-353" data-id="353"><div class="witget-button center">
<?if ($city):?>
<div class="button" id="buy3" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy3_clicked');"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
<?else:?>
<div class="button" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy3_clicked'); document.location.href='https://orthoboom.ru/catalog2/krossovki_art_37474_16_chernyy/';"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
<?endif;?>
</div></div></div></div>

<div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-353" data-id="353"><div class="witget-button center"><div class="button button_product3_info" style="background-color: rgb(197, 195, 183); border-color: #000;"><i class="fa fa-info"></i><span>РЈР·РЅР°С‚СЊ РїРѕРґСЂРѕР±РЅРµРµ</span></div></div></div></div></div>

</div></div>

</div>



<div class="s-row" id="s-354" data-id="354">

<div class="s-col size-8" id="s-359" data-id="359"><div class="s-col-wrapper" onclick="yaCounter44091509.reachGoal('buy4_clicked');">

<div class="s-row" id="s-360" data-id="360"><div class="s-col size-24" id="s-361" data-id="361"><div class="s-element" id="s-362" data-id="362"><div class="witget-image strech center clickable"  style="cursor:default;"><div class="image" style="height: 180px;"><img src="/sale/files/2e94542e5f.jpg" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-413" data-id="413"><div class="s-col size-24" id="s-414" data-id="414"><div class="s-element" id="s-415" data-id="415"><div class="witget-text"><h5 class="wysiwyg-text-align-center"><b>4 190 СЂСѓР±.</b></h5></div></div></div></div><div class="s-row" id="s-363" data-id="363"><div class="s-col size-24" id="s-364" data-id="364"><div class="s-element" id="s-365" data-id="365"><div class="witget-button center">
<?if ($city):?>
<div class="button" id="buy4" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy4_clicked');"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
<?else:?>
<div class="button" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy4_clicked'); document.location.href='https://orthoboom.ru/catalog2/tufli_letnie_art_47387_13_belo_siniy_krasnyy/';"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
<?endif;?>
</div></div></div></div>

<div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-365" data-id="365"><div class="witget-button center"><div class="button button_product4_info" style="background-color: rgb(197, 195, 183); border-color: #000;"><i class="fa fa-info"></i><span>РЈР·РЅР°С‚СЊ РїРѕРґСЂРѕР±РЅРµРµ</span></div></div></div></div></div>

</div></div>


<div class="s-col size-8" id="s-382" data-id="382"><div class="s-col-wrapper" onclick="yaCounter44091509.reachGoal('buy5_clicked');">

<div class="s-row" id="s-386" data-id="386"><div class="s-col size-24" id="s-387" data-id="387"><div class="s-element" id="s-388" data-id="388"><div class="witget-image strech center clickable"  style="cursor:default;"><div class="image" style="height: 180px;"><img src="/sale/files/b7e31b961c.jpg" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-520" data-id="520"><div class="s-col size-24" id="s-521" data-id="521"><div class="s-element" id="s-523" data-id="523"><div class="witget-text"><h5 class="wysiwyg-text-align-center"><b>4 490 СЂСѓР±.</b></h5></div></div></div></div><div class="s-row" id="s-516" data-id="516"><div class="s-col size-24" id="s-517" data-id="517"><div class="s-element" id="s-519" data-id="519"><div class="witget-button center">
<?if ($city):?>
<div class="button" id="buy5" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy5_clicked');"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
<?else:?>
<div class="button" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy5_clicked'); document.location.href='https://orthoboom.ru/catalog2/botinki_letnie_art_71497_1_malinovyy_v_goroshek/';"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
<?endif;?>
</div></div></div></div>

<div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-519" data-id="519"><div class="witget-button center"><div class="button button_product5_info" style="background-color: rgb(197, 195, 183); border-color: #000;"><i class="fa fa-info"></i><span>РЈР·РЅР°С‚СЊ РїРѕРґСЂРѕР±РЅРµРµ</span></div></div></div></div></div>

</div></div>

<div class="s-col size-8" id="s-393" data-id="393"><div class="s-col-wrapper" onclick="yaCounter44091509.reachGoal('buy6_clicked');">

<div class="s-row" id="s-394" data-id="394"><div class="s-col size-24" id="s-395" data-id="395"><div class="s-element" id="s-396" data-id="396"><div class="witget-image strech center clickable"  style="cursor:default;"><div class="image" style="height: 180px;"><img src="/sale/files/fe7b0f8fdb.jpg" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-421" data-id="421"><div class="s-col size-24" id="s-422" data-id="422"><div class="s-element" id="s-423" data-id="423"><div class="witget-text"><h5 class="wysiwyg-text-align-center"><b>4 390 СЂСѓР±.</b></h5></div></div></div></div><div class="s-row" id="s-397" data-id="397"><div class="s-col size-24" id="s-398" data-id="398"><div class="s-element" id="s-399" data-id="399"><div class="witget-button center">
<?if ($city):?>
<div class="button" id="buy6" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy6_clicked');"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
<?else:?>
<div class="button" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy6_clicked'); document.location.href='https://orthoboom.ru/catalog2/tufli_letnie_art_47387_9_rozovyy/';"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
<?endif;?>
</div></div></div></div>

<div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-399" data-id="399"><div class="witget-button center"><div class="button button_product6_info" style="background-color: rgb(197, 195, 183); border-color: #000;"><i class="fa fa-info"></i><span>РЈР·РЅР°С‚СЊ РїРѕРґСЂРѕР±РЅРµРµ</span></div></div></div></div></div>

</div></div>

</div>
<?else:?>

<div class="s-row" id="s-332" data-id="332">

<div class="s-col size-8" id="s-333" data-id="333"><div class="s-col-wrapper">

<div class="s-row" id="s-334" data-id="334"><div class="s-col size-24" id="s-335" data-id="335"><div class="s-element" id="s-336" data-id="336"><div class="witget-image strech center clickable"  style="cursor:default;"><div class="image" style="height: 180px;"><img src="/sale/files/vladivostok/1.jpeg" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-424" data-id="424"><div class="s-col size-24" id="s-425" data-id="425"><div class="s-element" id="s-426" data-id="426"><div class="witget-text"><h5 class="wysiwyg-text-align-center"><b>4 930 СЂСѓР±.</b></h5></div></div></div></div><div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center">
<div class="button" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy1_clicked'); document.location.href='https://ortom.ru/catalog/ortopedicheskaya_obuv/detskaya_ortopedicheskaya_obuv/novinki_1/sandalety_art_27057_04_m_30.html'"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
</div></div></div></div>

<div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center"><div class="button" style="background-color: rgb(197, 195, 183); border-color: #000;" onclick="document.location.href='https://ortom.ru/catalog/ortopedicheskaya_obuv/detskaya_ortopedicheskaya_obuv/novinki_1/sandalety_art_27057_04_m_30.html'"><i class="fa fa-info"></i><span>РЈР·РЅР°С‚СЊ РїРѕРґСЂРѕР±РЅРµРµ</span></div></div></div></div></div>

</div></div>


<div class="s-col size-8" id="s-333" data-id="333"><div class="s-col-wrapper">

<div class="s-row" id="s-334" data-id="334"><div class="s-col size-24" id="s-335" data-id="335"><div class="s-element" id="s-336" data-id="336"><div class="witget-image strech center clickable"  style="cursor:default;"><div class="image" style="height: 180px;"><img src="/sale/files/vladivostok/2.jpeg" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-424" data-id="424"><div class="s-col size-24" id="s-425" data-id="425"><div class="s-element" id="s-426" data-id="426"><div class="witget-text"><h5 class="wysiwyg-text-align-center"><b>5 200 СЂСѓР±.</b></h5></div></div></div></div><div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center">
<div class="button" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy2_clicked'); document.location.href='https://ortom.ru/catalog/ortopedicheskaya_obuv/detskaya_ortopedicheskaya_obuv/novinki_1/krossovki_art_37057_01_d_50.html'"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
</div></div></div></div>

<div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center"><div class="button" style="background-color: rgb(197, 195, 183); border-color: #000;" onclick="document.location.href='https://ortom.ru/catalog/ortopedicheskaya_obuv/detskaya_ortopedicheskaya_obuv/novinki_1/krossovki_art_37057_01_d_50.html'"><i class="fa fa-info"></i><span>РЈР·РЅР°С‚СЊ РїРѕРґСЂРѕР±РЅРµРµ</span></div></div></div></div></div>

</div></div>

<div class="s-col size-8" id="s-333" data-id="333"><div class="s-col-wrapper">

<div class="s-row" id="s-334" data-id="334"><div class="s-col size-24" id="s-335" data-id="335"><div class="s-element" id="s-336" data-id="336"><div class="witget-image strech center clickable"  style="cursor:default;"><div class="image" style="height: 180px;"><img src="/sale/files/vladivostok/3.jpeg" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-424" data-id="424"><div class="s-col size-24" id="s-425" data-id="425"><div class="s-element" id="s-426" data-id="426"><div class="witget-text"><h5 class="wysiwyg-text-align-center"><b>5 390 СЂСѓР±.</b></h5></div></div></div></div><div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center">
<div class="button" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy3_clicked'); document.location.href='https://ortom.ru/catalog/ortopedicheskaya_obuv/detskaya_ortopedicheskaya_obuv/novinki_1/sandalety_art_27057_11_m_30.html'"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
</div></div></div></div>

<div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center"><div class="button" style="background-color: rgb(197, 195, 183); border-color: #000;" onclick="document.location.href='https://ortom.ru/catalog/ortopedicheskaya_obuv/detskaya_ortopedicheskaya_obuv/novinki_1/sandalety_art_27057_11_m_30.html'"><i class="fa fa-info"></i><span>РЈР·РЅР°С‚СЊ РїРѕРґСЂРѕР±РЅРµРµ</span></div></div></div></div></div>

</div></div>

</div>



<div class="s-row" id="s-354" data-id="354">

<div class="s-col size-8" id="s-333" data-id="333"><div class="s-col-wrapper">

<div class="s-row" id="s-334" data-id="334"><div class="s-col size-24" id="s-335" data-id="335"><div class="s-element" id="s-336" data-id="336"><div class="witget-image strech center clickable"  style="cursor:default;"><div class="image" style="height: 180px;"><img src="/sale/files/vladivostok/4.jpeg" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-424" data-id="424"><div class="s-col size-24" id="s-425" data-id="425"><div class="s-element" id="s-426" data-id="426"><div class="witget-text"><h5 class="wysiwyg-text-align-center"><b>5 020 СЂСѓР±.</b></h5></div></div></div></div><div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center">
<div class="button" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy4_clicked'); document.location.href='https://ortom.ru/catalog/ortopedicheskaya_obuv/detskaya_ortopedicheskaya_obuv/novinki_1/botinki_letnie_art_81057_02_m_30.html'"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
</div></div></div></div>

<div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center"><div class="button" style="background-color: rgb(197, 195, 183); border-color: #000;" onclick="document.location.href='https://ortom.ru/catalog/ortopedicheskaya_obuv/detskaya_ortopedicheskaya_obuv/novinki_1/botinki_letnie_art_81057_02_m_30.html'"><i class="fa fa-info"></i><span>РЈР·РЅР°С‚СЊ РїРѕРґСЂРѕР±РЅРµРµ</span></div></div></div></div></div>

</div></div>


<div class="s-col size-8" id="s-333" data-id="333"><div class="s-col-wrapper">

<div class="s-row" id="s-334" data-id="334"><div class="s-col size-24" id="s-335" data-id="335"><div class="s-element" id="s-336" data-id="336"><div class="witget-image strech center clickable"  style="cursor:default;"><div class="image" style="height: 180px;"><img src="/sale/files/vladivostok/5.jpeg" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-424" data-id="424"><div class="s-col size-24" id="s-425" data-id="425"><div class="s-element" id="s-426" data-id="426"><div class="witget-text"><h5 class="wysiwyg-text-align-center"><b>5 010 СЂСѓР±.</b></h5></div></div></div></div><div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center">
<div class="button" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy5_clicked'); document.location.href='https://ortom.ru/catalog/ortopedicheskaya_obuv/detskaya_ortopedicheskaya_obuv/novinki_1/tufli_letnie_art_47057_03_m_30.html'"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
</div></div></div></div>

<div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center"><div class="button" style="background-color: rgb(197, 195, 183); border-color: #000;" onclick="document.location.href='https://ortom.ru/catalog/ortopedicheskaya_obuv/detskaya_ortopedicheskaya_obuv/novinki_1/tufli_letnie_art_47057_03_m_30.html'"><i class="fa fa-info"></i><span>РЈР·РЅР°С‚СЊ РїРѕРґСЂРѕР±РЅРµРµ</span></div></div></div></div></div>

</div></div>


<div class="s-col size-8" id="s-333" data-id="333"><div class="s-col-wrapper">

<div class="s-row" id="s-334" data-id="334"><div class="s-col size-24" id="s-335" data-id="335"><div class="s-element" id="s-336" data-id="336"><div class="witget-image strech center clickable"  style="cursor:default;"><div class="image" style="height: 180px;"><img src="/sale/files/vladivostok/6.jpeg" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-424" data-id="424"><div class="s-col size-24" id="s-425" data-id="425"><div class="s-element" id="s-426" data-id="426"><div class="witget-text"><h5 class="wysiwyg-text-align-center"><b>4 470 СЂСѓР±.</b></h5></div></div></div></div><div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center">
<div class="button" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy6_clicked'); document.location.href='https://ortom.ru/catalog/ortopedicheskaya_obuv/detskaya_ortopedicheskaya_obuv/novinki_1/botinki_letnie_art_71057_01_d_30.html'"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
</div></div></div></div>

<div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center"><div class="button" style="background-color: rgb(197, 195, 183); border-color: #000;" onclick="document.location.href='https://ortom.ru/catalog/ortopedicheskaya_obuv/detskaya_ortopedicheskaya_obuv/novinki_1/botinki_letnie_art_71057_01_d_30.html'"><i class="fa fa-info"></i><span>РЈР·РЅР°С‚СЊ РїРѕРґСЂРѕР±РЅРµРµ</span></div></div></div></div></div>

</div></div>

</div>


<div class="s-row" id="s-354" data-id="354">


<div class="s-col size-8" id="s-333" data-id="333"><div class="s-col-wrapper">

<div class="s-row" id="s-334" data-id="334"><div class="s-col size-24" id="s-335" data-id="335"><div class="s-element" id="s-336" data-id="336"><div class="witget-image strech center clickable"  style="cursor:default;"><div class="image" style="height: 180px;"><img src="/sale/files/vladivostok/7.jpeg" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-424" data-id="424"><div class="s-col size-24" id="s-425" data-id="425"><div class="s-element" id="s-426" data-id="426"><div class="witget-text"><h5 class="wysiwyg-text-align-center"><b>4 470 СЂСѓР±.</b></h5></div></div></div></div><div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center">
<div class="button" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy7_clicked'); document.location.href='https://ortom.ru/catalog/ortopedicheskaya_obuv/detskaya_ortopedicheskaya_obuv/novinki_1/sandalety_art_27057_02_d_30.html'"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
</div></div></div></div>

<div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center"><div class="button" style="background-color: rgb(197, 195, 183); border-color: #000;" onclick="document.location.href='https://ortom.ru/catalog/ortopedicheskaya_obuv/detskaya_ortopedicheskaya_obuv/novinki_1/sandalety_art_27057_02_d_30.html'"><i class="fa fa-info"></i><span>РЈР·РЅР°С‚СЊ РїРѕРґСЂРѕР±РЅРµРµ</span></div></div></div></div></div>

</div></div>


<div class="s-col size-8" id="s-333" data-id="333"><div class="s-col-wrapper">

<div class="s-row" id="s-334" data-id="334"><div class="s-col size-24" id="s-335" data-id="335"><div class="s-element" id="s-336" data-id="336"><div class="witget-image strech center clickable"  style="cursor:default;"><div class="image" style="height: 180px;"><img src="/sale/files/vladivostok/8.jpeg" alt="" title=""></div></div></div></div></div><div class="s-row" id="s-424" data-id="424"><div class="s-col size-24" id="s-425" data-id="425"><div class="s-element" id="s-426" data-id="426"><div class="witget-text"><h5 class="wysiwyg-text-align-center"><b>4 650 СЂСѓР±.</b></h5></div></div></div></div><div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center">
<div class="button" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('buy8_clicked'); document.location.href='https://ortom.ru/catalog/ortopedicheskaya_obuv/detskaya_ortopedicheskaya_obuv/novinki_1/tufli_letnie_art_47057_01_d_30.html'"><i class="fa fa-shopping-cart"></i><span>РљСѓРїРёС‚СЊ</span></div>
</div></div></div></div>

<div class="s-row" id="s-337" data-id="337"><div class="s-col size-24" id="s-338" data-id="338"><div class="s-element" id="s-339" data-id="339"><div class="witget-button center"><div class="button" style="background-color: rgb(197, 195, 183); border-color: #000;" onclick="document.location.href='https://ortom.ru/catalog/ortopedicheskaya_obuv/detskaya_ortopedicheskaya_obuv/novinki_1/tufli_letnie_art_47057_01_d_30.html'"><i class="fa fa-info"></i><span>РЈР·РЅР°С‚СЊ РїРѕРґСЂРѕР±РЅРµРµ</span></div></div></div></div></div>

</div></div>

</div>

<?endif;?>

<div class="s-row" id="s-408" data-id="408"><div class="s-col size-24" id="s-409" data-id="409"><div class="s-col-wrapper"><div class="s-row" id="s-410" data-id="410"><div class="s-col size-24" id="s-411" data-id="411"><div class="s-element" id="s-412" data-id="412"><div class="witget-button center"><div class="button" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('button_catalog_clicked'); document.location.href='/sale/files/download/catalog_2017.pdf';"><i class="fa fa-hand-o-right"></i><span>РЎРєР°С‡Р°С‚СЊ РєР°С‚Р°Р»РѕРі</span></div></div></div></div></div></div></div></div><div class="s-row" id="s-537" data-id="537"><div class="s-col size-24" id="s-538" data-id="538"><div class="s-col-wrapper"><div class="s-row" id="s-539" data-id="539"><div class="s-col size-24" id="s-540" data-id="540"><div class="s-element" id="s-541" data-id="541"><div class="witget-hr">
	<div class="hr empty"></div>
</div></div></div></div></div></div></div><div class="s-row" id="s-532" data-id="532"><div class="s-col size-24" id="s-533" data-id="533"><div class="s-col-wrapper"><div class="s-row" id="s-534" data-id="534"><div class="s-col size-24" id="s-535" data-id="535"><div class="s-element" id="s-536" data-id="536"><div class="witget-text"><div class="wysiwyg-text-align-center"><h3><span style="color: rgb(0, 0, 0);"><b>РђР”Р Р•РЎРђ РЎРђР›РћРќРћР’</b></span></h3></div></div></div></div></div></div></div></div>

<div class="s-row" id="s-400" data-id="400"><div class="s-col size-24" id="s-401" data-id="401"><div class="s-col-wrapper"><div class="s-row" id="s-402" data-id="402"><div class="s-col size-24" id="s-403" data-id="403"><div class="s-element" id="s-404" data-id="404"><div class="witget-html" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('map_clicked');">

<script type="text/javascript" charset="utf-8" async src="<?=$yamapSrc?>"></script>

</div></div></div></div></div></div></div></div></div></div>




<div class="s-grid vertical-align-top" id="s-219" data-id="219" data-index="0"><div class="s-grid-overlay"><div class="s-grid-wrapper" id="s-220" data-id="220" style="" data-index="0"><div class="s-row" id="s-221" data-id="221"><div class="s-col size-24" id="s-222" data-id="222"><div class="s-col-wrapper"><div class="s-row" id="s-573" data-id="573">

<!--<div class="s-col size-5" id="s-576" data-id="576"><div class="s-element" id="s-572" data-id="572" style=""><div class="witget-slideshow"><div class="content one" style="height: 120px;">
<div class="image"><img src="files/ee722f6603.jpg"></div>
<div class="clear"></div></div></div></div></div>-->

<div class="s-col size-19" id="s-574" data-id="574"><div class="s-element" id="s-230" data-id="230"><div class="witget-socials"><div class="share"><i class="fa fa-vk" data-type="vkontakte"></i></div><div class="share"><i class="fa fa-youtube" data-type="youtube"></i></div></div></div></div></div><div class="s-row" id="s-231" data-id="231"><div class="s-col size-24" id="s-232" data-id="232"><div class="s-element" id="s-233" data-id="233"><div class="witget-hr">
	<div class="hr empty"></div>
</div></div></div></div></div></div></div><div class="s-row" id="s-234" data-id="234"><div class="s-col size-10" id="s-235" data-id="235"><div class="s-col-wrapper"><div class="s-row" id="s-236" data-id="236"><div class="s-col size-6" id="s-237" data-id="237"><div class="s-element" id="s-238" data-id="238"><div class="witget-image strech center"><div class="image" style="height: 40px;"><img src="/sale/files/37a9c83d6f.png" alt="" title=""></div></div></div></div><div class="s-col size-18" id="s-239" data-id="239"><div class="s-element" id="s-240" data-id="240"><div class="witget-text"><p><a href="tel:<?=$phone?>" style="text-decoration:none;" onclick="yaCounter<?=$yaMetrikaCounterId?>.reachGoal('phone_clicked');"><?=$phoneLabel?></a>

</p></div></div></div></div></div></div><div class="s-col size-6" id="s-241" data-id="241"><div class="s-col-wrapper"><div class="s-row" id="s-242" data-id="242"><div class="s-col size-6" id="s-243" data-id="243"><div class="s-element" id="s-244" data-id="244"><div class="witget-image strech center"><div class="image" style="height: 40px;"><img src="/sale/files/dee9d5f233.png" alt="" title=""></div></div></div></div><div class="s-col size-18" id="s-245" data-id="245"><div class="s-element" id="s-246" data-id="246"><div class="witget-text"><p style="white-space:nowrap;"><a href="mailto:<?=$salesEmail?>"><?=$salesEmail?></a></p></div></div></div></div></div></div>

<?if(trim($officeAddr)):?>
<div class="s-col size-8" id="s-247" data-id="247"><div class="s-col-wrapper"><div class="s-row" id="s-248" data-id="248"><div class="s-col size-5" id="s-249" data-id="249"><div class="s-element" id="s-250" data-id="250"><div class="witget-image strech center"><div class="image" style="height: 40px;"><img src="/sale/files/1ad041dab3.png" alt="" title=""></div></div></div></div><div class="s-col size-19" id="s-251" data-id="251"><div class="s-element" id="s-252" data-id="252"><div class="witget-text"><?=$officeAddr?></div></div></div></div></div></div>
<?endif;?>

</div><div class="s-row" id="s-253" data-id="253"><div class="s-col size-24" id="s-254" data-id="254"><div class="s-col-wrapper"><div class="s-row" id="s-255" data-id="255"><div class="s-col size-24" id="s-256" data-id="256"><div class="s-element" id="s-257" data-id="257"><div class="witget-text"><p class="wysiwyg-text-align-center">Copyright В© Orthoboom, 2017</p></div></div></div></div></div></div></div></div></div></div>	</div>

	<script type="text/javascript" src="/sale/files/jquery.countdown.min.js"></script>
	<script type="text/javascript" src="/sale/files/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="/sale/files/velocity.min.js"></script>
	<script type="text/javascript" src="/sale/files/sweetalert.min.js"></script>
	<script type="text/javascript" src="/sale/files/jquery.cookie.js"></script>
	<script type="text/javascript" src="/sale/files/jquery.youtubebackground.js"></script>
	<script type="text/javascript" src="/sale/files/functions.v1.js"></script>
	<script type="text/javascript" src="/sale/files/openapi.js"></script>
	<script type="text/javascript" src="/sale/files/jquery.stellar.min.old.js"></script>
	
	
	<div class="device-desktop"></div>
	<div class="device-tablet"></div>
	<div class="device-mobile"></div>

<!-- РІСЃРїР»С‹РІР°С€РєР° -->
<div class="overlay" id="frm1">
    <div class="popup">
        <h5><?=$promoButtonText?></h5>
        <p style="text-align:center;">
			<div class="form">
				<form action="" method="post">
					<input type="hidden" name="type" value="frm1">
					<input type="hidden" name="city" value="<?=$city?>">
					<input type="text" name="name" placeholder="Р�РјСЏ">
					<input type="text" name="phone" placeholder="РўРµР»РµС„РѕРЅ">
					<input type="text" name="email" placeholder="E-mail*">
					<br><input type="checkbox" name="agree_pers_data"><span class="pers-data-text" style="font-size:12px;"> РЇ РїРѕРґС‚РІРµСЂР¶РґР°СЋ СЃРІРѕРµ СЃРѕРіР»Р°СЃРёРµ СЃ <a href="/consent-for-personal-data-processing" target="_blank">СЃРѕРіР»Р°С€РµРЅРёРµРј РѕР± РѕР±СЂР°Р±РѕС‚РєРµ РїРµСЂСЃРѕРЅР°Р»СЊРЅС‹С… РґР°РЅРЅС‹С…</a></span><br>
					<input type="submit" name="submit" value="РћС‚РїСЂР°РІРёС‚СЊ">
				</form>
			</div>
		</p>
        <button class="a-close" title="Р—Р°РєСЂС‹С‚СЊ" onclick="document.getElementById('frm1').style.display='none';"></button>
    </div>
</div>
<div class="overlay" id="frm2">
    <div class="popup">
        <h5></h5>
        <p style="text-align:center;">
			<div class="form">
				<form action="" method="post">
					<input type="hidden" name="type" value="frm2">
					<input type="hidden" name="city" value="<?=$city?>">
					<input type="hidden" name="product" value="">
					<input type="text" name="name" placeholder="Р�РјСЏ">
					<input type="text" name="phone" placeholder="РўРµР»РµС„РѕРЅ*">
					<br><input type="checkbox" name="agree_pers_data"><span class="pers-data-text" style="font-size:12px;"> РЇ РїРѕРґС‚РІРµСЂР¶РґР°СЋ СЃРІРѕРµ СЃРѕРіР»Р°СЃРёРµ СЃ <a href="/consent-for-personal-data-processing" target="_blank">СЃРѕРіР»Р°С€РµРЅРёРµРј РѕР± РѕР±СЂР°Р±РѕС‚РєРµ РїРµСЂСЃРѕРЅР°Р»СЊРЅС‹С… РґР°РЅРЅС‹С…</a></span><br>
					<input type="submit" name="submit" value="РћС‚РїСЂР°РІРёС‚СЊ">
				</form>
			</div>
			<div id="msg"></div>
		</p>
        <button class="a-close" title="Р—Р°РєСЂС‹С‚СЊ" onclick="document.getElementById('frm2').style.display='none';"></button>
    </div>
</div>
<div class="overlay" id="frm_product_info">
    <div class="popup" style="padding:35px;">
        <h5></h5>
        <div id="params">
			<div><span style="font-weight:bold;">РђСЂС‚РёРєСѓР»:</span> <span id="article"></span></div>
			<div><span style="font-weight:bold;">Р Р°Р·РјРµСЂ:</span> <span id="size"></span></div>
			<div><span style="font-weight:bold;">РџРѕР»:</span> <span id="gender"></span></div>
			<div><span style="font-weight:bold;">РњР°С‚РµСЂРёР°Р»С‹ РІРµСЂС…Р°:</span> <span id="upper_mat"></span></div>
			<div><span style="font-weight:bold;">РњР°С‚РµСЂРёР°Р»С‹ РїРѕРґРєР»Р°РґРєРё:</span> <span id="lining_mat"></span></div>
			<div><span style="font-weight:bold;">РЎРµР·РѕРЅ:</span> <span id="season"></span></div>
			<div><span style="font-weight:bold;">РўРёРї:</span> <span id="type">РїСЂРѕС„РёР»Р°РєС‚РёС‡РµСЃРєР°СЏ</span></div>
		</div>
		<br />
		<div>
			<p style="font-weight:bold;">РћРїРёСЃР°РЅРёРµ:</p>
			<div style="text-align:justify;">
				<ul style="list-style-type: disc; padding-left:25px;">
					<li>СЃСЉРµРјРЅР°СЏ СЃС‚РµР»СЊРєР°-СЃСѓРїРёРЅР°С‚РѕСЂ РїСЂРµРїСЏС‚СЃС‚РІСѓРµС‚ СЂР°Р·РІРёС‚РёСЋ РїР»РѕСЃРєРѕСЃС‚РѕРїРёСЏ;</li>
					<li>Р¶РµСЃС‚РєРёР№ Р·Р°РґРЅРёРє СЃ СѓРґР»РёРЅРµРЅРЅС‹РјРё РєСЂС‹Р»СЊСЏРјРё СЃС‚Р°Р±РёР»РёР·РёСЂСѓРµС‚ Рё С„РёРєСЃРёСЂСѓРµС‚ СЃС‚РѕРїСѓ РІ РїСЂР°РІРёР»СЊРЅРѕРј РїРѕР»РѕР¶РµРЅРёРё;</li>
					<li>РјСЏРіРєРёР№ РєР°РЅС‚ РѕР±РµСЃРїРµС‡РёРІР°РµС‚ РєРѕРјС„РѕСЂС‚ РїСЂРё С…РѕРґСЊР±Рµ;</li>
					<li>СЃС‚С‘Р¶РєРё РІРµР»СЊРєСЂРѕ РіР°СЂР°РЅС‚РёСЂСѓСЋС‚ РїР»РѕС‚РЅСѓСЋ Рё РЅР°РґРµР¶РЅСѓСЋ С„РёРєСЃР°С†РёСЋ;</li>
					<li>С€РёСЂРѕРєР°СЏ СЂР°СЃРєСЂС‹РІР°РµРјРѕСЃС‚СЊ Рё Р»РµРіРєРѕСЃС‚СЊ РѕРґРµРІР°РЅРёСЏ Р·Р° СЃС‡РµС‚ СѓРЅРёРєР°Р»СЊРЅРѕР№ РєРѕРЅСЃС‚СЂСѓРєС†РёРё РєСЂРѕСЏ РѕР±СѓРІРё - LowOpen;</li>
					<li>Р¶РµСЃС‚РєРёР№ Рё С€РёСЂРѕРєРёР№ РїРѕРґРЅРѕСЃРѕРє - РїРѕРјРѕРіР°РµС‚ СѓРґРѕР±РЅРѕРјСѓ Рё РїСЂРѕСЃС‚РѕСЂРЅРѕРјСѓ СЂР°Р·РјРµС‰РµРЅРёСЋ РїР°Р»СЊС†РµРІ Рё РёС… СЃРІРѕР±РѕРґРЅРѕРјСѓ РґРІРёР¶РµРЅРёСЋ, Р·Р°С‰РёС‰Р°РµС‚ РЅРѕР¶РєСѓ РїСЂРё С…РѕРґСЊР±Рµ;</li>
				</ul>
			</div>
			<br />
			<p style="font-weight:bold;">РџРѕРєР°Р·Р°РЅРёСЏ Рє РїСЂРёРјРµРЅРµРЅРёСЋ:</p>
			<div style="text-align:justify;">
				<ul style="list-style-type: disc; padding-left:25px;">
					<li>СЃ С†РµР»СЊСЋ РїСЂРѕС„РёР»Р°РєС‚РёРєРё РґР»СЏ РїСЂРµРґСѓРїСЂРµР¶РґРµРЅРёСЏ СЂР°Р·РІРёС‚РёСЏ РїР°С‚РѕР»РѕРіРёРё СЃС‚РѕРїС‹;</p>
					<li>РїСЂРё РЅР°С‡Р°Р»СЊРЅРѕР№ СЃС‚РµРїРµРЅРё РїР»РѕСЃРєРѕСЃС‚РѕРїРёСЏ РёР»Рё РєРѕСЃРѕР»Р°РїРѕСЃС‚Рё;</p>
					<li>СЃРѕС‡РµС‚Р°РЅРЅРѕР№ С„РѕСЂРјРµ РїР»РѕСЃРєРѕСЃС‚РѕРїРёСЏ.</p>
				</ul>
			</div>
		</div>
        <button class="a-close" title="Р—Р°РєСЂС‹С‚СЊ" onclick="document.getElementById('frm_product_info').style.display='none';"></button>
    </div>
</div>
<div class="overlay" id="frm_alert">
    <div class="popup">
        <h5>РЎРїР°СЃРёР±Рѕ!</h5>
		<br>
		<div id="msg"></div>
        <button class="a-close" title="Р—Р°РєСЂС‹С‚СЊ" onclick="document.getElementById('frm_alert').style.display='none';"></button>
    </div>
</div>
<!-- /РІСЃРїР»С‹РІР°С€РєР° -->	
</body></html>