п»ї<?
$arrInfo = array(
	'yaroslavl' => array(
		'logo' => 'yaroslavl.gif',
		'logoWidth' => '100%',
		'mainImg' => 'pic.jpg',
		'phoneLabel' => '8 (902) 330-08-08',
		'phone' => '+79023300808',
		'workingHours' => 'РµР¶РµРґРЅРµРІРЅРѕ СЃ 9.00-20.00',
		'officeAddr' => '',
		'discountPercent' => '25% РЎРљР�Р”РљРђ РќРђ Р’РўРћР РЈР® РџРђР РЈ',
		'textStyle' => '',
		'promoButtonText' => 'РџРѕР»СѓС‡РёС‚СЊ РїСЂРѕРјРѕРєРѕРґ',
		'salesEmail' => 'sales@atletika-orto.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A7a5d255bba56ba1d64fd43f3b274d6d737e42521e975e4feb303c85c8d37f165&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'Р”Рћ 31 Р�Р®Р›РЇ Р’ РЎР•РўР� РћР РўРћРџР•Р”Р�Р§Р•РЎРљР�РҐ РЎРђР›РћРќРћР’ &quot;РђРўР›Р•РўР�РљРђ&quot;',
		'caption' => 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM Р’ РЇР РћРЎР›РђР’Р›Р•',
		'yaMetrikaCounterId' => '44214884',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44214884 = new Ya.Metrika({
                    id:44214884,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44214884" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->',
	'gaCounter' => "<!--Google analytics-->
 <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-64147781-9', 'auto'); 
 ga('require', 'displayfeatures');
 ga('send', 'pageview');

 /* Accurate bounce rate by time */
 if (!document.referrer ||
 document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'РќРѕРІС‹Р№ РїРѕСЃРµС‚РёС‚РµР»СЊ', location.pathname);
 }, 15000);</script>

 <!--Google analytics-->"
	),
	
	'astrahan' => array(
		'logo' => 'astrahan.jpg',
		'logoWidth' => '100%',
		'mainImg' => 'pic.jpg',
		'phoneLabel' => '8 (8512) 666-700',
		'phone' => '+78512666700',
		'workingHours' => 'РµР¶РµРґРЅРµРІРЅРѕ СЃ 9.00-19.00',
		'officeAddr' => '<p>Рі. РђСЃС‚СЂР°С…Р°РЅСЊ,</p><p>РљСЂР°СЃРЅР°СЏ РќР°Р±РµСЂРµР¶РЅР°СЏ, Рґ.13</p>',
		'discountPercent' => '25% РЎРљР�Р”РљРђ РќРђ Р’РўРћР РЈР® РџРђР РЈ',
		'textStyle' => '',
		'promoButtonText' => 'РџРѕР»СѓС‡РёС‚СЊ РїСЂРѕРјРѕРєРѕРґ',
		'salesEmail' => 'ndemyanenko@orto-30.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa2b951cb6e7db93a13d3bc629fb0a9d2a36a48f4a6a09b660fff6127957633a4&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'Р”Рћ 31 Р�Р®Р›РЇ Р’ РЎР•РўР� РЎРђР›РћРќРћР’ &quot;РћР РўРћРџР•Р”Р�Р§Р•РЎРљР�Р• Р�Р—Р”Р•Р›Р�РЇ&quot;',
		'caption' => 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM Р’ РђРЎРўР РђРҐРђРќР�',
		'yaMetrikaCounterId' => '44248174',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44248174 = new Ya.Metrika({
                    id:44248174,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44248174" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->',
	'gaCounter' => "<!--Google analytics-->
 <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-64147781-10', 'auto'); 
 ga('require', 'displayfeatures');
 ga('send', 'pageview');

 /* Accurate bounce rate by time */
 if (!document.referrer ||
 document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'РќРѕРІС‹Р№ РїРѕСЃРµС‚РёС‚РµР»СЊ', location.pathname);
 }, 15000);</script>

 <!--Google analytics-->"
	),
	
	'kursk' => array(
		'logo' => 'kursk.jpg',
		'logoWidth' => '100%',
		'mainImg' => 'pic_megasale_all_but_nn.jpg',
		'phoneLabel' => '8 (800) 333-40-07',
		'phone' => '+78003334007',
		'workingHours' => 'РµР¶РµРґРЅРµРІРЅРѕ СЃ 9.00-20.00',
		'officeAddr' => '<p>Рі. РљСѓСЂСЃРє,</p><p>РџСЂРѕСЃРїРµРєС‚ Р›РµРЅРёРЅСЃРєРѕРіРѕ РљРѕРјСЃРѕРјРѕР»Р°, 57Рђ</p>',
		//'discountPercent' => '20% РЎРљР�Р”РљРђ РќРђ Р’РўРћР РЈР® РџРђР РЈ, 40% РЎРљР�Р”РљРђ РќРђ РўР Р•РўР¬Р® РџРђР РЈ',
		'discountPercent' => 'РЎРљР�Р”РљРђ Р”Рћ 50% РќРђ РћР‘РЈР’Р¬ РћР РўРћР‘РЈРњ',
		'textStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'РџРѕР»СѓС‡РёС‚СЊ РїСЂРѕРјРѕРєРѕРґ',
		'salesEmail' => 'callcentre@orto-doktor.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A1a77e35298686ff871dad78abef06afb66c557dc0e7e0f7419a5e3ffdab6b515&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		//'salonNameAndSaleDate' => 'Р”Рћ 31 Р�Р®Р›РЇ Р’ РЎР•РўР� РЎРђР›РћРќРћР’ РћР РўРћРџР•Р”Р�Р� &quot;РћР РўРћ-Р”РћРљРўРћР &quot;',
		'salonNameAndSaleDate' => 'РЎ 27 РџРћ 30 Р�Р®Р›РЇ Р’ РЎР•РўР� РЎРђР›РћРќРћР’ РћР РўРћРџР•Р”Р�Р� &quot;РћР РўРћ-Р”РћРљРўРћР &quot;',
		'caption' => 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM Р’ РљРЈР РЎРљР•',
		'yaMetrikaCounterId' => '44313804',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44313804 = new Ya.Metrika({
                    id:44313804,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44313804" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter --> ',
	'gaCounter' => "<!--Google analytics-->
 <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-64147781-8', 'auto'); 
 ga('require', 'displayfeatures');
 ga('send', 'pageview');

 /* Accurate bounce rate by time */
 if (!document.referrer ||
 document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'РќРѕРІС‹Р№ РїРѕСЃРµС‚РёС‚РµР»СЊ', location.pathname);
 }, 15000);</script>

 <!--Google analytics-->"
	),
	
	'krasnodar' => array(
		'logo' => 'krasnodar.jpg',
		'logoWidth' => '100%',
		'mainImg' => 'pic.jpg',
		'phoneLabel' => '8 (861) 241-20-01<br>8 (988) 241-20-01',
		'phone' => '+78612412001',
		'workingHours' => 'РµР¶РµРґРЅРµРІРЅРѕ СЃ 9.00-20.00',
		'officeAddr' => '<p>Рі. РљСЂР°СЃРЅРѕРґР°СЂ,</p><p>СѓР». РђР№РІР°Р·РѕРІСЃРєРѕРіРѕ, 108</p>',
		'discountPercent' => '50% РЎРљР�Р”РљРђ РќРђ Р’РўРћР РЈР® РџРђР РЈ',
		'textStyle' => '',
		'promoButtonText' => 'РџРѕР»СѓС‡РёС‚СЊ РїСЂРѕРјРѕРєРѕРґ',
		'salesEmail' => 'maximed@yandex.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aaf7ae376cecb830da6d25a7518aa72f5016ea7c38a1d3177b6701de82a36df1b&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'Р”Рћ 31 Р�Р®Р›РЇ Р’ РЎР•РўР� РЎРђР›РћРќРћР’ &quot;РњРђРљРЎР�РњР•Р”&quot;',
		'caption' => 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM Р’ РљР РђРЎРќРћР”РђР Р•',
		'yaMetrikaCounterId' => '44333914',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44333914 = new Ya.Metrika({
                    id:44333914,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44333914" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->',
		'gaCounter' => "<!--Google analytics-->
 <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-64147781-5', 'auto'); 
 ga('require', 'displayfeatures');
 ga('send', 'pageview');

 /* Accurate bounce rate by time */
 if (!document.referrer ||
 document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'РќРѕРІС‹Р№ РїРѕСЃРµС‚РёС‚РµР»СЊ', location.pathname);
 }, 15000);</script>

 <!--Google analytics-->"
	),
	
	'perm' => array(
		'logo' => 'perm.jpg',
		'logoWidth' => '100%',
		'mainImg' => 'pic_alteramed.jpg',
		'phoneLabel' => '8 (804) 333-01-59',
		'phone' => '+78043330159',
		'workingHours' => 'РµР¶РµРґРЅРµРІРЅРѕ СЃ 9.00-20.00',
		'officeAddr' => '<p>Рі. РџРµСЂРјСЊ,</p><p>СѓР».Р“РѕСЂСЊРєРѕРіРѕ, Рґ.9, РѕС„.17</p>',
		//'discountPercent' => 'РЎРљР�Р”РљРђ 20% РџР Р� РџРћРљРЈРџРљР• 2 РџРђР  Р� Р‘РћР›Р•Р•',
		'discountPercent' => 'РЎРљР�Р”РљРђ Р”Рћ 50% РќРђ РћР‘РЈР’Р¬ РћР РўРћР‘РЈРњ',
		'textStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'РџРѕР»СѓС‡РёС‚СЊ РїСЂРѕРјРѕРєРѕРґ',
		'salesEmail' => 'support@alteramed.org',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa9430ba8aca2bdb3a59f2b91746d047e7f14e595eead8c7e9634b734a4bc135f&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		//'salonNameAndSaleDate' => 'Р”Рћ 31 Р�Р®Р›РЇ Р’ РЎР•РўР� РЎРђР›РћРќРћР’ &quot;alteraMED&quot;',
		'salonNameAndSaleDate' => 'РЎ 28 РџРћ 30 Р�Р®Р›РЇ Р’ РЎР•РўР� РЎРђР›РћРќРћР’ &quot;alteraMED&quot;',
		'caption' => 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM Р’ РџР•Р РњР�',
		'yaMetrikaCounterId' => '44333857',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44333857 = new Ya.Metrika({
                    id:44333857,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44333857" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->',
	'gaCounter' => "<!--Google analytics-->
 <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-64147781-7', 'auto'); 
 ga('require', 'displayfeatures');
 ga('send', 'pageview');

 /* Accurate bounce rate by time */
 if (!document.referrer ||
 document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'РќРѕРІС‹Р№ РїРѕСЃРµС‚РёС‚РµР»СЊ', location.pathname);
 }, 15000);</script>

 <!--Google analytics-->"
	),
	
	'moskovskaya-obl' => array(
		'logo' => 'moskovskaya-obl.jpg',
		'logoWidth' => '100%',
		'mainImg' => 'pic.jpg',
		'phoneLabel' => '8 (985) 440-00-03',
		'phone' => '+79854400003',
		'workingHours' => 'РµР¶РµРґРЅРµРІРЅРѕ СЃ 9.00-20.00',
		'officeAddr' => '',
		'discountPercent' => 'Р”Р•РўРЎРљР�Р™ РћР РўРћРџР•Р”Р�Р§Р•РЎРљР�Р™ Р Р®РљР—РђРљ Р’ РџРћР”РђР РћРљ Р—Рђ РџРћРљРЈРџРљРЈ РџР•Р Р’РћР™ РџРђР Р« РћР‘РЈР’Р�, Р� РЎРљР�Р”РљРђ 25% РќРђ Р’РўРћР РЈР®',
		'textStyle' => '',
		'promoButtonText' => 'РџРѕР»СѓС‡РёС‚СЊ РїСЂРѕРјРѕРєРѕРґ',
		'salesEmail' => 'vzorto@yandex.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A6f314e42f914669a1c92ddeae7f0def878770239c59f182a03b6062783ad4e67&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'Р”Рћ 31 Р�Р®Р›РЇ Р’ РЎР•РўР� РЎРђР›РћРќРћР’ &quot;Р’РђРЁР• Р—Р”РћР РћР’Р¬Р•&quot;',
		'caption' => 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM Р’ РњРћРЎРљРћР’РЎРљРћР™ РћР‘Р›РђРЎРўР�',
		'yaMetrikaCounterId' => '44521144',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44521144 = new Ya.Metrika({
                    id:44521144,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44521144" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->',
	'gaCounter' => "<!--Google analytics-->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-64147781-12', 'auto'); 
ga('require', 'displayfeatures');
ga('send', 'pageview');

/* Accurate bounce rate by time */
if (!document.referrer ||
document.referrer.split('/')[2].indexOf(location.hostname) != 0)
setTimeout(function(){
ga('send', 'event', 'РќРѕРІС‹Р№ РїРѕСЃРµС‚РёС‚РµР»СЊ', location.pathname);
}, 15000);</script>

<!--Google analytics-->"
	),
	
	'ekaterinburg' => array(
		'logo' => 'ekaterinburg.png',
		'logoWidth' => '100%',
		'mainImg' => 'pic_megasale_all_but_nn.jpg',
		'phoneLabel' => '8 (343) 318-21-48',
		'phone' => '+73433182148',
		'workingHours' => 'РµР¶РµРґРЅРµРІРЅРѕ СЃ 9.00-20.00',
		'officeAddr' => '',
		//'discountPercent' => 'Р”Рћ 30% РЎРљР�Р”РљР� РќРђ РќРђ Р”Р•РўРЎРљРЈР® РћР РўРћРџР•Р”Р�Р§Р•РЎРљРЈР® РћР‘РЈР’Р¬ ORTHOBOOM',
		'discountPercent' => 'РЎРљР�Р”РљРђ Р”Рћ 50% РќРђ РћР‘РЈР’Р¬ РћР РўРћР‘РЈРњ',
		'textStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'РџРѕР»СѓС‡РёС‚СЊ РїСЂРѕРјРѕРєРѕРґ',
		'salesEmail' => 'im@ortix.ru,<br>spravka@ortix.ru,<br>k.alekseeva@ortix.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa6bcafb45c3cd4ee0a2d07f8e5246ca70ac1a34910b9c6f3bcdda6cf67910d4c&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		//'salonNameAndSaleDate' => 'Р”Рћ 30 Р�Р®Р›РЇ Р’ РЎР•РўР� РћР РўРћРџР•Р”Р�Р§Р•РЎРљР�РҐ РЎРђР›РћРќРћР’ &quot;РћР РўР�РљРЎ&quot;',
		'salonNameAndSaleDate' => 'РЎ 27 РџРћ 30 Р�Р®Р›РЇ Р’ РЎР•РўР� РћР РўРћРџР•Р”Р�Р§Р•РЎРљР�РҐ РЎРђР›РћРќРћР’ &quot;РћР РўР�РљРЎ&quot;',
		'caption' => 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM Р’ Р•РљРђРўР•Р Р�РќР‘РЈР Р“Р•',
		'yaMetrikaCounterId' => '44506444',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44506444 = new Ya.Metrika({
                    id:44506444,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44506444" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->',
	'gaCounter' => "<!--Google analytics-->
 <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-64147781-6', 'auto'); 
 ga('require', 'displayfeatures');
 ga('send', 'pageview');

 /* Accurate bounce rate by time */
 if (!document.referrer ||
 document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'РќРѕРІС‹Р№ РїРѕСЃРµС‚РёС‚РµР»СЊ', location.pathname);
 }, 15000);</script>

 <!--Google analytics-->"
	),
	
	'irkutsk' => array(
		'logo' => 'irkutsk.jpg',
		'logoWidth' => '100%',
		'mainImg' => 'pic.jpg',
		'phoneLabel' => '8 (800) 500-30-79',
		'phone' => '+78005003079',
		'workingHours' => 'РµР¶РµРґРЅРµРІРЅРѕ СЃ 9.00-20.00',
		'secondPic' => 'kovrik_ortho.jpg',
		'secondText' => 'РњРђРЎРЎРђР–РќР«Р™ РљРћР’Р Р�Рљ РћР РўРћ Р�Р— 4 РњРћР”РЈР›Р•Р™',
		'officeAddr' => 'Рі. Р�СЂРєСѓС‚СЃРє,<br>СѓР». Р›СѓРЅРЅР°СЏ, 1',
		'discountPercent' => 'РџР Р� РџРћРљРЈРџРљР• РћР РўРћРџР•Р”Р�Р§Р•РЎРљРћР™ РћР‘РЈР’Р� ORTHOBOOM Р”РђР Р�Рњ РњРђРЎРЎРђР–РќР«Р™ РљРћР’Р Р�Рљ РћР РўРћ Р�Р— 4 РњРћР”РЈР›Р•Р™',
		'textStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'РџРѕР»СѓС‡РёС‚СЊ РєСѓРїРѕРЅ РЅР° РїРѕРґР°СЂРѕРє',
		'salesEmail' => 'shop@osnovad.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Acf3a187adf6384f998aa621ab695f227522414be5c1b6a5d06ced30344c4d2fc&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'Р”Рћ 31 Р�Р®Р›РЇ Р’ РЎР•РўР� РњРђР“РђР—Р�РќРћР’ РћР РўРћРџР•Р”Р�Р� Р� РњР•Р”РўР•РҐРќР�РљР� &quot;РћРЎРќРћР’Рђ Р”Р’Р�Р–Р•РќР�РЇ&quot;',
		'caption' => 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM Р’ Р�Р РљРЈРўРЎРљР•',
		'yaMetrikaCounterId' => '44520625',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44520625 = new Ya.Metrika({
                    id:44520625,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44520625" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->',
	'gaCounter' => "<!--Google analytics-->
 <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-64147781-11', 'auto'); 
 ga('require', 'displayfeatures');
 ga('send', 'pageview');

 /* Accurate bounce rate by time */
 if (!document.referrer ||
 document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'РќРѕРІС‹Р№ РїРѕСЃРµС‚РёС‚РµР»СЊ', location.pathname);
 }, 15000);</script>

 <!--Google analytics-->"
	),
	
	'mahachkala' => array(
		'logo' => 'mahachkala.jpg',
		'logoWidth' => '60%',
		'mainImg' => 'pic_megasale_all_but_nn.jpg',
		'phoneLabel' => '8 (928) 800-78-78',
		'phone' => '+79288007878',
		'workingHours' => 'РµР¶РµРґРЅРµРІРЅРѕ СЃ 9.00-20.00',
		'secondPic' => '',
		'secondText' => '',
		'officeAddr' => 'Рі. РњР°С…Р°С‡РєР°Р»Р°, РїСЂ. Р .Р“Р°РјР·Р°С‚РѕРІР°, 97 Р‘',
		//'discountPercent' => 'РЎРљР�Р”РљРђ 10% РџР Р� РџРћРљРЈРџРљР• РћР‘РЈР’Р�',
		'discountPercent' => 'РЎРљР�Р”РљРђ Р”Рћ 30% РќРђ РћР‘РЈР’Р¬ РћР РўРћР‘РЈРњ',
		'textStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'РџРѕР»СѓС‡РёС‚СЊ РїСЂРѕРјРѕРєРѕРґ',
		'salesEmail' => 'ortositi.market@mail.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A0c0a64d829fc9b106b3d13cdcd85d842ec320fa77a4e89ce9bbc0967ba9e4662&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		//'salonNameAndSaleDate' => 'Р”Рћ 31 Р�Р®Р›РЇ Р’ РЎР•РўР� РЎРђР›РћРќРћР’ &quot;РћР РўРћРЎР�РўР�&quot;',
		'salonNameAndSaleDate' => 'РЎ 1 РџРћ 6 РђР’Р“РЈРЎРўРђ Р’ РЎР•РўР� РЎРђР›РћРќРћР’ &quot;РћР РўРћРЎР�РўР�&quot;',
		'caption' => 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM Р’ РњРђРҐРђР§РљРђР›Р•',
		'yaMetrikaCounterId' => '44521894',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44521894 = new Ya.Metrika({
                    id:44521894,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44521894" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->',
	'gaCounter' => "<!--Google analytics-->
 <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-64147781-15', 'auto'); 
 ga('require', 'displayfeatures');
 ga('send', 'pageview');

 /* Accurate bounce rate by time */
 if (!document.referrer ||
 document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'РќРѕРІС‹Р№ РїРѕСЃРµС‚РёС‚РµР»СЊ', location.pathname);
 }, 15000);</script>

 <!--Google analytics-->"
	),	
	
	'vladivostok' => array(
		'logo' => 'vladivostok.png',
		'logoWidth' => '100%',
		'mainImg' => 'pic_rukzak.jpg',
		'phoneLabel' => '8 (800) 250-47-99',
		'phone' => '+78002504799',
		'workingHours' => 'РµР¶РµРґРЅРµРІРЅРѕ СЃ 9.00-20.00',
		'secondPic' => '',
		'secondText' => '',
		'officeAddr' => 'Рі. Р’Р»Р°РґРёРІРѕСЃС‚РѕРє, СѓР». Р�СЂС‚С‹С€СЃРєР°СЏ, 23',
		'discountPercent' => 'Р Р®РљР—РђРљ Р’ РџРћР”РђР РћРљ РџР Р� РџРћРљРЈРџРљР• РћР‘РЈР’Р� РћР РўРћР‘РЈРњ',
		//'discountPercent' => 'РЎРљР�Р”РљРђ Р”Рћ 50% РќРђ РћР‘РЈР’Р¬ РћР РўРћР‘РЈРњ',
		'textStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'РџРѕР»СѓС‡РёС‚СЊ РїСЂРѕРјРѕРєРѕРґ',
		'salesEmail' => 'shop@ortom.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Adcccc70a2b6f6b848cba1a233e56828e42eb50c090838cfa65c25a57b15e17ba&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'Р”Рћ 31 Р�Р®Р›РЇ Р’ РЎР•РўР� РЎРђР›РћРќРћР’ &quot;РћР РўРћРњР•Р”&quot;',
		//'salonNameAndSaleDate' => 'РЎ 20 РџРћ 23 Р�Р®Р›РЇ Р’ РЎРђР›РћРќРђРҐ &quot;РћР РўРћРњР•Р”&quot;',
		'caption' => 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM',
		'yaMetrikaCounterId' => '44521729',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44521729 = new Ya.Metrika({
                    id:44521729,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44521729" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->',
	'gaCounter' => "<!--Google analytics-->
 <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-64147781-14', 'auto'); 
 ga('require', 'displayfeatures');
 ga('send', 'pageview');

 /* Accurate bounce rate by time */
 if (!document.referrer ||
 document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'РќРѕРІС‹Р№ РїРѕСЃРµС‚РёС‚РµР»СЊ', location.pathname);
 }, 15000);</script>

 <!--Google analytics-->"
	),
	
	'ufa' => array(
		'logo' => 'ufa.jpg',
		'logoWidth' => '40%',
		'mainImg' => 'pic.jpg',
		'phoneLabel' => '8 (347) 226 97 18',
		'phone' => '+73472269718',
		'workingHours' => 'РµР¶РµРґРЅРµРІРЅРѕ СЃ 9.00-20.00',
		'secondPic' => '',
		'secondText' => '',
		'officeAddr' => '',
		'discountPercent' => 'РЎРљР�Р”РљРђ 10% РќРђ РџРћРљРЈРџРљРЈ РћР‘РЈР’Р� РћР РўРћР‘РЈРњ',
		'textStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'РџРѕР»СѓС‡РёС‚СЊ РїСЂРѕРјРѕРєРѕРґ',
		'salesEmail' => '',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ab386850d790f104f1d10027405fa7c8e4df75b4937f14cc4413a49a0d70f6043&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'Р”Рћ 31 Р�Р®Р›РЇ Р’ РЎР•РўР� РћР РўРћРџР•Р”Р�Р§Р•РЎРљР�РҐ РЎРђР›РћРќРћР’ &quot;РћР РўРћР›Р•РќР”&quot;',
		'caption' => 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM Р’ РЈР¤Р•',
		'yaMetrikaCounterId' => '44521558',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44521558 = new Ya.Metrika({
                    id:44521558,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44521558" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->',
	'gaCounter' => "<!--Google analytics-->
 <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-64147781-13', 'auto'); 
 ga('require', 'displayfeatures');
 ga('send', 'pageview');

 /* Accurate bounce rate by time */
 if (!document.referrer ||
 document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'РќРѕРІС‹Р№ РїРѕСЃРµС‚РёС‚РµР»СЊ', location.pathname);
 }, 15000);</script>

 <!--Google analytics-->"
	),
	
	'kazan' => array(
		'logo' => 'kazan.jpg',
		'logoWidth' => '100%',
		'mainImg' => 'pic.jpg',
		'phoneLabel' => '8 (843) 226-69-99',
		'phone' => '+78432266999',
		'workingHours' => 'РµР¶РµРґРЅРµРІРЅРѕ СЃ 9.00-20.00',
		'secondPic' => '',
		'secondText' => '',
		'officeAddr' => '',
		'discountPercent' => 'РЎРљР�Р”РљРђ 25% РќРђ Р’РўРћР РЈР® РџРђР РЈ РџР Р� РџРћРљРЈРџРљР• РћР‘РЈР’Р� РћР РўРћР‘РЈРњ',
		'textStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'РџРѕР»СѓС‡РёС‚СЊ РїСЂРѕРјРѕРєРѕРґ',
		'salesEmail' => 'ortexmed@mail.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A73286a6d149e733d54f2aabe0b77fa631d4e9ad9f01bedf5dceb88f1fa105fc1&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'Р”Рћ 31 Р�Р®Р›РЇ Р’ РЎР•РўР� РћР РўРћРџР•Р”Р�Р§Р•РЎРљР�РҐ РЎРђР›РћРќРћР’ &quot;РћР РўР•РљРЎ РњР•Р”&quot;',
		'caption' => 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM Р’ РљРђР—РђРќР�',
		'yaMetrikaCounterId' => '44759233',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44759233 = new Ya.Metrika({
                    id:44759233,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44759233" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->',
	'gaCounter' => "<!--Google analytics-->
 <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-64147781-16', 'auto'); 
 ga('require', 'displayfeatures');
 ga('send', 'pageview');

 /* Accurate bounce rate by time */
 if (!document.referrer ||
 document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'РќРѕРІС‹Р№ РїРѕСЃРµС‚РёС‚РµР»СЊ', location.pathname);
 }, 15000);</script>

 <!--Google analytics-->"
	),
	
	'ivanovo' => array(
		'logo' => 'ivanovo.jpg',
		'logoWidth' => '70%',
		'mainImg' => 'pic_megasale_all_but_nn.jpg',
		'phoneLabel' => '8 (800) 250-62-20',
		'phone' => '+78002506220',
		'workingHours' => 'РµР¶РµРґРЅРµРІРЅРѕ СЃ 9.00-20.00',
		'secondPic' => '',
		'secondText' => '',
		'officeAddr' => '',
		//'discountPercent' => 'РЎРљР�Р”РљРђ 25% РќРђ Р’РўРћР РЈР® РџРђР РЈ РџР Р� РџРћРљРЈРџРљР• РћР‘РЈР’Р� РћР РўРћР‘РЈРњ',
		'discountPercent' => 'РЎРљР�Р”РљРђ Р”Рћ 50% РќРђ РћР‘РЈР’Р¬ РћР РўРћР‘РЈРњ',
		'textStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'РџРѕР»СѓС‡РёС‚СЊ РїСЂРѕРјРѕРєРѕРґ',
		'salesEmail' => 'kukonh_iv@nikamed-online.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ac74a654d21d4f1bece8b7ed0d50c2a31c211851cb64201ce0606021b985b7bc7&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		//'salonNameAndSaleDate' => 'Р”Рћ 31 Р�Р®Р›РЇ Р’ РЎР•РўР� РЎРђР›РћРќРћР’ &quot;РќР�РљРђ-РњР•Р”&quot;',
		'salonNameAndSaleDate' => 'РЎ 27 РџРћ 30 Р�Р®Р›РЇ Р’ РЎР•РўР� РЎРђР›РћРќРћР’ &quot;РќР�РљРђ-РњР•Р”&quot;',
		'caption' => 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM Р’ Р�Р’РђРќРћР’Рћ',
		'yaMetrikaCounterId' => '45181284',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter45181284 = new Ya.Metrika({
                    id:45181284,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45181284" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->',
	'gaCounter' => "<!--Google analytics-->
 <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-64147781-17', 'auto'); 
 ga('require', 'displayfeatures');
 ga('send', 'pageview');

 /* Accurate bounce rate by time */
 if (!document.referrer ||
 document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'РќРѕРІС‹Р№ РїРѕСЃРµС‚РёС‚РµР»СЊ', location.pathname);
 }, 15000);</script>

 <!--Google analytics-->"
	),
	
	'vladimir' => array(
		'logo' => 'vladimir.jpg',
		'logoWidth' => '70%',
		'mainImg' => 'pic.jpg',
		'phoneLabel' => '8 (800) 250-62-20',
		'phone' => '+78002506220',
		'workingHours' => 'РµР¶РµРґРЅРµРІРЅРѕ СЃ 9.00-20.00',
		'secondPic' => '',
		'secondText' => '',
		'officeAddr' => '',
		'discountPercent' => 'РЎРљР�Р”РљРђ 25% РќРђ Р’РўРћР РЈР® РџРђР РЈ РџР Р� РџРћРљРЈРџРљР• РћР‘РЈР’Р� РћР РўРћР‘РЈРњ',
		'textStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'РџРѕР»СѓС‡РёС‚СЊ РїСЂРѕРјРѕРєРѕРґ',
		'salesEmail' => 'kukonh_iv@nikamed-online.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A4e7aac098d1ec2f1f7695a104f1616b0958524bb4d7f43c6958b20f3d34d8932&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'Р”Рћ 31 Р�Р®Р›РЇ Р’ РЎР•РўР� РЎРђР›РћРќРћР’ &quot;Р—Р”РћР РћР’Р«Р• РќРћР“Р�&quot;',
		'caption' => 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM Р’Рћ Р’Р›РђР”Р�РњР�Р Р•',
		'yaMetrikaCounterId' => '45181218',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter45181218 = new Ya.Metrika({
                    id:45181218,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45181218" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->',
	'gaCounter' => "<!--Google analytics-->
 <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-64147781-18', 'auto'); 
 ga('require', 'displayfeatures');
 ga('send', 'pageview');

 /* Accurate bounce rate by time */
 if (!document.referrer ||
 document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'РќРѕРІС‹Р№ РїРѕСЃРµС‚РёС‚РµР»СЊ', location.pathname);
 }, 15000);</script>

 <!--Google analytics-->"
	),
	'spb' => array(
		'logo' => 'spb.jpg',
		'logoWidth' => '100%',
		'mainImg' => 'pic_terrapevtika.jpg',
		'phoneLabel' => '8 (812) 456-52-34',
		'phone' => '+78124565234',
		'workingHours' => '',
		'secondPic' => '',
		'secondText' => '',
		'officeAddr' => '',
		'discountPercent' => 'РџР Р� РџРћРљРЈРџРљР• РћР‘РЈР’Р� ORTHOBOOM вЂ“ Р’ РџРћР”РђР РћРљ РЁРљРћР›Р¬РќР«Р™ РџР•РќРђР› Р”Р›РЇ РњРђР›Р¬Р§Р�РљРћР’ Р� Р”Р•Р’РћР§Р•Рљ',
		'textStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'РџРѕР»СѓС‡РёС‚СЊ РїСЂРѕРјРѕРєРѕРґ',
		'salesEmail' => 'shop@terapevtica.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A7f1bd0b1c89b68023430dc3cf32bedd08ea098b84ad3241d6308ecaf10da5721&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'Р”Рћ 15 РЎР•РќРўРЇР‘Р РЇ Р’ РЎР•РўР� РЎРђР›РћРќРћР’ &quot;РўР•Р Р РђРџР•Р’РўР�РљРђ&quot;',
		'caption' => 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM Р’ РЎРђРќРљРў-РџР•РўР•Р Р‘РЈР Р“Р•',
		'yaMetrikaCounterId' => '45668397',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter45668397 = new Ya.Metrika({
                    id:45668397,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45668397" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->',
		'gaCounter' => "<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64147781-19', 'auto');
  ga('send', 'pageview');

</script>"
	),
	'omsk' => array(
		'logo' => 'spb.jpg',
		'logoWidth' => '100%',
		'mainImg' => 'pic_terrapevtika.jpg',
		'phoneLabel' => '8 (381) 229-03-60',
		'phone' => '+73812290360',
		'workingHours' => '',
		'secondPic' => '',
		'secondText' => '',
		'officeAddr' => '',
		'discountPercent' => 'РџР Р� РџРћРљРЈРџРљР• РћР‘РЈР’Р� ORTHOBOOM вЂ“ Р’ РџРћР”РђР РћРљ РЁРљРћР›Р¬РќР«Р™ РџР•РќРђР› Р”Р›РЇ РњРђР›Р¬Р§Р�РљРћР’ Р� Р”Р•Р’РћР§Р•Рљ',
		'textStyle' => 'text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
		'promoButtonText' => 'РџРѕР»СѓС‡РёС‚СЊ РїСЂРѕРјРѕРєРѕРґ',
		'salesEmail' => 'omsk1@terapevtica.ru',
		'yamapSrc' => 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ae3d193b6bc9e632686a55519fbb80ef75b0cc0977db8ba96545d56087a42c558&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true',
		'salonNameAndSaleDate' => 'Р”Рћ 15 РЎР•РќРўРЇР‘Р РЇ Р’ РЎР•РўР� РЎРђР›РћРќРћР’ &quot;РўР•Р Р РђРџР•Р’РўР�РљРђ&quot;',
		'caption' => 'Р”Р•РўРЎРљРђРЇ РћР РўРћРџР•Р”Р�Р§Р•РЎРљРђРЇ РћР‘РЈР’Р¬ ORTHOBOOM Р’ РћРњРЎРљР•',
		'yaMetrikaCounterId' => '45668439',
		'yaMetrikaCounter' => '<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter45668439 = new Ya.Metrika({
                    id:45668439,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45668439" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->',
		'gaCounter' => "<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64147781-20', 'auto');
  ga('send', 'pageview');

</script>"
	)


);

?>