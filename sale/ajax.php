<?php
require_once('config.php');

if (!isset($_POST['type'])) die();
if (!isset($_POST['city'])) die();
$type = strtolower(trim($_POST['type']));
$city = strtolower(trim($_POST['city']));

if (!$city) {
	$to = 'reklama@orthoboom.ru';
} else {
	$to = $arrInfo[$city]['salesEmail'];
}


// проверка капчи
/*
if (isset($_POST["captcha_word"]) && isset($_POST["captcha_code"])) {
	if(!$APPLICATION->CaptchaCheckCode($_POST["captcha_word"], $_POST["captcha_code"])) {
		die('0'); //Неверный проверочный код.'
	}
}
*/

foreach ($_POST as $key => $val) $_POST[$key] = iconv('UTF-8', 'Windows-1251', $val);

switch ($type) {
	case 'frm1':
		$subject = 'Уведомление сервиса "Акция Ортобум - Получить промокод на скидку / купон на подарок"';
		$message =  'Имя: ' . $_POST['name'] . "\r\n" .
			'E-mail: ' . $_POST['email'] . "\r\n" .
			'Телефон: ' . $_POST['phone'];
		//$successMessage = 'Спасибо! Мы свяжемся с вами в самое ближайшее время.';
		if (!$city) $to .= ',magazin@julianna.ru';
		break;
	case 'frm2':
		$subject = 'Уведомление сервиса "Акция Ортобум - Записаться на консультацию ортопеда"';
		$message =  'Имя: ' . $_POST['name'] . "\r\n" .
			'Телефон: ' . $_POST['phone'];
		//$successMessage = 'Спасибо! Мы свяжемся с вами в самое ближайшее время.';
		if (!$city) $to .= ',tt52-1@julianna.ru';
		break;	
	case 'frm3':
		$subject = 'Уведомление сервиса "Акция Ортобум - Обратный звонок"';
		$message =  'Имя: ' . $_POST['name'] . "\r\n" .
			'Телефон: ' . $_POST['phone'];
		//$successMessage = 'Спасибо! Мы свяжемся с вами в самое ближайшее время.';
		if (!$city) $to .= ',magazin@julianna.ru';
		break;
	case 'frm4':
		$subject = 'Уведомление сервиса "Акция Ортобум - Получить консультацию по подбору обуви"';
		$message =  'Имя: ' . $_POST['name'] . "\r\n" .
			'Телефон: ' . $_POST['phone'];
		//$successMessage = 'Спасибо! Мы свяжемся с вами в самое ближайшее время.';
		if (!$city) $to .= ',tt52-1@julianna.ru';
		break;
	case 'frm5':
		$subject = 'Уведомление сервиса "Акция Ортобум - Покупка товара"';
		$message =  'Имя: ' . $_POST['name'] . "\r\n" .
			'Телефон: ' . $_POST['phone'] . "\r\n" .
			'Порядковый номер товара со страницы акции: ' . $_POST['product'];
		//$successMessage = 'Спасибо! Мы свяжемся с вами в самое ближайшее время.';
		break;	
	default:
		die();
}

//$to = 'ko-alex@mail.ru';

$headers = 'From: noreply@orthoboom.ru' . "\r\n" .
    'Reply-To: noreply@orthoboom.ru' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

$res = mail($to, $subject, $message, $headers);
echo intval($res);

?>