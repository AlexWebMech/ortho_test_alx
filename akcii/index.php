<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Акции");
$APPLICATION->SetPageProperty('title', 'Акции | Интернет-магазин ORTHOBOOM');
$APPLICATION->SetPageProperty('description', 'Акции');

?>

<?

$cntIBLOCK_List = "offers";
$cache = new CPHPCache();
$cache_time = 86400;
$cache_id = 'arIBlockListID'.$cntIBLOCK_List;
$cache_path = 'arIBlockListID';
if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path))
{
    $res = $cache->GetVars();
    if (is_array($res["arIBlockListID"]) && (count($res["arIBlockListID"]) > 0))
        $arIBlockListID = $res["arIBlockListID"];
}
if (!is_array($arIBlockListID))
{
    $myarSelect = Array("ID","CATALOG_GROUP_91");
    $products = CIBlockElement::GetList(
        array(),
        array(
            "IBLOCK_ID"           => PRODUCT_CATALOG_IBLOCK,
            "SECTION_ID"          => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "CATALOG_AVAILABLE"   => "Y",
            "ACTIVE" => "Y",
        ),
        false,
        false,
        $myarSelect
    );
    while ($arProductDiscounts = $products->Fetch())
    {
        $price = CCatalogProduct::GetOptimalPrice($arProductDiscounts['ID'], 1, array(1, 2, 3, 4), 'N');
        $arProductDiscounts['DISCOUNT'] = $price['RESULT_PRICE']['DISCOUNT_PRICE'];

        if ((float)$arProductDiscounts['DISCOUNT'] != (float)$arProductDiscounts['CATALOG_PRICE_91']) {
            $arIBlockListID[]=$arProductDiscounts['ID'];
        }

    }
    if ($cache_time > 0)
    {
        $cache->StartDataCache($cache_time, $cache_id, $cache_path);
        $cache->EndDataCache(array("arIBlockListID"=>$arIBlockListID));
    }
}

if (empty($arIBlockListID)) {
    $arIBlockListID = 1;
}
$GLOBALS['arrFilterAkcii'] = array("ID"=>$arIBlockListID);


?>


    <section class="catalog">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 pl-sm-0">
                    <?php
                    $APPLICATION->IncludeComponent(
                        (defined('SMART_FILTER_UUID_VALUE') && SMART_FILTER_UUID_VALUE)?"gorgoc:catalog.smart.filter":"bitrix:catalog.smart.filter",
                        "series",
                        array(
                            "IBLOCK_TYPE" => "",
                            "IBLOCK_ID" => PRODUCT_CATALOG_IBLOCK,
                            "SECTION_ID" => "",
                            "FILTER_NAME" => "arrFilterAkcii",
                            "PRICE_CODE" => array(
                                0 => "MAIN_BASE_PRICE",
                            ),
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => 0,//$arParams["CACHE_TIME"],
                            "CACHE_GROUPS" => "N",
                            "SAVE_IN_SESSION" => "N",
                            "FILTER_VIEW_MODE" => "VERTICAL",
                            "XML_EXPORT" => "Y",
                            "SECTION_TITLE" => "NAME",
                            "SECTION_DESCRIPTION" => "DESCRIPTION",
                            "HIDE_NOT_AVAILABLE" => "Y",
                            "TEMPLATE_THEME" => "site",
                            "CONVERT_CURRENCY" => "N",
                            'CURRENCY_ID' => "",
                            "SEF_MODE" => "Y",
                            "SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
                            "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                            "PAGER_PARAMS_NAME" => "arrPager",
                            "INSTANT_RELOAD" => "N",
                            "SHOW_ALL_WO_SECTION"=>"Y"
                        ),
                        $component,
                        array('HIDE_ICONS' => 'Y')
                    );
                    ?>
                </div>
                <div class="col-lg-9 pr-sm-0">
                    <h1 class="titled mb-3"><?$APPLICATION->ShowTitle(false);?></h1>

                    <?
                    switch ($_REQUEST["sort"]) {
                        case "price_asc":
                            $forSort = "property_MIN_PRICE";
                            $forOrder = 'asc,nulls';
                            break;
                        case "price_desc":
                            $forSort = "property_MIN_PRICE";
                            $forOrder = 'desc,nulls';
                            break;
                        case "new":
                            $forSort = "property_NOVINKA_INTERNET_MAGAZIN";
                            $forOrder = 'asc,nulls';
                            break;
                        case "spec_pred":
                            $forSort = "property_AKTSIYA_INTERNET_MAGAZIN";
                            $forOrder = 'asc,nulls';
                            break;
                    }

                    if ($_REQUEST["show"]) {
                        $show = $_REQUEST["show"];
                    } else {
                        $show = 36;
                    }

                    $intSectionID = $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "offers",
                        array(
                            "FILTER_NAME" => "arrFilterAkcii",
                            "IBLOCK_TYPE" => "",
                            "IBLOCK_ID" => PRODUCT_CATALOG_IBLOCK,
                            "ELEMENT_SORT_FIELD" => $forSort,
                            "ELEMENT_SORT_ORDER" => $forOrder,
                            "ELEMENT_SORT_FIELD2" => $forSort,
                            "ELEMENT_SORT_ORDER2" =>  $forOrder,
                            "PROPERTY_CODE" => array(
                                0 => "",
                                1 => "NEWPRODUCT",
                                2 => "SALELEADER",
                                3 => "SPECIALOFFER",
                                4 => "OBUV_RAZMER",
                                5 => "ARTNUMBER",
                                6 => "SIZES_SHOES",
                                7 => "SIZES_CLOTHES",
                                8 => "COLOR_REF",
                                9 => "READ_PREVIEW",
                                10 => "GALLERY",
                            ),
                            "META_KEYWORDS" => "-",
                            "META_DESCRIPTION" => "-",
                            "BROWSER_TITLE" => "-",
                            "SET_LAST_MODIFIED" => "N",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "BASKET_URL" => "",
                            "ACTION_VARIABLE" => "action",
                            "PRODUCT_ID_VARIABLE" => "id",
                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                            "PRODUCT_PROPS_VARIABLE" => "prop",
                            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "N",
                            "SET_TITLE" => "Y",
                            "MESSAGE_404" => "",
                            "SET_STATUS_404" => "Y",
                            "SHOW_404" => "Y",
                            "FILE_404" => "",
                            "DISPLAY_COMPARE" => "N",
                            "PAGE_ELEMENT_COUNT" => $show,
                            "LINE_ELEMENT_COUNT" => "3",
                            "PRICE_CODE" => array(
                                0 => "MAIN_BASE_PRICE",
                            ),
                            "USE_PRICE_COUNT" => "N",
                            "SHOW_PRICE_COUNT" => "1",
                            "PRICE_VAT_INCLUDE" => "Y",
                            "USE_PRODUCT_QUANTITY" => "Y",
                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                            "PRODUCT_PROPERTIES" => array(),

                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "PAGER_TITLE" => "",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "modern",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_BASE_LINK" => "",
                            "PAGER_PARAMS_NAME" => "arrPager",

                            "OFFERS_CART_PROPERTIES" => array(),
                            "OFFERS_FIELD_CODE" => array(
                                0 => "NAME",
                                1 => "PREVIEW_PICTURE",
                                2 => "DETAIL_PICTURE",
                                3 => "",
                            ),
                            "OFFERS_PROPERTY_CODE" => array(
                                0 => "MORE_PHOTO",
                                1 => "SIZES_SHOES",
                                2 => "SIZES_CLOTHES",
                                3 => "COLOR_REF",
                                4 => "ARTNUMBER",
                                5 => "",
                            ),
                            "OFFERS_SORT_FIELD" => "sort",
                            "OFFERS_SORT_FIELD2" => "id",
                            "OFFERS_SORT_ORDER" => "desc",
                            "OFFERS_SORT_ORDER2" => "desc",
                            "OFFERS_LIMIT" => "0",

                            "SECTION_ID" => "",
                            "SECTION_URL" => "#SECTION_CODE_PATH#/",
                            "DETAIL_URL" => "../product/#ELEMENT_CODE#/",
                            "USE_MAIN_ELEMENT_SECTION" => "Y",
                            "CONVERT_CURRENCY" => "N",
                            'CURRENCY_ID' => "",
                            'HIDE_NOT_AVAILABLE' => 'Y',

                            "LABEL_PROP" => array(),
                            "ADD_PICT_PROP" => "-",
                            "PRODUCT_DISPLAY_MODE" => "Y",

                            "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
                            "OFFER_TREE_PROPS" => array(),
                            "PRODUCT_SUBSCRIPTION" => "Y",
                            "SHOW_DISCOUNT_PERCENT" => "Y",
                            "SHOW_OLD_PRICE" => "Y",
                            "MESS_BTN_BUY" => "Купить",
                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                            "MESS_BTN_DETAIL" => "Подробнее",
                            'MESS_NOT_AVAILABLE' => 'Нет в наличии',

                            "TEMPLATE_THEME" => "site",
                            "ADD_SECTIONS_CHAIN" => "Y",
                            'ADD_TO_BASKET_ACTION' => "ADD",
                            'SHOW_CLOSE_POPUP' => 'N',
                            'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
                            'BACKGROUND_IMAGE' => '',
                            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                            "SHOW_ALL_WO_SECTION" => "Y"
                        ),
                        $component
                    );?>
                </div>
            </div>
        </div>
    </section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>