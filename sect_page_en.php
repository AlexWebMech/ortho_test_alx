<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle("Main page");
$APPLICATION->SetPageProperty('title', 'Buy ORTHOBOOM orthopaedic shoes online');
$APPLICATION->SetPageProperty('description', 'ORTHOBOOM shoes advantages. «ORTHOBOOM» - real shoes');
?>  
<script type="text/javascript">
			$(document).ready(function() {

				$("#owl-demo").owlCarousel({
					navigation : true,
					slideSpeed : 1000,
					paginationSpeed : 1200,
					singleItem:true,
					navigationText: ["",""],
					autoPlay: 4000,
					rewindSpeed: 0

				});   
			}); 
</script>
		<div class="slider">

			<?$APPLICATION->IncludeComponent("bitrix:news.list", "owl-carousel-hidden-div", array(
	"IBLOCK_TYPE" => "1",
	"IBLOCK_ID" => "1",
	"NEWS_COUNT" => "20",
	"SORT_BY1" => "ID",
	"SORT_ORDER1" => "ASC",
	"SORT_BY2" => "",
	"SORT_ORDER2" => "",
	"FILTER_NAME" => "",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "NAME_ENG",
		1 => "TEXT_ENG",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "",
	"SET_STATUS_404" => "N",
	"SET_TITLE" => "Y",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
	"ADD_SECTIONS_CHAIN" => "Y",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "",
	"INCLUDE_SUBSECTIONS" => "Y",
	"PAGER_TEMPLATE" => "",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "Y",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>

						
	
		</div>

		<div class="parts">
			<h1 class="center">Online shop</h1><br />
			<div class="h1">ORTHOBOOM orthopaedic shoes have:</div>
			<a href="info/konstruktsia-obuvi/" class="container" style="display: block;">
<!--
				<p id="part1">Extended or butterfly-shaped heel caps</p>
				<p id="part2">Soft collars</p>				
				<p id="part3">Hook-and-loop straps and laces</p>
				<p id="part4">Removable shell-shaped insoles</p>
-->
			</a>
		</div>
<script>
$('.parts .container').css('background', 'url(/images/part2_en.png) 30px center no-repeat');
</script>	
		<div class="about">
			<div class="container">
				<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"benefit-list", 
	array(
		"IBLOCK_TYPE" => "1",
		"IBLOCK_ID" => "2",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ID",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "",
		"SORT_ORDER2" => "",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "NAME_ENG",
			1 => "TEXT_ENG",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"PAGER_TEMPLATE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "benefit-list",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?>

			</div>
		</div>
		<div class="profit">
			<div class="container">
				<div class="h1">On the whole, why should you use ORTHOBOOM shoes?</div>
				<div class="span2"><img alt="" src="/images/foots.png"></div>
				<div class="span1">				
				</div>
				<div class="span9 green">
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "sect",
							"AREA_FILE_SUFFIX" => "polza_en",
							"EDIT_TEMPLATE" => "",
							"AREA_FILE_RECURSIVE" => "Y"
						)
					);?>
				</div>
					
			</div>
		</div>
		<div class="forma">
			<div class="container">
				<div class="callback">

						<h2 class="center">Please ask us about our shoes</h2>


<?$APPLICATION->IncludeComponent(
	"altasib:feedback.form", 
	"template_common", 
	array(
		"IBLOCK_TYPE" => "feedback_structured",
		"IBLOCK_ID" => "8",
		"FORM_ID" => "1",
		"EVENT_TYPE" => "ALX_FEEDBACK_FORM",
		"PROPERTY_FIELDS" => array(
			0 => "FIO",
			1 => "TEL",
			2 => "EMAIL",
			3 => "FEEDBACK_TEXT",
		),
		"PROPERTY_FIELDS_REQUIRED" => array(
		),
		"NAME_ELEMENT" => "ALX_DATE",
		"BBC_MAIL" => "feedback@julianna.ru",
		"MESSAGE_OK" => "Message sent!",
		"CHECK_ERROR" => "Y",
		"ACTIVE_ELEMENT" => "Y",
		"USE_CAPTCHA" => "Y",
		"SEND_MAIL" => "N",
		"HIDE_FORM" => "Y",
		"USERMAIL_FROM" => "N",
		"SHOW_MESSAGE_LINK" => "Y",
		"REWIND_FORM" => "N",
		"WIDTH_FORM" => "50%",
		"SIZE_NAME" => "12px",
		"COLOR_NAME" => "#000000",
		"SIZE_HINT" => "10px",
		"COLOR_HINT" => "#000000",
		"SIZE_INPUT" => "12px",
		"COLOR_INPUT" => "#727272",
		"BACKCOLOR_ERROR" => "#ffffff",
		"COLOR_ERROR_TITLE" => "#A90000",
		"COLOR_ERROR" => "#8E8E8E",
		"IMG_ERROR" => "/upload/altasib.feedback.gif",
		"BORDER_RADIUS" => "3px",
		"COLOR_MESS_OK" => "#963258",
		"IMG_OK" => "/upload/altasib.feedback.ok.gif",
		"CATEGORY_SELECT_NAME" => "Select category",
		"SECTION_MAIL_ALL" => "",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"ALX_CHECK_NAME_LINK" => "N",
		"CAPTCHA_TYPE" => "default",
		"JQUERY_EN" => "Y",
		"SECTION_MAIL9" => "",
		"SECTION_MAIL11" => "",
		"SECTION_MAIL10" => "",
		"SECTION_MAIL7" => "",
		"SECTION_MAIL8" => ""
	),
	false
);?>

						
					
				</div>
			</div>
		</div>
