<?
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if ( $_REQUEST["product-id"] && $_REQUEST["question-name"] && $_REQUEST["question-phone"] && $_REQUEST["question-email"] && $_REQUEST["question-text"] && $_REQUEST["question-date"]) {
    \Bitrix\Main\Loader::includeModule('iblock');
    $arrayProperties = [];
    $values = [];
    $objElement = new CIBlockElement();
    /*
	$arrayProperties[2624] = iconv("UTF-8", "windows-1251", $_REQUEST["question-name"]);
    $arrayProperties[2625] = iconv("UTF-8", "windows-1251", $_REQUEST["question-phone"]);
    $arrayProperties[2626] = iconv("UTF-8", "windows-1251", $_REQUEST["question-email"]);
    $arrayProperties[2627] = iconv("UTF-8", "windows-1251", $_REQUEST["question-text"]);
	*/
	
	$arrayProperties[2624] = $_REQUEST["question-name"];
    $arrayProperties[2625] = $_REQUEST["question-phone"];
    $arrayProperties[2626] = $_REQUEST["question-email"];
    $arrayProperties[2627] = $_REQUEST["question-text"];	
    $arrayProperties[2628] = $_REQUEST["product-id"];
    $arLoadProductArray = [
        //"NAME"            => iconv("UTF-8", "windows-1251", $_REQUEST["question-name"]) . ' - ' . date("d.m.Y", $_REQUEST["question-date"]),
        "NAME"            => $_REQUEST["question-name"],
        "IBLOCK_ID"       => PRODUCT_QUESTIONS_IBLOCK,
        "PROPERTY_VALUES" => $arrayProperties,
        "DATE_ACTIVE_FROM"=> date("d.m.Y", $_REQUEST["question-date"]),
        "ACTIVE"          => "N",
    ];
    $productId = $objElement->Add($arLoadProductArray);
    if ($productId === false) {
        $return["status"] = "error";
        $return["message"] = $objElement->LAST_ERROR;
        echo "<pre>";
        print_r($return);
        echo "</pre>";
        die();
    } else {
        echo "OK";
    }
}