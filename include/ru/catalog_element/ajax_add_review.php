<?
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if ( $_REQUEST["element"] && $_REQUEST["text"] && $_REQUEST["rating"] && $_REQUEST["author"] && $_REQUEST["timestamp"]) {
    \Bitrix\Main\Loader::includeModule('iblock');
    $arrayProperties = [];
    $values = [];
    $objElement = new CIBlockElement();
    
	/* $arrayProperties[2619] = iconv("UTF-8", "windows-1251", $_REQUEST["element"]);
    $arrayProperties[2620] = iconv("UTF-8", "windows-1251", $_REQUEST["author"]);
    $arrayProperties[2622] = iconv("UTF-8", "windows-1251", $_REQUEST["text"]);
	*/
	$arrayProperties[2619] = $_REQUEST["element"];
    $arrayProperties[2620] = $_REQUEST["author"];
    $arrayProperties[2622] = $_REQUEST["text"];	
	
	
    $arrayProperties[2621] = $_REQUEST["rating"];
    $arLoadProductArray = [
        //"NAME"            => iconv("UTF-8", "windows-1251", $_REQUEST["author"]) . ' - ' . date("d.m.Y", $_REQUEST["timestamp"]),
        "NAME"            => $_REQUEST["author"] . ' - ' . date("d.m.Y", $_REQUEST["timestamp"]),
        "IBLOCK_ID"       => PRODUCT_REVIEWS_IBLOCK,
        "PROPERTY_VALUES" => $arrayProperties,
        "DATE_ACTIVE_FROM"=> date("d.m.Y", $_REQUEST["timestamp"]),
        "ACTIVE"          => "N",
    ];
    $productId = $objElement->Add($arLoadProductArray);
    if ($productId === false) {
        $return["status"] = "error";
        $return["message"] = $objElement->LAST_ERROR;
        echo "<pre>";
        print_r($return);
        echo "</pre>";
        die();
    } else {
        echo "OK";
    }
}