<?php
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) {
    die();
}
header('Content-Type: application/json; charset=utf-8');

\Bitrix\Main\Loader::includeModule('catalog');
global  $APPLICATION;

$arResult = array('start' => true);
if ( $_REQUEST["id"] && $_REQUEST["quantity"] )
{ 
	$arRewrite = array();
	if ($_REQUEST["fixed_price"]) {
        $arRewrite = [
            "NAME" => $_REQUEST["product_name"],
            "PRODUCT_PROVIDER_CLASS" => "\Bitrix\Catalog\Product\CatalogProvider",
            //'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
            "CURRENCY" => "RUB",
            "CAN_BUY" => "Y",
            'CUSTOM_PRICE' => 'Y',
            "MODULE" => "catalog",
            "PRICE" => $_REQUEST["fixed_price"],
            //'IGNORE_CALLBACK_FUNC' => 'N',
        ];
	}

    Add2BasketByProductID(
        $_REQUEST["id"],
        $_REQUEST["quantity"],
        $arRewrite,
        array()
    );

    if ($ex = $APPLICATION->GetException())
    {
		$arResult['error'] = normJsonStr($ex->GetString());
    }
    else
    {
        if(!\Bitrix\Main\Loader::includeModule("sale") || !\Bitrix\Main\Loader::includeModule("catalog"))
        {
            $arResult['error'] = "Ошибка подключения модулей";
        }
        else
        {
            $basket = Bitrix\Sale\Basket::loadItemsForFUser(Bitrix\Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
            $basketItems = $basket->getBasketItems();
            foreach ($basketItems as $basketItem)
            {
                if($basketItem->canBuy())
                    $arResult['CART']++;
            }
            $basket->save();
        }
    }
}
echo json_encode($arResult);


function normJsonStr($str){
    $str = preg_replace_callback('/\\\\u([a-f0-9]{4})/i', create_function('$m', 'return chr(hexdec($m[1])-1072+224);'), $str);
    return $str;
}
