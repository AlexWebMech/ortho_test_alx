<?php
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) {
    die();
}
header('Content-Type: application/json; charset=utf-8');

\Bitrix\Main\Loader::includeModule('catalog');
global  $APPLICATION;

/* function MY_CALLBACK_FUNC($PRODUCT_ID, $QUANTITY = 0){
      $arResult = array(
		//"CUSTOM_PRICE" = "Y",
		"PRODUCT_PROVIDER_CLASS" => "",
		"CURRENCY" => "RUB",
		"QUANTITY" => 1,
		//"LID" => s1,
		"DELAY" => "N",
		"CAN_BUY" => "Y",
		"MODULE" => "catalog",
		"DISCOUNT_VALUE" => "21",
		"DISCOUNT_PRICE" => "999",
		"DISCOUNT_NAME" => "Лови момент"
      );
      return $arResult;
} */


$arResult = array('start' => true);
if ( $_REQUEST["id"] && $_REQUEST["quantity"] )
{
    Add2BasketByProductID(
        $_REQUEST["id"],
        $_REQUEST["quantity"],
        array(//"CUSTOM_PRICE" = "Y",
		"PRODUCT_PROVIDER_CLASS" => "",
		"CURRENCY" => "RUB",
		"QUANTITY" => 1,
		//"LID" => s1,
		"DELAY" => "N",
		"CAN_BUY" => "Y",
		"MODULE" => "catalog",
		"DISCOUNT_VALUE" => "21",
		"DISCOUNT_PRICE" => "22",
		"DISCOUNT_NAME" => "Лови момент"),
		//array(),
        array()
    );
    if ($ex = $APPLICATION->GetException())
    {
		$arResult['error'] = normJsonStr($ex->GetString());
    }
    else
    {
        if(!\Bitrix\Main\Loader::includeModule("sale") || !\Bitrix\Main\Loader::includeModule("catalog"))
        {
            $arResult['error'] = "Ошибка подключения модулей";
        }
        else
        {
            $basket = Bitrix\Sale\Basket::loadItemsForFUser(Bitrix\Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
            $basketItems = $basket->getBasketItems();
            foreach ($basketItems as $basketItem)
            {
                if($basketItem->canBuy())
                    $arResult['CART']++;
            }
        }
    }
}
echo json_encode($arResult);


function normJsonStr($str){
    $str = preg_replace_callback('/\\\\u([a-f0-9]{4})/i', create_function('$m', 'return chr(hexdec($m[1])-1072+224);'), $str);
    return iconv('cp1251', 'utf-8', $str);
}
